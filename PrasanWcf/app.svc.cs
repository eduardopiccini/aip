﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;
using log4net;

namespace PrasanWcf
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    public class app : Iapp
    {
        private static readonly ILog log = LogManager.GetLogger("Log");
        private static readonly ILog logSummary = LogManager.GetLogger("LogSummary");

        public RechargeResponse Recharge(RechargeRequest Request)
        {
            RechargeResponse rechargeResponse = new RechargeResponse();
            StringBuilder param = new StringBuilder();
            StringBuilder xmlReq = new StringBuilder();
            String toMd5 = null, xml = null, msg = null;
            Uri uri;
            HttpWebRequest req = null;
            HttpWebResponse resp = null;
            byte[] data = null;
            StreamReader reader = null;
            System.Xml.XmlDocument xdoc = new System.Xml.XmlDocument();
            System.Xml.XmlNode eCode, eMessage, eConfirmationCode;
            XmlNamespaceManager manager = null;
            StringBuilder output = new StringBuilder();
            DateTime? begin = DateTime.Now, end = null;


            try
            {
                if (String.IsNullOrEmpty(Request.Phone))
                    throw new Exception(Common.Utils.PHONE_NUMBER_REQUIRE);

                if (Request.Type.Equals(Common.Utils.FLEXI_RECHARGE))
                {
                    if (String.IsNullOrEmpty(Request.Amount))
                        throw new Exception(Common.Utils.AMOUNT_REQUIRE);
                }

                if (String.IsNullOrEmpty(Request.TransactionId))
                    throw new Exception(Common.Utils.TRANSACTION_ID_REQUIRE);

                param.Append(Request.LoginId);
                param.Append("|");
                param.Append(Request.TransactionId);
                param.Append("|");
                param.Append(Request.ProductId);
                param.Append("|2|");
                param.Append(Request.Phone);
                param.Append("|");

                if (Request.Type.Equals(Common.Utils.FLEXI_RECHARGE))
                {
                    param.Append(Request.Amount);
                    param.Append("|");
                }

                param.Append(Request.ProtectedKey);

                toMd5 = Common.Utils.MD5_of_SHA1(param.ToString());

                xmlReq.Append("<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:m0=\"http://soapinterop.org/xsd\">");
                xmlReq.Append("<SOAP-ENV:Body>");
                xmlReq.Append("<m:");
                xmlReq.Append(Request.Type);
                xmlReq.Append("Recharge xmlns:m=\"");
                xmlReq.Append(Request.Url.Replace("/iTopUp/reseller_itopup.server.php", "/reseller_iTopUp/reseller_iTopUp.wsdl.php"));
                xmlReq.Append("\" SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">");
                xmlReq.Append("<");
                xmlReq.Append(Request.Type);
                xmlReq.Append("Recharge_Request xsi:type=\"m0:");
                xmlReq.Append(Request.Type);
                xmlReq.Append("Recharge_Request\"><LoginId xsi:type=\"xsd:string\">{0}</LoginId>");
                xmlReq.Append("<RequestId xsi:type=\"xsd:string\">{1}</RequestId>");
                xmlReq.Append("<BatchId xsi:type=\"xsd:string\">{2}</BatchId>");
                xmlReq.Append("<SystemServiceID xsi:type=\"xsd:string\">{3}</SystemServiceID>");
                xmlReq.Append("<MobileNumber xsi:type=\"xsd:string\">{4}</MobileNumber>");

                if (Request.Type.Equals(Common.Utils.FLEXI_RECHARGE))
                {
                    xmlReq.Append("<Amount xsi:type=\"xsd:string\">");
                    xmlReq.Append(Request.Amount);
                    xmlReq.Append("</Amount>");
                }


                xmlReq.Append("<Checksum xsi:type=\"xsd:string\">{5}</Checksum>");
                xmlReq.Append("</");
                xmlReq.Append(Request.Type);
                xmlReq.Append("Recharge_Request>");
                xmlReq.Append("</m:");
                xmlReq.Append(Request.Type);
                xmlReq.Append("Recharge></SOAP-ENV:Body></SOAP-ENV:Envelope>");

                xml = String.Format(xmlReq.ToString(), Request.LoginId, Request.TransactionId, Request.ProductId, "2", Request.Phone, toMd5);

                rechargeResponse.RequestMsg = xml;

                uri = new Uri(Request.Url);
                req = (HttpWebRequest)WebRequest.Create(uri);

                req.Headers.Add("SOAPAction: " + Request.Url.Replace("/iTopUp/reseller_itopup.server.php", "/reseller_iTopUp/reseller_iTopUp.wsdl.php"));

                req.ContentType = "text/xml";
                req.Method = "POST";

                data = System.Text.ASCIIEncoding.UTF8.GetBytes(xml);

                req.ContentLength = data.Length;
                System.IO.Stream os = req.GetRequestStream();
                os.Write(data, 0, data.Length);
                os.Close();

                resp = (HttpWebResponse)req.GetResponse();

                reader = new StreamReader(resp.GetResponseStream());

                msg = reader.ReadToEnd();

                rechargeResponse.ResponseMsg = msg;

                xdoc.LoadXml(msg);

                manager = new XmlNamespaceManager(xdoc.NameTable);
                manager.AddNamespace("ns4", Request.Url.Replace("/iTopUp/reseller_itopup.server.php", "/reseller_iTopUp/reseller_iTopUp.wsdl.php"));
                manager.AddNamespace("SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
                manager.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
                manager.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");


                eCode = xdoc.SelectSingleNode("/SOAP-ENV:Envelope/SOAP-ENV:Body/ns4:" + Request.Type + "RechargeResponse/" + Request.Type + "Recharge_Response/ResponseCode", manager);
                eMessage = xdoc.SelectSingleNode("/SOAP-ENV:Envelope/SOAP-ENV:Body/ns4:" + Request.Type + "RechargeResponse/" + Request.Type + "Recharge_Response/ResponseDescription", manager);
                eConfirmationCode = xdoc.SelectSingleNode("/SOAP-ENV:Envelope/SOAP-ENV:Body/ns4:" + Request.Type + "RechargeResponse/" + Request.Type + "Recharge_Response/ConfirmationCode", manager);

                rechargeResponse.Code = (eCode != null) ? eCode.InnerText : Common.Utils.RECHARGE_NO_RESPONSE;
                rechargeResponse.Message = (eMessage != null) ? eMessage.InnerText : "";
                rechargeResponse.AuthorizationNumber = (eConfirmationCode != null) ? eConfirmationCode.InnerText : "";
                rechargeResponse.TransactionDateTime = DateTime.Now.ToString();

                if (rechargeResponse.Code.Equals("000"))
                {
                    rechargeResponse.Code = Common.Utils.APPROVED;
                }
            }
            catch (Exception ex)
            {
                rechargeResponse.Code = Common.Utils.UNCAUGHT_ERROR;
                rechargeResponse.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                rechargeResponse.Details = ex.Message;
            }
            finally
            {
                if (req != null)
                {
                    req = null;
                }

                if (data != null)
                {
                    data = null;
                }

                if (reader != null)
                {
                    reader = null;
                }

                end = DateTime.Now;

                if (!String.IsNullOrEmpty(Request.RoutingId))
                {
                    using (TransLogWs.Service ws = new TransLogWs.Service())
                    {
                        TransLogWs.LogRequest requ = new TransLogWs.LogRequest();

                        requ.AuthorizationNumber = rechargeResponse.AuthorizationNumber;
                        requ.BeginDate = begin.Value;
                        requ.EndDate = end.Value;
                        requ.Code = rechargeResponse.Code;
                        requ.Message = rechargeResponse.Message;
                        requ.DbUser = Request.DbUser;
                        requ.Locale = Request.Locale;
                        requ.RoutingId = Request.RoutingId;

                        ws.LogAsync(requ);
                    }
                }

                Common.Utils.Concat(ref output, null, "RECHARGE:", false, true);
                Common.Utils.Concat(ref output, "Code", rechargeResponse.Code, false, false);
                Common.Utils.Concat(ref output, "Message", rechargeResponse.Message, false, false);
                Common.Utils.Concat(ref output, "AuthNumber", rechargeResponse.AuthorizationNumber, false, false);
                Common.Utils.Concat(ref output, "TranDateTime", rechargeResponse.TransactionDateTime, false, false);
                Common.Utils.Concat(ref output, "Amount", Request.Amount, false, false);
                Common.Utils.Concat(ref output, "Phone", Request.Phone, false, false);
                Common.Utils.Concat(ref output, "TransactionId", Request.TransactionId, false, false);
                Common.Utils.Concat(ref output, "RoutingId", Request.RoutingId, false, false);
                Common.Utils.Concat(ref output, "Type", Request.Type, false, true);

                logSummary.Debug(output.ToString());
                
                Common.Utils.Concat(ref output, ",Url", Request.Url, false, false);
                Common.Utils.Concat(ref output, "RequestMsg", rechargeResponse.RequestMsg, true, true);
                Common.Utils.Concat(ref output, "ResponseMsg", rechargeResponse.ResponseMsg, true, true);

                log.Debug(output.ToString());

            }

            return rechargeResponse;
        }

        public Response Void(VoidRequest Request)
        {
            Response voidResponse = new Response();
            StringBuilder param = new StringBuilder();
            StringBuilder xmlReq = new StringBuilder();
            String toMd5 = null, xml = null, msg = null;
            Uri uri;
            HttpWebRequest req = null;
            HttpWebResponse resp = null;
            byte[] data = null;
            StreamReader reader = null;
            System.Xml.XmlDocument xdoc = new System.Xml.XmlDocument();
            System.Xml.XmlNode eCode, eMessage;
            XmlNamespaceManager manager = null;
            StringBuilder output = new StringBuilder();
            DateTime? begin = DateTime.Now, end = null;

            try
            {
                if (String.IsNullOrEmpty(Request.TransactionId))
                    throw new Exception(Common.Utils.TRANSACTION_ID_REQUIRE);

                param.Append(Request.LoginId);
                param.Append("|");
                param.Append(Request.TransactionId);
                param.Append("|");
                param.Append(Request.ProtectedKey);

                toMd5 = Common.Utils.MD5_of_SHA1(param.ToString());

                xmlReq.Append("<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:m0=\"http://soapinterop.org/xsd\">");
                xmlReq.Append("<SOAP-ENV:Body>");
                xmlReq.Append("<m:");
                xmlReq.Append(Request.Type);
                xmlReq.Append("Void xmlns:m=\"");
                xmlReq.Append(Request.Url.Replace("/iTopUp/reseller_itopup.server.php", "/reseller_iTopUp/reseller_iTopUp.wsdl.php"));
                xmlReq.Append("\" SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">");
                xmlReq.Append("<");
                xmlReq.Append(Request.Type);
                xmlReq.Append("Void_Request xsi:type=\"m0:");
                xmlReq.Append(Request.Type);
                xmlReq.Append("Void_Request\"><LoginId xsi:type=\"xsd:string\">{0}</LoginId>");
                xmlReq.Append("<RequestId xsi:type=\"xsd:string\">{1}</RequestId>");
                xmlReq.Append("<Checksum xsi:type=\"xsd:string\">{2}</Checksum>");
                xmlReq.Append("</");
                xmlReq.Append(Request.Type);
                xmlReq.Append("Void_Request>");
                xmlReq.Append("</m:");
                xmlReq.Append(Request.Type);
                xmlReq.Append("Void></SOAP-ENV:Body></SOAP-ENV:Envelope>");

                xml = String.Format(xmlReq.ToString(), Request.LoginId, Request.TransactionId, toMd5);

                voidResponse.RequestMsg = xml;

                uri = new Uri(Request.Url);
                req = (HttpWebRequest)WebRequest.Create(uri);

                req.Headers.Add("SOAPAction: " + Request.Url.Replace("/iTopUp/reseller_itopup.server.php", "/reseller_iTopUp/reseller_iTopUp.wsdl.php"));

                req.ContentType = "text/xml";
                req.Method = "POST";

                data = System.Text.ASCIIEncoding.UTF8.GetBytes(xml);

                req.ContentLength = data.Length;
                System.IO.Stream os = req.GetRequestStream();
                os.Write(data, 0, data.Length);
                os.Close();

                resp = (HttpWebResponse)req.GetResponse();

                reader = new StreamReader(resp.GetResponseStream());

                msg = reader.ReadToEnd();

                voidResponse.ResponseMsg = msg;

                xdoc.LoadXml(msg);

                manager = new XmlNamespaceManager(xdoc.NameTable);
                manager.AddNamespace("m", Request.Url.Replace("/iTopUp/reseller_itopup.server.php", "/reseller_iTopUp/reseller_iTopUp.wsdl.php"));
                manager.AddNamespace("SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
                manager.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
                manager.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");


                eCode = xdoc.SelectSingleNode("/SOAP-ENV:Envelope/SOAP-ENV:Body/m:" + Request.Type + "VoidResponse/" + Request.Type + "Void_Response/ResponseCode", manager);
                eMessage = xdoc.SelectSingleNode("/SOAP-ENV:Envelope/SOAP-ENV:Body/m:" + Request.Type + "VoidResponse/" + Request.Type + "Void_Response/ResponseDescription", manager);

                voidResponse.Code = (eCode != null) ? eCode.InnerText : Common.Utils.RECHARGE_NO_RESPONSE;
                voidResponse.Message = (eMessage != null) ? eMessage.InnerText : "";
                voidResponse.TransactionDateTime = DateTime.Now.ToString();

                if (voidResponse.Code.Equals("000"))
                {
                    voidResponse.Code = Common.Utils.APPROVED;
                    voidResponse.Message = Common.Utils.APPROVED_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                voidResponse.Code = Common.Utils.UNCAUGHT_ERROR;
                voidResponse.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                voidResponse.Details = ex.Message;
            }
            finally 
            {
                if (req != null)
                {
                    req = null;
                }

                if (data != null)
                {
                    data = null;
                }

                if (reader != null)
                {
                    reader = null;
                }

                end = DateTime.Now;

                if (!String.IsNullOrEmpty(Request.RoutingId))
                {
                    using (TransLogWs.Service ws = new TransLogWs.Service())
                    {
                        TransLogWs.LogRequest requ = new TransLogWs.LogRequest();

                        requ.BeginDate = begin.Value;
                        requ.EndDate = end.Value;
                        requ.Code =  voidResponse.Code;
                        requ.Message = voidResponse.Message;
                        requ.DbUser = Request.DbUser;
                        requ.Locale = Request.Locale;
                        requ.RoutingId = Request.RoutingId;

                        ws.LogAsync(requ);
                    }
                }

                Common.Utils.Concat(ref output, null, "VOID:", false, true);
                Common.Utils.Concat(ref output, "Code", voidResponse.Code, false, false);
                Common.Utils.Concat(ref output, "Message", voidResponse.Message, false, false);
                Common.Utils.Concat(ref output, "TransactionId", Request.TransactionId, false, false);
                Common.Utils.Concat(ref output, "RoutingId", Request.RoutingId, false, false);
                Common.Utils.Concat(ref output, "Type", Request.Type, false, true);

                logSummary.Debug(output.ToString());

                Common.Utils.Concat(ref output, ",Url", Request.Url, false, false);
                Common.Utils.Concat(ref output, "RequestMsg", voidResponse.RequestMsg, true, true);
                Common.Utils.Concat(ref output, "ResponseMsg", voidResponse.ResponseMsg, true, true);

                log.Debug(output.ToString());
            }


            return voidResponse;
        }

    }
}
