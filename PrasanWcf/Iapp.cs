﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace PrasanWcf
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface Iapp
    {

        [OperationContract]
        RechargeResponse Recharge(RechargeRequest Request);

        [OperationContract]
        Response Void(VoidRequest composite);

        // TODO: Add your service operations here
    }

    [DataContract]
    public class VoidRequest
    {
        [DataMember]
        public String TransactionId { get; set; }

        [DataMember]
        public String Url { get; set; }

        [DataMember]
        public String Type { get; set; }

        [DataMember]
        public String LoginId { get; set; }

        [DataMember]
        public String ProtectedKey { get; set; }

        [DataMember]
        public String RoutingId { get; set; }

        [DataMember]
        public String Locale { get; set; }

        [DataMember]
        public String DbUser { get; set; }
    }


    [DataContract]
    public class Response
    {
        [DataMember]
        public String Code { get; set; }

        [DataMember]
        public String Message { get; set; }

        [DataMember]
        public String Details { get; set; }

        [DataMember]
        public String RequestMsg { get; set; }

        [DataMember]
        public String ResponseMsg { get; set; }

        [DataMember]
        public String TransactionDateTime { get; set; }
    }

    [DataContract]
    public class RechargeResponse : Response
    {
        [DataMember]
        public String AuthorizationNumber { get; set; }

    }

    [DataContract]
    public class RechargeRequest
    {
        [DataMember]
        public String Phone { get; set; }

        [DataMember]
        public String Amount { get; set; }

        [DataMember]
        public String ProductId { get; set; }

        [DataMember]
        public String TransactionId { get; set; }

        [DataMember]
        public String Url { get; set; }

        [DataMember]
        public String Type { get; set; }

        [DataMember]
        public String LoginId { get; set; }

        [DataMember]
        public String ProtectedKey { get; set; }

        [DataMember]
        public String RoutingId { get; set; }

        [DataMember]
        public String Locale { get; set; }

        [DataMember]
        public String DbUser { get; set; }
    }
}
