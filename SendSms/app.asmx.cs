﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Text;
using log4net;

namespace SendSms
{


    /// <summary>
    /// Summary description for app
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class app : System.Web.Services.WebService
    {
        private static readonly ILog log = LogManager.GetLogger("Log");

        [WebMethod]
        public String Sms(String accountId, String username, String password, String reference, String phone, String message)
        {
            String smsResponse = String.Empty;
            StringBuilder output = new StringBuilder();

            try
            {
                using (aipws.Service ws = new aipws.Service())
                {
                    ws.CookieContainer = new System.Net.CookieContainer();
                    aipws.Response loginResponse = null;
                    aipws.TransactionResult tranResult = null;

                    loginResponse = ws.Login(username, password, accountId);

                    if (loginResponse.Code.Equals("00"))
                    {
                        tranResult = ws.BuySms(reference, null, "5", phone, message);

                        if (tranResult.Code.Equals("00"))
                        {
                            smsResponse = "TRUE";
                        }
                        else
                        {
                            smsResponse = tranResult.Message;
                        }

                    }
                    else
                    {
                        smsResponse = loginResponse.Message;
                    }
                }
            }
            catch (Exception ex)
            {
                smsResponse = ex.Message;
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "SMS:", false, true);
                Common.Utils.Concat(ref output, "Message", smsResponse, false, false);
                Common.Utils.Concat(ref output, "Reference", reference, false, false);
                Common.Utils.Concat(ref output, "Phone", phone, false, false);
                Common.Utils.Concat(ref output, "Message", message, false, false);
                Common.Utils.Concat(ref output, "UserName", username, false, false);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, true);

                log.Debug(output.ToString());
            }

            return smsResponse;
        }
    }
}
