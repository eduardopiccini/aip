﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using AutoRechargeService.App_Code;
using System.Globalization;

namespace AutoRechargeService
{
    public partial class AutoRechargeService : ServiceBase
    {
        private String AppUser { get; set; }

        public AutoRechargeService()
        {
            InitializeComponent();
            
            if (!System.Diagnostics.EventLog.SourceExists("AutoRechargeService"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "AutoRechargeService", "AutoRechargeServiceLog");
            }
            eventLog.Source = "AutoRechargeService";
            eventLog.Log = "AutoRechargeServiceLog";

            AppUser = System.Configuration.ConfigurationManager.AppSettings["app_user"].ToString();
        }

        protected override void OnStart(string[] args)
        {
            var worker = new Thread(DoWork);
            worker.Name = "AutoRechargeServiceWorker";
            worker.IsBackground = false;
            worker.Start();

            eventLog.WriteEntry("Auto Recharge Service Started!", EventLogEntryType.SuccessAudit);
        }

        protected override void OnStop()
        {
            eventLog.WriteEntry("Auto Recharge Service Stopped!", EventLogEntryType.SuccessAudit);
        }

        public void DoWork()
        {
            List<AutoRecharge> list = null;
            long reference = 0;
            String locale = CultureInfo.CurrentCulture.Name;
            Response respSalePayment = null, respResponse = null;
            AuthWcf.SaleWithPaymentProfileRequest req = null;
            AuthWcf.SaleResponse saleResp = null;
            StringBuilder output = new StringBuilder();

            try
            {
                using (AutoRechargeDao dao = new AutoRechargeDao())
                {
                    using (AuthWcf.IauthClient wcf = new AuthWcf.IauthClient())
                    {
                        while (true)
                        {
                            list = dao.Search();

                            foreach (AutoRecharge item in list)
                            {
                                output = new StringBuilder();

                                reference = DateTime.Now.Ticks;

                                Common.Utils.Concat(ref output, null, "AUTO_RECHARGE:", false, true);
                                Common.Utils.Concat(ref output, "AutoFloorAmount", item.AutoFloorAmount, false, true);
                                Common.Utils.Concat(ref output, "AutoRechargeAmount", item.AutoRechargeAmount, false, true);
                                Common.Utils.Concat(ref output, "CardNumber", item.CardNumber, false, true);
                                Common.Utils.Concat(ref output, "ExpMonth", item.ExpMonth, false, true);
                                Common.Utils.Concat(ref output, "ExpYear", item.ExpYear, false, true);
                                Common.Utils.Concat(ref output, "FirstName", item.FirstName, false, true);
                                Common.Utils.Concat(ref output, "LastName", item.LastName, false, true);
                                Common.Utils.Concat(ref output, "PaymentId", item.PaymentId, false, true);
                                Common.Utils.Concat(ref output, "ProfileId", item.ProfileId, false, true);
                                Common.Utils.Concat(ref output, "UserId", item.UserId, false, true);
                                Common.Utils.Concat(ref output, "Reference", reference, false, true);

                                respSalePayment = dao.SalePayment(item.PaymentId, item.AutoRechargeAmount, reference.ToString(), "AutoRecharge Process",
                                    item.UserId, locale);

                                Common.Utils.Concat(ref output, "ResponseSalePaymentCode", respSalePayment.Code, false, true);
                                Common.Utils.Concat(ref output, "ResponseSalePaymentMessage", respSalePayment.Message, false, true);
                                Common.Utils.Concat(ref output, "LogId", respSalePayment.LogId, false, true);

                                if (respSalePayment.Code.Equals(Common.Utils.APPROVED))
                                {
                                    req = new AuthWcf.SaleWithPaymentProfileRequest();

                                    req.Amount = Convert.ToDecimal(item.AutoRechargeAmount);
                                    req.InvoiceNumber = respSalePayment.LogId;
                                    req.PaymentProfileId = Convert.ToInt64(item.PaymentId);
                                    req.ProfileId = Convert.ToInt64(item.ProfileId);

                                    saleResp = wcf.SaleWithPaymentProfile(req);

                                    Common.Utils.Concat(ref output, "AuthorizeAuthCode", saleResp.AuthorizationCode, false, true);
                                    Common.Utils.Concat(ref output, "AuthorizeCode", saleResp.Code, false, true);
                                    Common.Utils.Concat(ref output, "AuthorizeMessage", saleResp.Description, false, true);
                                    Common.Utils.Concat(ref output, "AuthorizeInvoiceNumber", saleResp.InvoiceNumber, false, true);
                                    Common.Utils.Concat(ref output, "AuthorizeTransactionId", saleResp.TransactionId, false, true);

                                    try
                                    {
                                        respResponse = dao.Response(respSalePayment.LogId, saleResp.Code, saleResp.Description,
                                            saleResp.AuthorizationCode, saleResp.InvoiceNumber, saleResp.TransactionId, item.UserId, locale, AppUser);

                                        Common.Utils.Concat(ref output, "ResponseDbCode", respResponse.Code, false, true);
                                        Common.Utils.Concat(ref output, "ResponseDbMessage", respResponse.Message, false, true);

                                        if (!respResponse.Code.Equals(Common.Utils.APPROVED) && saleResp.Code.Equals(Common.Utils.APPROVED))
                                        { 
                                        
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Common.Utils.Concat(ref output, "ResponseDBError", ex.Message, false, true);

                                        if (saleResp.Code.Equals(Common.Utils.APPROVED))
                                        { 
                                        
                                        }
                                    }
                                    
                                }

                                eventLog.WriteEntry(output.ToString(), EventLogEntryType.Information);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Common.Utils.Concat(ref output, "DoWorkError", ex.Message, false, true);

                eventLog.WriteEntry(output.ToString(), EventLogEntryType.Error);
            }
        }
    }
}
