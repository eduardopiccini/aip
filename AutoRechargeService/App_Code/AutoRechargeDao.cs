﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;

namespace AutoRechargeService.App_Code
{
    public class Response
    {
        public String Code { get; set; }

        public String Message { get; set; }

        public String LogId { get; set; }

        public String ProviderCode { get; set; }
    }

    public class AutoRecharge
    {
        public String PaymentId { get; set; }

        public String ProfileId { get; set; }

        public String CardNumber { get; set; }

        public String ExpMonth { get; set; }

        public String ExpYear { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public String AutoRechargeAmount { get; set; }

        public String AutoFloorAmount { get; set; }

        public String UserId { get; set; }

    }

    public class AutoRechargeDao : IDisposable
    {
        public List<AutoRecharge> Search()
        {
            List<AutoRecharge> list = new List<AutoRecharge>();
            OracleCommand cmd = new OracleCommand();
            StringBuilder query = new StringBuilder();
            DataTable dt = null;

            try
            {
                query.Append(" select payment_id, , profile_id, card_number, exp_month, exp_year, first_name, ");
                query.Append(" last_name, auto_recharge_amount, auto_floor_amount ");
                query.Append(" from cc_vw_country ");

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), String.Empty, String.Empty);

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        AutoRecharge obj = new AutoRecharge();

                        obj.AutoFloorAmount = Common.Utils.ObjectToString(row["auto_floor_amount"]);
                        obj.AutoRechargeAmount = Common.Utils.ObjectToString(row["auto_recharge_amount"]);
                        obj.CardNumber = Common.Utils.ObjectToString(row["card_number"]);
                        obj.ExpMonth = Common.Utils.ObjectToString(row["exp_month"]);
                        obj.ExpYear = Common.Utils.ObjectToString(row["exp_year"]);
                        obj.FirstName = Common.Utils.ObjectToString(row["first_name"]);
                        obj.LastName = Common.Utils.ObjectToString(row["last_name"]);
                        obj.PaymentId = Common.Utils.ObjectToString(row["payment_id"]);
                        obj.ProfileId = Common.Utils.ObjectToString(row["profile_id"]);
                        obj.ProfileId = Common.Utils.ObjectToString(row["user_id"]);

                        list.Add(obj);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return list;
        }

        public Response SalePayment(String paymentId, String amount, String reference, String note, String userId, String locale)
        {
            Response response = new Response();
            OracleCommand cmd = new OracleCommand();

            try
            {
                cmd.Parameters.Add("on_log_id", OracleDbType.Int64).Direction = System.Data.ParameterDirection.Output;
                cmd.Parameters.Add("on_provider_code", OracleDbType.Int64).Direction = System.Data.ParameterDirection.Output;
                cmd.Parameters.Add("oz_response_code", OracleDbType.Varchar2, 1000).Direction = System.Data.ParameterDirection.Output;
                cmd.Parameters.Add("oz_response_message", OracleDbType.Varchar2, 1000).Direction = System.Data.ParameterDirection.Output;
                cmd.Parameters.Add("iz_user_id", OracleDbType.Varchar2, 100).Value = userId;
                cmd.Parameters.Add("in_payment_id", OracleDbType.Int32).Value = paymentId;
                cmd.Parameters.Add("in_trans_amount", OracleDbType.Decimal).Value = amount;
                cmd.Parameters.Add("iz_reference", OracleDbType.Varchar2, 100).Value = reference;
                cmd.Parameters.Add("iz_note", OracleDbType.Varchar2, 100).Value = note;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;

                DataManager.RunDataCommandSP(ref cmd, "cc_pg_trans.sale_payment", String.Empty, String.Empty);

                response.Code = cmd.Parameters["oz_response_code"].Value.ToString();
                response.Message = cmd.Parameters["oz_response_message"].Value.ToString();

                if (response.Code.Equals(Common.Utils.TRANSACTION_IN_PROGRESS))
                {
                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;

                    response.LogId = cmd.Parameters["on_log_id"].Value.ToString();
                    response.ProviderCode = cmd.Parameters["on_provider_code"].Value.ToString();
                }
            }
            catch (Exception)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return response;
        }

        public Response Response(String logId, String responseCode, String responseMessage, 
            String authCode, String invoiceNumber, String transactionId, String userId, String locale, String appUser)
        {
            Response response = new Response();
            OracleCommand cmd = new OracleCommand();

            try
            {
                cmd.Parameters.Add("on_trans_log_id", OracleDbType.Int64).Direction = System.Data.ParameterDirection.Output;
                cmd.Parameters.Add("in_log_id", OracleDbType.Int64).Value = logId;
                cmd.Parameters.Add("iz_response_code", OracleDbType.Varchar2, 200).Value = responseCode;
                cmd.Parameters.Add("iz_response_message", OracleDbType.Varchar2, 200).Value = responseMessage;
                cmd.Parameters.Add("iz_auth_code", OracleDbType.Varchar2, 200).Value = authCode;
                cmd.Parameters.Add("in_invoice_number", OracleDbType.Int64).Value = invoiceNumber;
                cmd.Parameters.Add("in_transaction_id", OracleDbType.Int64).Value = transactionId;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 100).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "cc_pg_trans.response", String.Empty, String.Empty);

                response.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                response.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (response.Code.Equals(Common.Utils.DB_APPROVED))
                {
                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;

                    //response.TransLogId = Convert.ToInt64(cmd.Parameters["on_trans_log_id"].Value.ToString());
                }
            }
            catch (Exception)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return response;
        }

        public void Dispose()
        {

        }
    }
}
