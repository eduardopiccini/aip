﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class ApiLogParameter
    {
        public ApiLogParameter()
        {
        }


        public ApiLogParameter(String logId, String parameterId, String parameterValue, String dateCreated, String userCreated)
        {
            this.LogId = logId;
            this.ParameterId = parameterId;
            ParameterValue = parameterValue;
            this.DateCreated = dateCreated;
            this.UserCreated = userCreated;

        }

        public String LogId { get; set; }
        public String ParameterId { get; set; }
        public String ParameterValue { get; set; }
        public String DateCreated { get; set; }
        public String UserCreated { get; set; }




    }
}
