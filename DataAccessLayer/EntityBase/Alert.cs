﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class Alert
    {
        public Alert()
        {
        }

        public Alert(Int64 id, String description, String abbreviation,
            Int64 statusId, String statusDescription)
        {
            this.Id = id;
            this.Description = description;
            this.Abbreviation = abbreviation;
            this.StatusDescription = statusDescription;
            this.StatusId = statusId;
        }

        public Int64 Id { get; set; }
        public String Description { get; set; }
        public String Abbreviation { get; set; }
        public Int64 StatusId { get; set; }
        public String StatusDescription { get; set; }
    }
}