﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class AccountMerchant
    {

        public AccountMerchant()
        {
        }
        public String AccountId { get; set; }
        public String MerchantId { get; set; }
        public String MerchantName { get; set; }
        public String TerminalId { get; set; }
    }
}
