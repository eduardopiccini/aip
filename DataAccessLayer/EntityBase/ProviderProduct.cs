﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class ProviderProduct
    {
        public ProviderProduct()
        {
        }

        public ProviderProduct(String providerProductId, String description, String providerId,
            String productCode, String currencyId, String countryId, String carrierId, String cityId,
            String productTypeId, Decimal minAmount, Decimal fixedAmount)
        {
            this.ProviderProductId = providerProductId;
            this.Description = description;
            this.ProviderId = providerId;
            this.ProductCode = productCode;
            this.CurrencyId = currencyId;
            this.CountryId = countryId;
            this.CarrierId = carrierId;
            this.CityId = cityId;
            this.ProductTypeId = productTypeId;
            this.MinAmount = minAmount;
            this.FixedAmount = fixedAmount;
        }

        public String ProviderProductId { get; set; }
        public String Description { get; set; }
        public String ProviderId { get; set; }
        public String ProductCode { get; set; }
        public String CurrencyId { get; set; }
        public String CountryId { get; set; }
        public String CarrierId { get; set; }
        public String CityId { get; set; }
        public String ProductTypeId { get; set; }
        public Decimal MinAmount { get; set; }
        public Decimal FixedAmount { get; set; }
    }


    /*
     * select provider_product_id, description, provider_id, product_code, currency_id, 
country_id, carrier_id, city_id, product_type_id, min_amount, max_amount, fixed_amount
from ip_vw_provider_product
     * 
     * */
}
