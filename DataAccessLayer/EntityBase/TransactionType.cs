﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class TransactionType
    {
        public TransactionType()
        {
        }

        public TransactionType(Int64 id, String description, String abbreviation, Int32 inVoid, Int64 statusid, 
            String statusDescription)
        {
            this.Id = id;
            this.Description = description;
            this.Abbreviation = abbreviation;
            this.InVoid = inVoid;
            this.StatusId = statusid;
            this.StatusDescription = statusDescription;
        }

        public Int64 Id { get; set; }
        public String Description { get; set; }
        public String Abbreviation { get; set; }
        public Int32 InVoid { get; set; }
        public Int64 StatusId { get; set; }
        public String StatusDescription { get; set; }
    }
}