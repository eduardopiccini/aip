﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class LowBalance
    {
        public LowBalance()
        {
        }

        public LowBalance(String accountId, String name, Decimal balance,
            Decimal creditLimit, Decimal lowBalanceAlertAmount)
        {
            this.AccountId = accountId;
            this.Name = name;
            this.Balance = balance;
            this.CreditLimit = creditLimit;
            this.LowBalanceAlertAmount = lowBalanceAlertAmount;
        }

        public String AccountId { get; set; }
        public String Name { get; set; }
        public Decimal Balance { get; set; }
        public Decimal CreditLimit { get; set; }
        public Decimal LowBalanceAlertAmount { get; set; }
    }
}
