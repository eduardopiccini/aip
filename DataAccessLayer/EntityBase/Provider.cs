﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class Provider
    {
        public Provider()
        {
        }


        public Int64 Id { set; get; }
        public String Description {set; get;}
        public String Abbreviation {set; get;}
        public Int32 VoidAllowed { set; get; }
        public Int64 StatusId { set; get; }
        public String StatusDescription {set; get;}
        public Int32 AlertFailedQuantity {get; set;}
        public Int32 QuantityFailedTransaction {get; set;}
        public Int32 AlertFailedTransaction{ get; set; }
        public String CurrencyId { set; get; }
    }
}