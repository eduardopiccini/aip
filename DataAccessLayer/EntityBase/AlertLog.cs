﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class AlertLog
    {
        public AlertLog()
        {
        }

        public AlertLog(Int64 id, Int64 alertId, 
            String alertDescription, String message,
            String dateCreated,
            String userCreated)
        {
            this.Id = id;
            this.AlertId = alertId;
            this.AlertDescription = alertDescription;
            this.Message = message;
            this.DateCreated = dateCreated;
            this.UserCreated = userCreated;
        }

        public Int64 Id { get; set; }
        public Int64 AlertId { get; set; }
        public String AlertDescription { get; set; }
        public String Message { get; set; }
        public String DateCreated { get; set; }
        public String UserCreated { get; set; }
    }
}