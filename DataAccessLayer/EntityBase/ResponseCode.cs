﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class ResponseCode
    {
        public ResponseCode()
        {
        }

        public ResponseCode(String code, String message, String description)
        {
            this.Code = code;
            this.Message = message;
            this.Description = description;
        }

        public String Code {set; get;}
        public String Message {set; get;}
        public String Description {set; get;}
    }
}