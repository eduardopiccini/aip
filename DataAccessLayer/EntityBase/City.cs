﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class City
    {
        public City()
        {
        }

        public City(Int64 id, String description, String abbreviation,
            Int64? countryId, String countryDescription, Int64? statusId, 
            String statusDescription)
        {
            this.Id = id;
            this.Description = description;
            this.Abbreviation = abbreviation;
            this.CountryId = countryId;
            this.CountryDescription = countryDescription;
            this.StatusId = statusId;
            this.StatusDescription = statusDescription;
        }

        public Int64 Id { get; set; }
        public String Description { get; set; }
        public String Abbreviation { get; set; }
        public Int64? CountryId { get; set; }
        public String CountryDescription { get; set; }
        public Int64? StatusId { get; set; }
        public String StatusDescription { get; set; }
    }
}