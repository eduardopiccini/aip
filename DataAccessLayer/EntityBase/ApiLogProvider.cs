﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class ApiLogProvider
    {
        public ApiLogProvider()
        {
        }


        public ApiLogProvider(String id, String logId, String routingId,String phone, String dateBegin, String dateEnd, String accountId, String accountName, String amount,
            String discount, String discountAmount, String surcharge, String deckProductId, String productTypeId, 
            String productId, String productName, String profileId, String providerId, String providerName, String templateId, String countryCode, String operatorCode, String productCode,
            String weight, String productAmount, String responseMessage, String responseCodeId, String reference, String serial,
            String oldBalance, String newBalance, String currencyId, String currencyRate, String currencyAmount,
            String ip, String port, String url)
        {
            this.Id = id;
            this.LogId = logId;
            this.RoutingId = routingId;
            this.Phone = phone;
            this.DateBegin = dateBegin;
            this.DateEnd = dateEnd;
            this.AccountId = accountId;
            this.AccountName = accountName;
            this.Amount = amount;
            this.Discount = discount;
            this.DiscountAmount = discountAmount;
            this.Surcharge = surcharge;
            this.DeckProductId = deckProductId;
            this.ProductTypeId = productTypeId;
            this.ProductId = productId;
            this.ProductName = productName;
            this.ProfileId = profileId;
            this.ProviderId = providerId;
            this.ProviderName = providerName;
            this.TemplateId = templateId;
            this.CountryCode = countryCode;
            this.OperatorCode = operatorCode;
            this.ProductCode = productCode;
            this.Weight = weight;
            this.ProductAmount = productAmount;
            this.ResponseMessage = responseMessage;
            this.ResponseCodeId = responseCodeId;
            this.Reference = reference;
            this.Serial =  serial;
            this.OldBalance = oldBalance;
            this.NewBalance = newBalance;
            this.CurrencyId = currencyId;
            this.CurrencyRate = currencyRate;
            this.CurrencyAmount = currencyAmount;
            this.Ip = ip;
            this.Url = url;
            this.Port = port;
        }

        public String Id { get; set; }
        public String LogId { get; set; }
        public String RoutingId { get; set; }
        public String Phone { get; set; }
        public String DateBegin { get; set; }
        public String DateEnd { get; set; }
        public String AccountId { get; set; }
        public String AccountName { get; set; }
        
        public String Amount { get; set; }
        public String Discount { get; set; }
        public String DiscountAmount { get; set; }
        public String Surcharge { get; set; }
        public String DeckProductId { get; set; }
        public String ProductTypeId { get; set; }
        public String ProductId { get; set; }
        public String ProductName { get; set; }
        public String ProfileId { get; set; }
        public String ProviderId { get; set; }
        public String ProviderName { get; set; }
        
        public String TemplateId { get; set; }
        public String CountryCode { get; set; }
        public String OperatorCode { get; set; }
        
        public String ProductCode { get; set; }
        public String Weight { get; set; }
        public String ProductAmount { get; set; }
        public String ResponseMessage { get; set; }
        public String ResponseCodeId { get; set; }
        public String Reference { get; set; }
        public String Serial { get; set; }
        public String OldBalance { get; set; }
        public String NewBalance { get; set; }
        public String CurrencyId { get; set; }
        public String CurrencyRate { get; set; }
        public String CurrencyAmount { get; set; }
        public String Ip { get; set; }
        public String Port { get; set; }
        public String Url { get; set; }

        
    }
}
