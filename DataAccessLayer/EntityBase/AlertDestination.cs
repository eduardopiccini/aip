﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class AlertDestination
    {
        public AlertDestination()
        {
        }

        public AlertDestination(Int64 id, Int64 alertId, String email, Int64? mobile)
        {
            this.Id = id;
            this.AlertId = alertId;
            this.Email = email;
            this.Mobile = mobile;
        }

        public Int64 Id { get; set; }
        public Int64 AlertId { get; set; }
        public String Email {get;set;}
        public Int64? Mobile { get; set; }
    }
}


