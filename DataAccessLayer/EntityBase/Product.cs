﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class Product
    {
        
        public Product()
        {
        }


        public Product(Int64 id, String description,String description2, String abbreviation, Int64? typeId, String typeDescription,
            String currencyId, String currencyDescription, Int64 countryId, String countryDescription,
            String carrierId, String carrierDescription, Int64? cityId, String cityDescription, 
            Int64 statusId, String statusDescription, String dateCreated, String userCreated, String dateUpdated,
            String userUpdated)
        {
            this.Id = id;
            this.Description = description;
            this.Description2 = description2;
            this.Abbreviation = abbreviation;
            this.TypeId = typeId;
            this.TypeDescription = typeDescription;
            this.CurrencyId = currencyId;
            this.CurrencyDescription = currencyDescription;
            this.CountryId = countryId;
            this.CountryDescription = countryDescription;
            this.CarrierId = carrierId;
            this.CarrierDescription = carrierDescription;
            this.CityId = cityId;
            this.CityDescription = cityDescription;
            this.StatusId = statusId;
            this.StatusDescription = statusDescription;
        }

        public Int64 Id { set; get; }
        public String Description {set; get;}
        public String Description2 { set; get; }
        public String Abbreviation {set; get;}
        public Int64? TypeId {set; get;}
        public String TypeDescription {set; get;}
        public String CurrencyId {set; get;}
        public String CurrencyDescription {set; get;}
        public Int64 CountryId { set; get; }
        public String CountryDescription {set; get;}
        public String CarrierId {set; get;}
        public String CarrierDescription {set; get;}
        public Int64? CityId { set; get; }
        public String CityDescription {set; get;}
        public Int64 StatusId { set; get; }
        public String StatusDescription {set; get;}
    }
}