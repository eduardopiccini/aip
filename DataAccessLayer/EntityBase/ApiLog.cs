﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{


    [Serializable]
    public class ApiLog
    {
        public String LogId { get; set; }
        public String DateBegin { get; set; }
        public String Amount { get; set; }
        public String Discount { get; set; }
        public String DiscountAmount { get; set; }
        public String AccountName { set; get; }
        public String TransactionTypeName { get; set; }
        public String ProductName { get; set; }
        public String ProductTypeName { get; set; }
        public String CountryName { get; set; }
        public String CarrierName { get; set; }
        public String CityName { get; set; }
        public String Phone { get; set; }
        public String Serial { get; set; }
        public String ResponseCodeId { get; set; }
        public String ResponseMessage { get; set; }
        public String ProviderName { get; set; }
        public String ProviderAmount { get; set; }
        public String ProviderDiscount { get; set; }
        public String ProviderDiscountAmount { get; set; }
        public String ProviderResponseCodeId { get; set; }
        public String ProviderResponseDescription { get; set; }
        public String Merchant { get; set; }        
        public String SecondTimeoutTransaction { get; set; }
        public String TimeoutApproved { get; set; }
        public String TimeoutApprovedReference { get; set; }
        public String TimeResponse { get; set; }
        public String ProviderReference { get; set; }
        public String Reference { get; set; }
        
    }
}