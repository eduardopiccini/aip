﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class ProviderResponseCode
    {
        public ProviderResponseCode()
        {
        }

        public ProviderResponseCode(Int64 providerId, String providerDescription, 
            String responseCode, String responseCodeDescription, String abbreviation)
        {
            this.ProviderId = providerId;
            this.ProviderDescription = providerDescription;
            this.ResponseCode = responseCode;
            this.ResponseCodeDescription = responseCodeDescription;
            this.Abbreviation = abbreviation;
        }

        public Int64 ProviderId { set; get; }
        public String ProviderDescription {set; get;}
        public String ResponseCode {set; get;}
        public String ResponseCodeDescription {set; get;}
        public String Abbreviation { set; get; }
    }
}