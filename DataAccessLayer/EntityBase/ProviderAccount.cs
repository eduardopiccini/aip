﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class ProviderAccount
    {
        public ProviderAccount()
        {
        }

        //public City(Int64 id, String description, String abbreviation,
        //    Int64 countryId, String countryDescription, Int64 statusId, 
        //    String statusDescription)
        //{
        //    this.Id = id;
        //    this.Description = description;
        //    this.Abbreviation = abbreviation;
        //    this.CountryId = countryId;
        //    this.CountryDescription = countryDescription;
        //    this.StatusId = statusId;
        //    this.StatusDescription = statusDescription;
        //}

        public Int64 ProviderProductId { get; set; }
        public String Description { get; set; }
        public Int64 ProductId{ get; set; }
        public Int64 ProviderId { get; set; }
        public String ProviderName { get; set; }
        public Int64 ProductTypeId { get; set; }
        public String ProductTypeName { get; set; }
        public String CurrencyId { get; set; }
        public String CurrencyName { get; set; }
        public Int64 CountryId { get; set; }
        public String CountryName { get; set; }
        public Int64? CarrierId { get; set; }
        public String CarrierName { get; set; }
        public Int64? CityId { get; set; }
        public String CityName { get; set; }
        public String ProductCode { get; set; }
        public Decimal MinAmount { get; set; }
        public Decimal MaxAmount { get; set; }
        public Decimal Discount { get; set; }
        public Decimal ProductCost { get; set; }
        public Boolean SendProvider { get; set; }
        public String DestinationCurrency { get; set; }
        public String DestinationCurrencyName { get; set; }
        public Decimal RechargedAmount { get; set; }
        public Int64 ProviderOperatorId { get; set; }
    }
}