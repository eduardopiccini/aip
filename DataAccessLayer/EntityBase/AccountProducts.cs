﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class AccountProducts
    {

        public AccountProducts()
        {
        }

        public AccountProducts(String accountId, String accountDescription,
            String deckProductId, String deckProductDescription, String abbreviation,
            String deckId, String deckDescription, String productTypeId,
            String productTypeDescription, String productId, String productDescription,
            String countryId, String countryDescription, String carrierId,
            String carrierDescription, String currencyId, String currencyDescription,
            String cityId, String cityDescription, String statusId,
            String statusDescription, String amount, String maxAmount, String discount)
        {
            this.AccountId = accountId;
            this.AccountDescription = accountDescription;
            this.DeckProductId = deckProductId;
            this.DeckProductDescription = deckProductDescription;
            this.Abbreviation = abbreviation;
            this.DeckId = deckId;
            this.DeckDescription = deckDescription;
            this.ProductTypeId = productTypeId;
            this.ProductTypeDescription = productTypeDescription;
            this.ProductId = productId;
            this.ProductDescription = ProductDescription;
            this.CountryId = countryId;
            this.CountryDescription = countryDescription;
            this.CarrierId = carrierId;
            this.CarrierDescription = carrierDescription;
            this.CurrencyId = currencyId;
            this.CurrencyDescription = currencyDescription;
            this.CityId = cityId;
            this.CityDescription = cityDescription;
            this.StatusId = statusId;
            this.StatusDescription = statusDescription;
            this.Amount = amount;
            this.MaxAmount = maxAmount;
            this.Discount = discount;
        }

        public String AccountId { get; set; }
        public String AccountDescription {get; set;}
        public String DeckProductId { get; set; }
        public String DeckProductDescription {get;set;}
        public String Abbreviation {get; set;}
        public String DeckId { get; set; }
        public String DeckDescription {get;set;}
        public String ProductTypeId { get; set; }
        public String ProductTypeDescription {get; set;}
        public String ProductId { get; set; }
        public String ProductDescription {get; set;}
        public String CountryId { get; set; }
        public String CountryDescription {get; set;}
        public String CarrierId {get; set;}
        public String CarrierDescription {get; set;}
        public String CurrencyId {get; set;}
        public String CurrencyDescription {get; set;}
        public String CityId { get; set; }
        public String CityDescription {get; set;}
        public String StatusId { get; set; }
        public String StatusDescription {get; set;}
        public String Amount { get; set; }
        public String MaxAmount { get; set; }
        public String Discount { get; set; }
    }
}
