﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class TransactionLog
    {
        public TransactionLog()
        {
        }


       

        public String Id { set; get; }
        public String LogId { set; get; }
        public String DateBegin { set; get; }
        public String DateEnd { set; get; }
        public String AccountId { set; get; }
        public String AccountName { set; get; }
        public String TransactionTypeId { set; get; }
        public String TransactionTypeName { set; get; }
        public String ProductTypeId { set; get; }
        public String ProductTypeName { set; get; }
        public String DeckId { set; get; }
        public String DeckName { set; get; }
        public String DeckProductId { set; get; }
        public String DeckProductName { set; get; }
        public String TransactionAmount { set; get; }
        public String TransactionDiscount { set; get; }
        public String TransactionDiscountAmount { set; get; }
        public String TransactionNetAmount { set; get; }
        public String Surcharge { set; get; }
        public String SurchargeAmount { set; get; }
        public String ProductId { set; get; }
        public String ProductName { set; get; }
        public String ProductAmount { set; get; }
        public String VoidAllowed { set; get; }
        public String Reference { set; get; }
        public String CountryId { set; get; }
        public String CountryName { set; get; }
        public String CurrencyId { set; get; }
        public String CurrencyName { set; get; }
        public String CarrierId { set; get; }
        public String CarrierName { set; get; }
        public String CityId { set; get; }
        public String CityName { set; get; }
        public String Phone { set; get; }
        public String OldBalance { set; get; }
        public String NewBalance { set; get; }
        public String ResponseCodeId { set; get; }
        public String ResponseMessage { set; get; }
        public String ProviderAccountId { set; get; }
        public String ProviderAccountName { set; get; }
        public String ProviderProfileId { set; get; }
        public String ProviderId { set; get; }
        public String ProviderName { set; get; }
        public String ProviderTemplateId { set; get; }
        public String ProviderDeckProductId { set; get; }
        public String ProviderDeckProductName { set; get; }
        public String ProviderProductCode { set; get; }
        public String ProviderCountryCode { set; get; }
        public String ProviderOperatorCode { set; get; }
        public String ProviderReference { set; get; }
        public String ProviderSerial { set; get; }
        public String ProviderAmount { set; get; }
        public String ProviderDiscount { set; get; }
        public String ProviderDiscountAmount { set; get; }
        public String ProviderNetAmount { set; get; }
        public String ProviderOldBalance { set; get; }
        public String ProviderNewBalance { set; get; }
        public String ProviderResponseCodeId { set; get; }
        public String ProviderResponseDescription { set; get; }
        public String ProviderCurrencyId { get; set; }
        public String ProviderCurrencyRate { get; set; }
        public String ProviderCurrencyAmount { get; set; }
        public String InvoiceId { get; set; }
        public String ProviderInvoiceId { get; set; }
        public String VoidedTransactionId { get; set; }
        public String Ip { get; set; }
        public String Profit { get; set; }
        public String ProfitAmount { get; set; }
        public String UserCreated { set; get; }
        public String DateCreated { set; get; }
        public String UserUpdated { set; get; }
        public String DateUpdated { set; get; }
        public String Merchant { get; set; }
        public String AuthorizationNumber { get; set; }

    }
}