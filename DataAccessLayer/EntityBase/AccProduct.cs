﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class AccProduct
    {
        public AccProduct()
        {
        }

        public AccProduct(String id, String description, String amount, String maxAmount, String typeId,
            String carrierId, String countryId, String cityId, String currencyId)
        {
            this.Id = id;
            this.Description = description;
            this.Amount = amount;
            this.MaxAmount = maxAmount;
            this.TypeId = typeId;
            this.CarrierId = carrierId;
            this.CountryId = countryId;
            this.CityId = cityId;
            this.CurrencyId = currencyId;
        }

        public String Id { get; set; }
        public String Description { get; set; }
        public String Amount { get; set; }
        public String MaxAmount { get; set; }
        public String TypeId { get; set; }
        public String CarrierId { get; set; }
        public String CountryId { get; set; }
        public String CityId { get; set; }
        public String CurrencyId { get; set; }
    }
}
