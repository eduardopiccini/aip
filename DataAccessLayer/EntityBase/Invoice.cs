﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class Invoice
    {
        public Invoice()
        {
        }


        public Invoice(String id, String accountId,String accountName, String address, String fax,
            String zipCode, String telephone, DateTime? invoiceDate, String billingTermId,
            String billingTermName, String currencyId, String currencyName, String directionId, String directionName,
            Decimal? amount, Decimal? discountAmount, Decimal? surcharge, Decimal? balance, Decimal? previousBalance,
            DateTime? billingBeginDate, DateTime? billingEndDate,
            String invoiceStatusId, String statusName, DateTime? datePaid, String appUserPaid, String notePaid)
        {
            this.Id = id;
            this.AccountId = accountId;
            this.AccountName = accountName;
            this.Address = address;
            this.InvoiceDate = invoiceDate;
            this.BillingTermId = billingTermId;
            this.BillingTermName = billingTermName;
            this.CurrencyId = currencyId;
            this.CurrencyName = currencyName;
            this.DirectionId = directionId;
            this.DirectionName = directionName;
            this.Amount = amount;
            this.DiscountAmount = discountAmount;
            this.Surcharge = surcharge;
            this.BillingBeginDate = billingBeginDate;
            this.BillingEndDate = billingEndDate;
            this.InvoiceStatusId = invoiceStatusId;
            this.StatusName = statusName;
            this.DatePaid = datePaid;
            this.AppUserPaid = appUserPaid;
            this.NotePaid = notePaid;
            this.Fax = fax;
            this.Telephone = telephone;
            this.ZipCode = zipCode;
            this.Balance = balance;
            this.PreviousBalance = previousBalance;
        }


        public String Id { get; set; }
        public String AccountId { get; set; }
        public String AccountName { get; set; }
        public String Address { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public String BillingTermId { get; set; }
        public String BillingTermName { get; set; }
        public String CurrencyId { get; set; }
        public String CurrencyName { get; set; }
        public String DirectionId { get; set; }
        public String DirectionName { get; set; }
        public Decimal? Amount { get; set; }
        public Decimal? DiscountAmount { get; set; }
        public Decimal? Surcharge { get; set; }
        public DateTime? BillingBeginDate { get; set; }
        public DateTime? BillingEndDate { get; set; }
        public String InvoiceStatusId { get; set; }
        public String StatusName { get; set; }
        public DateTime? DatePaid { get; set; }
        public String AppUserPaid { get; set; }
        public String NotePaid { get; set; }
        
        public String Fax { get; set; }
        public String ZipCode { get; set; }
        public String Telephone { get; set; }
        public Decimal? Balance { get; set; }
        public Decimal? PreviousBalance { get; set; }
    }
}
