﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class CountryAreaCode
    {

        public CountryAreaCode()
        {
        }

        public CountryAreaCode(Int64 areaCode, Int64 countryId, 
            String countryDescription)
        {
            this.AreaCode = areaCode;
            this.CountryId = countryId;
            this.CountryDescription = countryDescription;
        }

        public Int64 AreaCode { get; set; }
        public Int64 CountryId { get; set; }
        public String CountryDescription { get; set; }
    }
}
