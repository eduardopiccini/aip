﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class ProductUpload
    {
        public ProductUpload()
        {
        }

        public ProductUpload(String id, String date, String productCode, String beginEfective, 
            String endEfective, String description, String productTypeId, 
            String productTypeDescription, String currencyId, String currencyDescription, 
            String countryId, String countryDescription,
            String carrierId, String carrierDescription, String cityId, String cityDescription, 
            String fixAmount, String minAmount, String maxAmount, 
            String cost, String fileName, String dateCreated, String userCreated, 
            String dateUpdated, String userUpdated)
        {
            this.Id = id;
            this.Date = date;
            this.ProductCode = productCode;
            this.BeginEfective = beginEfective;
            this.EndEfective = endEfective;
            this.Description = description;
            this.ProductTypeId = productTypeId;
            this.ProductTypeDescription = productTypeDescription;
            this.CurrencyId = currencyId;
            this.CurrencyDescription = currencyDescription;
            this.CountryId = countryId;
            this.CountryDescription = countryDescription;
            this.CarrierId = carrierId;
            this.CarrierDescription = carrierDescription;
            this.CityId = cityId;
            this.CityDescription = cityDescription;
            this.FixAmount = fixAmount;
            this.MinAmount = minAmount;
            this.MaxAmount = maxAmount;
            this.Cost = cost;
            this.FileName = fileName;
            this.DateCreated = dateCreated;
            this.UserCreated = userCreated;
            this.DateUpdated = dateUpdated;
            this.UserUpdated = userUpdated;
        }

        public String Id {set; get;}
        public String Date {set; get;}
        public String ProductCode {set; get;}
        public String BeginEfective {set; get;}
        public String EndEfective {set; get;}
        public String Description {set; get;} 
        public String ProductTypeId {set; get;}
        public String ProductTypeDescription {set; get;}
        public String CurrencyId {set; get;}
        public String CurrencyDescription {set; get;}
        public String CountryId {set; get;}
        public String CountryDescription {set; get;}
        public String CarrierId {set; get;}
        public String CarrierDescription {set; get;}
        public String CityId {set; get;}
        public String CityDescription {set; get;}
        public String FixAmount {set; get;}
        public String MinAmount {set; get;}
        public String MaxAmount {set; get;}
        public String Cost {set; get;}
        public String FileName {set; get;}
        public String DateCreated {set; get;}
        public String UserCreated { set; get; }
        public String DateUpdated {set; get;}
        public String UserUpdated {set; get;}
    }
}