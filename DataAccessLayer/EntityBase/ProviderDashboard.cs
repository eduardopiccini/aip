﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    public class ProviderDashboard
    {

       

        public String ProviderId {get;set;}
        public String Description {get;set;}
        public Int32 TotalTransa {get;set;}
        public Int32 ValidTransa {get;set;}
        public Int32 InvalidTransa {get;set;}
        public Decimal GrossAmount {get;set;}
        public Decimal NetAmount {get;set;}
        public Int32 SurchargeQuantity {get;set;}
        public Decimal Surcharge {get;set;}
        public Decimal GrossAmountCurrency { get; set; }
        public Decimal NetAmountCurrency { get; set; }
        public Decimal SurchargeCurrency { get; set; }
    }
}
