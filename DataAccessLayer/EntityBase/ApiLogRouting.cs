﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class ApiLogRouting
    {
        public ApiLogRouting()
        {
        }

        public ApiLogRouting(String routingId, String logId, String routingMode, String providerId, String providerName,
                             String providerAccountId, String providerAccountName, String productId, String productName,
                             String templateId, String vendorGroupId, String amount, String currencyId, String currencyRate,
                             String currencyAmount, String phone, String ip, String port, String url, String dateCreated,
                             String appUserCreated, String appUserUpdated)
        {
            RoutingId = routingId;
            LogId = logId;
            RoutingMode = routingMode;
            ProviderId = providerId;
            ProviderName = providerName;
            ProviderAccountId = providerAccountId;
            ProviderAccountName = providerAccountName;
            ProductId = productId;
            ProductName = productName;
            TemplateId = templateId;
            VendorGroupId = vendorGroupId;
            Amount = amount;
            CurrencyId = currencyId;
            CurrencyRate = currencyRate;
            CurrencyAmount = currencyAmount;
            Phone = phone;
            Ip = ip;
            Port = port;
            Url = url;
            DateCreated = dateCreated;
            AppUserCreated = appUserCreated;
            AppUserUpdated = appUserUpdated;

        }
        public String RoutingId { get; set; }
        public String LogId { get; set; }
        public String RoutingMode { get; set; }
        public String ProviderId { get; set; }
        public String ProviderName { get; set; }
        public String ProviderAccountId { get; set; }
        public String ProviderAccountName { get; set; }
        public String ProductId { get; set; }
        public String ProductName { get; set; }
        public String TemplateId { get; set; }
        public String VendorGroupId { get; set; }
        public String Amount { get; set; }
        public String CurrencyId { get; set; }
        public String CurrencyRate { get; set; }
        public String CurrencyAmount { get; set; }
        public String Phone { get; set; }
        public String Ip { get; set; }
        public String Port { get; set; }
        public String Url { get; set; }
        public String DateCreated { get; set; }
        public String AppUserCreated { get; set; }
        public String AppUserUpdated { get; set; }

    }
}
