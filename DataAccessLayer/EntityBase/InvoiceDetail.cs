﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class InvoiceDetail
    {
        

        public InvoiceDetail()
        {
        }

        public InvoiceDetail(DateTime? transactionDate, String productId, String productName,
            Decimal? quantity, Decimal? transactionAmount, Decimal? grossAmount, Decimal? discountAmount,
            Decimal? netAmount, Decimal? surcharge)
        {
            this.TransactionDate = transactionDate;
            this.ProductId = productId;
            this.ProductName = productName;
            this.Quantity = quantity;
            this.TransactionAmount = transactionAmount;
            this.GrossAmount = grossAmount;
            this.DiscountAmount = discountAmount;
            this.NetAmount = netAmount;
            this.Surcharge = surcharge;
        }

        public DateTime? TransactionDate { get; set; }
        public String ProductId { get; set; }
        public String ProductName { get; set; }
        public Decimal? Quantity { get; set; }
        public Decimal? TransactionAmount { get; set; }
        public Decimal? GrossAmount { get; set; }
        public Decimal? DiscountAmount { get; set; }
        public Decimal? NetAmount { get; set; }
        public Decimal? Surcharge { get; set; }
    }
}
