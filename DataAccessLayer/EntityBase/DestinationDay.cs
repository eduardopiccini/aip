﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class DestinationDay
    {
        public String CountryId {get;set;}
        public String CarrierId { get; set; }
        public String Name { get; set; }
        public Int32 Quantity { get; set; }
        public Decimal GrossAmount { get; set; }
        public Decimal NetAmount { get; set; }
        public Decimal Surcharge { get; set; }
        public Decimal GrossAmountCurrency { get; set; }
        public Decimal NetAmountCurrency { get; set; }
        public Decimal SurchargeCurrency { get; set; }
       
    }
}
