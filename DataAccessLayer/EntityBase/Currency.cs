﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class Currency
    {
        public Currency()
        {
        }

        public Currency(String id, String description, String abbreviation, 
            Decimal rate, Int64 statusId, 
            String statusDescription)
        {
            this.Id = id;
            this.Description = description;
            this.Abbreviation = abbreviation;
            this.Rate = rate;
            this.StatusId = statusId;
            this.StatusDescription = statusDescription;
        }

        public String Id { get; set; }
        public String Description { get; set; }
        public String Abbreviation { get; set; }
        public Decimal Rate { get; set; }
        public Int64 StatusId { get; set; }
        public String StatusDescription { get; set; }
    }
}