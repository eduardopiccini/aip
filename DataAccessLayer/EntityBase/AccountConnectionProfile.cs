﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class AccountConnectionProfile
    {
        public AccountConnectionProfile()
        {
        }


        public AccountConnectionProfile(Int64 id, Int64 profileId, String ip, 
            String port, String url, String weight)
        {
            this.Id = id;
            this.ProfileId = profileId;
            this.Ip = ip;
            this.Port = port;
            this.Url = url;
            this.Weight = weight;
        }


        public Int64 Id { get; set; }
        public Int64 ProfileId { get; set; }
        public String Ip { get; set; }
        public String Port { get; set; }
        public String Url { get; set; }
        public String Weight { get; set; }
    }
}