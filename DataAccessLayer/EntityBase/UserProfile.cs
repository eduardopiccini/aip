﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class UserProfile
    {
        public UserProfile()
        {
        }

        public UserProfile(String name, String lastName, String userId, String optionId,
            String optionDescription, String viewAllowed, String updateAllowed,
            String userCreated, String dateCreated, String userUpdated, String dateUpdated)
        {
            this.Name = name;
            this.LastName = lastName;
            this.UserId = userId;
            this.OptionId = optionId;
            this.OptionDescription = optionDescription;
            this.ViewAllowed = viewAllowed;
            this.UpdateAllowed = updateAllowed;
            this.UserCreated = userCreated;
            this.DateCreated = dateCreated;
            this.UserUpdated = userUpdated;
            this.DateUpdated = dateUpdated;
        }

        public String Name {get; set;}
        public String LastName {get; set;}
        public String UserId {get; set;}
        public String OptionId {get; set;}
        public String OptionDescription {get; set;}
        public String ViewAllowed {get; set;}
        public String UpdateAllowed {get; set;}
        public String UserCreated {get; set;}
        public String DateCreated {get; set;}
        public String UserUpdated {get; set;}
        public String DateUpdated { get; set; }
    }
}
