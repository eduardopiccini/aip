﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class AccountProfile
    {

        public AccountProfile()
        {
        }

        public Int64 ProfileId { get; set; }
        public Int64 DirectionId { get; set; }
        public Int64 AccountId { get; set; }
        public String ApiUser { get; set; }
        public String StatusId { get; set; }
        public String StatusDescription { get; set; }
        public String FailedAttemps { get; set; }
        public String DateBlocked { get; set; }
        public String RequiredIp { get; set; }
        public String RequiredMerchant { get; set; }
    }
}
