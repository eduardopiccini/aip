﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class ProductType
    {
        public ProductType()
        {
        }

        public ProductType(Int64 id, String description, String abbreviation, Int64 status_id, 
            String statusDescription)
        {
            this.Id = id;
            this.Description = description;
            this.Abbreviation = abbreviation;
            this.StatusId = status_id;
            this.StatusDescription = statusDescription;
        }

        public Int64 Id { set; get; }
        public String Description {set; get;}
        public String Abbreviation {set; get;}
        public Int64 StatusId { set; get; } 
        public String StatusDescription {set; get;}
    }
}