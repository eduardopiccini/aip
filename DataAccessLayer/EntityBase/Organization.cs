﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class Organization
    {

        public Organization()
        {
        }

        public Organization(String accountId, String name, Decimal balance, Decimal creditLimit,
            Int32 transaQuantity, Decimal grossAmount, Decimal netAmount, Int32 surchargeQuantity,
            Decimal surcharge)
        {
            this.AccountId = accountId;
            this.Name = name;
            this.Balance = balance;
            this.CreditLimit = creditLimit;
            this.TransaQuantity = transaQuantity;
            this.GrossAmount = grossAmount;
            this.NetAmount = netAmount;
            this.SurchargeQuantity = surchargeQuantity;
            this.Surcharge = surcharge;
        }

        public String AccountId { get; set; }
        public String Name { get; set; }
        public Decimal Balance { get; set; }
        public Decimal CreditLimit { get; set; }
        public Int32 TransaQuantity { get; set; }
        public Decimal GrossAmount { get; set; }
        public Decimal NetAmount { get; set; }
        public Int32 SurchargeQuantity { get; set; }
        public Decimal Surcharge { get; set; }
    }
}
