﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class AccountStatus
    {
        public AccountStatus()
        {
        }

        public AccountStatus(Int64 id, String description, String abbreviation)
        {
            this.Id = id;
            this.Description = description;
            this.Abbreviation = abbreviation;
        }

        public Int64 Id { get; set; }
        public String Description { get; set; }
        public String Abbreviation { get; set; }
    }
}