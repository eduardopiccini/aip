﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class Deck
    {
        public Deck()
        {
        }

        public Deck(Int64 id, Int64 accountId, String name, Int64 directionId,
            String directionDescription, String description, String abbreviation, Int64 statusId, 
            String statusDescription)
        {
            this.Id = id;
            this.Description = description;
            this.Abbreviation = abbreviation;
            this.AccountId = accountId;
            this.Name = name;
            this.DirectionId = directionId;
            this.DirectionDescription = directionDescription;
            this.StatusId = statusId;
            this.StatusDescription = statusDescription;
            
        }

        public Int64 Id { get; set; }
        public String Description {get; set;}
        public String Abbreviation {get; set;}
        public Int64 AccountId { get; set; }
        public String Name {get; set;}
        public Int64 DirectionId { get; set; }
        public String DirectionDescription {get; set;}
        public Int64 StatusId { get; set; }
        public String StatusDescription {get; set;}
     
    }
}