﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class DeckTemplate
    {
        public DeckTemplate()
        {
        }

        public DeckTemplate(Int64 id, Int64 deckProductId, String providerAccountId,
            String providerAccountName, Int64 weight, String productCode, Decimal realAmount, Decimal maxAmount,
            Decimal discount, Decimal surcharge,  Decimal providerBalance, Decimal providerCreditLimit,
            String paymentType)
        {
            this.Id = id;
            this.DeckProductId = deckProductId;
            this.ProviderAccountId = providerAccountId;
            this.ProviderAccountName = providerAccountName;
            this.Weight = weight;
            this.ProductCode = productCode;
            this.RealAmount = realAmount;
            this.Discount = discount;
            this.ProviderBalance = providerBalance;
            this.ProviderCreditLimit = providerCreditLimit;
            this.PaymentType = paymentType;
            this.MaxAmount = maxAmount;
            this.Surcharge = surcharge;
        }

        public Int64 Id { get; set; }
        public Int64 DeckProductId { get; set; }
        public String ProviderAccountId {get; set;}
        public String ProviderAccountName {get; set;}
        public Int64 Weight { get; set; }
        public String ProductCode { get; set; }
        public Decimal RealAmount { get; set; }
        public Decimal MaxAmount { get; set; }
        public Decimal Discount { get; set; }
        public Decimal Surcharge { get; set; }
        public Decimal ProviderBalance { get; set; }
        public Decimal ProviderCreditLimit { get; set; }
        public String PaymentType { get; set; }
    }
}