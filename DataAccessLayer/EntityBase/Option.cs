﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class Option
    {
        public Option()
        {
        }

        public Option(String id, String description, String abbreviation, Int64 status_id, 
            String statusDescription)
        {
            this.Id = id;
            this.Description = description;
            this.Abbreviation = abbreviation;
            this.StatusId = status_id;
            this.StatusDescription = statusDescription;
        }

        public String Id { get; set; }
        public String Description {get; set;}
        public String Abbreviation {get; set;}
        public Int64 StatusId { get; set; }
        public String StatusDescription {get; set;}
    }
}