﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class DeckProductAccout
    {
        public DeckProductAccout()
        {
        }

        public Int64 AccountId { get; set; }
        public String AccountName { get; set; }
        public Int64 DirectionId { get; set; }
        public String DirectionName { get; set; }
        public Int64 DeckProductId { get; set; }
        public String Description { get; set; }
        public Int64 ProductTypeId { get; set; }
        public String ProductTypeName { get; set; }
        public Int64 ProductId { get; set; }
        public String ProductName { get; set; }
        public String CurrencyId { get; set; }
        public String CurrencyName { get; set; }
        public Int64 CountryId { get; set; }
        public String CountryName { get; set; }
        public Int64 CarrierId { get; set; }
        public String CarrierName { get; set; }
        public Int64 CityId { get; set; }
        public String CityName { get; set; }
        public Int64 StatusId { get; set; }
        public String StatusName { get; set; }
        public Decimal Amount { get; set; }
        public Decimal MaxAmount { get; set; }
        public Decimal Discount { get; set; }
        public Decimal DiscountAmount { get; set; }
        public Decimal Surcharge { get; set; }
        public Decimal Margin { get; set; }
        public Decimal MarginAmount { get; set; }
        public Int64 RoutingMode { get; set; }
        public String RoutingModeName { get; set; }
        
    }
}