﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{

    [Serializable]
    public class AccCountry
    {
        public AccCountry()
        {
        }

        public AccCountry(Int64 id, String description)
        {
            this.Id = id;
            this.Description = description;
        }


        public Int64 Id { get; set; }
        public String Description { get; set; }
        public String Iso2 { get; set; }
        public String Iso3 { get; set; }
        public String CurrencyId { get; set; }
        public String CurrencyDescription { get; set; }
        public String CountryCode { get; set; }
    }

    [Serializable]
    public class Country
    {
        public Country()
        {
        }

        public Country(Int64 id, String description,
            String iso2, String iso3, String currencyId, 
            String currencyDescription, String countryCode)
        {
            this.Id = id;
            this.Description = description;
            this.Iso2 = iso2;
            this.Iso3 = iso3;
            this.CurrencyId = currencyId;
            this.CurrencyDescription = currencyDescription;
            this.CountryCode = countryCode;
        }


        public Int64 Id { get; set; }
        public String Description {get; set;}
        public String Iso2 {get; set;}
        public String Iso3 {get; set;}
        public String CurrencyId {get; set;}
        public String CurrencyDescription {get; set;}
        public String CountryCode { get; set; }
    }
}