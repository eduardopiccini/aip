﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class BankTransactionType
    {
        public BankTransactionType()
        {
        }
        public Int64 Id { get; set; }
        public String Description { get; set; }
        public Int64 StatusId { get; set; }
        public String StatusDescription { get; set; }
    }
}