﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class CarrierProvider
    {
        public CarrierProvider()
        {
        }

        public CarrierProvider(Int64 id, String description, String carrierId, 
            String carrierDescription, 
            String providerId, String providerDescription)
        {
            this.Id = id;
            this.Description = description;
            this.CarrierId = carrierId;
            this.CarrierDescription = carrierDescription;
            this.ProviderId = providerId;
            this.ProviderDescription = providerDescription;
        }

        public Int64 Id { get; set; }
        public String Description { get; set; }
        public String CarrierId { get; set; }
        public String CarrierDescription { get; set; }
        public String ProviderId { get; set; }
        public String ProviderDescription { get; set; }
    }
}