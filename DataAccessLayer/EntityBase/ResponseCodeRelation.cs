﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class ResponseCodeRelation
    {
        public ResponseCodeRelation()
        {
        }

        public ResponseCodeRelation(String responseCode, String responseMessage, Int64 providerId, String providerDescription,
            String providerResponseCodeId, String providerResponseDescription, Int64 weigth)
        {
            this.ResponseCode = responseCode;
            this.ResponseMessage = responseMessage;
            this.ProviderId = providerId;
            this.ProviderResponseCodeId = providerResponseCodeId;
            this.ProviderResponseDescription = providerResponseDescription;
            this.Weight = weigth;
            this.ProviderDescription = providerDescription;
        }

        public String ResponseCode { get; set; }
        public String ResponseMessage { get; set; }
        public Int64 ProviderId { get; set; }
        public String  ProviderDescription { get; set; }
        public String ProviderResponseCodeId { get; set; }
        public String ProviderResponseDescription { get; set; }
        public Int64 Weight { get; set; }
    }
}