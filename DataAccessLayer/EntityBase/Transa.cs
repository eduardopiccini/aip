﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    public class Transa
    {
      
        public Int64? TotalTransa { get; set; }
        public Int64? ValidTransa { get; set; }
        public Int64? InvalidTransa { get; set; }
        public Decimal? GrossAmount { get; set; }
        public Decimal? NetAmount { get; set; }
        public Int64? SurchargeQuantity { get; set; }
        public Decimal? Surcharge { get; set; }
        public Decimal? GrossAmountCurrency { get; set; }
        public Decimal? NetAmountCurrency { get; set; }
        public Decimal? SurchargeCurrency { get; set; }
    }
}
