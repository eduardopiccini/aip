﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class User
    {
        public User()
        {
        }

        public User(String id, String statusId, String statusDescription, String name, String lastName,
            String email, String telephone, String mobile, String portal)
        {
            this.Id = id;
            this.StatusId = statusId;
            this.StatusDescription = statusDescription;
            this.Name = name;
            this.LastName = lastName;
            this.Email = email;
            this.Telephone = telephone;
            this.Mobile = mobile;
            this.Portal = portal;
        }


        public String Id { get; set; }
        public String StatusId { get; set; }
        public String StatusDescription { get; set; }
        public String Name { get; set; }
        public String LastName { get; set; }
        public String Email { get; set; }
        public String Telephone { get; set; }
        public String Mobile { get; set; }
        public String Portal { get; set; }
    }
}
