﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{

    [Serializable]
    public class CurrencyRate
    {
        public CurrencyRate()
        {
        }

        public CurrencyRate(String currencyId, String currencyName, String outCurrencyId, String outCurrencyName,
            String outRate)
        {
            this.CurrencyId = currencyId;
            this.CurrencyName = currencyName;
            this.OutCurrencyId = outCurrencyId;
            this.OutCurrencyName = outCurrencyName;
            this.OutRate = outRate;
        }

        public String CurrencyId { get; set; }
        public String CurrencyName { get; set; }
        public String OutCurrencyId { get; set; }
        public String OutCurrencyName { get; set; }
        public String OutRate { get; set; }

    }
}
