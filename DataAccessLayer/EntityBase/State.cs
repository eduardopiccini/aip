﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class State
    {
        public State()
        {
        }

        public State(String id, String description, String abbreviation,
            Int64 countryId, String countryDescription)
        {
            this.Id = id;
            this.Description = description;
            this.Abbreviation = abbreviation;
            this.CountryId = countryId;
            this.CountryDescription = countryDescription;
        }

        public String Id {set; get;}
        public String Description {set; get;}
        public String Abbreviation {set; get;}
        public Int64 CountryId { set; get; }
        public String CountryDescription {set; get;}
    }
}