﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class TopupDenomination
    {
        public TopupDenomination()
        {
        }

        public TopupDenomination(String denominationId, String providerId, String providerDescription,
            Decimal minAmount, Decimal maxAmount, Int32 multiple)
        {
            this.DenominationId = denominationId;
            this.ProviderId = providerId;
            this.ProviderDescription = providerDescription;
            this.MinAmount = minAmount;
            this.MaxAmount = maxAmount;
            this.Multiple = multiple;
        }

        public String DenominationId { get; set; }
        public String ProviderId { get; set; }
        public String ProviderDescription { get; set; }
        public Decimal MinAmount { get; set; }
        public Decimal MaxAmount { get; set; }
        public Int32 Multiple { get; set; }
    }
}
