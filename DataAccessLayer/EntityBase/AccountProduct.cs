﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class AccountOrganization
    {
        public AccountOrganization()
        {
        }
        public Int64? Id { get; set; }
        public Int64? Surcharge { get; set; }
        public String RoutingMode { get; set; }
        public Int64? Discount { get; set; }
        public Int64? AccountId { get; set; }
        public String AccountName { get; set; }
        public Decimal? AccountBalance{ get; set; }

        
    }

    [Serializable]
    public class AccountProvider
    {
        public AccountProvider()
        {
        }
        public Int64? Id { get; set; }
        public Int64? TransactionQuantity { get; set; }
        public Int64? TransactionValid { get; set; }
        public Int64? TransactionInvalid { get; set; }
        public Decimal? TransactionAmount{ get; set; }
        public Int64? Surcharge { get; set; }
        public String ProviderName { get; set; }
        public Int64? ProviderId { get; set; }
        public String ProductCode { get; set; }
        public Int64? Discount { get; set; }
        public Decimal? AccountBalance{ get; set; }

    }

   
    [Serializable]
    public class AccountProduct 
    {
        public AccountProduct()
        {
        }
        public Int64? TestSecId { get; set; }
        public Int64? TestLogId { get; set; }
        public Int64? ProductId { get; set; }
        public String ProductName { get; set; }
        public Decimal? Amount{ get; set; }
        public Decimal? MaxAmount{ get; set; }
        

    }


}