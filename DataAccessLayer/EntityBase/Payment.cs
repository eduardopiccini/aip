﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class Payment
    {
        public Int64? TransactionId { get; set; }
        public String BalanceLogId { get; set; }
        public DateTime Date { get; set; }
        public String AccountId { get; set; }
        public String AccountName { get; set; }
        public String DirectionId { get; set; }
        public String DirectionName { get; set; }
        public String Debit { get; set; }
        public String DebitName { get; set; }
        public Decimal? TransactionAmount { get; set; }
        public Decimal? Balance { get; set; }
        public String Note { get; set; }
        public String SwiftCode { get; set; }
        public Int64? AbaNumber { get; set; }
        public String BankTransactionTypeName { get; set; }
        public String User { get; set; }

    }
}
