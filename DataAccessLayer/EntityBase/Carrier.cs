﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class Carrier
    {
        public Carrier()
        {
        }

        public Carrier(String id, String description, String abbreviation, Int64 statusid, 
            String statusDescription)
        {
            this.Id = id;
            this.Description = description;
            this.Abbreviation = abbreviation;
            this.StatusId = statusid;
            this.StatusDescription = statusDescription;
        }

        public String Id { get; set; }
        public String Description { get; set; }
        public String Abbreviation { get; set; }
        public Int64 StatusId { get; set; }
        public String StatusDescription { get; set; }
    }
}