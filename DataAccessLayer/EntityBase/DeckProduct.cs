﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class DeckProduct
    {
        public DeckProduct()
        {
        }

        public DeckProduct(Int64 id, String accountId, String description, String abbreviation, Int64 deckId,
            String deckDescription, Int64? productTypeId, String productTypeDescription, Int64 productId, String routingMode,
            String surcharge,
            String productDescription, Int64 countryId, String countryDescription, String carrierId,
            String carrierDescription, String currencyId, String currencyDescription, Decimal amount, Decimal maxAmount,
            Decimal discount, String  cityId, String cityDescription,
            Int64 statusId, String statusDescription,String sendProvider)
        {
            this.Id = id;
            this.AccountId = accountId;
            this.Description = description;
            this.Abbreviation = abbreviation;
            this.DeckId = deckId;
            this.DeckDescription = deckDescription;
            this.ProductTypeId = productTypeId;
            this.ProductTypeDescription = productTypeDescription;
            this.ProductId = productId;
            this.ProductDescription = productDescription;
            this.CountryId = countryId;
            this.CountryDescription = countryDescription;

            this.CityId = cityId;
            this.CityDescription = cityDescription;

            this.CarrierId = carrierId;
            this.CarrierDescription = carrierDescription;
            this.CurrencyId = currencyId;
            this.Amount = amount;
            this.MaxAmount = maxAmount;
            this.Discount = discount;
            this.StatusId = statusId;
            this.StatusDescription = statusDescription;
            this.SendProvider = sendProvider;
            this.RoutingMode = routingMode;
            this.Surcharge = surcharge;
        }

        public Int64 Id { get; set; }
        public String AccountId { get; set; }
        public String Description { get; set; }
        public String Abbreviation { get; set; }
        public Int64 DeckId { get; set; }
        public String DeckDescription { get; set; }
        public Int64? ProductTypeId { get; set; }
        public String ProductTypeDescription{ get; set; }
        public Int64 ProductId { get; set; }
        public String ProductDescription { get; set; }
        public Int64 CountryId { get; set; }
        public String CountryDescription { get; set; }

        public String CityId { get; set; }
        public String CityDescription { get; set; }
        public String CarrierId { get; set; }
        public String CarrierDescription { get; set; }
        public String CurrencyId { get; set; }
        public String CurrencyDescription { get; set; }
        public Decimal Amount { get; set; }
        public Decimal MaxAmount { get; set; }
        public Decimal Discount { get; set; }
        public Int64 StatusId { get; set; }
        public String StatusDescription { get; set; }
        public String SendProvider { get; set; }
        public String RoutingMode { get; set; }
        public String Surcharge { get; set; }
    }
}