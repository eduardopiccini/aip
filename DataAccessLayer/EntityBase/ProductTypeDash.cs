﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class ProductTypeDash
    {
        public String ProductTypeId { get; set; }
        public String Description { get; set; }
        public Int32 Transa { get; set; }
        public Decimal GrossAmount { get; set; }
        public Decimal NetAmount { get; set; }
        public Decimal GrossAmountCurrency { get; set; }
        public Decimal NetAmountCurrency { get; set; }
    }
}
