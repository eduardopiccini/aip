﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class UserOption
    {
        public UserOption()
        {
        }

        public UserOption(String userId, String name, String lastName, String optionId, String optionDescription,
            String viewAllowed, String updateAllowed, String statusOption)
        {
            this.UserId = userId;
            this.Name = name;
            this.LastName = lastName;
            this.OptionId = optionId;
            this.OptionDescription = optionDescription;
            this.ViewAllowed = viewAllowed;
            this.UpdateAllowed = updateAllowed;
            this.StatusOption = statusOption;
        }

        public String UserId { get; set; }
        public String Name { get; set; }
        public String LastName { get; set; }
        public String OptionId { get; set; }
        public String OptionDescription { get; set; }
        public String ViewAllowed { get; set; }
        public String UpdateAllowed { get; set; }
        public String StatusOption { get; set; }
    }
}
