﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class ErrorLog
    {
        public ErrorLog()
        {
        }

        
        public ErrorLog(String id, String dateCreated, String message,
            String code, String description, String level)
        {
            this.Id = id;
            this.DateCreated = dateCreated;
            this.Message = message;
            this.Code = code;
            this.Description = description;
            this.Level = level;
        }

        public String Id { get; set; }
        public String DateCreated { get; set; }
        public String Message { get; set; }
        public String Code { get; set; }
        public String Description { get; set; }
        public String Level { get; set; }
    }
}
