﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
        [Serializable]
        public class UserOrganization
        {
            public String AccountId { get; set; }
            public String AccountName { get; set; }
        }
}
