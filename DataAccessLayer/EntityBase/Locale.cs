﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class Locale
    {
        public Locale()
        {
        }

        public Locale(String id, String description, String abbreviation)
        {
            this.Id = id;
            this.Description = description;
            this.Abbreviation = abbreviation;
        }

        public String Id { get; set; }
        public String Description { get; set; }
        public String Abbreviation { get; set; }
    }
}