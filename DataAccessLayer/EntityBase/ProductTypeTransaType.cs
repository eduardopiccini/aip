﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class ProductTypeTransaType
    {
        public ProductTypeTransaType()
        {
        }

        public ProductTypeTransaType(Int64 productTypeId, String productTypeDescription,
            Int64 transaTypeId, String transaTypeDescription)
        {
            this.ProductTypeId = productTypeId;
            this.ProductTypeDescription = productTypeDescription;
            this.TransaTypeId = transaTypeId;
            this.TransaTypeDescription = transaTypeDescription;
        }

        public Int64 ProductTypeId { set; get; }
        public String ProductTypeDescription {set; get;}
        public Int64 TransaTypeId { set; get; }
        public String TransaTypeDescription {set; get;}
    }
}