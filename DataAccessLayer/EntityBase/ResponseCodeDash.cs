﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class ResponseCodeDash
    {
        public ResponseCodeDash()
        {
        }

        public ResponseCodeDash(String responseCodeId, String responseCodeDescription, Int32 quantity,
            Decimal amount, Decimal amountCurrency)
        {
            this.ResponseCodeId = responseCodeId;
            this.ResponseCodeDescription = responseCodeDescription;
            this.Quantity = quantity;
            this.Amount = amount;
            this.AmountCurrency = amountCurrency;
        }

        public String ResponseCodeId { get; set; }
        public String ResponseCodeDescription { get; set; }
        public Int32 Quantity { get; set; }
        public Decimal Amount { get; set; }
        public Decimal AmountCurrency { get; set; }
    }
}
