﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class AccountContact
    {
        public AccountContact()
        {
        }

        public AccountContact(Int64 id, Int64 accountId, String name, Int64 statusId,
            String statusDescription, String email, Int64? telephone, Int64? mobile,
            String department, String job, String billingNotification)
        {
            this.Id = id;
            this.AccountId = accountId;
            this.Name = name;
            this.StatusId = statusId;
            this.StatusDescription = statusDescription;
            this.Email = email;
            this.Telephone = telephone;
            this.Mobile = mobile;
            this.Department = department;
            this.Job = job;
            this.BillingNotification = billingNotification;
        }

        public Int64 Id { get; set; }
        public Int64 AccountId { get; set; }
        public String Name { get; set; }
        public Int64 StatusId { get; set; }
        public String StatusDescription { get; set; }
        public String Email { get; set; }
        public Int64? Telephone { get; set; }
        public Int64? Mobile { get; set; }
        public String Department { get; set; }
        public String Job { get; set; }
        public String BillingNotification { get; set; }
    }
}