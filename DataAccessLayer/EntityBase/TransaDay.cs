﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class TransaDay
    {
        public String DayNumber { get; set; }
        public String DayName { get; set; }
        public Decimal Quantity { get; set; }
        public Decimal GrossAmount { get; set; }
        public Decimal NetAmount { get; set; }
        public Decimal GrossAmountCurrency { get; set; }
        public Decimal NetAmountCurrency { get; set; }
    }
}
