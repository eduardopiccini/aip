﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class ApiDay
    {
        public ApiDay()
        {
        }

        public ApiDay(Int32 dayNumber, String dayName, Decimal quantity, Decimal netAmount,
            Decimal failedQuantity, Decimal failedGrossAmount, Decimal failedNetAmount)
        {
            this.DayNumber = dayNumber;
            this.DayName = dayName;
            this.Quantity = quantity;
            this.NetAmount = netAmount;
            this.FailedQuantity = failedQuantity;
            this.FailedGrossAmount = failedGrossAmount;
            this.FailedNetAmount = failedNetAmount;
        }

        public Int32 DayNumber { get; set; }
        public String DayName { get; set; }
        public Decimal Quantity { get; set; }
        public Decimal NetAmount { get; set; }
        public Decimal FailedQuantity { get; set; }
        public Decimal FailedGrossAmount { get; set; }
        public Decimal FailedNetAmount { get; set; }
    }
}
