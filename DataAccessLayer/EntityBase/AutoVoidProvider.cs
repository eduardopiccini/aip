﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    public class AutoVoidProvider
    {
        public String RoutingId { get; set; }
        public String AccountId { get; set; }
        public String AccountName { get; set; }
        public String Ip { get; set; }
        public String Port { get; set; }
        public String ProviderId { get; set; }
        public String ProviderName { get; set; }
        public String Amount { get; set; }
        public String Url { get; set; }
        public String Phone { get; set; }
        public String RechargeStamp { get; set; }
        public String Reference { get; set; }
    }
}
