﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class Account
    {
        public Account()
        {
        }

        public Account(Int64 id, Int64? parent_account_id, String parent_account_name, 
            String name, String abbreviation, String address,
            Decimal balance, Decimal creditLimit, Int32 lowBalanceAlert,
            String fax, String telephone, String zipcode, String note,
            Int32 countryId, String countryDescription, Int32 billingTermId,
            String billingTermDescription, String currencyId,
            String currencyDescription,
            String vendorGroupId, String vendorGroupDescription,
            Decimal providerBalance, Decimal providerCreditLimit,
            String stateId, String stateDescription, Int32 statusId,
            String statusDescription, String lowBalanceAlertAmount,
            String lowBalanceAlertInterval, String lowBalanceProviderAmount, String accountTypeId,
            String accountTypeDescription, DateTime? lastPaymentDate, Decimal lastPaymentAmount, String lastPaymentType, String logo, String secondtimeouttransaction)
        {
            this.Id = id;
            this.Name = name;
            this.ParentAccountId = parent_account_id;
            this.ParentAccountName = parent_account_name;
            this.Abbreviation = abbreviation;
            this.Address = address;
            this.Balance = balance;
            this.CreditLimit = creditLimit;
            this.LowBalanceAlert = lowBalanceAlert;
            this.Fax = fax;
            this.Telephone = telephone;
            this.ZipCode = zipcode;
            this.Note = note;
            this.StatusId = statusId;
            this.StatusDescription = statusDescription;
            this.CountryId = countryId;
            this.CountryDescription = countryDescription;
            this.BillingTermId = billingTermId;
            this.BillingTermDescription = billingTermDescription;
            this.CurrencyId = currencyId;
            this.CurrencyDescription = currencyDescription;
            this.VendorGroupId = vendorGroupId;
            this.VendorGroupDescription = vendorGroupDescription;
            this.StateId = stateId;
            this.StateDescription = stateDescription;
            this.ProviderBalance=providerBalance;
            this.ProviderCreditLimit = providerCreditLimit;
            this.LowBalanceAlertAmount = lowBalanceAlertAmount;
            this.LowBalanceAlertInterval = lowBalanceAlertInterval;
            this.AccountTypeId = accountTypeId;
            this.AccountTypeDescription = accountTypeDescription;
            this.LowBalanceProviderAmount = lowBalanceProviderAmount;
            this.LastPaymentAmount = lastPaymentAmount;
            this.LastPaymentDate = lastPaymentDate;
            this.LastPaymentType = lastPaymentType;
            this.Logo = logo;
            this.SecondTimeoutTransaction = secondtimeouttransaction;
        }


        public Int64 Id { get; set; }
        public Int64? ParentAccountId { get; set; }
        public String ParentAccountName { get; set; }
        public String Name { get; set; }
        public String Abbreviation { get; set; }
        public String Address { get; set; }
        public Decimal Balance { get; set; }
        public Decimal CreditLimit { get; set; }
        public Int32 LowBalanceAlert { get; set; }
        public String Fax { get; set; }
        public String Telephone { get; set; }
        public String ZipCode { get; set; }
        public String Note { get; set; }
        public Int32 StatusId { get; set; }
        public String StatusDescription { get; set; }
        public Int32 CountryId { get; set; }
        public String CountryDescription { get; set; }
        public Int32 BillingTermId { get; set; }
        public String BillingTermDescription { get; set; }
        public String CurrencyId { get; set; }
        public String CurrencyDescription { get; set; }
        public String VendorGroupId { get; set; }
        public String VendorGroupDescription { get; set; }
        public Decimal ProviderBalance { get; set; }
        public Decimal ProviderCreditLimit { get; set; }
        public String StateId { get; set; }
        public String StateDescription { get; set; }
        public String LowBalanceAlertAmount { get; set; }
        public String LowBalanceAlertInterval { get; set; }
        public String AccountTypeId { get; set; }
        public String AccountTypeDescription { get; set; }
        public String LowBalanceProviderAmount { get; set; }
        public DateTime? LastPaymentDate { get; set; }
        public Decimal LastPaymentAmount { get; set; }
        public String  LastPaymentType{ get; set; }
        public String Logo { get; set; }
        public String SecondTimeoutTransaction { get; set; }
    }
}