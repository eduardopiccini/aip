﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class BalanceLog
    {
        public BalanceLog()
        {
        }

        public BalanceLog(Int64 id, String directionId,
            String directionName, Int64 accountId, String accountName, String dateCreated,
            Decimal balance, Decimal oldBalance, String userCreated, String dateUpdated,
            String userUpdated)
        {
            this.Id = id;
            this.DirectionId = directionId;
            this.DirectionName = directionName;
            this.AccountId = accountId;
            this.AccountName = accountName;
            this.DateCreated = dateCreated;
            this.Balance = balance;
            this.OldBalance = oldBalance;
            this.UserCreated = userCreated;
            this.DateUpdated = dateUpdated;
            this.UserUpdated = userUpdated;
        }

        public Int64 Id { get; set; }
        public String DirectionId { get; set; }
        public String DirectionName { get; set; }
        public Int64 AccountId { get; set; }
        public String AccountName { get; set; }
        public String DateCreated { get; set; }
        public Decimal Balance { get; set; }
        public Decimal OldBalance { get; set; }
        public String UserCreated { get; set; }
        public String DateUpdated { get; set; }
        public String UserUpdated { get; set; }

    }

}