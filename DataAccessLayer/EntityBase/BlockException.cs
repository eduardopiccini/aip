﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class BlockException
    {
        public BlockException()
        {
        
        }

        public BlockException(String id, String description)
        {
            this.Id = id;
            this.Description = description;
        }


        public String Id { get; set; }
        public String Description { get; set; }
    }
}
