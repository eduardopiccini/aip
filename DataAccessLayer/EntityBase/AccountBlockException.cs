﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class AccountBlockException
    {
        public AccountBlockException()
        {
        }

        public AccountBlockException(String recordId, String accountId, String accountName, String exceptionTypeId,
            String exceptionTypeName, String id, String description,
            String productTypeId, String productTypeName, String productId, String productName,
            String providerId, String providerName, String countryId, String countryName,
            String carrierId, String carrierName, String merchant, String note)
        {
            this.AccountId = accountId;
            this.AccountName = accountName;
            this.ExceptionTypeId = exceptionTypeId;
            this.ExceptionTypeName = exceptionTypeName;
            this.Id = id;
            this.Description = description;
            this.ProductTypeId = productTypeId;
            this.ProductTypeName = productTypeName;
            this.ProductId = productId;
            this.ProductName = productName;
            this.ProviderId = providerId;
            this.ProviderName = providerName;
            this.CountryId = countryId;
            this.CountryName = countryName;
            this.CarrierId = carrierId;
            this.CarrierName = carrierName;
            this.Merchant = merchant;
            this.Note = note;
            this.RecordId = recordId;
        }

        public String RecordId { get; set; }
        public String AccountId {get;set;}
        public String AccountName { get; set; }
        public String ExceptionTypeId { get; set; }
        public String ExceptionTypeName { get; set; }
        public String Id { get; set; }
        public String Description { get; set; }
        public String ProductTypeId { get; set; }
        public String ProductTypeName { get; set; }
        public String ProductId { get; set; }
        public String ProductName { get; set; }
        public String ProviderId { get; set; }
        public String ProviderName { get; set; }
        public String CountryId { get; set; }
        public String CountryName { get; set; }
        public String CarrierId { get; set; }
        public String CarrierName { get; set; }
        public String Merchant { get; set; }
        public String Note { get; set; }
    }
}
