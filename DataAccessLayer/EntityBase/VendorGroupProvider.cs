﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class VendorGroupProvider
    {
        public VendorGroupProvider()
        {
        }

        public VendorGroupProvider(String vendorGroupId, String vendorGroupDescription, 
            String providerId, String providerDescription, String weight)
        {
            this.VendorGroupId = vendorGroupId;
            this.VendorGroupDescription = vendorGroupDescription;
            this.ProviderId = providerId;
            this.ProviderDescription = providerDescription;
            this.Weight = weight;
        }

        public String VendorGroupId { get; set; }
        public String VendorGroupDescription { get; set; }
        public String ProviderId { get; set; }
        public String ProviderDescription { get; set; }
        public String Weight { get; set; }
    }
}