﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.EntityBase
{
    [Serializable]
    public class SendSms
    {
        public SendSms()
        { 
        
        }

        public SendSms(String requestId, String phone, String message)
        {
            this.RequestId = requestId;
            this.Phone = phone;
            this.Message = message;
        }

        public String RequestId { get; set; }
        public String Phone { get; set; }
        public String Message { get; set; }
    }
}
