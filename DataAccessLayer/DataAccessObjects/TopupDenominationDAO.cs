﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;

namespace DataAccessLayer.DataAccessObjects
{

    [Serializable]
    public class TopupDenominationResult : Result
    {
        public List<TopupDenomination> TopupDenomination { get; set; }
    }

    public class TopupDenominationDAO
    {
        public TopupDenominationResult Search(String id, String providerId)
        {
            TopupDenominationResult result = new TopupDenominationResult();
            List<TopupDenomination> list = new List<TopupDenomination>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select denomination_id, provider_id, provider_description, min_amount, max_amount, multiple ");
                query.Append(" from ip_vw_topup_denomination ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "denomination_id", (id == null) ? null : id.ToUpper(), OracleDbType.Varchar2);
                DataManager.AddBindParamCmd(ref cmd, ref query, "provider_id", (providerId == null) ? null : providerId, OracleDbType.Int32);
                
                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new TopupDenomination(
                            row["denomination_id"] == null ? null : row["denomination_id"].ToString(),
                            row["provider_id"] == null ? null : row["provider_id"].ToString(),
                            row["provider_description"] == null ? null : row["provider_description"].ToString(),
                            row["min_amount"] == null ? 0 : Convert.ToDecimal(row["min_amount"].ToString()),
                            row["max_amount"] == null ? 0 : Convert.ToDecimal(row["max_amount"].ToString()),
                            row["multiple"] == null ? 0 : Convert.ToInt32(row["multiple"].ToString())));
                    }

                    result.TopupDenomination = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result Save(String providerId, String minAmount,
            String maxAmount, String multiple, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_provider_id", OracleDbType.Varchar2).Value = providerId;
                cmd.Parameters.Add("in_min_amount", OracleDbType.Decimal).Value = (String.IsNullOrEmpty(minAmount) ? null : minAmount);
                cmd.Parameters.Add("in_max_amount", OracleDbType.Decimal).Value = (String.IsNullOrEmpty(maxAmount) ? null : maxAmount);
                cmd.Parameters.Add("in_multiple", OracleDbType.Int32).Value = (String.IsNullOrEmpty(multiple) ? null : multiple);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_provider.save_denomination", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result Remove(String denominationId, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_denomination_id", OracleDbType.Varchar2).Value = denominationId;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_provider.remove_denomination", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }
    }
}
