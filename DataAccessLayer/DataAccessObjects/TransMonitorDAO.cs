﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using Common;

namespace DataAccessLayer.DataAccessObjects
{
    public class TransMonitorDAO
    {

        public List<AutoVoidProvider> AutoVoidProvider { get; set; }

        public Result Search()
        {
            Result result = new Result();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select routing_id, account_id, ip, port, provider_id, provider_name, phone, account_name, amount, url, reference, recharge_stamp ");
                query.Append(" from ip_VW_AUTO_VOID_PROVIDER ");

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    AutoVoidProvider = new List<AutoVoidProvider>();

                    foreach (DataRow row in dt.Rows)
                    {
                        AutoVoidProvider auto = new AutoVoidProvider();

                        auto.RoutingId = Common.Utils.IsNull(row["routing_id"], "").ToString();
                        auto.AccountId = Common.Utils.IsNull(row["account_id"], "").ToString();
                        auto.AccountName = Common.Utils.IsNull(row["account_name"], "").ToString();
                        auto.Ip  = Common.Utils.IsNull(row["ip"], "").ToString();
                        auto.Port = Common.Utils.IsNull(row["port"], "").ToString();
                        auto.ProviderId = Common.Utils.IsNull(row["provider_id"], "").ToString();
                        auto.ProviderName = Common.Utils.IsNull(row["provider_name"], "").ToString();
                        auto.Phone = Common.Utils.IsNull(row["phone"], "").ToString();
                        auto.Amount = Common.Utils.IsNull(row["amount"], "").ToString();
                        auto.Url = Common.Utils.IsNull(row["url"], "").ToString();
                        auto.Reference = Common.Utils.IsNull(row["reference"], "").ToString();
                        auto.RechargeStamp = Common.Utils.IsNull(row["recharge_stamp"], "").ToString();

                        AutoVoidProvider.Add(auto);
                    }

                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result ReferenceSequenceTimeout()
        {
            Result result = new Result();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            Object sec = null;

            try
            {
                query.Append(" SELECT ip_sq_reference_timeout.NEXTVAL  FROM DUAL ");

                sec = DataManager.GetExecuteScalar(ref cmd, query.ToString(), "", "");

                if (sec!=null)
                {
                    result.Code = "0";
                    result.Message = "TRUE";
                    result.Id = Convert.ToString(sec);
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result AutoVoidLog(String routingId, DateTime? dateBegin, DateTime? dateEnd, String responseCodeId, String responseMessage,
            String providerReference)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                /*
                 * ip_PG_TRANS_MONITOR.auto_void_log 
                                                              (in_routing_id                  IN number,
                                                               id_date_begin                IN date,
                                                               id_date_end                   IN date,
                                                               iz_response_code_id      IN varchar2,
                                                               iz_response_message    IN varchar2,
                                                               iz_provider_reference      IN varchar2) 
                 * 
                 * */

                cmd.Parameters.Add("in_routing_id", OracleDbType.Int64).Value = routingId;
                cmd.Parameters.Add("id_date_begin", OracleDbType.Date).Value = dateBegin;
                cmd.Parameters.Add("id_date_end", OracleDbType.Date).Value = dateEnd;
                cmd.Parameters.Add("iz_response_code_id", OracleDbType.Varchar2, 200).Value = responseCodeId;
                cmd.Parameters.Add("iz_response_message", OracleDbType.Varchar2, 1000).Value = responseMessage;
                cmd.Parameters.Add("iz_provider_reference", OracleDbType.Varchar2, 100).Value = providerReference;

                DataManager.RunDataCommandSP(ref cmd, "ip_PG_TRANS_MONITOR.auto_void_log", "", "");

                result.Code = "0";
                result.Message = "TRUE";
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNCAUGHT_ERROR;
                result.Message = ex.Message;
            }

            return result;
        }
    }
}
