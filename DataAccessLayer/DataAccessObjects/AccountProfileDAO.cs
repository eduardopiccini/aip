﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using Common;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class AccountProfileResult : Result
    {
        public List<AccountProfile> AccountProfile { get; set; }
    }

    [Serializable]
    public class ProfileLoginResult : Result
    {
        public ProfileLoginResult()
        {
            
        }

        public String ProfileId { get; set; }
        public String ProviderId { get; set; }
    }

    [Serializable]
    public class AccountConnectionProfileResult : Result
    {
        public List<AccountConnectionProfile> AccountConnectionProfile { get; set; }
    }

    public class AccountProfileDAO : IDisposable
    {


        public AccountProfileResult SearchProfile(String id, String directionId, String accountId,
             String apiUser)
        {
            AccountProfileResult result = new AccountProfileResult();
            List<AccountProfile> item = new List<AccountProfile>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select profile_id, direction_id, account_id,  api_user,  ");
                query.Append(" status_id, status_description, failed_attemps, date_blocked, required_ip, required_merchant, ");
                query.Append(" date_created, user_created, date_updated, user_updated ");
                query.Append(" from ip_vw_account_profile ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "profile_id", id, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "direction_id", directionId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "account_id", accountId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "api_user", apiUser, OracleDbType.Varchar2);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        item.Add(new AccountProfile
                        {
                            ProfileId = Convert.ToInt64(row["profile_id"].ToString()),
                            DirectionId = Convert.ToInt64(row["direction_id"].ToString()),
                            AccountId = Convert.ToInt64(row["account_id"].ToString()),
                            ApiUser = row["api_user"] == null ? null : row["api_user"].ToString(),
                            StatusId = row["status_id"] == null ? null : row["status_id"].ToString(),
                            StatusDescription = row["status_description"] == null ? null : row["status_description"].ToString(),
                            FailedAttemps = row["failed_attemps"] == null ? null : row["failed_attemps"].ToString(),
                            DateBlocked = row["date_blocked"] == null ? null : row["date_blocked"].ToString(),
                            RequiredIp = row["required_ip"].ToString(),
                            RequiredMerchant = row["required_merchant"].ToString()
                        });

                   
                            
                    }

                    result.AccountProfile = item;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public ProfileLoginResult LoginProfile(String user, String password, String accountId, String ip, String locale)
        {
            OracleCommand cmd = new OracleCommand();
            ProfileLoginResult result = new ProfileLoginResult();

            try
            {
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 30).Value = user;
                cmd.Parameters.Add("iz_api_password", OracleDbType.Varchar2, 50).Value = password;
                cmd.Parameters.Add("in_account_id", OracleDbType.Int64).Value = accountId;
                cmd.Parameters.Add("iz_ip", OracleDbType.Varchar2, 20).Value = ip;
                cmd.Parameters.Add("on_profile_id", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_account_profile.profile_login", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                result.ProfileId = (cmd.Parameters["on_profile_id"]==null) ? "" : cmd.Parameters["on_profile_id"].ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }
        


        public Result ChangeProfilePassword(String id, String user, String password, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_profile_id", OracleDbType.Int32).Value = id;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 30).Value = user;
                cmd.Parameters.Add("iz_api_password", OracleDbType.Varchar2, 50).Value = password;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_account_profile.change_password", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result RemoveProfile(String id, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_profile_id", OracleDbType.Int32).Value = id;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_account_profile.remove_profile", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result SaveProfile(String id, String directionId, String accountId, String appUser, String statusId, String requiredIp, String requiredMerchant,
            String locale, String actApiUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_profile_id", OracleDbType.Int32, Common.Utils.IsNull(id, null), ParameterDirection.InputOutput);
                cmd.Parameters.Add("in_direction_id", OracleDbType.Int32).Value = directionId;
                cmd.Parameters.Add("in_account_id", OracleDbType.Int32).Value = accountId;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 100).Value = appUser;
                cmd.Parameters.Add("in_status_id", OracleDbType.Int32).Value = statusId;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = actApiUser;
                cmd.Parameters.Add("in_required_ip", OracleDbType.Int32).Value = requiredIp;
                cmd.Parameters.Add("in_required_merchant", OracleDbType.Int32).Value = requiredMerchant;


                

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_account_profile.save_profile", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (result.Code.Equals("0"))
                {
                    result.Id = cmd.Parameters["in_profile_id"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public AccountConnectionProfileResult SearchProfileConnection(String id, String profileId)
        {
            AccountConnectionProfileResult result = new AccountConnectionProfileResult();
            List<AccountConnectionProfile> item = new List<AccountConnectionProfile>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select profile_conexion_id, profile_id, ip, port, url, weight, ");
                query.Append(" date_created, user_created, date_updated, user_updated ");
                query.Append(" from ip_vw_profile_conexion ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "profile_conexion_id", id, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "profile_id", profileId, OracleDbType.Int32);
                
                query.Append("order by weight asc ");

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        item.Add(new AccountConnectionProfile(
                            Convert.ToInt64(row["profile_conexion_id"].ToString()),
                            Convert.ToInt64(row["profile_id"].ToString()),
                            Common.Utils.IsNull(row["ip"], "").ToString(),
                            Common.Utils.IsNull(row["port"], "").ToString(),
                            Common.Utils.IsNull(row["url"], "").ToString(),
                            Common.Utils.IsNull(row["weight"], "").ToString()));
                    }

                    result.AccountConnectionProfile = item;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result SaveProfileConnection(String id, String profileId, String ip, String port, String url, String weight,
            String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_profile_conexion_id", OracleDbType.Int32, ParameterDirection.InputOutput).Value = (String.IsNullOrEmpty(id) ? null : id);
                cmd.Parameters.Add("in_profile_id", OracleDbType.Int32).Value = (String.IsNullOrEmpty(profileId) ? null : profileId);
                cmd.Parameters.Add("iz_ip", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(ip) ? null : ip);
                cmd.Parameters.Add("in_port", OracleDbType.Int32).Value = (String.IsNullOrEmpty(port) ? null : port);
                cmd.Parameters.Add("iz_url", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(url) ? null : url);
                cmd.Parameters.Add("in_weight", OracleDbType.Int32).Value = (String.IsNullOrEmpty(weight) ? null : weight);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_account_profile.save_conexion", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (result.Code.Equals("0"))
                {
                    result.Id = cmd.Parameters["in_profile_conexion_id"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result RemoveProfileConnection(String id, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_profile_conexion_id", OracleDbType.Int32).Value = (String.IsNullOrEmpty(id) ? null : id);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_account_profile.remove_conexion", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public void Dispose()
        {
        }
    }
}
