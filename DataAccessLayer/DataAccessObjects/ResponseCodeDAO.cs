﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using Common;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class ResponseCodeResult : Result
    {
        public List<ResponseCode> ResponseCode { get; set; }
    }

    [Serializable]
    public class ResponseCodeRelationResult : Result
    {
        public List<ResponseCodeRelation> ResponseCodeRelation { get; set; }
    }


    public class ResponseCodeDAO : IDisposable
    {
        /// <summary>
        /// Gets the response code by the code
        /// </summary>
        /// <param name="code"></param>
        /// <returns>A response code</returns>
        public ResponseCodeResult Search(String code, String message, String description)
        {
            ResponseCodeResult result = new ResponseCodeResult();
            List<ResponseCode> list = new List<ResponseCode>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select response_code_id, response_message, response_description ");
                query.Append(" from ip_vw_response_code ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "response_code_id", code, OracleDbType.Varchar2);
                DataManager.AddBindParamCmd(ref cmd, ref query, "response_message", Common.Utils.IsNull(message,"").ToString().ToUpper(), OracleDbType.Varchar2, DataManager.eOperador.Like_Donde_Encuentre);
                DataManager.AddBindParamCmd(ref cmd, ref query, "response_description", Common.Utils.IsNull(description, "").ToString().ToUpper(), OracleDbType.Varchar2, DataManager.eOperador.Like_Donde_Encuentre);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new ResponseCode
                        {
                            Code = Common.Utils.IsNull(row["response_code_id"], "").ToString(),
                            Description = Common.Utils.IsNull(row["response_description"], "").ToString(),
                            Message = Common.Utils.IsNull(row["response_message"], "").ToString()
                        });
                    }

                    result.ResponseCode = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result Add(String code, String message, String description, String locale,
            String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("iz_response_code_id", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(code) ? null : code);
                cmd.Parameters.Add("iz_response_message", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(message) ? null : message);
                cmd.Parameters.Add("iz_response_description", OracleDbType.Varchar2, 20).Value = (String.IsNullOrEmpty(description) ? null : description);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_response_code.save", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result Modify(String code, String message, String description, String locale,
            String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("iz_response_code", OracleDbType.Varchar2, 100).Value = code;
                cmd.Parameters.Add("iz_response_message", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(message) ? null : message);
                cmd.Parameters.Add("iz_response_description", OracleDbType.Varchar2, 20).Value = (String.IsNullOrEmpty(description) ? null : description);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_response_code.change", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result Remove(String code, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("iz_response_code", OracleDbType.Varchar2, 100).Value = code;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_response_code.remove", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public ResponseCodeRelationResult SearchRelation(String code, String providerId)
        {
            ResponseCodeRelationResult result = new ResponseCodeRelationResult();
            List<ResponseCodeRelation> list = new List<ResponseCodeRelation>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select response_code_id, response_message, provider_id, provider_name, ");
                query.Append(" provider_response_code_id, prov_resp_description, weight ");
                query.Append(" from ip_vw_response_code_relation ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "response_code_id", code, OracleDbType.Varchar2);
                DataManager.AddBindParamCmd(ref cmd, ref query, "provider_id", providerId, OracleDbType.Int32);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new ResponseCodeRelation(
                            row["response_code_id"] == null ? null : row["response_code_id"].ToString(),
                            row["response_message"] == null ? null : row["response_message"].ToString(),
                            Convert.ToInt64(row["provider_id"].ToString()),
                            row["provider_name"].ToString(),
                            row["provider_response_code_id"] == null ? null : row["provider_response_code_id"].ToString(),
                            row["prov_resp_description"] == null ? null : row["prov_resp_description"].ToString(),
                            Convert.ToInt64(row["weight"].ToString())));
                    }

                    result.ResponseCodeRelation = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result SaveRelation(String code, String providerId, String providerResponseCodeId, String weight,
            String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("iz_response_code_id", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(code) ? null : code);
                cmd.Parameters.Add("in_provider_id", OracleDbType.Int32).Value = (String.IsNullOrEmpty(providerId) ? null : providerId);
                cmd.Parameters.Add("iz_provider_response_code_id", OracleDbType.Varchar2, 20).Value = (String.IsNullOrEmpty(providerResponseCodeId) ? null : providerResponseCodeId);
                cmd.Parameters.Add("in_weight", OracleDbType.Int32).Value = (String.IsNullOrEmpty(weight) ? null : weight);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_response_code.save_relation", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result RemoveRelation(String code, String providerId, String providerResponseCodeId, String locale,
            String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("iz_response_code_id", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(code) ? null : code);
                cmd.Parameters.Add("in_provider_id", OracleDbType.Int32).Value = (String.IsNullOrEmpty(providerId) ? null : providerId);
                cmd.Parameters.Add("iz_provider_response_code_id", OracleDbType.Varchar2, 20).Value = providerResponseCodeId;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_response_code.remove_relation", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result GetResponseCode(String providerId, String providerResponseCodeId, String providerResponseCodeMessage, String locale,
            String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_provider_id", OracleDbType.Int32).Value = providerId;
                cmd.Parameters.Add("iz_provider_response_code_id", OracleDbType.Varchar2).Value = providerResponseCodeId;
                cmd.Parameters.Add("iz_provider_response_code_message", OracleDbType.Varchar2).Value = providerResponseCodeMessage;
                cmd.Parameters.Add("oz_response_code_id", OracleDbType.Varchar2, 10).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_response_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                
                DataManager.RunDataCommandSP(ref cmd, "ip_pg_response_code.response_code", "", "");

                result.Code = (cmd.Parameters["oz_response_code_id"].Value == null) ? "" : cmd.Parameters["oz_response_code_id"].Value.ToString();
                result.Message = (cmd.Parameters["oz_response_message"].Value == null) ? "" : cmd.Parameters["oz_response_message"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        #region IDisposable Members

        public void Dispose()
        {

        }

        #endregion
    }
}
