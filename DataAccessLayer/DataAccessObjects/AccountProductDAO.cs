﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using OracleAccessLayer;
using Oracle.DataAccess.Client;
using System.Data;
using Common;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class AccountOrganizationResult : Result
    {
        public List<AccountOrganization> AccountProduct { get; set; }
    }

    [Serializable]
    public class AccountProviderResult : Result
    {
        public List<AccountProvider> AccountProvider { get; set; }
    }

    [Serializable]
    public class AccountProductResult : Result
    {
        public List<AccountProduct> AccountProduct { get; set; }
    }



    public class AccountProductDAO
    {
        public AccountProductResult GetProductAccount(String TestSecId)
        {
            AccountProductResult result = new AccountProductResult();
            List<AccountProduct> list = new List<AccountProduct>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select test_sec_id, test_log_id, product_name, product_id, max_amount, amount ");
                query.Append(" from ip_vw_routing_test ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "test_sec_id", TestSecId, OracleDbType.Int32);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new AccountProduct
                        {
                            TestSecId = Utils.ConvertToInt64(row["test_sec_id"], DBNull.Value),
                            TestLogId = Utils.ConvertToInt64(row["test_log_id"], DBNull.Value),
                            ProductId = Utils.ConvertToInt64(row["product_id"], DBNull.Value),
                            ProductName = row["product_name"].ToString(),
                            Amount = Utils.ConvertToDecimal(row["amount"], DBNull.Value),
                            MaxAmount = Utils.ConvertToDecimal(row["max_amount"], DBNull.Value)

                        });
                    }

                    result.AccountProduct = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }

            return result;
        }
        public AccountOrganizationResult GetProductOrganization(String Id)
        {
            AccountOrganizationResult result = new AccountOrganizationResult();
            List<AccountOrganization> list = new List<AccountOrganization>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select test_log_id, account_id, account_name, discount, surcharge, routing_mode, account_balance ");
                query.Append(" from ip_vw_routing_test_account ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "test_log_id", Id, OracleDbType.Int32);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new AccountOrganization
                        {
                            Id = Utils.ConvertToInt64(row["test_log_id"], DBNull.Value),
                            AccountId = Utils.ConvertToInt64(row["account_id"], DBNull.Value),
                            AccountName = row["account_name"].ToString(),
                            Discount = Utils.ConvertToInt64(row["discount"], DBNull.Value),
                            Surcharge = Utils.ConvertToInt64(row["surcharge"], DBNull.Value),
                            RoutingMode = row["routing_mode"].ToString(),
                            AccountBalance = Utils.ConvertToDecimal(row["account_balance"], DBNull.Value)
                        });
                    }

                    result.AccountProduct = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }

            return result;
        }

        public AccountProviderResult GetProductProvider(String Id)
        {
            AccountProviderResult result = new AccountProviderResult();
            List<AccountProvider> list = new List<AccountProvider>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select test_log_id, provider_id, provider_name, discount, surcharge, account_balance, product_code, trans_quantity, trans_valid, trans_invalid, trans_amount ");
                query.Append(" from ip_vw_routing_test_provider ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "test_log_id", Id, OracleDbType.Int32);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new AccountProvider
                        {
                            Id = Utils.ConvertToInt64(row["test_log_id"], DBNull.Value),
                            ProviderId = Utils.ConvertToInt64(row["provider_id"], DBNull.Value),
                            ProviderName = row["provider_name"].ToString(),
                            Discount = Utils.ConvertToInt64(row["discount"], DBNull.Value),
                            Surcharge = Utils.ConvertToInt64(row["surcharge"], DBNull.Value),
                            AccountBalance = Utils.ConvertToDecimal(row["account_balance"], DBNull.Value),
                            ProductCode = row["product_code"].ToString(),
                            TransactionQuantity = Utils.ConvertToInt64(row["trans_quantity"], DBNull.Value),
                            TransactionValid = Utils.ConvertToInt64(row["trans_valid"], DBNull.Value),
                            TransactionInvalid = Utils.ConvertToInt64(row["trans_invalid"], DBNull.Value),
                            TransactionAmount = Utils.ConvertToDecimal(row["trans_amount"], DBNull.Value)
                        });
                    }

                    result.AccountProvider = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }

            return result;
        }

        public String GetCountryRoutingId(String Account, String ProductType, String Country, String Carrier, String Amount, String locale,
            String appUser)
        {
            OracleCommand cmd = new OracleCommand();

            try
            {
                cmd.Parameters.Add("in_account_id", OracleDbType.Int64).Value = Utils.ConvertToInt64(Account, DBNull.Value);
                cmd.Parameters.Add("in_product_type_id", OracleDbType.Int64).Value = Utils.ConvertToInt64(ProductType, DBNull.Value);
                cmd.Parameters.Add("in_country_id", OracleDbType.Int64).Value = Utils.ConvertToInt64(Country, DBNull.Value); ;
                cmd.Parameters.Add("in_carrier_id", OracleDbType.Varchar2, 50).Value = Carrier;
                cmd.Parameters.Add("in_amount", OracleDbType.Decimal).Value = Utils.ConvertToDecimal(Amount, DBNull.Value);
                cmd.Parameters.Add("on_test_sec_id", OracleDbType.Int64).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_routing.country_routing", "", "");


            }
            catch (Exception ex)
            {
                return "";
            }

            return cmd.Parameters["on_test_sec_id"].Value.ToString();
        }


    }
}
