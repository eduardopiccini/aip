﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class AccountBlockExceptionResult : Result
    {
        public List<AccountBlockException> AccountBlockException { get; set; }
    }

    public class AccountBlockExceptionDAO
    {
        public AccountBlockExceptionResult Search(String recordId, String accountId, String productTypeId, String providerId,
            String productId, String countryId, String carrierId, String merchant)
        {
            AccountBlockExceptionResult result = new AccountBlockExceptionResult();
            List<AccountBlockException> list = new List<AccountBlockException>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select record_id, account_id, account_name, exception_type_id, product_type_id, ");
                query.Append(" exception_type_name, id, description, product_type_name, product_id, product_name, ");
                query.Append(" provider_id, provider_name, country_id, country_name, carrier_id, carrier_name, merchant, note ");
                query.Append(" from ip_vw_account_block_exception ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "record_id", recordId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "account_id", accountId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "product_type_id", productTypeId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "product_id", productId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "provider_id", providerId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "country_id", countryId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "carrier_id", carrierId, OracleDbType.Varchar2);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new AccountBlockException(
                            row["record_id"] == null ? null : row["record_id"].ToString(),
                            row["account_id"] == null ? null : row["account_id"].ToString(),
                            row["account_name"] == null ? null : row["account_name"].ToString(),
                            row["exception_type_id"] == null ? null : row["exception_type_id"].ToString(),
                            row["exception_type_name"] == null ? null : row["exception_type_name"].ToString(),
                            row["id"] == null ? null : row["id"].ToString(),
                            row["description"] == null ? null : row["description"].ToString(),
                            row["product_type_id"] == null ? null : row["product_type_id"].ToString(),
                            row["product_type_name"] == null ? null : row["product_type_name"].ToString(),
                            row["product_id"] == null ? null : row["product_id"].ToString(),
                            row["product_name"] == null ? null : row["product_name"].ToString(),
                            row["provider_id"] == null ? null : row["provider_id"].ToString(),
                            row["provider_name"] == null ? null : row["provider_name"].ToString(),
                            row["country_id"] == null ? null : row["country_id"].ToString(),
                            row["country_name"] == null ? null : row["country_name"].ToString(),
                            row["carrier_id"] == null ? null : row["carrier_id"].ToString(),
                            row["carrier_name"] == null ? null : row["carrier_name"].ToString(),
                            row["merchant"] == null ? null : row["merchant"].ToString(),
                            row["note"] == null ? null : row["note"].ToString()));
                    }

                    result.AccountBlockException = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result Save(String accountId, String exceptionType, String productTypeId, String productId, String providerId,
            String countryId, String carrierId, String note, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_account_id", OracleDbType.Int32, ParameterDirection.InputOutput).Value = Common.Utils.IsNull(accountId, null);
                cmd.Parameters.Add("iz_exception_type", OracleDbType.Varchar2, 20).Value = Common.Utils.IsNull(exceptionType, null);
                cmd.Parameters.Add("in_product_type_id", OracleDbType.Int32).Value = Common.Utils.IsNull(productTypeId, null);
                cmd.Parameters.Add("in_product_id", OracleDbType.Int32).Value = Common.Utils.IsNull(productId, null);
                cmd.Parameters.Add("in_provider_id", OracleDbType.Int32).Value = Common.Utils.IsNull(providerId, null);
                cmd.Parameters.Add("in_country_id", OracleDbType.Int32).Value = Common.Utils.IsNull(countryId, null);
                cmd.Parameters.Add("in_carrier_id", OracleDbType.Int32).Value = Common.Utils.IsNull(carrierId, null);
                cmd.Parameters.Add("iz_note", OracleDbType.Varchar2, 200).Value = Common.Utils.IsNull(note, null);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = Common.Utils.IsNull(locale, null);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_account.insert_block_exception", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result Remove(String recordId, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_record_id", OracleDbType.Int32, ParameterDirection.Input).Value = recordId;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = Common.Utils.IsNull(locale, null);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_account.remove_block_exception", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }
    }
}
