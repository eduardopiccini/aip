﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using System.Data;
using Oracle.DataAccess.Client;
using OracleAccessLayer;
using Common;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class ProductResult : Result
    {
        public List<Product> Product { get; set; }
    }

    public class ProductDAO
    {
        public ProductResult SearchProducts(String id, String productTypeId, String currencyId, 
            String countryId, String carrierId, String cityId, String statusId, String description)
        {
            ProductResult result = new ProductResult();
            List<Product> list = new List<Product>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select product_id, description, abbreviation, product_type_id, ");
                query.Append(" type_description, currency_id, currency_description, country_id, ");
                query.Append(" country_description, carrier_id, carrier_description, ");
                query.Append(" city_id, city_description, ");
                query.Append(" status_id, status_description, date_created, user_created, date_updated, user_updated ");
                query.Append(" from ip_vw_product ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "product_id", id, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "product_type_id", productTypeId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "currency_id", currencyId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "country_id", countryId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "carrier_id", carrierId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "city_id", cityId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "status_id", statusId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "description", Common.Utils.IsNull(description,"").ToString().ToUpper(), OracleDbType.Varchar2, DataManager.eOperador.Like_Donde_Encuentre);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new Product(
                            Convert.ToInt64(row["product_id"].ToString()),
                            row["description"] == null ? null : row["description"].ToString(),
                            (row["description"] == null ? null : row["description"].ToString()) + " (" + (row["currency_id"] == null ? null : row["currency_id"].ToString()) + ")",
                            row["abbreviation"] == null ? null : row["abbreviation"].ToString(),
                            Convert.ToInt64(row["product_type_id"].ToString()),
                            row["type_description"] == null ? null : row["type_description"].ToString(),
                            row["currency_id"] == null ? null : row["currency_id"].ToString(),
                            row["currency_description"] == null ? null : row["currency_description"].ToString(),
                            Convert.ToInt64(row["country_id"].ToString()),
                            row["country_description"] == null ? null : row["country_description"].ToString(),
                            row["carrier_id"] == null ? null : row["carrier_id"].ToString(),
                            row["carrier_description"] == null ? null : row["carrier_description"].ToString(),
                            Convert.ToInt64(Common.Utils.IsNull(row["city_id"],null)),
                            row["city_description"] == null ? null : row["city_description"].ToString(),
                            Convert.ToInt64(row["status_id"].ToString()),
                            row["status_description"] == null ? null : row["status_description"].ToString(),
                            row["date_created"] == null ? null : row["date_created"].ToString(),
                            row["user_created"] == null ? null : row["user_created"].ToString(),
                            row["date_updated"] == null ? null : row["date_updated"].ToString(),
                            row["user_updated"] == null ? null : row["user_updated"].ToString()));
                    }

                    result.Product = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public ProductResult SearchDominicanMovil(String id, 
            String currencyId, String countryId, String cityId, String statusId)
        {
            ProductResult result = new ProductResult();
            List<Product> list = new List<Product>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select product_id, description, abbreviation,  ");
                query.Append(" currency_id, currency_description, country_id, ");
                query.Append(" country_description, ");
                query.Append(" city_id, city_description,  ");
                query.Append(" status_id, status_description ");
                query.Append(" from ip_vw_product_dominican_movil ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "product_id", id, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "currency_id", currencyId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "country_id", countryId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "city_id", cityId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "status_id", statusId, OracleDbType.Int32);


                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new Product(
                            Convert.ToInt64(row["product_id"].ToString()),
                            row["description"] == null ? null : row["description"].ToString(),
                            (row["description"] == null ? null : row["description"].ToString()) + " (" + (row["currency_id"] == null ? null : row["currency_id"].ToString()) + ")",
                            row["abbreviation"] == null ? null : row["abbreviation"].ToString(), 
                            null, 
                            null,
                            row["currency_id"] == null ? null : row["currency_id"].ToString(),
                            row["currency_description"] == null ? null : row["currency_description"].ToString(),
                            Convert.ToInt64(row["country_id"].ToString()),
                            row["country_description"] == null ? null : row["country_description"].ToString(),
                            null,
                            null,
                            Convert.ToInt64(row["city_id"].ToString()),
                            row["city_description"] == null ? null : row["city_description"].ToString(),
                            Convert.ToInt64(row["status_id"].ToString()),
                            row["status_description"] == null ? null : row["status_description"].ToString(),
                            null,
                            null,
                            null,
                            null));
                    }

                    result.Product = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public ProductResult SearchMiPais(String id, String currencyId, String countryId,
            String cityId, String statusId)
        {
            ProductResult result = new ProductResult();
            List<Product> list = new List<Product>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select product_id, description, abbreviation,  ");
                query.Append(" currency_id, currency_description, country_id, ");
                query.Append(" country_description, ");
                query.Append(" city_id, city_description, ");
                query.Append(" status_id, status_description ");
                query.Append(" from ip_vw_product_mipais ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "product_id", id, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "currency_id", currencyId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "country_id", countryId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "city_id", cityId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "status_id", statusId, OracleDbType.Int32);


                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new Product(
                            Convert.ToInt64(row["product_id"].ToString()),
                            row["description"] == null ? null : row["description"].ToString(),
                            (row["description"] == null ? null : row["description"].ToString()) + " (" + (row["currency_id"] == null ? null : row["currency_id"].ToString()) + ")",
                            row["abbreviation"] == null ? null : row["abbreviation"].ToString(),
                            null,
                            null,
                            row["currency_id"] == null ? null : row["currency_id"].ToString(),
                            row["currency_description"] == null ? null : row["currency_description"].ToString(),
                            Convert.ToInt64(row["country_id"].ToString()),
                            row["country_description"] == null ? null : row["country_description"].ToString(),
                            null,
                            null,
                            Convert.ToInt64(row["city_id"].ToString()),
                            row["city_description"] == null ? null : row["city_description"].ToString(),
                            Convert.ToInt64(row["status_id"].ToString()),
                            row["status_description"] == null ? null : row["status_description"].ToString(),
                            null,
                            null,
                            null,
                            null));
                    }

                    result.Product = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public ProductResult SearchTopUp(String id, String currencyId, String countryId,
            String carrierId, String cityId, String statusId)
        {
            ProductResult result = new ProductResult();
            List<Product> list = new List<Product>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select product_id, description, abbreviation,  ");
                query.Append(" currency_id, currency_description, country_id, ");
                query.Append(" country_description, carrier_id, carrier_description, ");
                query.Append(" city_id, city_description, ");
                query.Append(" status_id, status_description ");
                query.Append(" from ip_vw_product_topup ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "product_id",  id, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "currency_id", currencyId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "country_id", countryId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "city_id", cityId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "status_id", statusId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "carrier_id", carrierId, OracleDbType.Int32);


                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new Product(
                            Convert.ToInt64(row["product_id"].ToString()),
                            row["description"] == null ? null : row["description"].ToString(),
                            (row["description"] == null ? null : row["description"].ToString()) + " (" + (row["currency_id"] == null ? null : row["currency_id"].ToString()) + ")",
                            row["abbreviation"] == null ? null : row["abbreviation"].ToString(),
                            null,
                            null,
                            row["currency_id"] == null ? null : row["currency_id"].ToString(),
                            row["currency_description"] == null ? null : row["currency_description"].ToString(),
                            Convert.ToInt64(row["country_id"].ToString()),
                            row["country_description"] == null ? null : row["country_description"].ToString(),
                            row["carrier_id"] == null ? null : row["carrier_id"].ToString(),
                            row["carrier_description"] == null ? null : row["carrier_description"].ToString(),
                            Convert.ToInt64(row["city_id"].ToString()),
                            row["city_description"] == null ? null : row["city_description"].ToString(),
                            Convert.ToInt64(row["status_id"].ToString()),
                            row["status_description"] == null ? null : row["status_description"].ToString(),
                            null,
                            null,
                            null,
                            null));
                    }

                    result.Product = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public ProductResult SearchPin(String id, String currencyId, 
            String countryId, String carrierId, String cityId, String statusId)
        {
            ProductResult result = new ProductResult();
            List<Product> list = new List<Product>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select product_id, description, abbreviation,  ");
                query.Append(" currency_id, currency_description, country_id, ");
                query.Append(" country_description, carrier_id, carrier_description, ");
                query.Append(" city_id, city_description, ");
                query.Append(" status_id, status_description ");
                query.Append(" from ip_vw_product_pin ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "product_id", id, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "currency_id", currencyId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "country_id", countryId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "city_id",cityId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "status_id",statusId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "carrier_id",carrierId, OracleDbType.Int32);


                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new Product(
                            Convert.ToInt64(row["product_id"].ToString()),
                            row["description"] == null ? null : row["description"].ToString(),
                            (row["description"] == null ? null : row["description"].ToString()) + " (" + (row["currency_id"] == null ? null : row["currency_id"].ToString()) + ")",
                            row["abbreviation"] == null ? null : row["abbreviation"].ToString(),
                            null,
                            null,
                            row["currency_id"] == null ? null : row["currency_id"].ToString(),
                            row["currency_description"] == null ? null : row["currency_description"].ToString(),
                            Convert.ToInt64(row["country_id"].ToString()),
                            row["country_description"] == null ? null : row["country_description"].ToString(),
                            row["carrier_id"] == null ? null : row["carrier_id"].ToString(),
                            row["carrier_description"] == null ? null : row["carrier_description"].ToString(),
                            Convert.ToInt64(row["city_id"].ToString()),
                            row["city_description"] == null ? null : row["city_description"].ToString(),
                            Convert.ToInt64(row["status_id"].ToString()),
                            row["status_description"] == null ? null : row["status_description"].ToString(),
                            null,
                            null,
                            null,
                            null));
                    }
                    result.Product = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result Save(String id, String description, String abbreviation, String productTypeId,
            String currencyId, String countryId, String carrierId, String cityId,String statusId, 
            String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("ion_product_id", OracleDbType.Int64, ParameterDirection.InputOutput).Value = Common.Utils.IsNull(id, null);
                cmd.Parameters.Add("iz_description", OracleDbType.Varchar2, 100).Value = description;
                cmd.Parameters.Add("iz_abbreviation", OracleDbType.Varchar2, 20).Value = abbreviation;
                cmd.Parameters.Add("in_product_type_id", OracleDbType.Int64).Value = productTypeId;
                cmd.Parameters.Add("iz_currency_id", OracleDbType.Varchar2, 3).Value = currencyId;
                cmd.Parameters.Add("in_country_id", OracleDbType.Int64).Value = countryId;
                cmd.Parameters.Add("in_carrier_id", OracleDbType.Varchar2,20).Value = carrierId;
                cmd.Parameters.Add("in_city_id", OracleDbType.Int64).Value = Common.Utils.IsNull(cityId, null);
                cmd.Parameters.Add("in_status_id", OracleDbType.Int64).Value = statusId;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int64).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 4000).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_product.save", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (result.Code.Equals("0"))
                {
                    result.Id = cmd.Parameters["ion_product_id"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result Remove(String id, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("ion_product_id", OracleDbType.Int32).Value = id;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_product.remove", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }
    }
}
