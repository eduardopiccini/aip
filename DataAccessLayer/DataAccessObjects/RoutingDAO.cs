﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using Common;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class RoutingResult : Result
    {
        public String LogId { get; set; }
        public List<RoutingProvider> RoutingProvider { get; set; }
    }


    [Serializable]
    public class PinResult : Result
    {
        public String AuthCode { get; set; }
        public String PinSerial { get; set; }
        public String PinNumber { get; set; }
    }


    [Serializable]
    public class RoutingProvider
    {
        public RoutingProvider()
        {
        }

        public RoutingProvider(String routingId, String logId,
            String providerAccountId, String templateId, String weight,
            String profileId, String providerId, String productCode,
            String countryCode, String operatorCode, String productTypeId,
            String ip, String port, String url, String providerOperatorId, String deckProductId,
            String currencyAmount, String rechargeStamp, String localCurrencyAmount, String transactionMode,
            String providerName, String productId, String productName, String voidRoutingId, String terminalId,
            String phone, String transactionTimeout, String providerReference, String secondStopRouting,
            String distributor, String user, String password)
        {
            this.RoutingId = routingId;
            this.LogId = logId;
            this.ProviderAccountId = providerAccountId;
            this.TemplateId = templateId;
            this.Weight = weight;
            this.ProfileId = profileId;
            this.ProviderId = providerId;
            this.ProductCode = productCode;
            this.CountryCode = countryCode;
            this.OperatorCode = operatorCode;
            this.ProductTypeId = productTypeId;
            this.Ip = ip;
            this.Port = port;
            this.Url = url;
            this.ProviderOperatorId = providerOperatorId;
            this.DeckProductId = deckProductId;
            this.CurrencyAmount = currencyAmount;
            this.RechargeStamp = rechargeStamp;
            this.LocalCurrencyAmount = localCurrencyAmount;
            this.TransactionMode = transactionMode;
            this.ProviderName = providerName;
            this.ProductId = productId;
            this.ProductName = productName;
            this.VoidRoutingId = voidRoutingId;
            this.TerminalId = terminalId;
            this.Phone = phone;
            this.TransactionTimeout = transactionTimeout;
            this.ProviderReference = providerReference;
            this.SecondStopRouting = secondStopRouting;
            this.Distributor = distributor;
            this.User = user;
            this.Password = password;
        }

        public String RoutingId { get; set; }
        public String LogId { get; set; }
        public String ProviderAccountId { get; set; }
        public String TemplateId { get; set; }
        public String Weight { get; set; }
        public String ProfileId { get; set; }
        public String ProviderId { get; set; }
        public String ProductCode { get; set; }
        public String CountryCode { get; set; }
        public String OperatorCode { get; set; }
        public String ProductTypeId { get; set; }
        public String Ip { get; set; }
        public String Port { get; set; }
        public String Url { get; set; }
        public String ProviderOperatorId { get; set; }
        public String DeckProductId { get; set; }
        public String CurrencyAmount { get; set; }
        public String LocalCurrencyAmount { get; set; }
        public String RechargeStamp { get; set; }
        public String TransactionMode { get; set; }
        public String ProviderName { get; set; }
        public String ProductId { get; set; }
        public String ProductName { get; set; }
        public String VoidRoutingId { get; set; }
        public String TerminalId { get; set; }
        public String Phone { get; set; }
        public String TransactionTimeout { get; set; }
        public String ProviderReference { get; set; }
        public String SecondStopRouting { get; set; }
        public String Distributor { get; set; }
        public String User { get; set; }
        public String Password { get; set; }
    }



    [Serializable]
    public class RoutingTransactionLogResult
    {
        public String Code { get; set; }
        public String Message { get; set; }
        public String Details { get; set; }
        public String LogProviderId { get; set; }
        public String TransactionId { get; set; }
        public String NewBalance { get; set; }
        public String ProviderNewBalance { get; set; }
    }

    [Serializable]
    public class RoutingTransactionLogTimeoutResult : Result
    {
        public String ProviderReference { get; set; }
        public String TransactionId { get; set; }
        public String ResponseCodeId { get; set; }
        public String ResponseMessage { get; set; }
    }


    public class RoutingDAO : IDisposable
    {

        public RoutingResult Buy(String apiUser, String apiPassword,
            String accountId, String productId,
            String amount, String phone, String reference, String ip, String merchant, String merchanrName, String smartRouting,
            String locale, String appUser)
        {
            List<RoutingProvider> list = new List<RoutingProvider>();
            OracleCommand cmd = new OracleCommand();
            RoutingResult result = new RoutingResult();
            EncryptDAO enc = new EncryptDAO();



            try
            {
                String[] ParameterId = { "ApiUser", "ApiPassword", "AccountId", "ProductId", "Amount", "Phone", "Reference", "Ip", "Merchant", "Locale", "AppUser" };
                String[] ParameterValue = { apiUser, apiPassword, accountId, productId, amount, phone, reference, ip, merchant, locale, appUser };
                
                cmd.Parameters.Add("on_log_id", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("or_routing_provider", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_api_user", OracleDbType.Varchar2, 100).Value = Common.Utils.IsNull(apiUser, null);
                cmd.Parameters.Add("iz_api_password", OracleDbType.Varchar2, 100).Value = Common.Utils.IsNull(enc.Decrypt(apiPassword), null);
                cmd.Parameters.Add("in_account_id", OracleDbType.Int32).Value = Common.Utils.IsNull(accountId, null);
                cmd.Parameters.Add("in_product_id", OracleDbType.Int32).Value = Common.Utils.IsNull(productId, null);
                cmd.Parameters.Add("in_amount", OracleDbType.Decimal).Value = Common.Utils.IsNull(amount, null);
                cmd.Parameters.Add("in_phone", OracleDbType.Int64).Value = Common.Utils.IsNull(phone, null);
                cmd.Parameters.Add("in_reference", OracleDbType.Int64).Value = Common.Utils.IsNull(reference, null);
                cmd.Parameters.Add("iz_ip", OracleDbType.Varchar2, 20).Value = Common.Utils.IsNull(ip, null);
                cmd.Parameters.Add("iz_merchant", OracleDbType.Varchar2, 50).Value = Common.Utils.IsNull(merchant, null);

                cmd.Parameters.Add("ia_parameter_id", OracleDbType.Varchar2);
                cmd.Parameters["ia_parameter_id"].CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                cmd.Parameters["ia_parameter_id"].Value = ParameterId;


                cmd.Parameters.Add("ia_parameter_value", OracleDbType.Varchar2);
                cmd.Parameters["ia_parameter_value"].CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                cmd.Parameters["ia_parameter_value"].Value = ParameterValue;

                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_errormessage", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;
                cmd.Parameters.Add("in_smart_routing", OracleDbType.Int32).Value = smartRouting;

                DataTable dt = DataManager.GetDataTableSP(ref cmd, "ip_pg_routing.buy", "", "", "or_routing_provider");

                if (cmd.Parameters["on_log_id"] != null) result.LogId = cmd.Parameters["on_log_id"].Value.ToString();
                if (cmd.Parameters["on_errorcode"] != null) result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                if (cmd.Parameters["on_errormessage"] != null) result.Message = cmd.Parameters["on_errormessage"].Value.ToString();

                if (result.Code.Equals("0"))
                {

                    foreach (DataRow item in dt.Rows)
                    {
                        list.Add(new RoutingProvider(Common.Utils.IsNull(item["routing_id"], "").ToString(),
                            Common.Utils.IsNull(item["log_id"], "").ToString(),
                            Common.Utils.IsNull(item["provider_account_id"], "").ToString(),
                            Common.Utils.IsNull(item["template_id"], "").ToString(),
                            Common.Utils.IsNull(item["weight"], "").ToString(),
                            Common.Utils.IsNull(item["profile_id"], "").ToString(),
                            Common.Utils.IsNull(item["provider_id"], "").ToString(),
                            Common.Utils.IsNull(item["product_code"], "").ToString(),
                            Common.Utils.IsNull(item["country_code"], "").ToString(),
                            Common.Utils.IsNull(item["operator_code"], "").ToString(),
                            Common.Utils.IsNull(item["product_type_id"], "").ToString(),
                            Common.Utils.IsNull(item["ip"], "").ToString(),
                            Common.Utils.IsNull(item["port"], "").ToString(),
                            Common.Utils.IsNull(item["url"], "").ToString(),
                            Common.Utils.IsNull(item["provider_operator_id"], "").ToString(),
                            Common.Utils.IsNull(item["deck_product_id"], "").ToString(),
                            Common.Utils.IsNull(item["currency_amount"], "").ToString(),
                            Common.Utils.IsNull(item["recharge_stamp"], "").ToString(),
                            Common.Utils.IsNull(item["local_currency_amount"], "").ToString(),
                            Common.Utils.IsNull(item["transaction_mode"], "").ToString(),
                            Common.Utils.IsNull(item["provider_name"], "").ToString(),
                            Common.Utils.IsNull(item["product_id"], "").ToString(),
                            Common.Utils.IsNull(item["product_name"], "").ToString(),
                            Common.Utils.IsNull(item["void_routing_id"], "").ToString(),
                            Common.Utils.IsNull(item["terminal_id"], "").ToString(),
                            Common.Utils.IsNull(item["phone"], "").ToString(),
                            Common.Utils.IsNull(item["second_timeout_transaction"], "0").ToString(),
                            null,
                            Common.Utils.IsNull(item["second_stop_routing"], "").ToString(),
                            null,
                            Common.Utils.IsNull(item["api_user"], "").ToString(),
                            Common.Utils.IsNull(item["api_password"], "").ToString())
                        );
                    }
                    
                    result.RoutingProvider = list;
                }


            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public RoutingResult Void(String transactionId, String apiUser, String apiPassword,
            String accountId, String reference, String ip, String locale, String appUser)
        {
            
            List<RoutingProvider> list = new List<RoutingProvider>();
            OracleCommand cmd = new OracleCommand();
            RoutingResult result = new RoutingResult();
            EncryptDAO enc = new EncryptDAO();

            try
            {
                String[] ParameterId = { "TransactionId", "ApiUser", "ApiPassword", "AccountId", "Reference", "Ip", "Locale", "AppUser" };
                String[] ParameterValue = { transactionId, apiUser, apiPassword, accountId, reference, ip, locale, appUser };

                cmd.Parameters.Add("in_transaction_id", OracleDbType.Int64).Value = transactionId;
                cmd.Parameters.Add("iz_api_user", OracleDbType.Varchar2, 100).Value = Common.Utils.IsNull(apiUser, null);
                cmd.Parameters.Add("iz_api_password", OracleDbType.Varchar2, 100).Value = Common.Utils.IsNull(enc.Decrypt(apiPassword), null);
                cmd.Parameters.Add("in_account_id", OracleDbType.Int32).Value = Common.Utils.IsNull(accountId, null);
                cmd.Parameters.Add("in_reference", OracleDbType.Int64).Value = Common.Utils.IsNull(reference, null);
                cmd.Parameters.Add("iz_ip", OracleDbType.Varchar2, 20).Value = Common.Utils.IsNull(ip, null);

                cmd.Parameters.Add("ia_parameter_id", OracleDbType.Varchar2);
                cmd.Parameters["ia_parameter_id"].CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                cmd.Parameters["ia_parameter_id"].Value = ParameterId;


                cmd.Parameters.Add("ia_parameter_value", OracleDbType.Varchar2);
                cmd.Parameters["ia_parameter_value"].CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                cmd.Parameters["ia_parameter_value"].Value = ParameterValue;

                cmd.Parameters.Add("on_log_id", OracleDbType.Int64).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("or_routing_provider", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_errormessage", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataTable dt = DataManager.GetDataTableSP(ref cmd, "ip_pg_routing.void", "", "", "or_routing_provider");

                if (cmd.Parameters["on_log_id"].Value != null) result.LogId = cmd.Parameters["on_log_id"].Value.ToString();
                if (cmd.Parameters["on_errorcode"].Value != null) result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                if (cmd.Parameters["on_errormessage"].Value != null) result.Message = cmd.Parameters["on_errormessage"].Value.ToString();


                if (result.Code.Equals("0"))
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        list.Add(new RoutingProvider(Common.Utils.IsNull(item["routing_id"], "").ToString(),
                            Common.Utils.IsNull(item["log_id"], "").ToString(),
                            Common.Utils.IsNull(item["provider_account_id"], "").ToString(),
                            Common.Utils.IsNull(item["template_id"], "").ToString(),
                            Common.Utils.IsNull(item["weight"], "").ToString(),
                            Common.Utils.IsNull(item["profile_id"], "").ToString(),
                            Common.Utils.IsNull(item["provider_id"], "").ToString(),
                            Common.Utils.IsNull(item["product_code"], "").ToString(),
                            Common.Utils.IsNull(item["country_code"], "").ToString(),
                            Common.Utils.IsNull(item["operator_code"], "").ToString(),
                            Common.Utils.IsNull(item["product_type_id"], "").ToString(),
                            Common.Utils.IsNull(item["ip"], "").ToString(),
                            Common.Utils.IsNull(item["port"], "").ToString(),
                            Common.Utils.IsNull(item["url"], "").ToString(),
                            Common.Utils.IsNull(item["provider_operator_id"], "").ToString(),
                            Common.Utils.IsNull(item["deck_product_id"], "").ToString(),
                            Common.Utils.IsNull(item["currency_amount"], "").ToString(),
                            Common.Utils.IsNull(item["recharge_stamp"], "").ToString(),
                            Common.Utils.IsNull(item["local_currency_amount"], "").ToString(),
                            Common.Utils.IsNull(item["transaction_mode"], "").ToString(),
                            Common.Utils.IsNull(item["provider_name"], "").ToString(),
                            Common.Utils.IsNull(item["product_id"], "").ToString(),
                            Common.Utils.IsNull(item["product_name"], "").ToString(),
                            Common.Utils.IsNull(item["void_routing_id"], "").ToString(),
                            Common.Utils.IsNull(item["terminal_id"], "").ToString(),
                            Common.Utils.IsNull(item["phone"], "").ToString(),
                            Common.Utils.IsNull(item["second_timeout_transaction"], "0").ToString(),
                            null,
                            Common.Utils.IsNull(item["second_stop_routing"], "").ToString(),
                            null,
                            Common.Utils.IsNull(item["api_user"], "").ToString(),
                            Common.Utils.IsNull(item["api_password"], "").ToString())
                        );
                    }
                    result.RoutingProvider = list;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }



        public RoutingResult VoidByReference(String reference, String apiUser, String apiPassword,
            String accountId, String referenceVoid, String ip, String locale, String appUser)
        {

            List<RoutingProvider> list = new List<RoutingProvider>();
            OracleCommand cmd = new OracleCommand();
            RoutingResult result = new RoutingResult();
            EncryptDAO enc = new EncryptDAO();

            try
            {
                String[] ParameterId = { "Reference", "ApiUser", "ApiPassword", "AccountId", "ReferenceVoid", "Ip", "Locale", "AppUser" };
                String[] ParameterValue = { reference, apiUser, apiPassword, accountId, referenceVoid, ip, locale, appUser };

                cmd.Parameters.Add("in_reference", OracleDbType.Int64).Value = reference;
                cmd.Parameters.Add("iz_api_user", OracleDbType.Varchar2, 100).Value = Common.Utils.IsNull(apiUser, null);
                cmd.Parameters.Add("iz_api_password", OracleDbType.Varchar2, 100).Value = Common.Utils.IsNull(enc.Decrypt(apiPassword), null);
                cmd.Parameters.Add("in_account_id", OracleDbType.Int32).Value = Common.Utils.IsNull(accountId, null);
                cmd.Parameters.Add("in_reference_void", OracleDbType.Int64).Value = Common.Utils.IsNull(referenceVoid, null);
                cmd.Parameters.Add("iz_ip", OracleDbType.Varchar2, 20).Value = Common.Utils.IsNull(ip, null);

                cmd.Parameters.Add("ia_parameter_id", OracleDbType.Varchar2);
                cmd.Parameters["ia_parameter_id"].CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                cmd.Parameters["ia_parameter_id"].Value = ParameterId;


                cmd.Parameters.Add("ia_parameter_value", OracleDbType.Varchar2);
                cmd.Parameters["ia_parameter_value"].CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                cmd.Parameters["ia_parameter_value"].Value = ParameterValue;

                cmd.Parameters.Add("on_log_id", OracleDbType.Int64).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("or_routing_provider", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_response_code_id", OracleDbType.Varchar2, 200).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_response_code_message", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_errormessage", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataTable dt = DataManager.GetDataTableSP(ref cmd, "ip_pg_routing.void_reference", "", "", "or_routing_provider");

                if (cmd.Parameters["on_log_id"].Value != null) result.LogId = cmd.Parameters["on_log_id"].Value.ToString();
                if (cmd.Parameters["on_errorcode"].Value != null) result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                if (cmd.Parameters["on_errormessage"].Value != null) result.Message = cmd.Parameters["on_errormessage"].Value.ToString();


                if (result.Code.Equals("0"))
                {
                    if (dt != null)
                    {
                        foreach (DataRow item in dt.Rows)
                        {
                            list.Add(new RoutingProvider(Common.Utils.IsNull(item["routing_id"], "").ToString(),
                                Common.Utils.IsNull(item["log_id"], "").ToString(),
                                Common.Utils.IsNull(item["provider_account_id"], "").ToString(),
                                Common.Utils.IsNull(item["template_id"], "").ToString(),
                                Common.Utils.IsNull(item["weight"], "").ToString(),
                                Common.Utils.IsNull(item["profile_id"], "").ToString(),
                                Common.Utils.IsNull(item["provider_id"], "").ToString(),
                                Common.Utils.IsNull(item["product_code"], "").ToString(),
                                Common.Utils.IsNull(item["country_code"], "").ToString(),
                                Common.Utils.IsNull(item["operator_code"], "").ToString(),
                                Common.Utils.IsNull(item["product_type_id"], "").ToString(),
                                Common.Utils.IsNull(item["ip"], "").ToString(),
                                Common.Utils.IsNull(item["port"], "").ToString(),
                                Common.Utils.IsNull(item["url"], "").ToString(),
                                Common.Utils.IsNull(item["provider_operator_id"], "").ToString(),
                                Common.Utils.IsNull(item["deck_product_id"], "").ToString(),
                                Common.Utils.IsNull(item["currency_amount"], "").ToString(),
                                Common.Utils.IsNull(item["recharge_stamp"], "").ToString(),
                                Common.Utils.IsNull(item["local_currency_amount"], "").ToString(),
                                Common.Utils.IsNull(item["transaction_mode"], "").ToString(),
                                Common.Utils.IsNull(item["provider_name"], "").ToString(),
                                Common.Utils.IsNull(item["product_id"], "").ToString(),
                                Common.Utils.IsNull(item["product_name"], "").ToString(),
                                Common.Utils.IsNull(item["void_routing_id"], "").ToString(),
                                Common.Utils.IsNull(item["terminal_id"], "").ToString(),
                                Common.Utils.IsNull(item["phone"], "").ToString(),
                                Common.Utils.IsNull(item["second_timeout_transaction"], "0").ToString(),
                                Common.Utils.IsNull(item["provider_reference"], "").ToString(),
                                Common.Utils.IsNull(item["second_stop_routing"], "").ToString(),
                                null,
                                Common.Utils.IsNull(item["api_user"], "").ToString(),
                                Common.Utils.IsNull(item["api_password"], "").ToString())
                            );
                        }
                        result.RoutingProvider = list;
                    }
                    else
                    {
                        if (cmd.Parameters["oz_response_code_id"].Value != null) result.Code = cmd.Parameters["oz_response_code_id"].Value.ToString();
                        if (cmd.Parameters["oz_response_code_message"].Value != null) result.Message = cmd.Parameters["oz_response_code_message"].Value.ToString();
                    }
                    
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public RoutingTransactionLogResult TransLog(String routingId, DateTime? beginDate,
            DateTime? endDate, String responseCodeId, String responseCodeMessage,
            String providerReference, String providerSerial, String providerPin, String locale, String appUser,
            String rechargeStamp)
        {
            OracleCommand cmd = new OracleCommand();
            RoutingTransactionLogResult result = new RoutingTransactionLogResult();

            try
            {
                cmd.Parameters.Add("in_routing_id", OracleDbType.Int64).Value = routingId;
                cmd.Parameters.Add("id_date_begin", OracleDbType.Date).Value = beginDate;
                cmd.Parameters.Add("id_date_end", OracleDbType.Date).Value = endDate;
                cmd.Parameters.Add("iz_response_code_id", OracleDbType.Varchar2, 100).Value = responseCodeId;
                cmd.Parameters.Add("iz_response_message", OracleDbType.Varchar2, 2000).Value = responseCodeMessage;
                cmd.Parameters.Add("iz_provider_reference", OracleDbType.Varchar2, 2000).Value = providerReference;
                cmd.Parameters.Add("iz_provider_serial", OracleDbType.Varchar2, 2000).Value = providerSerial;
                cmd.Parameters.Add("iz_provider_pin", OracleDbType.Varchar2, 2000).Value = providerPin;
                cmd.Parameters.Add("on_log_provider_id", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_transaction_id", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_balance_new", OracleDbType.Decimal).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_provider_balance_new", OracleDbType.Decimal).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 2000).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;
                cmd.Parameters.Add("iz_recharge_stamp", OracleDbType.Varchar2, 100).Value = rechargeStamp;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_routing.trans_log", "", "");

                result.TransactionId = Common.Utils.IsNull(cmd.Parameters["on_transaction_id"].Value, "").ToString();

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (result.Code.Equals("0"))
                {
                    result.LogProviderId = (cmd.Parameters["on_log_provider_id"].Value == null) ? "" : cmd.Parameters["on_log_provider_id"].Value.ToString();
                    result.TransactionId = (cmd.Parameters["on_transaction_id"].Value == null) ? "" : cmd.Parameters["on_transaction_id"].Value.ToString();
                    result.NewBalance = (cmd.Parameters["on_balance_new"].Value == null) ? "" : cmd.Parameters["on_balance_new"].Value.ToString();
                    result.ProviderNewBalance = (cmd.Parameters["on_provider_balance_new"].Value == null) ? "" : cmd.Parameters["on_provider_balance_new"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public RoutingTransactionLogResult TransLogWs(String routingId, String success, DateTime? beginDate,
            DateTime? endDate, String responseCodeId, String responseCodeMessage,
            String providerReference, String providerSerial, String providerPin, String locale, String appUser,
            String rechargeStamp, String errorMessage)
        {
            OracleCommand cmd = new OracleCommand();
            RoutingTransactionLogResult result = new RoutingTransactionLogResult();

            try
            {
                cmd.Parameters.Add("in_routing_id", OracleDbType.Int64).Value = routingId;
                cmd.Parameters.Add("in_success", OracleDbType.Int64).Value = success;
                cmd.Parameters.Add("id_date_begin", OracleDbType.Date).Value = beginDate;
                cmd.Parameters.Add("id_date_end", OracleDbType.Date).Value = endDate;
                cmd.Parameters.Add("iz_response_code_id", OracleDbType.Varchar2, 100).Value = responseCodeId;
                cmd.Parameters.Add("iz_response_message", OracleDbType.Varchar2, 2000).Value = responseCodeMessage;
                cmd.Parameters.Add("iz_provider_reference", OracleDbType.Varchar2, 2000).Value = providerReference;
                cmd.Parameters.Add("iz_provider_serial", OracleDbType.Varchar2, 2000).Value = providerSerial;
                cmd.Parameters.Add("iz_provider_pin", OracleDbType.Varchar2, 2000).Value = providerPin;
                cmd.Parameters.Add("iz_recharge_stamp", OracleDbType.Varchar2, 100).Value = rechargeStamp;
                cmd.Parameters.Add("iz_ws_error_message", OracleDbType.Varchar2, 2000).Value = errorMessage;
                cmd.Parameters.Add("on_record_id", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 2000).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_routing.trans_log_ws", "", "");

                result.TransactionId = Common.Utils.IsNull(cmd.Parameters["on_record_id"].Value, "").ToString();
                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Transaction BuyPin(String accountId, String productId, String denomination)
        {
            OracleCommand cmd = new OracleCommand();
            Transaction result = new Transaction();

            try
            {
                cmd.Parameters.Add("in_account_id", OracleDbType.Int32).Value = accountId;
                cmd.Parameters.Add("in_product_id", OracleDbType.Int32).Value = productId;
                cmd.Parameters.Add("in_denomination", OracleDbType.Int32).Value = denomination;
                cmd.Parameters.Add("on_auth_code", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_pin_serial", OracleDbType.Varchar2, 100).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_pin_number", OracleDbType.Varchar2, 100).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_response_code", OracleDbType.Varchar2, 100).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_response_message", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_local_pin.buy", "", "");

                result.Code = cmd.Parameters["on_response_code"].Value.ToString();
                result.Message = cmd.Parameters["oz_response_message"].Value.ToString();

                if (result.Code.Equals(Common.Utils.APPROVED))
                {
                    result.AuthorizationNumber = (cmd.Parameters["on_auth_code"].Value == null) ? "" : cmd.Parameters["on_auth_code"].Value.ToString();
                    result.SerialNumber = (cmd.Parameters["oz_pin_serial"].Value == null) ? "" : cmd.Parameters["oz_pin_serial"].Value.ToString();
                    result.PinNumber = (cmd.Parameters["oz_pin_number"].Value == null) ? "" : cmd.Parameters["oz_pin_number"].Value.ToString().Replace('\0', ' ').Trim();
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result VoidPin(String accountId, String pinSerial, String authCode)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_account_id", OracleDbType.Int32).Value = accountId;
                cmd.Parameters.Add("iz_pin_serial", OracleDbType.Varchar2, 100).Value = pinSerial;
                cmd.Parameters.Add("in_auth_code", OracleDbType.Varchar2, 100).Value = authCode;
                cmd.Parameters.Add("on_response_code", OracleDbType.Varchar2, 100).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_response_message", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_local_pin.void", "", "");

                result.Code = cmd.Parameters["on_response_code"].Value.ToString();
                result.Message = cmd.Parameters["oz_response_message"].Value.ToString();

                if (result.Code.Equals("0"))
                {
                    result.Code = Common.Utils.APPROVED;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


          //PROCEDURE trans_log_timeout (in_log_id                     IN     ip_tb_api_log.log_id%TYPE,
          //                      oz_provider_reference            OUT ip_tb_api_log.reference%TYPE,
          //                      on_transaction_id                OUT ip_tb_transaction_log.transaction_id%TYPE,
          //                      oz_response_code_id              OUT VARCHAR2,
          //                      oz_response_message              OUT VARCHAR2,
          //                      on_errorcode                     OUT NUMBER,
          //                      oz_errormessage                  OUT VARCHAR2,
          //                      iz_message_locale             IN     ip_tb_locale.locale_id%TYPE DEFAULT NULL,
          //                      iz_app_user                   IN     VARCHAR2 DEFAULT NULL)


        public RoutingTransactionLogTimeoutResult TransLogTimeout(String logId, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            RoutingTransactionLogTimeoutResult result = new RoutingTransactionLogTimeoutResult();

            try
            {
                cmd.Parameters.Add("in_log_id", OracleDbType.Int64).Value = logId;
                cmd.Parameters.Add("oz_provider_reference", OracleDbType.Varchar2, 2000).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_transaction_id", OracleDbType.Int64).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_response_code_id", OracleDbType.Varchar2, 100).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_response_message", OracleDbType.Varchar2, 2000).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 2000).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_routing.trans_log_timeout", "", "");

                result.TransactionId = Common.Utils.IsNull(cmd.Parameters["on_transaction_id"].Value, "").ToString();

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (result.Code.Equals("0"))
                {
                    result.ProviderReference = Common.Utils.IsNull(cmd.Parameters["oz_provider_reference"].Value, "").ToString();
                    result.ResponseCodeId = Common.Utils.IsNull(cmd.Parameters["oz_response_code_id"].Value, "").ToString();
                    result.ResponseMessage = Common.Utils.IsNull(cmd.Parameters["oz_response_message"].Value, "").ToString();
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result Answered(String logId, String clientStatus)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_log_id", OracleDbType.Int64).Value = logId;
                cmd.Parameters.Add("iz_client_status", OracleDbType.Varchar2, 50).Value = clientStatus;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_routing.answered", "", "");

                result.Code = Common.Utils.APPROVED;
                result.Message = Common.Utils.APPROVED_MESSAGE;
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Message = ex.Message;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public void Dispose()
        {
        }
    }
}
