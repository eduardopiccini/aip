﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using Common;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class ApiLogParameterResult : Result
    {
        public List<ApiLogParameter> ApiLogParameter { get; set; }
    }

    public class ApiLogParameterDAO
    {

        public ApiLogParameterResult Search(String id, String logId, DateTime? dateBegin, DateTime? dateEnd)
        {
            ApiLogParameterResult result = new ApiLogParameterResult();
            List<ApiLogParameter> list = new List<ApiLogParameter>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {

                query.Append(" Select log_id, parameter_id, parameter_value,date_created,user_created ");
                query.Append(" from   ip_vw_api_log_parameter ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "parameter_id", id, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "log_id", logId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "date_created", dateBegin, dateEnd, OracleDbType.Date, DataManager.eOperador.Between);


                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {

                        list.Add(new ApiLogParameter { LogId = Common.Utils.IsNull(row["log_id"], "").ToString(), ParameterId = Common.Utils.IsNull(row["parameter_id"], "").ToString(), ParameterValue = Common.Utils.IsNull(row["parameter_value"], "").ToString(), DateCreated = Common.Utils.IsNull(row["date_created"], "").ToString(), UserCreated = Common.Utils.IsNull(row["user_created"], "").ToString() });
                    }

                    result.ApiLogParameter = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }
    }
}
