﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using Common;

namespace DataAccessLayer.DataAccessObjects
{
    public class ErrorDAO
    {


       
        public Result Get_Error(String Procedure,String locale, String id,String Message)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("iz_procedure", OracleDbType.Varchar2).Value = Procedure;
                cmd.Parameters.Add("iz_locale_id", OracleDbType.Varchar2).Value = locale;
                cmd.Parameters.Add("oz_error_code", OracleDbType.Varchar2,10).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_error_message", OracleDbType.Varchar2, 100).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_error_message", OracleDbType.Varchar2, 100).Value = Message;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_error.error_message", "", "");

                if (cmd.Parameters["oz_error_message"].Value != null)
                {
                    result.Code = id;
                    result.Message = cmd.Parameters["oz_error_message"].Value.ToString();
                }

            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result GetErrorMessage(String code, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("ReturnValue", OracleDbType.Varchar2, 2000).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("iz_error_code", OracleDbType.Varchar2).Value = code;
                cmd.Parameters.Add("iz_locale_id", OracleDbType.Varchar2).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 100).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_error.message", "", "");

                if (cmd.Parameters["ReturnValue"].Value != null)
                {
                    result.Code = code;
                    result.Message = cmd.Parameters["ReturnValue"].Value.ToString();
                }

            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result Log(String error, String message, String level, String description, 
            String appUser, String notify)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("on_error_log_id", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_error", OracleDbType.Varchar2, 100).Value = error;
                cmd.Parameters.Add("iz_error_message", OracleDbType.Varchar2, 500).Value = message;
                cmd.Parameters.Add("iz_error_level", OracleDbType.Varchar2, 200).Value = level;
                cmd.Parameters.Add("iz_description", OracleDbType.Varchar2, 500).Value = message;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;
                cmd.Parameters.Add("ib_notify", OracleDbType.Varchar2, 10).Value = notify;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_error.error_log", "", "");

                result.Code = (cmd.Parameters["on_error_log_id"]==null)? null : cmd.Parameters["on_error_log_id"].ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }
    }
}
