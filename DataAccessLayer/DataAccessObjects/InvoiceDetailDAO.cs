﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Common;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class InvoiceDetailResult : Result
    {
        public List<InvoiceDetail> InvoiceDetail { get; set; }
    }

    public class InvoiceDetailDAO
    {
        public InvoiceDetailResult Search(String invoiceId, String productId)
        {
            InvoiceDetailResult result = new InvoiceDetailResult();
            List<InvoiceDetail> list = new List<InvoiceDetail>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select trunc(transaction_date) transaction_date, product_id, product_name, sum(quantity) quantity, transaction_amount, ");
                query.Append(" sum(gross_amount) gross_amount, sum(discount_amount) discount_amount, sum(net_amount) net_amount, sum(surcharge) surcharge ");
                query.Append(" from ip_vw_invoice_detail ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "invoice_id", invoiceId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "product_id", productId, OracleDbType.Int64);
                query.Append(" group by trunc(transaction_date), product_id, product_name, transaction_amount order by trunc(transaction_date), product_id, transaction_amount");
                
                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new InvoiceDetail(Common.Utils.IsNull(row["transaction_date"],null) == null ? (DateTime?)null : Convert.ToDateTime(row["transaction_date"].ToString()),
                            row["product_id"] == null ? null : row["product_id"].ToString(),
                            row["product_name"] == null ? null : row["product_name"].ToString(),
                            Convert.ToDecimal(Common.Utils.IsNull(row["quantity"], 0)),
                            Convert.ToDecimal(Common.Utils.IsNull(row["transaction_amount"], 0)),
                            Convert.ToDecimal(Common.Utils.IsNull(row["gross_amount"], 0)),
                            Convert.ToDecimal(Common.Utils.IsNull(row["discount_amount"], 0)),
                            Convert.ToDecimal(Common.Utils.IsNull(row["net_amount"], 0)),
                            Convert.ToDecimal(Common.Utils.IsNull(row["surcharge"], 0))));
                    }

                    result.InvoiceDetail = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }
    }
}
