﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using Common;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class ApiLogProviderResult : Result
    {
        public List<ApiLogProvider> ApiLogProvider { get; set; }
    }

    public class ApiLogProviderDAO
    {

        public ApiLogProviderResult Search(String id, String logId, String routingId,
            DateTime? dateBeginBegin, DateTime? dateBeginEnd, DateTime? dateEndBegin, DateTime? dateEndEnd,
            String accountId, String deckProductId, String productTypeId, String productId,
            String profileId, String providerId, String templateId, String responseCodeId, String currencyId)
        {
            ApiLogProviderResult result = new ApiLogProviderResult();
            List<ApiLogProvider> list = new List<ApiLogProvider>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select log_provider_id, log_id, routing_id, phone, date_begin, date_end, ");
                query.Append(" provider_id, provider_name, account_id, account_name, product_id, ");
                query.Append(" product_name, amount, discount, discount_amount,surcharge,  ");
                query.Append(" response_message, response_code_id, reference, serial, country_code, ");
                query.Append(" operator_code, product_code, weight, deck_product_id, product_type_id, ");
                query.Append(" profile_id, template_id, product_amount, old_balance, new_balance, currency_id, ");
                query.Append(" currency_rate, currency_amount, ip, port, url, date_created, user_created, ");
                query.Append(" app_user_created, app_user_updated from ip_vw_api_log_provider ");




                DataManager.AddBindParamCmd(ref cmd, ref query, "log_provider_id", id, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "log_id", logId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "routing_id", routingId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "date_begin", dateBeginBegin, dateBeginEnd, OracleDbType.Date, DataManager.eOperador.Between);
                DataManager.AddBindParamCmd(ref cmd, ref query, "date_end", dateEndBegin, dateEndEnd, OracleDbType.Date, DataManager.eOperador.Between);
                DataManager.AddBindParamCmd(ref cmd, ref query, "account_id", accountId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "deck_product_id", deckProductId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "product_id", productId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "profile_id", profileId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "provider_id", providerId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "template_id", templateId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "response_code_id", responseCodeId, OracleDbType.Varchar2);
                DataManager.AddBindParamCmd(ref cmd, ref query, "currency_id", currencyId, OracleDbType.Varchar2);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {

                        list.Add(new ApiLogProvider
                        {
                            Id = Common.Utils.IsNull(row["log_provider_id"], "").ToString(),
                            LogId = Common.Utils.IsNull(row["log_id"], "").ToString(),
                            RoutingId = Common.Utils.IsNull(row["routing_id"], "").ToString(),
                            Phone = Common.Utils.IsNull(row["phone"], "").ToString(),
                            DateBegin = Common.Utils.IsNull(row["date_begin"], "").ToString(),
                            DateEnd = Common.Utils.IsNull(row["date_end"], "").ToString(),
                            AccountId = Common.Utils.IsNull(row["account_id"], "").ToString(),
                            AccountName = Common.Utils.IsNull(row["account_name"], "").ToString(),
                            Amount = Common.Utils.IsNull(row["amount"], "").ToString(),
                            Discount = Common.Utils.IsNull(row["discount"], "").ToString(),
                            DiscountAmount = Common.Utils.IsNull(row["discount_amount"], "").ToString(),
                            Surcharge = Common.Utils.IsNull(row["surcharge"], "").ToString(),
                            DeckProductId = Common.Utils.IsNull(row["deck_product_id"], "").ToString(),
                            ProductTypeId = Common.Utils.IsNull(row["product_type_id"], "").ToString(),
                            ProductId = Common.Utils.IsNull(row["product_id"], "").ToString(),
                            ProductName = Common.Utils.IsNull(row["product_name"], "").ToString(),
                            ProviderId = Common.Utils.IsNull(row["provider_id"], "").ToString(),
                            ProviderName = Common.Utils.IsNull(row["provider_name"], "").ToString(),
                            ProfileId = Common.Utils.IsNull(row["profile_id"], "").ToString(),
                            TemplateId = Common.Utils.IsNull(row["template_id"], "").ToString(),
                            CountryCode = Common.Utils.IsNull(row["country_code"], "").ToString(),
                            OperatorCode = Common.Utils.IsNull(row["operator_code"], "").ToString(),
                            ProductCode = Common.Utils.IsNull(row["product_code"], "").ToString(),
                            Weight = Common.Utils.IsNull(row["weight"], "").ToString(),
                            ProductAmount = Common.Utils.IsNull(row["product_amount"], "").ToString(),
                            ResponseMessage = Common.Utils.IsNull(row["response_message"], "").ToString(),
                            ResponseCodeId = Common.Utils.IsNull(row["response_code_id"], "").ToString(),
                            Reference = Common.Utils.IsNull(row["reference"], "").ToString(),
                            Serial = Common.Utils.IsNull(row["serial"], "").ToString(),
                            OldBalance = Common.Utils.IsNull(row["old_balance"], "").ToString(),
                            NewBalance = Common.Utils.IsNull(row["new_balance"], "").ToString(),
                            CurrencyId = Common.Utils.IsNull(row["currency_id"], "").ToString(),
                            CurrencyRate = Common.Utils.IsNull(row["currency_rate"], "").ToString(),
                            CurrencyAmount = Common.Utils.IsNull(row["currency_amount"], "").ToString(),
                            Ip = Common.Utils.IsNull(row["ip"], "").ToString(),
                            Port = Common.Utils.IsNull(row["port"], "").ToString(),
                            Url = Common.Utils.IsNull(row["url"], "").ToString(),
                            
                        });
                    }

                    result.ApiLogProvider = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }
    }
}
