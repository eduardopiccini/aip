﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using Common;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class CurrencyRateResult : Result
    {
        public List<CurrencyRate> CurrencyRate { get; set; }
    }

    public class CurrencyRateDAO
    {
        /// <summary>
        /// Gets all the currency register in the data base
        /// </summary>
        /// <returns>A list of currency</returns>
        public CurrencyRateResult Search(String currencyId, String outCurrencyId)
        {
            CurrencyRateResult result = new CurrencyRateResult();
            List<CurrencyRate> list = new List<CurrencyRate>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select currency_id, in_currency_name, out_currency_id, out_currency_name, out_rate ");
                query.Append(" from ip_vw_currency_rate ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "currency_id", Common.Utils.IsNull(currencyId, "").ToString().ToUpper(), OracleDbType.Varchar2);
                DataManager.AddBindParamCmd(ref cmd, ref query, "out_currency_id", Common.Utils.IsNull(outCurrencyId, "").ToString().ToUpper(), OracleDbType.Varchar2);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new CurrencyRate(
                            Utils.IsNull(row["currency_id"], "").ToString(),
                            Utils.IsNull(row["in_currency_name"], "").ToString(),
                            Utils.IsNull(row["out_currency_id"], "").ToString(),
                            Utils.IsNull(row["out_currency_name"], "").ToString(),
                            Utils.IsNull(row["out_rate"], "").ToString()));

                    }

                    result.CurrencyRate = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result Add(String currencyId, String outCurrencyId, String outRate,
            String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("iz_currency_id", OracleDbType.Varchar2).Value = currencyId;
                cmd.Parameters.Add("iz_out_currency_id", OracleDbType.Varchar2).Value = outCurrencyId;
                cmd.Parameters.Add("in_rate", OracleDbType.Decimal).Value = (String.IsNullOrEmpty(outRate) ? null : outRate);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_currency.save_currency_rate", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result Remove(String currencyId, String outCurrencyId, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("iz_currency_id", OracleDbType.Varchar2).Value = currencyId;
                cmd.Parameters.Add("iz_out_currency_id", OracleDbType.Varchar2).Value = outCurrencyId;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_currency.remove_currency_rate", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

    }
}
