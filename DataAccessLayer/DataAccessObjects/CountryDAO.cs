﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using Common;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class CountryResult : Result
    {
        public List<Country> Country { get; set; }
    }

    [Serializable]
    public class AccCountryResult : Result
    {
        public List<AccCountry> Country { get; set; }
    }

    [Serializable]
    public class CountryAreaCodeResult : Result
    {
        public List<CountryAreaCode> CountryAreaCode { get; set; }
    }

    public class CountryDAO
    {

        public CountryAreaCodeResult SearchAreaCodes(String areaCode, String countryId)
        {
            CountryAreaCodeResult result = new CountryAreaCodeResult();
            List<CountryAreaCode> list = new List<CountryAreaCode>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select area_code, country_id, country_description ");
                query.Append(" from ip_vw_area_code ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "area_code", areaCode, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "country_id", countryId, OracleDbType.Int32);


                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new CountryAreaCode(
                            Convert.ToInt64(row["area_code"].ToString()),
                            Convert.ToInt64(row["country_id"].ToString()),
                            row["country_description"] == null ? null : row["country_description"].ToString()));
                    }

                    result.CountryAreaCode = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        /// <summary>
        /// Gets all the countries register en the data base
        /// </summary>
        /// <returns>A list of countries</returns>
        public CountryResult Search(String id, String description)
        {
            CountryResult result = new CountryResult();
            List<Country> list = new List<Country>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select country_id, description, iso3166_a2, iso3166_a3, ");
                query.Append(" currency_id, currency_description, country_code ");
                query.Append(" from ip_vw_country ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "country_id",  id, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "description", (description==null) ? null : description.ToUpper(), OracleDbType.Varchar2, DataManager.eOperador.Like_Donde_Encuentre);


                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");


                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new Country(
                            Convert.ToInt64(row["country_id"].ToString()),
                            row["description"] == null ? null : row["description"].ToString(),
                            row["iso3166_a2"] == null ? null : row["iso3166_a2"].ToString(),
                            row["iso3166_a3"] == null ? null : row["iso3166_a3"].ToString(),
                            row["currency_id"] == null ? null : row["currency_id"].ToString(),
                            row["currency_description"] == null ? null : row["currency_description"].ToString(),
                            row["country_code"] == null ? null : row["country_code"].ToString()));
                    }

                    result.Country = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public AccCountryResult SearchAccountCountry(String id, String description)
        {
            AccCountryResult result = new AccCountryResult();
            List<AccCountry> list = new List<AccCountry>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select country_id, description");
                query.Append(" from ip_vw_country ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "country_id", id, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "description", (description == null) ? null : description.ToUpper(), OracleDbType.Varchar2, DataManager.eOperador.Like_Donde_Encuentre);


                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");


                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new AccCountry(
                            Convert.ToInt64(row["country_id"].ToString()),
                            row["description"] == null ? null : row["description"].ToString()));
                    }

                    result.Country = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result Add(String id, String description, String currencyId, String countryCode,
            String iso2, String iso3, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_country_id", OracleDbType.Int32).Value = id;
                cmd.Parameters.Add("iz_description", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(description) ? null : description);
                cmd.Parameters.Add("iz_currency_id", OracleDbType.Varchar2).Value = (String.IsNullOrEmpty(currencyId) ? null : currencyId);
                cmd.Parameters.Add("iz_country_code", OracleDbType.Int32).Value = (String.IsNullOrEmpty(countryCode) ? null : countryCode);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_iso3166_a2", OracleDbType.Varchar2, 20).Value = (String.IsNullOrEmpty(iso2) ? null : iso2);
                cmd.Parameters.Add("iz_iso3166_a3", OracleDbType.Varchar2, 20).Value = (String.IsNullOrEmpty(iso3) ? null : iso3);
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_country.save", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result SaveAreaCode(String countryId, String areaCode, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_area_code", OracleDbType.Int32).Value = areaCode;
                cmd.Parameters.Add("in_country_id", OracleDbType.Int32).Value = countryId;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_country.save_area_code", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result Modify(String id, String description, String currencyId, String countryCode,
            String iso2, String iso3, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_country_id", OracleDbType.Int32).Value = id;
                cmd.Parameters.Add("iz_description", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(description) ? null : description);
                cmd.Parameters.Add("iz_currency_id", OracleDbType.Varchar2).Value = (String.IsNullOrEmpty(currencyId) ? null : currencyId);
                cmd.Parameters.Add("iz_country_code", OracleDbType.Int32).Value = (String.IsNullOrEmpty(countryCode) ? null : countryCode);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_iso3166_a2", OracleDbType.Varchar2, 20).Value = (String.IsNullOrEmpty(iso2) ? null : iso2);
                cmd.Parameters.Add("iz_iso3166_a3", OracleDbType.Varchar2, 20).Value = (String.IsNullOrEmpty(iso3) ? null : iso3);
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_country.change", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result Remove(String id, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_country_id", OracleDbType.Int32).Value = id;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_country.remove", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result RemoveAreaCode(String areaCode, String countryId, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_area_code", OracleDbType.Int32).Value = areaCode;
                cmd.Parameters.Add("in_country_id", OracleDbType.Int32).Value = countryId;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_country.remove", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }
    }
}
