﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using Common;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class ProviderResult : Result
    {
        public List<Provider> Provider { get; set; }
    }

    [Serializable]
    public class ProviderResponseCodeResult : Result
    {
        public List<ProviderResponseCode> ProviderResponseCode { get; set; }
    }

    public class ProviderDAO
    {
        /// <summary>
        /// Get all the api providers.
        /// </summary>
        /// <returns>A list of Api_Provider</returns>
        public ProviderResult Search(String id, String statusId,
            String description)
        {

            ProviderResult result = new ProviderResult();
            List<Provider> list = new List<Provider>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select provider_id, description, abbreviation, void_allowed, status_id, ");
                query.Append(" status_description, nvl(alert_failed_quantity,0)alert_failed_quantity,nvl(alert_failed_transaction,0) alert_failed_transaction, nvl(quantity_failed_transaction,0)quantity_failed_transaction, currency_id, timeout_quantity_failures ");
                query.Append(" from ip_vw_provider ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "provider_id", id, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "status_id", statusId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "description", Common.Utils.IsNull(description,"").ToString().ToUpper(), OracleDbType.Varchar2, DataManager.eOperador.Like_Donde_Encuentre);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new Provider
                        {
                            Id = Convert.ToInt64(row["provider_id"].ToString()),
                            Description = row["description"] == null ? null : row["description"].ToString(),
                            Abbreviation = row["abbreviation"] == null ? null : row["abbreviation"].ToString(),
                            VoidAllowed = Convert.ToInt32(row["void_allowed"].ToString()),
                            StatusId = Convert.ToInt64(row["status_id"].ToString()),
                            StatusDescription = row["status_description"] == null ? null : row["status_description"].ToString(),
                            AlertFailedQuantity = Convert.ToInt32(row["alert_failed_quantity"].ToString()),
                            CurrencyId = row["currency_id"] == null ? null : row["currency_id"].ToString(),
                            QuantityFailedTransaction = Convert.ToInt32(row["timeout_quantity_failures"].ToString()),
                            AlertFailedTransaction = Convert.ToInt32(row["alert_failed_transaction"].ToString())
                        });
                    }
                            
                    result.Provider = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        

        public Result Save(String id, String description, String abbreviation,String voidAllowed,
            String alertFailedTransaction, String alertFailedQuantity, String currencyId, String statusId, String timeoutQuantityFailed, String locale,
            String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("ion_provider_id", OracleDbType.Int64, ParameterDirection.InputOutput).Value = (String.IsNullOrEmpty(id) ? null : id);
                cmd.Parameters.Add("iz_description", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(description) ? null : description);
                cmd.Parameters.Add("iz_abbreviation", OracleDbType.Varchar2, 20).Value = (String.IsNullOrEmpty(abbreviation) ? null : abbreviation);
                cmd.Parameters.Add("in_void_allowed", OracleDbType.Int16).Value = Common.Utils.IsNull(voidAllowed, "0");
                cmd.Parameters.Add("in_alert_failed_transaction", OracleDbType.Int32).Value = Common.Utils.IsNull(alertFailedTransaction, "0");
                cmd.Parameters.Add("in_alert_failed_quantity", OracleDbType.Int32).Value = Common.Utils.IsNull(alertFailedQuantity, "0");
                cmd.Parameters.Add("iz_currency_id", OracleDbType.Varchar2, 50).Value = (String.IsNullOrEmpty(currencyId) ? null : currencyId);
                cmd.Parameters.Add("iz_status_id", OracleDbType.Int64).Value = (String.IsNullOrEmpty(statusId) ? null : statusId);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;
                cmd.Parameters.Add("in_timeout_quantity_failures", OracleDbType.Int32).Value = Common.Utils.IsNull(timeoutQuantityFailed, null);
                

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_provider.save", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (result.Code.Equals("0"))
                {
                    result.Id = cmd.Parameters["ion_provider_id"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        

        public Result Remove(String id, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_provider_id", OracleDbType.Int32).Value = id;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_provider.remove", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public ProviderResponseCodeResult SearchResponseCode(String providerId, String responseCodeId,
            String responseCodeDescription)
        {
            List<ProviderResponseCode> list = new List<ProviderResponseCode>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;
            ProviderResponseCodeResult result = new ProviderResponseCodeResult();

            try
            {
                query.Append(" select provider_id, provider_description, response_code, response_code_description, abbreviation ");
                query.Append(" from ip_vw_provider_response_code ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "provider_id", providerId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "response_code", responseCodeId, OracleDbType.Varchar2);
                DataManager.AddBindParamCmd(ref cmd, ref query, "response_code_description", Common.Utils.IsNull(responseCodeDescription, "").ToString().ToUpper(), OracleDbType.Varchar2, DataManager.eOperador.Like_Donde_Encuentre);


                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new ProviderResponseCode(
                            Convert.ToInt64(row["provider_id"].ToString()),
                            row["provider_description"] == null ? null : row["provider_description"].ToString(),
                            row["response_code"] == null ? null : row["response_code"].ToString(),
                            row["response_code_description"] == null ? null : row["response_code_description"].ToString(),
                            row["abbreviation"] == null ? null : row["abbreviation"].ToString()));
                    }

                    result.ProviderResponseCode = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }



        public Result SaveResponseCode(String providerId, String responseCodeId, String description, String abbreviation,
            String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("ion_provider_id", OracleDbType.Int32).Value = providerId;
                cmd.Parameters.Add("iz_provider_respose_code_id", OracleDbType.Varchar2, 100).Value = responseCodeId;
                cmd.Parameters.Add("iz_description", OracleDbType.Varchar2, 100).Value = description;
                cmd.Parameters.Add("iz_abbreviation", OracleDbType.Varchar2, 20).Value = abbreviation;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_provider.save_resp_code", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();


                if (result.Code.Equals("0"))
                {
                    result.Id = cmd.Parameters["ion_provider_id"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result RemoveReponseCode(String providerId, String responseCodeId, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_provider_id", OracleDbType.Int32).Value = providerId;
                cmd.Parameters.Add("iz_provider_respose_code_id", OracleDbType.Varchar2, 100).Value = responseCodeId;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_provider.remove_resp_code", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }
        
    }
}
