﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using Common;
using OracleAccessLayer;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class PaymentResult : Result
    {
        public List<Payment> Payment { get; set; }
    }



    public class PaymentDAO
    {



        public PaymentResult SearchDestination(String AccountId, DateTime? DateBegin, DateTime? DateEnd,String DirectionId)
        {
            PaymentResult result = new PaymentResult();
            List<Payment> list = new List<Payment>();
            StringBuilder query = new StringBuilder();
            DataTable dt;
            OracleCommand cmd = new OracleCommand();

            try
            {
                //query.Append(" select balance_log_id, date_created, account_id, account_name, transaction_amount, debit_name, note ");
                //query.Append(" from ip_vw_balance_log where transaction_id is null");
                query.Append(" select  balance_log_id, account_id, account_name, direction_id, direction_name, balance, transaction_amount, ");
                query.Append(" debit, debit_name, transaction_id, old_balance, note, swift_code, aba_number, bank_transaction_type_name, date_created, user_created, date_updated,  ");
                query.Append(" user_updated, app_user_created, app_user_updated ");

                query.Append(" from ip_vw_balance_log where transaction_id is null");
                DataManager.AddBindParamCmd(ref cmd, ref query, "direction_id", Utils.ConvertToInt64(DirectionId,1), OracleDbType.Int64);
                //DataManager.AddBindParamCmd(ref cmd, ref query, "account_id", AccountId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "date_created", DateBegin, DateEnd, OracleDbType.Date, DataManager.eOperador.Between);
                if (AccountId.Trim() !="") DataManager.AddCondSQL(ref query, " (account_id = " + AccountId + " or parent_account_id=" + AccountId + ")");

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        DateTime date;
                        DateTime.TryParse(row["date_created"].ToString(), out date);

                        list.Add(new Payment
                        {
                            TransactionId = Utils.ConvertToInt64(row["transaction_id"]),
                            Balance = Utils.ConvertToDecimal(row["balance"], 0),
                            DirectionId = row["direction_id"].ToString(),
                            DirectionName = row["direction_name"].ToString(),
                            Debit = row["debit"].ToString(),
                            Date = date,
                            AccountId = row["account_id"].ToString(),
                            AccountName = row["account_name"].ToString(),
                            BalanceLogId = row["balance_log_id"].ToString(),
                            DebitName = row["debit_name"].ToString(),
                            Note = row["note"].ToString(),
                            TransactionAmount = Utils.ConvertToDecimal(row["transaction_amount"], 0),
                            AbaNumber = Utils.ConvertToInt64(row["aba_number"], DBNull.Value),
                            BankTransactionTypeName = row["bank_transaction_type_name"].ToString(),
                            SwiftCode = row["swift_code"].ToString(),
                            User = row["user_created"].ToString(),
                        });

                    }

                    result.Payment = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }

            return result;
        }



    }
}
