﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using Common;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class StateResult : Result
    {
        public List<State> State { get; set; }
    }

    public class StateDAO
    {
        /// <summary>
        /// Gets the states by the country id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A list of states</returns>
        public StateResult Search(String id, String countryId,
            String description)
        {
            List<State> list = new List<State>();
            StateResult result = new StateResult();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select state_id, description, abbreviation, country_id, country_description ");
                query.Append(" from ip_vw_state ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "state_id", id, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "country_id", countryId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "description", Common.Utils.IsNull(description,"").ToString().ToUpper() , OracleDbType.Varchar2, DataManager.eOperador.Like_Donde_Encuentre);
                
                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {

                        list.Add(new State { 
                            Id                  =   Common.Utils.IsNull(row["state_id"], "").ToString(), 
                            Description         =   Common.Utils.IsNull(row["description"], "").ToString(),
                            Abbreviation        =   Common.Utils.IsNull(row["abbreviation"],"").ToString(), 
                            CountryId           =   Convert.ToInt64(row["country_id"].ToString()), 
                            CountryDescription  =   Common.Utils.IsNull(row["country_description"],"").ToString()});
                    }

                    result.State = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        
        public Result Add(String id, String countryId, String description, String abbreviation,
            String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_state_id", OracleDbType.Int32).Value     = id;
                cmd.Parameters.Add("in_country_id", OracleDbType.Int32).Value   = Common.Utils.IsNull(countryId, DBNull.Value);
                cmd.Parameters.Add("iz_description", OracleDbType.Varchar2, 100).Value  = Common.Utils.IsNull(description,DBNull.Value);
                cmd.Parameters.Add("iz_abbreviation", OracleDbType.Varchar2, 20).Value  = Common.Utils.IsNull(abbreviation,DBNull.Value);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction        = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = Common.Utils.IsNull(locale, null);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_state.save", "", "");

                result.Code =  cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result Modify(String id, String countryId, String description, String abbreviation,
            String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_state_id", OracleDbType.Int32).Value = id;
                cmd.Parameters.Add("in_country_id", OracleDbType.Int32).Value = Common.Utils.IsNull(countryId, DBNull.Value);
                cmd.Parameters.Add("iz_description", OracleDbType.Varchar2, 100).Value = Common.Utils.IsNull(description, DBNull.Value);
                cmd.Parameters.Add("iz_abbreviation", OracleDbType.Varchar2, 20).Value = Common.Utils.IsNull(abbreviation, DBNull.Value);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = Common.Utils.IsNull(locale, null);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_state.change", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result Remove(String id, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_state_id", OracleDbType.Int32).Value = id;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_state.remove", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }
    }
}
