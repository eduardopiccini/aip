﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using Common;
using System.Reflection;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class AccountResult : Result
    {
        public List<Account> Account { get; set; }
    }

    [Serializable]
    public class AccountContactResult : Result
    {
        public List<AccountContact> AccountContact { get; set; }
    }


    [Serializable]
    public class AccountsProductResult : Result
    {
        public List<AccProduct> AccountProducts { get; set; }
    }

    public class AccountDAO
    {


        public AccountResult SearchAccount(String id, String parent_id, String name, 
            String telephone, String zipCode, String countryId, 
            String billingTermId, String currencyId, String provider_id, 
            String statusId, String stateId, String vendorGroupId, bool vendorGroupOnly,
            String[] accountTypeId)
        {
            AccountResult result = new AccountResult();
            List<Account> item = new List<Account>();
            StringBuilder query = new StringBuilder();
            StringBuilder inVar = new StringBuilder();
            
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select account_id, parent_account_id,parent_account_name, name, abbreviation, address, ");
                query.Append(" balance, credit_limit, low_balance_alert, fax, ");
                query.Append(" telephone, zip_code, note, country_id, country_description, ");
                query.Append(" billing_term_id, billing_description, currency_id, ");
                query.Append(" currency_description, provider_description, provider_balance, provider_credit_limit, ");
                query.Append(" state_id, state_description, status_account_id, status_description, ");
                query.Append(" low_balance_alert_amount, low_balance_alert_interval, account_type_id, account_type_description, ");
                query.Append(" low_balance_provider_amount, ");
                query.Append(" date_created, user_created, date_updated, user_updated, vendor_group_id, vendor_group_description, ");
                query.Append(" last_payment_date, last_payment_amount, last_payment_transaction_type, logo,second_timeout_transaction ");
                query.Append(" from ip_vw_account ");

                if (accountTypeId != null)
                {
                    if (accountTypeId.Length > 0)
                    {
                        for (int i = 0; i < accountTypeId.Length; i++)
                        {
                            if (!String.IsNullOrEmpty(accountTypeId[i]))
                            {
                                inVar.Append(",");
                                inVar.Append(accountTypeId[i]);
                            }
                        }

                        if (!String.IsNullOrEmpty(inVar.ToString()))
                        {
                            query.Append(" WHERE account_type_id in (");
                            query.Append(inVar.ToString().Substring(1));
                            query.Append(") ");
                        }
                    }
                }

                DataManager.AddBindParamCmd(ref cmd, ref query, "account_id", id, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "parent_account_id", parent_id, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "name", Common.Utils.IsNull(name, "").ToString().ToUpper(), OracleDbType.Varchar2, DataManager.eOperador.Like_Donde_Encuentre);
                DataManager.AddBindParamCmd(ref cmd, ref query, "telephone", telephone, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "zip_code", zipCode, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "country_id", countryId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "billing_term_id", billingTermId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "currency_id", currencyId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "provider_id", provider_id, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "status_account_id", statusId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "state_id", stateId, OracleDbType.Int32);


                
                
                DataManager.AddBindParamCmd(ref cmd, ref query, "vendor_group_id", vendorGroupId, OracleDbType.Int32);

                if (vendorGroupOnly)
                {
                    query.Append(" where vendor_group_id is not null ");
                }

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        
                        DateTime date;
                        DateTime.TryParse(row["last_payment_date"].ToString(), out date);
                        
                        item.Add(new Account(
                            Convert.ToInt32(row["account_id"].ToString()),
                            Convert.ToInt32((Common.Utils.IsNull(row["parent_account_id"], 0))),
                           row["parent_account_name"] == null ? null : row["parent_account_name"].ToString(),
                           row["name"] == null ? null : row["name"].ToString(),
                           row["abbreviation"] == null ? null : row["abbreviation"].ToString(),
                           row["address"] == null ? null : row["address"].ToString(),
                           Convert.ToDecimal(row["balance"].ToString()),
                           Convert.ToDecimal(row["credit_limit"].ToString()),
                           Convert.ToInt32(row["low_balance_alert"].ToString()),
                           row["fax"] == null ? null : row["fax"].ToString(),
                           row["telephone"] == null ? null : row["telephone"].ToString(),
                           row["zip_code"] == null ? null : row["zip_code"].ToString(),
                           row["note"] == null ? null : row["note"].ToString(),
                           Convert.ToInt32(row["country_id"].ToString()),
                           row["country_description"] == null ? null : row["country_description"].ToString(),
                           Convert.ToInt32(row["billing_term_id"]),
                           row["billing_description"] == null ? null : row["billing_description"].ToString(),
                           row["currency_id"] == null ? null : row["currency_id"].ToString(),
                           row["currency_description"] == null ? null : row["currency_description"].ToString(),
                           row["vendor_group_id"] == null ? null : row["vendor_group_id"].ToString(),
                           row["vendor_group_description"] == null ? null : row["vendor_group_description"].ToString(),
                           Convert.ToDecimal(row["provider_balance"]),
                           Convert.ToDecimal(row["provider_credit_limit"]),
                           row["state_id"] == null ? null : row["state_id"].ToString(),
                           row["state_description"] == null ? null : row["state_description"].ToString(),
                           Convert.ToInt32(row["status_account_id"].ToString()),
                           row["status_description"] == null ? null : row["status_description"].ToString(),
                           row["low_balance_alert_amount"] == null ? null : row["low_balance_alert_amount"].ToString(),
                           row["low_balance_alert_interval"] == null ? null : row["low_balance_alert_interval"].ToString(),
                           row["low_balance_provider_amount"] == null ? null : row["low_balance_provider_amount"].ToString(),
                           row["account_type_id"] == null ? null : row["account_type_id"].ToString(),
                           row["account_type_description"] == null ? null : row["account_type_description"].ToString(),
                           date,
                           Convert.ToDecimal(Utils.IsNull(row["last_payment_amount"], 0)), row["last_payment_transaction_type"].ToString(),
                           row["logo"].ToString(), row["second_timeout_transaction"].ToString() 
                           ));   
                    }

                    result.Account = item;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public AccountsProductResult SearchProducts(String accountId, String deckProductId, 
            String deckId, String productTypeId, String productId, String countryId,
            String carrierId, String currencyId, String cityId, String statusId, String directionId)
        {
            AccountsProductResult result = new AccountsProductResult();
            List<AccProduct> item = new List<AccProduct>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;
            Int32 total = 0;

            try
            {
                query.Append(" select product_id, description, amount, max_amount, product_type_id, ");
                query.Append("carrier_id, country_id, city_id, currency_id, direction_id ");
                query.Append(" from ip_vw_deck_product ");

                
                DataManager.AddBindParamCmd(ref cmd, ref query, "account_id", accountId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "deck_product_id", deckProductId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "deck_id", deckId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "product_type_id", productTypeId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "product_id", productId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "country_id", countryId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "carrier_id", carrierId, OracleDbType.Varchar2);
                DataManager.AddBindParamCmd(ref cmd, ref query, "currency_id", currencyId, OracleDbType.Varchar2);
                DataManager.AddBindParamCmd(ref cmd, ref query, "city_id", cityId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "status_id", statusId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "direction_id", directionId, OracleDbType.Int64);

                query.Append("order by product_id asc ");

                cmd.CommandText = query.ToString();
                cmd.CommandTimeout = 0;
                cmd.Connection =  new OracleConnection(DataManager.ConnStr("", ""));
                cmd.Connection.Open();
                OracleDataReader reader =  cmd.ExecuteReader();

                

                if (reader.HasRows)
                {
                    reader.FetchSize = cmd.RowSize * 100;

                    while (reader.Read())
                    {
                        item.Add(new AccProduct(
                            reader[0] == null ? null : reader[0].ToString(),
                            reader[1] == null ? null : reader[1].ToString(),
                            reader[2] == null ? null : reader[2].ToString(),
                            reader[3] == null ? null : reader[3].ToString(),
                            reader[4] == null ? null : reader[4].ToString(),
                            reader[5] == null ? null : reader[5].ToString(),
                            reader[6] == null ? null : reader[6].ToString(),
                            reader[7] == null ? null : reader[7].ToString(),
                            reader[8] == null ? null : reader[8].ToString()));
                    }
                    result.AccountProducts = item;
                    result.Code = "0";
                    result.Message = "TRUE";

                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }

                

                

                cmd.Connection.Close();

                //dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                //if (dt != null && dt.Rows.Count > 0)
                //{
                //    foreach (DataRow row in dt.Rows)
                //    {
                //        item.Add(new AccountProducts(
                //            row["account_id"] == null ? null : row["account_id"].ToString(),
                //            row["account_description"] == null ? null : row["account_description"].ToString(),
                //            row["deck_product_id"] == null ? null : row["deck_product_id"].ToString(),
                //            row["deck_product_description"] == null ? null : row["deck_product_description"].ToString(),
                //            row["abbreviation"] == null ? null : row["abbreviation"].ToString(),
                //            row["deck_id"] == null ? null : row["deck_id"].ToString(),
                //            row["deck_description"] == null ? null : row["deck_description"].ToString(),
                //            row["product_type_id"] == null ? null : row["product_type_id"].ToString(),
                //            row["product_type_description"] == null ? null : row["product_type_description"].ToString(),
                //            row["product_id"] == null ? null : row["product_id"].ToString(),
                //            row["product_description"] == null ? null : row["product_description"].ToString(),
                //            row["country_id"] == null ? null : row["country_id"].ToString(),
                //            row["country_description"] == null ? null : row["country_description"].ToString(),
                //            row["carrier_id"] == null ? null : row["carrier_id"].ToString(),
                //            row["carrier_description"] == null ? null : row["carrier_description"].ToString(),
                //            row["currency_id"] == null ? null : row["currency_id"].ToString(),
                //            row["currency_description"] == null ? null : row["currency_description"].ToString(),
                //            row["city_id"] == null ? null : row["city_id"].ToString(),
                //            row["city_description"] == null ? null : row["city_description"].ToString(),
                //            row["status_id"] == null ? null : row["status_id"].ToString(),
                //            row["status_description"] == null ? null : row["status_description"].ToString(),
                //            row["amount"] == null ? null : row["amount"].ToString(),
                //            row["discount"] == null ? null : row["discount"].ToString()));
                //    }
                    
                //    result.AccountProducts = item;
                //    result.Code = "0";
                //    result.Message = "TRUE";
                //}
                //else
                //{
                //    result.Code = Common.Utils.NO_RECORDS_FOUND;
                //}
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    if (cmd.Connection != null)
                    {
                        if (cmd.Connection.State == ConnectionState.Open)
                        {
                            cmd.Connection.Close();
                        }
                    }
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result SaveAccount(String id, String parentAccountId, String name, String abbreviation, String accountStatusId,
            String countryId, String billingTermId, String currencyId, String vendorGroupId, String address, String typeId,
            String lowBalanceAlert, String stateId, String fax, String telephone, String zipCode, String note, 
            String lowBalanceAlertAmount, String lowBalanceAlertInterval, String lowBalanceProviderAmount, String secondTimeoutTransaction, String locale,
            String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_account_id", OracleDbType.Int32, ParameterDirection.InputOutput).Value = Common.Utils.IsNull(id,null);
                cmd.Parameters.Add("in_parent_account_id", OracleDbType.Int32).Value = Common.Utils.IsNull(parentAccountId,null);
                cmd.Parameters.Add("iz_name", OracleDbType.Varchar2, 100).Value = Common.Utils.IsNull(name,null);
                cmd.Parameters.Add("iz_abbreviation", OracleDbType.Varchar2, 20).Value = Common.Utils.IsNull(abbreviation,null);
                cmd.Parameters.Add("in_status_account_id", OracleDbType.Int32).Value = Common.Utils.IsNull(accountStatusId,null);
                cmd.Parameters.Add("in_country_id", OracleDbType.Int32).Value = Common.Utils.IsNull(countryId,null);
                cmd.Parameters.Add("in_billing_term_id", OracleDbType.Int32).Value = Common.Utils.IsNull(billingTermId,null);
                cmd.Parameters.Add("iz_currency_id", OracleDbType.Varchar2).Value = Common.Utils.IsNull(currencyId,null);
                cmd.Parameters.Add("in_vendor_group_id", OracleDbType.Int32).Value = Common.Utils.IsNull(vendorGroupId, null);
                cmd.Parameters.Add("iz_address", OracleDbType.Varchar2, 100).Value = Common.Utils.IsNull(address,null);
                cmd.Parameters.Add("in_account_type_id", OracleDbType.Int32).Value = Common.Utils.IsNull(typeId, null);
                cmd.Parameters.Add("in_low_balance_alert", OracleDbType.Int32).Value = Common.Utils.IsNull(lowBalanceAlert, null);
                cmd.Parameters.Add("in_low_balance_alert_amount", OracleDbType.Int32).Value = Common.Utils.IsNull(lowBalanceAlertAmount, null);
                cmd.Parameters.Add("in_low_balance_alert_interval", OracleDbType.Int32).Value = Common.Utils.IsNull(lowBalanceAlertInterval, null);
                cmd.Parameters.Add("in_low_balance_provider_amount", OracleDbType.Int32).Value = Common.Utils.IsNull(lowBalanceProviderAmount, null);
                cmd.Parameters.Add("iz_state_id", OracleDbType.Int32).Value = Common.Utils.IsNull(stateId,null);
                cmd.Parameters.Add("in_fax", OracleDbType.Int64).Value = Common.Utils.IsNull(fax,null);
                cmd.Parameters.Add("in_telephone", OracleDbType.Int64).Value = Common.Utils.IsNull(telephone,null);
                cmd.Parameters.Add("in_zip_code", OracleDbType.Int32).Value = Common.Utils.IsNull(zipCode,null);
                cmd.Parameters.Add("iz_note", OracleDbType.Varchar2, 500).Value = Common.Utils.IsNull(note,null);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = Common.Utils.IsNull(locale,null);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;
                cmd.Parameters.Add("in_second_timeout_transaction", OracleDbType.Int32).Value = Common.Utils.IsNull(secondTimeoutTransaction, null);
                     

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_account.save_account", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (result.Code.Equals("0"))
                {
                    result.Id = cmd.Parameters["in_account_id"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result RemoveAccount(String id, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_account_id", OracleDbType.Int32).Value = (String.IsNullOrEmpty(id) ? null : id);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_account.remove_account", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }

            return result;
        }

        public AccCountryResult SearchCountry(String accountId)
        {
            AccCountryResult result = new AccCountryResult();
            List<AccCountry> list = new List<AccCountry>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select country_id, description ");
                query.Append(" from ip_vw_api_country  ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "account_id", accountId, OracleDbType.Int64);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");


                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new AccCountry(
                            Convert.ToInt64(row["country_id"].ToString()),
                            row["description"] == null ? null : row["description"].ToString()));
                    }

                    result.Country = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public CityResult SearchCity(String accountId, String countryId)
        {
            CityResult result = new CityResult();
            List<City> list = new List<City>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select city_id, description, country_id ");
                query.Append(" from ip_vw_api_city  ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "account_id", (accountId == null) ? null : accountId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "country_id", (countryId == null) ? null : countryId, OracleDbType.Int64);
                
                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new City(
                            Convert.ToInt64(row["city_id"].ToString()),
                            row["description"] == null ? null : row["description"].ToString(),
                            null,
                            null,
                            null,
                            null,
                            null));
                    }

                    result.City = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public CarrierResult SearchCarrier(String accountId)
        {
            CarrierResult result = new CarrierResult();
            List<Carrier> list = new List<Carrier>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select carrier_id, description ");
                query.Append(" from ip_vw_api_carrier ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "account_id", accountId, OracleDbType.Int64);
                
                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new Carrier(
                            row["carrier_id"] == null ? null : row["carrier_id"].ToString(),
                            row["description"] == null ? null : row["description"].ToString(),
                            null,
                            1,
                            null));
                    }

                    result.Carrier = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public NewBalanceResult UpdateAccountBalance(String AccountId, String transactionAmount, String transactionId, String Type,
            String note, String locale, String appUser,String BankTransactionTypeId,String SwiftCode, String AbaNumber)
        {
            OracleCommand cmd = new OracleCommand();
            NewBalanceResult result = new NewBalanceResult();

            try
            {
                cmd.Parameters.Add("in_operation", OracleDbType.Int32).Value = Type;
                cmd.Parameters.Add("in_account_id", OracleDbType.Int32).Value = AccountId;
                cmd.Parameters.Add("in_transaction_amount", OracleDbType.Decimal).Value = Common.Utils.ConvertToDecimal(transactionAmount, DBNull.Value);
                cmd.Parameters.Add("in_transaction_id", OracleDbType.Int32).Value =Common.Utils.IsNull(transactionId,null);
                cmd.Parameters.Add("iz_note", OracleDbType.Varchar2, 100).Value = note;
                cmd.Parameters.Add("on_old_balance", OracleDbType.Decimal).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_balance_new", OracleDbType.Decimal).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;
                cmd.Parameters.Add("in_bank_transaction_type_id", OracleDbType.Int64).Value = Utils.ConvertToInt64(BankTransactionTypeId, DBNull.Value);
                cmd.Parameters.Add("iz_swift_code", OracleDbType.Varchar2).Value = SwiftCode;
                cmd.Parameters.Add("in_aba_number", OracleDbType.Int64).Value = Utils.ConvertToInt64(AbaNumber, DBNull.Value);
                DataManager.RunDataCommandSP(ref cmd, "ip_pg_account.change_account_balance", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
                result.Balance = Convert.ToDecimal(cmd.Parameters["on_balance_new"].Value.ToString());
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result UpdateAccountCreditLimit(String id, String creditLimit, String note, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_account_id", OracleDbType.Int32).Value = id;
                cmd.Parameters.Add("in_credit_limite_new", OracleDbType.Decimal).Value = creditLimit;
                cmd.Parameters.Add("iz_note", OracleDbType.Varchar2, 100).Value = note;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_account.change_account_credit_limit", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public NewBalanceResult UpdateProviderBalance(String AccountId, String transactionAmount, String transactionId, String Type,
             String note, String locale, String appUser, String BankTransactionTypeId,String SwiftCode, String AbaNumber)
        {
            OracleCommand cmd = new OracleCommand();
            NewBalanceResult result = new NewBalanceResult();

            try
            {
                cmd.Parameters.Add("in_operation", OracleDbType.Int32).Value = Type;
                cmd.Parameters.Add("in_account_id", OracleDbType.Int32).Value = AccountId;
                cmd.Parameters.Add("in_transaction_amount", OracleDbType.Decimal).Value = transactionAmount;
                cmd.Parameters.Add("in_transaction_id", OracleDbType.Int32).Value = Common.Utils.IsNull(transactionId, null);
                cmd.Parameters.Add("iz_note", OracleDbType.Varchar2, 100).Value = note;
                cmd.Parameters.Add("on_old_balance", OracleDbType.Decimal).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_balance_new", OracleDbType.Decimal).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;
                cmd.Parameters.Add("in_bank_transaction_type_id", OracleDbType.Int64).Value = Utils.ConvertToInt64 (BankTransactionTypeId, DBNull.Value);
                cmd.Parameters.Add("iz_swift_code", OracleDbType.Varchar2).Value = SwiftCode;
                cmd.Parameters.Add("in_aba_number", OracleDbType.Int64).Value = Utils.ConvertToInt64 (AbaNumber, DBNull.Value);
                
                DataManager.RunDataCommandSP(ref cmd, "ip_pg_account.change_provider_balance", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
                result.Balance = Convert.ToDecimal(cmd.Parameters["on_balance_new"].Value.ToString());
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result UpdateProviderCreditLimit(String id, String creditLimit, String note, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_account_id", OracleDbType.Int32).Value = id;
                cmd.Parameters.Add("in_credit_limite_new", OracleDbType.Decimal).Value = creditLimit;
                cmd.Parameters.Add("iz_note", OracleDbType.Varchar2, 100).Value = note;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_account.change_provider_credit_limit", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public AccountContactResult SearchContact(String id, String accountId, String statusId,
            String name, String email, String telephone, String mobile,
            String department, String job, String billing_notification)
        {
            AccountContactResult result = new AccountContactResult();
            List<AccountContact> list = new List<AccountContact>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select contact_id, account_id, name, status_id, ");
                query.Append(" status_description, email, telephone, mobile, ");
                query.Append(" department, job, billing_notification, ");
                query.Append(" date_created, user_created, date_updated, user_updated ");
                query.Append(" from ip_vw_account_contact ");


                DataManager.AddBindParamCmd(ref cmd, ref query, "account_id", (String.IsNullOrEmpty(accountId) ? null : accountId), OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "contact_id", (String.IsNullOrEmpty(id) ? null : id), OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "status_id", (String.IsNullOrEmpty(statusId) ? null : statusId), OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "name", (String.IsNullOrEmpty(name) ? null : name), OracleDbType.Varchar2, DataManager.eOperador.Like_Donde_Encuentre);
                DataManager.AddBindParamCmd(ref cmd, ref query, "email", (String.IsNullOrEmpty(email) ? null : email), OracleDbType.Varchar2, DataManager.eOperador.Like_Donde_Encuentre);
                DataManager.AddBindParamCmd(ref cmd, ref query, "telephone", (String.IsNullOrEmpty(telephone) ? null : telephone), OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "mobile", (String.IsNullOrEmpty(mobile) ? null : mobile), OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "department", (String.IsNullOrEmpty(department) ? null : department), OracleDbType.Varchar2, DataManager.eOperador.Like_Donde_Encuentre);
                DataManager.AddBindParamCmd(ref cmd, ref query, "job", (String.IsNullOrEmpty(job) ? null : job), OracleDbType.Varchar2, DataManager.eOperador.Like_Donde_Encuentre);
                DataManager.AddBindParamCmd(ref cmd, ref query, "billing_notification", (String.IsNullOrEmpty(billing_notification) ? null : billing_notification), OracleDbType.Int32);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new AccountContact(
                            Convert.ToInt64(row["contact_id"].ToString()),
                            Convert.ToInt64(row["account_id"].ToString()),
                            row["name"] == null ? null : row["name"].ToString(),
                            Convert.ToInt64(row["status_id"].ToString()),
                            row["status_description"] == null ? null : row["status_description"].ToString(),
                            row["email"] == null ? null : row["email"].ToString(),
                            Convert.ToInt64(Common.Utils.IsNull(row["telephone"], 0)),
                            Convert.ToInt64(Common.Utils.IsNull(row["mobile"], 0)),
                            row["department"] == null ? null : row["department"].ToString(),
                            row["job"] == null ? null : row["job"].ToString(),
                            row["billing_notification"] == null ? null : row["billing_notification"].ToString()));
                    }
                    result.AccountContact = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result SaveContact(String id, String accountId, String name, String statusId, String email,
            String telephone, String mobile, String department, String job, String billingNotification,
            String updateNotification, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_contact_id", OracleDbType.Int64, ParameterDirection.InputOutput).Value = Common.Utils.IsNull(id, null);
                cmd.Parameters.Add("in_account_id", OracleDbType.Int64).Value = accountId;
                cmd.Parameters.Add("iz_name", OracleDbType.Varchar2, 100).Value = name;
                cmd.Parameters.Add("in_status_id", OracleDbType.Int64).Value = statusId;
                cmd.Parameters.Add("iz_email", OracleDbType.Varchar2).Value = email;
                cmd.Parameters.Add("in_telephone", OracleDbType.Int64).Value = Common.Utils.IsNull(telephone, null);
                cmd.Parameters.Add("in_mobile", OracleDbType.Int64).Value = Common.Utils.IsNull(mobile, null);
                cmd.Parameters.Add("iz_department", OracleDbType.Varchar2, 50).Value = department;
                cmd.Parameters.Add("iz_job", OracleDbType.Varchar2, 50).Value = Common.Utils.IsNull(job, null);
                cmd.Parameters.Add("in_billing_notification", OracleDbType.Int32).Value = Common.Utils.IsNull(billingNotification, null);
                cmd.Parameters.Add("in_update_notification", OracleDbType.Int32).Value = Common.Utils.IsNull(updateNotification, null);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_account.save_contact", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (result.Code.Equals("0"))
                {
                    result.Id = cmd.Parameters["in_contact_id"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result RemoveContact(String id, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("pcontact_id", OracleDbType.Int32, ParameterDirection.InputOutput).Value = (String.IsNullOrEmpty(id) ? null : id);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_account.remove_contact", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (String.IsNullOrEmpty(id) && result.Code.Equals("0"))
                {
                    result.Id = cmd.Parameters["pcontact_id"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }
     
    }
}
