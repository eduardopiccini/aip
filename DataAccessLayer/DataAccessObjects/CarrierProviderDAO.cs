﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using Common;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class CarrierProviderResult : Result
    {
        public List<CarrierProvider> CarrierProvider { get; set; }
    }

    public class CarrierProviderDAO
    {

        /// <summary>
        /// Gets all the carriers register in the data base
        /// </summary>
        /// <returns>A list of carriers</returns>
        public CarrierProviderResult Search(String id, String carrierId, 
            String providerId, String description)
        {
            CarrierProviderResult result = new CarrierProviderResult();
            List<CarrierProvider> list = new List<CarrierProvider>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select carrier_provider_id, description, carrier_id, carrier_description, ");
                query.Append(" provider_id, provider_description ");
                query.Append(" from ip_vw_carrier_provider ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "carrier_provider_id", id, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "carrier_id", carrierId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "provider_id",providerId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "description",  description.ToUpper(), OracleDbType.Varchar2, DataManager.eOperador.Like_Donde_Encuentre);


                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new CarrierProvider(
                            Convert.ToInt64(row["carrier_provider_id"].ToString()),
                            row["description"] == null ? null : row["description"].ToString(),
                            row["carrier_id"] == null ? null : row["carrier_id"].ToString(),
                            row["carrier_description"] == null ? null : row["carrier_description"].ToString(),
                            row["provider_id"] == null ? null : row["provider_id"].ToString(),
                            row["provider_description"] == null ? null : row["provider_description"].ToString()));
                    }

                    result.CarrierProvider = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result Add(String id, String description, String providerId, String carrierId, String locale)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("iz_carrier_provider_id", OracleDbType.Int32).Value = id;
                cmd.Parameters.Add("iz_description", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(description) ? null : description);
                cmd.Parameters.Add("in_provider_id", OracleDbType.Int32).Value = (String.IsNullOrEmpty(providerId) ? null : providerId);
                cmd.Parameters.Add("in_carrier_id", OracleDbType.Int32).Value = (String.IsNullOrEmpty(carrierId) ? null : carrierId);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_carrier_provider.save", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result Change(String providerId, String description, String carrierId, 
            String operatorId, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_provider_id", OracleDbType.Int32).Value = providerId;
                cmd.Parameters.Add("in_carrier_id", OracleDbType.Int32).Value = carrierId;
                cmd.Parameters.Add("iz_operator_code", OracleDbType.Varchar2, 100).Value = operatorId;
                cmd.Parameters.Add("iz_description", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(description) ? null : description);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_carrier_provider.change", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result Remove(String providerId, String carrierId, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_provider_id", OracleDbType.Int32).Value = providerId;
                cmd.Parameters.Add("in_carrier_id", OracleDbType.Int32).Value = carrierId;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_carrier_provider.remove", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }
    }
}
