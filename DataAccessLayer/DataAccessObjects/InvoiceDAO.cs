﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Common;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class InvoiceResult : Result
    {
        public List<Invoice> Invoice { get; set; }
    }

    public class InvoiceDAO
    {
        public InvoiceResult Search(String id, String accountId,
            DateTime? billingBeginDateBegin, DateTime? billingBeginDateEnd,
            DateTime? billingEndDateBegin, DateTime? billingEndDateEnd,
            String invoiceStatusId, DateTime? datePaidBegin, DateTime? datePaidEnd,
            DateTime? invoiceDateBegin, DateTime? invoiceDateEnd)
        {
            InvoiceResult result = new InvoiceResult();
            List<Invoice> list = new List<Invoice>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select invoice_id, account_id, account_name, address, fax, telephone, zip_code, invoice_date, billing_term_id, ");
                query.Append(" billing_term_name, currency_id, currency_name, direction_id, direction_name, ");
                query.Append(" amount, discount_amount, surcharge, balance, previous_balance, billing_begin_date, billing_end_date, ");
                query.Append(" invoice_status_id, status_name, date_paid, app_user_paid, note_paid ");
                query.Append(" from ip_vw_invoice ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "invoice_id", id, OracleDbType.Int64);
                //DataManager.AddBindParamCmd(ref cmd, ref query, "account_id", accountId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "billing_begin_date", billingBeginDateBegin, billingBeginDateEnd, OracleDbType.Date, DataManager.eOperador.Between);
                DataManager.AddBindParamCmd(ref cmd, ref query, "billing_end_date", billingEndDateBegin, billingEndDateEnd, OracleDbType.Date, DataManager.eOperador.Between);
                DataManager.AddBindParamCmd(ref cmd, ref query, "invoice_status_id", invoiceStatusId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "date_paid", datePaidBegin, datePaidEnd, OracleDbType.Date, DataManager.eOperador.Between);
                DataManager.AddBindParamCmd(ref cmd, ref query, "invoice_date", invoiceDateBegin, invoiceDateEnd, OracleDbType.Date, DataManager.eOperador.Between);
                if (accountId.Trim() !="") DataManager.AddCondSQL(ref query, " (account_id = " + accountId + " or parent_account_id=" + accountId + ")");


                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new Invoice(
                            row["invoice_id"] == null ? null : row["invoice_id"].ToString(),
                            row["account_id"] == null ? null : row["account_id"].ToString(),
                            row["account_name"].ToString(),
                            row["address"] == null ? null : row["address"].ToString(),
                            row["fax"] == null ? null : row["fax"].ToString(),
                            row["zip_code"] == null ? null : row["zip_code"].ToString(),
                            row["telephone"] == null ? null : row["telephone"].ToString(),
                            Common.Utils.IsNull(row["invoice_date"],null) == null ? (DateTime?)null : Convert.ToDateTime(row["invoice_date"].ToString()),
                            row["billing_term_id"] == null ? null : row["billing_term_id"].ToString(),
                            row["billing_term_name"] == null ? null : row["billing_term_name"].ToString(),
                            row["currency_id"] == null ? null : row["currency_id"].ToString(),
                            row["currency_name"] == null ? null : row["currency_name"].ToString(),
                            row["direction_id"] == null ? null : row["direction_id"].ToString(),
                            row["direction_name"] == null ? null : row["direction_name"].ToString(),
                            Convert.ToDecimal(Common.Utils.IsNull(row["amount"], 0)),
                            Convert.ToDecimal(Common.Utils.IsNull(row["discount_amount"], 0)),
                            Convert.ToDecimal(Common.Utils.IsNull(row["surcharge"], 0)),
                            Convert.ToDecimal(Common.Utils.IsNull(row["balance"], 0)),
                            Convert.ToDecimal(Common.Utils.IsNull(row["previous_balance"], 0)),
                            Common.Utils.IsNull(row["billing_begin_date"],null) == null ? (DateTime?)null : Convert.ToDateTime(row["billing_begin_date"].ToString()),
                            Common.Utils.IsNull(row["billing_end_date"],null) == null ? (DateTime?)null : Convert.ToDateTime(row["billing_end_date"].ToString()),
                            row["invoice_status_id"] == null ? null : row["invoice_status_id"].ToString(),
                            row["status_name"] == null ? null : row["status_name"].ToString(),
                            Common.Utils.IsNull(row["date_paid"], null)==null ? (DateTime?)null : Convert.ToDateTime(row["date_paid"].ToString()),
                            row["app_user_paid"] == null ? null : row["app_user_paid"].ToString(),
                            row["note_paid"] == null ? null : row["note_paid"].ToString()));
                    }

                    result.Invoice = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }
        
                          
        
        public Result Save(String accountId, String invoiceDate, String billingTermId, String currencyId,
            String directionId, String amount, String discountAmount, String surcharge, String billingBeginDate,
            String billingEndDate, String html,
            String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("on_invoice_id", OracleDbType.Int64, ParameterDirection.Output).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("in_account_id", OracleDbType.Int64).Value = (String.IsNullOrEmpty(accountId) ? null : accountId);
                cmd.Parameters.Add("id_invoice_date", OracleDbType.Date).Value = (String.IsNullOrEmpty(invoiceDate) ? null : invoiceDate);
                cmd.Parameters.Add("in_billing_term_id", OracleDbType.Int32).Value = (String.IsNullOrEmpty(billingTermId) ? null : billingTermId);
                cmd.Parameters.Add("iz_currency_id", OracleDbType.Varchar2, 20).Value = (String.IsNullOrEmpty(currencyId) ? null : currencyId);
                cmd.Parameters.Add("in_direction_id", OracleDbType.Int32).Value = (String.IsNullOrEmpty(directionId) ? null : directionId);
                cmd.Parameters.Add("in_amount", OracleDbType.Decimal).Value = (String.IsNullOrEmpty(amount) ? null : amount);
                cmd.Parameters.Add("in_discount_amount", OracleDbType.Decimal).Value = (String.IsNullOrEmpty(discountAmount) ? null : discountAmount);
                cmd.Parameters.Add("in_surcharge", OracleDbType.Decimal).Value = (String.IsNullOrEmpty(surcharge) ? null : surcharge);
                cmd.Parameters.Add("id_billing_begin_date", OracleDbType.Date).Value = (String.IsNullOrEmpty(billingBeginDate) ? null : billingBeginDate);
                cmd.Parameters.Add("id_billing_end_date", OracleDbType.Date).Value = (String.IsNullOrEmpty(billingEndDate) ? null : billingEndDate);
                cmd.Parameters.Add("iz_html", OracleDbType.Varchar2, 1000).Value = (String.IsNullOrEmpty(html) ? null : html);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_invoice.save_invoice", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (result.Code.Equals("0"))
                {
                    result.Id = cmd.Parameters["on_invoice_id"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result Paid(String id, String statusId, String note, 
            String appUser, String locale)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_invoice_id", OracleDbType.Int64).Value = id;
                cmd.Parameters.Add("in_invoice_status_id", OracleDbType.Int64).Value = statusId;
                cmd.Parameters.Add("iz_note_paid", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(note) ? null : note);
                cmd.Parameters.Add("iz_app_user_paid", OracleDbType.Varchar2, 50).Value = appUser;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 50).Value = locale;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_invoice.paid_invoice", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (result.Code.Equals("0"))
                {
                    result.Id = cmd.Parameters["on_invoice_id"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }
    }
}
