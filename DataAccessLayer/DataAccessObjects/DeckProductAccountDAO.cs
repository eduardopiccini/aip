﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using Common;


namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class DeckProductAccountResult : Result
    {
        public List<DeckProductAccout> DeckProductAccount { get; set; }
    }

    public class DeckProductAccountDAO
    {
        /// <summary>
        /// Gets the list of cities register in the data base by the country id
        /// </summary>
        /// <param name="country_id"></param>
        /// <returns>A list of cities of the country specified</returns>
        public DeckProductAccountResult Search(String productId, String minAmount, String maxAmount)
        {
            DeckProductAccountResult result = new DeckProductAccountResult();
            List<DeckProductAccout> list = new List<DeckProductAccout>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {

                query.Append("select account_id, account_name, direction_id, direction_name, deck_product_id, description, ");
                query.Append("product_type_id, product_type_name, product_id, product_name, currency_id,country_id, country_name, ");
                query.Append("carrier_id, carrier_name, currency_id, currency_name, city_id, city_name, status_id, ");
                query.Append("status_name, amount, max_amount, discount, discount_amount, surcharge, margin, margin_amount, ");
                query.Append(" routing_mode, routing_mode_name ");
                query.Append("from ip_vw_account_product ");
                
                DataManager.AddBindParamCmd(ref cmd, ref query, "product_id", productId, OracleDbType.Varchar2);
                query.Append("and ((amount >= :min and max_amount <= :max) or (:min2 between amount and max_amount and :max2 between  amount and max_amount ))");
                cmd.Parameters.Add(":min", OracleDbType.Decimal, Utils.ConvertToDecimal(minAmount), ParameterDirection.Input);
                cmd.Parameters.Add(":max", OracleDbType.Decimal, Utils.ConvertToDecimal(maxAmount), ParameterDirection.Input);
                cmd.Parameters.Add(":min2", OracleDbType.Decimal, Utils.ConvertToDecimal(minAmount), ParameterDirection.Input);
                cmd.Parameters.Add(":max2", OracleDbType.Decimal, Utils.ConvertToDecimal(maxAmount), ParameterDirection.Input);

                
                

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new DeckProductAccout
                        {
                            AccountId = Utils.ConvertToInt64(row["account_id"]),
                            AccountName = row["account_name"].ToString(),
                            DirectionId = Utils.ConvertToInt64(row["direction_id"]),
                            DirectionName = row["direction_name"].ToString(),
                            DeckProductId = Utils.ConvertToInt64(row["deck_product_id"]),
                            Description = row["description"].ToString(),
                            StatusId = Utils.ConvertToInt64(row["status_id"]),
                            StatusName = row["status_name"].ToString(),
                            ProductId = Utils.ConvertToInt64(row["product_id"]),
                            ProductName = row["product_name"].ToString(),
                            RoutingMode = Utils.ConvertToInt64(row["routing_mode"]),
                            RoutingModeName = row["routing_mode_name"].ToString(),
                            CarrierId = Utils.ConvertToInt64(row["carrier_id"]),
                            CarrierName = row["carrier_name"].ToString(),
                            CityId = Utils.ConvertToInt64(row["city_id"]),
                            CityName = row["city_name"].ToString(),
                            CountryId = Utils.ConvertToInt64(row["country_id"]),
                            CountryName = row["country_name"].ToString(),
                            CurrencyId = row["currency_id"].ToString(),
                            CurrencyName = row["currency_name"].ToString(),
                            Amount=Utils.ConvertToDecimal(row["amount"]),
                            MaxAmount = Utils.ConvertToDecimal(row["max_amount"]),
                            Discount = Utils.ConvertToDecimal(row["discount"]),
                            DiscountAmount = Utils.ConvertToDecimal(row["discount_amount"]),
                            ProductTypeId = Utils.ConvertToInt64(row["product_type_id"]),
                            Surcharge = Utils.ConvertToDecimal(row["surcharge"]),
                            Margin = Utils.ConvertToDecimal(row["margin"]),
                            MarginAmount = Utils.ConvertToDecimal(row["margin_amount"]),
                            ProductTypeName = row["product_type_name"].ToString()
                            
                            
                        });
                    }

                    result.DeckProductAccount = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result Save(String deckProductId,String productId, String accountId, String amount, String maxAmount, String discount, 
                           String surcharge, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("ion_deck_product_id", OracleDbType.Int64, ParameterDirection.InputOutput).Value = Utils.ConvertToInt64(deckProductId, null);
                cmd.Parameters.Add("in_product_id", OracleDbType.Int64).Value = Utils.ConvertToInt64(productId, null);
                cmd.Parameters.Add("in_account_id", OracleDbType.Int64).Value = Utils.ConvertToInt64(accountId, null);
                cmd.Parameters.Add("in_amount", OracleDbType.Decimal).Value = Utils.ConvertToDecimal(amount, null);
                cmd.Parameters.Add("in_max_amount", OracleDbType.Decimal).Value = Utils.ConvertToDecimal(maxAmount, null);
                cmd.Parameters.Add("in_discount", OracleDbType.Decimal).Value = Utils.ConvertToDecimal(discount, null);
                cmd.Parameters.Add("in_surcharge", OracleDbType.Decimal).Value = Utils.ConvertToDecimal(surcharge, null);
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = Utils.IsNull(locale, null);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_provider.save_account_product", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
                result.Id = cmd.Parameters["ion_deck_product_id"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


    }
}
