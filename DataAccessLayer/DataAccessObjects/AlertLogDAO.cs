﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using Common;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class LogResult : Result
    {
        public String Id {get; set;}
    }

    [Serializable]
    public class AlertLogResult : Result
    {
        public List<AlertLog> AlertLog { get; set; }
    }

    public class AlertLogDAO
    {




        /// <summary>
        /// Gets the alert log by the alert id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A list of AlertLogs</returns>
        public AlertLogResult Search(String id, String alertId,
            DateTime? createdBegin, DateTime? createEnd, String userCreated)
        {
            AlertLogResult result = new AlertLogResult();
            List<AlertLog> list = new List<AlertLog>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select alert_log_id, alert_id, alert_description, message, ");
                query.Append(" date_created, user_created");
                query.Append(" from ip_vw_alert_log ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "alert_log_id", (String.IsNullOrEmpty(id) ? null : id), OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "alert_id", (String.IsNullOrEmpty(alertId) ? null : alertId), OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "date_created", createdBegin, createEnd, OracleDbType.Date, DataManager.eOperador.Between);
                DataManager.AddBindParamCmd(ref cmd, ref query, "user_created", userCreated, OracleDbType.Varchar2);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new AlertLog(
                            Convert.ToInt64(row["alert_log_id"].ToString()),
                            Convert.ToInt64(row["alert_id"].ToString()),
                            row["alert_description"] == null ? null : row["alert_description"].ToString(),
                            row["message"] == null ? null : row["message"].ToString(),
                            row["date_created"] == null ? null : row["date_created"].ToString(),
                            row["user_created"] == null ? null : row["user_created"].ToString()));
                    }

                    result.AlertLog = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }

            return result;
        }


        
        /// <summary>
        /// Saves an alert on the data base.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="alertid"></param>
        /// <returns>Returns 1 if the alert log was register and 0 if not</returns>
        public LogResult Save(String alertId, String message, String description, String locale,
            String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            LogResult result = new LogResult();

            try
            {
                cmd.Parameters.Add("in_alert_log_id", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("in_alert_id", OracleDbType.Int32).Value = (String.IsNullOrEmpty(alertId) ? null : alertId);
                cmd.Parameters.Add("iz_message", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(message) ? null : message);
                cmd.Parameters.Add("iz_description", OracleDbType.Varchar2, 20).Value = (String.IsNullOrEmpty(description) ? null : description);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_alert_log.save", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (result.Code.Equals("0"))
                {
                    result.Id = cmd.Parameters["in_alert_log_id"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result Remove(String id, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_alert_log_id", OracleDbType.Int32).Value = id;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_alert_log.remove", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }
    }
}
