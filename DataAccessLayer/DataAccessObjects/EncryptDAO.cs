﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;

namespace DataAccessLayer.DataAccessObjects
{
    public class EncryptDAO : IDisposable
    {
        
        public String Encrypt(String value)
        {
            OracleCommand cmd = new OracleCommand();
            String response = null;

            try
            {

                if (String.IsNullOrEmpty(value))
                {
                    throw new Exception("Value to encrypt cannot be null or empty");
                }

                cmd.Parameters.Add("returns", OracleDbType.Varchar2, 2000).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("iz_value", OracleDbType.Varchar2, 2000).Value = value;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_encrypt.encrypt", "", "");

                response = cmd.Parameters["returns"].Value.ToString();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return response;
        }

        

        public String Decrypt(String encryptedValue)
        {
            OracleCommand cmd = new OracleCommand();
            String response = null;

            try
            {
                if (String.IsNullOrEmpty(encryptedValue))
                {
                    throw new Exception("Value to decrypt cannot be null or empty");
                }

                cmd.Parameters.Add("returns", OracleDbType.Varchar2, 2000).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("iz_encrypted_value", OracleDbType.Varchar2, 2000).Value = encryptedValue;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_encrypt.decrypt", "", "");

                response = cmd.Parameters["returns"].Value.ToString();


                if (!String.IsNullOrEmpty(response))
                {
                    response = response.Replace('\0', ' ').Trim();
                }
            }
            catch (Exception)
            {
                throw;
            }

            return response;
        }

        #region IDisposable Members

        public void Dispose()
        {

        }

        #endregion
    }
}
