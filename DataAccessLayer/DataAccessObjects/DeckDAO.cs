﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using Common;

namespace DataAccessLayer.DataAccessObjects
{


    [Serializable]
    public class DeckResult : Result
    {
        public List<Deck> Deck { get; set; }
    }

    [Serializable]
    public class DeckProductResult : Result
    {
        public List<DeckProduct> DeckProduct { get; set; }
    }

    [Serializable]
    public class DeckTemplateResult : Result
    {
        public List<DeckTemplate> Deck { get; set; }
    }

    [Serializable]
    public class DeckCountResult : Result
    {
        public Int64 Count { get; set; }
    }

    public class DeckDAO
    {
        /// <summary>
        /// Gets the decks by the account id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A list of decks</returns>
        public DeckResult SearchDeck(String id, String accountId, String directionId,
            String statusId, String name, String description)
        {
            DeckResult result = new DeckResult();
            List<Deck> list = new List<Deck>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select deck_id, account_id, name, direction_id, direction_description, ");
                query.Append(" description, abbreviation, status_id, status_description, date_created, ");
                query.Append(" user_created, date_updated, user_updated ");
                query.Append(" from ip_vw_deck ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "deck_id", id, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "account_id", accountId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "direction_id", directionId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "description", Common.Utils.IsNull(description, "").ToString().ToUpper(), OracleDbType.Varchar2, DataManager.eOperador.Like_Donde_Encuentre);
                DataManager.AddBindParamCmd(ref cmd, ref query, "name", name.ToUpper(), OracleDbType.Varchar2, DataManager.eOperador.Like_Donde_Encuentre);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new Deck(
                            Convert.ToInt64(row["deck_id"].ToString()),
                            Convert.ToInt64(row["account_id"].ToString()),
                            row["name"] == null ? null : row["name"].ToString(),
                            Convert.ToInt64(row["direction_id"].ToString()),
                            row["direction_description"] == null ? null : row["direction_description"].ToString(),
                            row["description"] == null ? null : row["description"].ToString(),
                            row["abbreviation"] == null ? null : row["abbreviation"].ToString(),
                            Convert.ToInt64(row["status_id"].ToString()),
                            row["status_description"] == null ? null : row["status_description"].ToString()));
                    }

                    result.Deck = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }



        public Result SaveDeck(String accountId, String directionId,
            String description, String abbreviation, String statusId, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_direction_id", OracleDbType.Int32).Value = (String.IsNullOrEmpty(directionId) ? null : directionId);
                cmd.Parameters.Add("in_account_id", OracleDbType.Int32).Value = (String.IsNullOrEmpty(accountId) ? null : accountId);
                cmd.Parameters.Add("iz_description", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(description) ? null : description);
                cmd.Parameters.Add("iz_abbreviation", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(abbreviation) ? null : abbreviation);
                cmd.Parameters.Add("in_status_id", OracleDbType.Int32).Value = (String.IsNullOrEmpty(statusId) ? null : statusId);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_deck.save_deck", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();


                if (result.Code.Equals("0"))
                {
                    result.Id = cmd.Parameters["ion_deck_id"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result RemoveDeck(String deckId, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_deck_id", OracleDbType.Int32).Value = (String.IsNullOrEmpty(deckId) ? null : deckId);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_deck.remove_deck", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public DeckProductResult SearchDeckProduct(String id, String accountId, String DirectionId, String deckId, String productTypeId,
            String productId, String ProductDescription, String countryId, String carrierId, String currencyId,
            String cityid, String statusId, int startRowIndex, int maxRows)
        {
            DeckProductResult result = new DeckProductResult();
            List<DeckProduct> list = new List<DeckProduct>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select * from (select t.*, ROWNUM as rn from (  select account_id, deck_product_id, description, abbreviation, deck_id, deck_description, ");
                query.Append(" product_type_id, product_type_description, product_id, routing_mode, product_description, ");
                query.Append(" country_id, surcharge, country_description, carrier_id, carrier_description, ");
                query.Append(" currency_id, currency_description, amount, max_amount, discount, city_id, ");
                query.Append(" city_description, status_id, status_description, send_provider ");
                query.Append(" from ip_vw_deck_product ");


                DataManager.AddBindParamCmd(ref cmd, ref query, "account_id", accountId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "direction_id", DirectionId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "deck_product_id", id, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "deck_id", deckId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "product_type_id", productTypeId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "product_id", productId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "product_description", ProductDescription.ToUpper(), OracleDbType.NVarchar2, DataManager.eOperador.Like_Donde_Encuentre);
                DataManager.AddBindParamCmd(ref cmd, ref query, "country_id", countryId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "carrier_id", carrierId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "currency_id", currencyId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "city_id", cityid, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "status_id", statusId, OracleDbType.Int64);
                query.Append(" order by product_description desc) t) ");
                query.Append(" where rn >= " + startRowIndex + " and rownum <=" + maxRows.ToString());

                OracleConnection cn = new OracleConnection(DataManager.ConnStr("", ""));

                cmd.Connection = cn;
                cmd.CommandText = query.ToString();
                cmd.Connection.Open();
                OracleDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        list.Add(new DeckProduct
                        {
                            DeckId = Convert.ToInt64(dr["deck_id"].ToString()),
                            AccountId = Common.Utils.IsNull(dr["account_id"], "").ToString(),
                            Description = Common.Utils.IsNull(dr["description"], "").ToString(),
                            Abbreviation = Common.Utils.IsNull(dr["abbreviation"], "").ToString(),
                            Id = Convert.ToInt64(dr["deck_product_id"].ToString()),
                            DeckDescription = Common.Utils.IsNull(dr["deck_description"], "").ToString(),
                            ProductTypeId = Convert.ToInt64(dr["product_type_id"].ToString()),
                            ProductTypeDescription = Common.Utils.IsNull(dr["product_type_description"], "").ToString(),
                            ProductId = Convert.ToInt64(dr["product_id"].ToString()),
                            RoutingMode = Common.Utils.IsNull(dr["routing_mode"], "").ToString(),
                            Surcharge = Common.Utils.IsNull(dr["surcharge"], "").ToString(),
                            ProductDescription = Common.Utils.IsNull(dr["product_description"], "").ToString(),
                            CountryId = Convert.ToInt64(dr["country_id"].ToString()),
                            CountryDescription = Common.Utils.IsNull(dr["country_description"], "").ToString(),
                            CarrierId = Common.Utils.IsNull(dr["carrier_id"], "").ToString(),
                            CarrierDescription = Common.Utils.IsNull(dr["carrier_description"], "").ToString(),
                            CurrencyId = Common.Utils.IsNull(dr["currency_id"], "").ToString(),
                            CurrencyDescription = Common.Utils.IsNull(dr["currency_description"], "").ToString(),
                            Amount = Convert.ToDecimal(Common.Utils.IsNull(dr["amount"], 0)),
                            MaxAmount = Convert.ToDecimal(Common.Utils.IsNull(dr["max_amount"], 0)),
                            Discount = Convert.ToDecimal(Common.Utils.IsNull(dr["discount"], 0)),
                            CityId = Common.Utils.IsNull(dr["city_id"], "").ToString(),
                            CityDescription = Common.Utils.IsNull(dr["city_description"], "").ToString(),
                            StatusId = Convert.ToInt64(dr["status_id"].ToString()),
                            StatusDescription = Common.Utils.IsNull(dr["status_description"], "").ToString(),
                            SendProvider = dr["send_provider"].ToString()
                        });

                    }


                    result.DeckProduct = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
                if (cmd.Connection.State == ConnectionState.Open) cmd.Connection.Close();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    if (cmd.Connection != null)
                    {
                        if (cmd.Connection.State == ConnectionState.Open)
                        {
                            cmd.Connection.Close();
                        }
                    }
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Int64? SearchDeckProductCount(String id, String accountId, String deckId, String productTypeId,
            String productId, String ProductDescription, String countryId, String carrierId, String currencyId,
            String cityid, String statusId)
        {
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();

            //tiempo.Append("Llego al Select :" + DateTime.Now.ToString("hh:mm:ss tt") + " \n");
            query.Append(" select count(1) ");
            query.Append(" from ip_vw_deck_product ");

            DataManager.AddBindParamCmd(ref cmd, ref query, "account_id", accountId, OracleDbType.Int64);
            DataManager.AddBindParamCmd(ref cmd, ref query, "deck_product_id", id, OracleDbType.Int64);
            DataManager.AddBindParamCmd(ref cmd, ref query, "deck_id", deckId, OracleDbType.Int64);
            DataManager.AddBindParamCmd(ref cmd, ref query, "product_type_id", productTypeId, OracleDbType.Int64);
            DataManager.AddBindParamCmd(ref cmd, ref query, "product_id", productId, OracleDbType.Int64);
            DataManager.AddBindParamCmd(ref cmd, ref query, "product_description", ProductDescription.ToUpper(), OracleDbType.NVarchar2, DataManager.eOperador.Like_Donde_Encuentre);
            DataManager.AddBindParamCmd(ref cmd, ref query, "country_id", countryId, OracleDbType.Int64);
            DataManager.AddBindParamCmd(ref cmd, ref query, "carrier_id", carrierId, OracleDbType.Int64);
            DataManager.AddBindParamCmd(ref cmd, ref query, "currency_id", currencyId, OracleDbType.Int64);
            DataManager.AddBindParamCmd(ref cmd, ref query, "city_id", cityid, OracleDbType.Int64);
            DataManager.AddBindParamCmd(ref cmd, ref query, "status_id", statusId, OracleDbType.Int64);

            return Common.Utils.ConvertToInt64(Common.Utils.IsNull(DataManager.GetExecuteScalar(ref cmd, query.ToString(), "", "").ToString(), 0)); ;
        }



        public Result SaveDeckProduct(String id, String deckId,
            String productId, String routingMode, String amount, String maxAmount, String discount, String surcharge, String SendProvider,
            String statusId, String description, String abbreviation, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {


                cmd.Parameters.Add("ion_deck_product_id", OracleDbType.Int64).Direction = ParameterDirection.InputOutput;
                cmd.Parameters["ion_deck_product_id"].Value = Utils.IsNull(id, null);
                cmd.Parameters.Add("iz_description", OracleDbType.Varchar2, 100).Value = Utils.IsNull(description, null);
                cmd.Parameters.Add("iz_abbreviation", OracleDbType.Varchar2).Value = Utils.IsNull(abbreviation, null);
                cmd.Parameters.Add("in_deck_id", OracleDbType.Int64).Value = Utils.IsNull(deckId, null);
                cmd.Parameters.Add("in_product_id", OracleDbType.Int64).Value = Utils.IsNull(productId, null);
                cmd.Parameters.Add("iz_routing_mode", OracleDbType.Int32).Value = Utils.IsNull(routingMode, null);
                cmd.Parameters.Add("in_amount", OracleDbType.Decimal).Value = Convert.ToDecimal(Utils.IsNull(amount, 0));
                cmd.Parameters.Add("in_max_amount", OracleDbType.Decimal).Value = Convert.ToDecimal(Utils.IsNull(maxAmount, 0));
                cmd.Parameters.Add("in_discount", OracleDbType.Decimal).Value = Convert.ToDecimal(Utils.IsNull(discount, 0));
                cmd.Parameters.Add("in_surcharge", OracleDbType.Decimal).Value = Convert.ToDecimal(Utils.IsNull(surcharge, 0));
                cmd.Parameters.Add("in_send_provider", OracleDbType.Int64).Value = Utils.ConvertToInt64(Utils.IsNull(SendProvider, 0));
                cmd.Parameters.Add("in_status_id", OracleDbType.Int64).Value = Common.Utils.IsNull(statusId, null);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int64).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = Common.Utils.IsNull(locale, null);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;


                DataManager.RunDataCommandSP(ref cmd, "ip_pg_deck.save_deck_product", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (result.Code.Equals("0"))
                {
                    result.Id = cmd.Parameters["ion_deck_product_id"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result RemoveDeckProduct(String id, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("ion_deck_product_id", OracleDbType.Int64).Value = Common.Utils.ConvertToInt64(id, DBNull.Value);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_deck.remove_deck_product", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public DeckTemplateResult SearchDeckTemplate(String id, String deckProductId,
            String providerAccountId, String statusId)
        {
            DeckTemplateResult result = new DeckTemplateResult();
            List<DeckTemplate> item = new List<DeckTemplate>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select template_id, deck_product_id, provider_account_id, provider_account_name,product_code,real_amount, max_amount, ");
                query.Append(" discount, surcharge, provider_balance, provider_credit_limit, payment_type, ");
                query.Append(" weight, date_created, user_created, date_updated, user_updated ");
                query.Append(" from ip_vw_deck_template");


                DataManager.AddBindParamCmd(ref cmd, ref query, "template_id", id, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "deck_product_id", deckProductId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "provider_account_id", providerAccountId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "status_id", statusId, OracleDbType.Int32);
                query.Append(" order by weight");

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        item.Add(new DeckTemplate(
                            Convert.ToInt64(row["template_id"].ToString()),
                            Convert.ToInt64(row["deck_product_id"].ToString()),
                            Common.Utils.IsNull(row["provider_account_id"], "").ToString(),
                            Common.Utils.IsNull(row["provider_account_name"], "").ToString(),
                            Convert.ToInt64(row["weight"].ToString()),
                            Common.Utils.IsNull(row["product_code"], "").ToString(),
                            Convert.ToDecimal(Common.Utils.IsNull(row["real_amount"], 0)),
                            Convert.ToDecimal(Common.Utils.IsNull(row["max_amount"], 0)),
                            Convert.ToDecimal(Common.Utils.IsNull(row["discount"], "0").ToString()),
                            Convert.ToDecimal(Common.Utils.IsNull(row["surcharge"], 0)),
                            Convert.ToDecimal(Common.Utils.IsNull(row["provider_balance"], "0").ToString()),
                            Convert.ToDecimal(Common.Utils.IsNull(row["provider_credit_limit"], "0").ToString()),
                            Common.Utils.IsNull(row["payment_type"], "").ToString()
                            ));
                    }

                    result.Deck = item;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result SaveDeckTemplate(String id, String deckProductId, String providerAccountId,
            String weight, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("ion_template_id", OracleDbType.Int32, ParameterDirection.InputOutput).Value = (String.IsNullOrEmpty(id) ? null : id);
                cmd.Parameters.Add("in_deck_product_id", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(deckProductId) ? null : deckProductId);
                cmd.Parameters.Add("in_provider_account_id", OracleDbType.Int32).Value = (String.IsNullOrEmpty(providerAccountId) ? null : providerAccountId);
                cmd.Parameters.Add("in_weight", OracleDbType.Int32).Value = (String.IsNullOrEmpty(weight) ? null : weight);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_deck.save_template", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();

                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();


                if (result.Code.Equals("0"))
                {
                    result.Id = cmd.Parameters["ion_template_id"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result RemoveDeckTemplate(String id, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_template_id", OracleDbType.Int32).Value = id;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_deck.remove_template", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result GenDeckTemplate(String id, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_deck_product_id", OracleDbType.Int32).Value = id;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_deck.gen_deck_template", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }
    }
}
