﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;

namespace DataAccessLayer.DataAccessObjects
{
    //    SELECT request_id, description, message, status_id, status_name, date_program,
    //date_start, date_finish, pending, delivered, failures
    //FROM ip_vw_send_msg_summary

    [Serializable]
    public class SendSmsResult : Result
    {
        public List<SendSms> SendSms { get; set; }
    }


    public class SendMsgSummaryResult : Result
    {
        public List<SendMsgSummary> SendMsgSummary { get; set; }

        public SendMsgSummaryResult()
        {
            this.SendMsgSummary = new List<SendMsgSummary>();
        }


    }


    [Serializable]
    public class SendMsgSummary
    {
        public SendMsgSummary()
        {

        }

        public String RequestId { get; set; }
        public String Description { get; set; }
        public String Message { get; set; }
        public String StatusId { get; set; }
        public String StatusName { get; set; }
        public String DateProgram { get; set; }
        public String DateStart { get; set; }
        public String DateFinish { get; set; }
        public String Pending { get; set; }
        public String Delivered { get; set; }
        public String Failures { get; set; }
    }

    public class SendSmsDAO
    {
        public SendMsgSummaryResult Search(String id, String statusId)
        {
            SendMsgSummaryResult result = new SendMsgSummaryResult();
            List<SendMsgSummary> list = new List<SendMsgSummary>();
            StringBuilder query = new StringBuilder();
            DataTable dt;
            OracleCommand cmd = new OracleCommand();

            try
            {




                query.Append(" SELECT request_id, description, message, status_id, status_name, date_program, ");
                query.Append(" date_start, date_finish, pending, delivered, failures ");
                query.Append(" FROM ip_vw_send_msg_summary ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "request_id", id, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "status_id", statusId, OracleDbType.Int32);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        SendMsgSummary sum = new SendMsgSummary();

                        sum.DateFinish = Utils.IsNull(row["date_finish"], "").ToString();
                        sum.DateProgram = Utils.IsNull(row["date_program"], "").ToString();
                        sum.DateStart = Utils.IsNull(row["date_start"], "").ToString();
                        sum.Delivered = Utils.IsNull(row["delivered"], "").ToString();
                        sum.Description = Utils.IsNull(row["description"], "").ToString();
                        sum.Failures = Utils.IsNull(row["failures"], "").ToString();
                        sum.Message = Utils.IsNull(row["message"], "").ToString();
                        sum.Pending = Utils.IsNull(row["pending"], "").ToString();
                        sum.RequestId = Utils.IsNull(row["request_id"], "").ToString();
                        sum.StatusId = Utils.IsNull(row["status_id"], "").ToString();
                        sum.StatusName = Utils.IsNull(row["status_name"], "").ToString();

                        list.Add(sum);
                    }

                    result.SendMsgSummary = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }

            return result;
        }

        public Result UpdateResponse(String requestId, String phone, String responseCode, String responseMessage)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_request_id", OracleDbType.Int32).Value = requestId;
                cmd.Parameters.Add("iz_phone", OracleDbType.Varchar2, 100).Value = phone;
                cmd.Parameters.Add("iz_response_code", OracleDbType.Varchar2, 100).Value = responseCode;
                cmd.Parameters.Add("iz_response_message", OracleDbType.Varchar2, 1000).Value = responseMessage;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_send_sms.update_response", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public SendSmsResult Search(String rownum)
        {
            SendSmsResult result = new SendSmsResult();
            List<SendSms> list = new List<SendSms>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select request_id, phone, message ");
                query.Append(" from ip_vw_send_sms ");

                if (!String.IsNullOrEmpty(rownum))
                {
                    query.Append("where rownum<=");
                    query.Append(rownum);
                }

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new SendSms(
                            row["request_id"].ToString(),
                            row["phone"].ToString(),
                            row["message"].ToString()));
                    }

                    result.SendSms = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public String GetReference()
        {
            String sequence = null;
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select ip_sq_send_sms.nextval ");
                query.Append(" from dual ");

                sequence = DataManager.GetExecuteScalar(query.ToString(), "", "", false).ToString();
            }
            catch (Exception)
            {
                throw;
            }

            return sequence;
        }

        public Result SaveSms(String description, String message, DateTime? dateStart, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("on_request_id", OracleDbType.Int64).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_description", OracleDbType.Varchar2, 200).Value = description;
                cmd.Parameters.Add("iz_message", OracleDbType.Varchar2, 1000).Value = message;
                cmd.Parameters.Add("id_date_start", OracleDbType.Date).Value = dateStart;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 2000).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_send_sms.save_sms", "", "");

                result.Id = Common.Utils.IsNull(cmd.Parameters["on_request_id"].Value, "").ToString();

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result RemoveSms(String requestId, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("on_request_id", OracleDbType.Int64).Value = requestId;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 2000).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_send_sms.remove_sms", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result SaveSmsPhone(String requestId, List<Int64> phones, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("on_request_id", OracleDbType.Int64).Value = Common.Utils.ConvertToInt64(requestId,0);
                
                cmd.Parameters.Add("ia_phone", OracleDbType.Int64);
                cmd.Parameters["ia_phone"].CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                cmd.Parameters["ia_phone"].Value = phones.ToArray();

                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 2000).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_send_sms.save_sms_phone", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result RemoveSmsPhone(String requestId, List<String> phones, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("on_request_id", OracleDbType.Int64).Value = requestId;

                cmd.Parameters.Add("ia_phone", OracleDbType.Varchar2);
                cmd.Parameters["ia_phone"].CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                cmd.Parameters["ia_phone"].Value = phones.ToArray();

                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 2000).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_send_sms.remove_save_phone", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }
    }
}
