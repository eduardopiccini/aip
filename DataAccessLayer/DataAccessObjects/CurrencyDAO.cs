﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using Common;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class CurrencyResult : Result
    {
        public List<Currency> Currency { get; set; }
    }

    public class CurrencyDAO
    {
        /// <summary>
        /// Gets all the currency register in the data base
        /// </summary>
        /// <returns>A list of currency</returns>
        public CurrencyResult Search(String id, String statusId,
            String description)
        {
            CurrencyResult result = new CurrencyResult();
            List<Currency> list = new List<Currency>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select currency_id, description, abbreviation, rate, status_id, status_description ");
                query.Append(" from ip_vw_currency ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "currency_id", (id == null) ? null : id.ToUpper(), OracleDbType.Varchar2, DataManager.eOperador.Like_Donde_Encuentre);
                DataManager.AddBindParamCmd(ref cmd, ref query, "status_id", (statusId==null) ? null : statusId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "description", (description == null) ? null : description.ToUpper(), OracleDbType.Varchar2, DataManager.eOperador.Like_Donde_Encuentre);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                query.Append("order by currency_id asc ");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new Currency(
                            row["currency_id"] == null ? null : row["currency_id"].ToString(),
                            row["description"] == null ? null : row["description"].ToString(),
                            row["abbreviation"] == null ? null : row["abbreviation"].ToString(),
                            Convert.ToDecimal(Common.Utils.IsNull(row["rate"], "0").ToString()),
                            Convert.ToInt64(row["status_id"].ToString()),
                            row["status_description"] == null ? null : row["status_description"].ToString()));
                    }

                    result.Currency = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result Add(String id, String description, String abbreviation, String rate,
            String statusId, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("iz_currency_id", OracleDbType.Varchar2).Value = id;
                cmd.Parameters.Add("iz_description", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(description) ? null : description);
                cmd.Parameters.Add("iz_abbreviation", OracleDbType.Varchar2, 20).Value = (String.IsNullOrEmpty(abbreviation) ? null : abbreviation);
                cmd.Parameters.Add("in_rate", OracleDbType.Decimal).Value = (String.IsNullOrEmpty(rate) ? null : rate);
                cmd.Parameters.Add("in_status_id", OracleDbType.Int32).Value = (String.IsNullOrEmpty(statusId) ? null : statusId);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_currency.save", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result Modify(String id, String description, String abbreviation,String rate,
                  String statusId, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("iz_currency_id", OracleDbType.Varchar2).Value = id;
                cmd.Parameters.Add("iz_description", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(description) ? null : description);
                cmd.Parameters.Add("iz_abbreviation", OracleDbType.Varchar2, 20).Value = (String.IsNullOrEmpty(abbreviation) ? null : abbreviation);
                cmd.Parameters.Add("in_rate", OracleDbType.Decimal).Value = (String.IsNullOrEmpty(rate) ? null : rate);
                cmd.Parameters.Add("in_status_id", OracleDbType.Int32).Value = (String.IsNullOrEmpty(statusId) ? null : statusId);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_currency.change", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result Remove(String id, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("iz_currency_id", OracleDbType.Varchar2).Value = id;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_currency.remove", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

    }
}
