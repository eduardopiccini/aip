﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using Common;
using OracleAccessLayer;

namespace DataAccessLayer.DataAccessObjects
{
    //[Serializable]
    //public class AlertResult : Result
    //{
    //    public List<Alert> Alert { get; set; }
    //}


    public class PortalDAO
    {

        public Result UpdateProfile(String AccountId, String Address, String CountryId, String StateId, String Fax, String Phone, String ZipCode, String LowBalanceAlert, String LowBalanceAlertAmount, String LowBalanceAlertInterval, String Note, String Logo, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_account_id", OracleDbType.Int64).Value = AccountId;
                cmd.Parameters.Add("iz_address", OracleDbType.Varchar2, 100).Value = Address;
                cmd.Parameters.Add("in_country_id", OracleDbType.Int64).Value =  Utils.ConvertToInt64(CountryId, DBNull.Value);
                cmd.Parameters.Add("iz_state_id", OracleDbType.Int64).Value = Utils.ConvertToInt64(StateId, DBNull.Value);
                cmd.Parameters.Add("in_fax", OracleDbType.Int64).Value = Utils.ConvertToInt64(Fax,DBNull.Value);
                cmd.Parameters.Add("in_telephone", OracleDbType.Int64).Value = Utils.ConvertToInt64(Phone,DBNull.Value);
                cmd.Parameters.Add("in_zip_code", OracleDbType.Int64).Value = Utils.ConvertToInt64(ZipCode, DBNull.Value);


                cmd.Parameters.Add("in_low_balance_alert", OracleDbType.Int64,1).Value =  Utils.ConvertToInt64(LowBalanceAlert,0);
                cmd.Parameters.Add("in_low_balance_alert_amount", OracleDbType.Decimal).Value = Utils.ConvertToDecimal(LowBalanceAlertAmount,0);
                cmd.Parameters.Add("in_low_balance_alert_interval", OracleDbType.Decimal).Value = Utils.ConvertToDecimal(LowBalanceAlertInterval,0);
                cmd.Parameters.Add("iz_note", OracleDbType.Varchar2, 100).Value = Note;
                cmd.Parameters.Add("iz_logo", OracleDbType.Varchar2, 50).Value = Logo;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_account.portal_setting", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();

                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (result.Code.Equals("0"))
                {
                    result.Id = cmd.Parameters["in_account_id"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }

            return result;
        }

    }
}
