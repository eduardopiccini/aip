﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using Common;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class UserOrganizationResult : Result
    {
        public List<UserOrganization> UserOrganization { get; set; }
    }


    public class UserOrganizationDAO
    {
        public UserOrganizationResult Search(String UserId)
        {
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            UserOrganizationResult result = new UserOrganizationResult();
            DataTable dt = new DataTable();
            try
            {
                List<UserOrganization> lstUserOrganization = new List<UserOrganization>();
                query.Append(" select account_id, account_name from ip_vw_user_account ");


                DataManager.AddBindParamCmd(ref cmd, ref query, "user_id", UserId, OracleDbType.NVarchar2);
                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");
                foreach (DataRow dr in dt.Rows)
                {
                    lstUserOrganization.Add(new UserOrganization { AccountId = dr["account_id"].ToString(), AccountName = dr["account_name"].ToString() });

                }
                if (dt.Rows.Count > 0)
                {

                    result.UserOrganization = lstUserOrganization;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }

            }
            catch (Exception ex)
            {

                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null) cmd.Dispose();
            }


            return result;

        }

        public Result Remove(String UserId, String AccountId, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("iz_user_id", OracleDbType.Varchar2, 50).Value = UserId;
                cmd.Parameters.Add("in_account_id", OracleDbType.Varchar2, 50).Value = AccountId;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_user.remove_account", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }

            return result;
        }

        public Result Save(String UserId, String AccountId, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("iz_user_id", OracleDbType.Varchar2, 50).Value = UserId;
                cmd.Parameters.Add("in_account_id", OracleDbType.Varchar2, 50).Value = AccountId;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_user.save_account", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }

            return result;
        }
    
    }








//PROCEDURE save_account (iz_user_id            IN     ip_tb_user_account.user_id%TYPE,
//                    in_account_id         IN     ip_tb_user_account.account_id%TYPE,
//                    on_errorcode             OUT NUMBER,
//                    oz_errormessage          OUT VARCHAR2,
//                    iz_message_locale     IN     ip_tb_locale.locale_id%TYPE DEFAULT NULL,
//                    iz_app_user           IN     VARCHAR2 DEFAULT NULL) IS


//PROCEDURE remove_account (iz_user_id            IN     ip_tb_user_account.user_id%TYPE,
//                        in_account_id         IN     ip_tb_user_account.account_id%TYPE,
//                        on_errorcode             OUT NUMBER,
//                        oz_errormessage          OUT VARCHAR2,
//                        iz_message_locale     IN     ip_tb_locale.locale_id%TYPE DEFAULT NULL,
//                        iz_app_user           IN     VARCHAR2 DEFAULT NULL) IS
}
