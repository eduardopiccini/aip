﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Common;
using System.Data;
using Oracle.DataAccess.Client;
using OracleAccessLayer;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class LowBalanceResult : Result
    {
        public List<LowBalance> LowBalance { get; set; }
    }

    [Serializable]
    public class DestinationDayResult : Result
    {
        public List<DestinationDay> DestinationDay { get; set; }
    }

    [Serializable]
    public class DestinationResult : Result
    {
        public List<Destination> Destination { get; set; }
    }

    [Serializable]
    public class ProviderDashboardResult : Result
    {
        public List<ProviderDashboard> ProviderDashboard { get; set; }
    }

    [Serializable]
    public class ProductDashboardResult : Result
    {
        public List<DashProduct> ProductDashboard { get; set; }
    }

    [Serializable]
    public class TransaResult : Result
    {
        public List<Transa> Transa { get; set; }
    }

    [Serializable]
    public class TransaDayResult : Result
    {
        public List<TransaDay> TransaDay { get; set; }
    }

    [Serializable]
    public class ProductTypeDashResult : Result
    {
        public List<ProductTypeDash> ProductTypeDash { get; set; }
    }

    [Serializable]
    public class ApiDayResult : Result
    {
        public List<ApiDay> ApiDay { get; set; }
    }

    [Serializable]
    public class TransaHourResult : Result
    {
        public List<TransaHour> TransaHour { get; set; }
    }


    [Serializable]
    public class OrganizationResult : Result
    {
        public List<Organization> Organization { get; set; }
    }


    [Serializable]
    public class ResponseCodeDashResult : Result
    {
        public List<ResponseCodeDash> ResponseCodeDash { get; set; }
    }

    public class DashboardDAO
    {

        public ProductDashboardResult SearchProduct(String Accounts)
        {
            ProductDashboardResult result = new ProductDashboardResult();
            List<DashProduct> list = new List<DashProduct>();
            StringBuilder query = new StringBuilder();
            DataTable dt;
            OracleCommand cmd = new OracleCommand();

            try
            {
                query.Append(" select product_id, description, sum (transa)transa,  sum(gross_amount) gross_amount, sum(net_amount) net_amount,sum(net_amount_currency) net_amount_currency, sum(gross_amount_currency) gross_amount_currency  ");
                query.Append(" from ip_vw_dashboard_product ");
                if (Accounts.Trim() != "") DataManager.AddCondSQL(ref query, "account_id in (" + Accounts + ")");
                query.Append(" group by product_id, description order by net_amount desc");
                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new DashProduct
                        {
                            ProductId = row["product_id"].ToString(),
                            Description = row["description"].ToString(),
                            Transa = Convert.ToInt32(row["transa"].ToString()),
                            GrossAmount = Convert.ToDecimal(row["gross_amount"].ToString()),
                            NetAmount = Convert.ToDecimal(row["net_amount"].ToString()),
                            GrossAmountCurrency = Convert.ToDecimal(row["gross_amount_currency"].ToString()),
                            NetAmountCurrency = Convert.ToDecimal(row["net_amount_currency"].ToString())
                        });


                    }

                    result.ProductDashboard = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public ResponseCodeDashResult SearchResponseCode(String Accounts)
        {
            ResponseCodeDashResult result = new ResponseCodeDashResult();
            List<ResponseCodeDash> list = new List<ResponseCodeDash>();
            StringBuilder query = new StringBuilder();
            DataTable dt;
            OracleCommand cmd = new OracleCommand();

            try
            {
                query.Append(" select provider_response_code_id, provider_response_description, sum(quantity) quantity, sum(amount) amount, sum(amount_currency) amount_currency ");
                query.Append(" from ip_vw_dashborad_response_code ");
                if (Accounts.Trim() != "") DataManager.AddCondSQL(ref query, "account_id in (" + Accounts + ")");
                query.Append(" group by provider_response_code_id, provider_response_description order by amount desc");
                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new ResponseCodeDash(row["provider_response_code_id"].ToString(),
                            row["provider_response_description"].ToString(),
                            Convert.ToInt32(row["quantity"].ToString()),
                            Convert.ToDecimal(row["amount"].ToString()),
                            Convert.ToDecimal(row["amount_currency"].ToString())));
                    }

                    result.ResponseCodeDash = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        //public OrganizationResult SearchOrganization(String Accounts)
        //{
        //    OrganizationResult result = new OrganizationResult();
        //    List<Organization> list = new List<Organization>();
        //    StringBuilder query = new StringBuilder();
        //    DataTable dt;
        //    OracleCommand cmd = new OracleCommand();

        //    try
        //    {
        //        query.Append(" select account_id, name, sum(balance) balance, sum(transa_quantity) transa_quantity, sum(gross_amount) gross_amount, ");
        //        query.Append(" sum(net_amount, surcharge_quantity, surcharge ");
        //        query.Append(" from ip_vw_dashboard_organization ");

        //        if (Accounts.Trim() != "") DataManager.AddCondSQL(ref query, "account_id in (" + Accounts + ")");

        //        dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

        //        if (dt != null && dt.Rows.Count > 0)
        //        {
        //            foreach (DataRow row in dt.Rows)
        //            {
        //                list.Add(new Organization(row["account_id"].ToString(),
        //                    row["name"].ToString(),
        //                    Convert.ToDecimal(row["balance"].ToString()),
        //                    Convert.ToDecimal(row["credit_limit"].ToString()),
        //                    Convert.ToInt32(row["transa_quantity"].ToString()),
        //                    Convert.ToDecimal(row["gross_amount"].ToString()),
        //                    Convert.ToDecimal(row["net_amount"].ToString()),
        //                    Convert.ToInt32(row["surcharge_quantity"].ToString()),
        //                    Convert.ToDecimal(row["surcharge"].ToString())));
        //            }

        //            result.Organization = list;
        //            result.Code = "0";
        //            result.Message = "TRUE";
        //        }
        //        else
        //        {
        //            result.Code = Common.Utils.NO_RECORDS_FOUND;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        result.Code = Common.Utils.UNKNOWN_EXCEPTION;
        //        result.Details = ex.Message;
        //    }
        //    finally
        //    {
        //        if (cmd != null)
        //        {
        //            cmd.Dispose();
        //        }
        //    }

        //    return result;
        //}

        public ProductTypeDashResult SearchProductType(String Accounts)
        {
            ProductTypeDashResult result = new ProductTypeDashResult();
            List<ProductTypeDash> list = new List<ProductTypeDash>();
            StringBuilder query = new StringBuilder();
            DataTable dt;
            OracleCommand cmd = new OracleCommand();

            try
            {
                query.Append(" select product_type_id, description, sum(transa) transa, sum(gross_amount) gross_amount, sum(net_amount) net_amount, sum(gross_amount_currency) gross_amount_currency, sum(net_amount_currency) net_amount_currency ");
                query.Append(" from ip_vw_dashboard_product_type ");

                if (Accounts.Trim() != "") DataManager.AddCondSQL(ref query, "account_id in (" + Accounts + ")");
                query.Append(" group by product_type_id, description order by net_amount desc");

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new ProductTypeDash
                        {
                            ProductTypeId = row["product_type_id"].ToString(),
                            Description = row["description"].ToString(),
                            Transa = Convert.ToInt32(row["transa"].ToString()),
                            GrossAmount = Convert.ToDecimal(row["gross_amount"].ToString()),
                            GrossAmountCurrency = Convert.ToDecimal(row["gross_amount_currency"].ToString()),
                            NetAmount = Convert.ToDecimal(row["net_amount"].ToString()),
                            NetAmountCurrency = Convert.ToDecimal(row["net_amount_currency"].ToString())
                        });

                    }

                    result.ProductTypeDash = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }



        public LowBalanceResult SearchLowBalance(String accountType)
        {
            LowBalanceResult result = new LowBalanceResult();
            List<LowBalance> list = new List<LowBalance>();
            StringBuilder query = new StringBuilder();
            DataTable dt;
            OracleCommand cmd = new OracleCommand();

            try
            {
                query.Append(" select account_id, name, balance balance, credit_limit, low_balance_alert_amount ");
                query.Append(" from ip_vw_dashboard_low_balance ");
                DataManager.AddCondSQL(ref query, "account_type_id in(" + accountType + ")");


                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new LowBalance(row["account_id"].ToString(),
                            row["name"].ToString(),
                            Convert.ToDecimal(row["balance"].ToString()),
                            Convert.ToDecimal(row["credit_limit"].ToString()),
                            Convert.ToDecimal(row["low_balance_alert_amount"].ToString())));
                    }

                    result.LowBalance = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public DestinationDayResult SearchDestinationDay(String Accounts)
        {
            DestinationDayResult result = new DestinationDayResult();
            List<DestinationDay> list = new List<DestinationDay>();
            StringBuilder query = new StringBuilder();
            DataTable dt;
            OracleCommand cmd = new OracleCommand();

            try
            {
                query.Append("select * from (select country_id, carrier_id, name, sum(quantity) quantity, sum(gross_amount) gross_amount, sum(net_amount) net_amount, sum(surcharge) surcharge, sum(surcharge_currency) surcharge_currency,sum(net_amount_currency) net_amount_currency,sum(gross_amount_currency) gross_amount_currency  ");
                query.Append(" from ip_vw_dashboard_dest_day ");
                if (Accounts.Trim() != "") DataManager.AddCondSQL(ref query, "account_id in (" + Accounts + ")");
                query.Append(" group by country_id, carrier_id, name order by net_amount desc ) where rownum <=15");

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new DestinationDay
                        {
                            CountryId = row["country_id"].ToString(),
                            CarrierId = row["carrier_id"].ToString(),
                            Name = row["name"].ToString(),
                            Quantity = Convert.ToInt32(row["quantity"].ToString()),
                            GrossAmount = Convert.ToDecimal(row["gross_amount"].ToString()),
                            GrossAmountCurrency = Convert.ToDecimal(row["gross_amount_currency"].ToString()),
                            NetAmount = Convert.ToDecimal(row["net_amount"].ToString()),
                            NetAmountCurrency = Convert.ToDecimal(row["net_amount_currency"].ToString()),
                            Surcharge = Convert.ToDecimal(row["surcharge"].ToString()),
                            SurchargeCurrency = Convert.ToDecimal(row["surcharge_currency"].ToString())
                        });
                    }

                    result.DestinationDay = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public TransaHourResult SearchTransaHour(String Accounts)
        {
            TransaHourResult result = new TransaHourResult();
            List<TransaHour> list = new List<TransaHour>();
            StringBuilder query = new StringBuilder();
            DataTable dt;
            OracleCommand cmd = new OracleCommand();

            try
            {
                query.Append(" select hour, sum(quantity) quantity, sum(gross_amount) gross_amount, sum(net_amount) net_amount, sum(net_amount_currency) net_amount_currency, sum(gross_amount_currency) gross_amount_currency ");
                query.Append(" from ip_vw_dashboard_trans_hour ");
                if (Accounts.Trim() != "") DataManager.AddCondSQL(ref query, "account_id in (" + Accounts + ")");
                query.Append(" group by hour order by hour");
                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new TransaHour
                        {
                            Hour = row["hour"].ToString(),
                            Quantity = Convert.ToDecimal(Common.Utils.IsNull(row["quantity"], "0").ToString()),
                            GrossAmount = Convert.ToDecimal(Common.Utils.IsNull(row["gross_amount"], "0").ToString()),
                            GrossAmountCurrency = Convert.ToDecimal(Common.Utils.IsNull(row["gross_amount_currency"], "0").ToString()),
                            NetAmount = Convert.ToDecimal(Common.Utils.IsNull(row["net_amount"], "0").ToString()),
                            NetAmountCurrency = Convert.ToDecimal(Common.Utils.IsNull(row["net_amount_currency"], "0").ToString())
                        });
                    }

                    result.TransaHour = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public DestinationResult SearchDestination(String Accounts)
        {
            DestinationResult result = new DestinationResult();
            List<Destination> list = new List<Destination>();
            StringBuilder query = new StringBuilder();
            DataTable dt;
            OracleCommand cmd = new OracleCommand();

            try
            {
                query.Append(" select * from (select country_id, carrier_id, name, sum(quantity) quantity, sum(gross_amount) gross_amount, sum(net_amount) net_amount, sum(surcharge) surcharge, sum(surcharge_currency) surcharge_currency, sum(net_amount_currency) net_amount_currency, sum(gross_amount_currency) gross_amount_currency ");
                query.Append(" from ip_vw_dashboard_destination ");
                if (Accounts.Trim() != "") DataManager.AddCondSQL(ref query, "account_id in (" + Accounts + ")");
                query.Append(" group by country_id, carrier_id, name order by net_amount desc ) where rownum <=15");

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new Destination
                        {
                            CountryId = row["country_id"].ToString(),
                            CarrierId = row["carrier_id"].ToString(),
                            Name = row["name"].ToString(),
                            Quantity = Convert.ToInt32(row["quantity"].ToString()),
                            GrossAmount = Convert.ToDecimal(row["gross_amount"].ToString()),
                            GrossAmountCurrency = Convert.ToDecimal(row["gross_amount_currency"].ToString()),
                            NetAmount = Convert.ToDecimal(row["net_amount"].ToString()),
                            NetAmountCurrency = Convert.ToDecimal(row["net_amount_currency"].ToString()),
                            Surcharge = Convert.ToDecimal(row["surcharge"].ToString()),
                            SurchargeCurrency = Convert.ToDecimal(row["surcharge_currency"].ToString())
                        });
                    }

                    result.Destination = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public TransaResult SearchTransa(String Accounts)
        {
            TransaResult result = new TransaResult();
            List<Transa> list = new List<Transa>();
            StringBuilder query = new StringBuilder();
            DataTable dt;
            OracleCommand cmd = new OracleCommand();
            try
            {
                query.Append(" select sum(total_transa) total_transa, sum(valid_transa) valid_transa,  sum(invalid_transa) invalid_transa, sum(gross_amount) gross_amount, sum(net_amount) net_amount, sum(surcharge_quantity) surcharge_quantity, sum(surcharge) surcharge, sum(surcharge_currency) surcharge_currency, sum(net_amount_currency) net_amount_currency, sum(gross_amount_currency) gross_amount_currency ");
                query.Append(" from ip_vw_dashboard_trans ");

                if (Accounts.Trim() != "") DataManager.AddCondSQL(ref query, "account_id in (" + Accounts + ")");

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new Transa
                        {
                            TotalTransa = Utils.ConvertToInt64(row["total_transa"], 0),
                            ValidTransa = Utils.ConvertToInt64(row["valid_transa"], 0),
                            InvalidTransa = Utils.ConvertToInt64(row["invalid_transa"], 0),
                            GrossAmount = Utils.ConvertToDecimal(row["gross_amount"], 0),
                            GrossAmountCurrency = Utils.ConvertToDecimal(row["gross_amount_currency"], 0),
                            NetAmount = Utils.ConvertToDecimal(row["net_amount"], 0),
                            NetAmountCurrency = Utils.ConvertToDecimal(row["net_amount_currency"], 0),
                            Surcharge = Utils.ConvertToDecimal(row["surcharge"], 0),
                            SurchargeCurrency = Utils.ConvertToDecimal(row["surcharge_currency"], 0),
                            SurchargeQuantity = Utils.ConvertToInt64(row["surcharge_quantity"], 0)
                        });
                    }

                    result.Transa = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="Account">Accounts separado por , ejem: 1,7,9</param>
        /// <returns></returns>
        public ProviderDashboardResult SearchProvider(String Accounts)
        {
            ProviderDashboardResult result = new ProviderDashboardResult();
            List<ProviderDashboard> list = new List<ProviderDashboard>();
            StringBuilder query = new StringBuilder();
            DataTable dt;
            OracleCommand cmd = new OracleCommand();

            try
            {
                query.Append(" select  provider_id, description, sum(total_transa) total_transa, sum(valid_transa) valid_transa, sum(invalid_transa) invalid_transa, ");
                query.Append(" sum(gross_amount) gross_amount, sum(net_amount) net_amount, sum(surcharge_quantity) surcharge_quantity, sum(surcharge) surcharge, sum(surcharge_currency) surcharge_currency,sum(net_amount_currency) net_amount_currency, sum(gross_amount_currency) gross_amount_currency  ");
                query.Append(" from ip_vw_dashboard_provider ");

                if (Accounts.Trim() != "") DataManager.AddCondSQL(ref query, "account_id in (" + Accounts + ")");
                query.Append(" group by provider_id, description order by net_amount desc");
                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new ProviderDashboard
                        {
                            ProviderId = row["provider_id"].ToString(),
                            Description = row["description"].ToString(),
                            TotalTransa = Convert.ToInt32(row["total_transa"].ToString()),
                            ValidTransa = Convert.ToInt32(row["valid_transa"].ToString()),
                            InvalidTransa = Convert.ToInt32(row["invalid_transa"].ToString()),
                            GrossAmount = Convert.ToDecimal(row["gross_amount"].ToString()),
                            GrossAmountCurrency = Convert.ToDecimal(row["gross_amount_currency"].ToString()),
                            NetAmount = Convert.ToDecimal(row["net_amount"].ToString()),
                            NetAmountCurrency = Convert.ToDecimal(row["net_amount_currency"].ToString()),
                            SurchargeQuantity = Convert.ToInt32(row["surcharge_quantity"].ToString()),
                            Surcharge = Convert.ToDecimal(row["surcharge"].ToString()),
                            SurchargeCurrency = Convert.ToDecimal(row["surcharge_currency"].ToString())
                        });
                    }

                    result.ProviderDashboard = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public TransaDayResult SearchTransaDay(String Accounts)
        {
            TransaDayResult result = new TransaDayResult();
            List<TransaDay> list = new List<TransaDay>();
            StringBuilder query = new StringBuilder();
            DataTable dt;
            OracleCommand cmd = new OracleCommand();

            try
            {
                query.Append(" select day_number, day_name, sum(quantity) quantity, sum(gross_amount) gross_amount, sum(net_amount) net_amount, sum(net_amount_currency) net_amount_currency, sum(gross_amount_currency) gross_amount_currency ");
                query.Append(" from ip_vw_dashboard_trans_day ");

                if (Accounts.Trim() != "") DataManager.AddCondSQL(ref query, "account_id in (" + Accounts + ")");
                query.Append(" group by day_number, day_name order by day_number ");

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new TransaDay
                        {
                            DayNumber = row["day_number"].ToString(),
                            DayName = row["day_name"].ToString(),
                            Quantity = Convert.ToDecimal(row["quantity"].ToString()),
                            GrossAmount = Convert.ToDecimal(row["gross_amount"].ToString()),
                            GrossAmountCurrency = Convert.ToDecimal(row["gross_amount_currency"].ToString()),
                            NetAmount = Convert.ToDecimal(row["net_amount"].ToString()),
                            NetAmountCurrency = Convert.ToDecimal(row["net_amount_currency"].ToString())
                        });
                    }

                    result.TransaDay = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public ApiDayResult SearchApiDay(String Accounts)
        {
            ApiDayResult result = new ApiDayResult();
            List<ApiDay> list = new List<ApiDay>();
            StringBuilder query = new StringBuilder();
            DataTable dt;
            OracleCommand cmd = new OracleCommand();

            try
            {


                query.Append(" select day_number, day_name, sum(quantity) quantity, sum(gross_amount) gross_amount, sum(net_amount) net_amount, ");
                query.Append(" sum(failed_quantity) failed_quantity, sum(failed_gross_amount) failed_gross_amount, sum(failed_net_amount) day_number, day_name ");
                query.Append(" from ip_vw_dashboard_api_day  ");

                if (Accounts.Trim() != "") DataManager.AddCondSQL(ref query, "account_id in (" + Accounts + ")");
                query.Append(" group by day_number, day_name order by day_number");

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new ApiDay(
                            Convert.ToInt32(row["day_number"].ToString()),
                            row["day_name"].ToString(),
                            Convert.ToDecimal(row["quantity"].ToString()),
                            Convert.ToDecimal(row["net_amount"].ToString()),
                            Convert.ToDecimal(row["failed_quantity"].ToString()),
                            Convert.ToDecimal(row["failed_gross_amount"].ToString()),
                            Convert.ToDecimal(row["failed_net_amount"].ToString())));
                    }

                    result.ApiDay = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }
    }
}
