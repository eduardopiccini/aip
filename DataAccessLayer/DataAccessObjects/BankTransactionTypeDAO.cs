﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using Common;
using OracleAccessLayer;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class BankTransactionTypeResult : Result
    {
        public List<BankTransactionType> ListBankTransactionType { get; set; }
    }


    public class BankTransactionTypeDAO
    {

      
        public BankTransactionTypeResult Search(String id, String Description, String StatusId)
        {
            BankTransactionTypeResult result = new BankTransactionTypeResult();
            List<BankTransactionType> list = new List<BankTransactionType>();
            StringBuilder query = new StringBuilder();
            DataTable dt;
            OracleCommand cmd = new OracleCommand();

            try
            {
                query.Append(" select bank_transaction_type_id, description, status_id, status_name from ip_vw_bank_transaction_type ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "bank_transaction_type_id", id, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "description", Description.ToUpper(), OracleDbType.Varchar2, DataManager.eOperador.Like_Donde_Encuentre);
                DataManager.AddBindParamCmd(ref cmd, ref query, "status_id", StatusId, OracleDbType.Int64);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new BankTransactionType { Id = Utils.ConvertToInt64(row["bank_transaction_type_id"].ToString()), Description = row["description"].ToString(), StatusId = Utils.ConvertToInt64(row["status_id"].ToString()), StatusDescription = row["status_name"].ToString() });
                    }

                    result.ListBankTransactionType = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }

            return result;
        }
    }
}
