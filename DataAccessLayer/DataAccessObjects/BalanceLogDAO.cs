﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using DataAccessLayer.EntityBase;
using Common;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class BalanceLogResult : Result
    {
        public List<BalanceLog> AccountBalanceLog { get; set; }
    }

    public class BalanceLogDAO
    {
        public BalanceLogResult SearchAccount(String id, String accountId,
            DateTime? createdBegin, DateTime?  createEnd, DateTime? updateBegin, 
            DateTime? updateEnd, String userCreated, String userUpdated)
        {
            BalanceLogResult result = new BalanceLogResult();
            List<BalanceLog> list = new List<BalanceLog>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select balance_log_id, direction_id, direction_name, ");
                query.Append(" account_id, account_name, date_created, ");
                query.Append(" balance, old_balance, user_created, date_updated, user_updated ");
                query.Append(" from ip_vw_balance_log_account ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "balance_log_id", (String.IsNullOrEmpty(id) ? null : id), OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "account_id", (String.IsNullOrEmpty(accountId) ? null : accountId), OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "date_created", createdBegin, createEnd, OracleDbType.Date, DataManager.eOperador.Between);
                DataManager.AddBindParamCmd(ref cmd, ref query, "date_updated", updateBegin, updateEnd, OracleDbType.Date, DataManager.eOperador.Between);
                DataManager.AddBindParamCmd(ref cmd, ref query, "user_created", userCreated, OracleDbType.Varchar2);
                DataManager.AddBindParamCmd(ref cmd, ref query, "user_updated", userUpdated, OracleDbType.Varchar2);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new BalanceLog(
                            Convert.ToInt64(Common.Utils.IsNull(row["balance_log_id"], 0)),
                            row["direction_id"] == null ? null : row["direction_id"].ToString(),
                            row["direction_name"] == null ? null : row["direction_name"].ToString(),
                            Convert.ToInt64(Common.Utils.IsNull(row["account_id"], 0)),
                            row["account_name"] == null ? null : row["account_name"].ToString(),
                            row["date_created"] == null ? null : row["date_created"].ToString(),
                            Convert.ToDecimal(Common.Utils.IsNull(row["balance"], 0)),
                            Convert.ToDecimal(Common.Utils.IsNull(row["old_balance"], 0)),
                            row["user_created"] == null ? null : row["user_created"].ToString(),
                            row["date_updated"] == null ? null : row["date_updated"].ToString(),
                            row["user_updated"] == null ? null : row["user_updated"].ToString()));
                    }

                    result.AccountBalanceLog = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public BalanceLogResult SearchProvider(String id, String accountId)
        {
            BalanceLogResult result = new BalanceLogResult();
            List<BalanceLog> list = new List<BalanceLog>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select balance_log_id, direction_id, direction_name, ");
                query.Append(" account_id, account_name, date_created, ");
                query.Append(" balance, old_balance, user_created, date_updated, user_updated ");
                query.Append(" from ip_vw_balance_log_provider ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "balance_log_id", (String.IsNullOrEmpty(id) ? null : id), OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "account_id", (String.IsNullOrEmpty(accountId) ? null : accountId), OracleDbType.Int32);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new BalanceLog(
                            Convert.ToInt64(Common.Utils.IsNull(row["balance_log_id"], 0)),
                            row["direction_id"] == null ? null : row["direction_id"].ToString(),
                            row["direction_name"] == null ? null : row["direction_name"].ToString(),
                            Convert.ToInt64(Common.Utils.IsNull(row["account_id"], 0)),
                            row["account_name"] == null ? null : row["account_name"].ToString(),
                            row["date_created"] == null ? null : row["date_created"].ToString(),
                            Convert.ToDecimal(Common.Utils.IsNull(row["balance"], 0)),
                            Convert.ToDecimal(Common.Utils.IsNull(row["old_balance"], 0)),
                            row["user_created"] == null ? null : row["user_created"].ToString(),
                            row["date_updated"] == null ? null : row["date_updated"].ToString(),
                            row["user_updated"] == null ? null : row["user_updated"].ToString()));
                    }

                    result.AccountBalanceLog = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }
    }
}
