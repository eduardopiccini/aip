﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using Common;


namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class ProviderAccountResult : Result
    {
        public List<ProviderAccount> lProviderAccount { get; set; }
    }

    public class ProviderAccountDAO
    {
        /// <summary>
        /// Gets the list of cities register in the data base by the country id
        /// </summary>
        /// <param name="country_id"></param>
        /// <returns>A list of cities of the country specified</returns>
        public ProviderAccountResult Search(String ProductTypeId, String ProviderId, String CountryId, String CarrierId, String ProductId)
        {
            ProviderAccountResult result = new ProviderAccountResult();
            List<ProviderAccount> list = new List<ProviderAccount>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append("select provider_product_id, description, product_id, provider_id, provider_name, product_type_id, product_type_name, ");
                query.Append("currency_id, currency_name, country_id, country_name, carrier_id, carrier_name, city_id, ");
                query.Append("city_name, product_code, min_amount, max_amount, discount, product_cost, send_provider, destination_currency, ");
                query.Append("destination_currency_name, recharged_amount, provider_operator_id, date_created, user_created, ");
                query.Append("date_updated, user_updated, app_user_created, app_user_updated ");
                query.Append("from ip_vw_provider_product ");



                DataManager.AddBindParamCmd(ref cmd, ref query, "product_type_id", Utils.ConvertToInt64(ProductTypeId, null), OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "provider_id", Utils.ConvertToInt64(ProviderId, null), OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "country_id", Utils.ConvertToInt64(CountryId, null), OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "carrier_id", Utils.ConvertToInt64(CarrierId, null), OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "product_id", Utils.ConvertToInt64(ProductId, null), OracleDbType.Int64);


                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new ProviderAccount
                        {
                            ProductId = Utils.ConvertToInt64(row["product_id"]),

                            CarrierId = Utils.ConvertToInt64(row["carrier_id"], null),
                            CarrierName = row["carrier_name"].ToString(),
                            CityId = Utils.ConvertToInt64(row["city_id"], null),
                            CityName = row["city_name"].ToString(),
                            CountryId = Utils.ConvertToInt64(row["country_id"]),
                            CountryName = row["country_name"].ToString(),
                            CurrencyId = row["currency_id"].ToString(),
                            CurrencyName = row["currency_name"].ToString(),
                            Description = row["description"].ToString(),
                            DestinationCurrency = row["destination_currency"].ToString(),
                            DestinationCurrencyName = row["destination_currency_name"].ToString(),
                            Discount = Utils.ConvertToDecimal(row["discount"]),
                            MaxAmount = Utils.ConvertToDecimal(row["max_amount"]),
                            MinAmount = Utils.ConvertToDecimal(row["min_amount"]),
                            ProductCode = row["product_code"].ToString(),
                            ProductCost = Utils.ConvertToDecimal(row["product_cost"]),
                            SendProvider= (Utils.IsNull(row["send_provider"],"0").ToString() =="1"),
                            ProductTypeId = Utils.ConvertToInt64(row["product_type_id"]),
                            ProductTypeName = row["product_type_name"].ToString(),
                            ProviderId = Utils.ConvertToInt64(row["provider_id"]),
                            ProviderName = row["provider_name"].ToString(),
                            ProviderOperatorId = Utils.ConvertToInt64(row["provider_operator_id"]),
                            ProviderProductId = Utils.ConvertToInt64(row["provider_product_id"]),
                            RechargedAmount = Utils.ConvertToDecimal(row["recharged_amount"])
                        });
                    }

                    result.lProviderAccount = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result Save(String providerProductId, String idProvider, String productTypeId, String countryId, String carrierId, String cityId,
                           String currencyId, String productCode, String providerOperatorId, String minAmount, String maxAmount, String Discount,
                           String ProductCost,String SendProvider, String destinationCurrency, String rechargedAmount, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("oin_provider_product_id", OracleDbType.Int64, ParameterDirection.InputOutput).Value = Utils.ConvertToInt64(providerProductId, null);
                cmd.Parameters.Add("in_provider_id", OracleDbType.Int64).Value = Utils.ConvertToInt64(idProvider, null);
                cmd.Parameters.Add("in_product_type_id", OracleDbType.Int64).Value = Utils.ConvertToInt64(productTypeId, null);
                cmd.Parameters.Add("in_country_id", OracleDbType.Int64).Value = Utils.ConvertToInt64(countryId, null);
                cmd.Parameters.Add("iz_carrier_id", OracleDbType.Varchar2).Value = carrierId;
                cmd.Parameters.Add("in_city_id", OracleDbType.Int64).Value = Utils.ConvertToInt64(cityId, null);
                cmd.Parameters.Add("iz_currency_id", OracleDbType.Varchar2).Value = Utils.IsNull(currencyId, null);
                cmd.Parameters.Add("iz_product_code", OracleDbType.Varchar2).Value = productCode;

                cmd.Parameters.Add("iz_provider_operator_id", OracleDbType.Varchar2).Value = providerOperatorId;
                cmd.Parameters.Add("in_min_amount", OracleDbType.Decimal).Value = Utils.ConvertToDecimal(minAmount, null);
                cmd.Parameters.Add("in_max_amount", OracleDbType.Decimal).Value = Utils.ConvertToDecimal(maxAmount, null);
                cmd.Parameters.Add("in_discount", OracleDbType.Decimal).Value = Utils.ConvertToDecimal(Discount, null);
                cmd.Parameters.Add("in_product_cost", OracleDbType.Decimal).Value = Utils.ConvertToDecimal(ProductCost, null);
                cmd.Parameters.Add("in_send_provider", OracleDbType.Int64).Value = Utils.ConvertToInt64(SendProvider, 0);

                cmd.Parameters.Add("iz_destination_currency", OracleDbType.Varchar2).Value = destinationCurrency;
                cmd.Parameters.Add("in_recharged_amount", OracleDbType.Decimal).Value = Utils.ConvertToDecimal(rechargedAmount, null);
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = Utils.IsNull(locale, null);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;



                DataManager.RunDataCommandSP(ref cmd, "ip_pg_provider.save_provider_product", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
                result.Id = cmd.Parameters["oin_provider_product_id"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result Remove(String id, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_provider_product_id", OracleDbType.Int32).Value = (String.IsNullOrEmpty(id) ? null : id);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_provider.remove_provider_product", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


    }
}
