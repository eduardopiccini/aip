﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using Common;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class CityResult : Result
    {
        public List<City> City { get; set; }
    }

    public class CityDAO
    {
        /// <summary>
        /// Gets the list of cities register in the data base by the country id
        /// </summary>
        /// <param name="country_id"></param>
        /// <returns>A list of cities of the country specified</returns>
        public CityResult Search(String id, String countryId, String statusId,
            String description)
        {
            CityResult result = new CityResult();
            List<City> list = new List<City>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select city_id, description, abbreviation, country_id, ");
                query.Append(" country_description, status_id, status_description ");
                query.Append(" from ip_vw_city ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "city_id", (id==null) ? null : id, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "country_id", (countryId==null) ? null : countryId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "status_id", (statusId == null) ? null : statusId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "description", (description == null) ? null : description.ToUpper(), OracleDbType.Varchar2, DataManager.eOperador.Like_Donde_Encuentre);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new City(
                            Convert.ToInt64(row["city_id"].ToString()),
                            row["description"] == null ? null : row["description"].ToString(),
                            row["abbreviation"] == null ? null : row["abbreviation"].ToString(),
                            Convert.ToInt64(row["country_id"].ToString()),
                            row["country_description"] == null ? null : row["country_description"].ToString(),
                            Convert.ToInt64(row["status_id"].ToString()),
                            row["status_description"] == null ? null : row["status_description"].ToString()));
                    }

                    result.City = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        
        public Result Add(String id, String countryId, String description, String abbreviation, String statusId,
            String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_city_id", OracleDbType.Int32).Value = id;
                cmd.Parameters.Add("in_country_id", OracleDbType.Int32).Value = (String.IsNullOrEmpty(countryId) ? null : countryId);
                cmd.Parameters.Add("iz_description", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(description) ? null : description);
                cmd.Parameters.Add("iz_abbreviation", OracleDbType.Varchar2, 20).Value = (String.IsNullOrEmpty(abbreviation) ? null : abbreviation);
                cmd.Parameters.Add("in_status_id", OracleDbType.Int32).Value = (String.IsNullOrEmpty(statusId) ? null : statusId);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_city.save", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result Modify(String id, String countryId, String description, String abbreviation, String statusId,
            String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_city_id", OracleDbType.Int32).Value = id;
                cmd.Parameters.Add("in_country_id", OracleDbType.Int32).Value = (String.IsNullOrEmpty(countryId) ? null : countryId);
                cmd.Parameters.Add("iz_description", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(description) ? null : description);
                cmd.Parameters.Add("iz_abbreviation", OracleDbType.Varchar2, 20).Value = (String.IsNullOrEmpty(abbreviation) ? null : abbreviation);
                cmd.Parameters.Add("in_status_id", OracleDbType.Int32).Value = (String.IsNullOrEmpty(statusId) ? null : statusId);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_city.change", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result Remove(String id, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();


            try
            {
                cmd.Parameters.Add("in_city_id", OracleDbType.Int32).Value = id;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_city.remove", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }
    }
}
