﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using Common;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class AccountMerchantResult : Result
    {
        public List<AccountMerchant> AccountMerchant { get; set; }
    }



    public class AccountMerchantDAO
    {


        public AccountMerchantResult SearchMerchant(String AccountId, String MerchantId, String MerchantName, String TerminalId)
        {
            AccountMerchantResult result = new AccountMerchantResult();
            List<AccountMerchant> item = new List<AccountMerchant>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;


            try
            {
                query.Append(" select account_id, merchant_id, merchant_name, terminal_id from ip_vw_merchant ");


                DataManager.AddBindParamCmd(ref cmd, ref query, "account_id", AccountId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "upper(merchant_id)", "merchant_id", MerchantId.ToUpper(), OracleDbType.Varchar2, DataManager.eOperador.Like_Inicial, false );
                DataManager.AddBindParamCmd(ref cmd, ref query, "upper(merchant_name)","merchant_name", MerchantName.ToUpper(), OracleDbType.Varchar2, DataManager.eOperador.Like_Inicial, false);
                DataManager.AddBindParamCmd(ref cmd, ref query, "upper(terminal_id)", "TerminalId", TerminalId.ToUpper(), OracleDbType.Varchar2, DataManager.eOperador.Like_Inicial, false);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), true, "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        item.Add(new AccountMerchant
                        {
                            AccountId = row["account_id"].ToString(),
                            MerchantId = row["merchant_id"].ToString(),
                            MerchantName = row["merchant_name"].ToString(),
                            TerminalId = row["terminal_id"].ToString()
                        });
                    }

                    result.AccountMerchant  = item;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }



        public Result RemoveMerchant(String MerchantId, String AccountId, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();
            try
            {
                cmd.Parameters.Add("iz_merchant_id", OracleDbType.Varchar2,100).Value =  MerchantId;
                cmd.Parameters.Add("in_account_id", OracleDbType.Int64 ).Value =Utils.ConvertToInt64(AccountId, DBNull.Value );

                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_merchant.remove", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result SaveMerchant(String MerchantId, String MerchantName, String TerminalId, String AccountId,
            String locale, String actApiUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();
              
            try
            {
                cmd.Parameters.Add("iz_merchant_id", OracleDbType.Varchar2,50).Value =  MerchantId;
                cmd.Parameters.Add("in_account_id", OracleDbType.Int64 ).Value =Utils.ConvertToInt64(AccountId, DBNull.Value );
                cmd.Parameters.Add("iz_merchant_name", OracleDbType.Varchar2,100).Value =  MerchantName;
                cmd.Parameters.Add("iz_terminal_id", OracleDbType.Varchar2,50).Value =  TerminalId;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = actApiUser;


                DataManager.RunDataCommandSP(ref cmd, "ip_pg_merchant.save", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }
    }
}
