﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using Common;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class LoginResult : Result
    {
        public String Name {get;set;}
        public String LastName {get;set;}
        public String Email {get;set;}
        public String Telephone {get;set;}
        public String Mobile {get;set;}
    }

    [Serializable]
    public class UserResult : Result
    {
        public List<User> Users { get; set; }
    }

    [Serializable]
    public class UserOptionResult : Result
    {
        public List<UserOption> UserOption { get; set; }
    }

    public class UserDAO
    {
        /// <summary>
        /// Gets all the status register in the database
        /// </summary>
        /// <returns>A list of status</returns>
        public UserResult Search(String id, String statusId, String name, String lastName, 
            String email, String telephone, String mobile)
        {
            UserResult result = new UserResult();
            List<User> list = new List<User>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select user_id, status_id, status_description, name, last_name, ");
                query.Append(" email, telephone, mobile, portal_user, ");
                query.Append(" date_created, user_created, date_updated, user_updated ");
                query.Append(" from ip_vw_user ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "upper(user_id)","user_id", Common.Utils.IsNull(id,"").ToString().ToUpper(), OracleDbType.Varchar2, DataManager.eOperador.Like_Donde_Encuentre,false);
                DataManager.AddBindParamCmd(ref cmd, ref query, "status_id", statusId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "name", Common.Utils.IsNull(name, "").ToString().ToUpper(), OracleDbType.Varchar2, DataManager.eOperador.Like_Donde_Encuentre);
                DataManager.AddBindParamCmd(ref cmd, ref query, "last_name", Common.Utils.IsNull(lastName, "").ToString().ToUpper(), OracleDbType.Varchar2, DataManager.eOperador.Like_Donde_Encuentre);
                DataManager.AddBindParamCmd(ref cmd, ref query, "email", Common.Utils.IsNull(email,"").ToString().ToUpper(), OracleDbType.Varchar2, DataManager.eOperador.Like_Donde_Encuentre);
                DataManager.AddBindParamCmd(ref cmd, ref query, "telephone", telephone, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "mobile",  mobile, OracleDbType.Int64);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new User(
                            row["user_id"] == null ? null : row["user_id"].ToString(),
                            row["status_id"] == null ? null : row["status_id"].ToString(),
                            row["status_description"] == null ? null : row["status_description"].ToString(),
                            row["name"] == null ? null : row["name"].ToString(),
                            row["last_name"] == null ? null : row["last_name"].ToString(),
                            row["email"] == null ? null : row["email"].ToString(),
                            row["telephone"] == null ? null : row["telephone"].ToString(),
                            row["mobile"] == null ? null : row["mobile"].ToString(),
                            row["portal_user"] == null ? null : row["portal_user"].ToString()));
                    }

                    result.Users = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }

            return result;
        }


        public UserOptionResult UserOptionSearch(String userId, String optionId)
        {
            UserOptionResult result = new UserOptionResult();
            List<UserOption> list = new List<UserOption>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select name, last_name, user_id, option_id, option_description, view_allowed, update_allowed, status_option ");
                query.Append(" from ip_vw_user_option ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "user_id", userId, OracleDbType.Varchar2);
                DataManager.AddBindParamCmd(ref cmd, ref query, "option_id", optionId, OracleDbType.Varchar2);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new UserOption(
                            row["user_id"] == null ? null : row["user_id"].ToString(),
                            row["name"] == null ? null : row["name"].ToString(),
                            row["last_name"] == null ? null : row["last_name"].ToString(),
                            row["option_id"] == null ? null : row["option_id"].ToString(),
                            row["option_description"] == null ? null : row["option_description"].ToString(),
                            row["view_allowed"] == null ? null : row["view_allowed"].ToString(),
                            row["update_allowed"] == null ? null : row["update_allowed"].ToString(),
                            row["status_option"] == null ? null : row["status_option"].ToString()));
                    }

                    result.UserOption = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }

            return result;
        }


        public Result UserAdd(String id, String name, String lastName,
            String email, String telephone, String mobile, String statusId, String locale,
            String appUser, String portalUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("iz_user_id", OracleDbType.Varchar2, 50).Value = id;
                cmd.Parameters.Add("iz_last_name", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(lastName) ? null : lastName);
                cmd.Parameters.Add("iz_name", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(name) ? null : name);
                cmd.Parameters.Add("iz_email", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(email) ? null : email);
                cmd.Parameters.Add("in_telephone", OracleDbType.Int64).Value = (String.IsNullOrEmpty(telephone) ? null : telephone);
                cmd.Parameters.Add("in_mobile", OracleDbType.Int64).Value = (String.IsNullOrEmpty(mobile) ? null : mobile);
                cmd.Parameters.Add("in_status_id", OracleDbType.Int32).Value = (String.IsNullOrEmpty(statusId) ? null : statusId);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Decimal).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;
                cmd.Parameters.Add("in_portal_user", OracleDbType.Int16).Value = Convert.ToInt16(Utils.IsNull(portalUser, "0").ToString());

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_user.save_user", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }

            return result;
        }


        public Result UserModify(String id, String name, String lastName,
            String email, String telephone, String mobile, String statusId, String locale, String appUser, String portalUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("iz_user_id", OracleDbType.Varchar2, 50).Value = id;
                cmd.Parameters.Add("iz_last_name", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(lastName) ? null : lastName);
                cmd.Parameters.Add("iz_name", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(name) ? null : name);
                cmd.Parameters.Add("iz_email", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(email) ? null : email);
                cmd.Parameters.Add("in_telephone", OracleDbType.Int64).Value = (String.IsNullOrEmpty(telephone) ? null : telephone);
                cmd.Parameters.Add("in_mobile", OracleDbType.Int64).Value = (String.IsNullOrEmpty(mobile) ? null : mobile);
                cmd.Parameters.Add("in_status_id", OracleDbType.Int32).Value = (String.IsNullOrEmpty(statusId) ? null : statusId);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Decimal).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;
                cmd.Parameters.Add("in_portal_user", OracleDbType.Int16).Value = Convert.ToInt16(Utils.IsNull(portalUser,"0").ToString());
                

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_user.change_user", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }

            return result;
        }


        public Result RemoveUser(String id, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("iz_user_id", OracleDbType.Varchar2, 50).Value = id;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_user.remove_user", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }

            return result;
        }


        public Result ChangePassword(String id, String password, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("iz_user_id", OracleDbType.Varchar2, 50).Value = id;
                cmd.Parameters.Add("iz_password", OracleDbType.Varchar2, 50).Value = password;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_user.change_password", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }

            return result;
        }

        public LoginResult Login(String id, String password, String locale) {
            return Login(id, password, "", locale);
        }
        public LoginResult Login(String id, String password, String Portal, String locale)
        {
            OracleCommand cmd = new OracleCommand();
            LoginResult result = new LoginResult();
            EncryptDAO enc = new EncryptDAO();

            try
            {

                cmd.Parameters.Add("iz_user_id", OracleDbType.Varchar2, 50).Value = (String.IsNullOrEmpty(id) ? null : id);
                cmd.Parameters.Add("iz_password", OracleDbType.Varchar2, 50).Value = (String.IsNullOrEmpty(password) ? null : enc.Encrypt(password));
                cmd.Parameters.Add("oz_last_name", OracleDbType.Varchar2, 50).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_name", OracleDbType.Varchar2, 50).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_email", OracleDbType.Varchar2, 100).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_telephone", OracleDbType.Int64).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_mobile", OracleDbType.Int64).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = null;
                cmd.Parameters.Add("iz_ip", OracleDbType.Varchar2, 50).Value = null;

                cmd.Parameters.Add("in_portal", OracleDbType.Int64, 1).Value =  Utils.ConvertToInt64(Portal,0);


                DataManager.RunDataCommandSP(ref cmd, "ip_pg_user.login", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (result.Code.Equals("0"))
                {
                    result.Email = cmd.Parameters["oz_email"].Value == null ? null : cmd.Parameters["oz_email"].Value.ToString();
                    result.LastName = cmd.Parameters["oz_last_name"].Value == null ? null : cmd.Parameters["oz_last_name"].Value.ToString();
                    result.Mobile = cmd.Parameters["on_mobile"].Value == null ? null : cmd.Parameters["on_mobile"].Value.ToString();
                    result.Name = cmd.Parameters["oz_name"].Value == null ? null : cmd.Parameters["oz_name"].Value.ToString();
                    result.Telephone = cmd.Parameters["on_telephone"].Value == null ? null : cmd.Parameters["on_telephone"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }

                enc = null;
            }

            return result;
        }

        public Result SaveUserOption(String id, String optionId, String allowView,
            String allowUpdate, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("iz_user_id", OracleDbType.Varchar2, 50).Value = String.IsNullOrEmpty(id) ? null : id;
                cmd.Parameters.Add("in_option_id", OracleDbType.Varchar2, 50).Value = String.IsNullOrEmpty(optionId) ? null : optionId;
                cmd.Parameters.Add("in_view_allowed", OracleDbType.Int32).Value = String.IsNullOrEmpty(allowView) ? null : allowView;
                cmd.Parameters.Add("in_update_allowed", OracleDbType.Int32).Value = String.IsNullOrEmpty(allowUpdate) ? null : allowUpdate;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Decimal).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = String.IsNullOrEmpty(locale) ? null : locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_user.save_option", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result RemoveUserOption(String id, String optionId, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("iz_user_id", OracleDbType.Varchar2, 50).Value = String.IsNullOrEmpty(id) ? null : id;
                cmd.Parameters.Add("in_option_id", OracleDbType.Varchar2, 50).Value = String.IsNullOrEmpty(optionId) ? null : optionId;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = String.IsNullOrEmpty(locale) ? null : locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_user.remove_option", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

    }
}
