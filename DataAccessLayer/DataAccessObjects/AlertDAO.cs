﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using Common;
using OracleAccessLayer;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class AlertResult : Result
    {
        public List<Alert> Alert { get; set; }
    }

    [Serializable]
    public class AlertDestinationResult : Result
    {
        public List<AlertDestination> AlertDestination { get; set; }
    }

    public class AlertDAO
    {

        public Result SendEmail(String email, String subject, String message)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("return_value", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("p_direccion_email", OracleDbType.Varchar2, 100).Value = email;
                cmd.Parameters.Add("p_subject", OracleDbType.Varchar2, 50).Value = subject;
                cmd.Parameters.Add("p_mensaje", OracleDbType.Varchar2, 10000).Value = message;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_alert.send_email", "", "");

                if (cmd.Parameters["return_value"].Value != null)
                {
                    result.Message = cmd.Parameters["return_value"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }

            return result;
        }

        public AlertDestinationResult SearchDestination(String id, String alertId, String email,
            String mobile)
        {
            AlertDestinationResult result = new AlertDestinationResult();
            List<AlertDestination> list = new List<AlertDestination>();
            StringBuilder query = new StringBuilder();
            DataTable dt;
            OracleCommand cmd = new OracleCommand();

            try
            {
                query.Append(" select alert_destination_id, alert_id, email, mobile ");
                query.Append(" from ip_vw_alert_destination ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "alert_destination_id", id, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "alert_id", alertId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "email", email.ToUpper(), OracleDbType.Varchar2, DataManager.eOperador.Like_Donde_Encuentre);
                DataManager.AddBindParamCmd(ref cmd, ref query, "mobile", mobile, OracleDbType.Int64);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new AlertDestination(
                            Convert.ToInt64(Utils.IsNull (row["alert_destination_id"],0).ToString()),
                            Convert.ToInt64(Utils.IsNull (row["alert_id"],0).ToString()),
                            Utils.IsNull (row["email"],"").ToString (),
                            Convert.ToInt64(Utils.IsNull (row["mobile"],0).ToString())));
                    }

                    result.AlertDestination = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }

            return result;
        }


        /// <summary>
        /// Gets all the alerts
        /// </summary>
        /// <returns>A list of Alerts</returns>
        public AlertResult Search(String id, String statusId, String description)
        {
            AlertResult result = new AlertResult();
            List<Alert> list = new List<Alert>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;
            OracleConnection con = new OracleConnection();
            
            try
            {
                query.Append(" select alert_id, alert_description, abbreviation, ");
                query.Append(" status_id, status_description ");
                query.Append(" from ip_vw_alert ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "alert_id", id, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "status_id", statusId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "alert_description", Common.Utils.IsNull(description, "").ToString().ToUpper(), OracleDbType.Varchar2, DataManager.eOperador.Like_Donde_Encuentre);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new Alert(
                            Convert.ToInt64(row["alert_id"].ToString()),
                            row["alert_description"] == null ? null : row["alert_description"].ToString(),
                            row["abbreviation"] == null ? null : row["abbreviation"].ToString(),
                            Convert.ToInt64(row["status_id"].ToString()),
                            row["status_description"] == null ? null : row["status_description"].ToString()));
                    }

                    result.Alert = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }

            return result;
        }

        
        /// <summary>
        /// Saves an Alert on the data base.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="description"></param>
        /// <param name="abbreviation"></param>
        /// <returns>Returns 1 if the alert was register and 0 if not</returns>
        public Result Save(String id, String description, String abbreviation, String statusId, String locale,
            String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_alert_id", OracleDbType.Int32, ParameterDirection.InputOutput).Value = (String.IsNullOrEmpty(id) ? null : id);
                cmd.Parameters.Add("iz_description", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(description) ? null : description);
                cmd.Parameters.Add("iz_abbreviation", OracleDbType.Varchar2, 20).Value = (String.IsNullOrEmpty(abbreviation) ? null : abbreviation);
                cmd.Parameters.Add("in_status_id", OracleDbType.Int32).Value = (String.IsNullOrEmpty(statusId) ? null : statusId);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_alert.save", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (result.Code.Equals("0"))
                {
                    result.Id = cmd.Parameters["in_alert_id"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }

            return result;
        }


        public Result Remove(String id, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_alert_id", OracleDbType.Int32).Value = id;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_alert.remove", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }

            return result;
        }


        public Result SaveDestination(String alertId, String email, String mobile, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("on_alert_destination", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("in_alert_id", OracleDbType.Int32).Value = alertId;
                cmd.Parameters.Add("iz_email", OracleDbType.Varchar2, 100).Value = email;
                cmd.Parameters.Add("iz_mobile", OracleDbType.Int64).Value = Common.Utils.IsNull(mobile, null);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_alert.save_destination", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (result.Code.Equals("0"))
                {
                    result.Id = cmd.Parameters["on_alert_destination"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }

            return result;
        }

        public Result RemoveDestination(String id, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_alert_destination", OracleDbType.Int32).Value = id;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_alert.remove_destination", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }

            return result;
        }
    }
}
