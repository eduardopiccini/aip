﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using Common;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class BillingTermResult : Result
    {
        public List<BillingTerm> BillingTerm { get; set; }
    }

    public class BillingTermDAO
    {
        public BillingTermResult Search(String id, String statusId,
            String description)
        {
            BillingTermResult result = new BillingTermResult();
            List<BillingTerm> list = new List<BillingTerm>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select billing_term_id, description, abbreviation, status_id, status_description ");
                query.Append(" from ip_vw_billing_term ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "billing_term_id", id, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "status_id", statusId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "description", Common.Utils.IsNull(description, "").ToString().ToUpper(), OracleDbType.Varchar2, DataManager.eOperador.Like_Donde_Encuentre);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new BillingTerm(
                            Convert.ToInt64(row["billing_term_id"].ToString()),
                            row["description"] == null ? null : row["description"].ToString(),
                            row["abbreviation"] == null ? null : row["abbreviation"].ToString(),
                            Convert.ToInt64(row["status_id"].ToString()),
                            row["status_description"] == null ? null : row["status_description"].ToString()));
                    }
                    result.BillingTerm = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        
        public Result Save(String id, String description, String abbreviation, String statusId, String locale,
            String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_billing_term_id", OracleDbType.Int32, ParameterDirection.InputOutput).Value = Common.Utils.IsNull(id, DBNull.Value);
                cmd.Parameters.Add("iz_description", OracleDbType.Varchar2, 100).Value = description;
                cmd.Parameters.Add("iz_abbreviation", OracleDbType.Varchar2, 20).Value = abbreviation;
                cmd.Parameters.Add("in_status_id", OracleDbType.Int32).Value = statusId;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_billing_term.save", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (result.Code.Equals("0"))
                {
                    result.Id = cmd.Parameters["in_billing_term_id"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result Remove(String id, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_billing_term_id", OracleDbType.Int32).Value = id;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_billing_term.remove", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }
    }
}
