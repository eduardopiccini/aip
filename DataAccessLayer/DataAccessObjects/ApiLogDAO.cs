﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using Common;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class ApiLogResult : Result
    {
        public List<ApiLog> ApiLog { get; set; }
    }

    [Serializable]
    public class ApiLogCountResult : Result
    {
        public Int64 Count { get; set; }
    }

    public class ApiLogDAO : IDisposable
    {
        public ApiLogResult Search(String id, String accountId, String transactionTypeId,
              String productTypeId, String deckProductId, String productId, String providerId,
              DateTime? dateBeginFrom, DateTime? dateBeginEnd, DateTime? dateEndFrom, DateTime? dateEndEnd,
              String countryId, String carrierId, String phone, String responseCode, String Success,String merchant, int startRowIndex, int maxRows)
        {
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            ApiLogResult result = new ApiLogResult();
            try
            {
                List<ApiLog> lstApiLog = new List<ApiLog>();
                query.Append(" select * from (select t.*, ROWNUM as rn from ( select log_id, date_begin, date_end, account_id, account_name, amount, discount, discount_amount, ");
                query.Append(" surcharge, transaction_type_id, transaction_type_name, product_type_id, ");
                query.Append(" product_type_name, reference, deck_product_id, deck_product_name, ");
                query.Append(" product_id, product_name, product_amount, currency_id, currency_name, ");
                query.Append(" country_id, country_name, carrier_id, carrier_name, city_id, city_name, ");
                query.Append(" phone, void_allowed, old_balance, new_balance, response_code_id, ");
                query.Append(" response_message, provider_account_id, provider_account_name, ");
                query.Append(" provider_template_id, provider_deck_product_id, provider_deck_product_name, ");
                query.Append(" provider_profile_id, merchant, ");
                query.Append(" provider_id, provider_name, provider_product_code, provider_country_code, ");
                query.Append(" provider_operator_code, provider_amount, provider_discount, ");
                query.Append(" provider_discount_amount, provider_reference, provider_serial, ");
                query.Append(" provider_old_balance, provider_new_balance, provider_response_code_id, ");
                query.Append(" provider_response_description, voided_transaction_id, provider_currency_id, ");
                query.Append(" provider_currency_rate, provider_currency_amount, ip, date_created, user_created, date_updated, user_updated ");
                query.Append(" from ip_vw_api_log ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "log_id", id, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "account_id", accountId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "transaction_type_id", transactionTypeId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "product_type_id", productTypeId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "deck_product_id", deckProductId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "product_id", productId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "provider_id", providerId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "country_id", countryId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "carrier_id", carrierId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "phone", phone, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "date_begin", dateBeginFrom, dateBeginEnd, OracleDbType.Date, DataManager.eOperador.Between);
                DataManager.AddBindParamCmd(ref cmd, ref query, "date_end", dateEndFrom, dateEndEnd, OracleDbType.Date, DataManager.eOperador.Between);
                DataManager.AddBindParamCmd(ref cmd, ref query, "provider_response_code_id", responseCode, OracleDbType.NVarchar2);
                
                if (Success.Trim() == "00") {
                    DataManager.AddBindParamCmd(ref cmd, ref query, "provider_response_code_id", "success", "00", OracleDbType.NVarchar2, DataManager.eOperador.Igual, false);
                }
                else if (Success.Trim() != "")
                {
                    DataManager.AddBindParamCmd(ref cmd, ref query, "provider_response_code_id","success", "00", OracleDbType.NVarchar2, DataManager.eOperador.Diferente, false);
                }
                DataManager.AddBindParamCmd(ref cmd, ref query, "merchant", merchant.ToUpper(), OracleDbType.NVarchar2);
                
                
                query.Append(" and rownum<= " + maxRows + startRowIndex + ") t) ");
                query.Append(" where rn >= " + startRowIndex + " and rownum <=" + maxRows.ToString());
                OracleConnection cn = new OracleConnection(DataManager.ConnStr("", ""));
                cmd.Connection = cn;
                cmd.CommandText = query.ToString();
                cmd.Connection.Open();
                OracleDataReader dr  = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lstApiLog.Add(new ApiLog
                        {
                            DateBegin = dr["date_begin"] == null ? null : dr["date_begin"].ToString(),
                            ProductName = dr["product_name"].ToString(),
                            Phone = dr["phone"] == null ? null : dr["phone"].ToString(),
                            Amount = dr["amount"] == null ? null : dr["amount"].ToString(),
                            DiscountAmount = dr["discount_amount"] == null ? null : dr["discount_amount"].ToString(),
                            Discount = dr["discount"].ToString(),
                            ProviderResponseCodeId = dr["provider_response_code_id"].ToString(),
                            ProviderResponseDescription = dr["provider_response_description"].ToString(),
                            AccountName = dr["account_name"].ToString(),
                            ProviderName = dr["provider_name"].ToString(),
                            ProviderAmount = dr["provider_amount"] == null ? null : dr["provider_amount"].ToString(),
                            ProviderDiscount = dr["provider_discount"] == null ? null : dr["provider_discount"].ToString(),
                            ProviderDiscountAmount = dr["provider_discount_amount"] == null ? null : dr["provider_discount_amount"].ToString(),
                            ProductTypeName = dr["product_type_name"].ToString(),
                            CarrierName = dr["carrier_name"].ToString(),
                            CityName = dr["city_name"].ToString(),
                            CountryName = dr["country_name"].ToString(),
                            Merchant = dr["merchant"].ToString(),
                            LogId = dr["log_id"].ToString(),
                            ResponseCodeId = dr["response_code_id"] == null ? null : dr["response_code_id"].ToString(),
                            ResponseMessage = dr["response_message"] == null ? null : dr["response_message"].ToString(),

                        });


                    }

                    result.ApiLog = lstApiLog;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }

                if (cmd.Connection.State == ConnectionState.Open) cmd.Connection.Close();
            }
            catch (Exception ex)
            {

                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    if (cmd.Connection != null)
                    {
                        if (cmd.Connection.State == ConnectionState.Open)
                        {
                            cmd.Connection.Close();
                        }
                    }
                    cmd.Dispose();
                }
            }


            return result;


        }


        public Int64? GetCountRoutingLog(String id, String accountId, String transactionTypeId,
             String productTypeId, String deckProductId, String productId, String providerId,
             DateTime? dateBeginFrom, DateTime? dateBeginEnd, DateTime? dateEndFrom, DateTime? dateEndEnd,
             String countryId, String carrierId, String phone, String responseCode, String Success, String merchant)
        {
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();

            //tiempo.Append("Llego al Select :" + DateTime.Now.ToString("hh:mm:ss tt") + " \n");
            query.Append(" select count(1) ");
            query.Append(" from ip_vw_api_log ");



            DataManager.AddBindParamCmd(ref cmd, ref query, "log_id", id, OracleDbType.Int64);
            DataManager.AddBindParamCmd(ref cmd, ref query, "account_id", accountId, OracleDbType.Int64);
            DataManager.AddBindParamCmd(ref cmd, ref query, "transaction_type_id", transactionTypeId, OracleDbType.Int64);
            DataManager.AddBindParamCmd(ref cmd, ref query, "product_type_id", productTypeId, OracleDbType.Int64);
            DataManager.AddBindParamCmd(ref cmd, ref query, "deck_product_id", deckProductId, OracleDbType.Int64);
            DataManager.AddBindParamCmd(ref cmd, ref query, "product_id", productId, OracleDbType.Int64);
            DataManager.AddBindParamCmd(ref cmd, ref query, "provider_id", providerId, OracleDbType.Int64);
            DataManager.AddBindParamCmd(ref cmd, ref query, "country_id", countryId, OracleDbType.Int32);
            DataManager.AddBindParamCmd(ref cmd, ref query, "carrier_id", carrierId, OracleDbType.Int32);
            DataManager.AddBindParamCmd(ref cmd, ref query, "phone", phone, OracleDbType.Int64);
            DataManager.AddBindParamCmd(ref cmd, ref query, "date_begin", dateBeginFrom, dateBeginEnd, OracleDbType.Date, DataManager.eOperador.Between);
            DataManager.AddBindParamCmd(ref cmd, ref query, "date_end", dateEndFrom, dateEndEnd, OracleDbType.Date, DataManager.eOperador.Between);
            DataManager.AddBindParamCmd(ref cmd, ref query, "provider_response_code_id", responseCode, OracleDbType.NVarchar2);

            if (Success.Trim() == "00")
            {
                DataManager.AddBindParamCmd(ref cmd, ref query, "provider_response_code_id", "success", "00", OracleDbType.NVarchar2, DataManager.eOperador.Igual, false);
            }
            else if (Success.Trim() != "")
            {
                DataManager.AddBindParamCmd(ref cmd, ref query, "provider_response_code_id", "success", "00", OracleDbType.NVarchar2, DataManager.eOperador.Diferente, false);
            }

            DataManager.AddBindParamCmd(ref cmd, ref query, "merchant", merchant.ToUpper(), OracleDbType.NVarchar2);

            return Common.Utils.ConvertToInt64(Common.Utils.IsNull(DataManager.GetExecuteScalar(ref cmd, query.ToString(), "", "").ToString(), 0)); ;
        }


        public Result SearchApiLogResponseCode(String logId)
        {
            Result result = new Result();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select response_code_id, response_message ");
                query.Append(" from ip_vw_api_log_response ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "log_id", logId, OracleDbType.Int64);

                if (Utils.IsNull(logId, "") == "") { throw new Exception("SearchApiLogResponseCode - LogId required."); }

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    result.Code = dt.Rows[0]["response_code_id"].ToString();
                    result.Message = dt.Rows[0]["response_message"].ToString();
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        #region IDisposable Members

        public void Dispose()
        {

        }

        #endregion
    }
}
