﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using Common;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class StatusResult : Result
    {
        public List<Status> Status { get; set; }
    }

    public class StatusDAO
    {
        /// <summary>
        /// Gets all the status register in the database
        /// </summary>
        /// <returns>A list of status</returns>
        public StatusResult Search(String id, String description)
        {
            List<Status> list = new List<Status>();
            StatusResult result = new StatusResult();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select status_id, description, abbreviation, ");
                query.Append(" date_created, user_created, date_updated, user_updated ");
                query.Append(" from ip_vw_status ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "status_id", id, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "description", Common.Utils.IsNull(description,"").ToString().ToUpper(), OracleDbType.Varchar2, DataManager.eOperador.Like_Donde_Encuentre);
                
                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new Status(
                            Convert.ToInt64(row["status_id"].ToString()),
                            row["description"] == null ? null : row["description"].ToString(),
                            row["abbreviation"] == null ? null : row["abbreviation"].ToString(),
                            row["date_created"] == null ? null : row["date_created"].ToString(),
                            row["user_created"] == null ? null : row["user_created"].ToString(),
                            row["date_updated"] == null ? null : row["date_updated"].ToString(),
                            row["user_updated"] == null ? null : row["user_updated"].ToString()));
                    }

                    result.Status = list;
                    result.Code = "0";
                    result.Message = "TRUE";   
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }

        public Result Add(String id, String description, String abbreviation, String locale,
            String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_status_id", OracleDbType.Int32).Value = id;
                cmd.Parameters.Add("iz_description", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(description) ? null : description);
                cmd.Parameters.Add("iz_abbreviation", OracleDbType.Varchar2, 20).Value = (String.IsNullOrEmpty(abbreviation) ? null : abbreviation);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Decimal).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_status.save", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result Modify(String id, String description, String abbreviation, String locale,
            String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_status_id", OracleDbType.Int32).Value = id;
                cmd.Parameters.Add("iz_description", OracleDbType.Varchar2, 100).Value = (String.IsNullOrEmpty(description) ? null : description);
                cmd.Parameters.Add("iz_abbreviation", OracleDbType.Varchar2, 20).Value = (String.IsNullOrEmpty(abbreviation) ? null : abbreviation);
                cmd.Parameters.Add("on_errorcode", OracleDbType.Decimal).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = (String.IsNullOrEmpty(locale) ? null : locale);
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_status.change", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public Result Remove(String id, String locale, String appUser)
        {
            OracleCommand cmd = new OracleCommand();
            Result result = new Result();

            try
            {
                cmd.Parameters.Add("in_status_id", OracleDbType.Int32).Value = id;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 50).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "ip_pg_status.remove", "", "");

                result.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                result.Message = cmd.Parameters["oz_errormessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }
    }
}
