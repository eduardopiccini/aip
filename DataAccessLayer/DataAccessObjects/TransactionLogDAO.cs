﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using Common;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class TransactionLogResult : Result
    {
        public List<TransactionLog> TransactionLog { get; set; }
    }

    public class CountTransactionLogResult : Result
    {
        public Int64 CountTransactionLog { get; set; }
    }

    public class TransactionLogWsResult : Result
    {
        public TransactionLogWsResult()
        {
            TransactionLogWs = new List<TransactionLogWs>();
        }

        public List<TransactionLogWs> TransactionLogWs { get; set; } 
    }


    [Serializable]
    public class TransactionLogWs
    {

        public String TransactionId { get; set; }
        public String ProductTypeId { get; set; }
        public String ProductId { get; set; }
        public String Amount { get; set; }
        public String Phone { get; set; }
        public String ResponseCode { get; set; }
        public String ResponseMessage { get; set; }
        public String ProviderReference { get; set; }
        public String TransactionTypeId { get; set; }
        public String TransactionTypeName { get; set; }
        public String Merchant { get; set; }
        public String DateCreated { get; set; }
    }

    public class TransactionLogDAO
    {
        public TransactionLogWsResult SearchByReference(String reference, String accountId)
        {
            TransactionLogWsResult result = new TransactionLogWsResult();
            List<TransactionLogWs> list = new List<TransactionLogWs>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" SELECT log_id transaction_id, product_type_id, product_id, amount, phone,  ");
                query.Append(" response_code_id response_code, response_message, provider_reference, transaction_type_id, transaction_type_name, merchant, date_created ");
                query.Append(" from ip_vw_transaction_log_ws ");
                
                DataManager.AddBindParamCmd(ref cmd, ref query, "reference", reference, OracleDbType.Varchar2);
                DataManager.AddBindParamCmd(ref cmd, ref query, "account_id", accountId, OracleDbType.Int32);
                
                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new TransactionLogWs
                        {
                            TransactionId = row["transaction_id"] == null ? null : row["transaction_id"].ToString(),
                            ProductId = row["product_id"] == null ? null : row["product_id"].ToString(),
                            ProductTypeId = row["product_type_id"] == null ? null : row["product_type_id"].ToString(),
                            Amount = row["amount"] == null ? null : row["amount"].ToString(),
                            Phone = row["phone"] == null ? null : row["phone"].ToString(),
                            ResponseCode = row["response_code"] == null ? null : row["response_code"].ToString(),
                            ResponseMessage = row["response_message"] == null ? null : row["response_message"].ToString(),
                            ProviderReference = row["provider_reference"] == null ? null : row["provider_reference"].ToString(),
                            TransactionTypeId = row["transaction_type_id"] == null ? null : row["transaction_type_id"].ToString(),
                            TransactionTypeName = row["transaction_type_name"] == null ? null : row["transaction_type_name"].ToString(),
                            Merchant = row["merchant"] == null ? null : row["merchant"].ToString(),
                            DateCreated = row["date_created"] == null ? null : row["date_created"].ToString()
                        });

                    }

                    result.TransactionLogWs = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        /// <summary>
        /// Get the transaction logs by the account id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A list of transaction logs</returns>
        public TransactionLogResult Search(String id, String logId, String accountId,
            String transactionTypeId, String productTypeId, String deckId, String productId,
            String providerId, DateTime? createdBegin, DateTime? createEnd,
            DateTime? updateBegin, DateTime? updateEnd, String userCreated, String userUpdated, String Reference, String Phone, String countryId, String carrierId, int? startRowIndex, int? maxRows)
        {
            TransactionLogResult result = new TransactionLogResult();
            List<TransactionLog> list = new List<TransactionLog>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {

                query.Append(" select * from (select t.*, ROWNUM as rn from (select transaction_id, log_id, date_begin, date_end, account_id, account_name, transaction_type_id, transaction_type_name, product_type_id, ");
                query.Append(" product_type_name, deck_id, deck_name, deck_product_id, deck_product_name, transaction_amount, transaction_discount, transaction_discount_amount, ");
                query.Append(" transaction_net_amount, surcharge, surcharge_amount, product_id, product_name, product_amount, void_allowed, reference, country_id, ");
                query.Append(" country_name, currency_id, currency_name, carrier_id, carrier_name, city_id, city_name, phone, old_balance, new_balance, response_code_id, response_message, ");
                query.Append(" provider_account_id, provider_account_name, provider_profile_id, provider_id, provider_name, provider_template_id, provider_deck_product_id, provider_deck_product_name, ");
                query.Append(" provider_product_code, provider_country_code, provider_operator_code, provider_reference, provider_serial, provider_amount, provider_discount_amount, ");
                query.Append(" provider_discount, provider_net_amount, provider_old_balance, provider_new_balance, provider_response_code_id, provider_response_description, provider_currency_id, ");
                query.Append(" provider_currency_rate, provider_currency_amount, invoice_id, provider_invoice_id, voided_transaction_id, ip, merchant, wholesale_price, retail_price, ");
                query.Append(" profit, profit_amount, date_created, user_created, user_updated, date_updated, app_user_created, app_user_updated ");


                query.Append(" from ip_vw_transaction_log ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "transaction_id", id, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "phone", Phone, OracleDbType.Int64);

                DataManager.AddBindParamCmd(ref cmd, ref query, "reference", Reference, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "log_id", logId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "account_id", accountId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "transaction_type_id", transactionTypeId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "product_type_id", productTypeId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "deck_id", deckId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "product_id", productId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "provider_id", providerId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "country_id", countryId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "carrier_id", carrierId, OracleDbType.Int64);


                DataManager.AddBindParamCmd(ref cmd, ref query, "date_begin", createdBegin, createEnd, OracleDbType.Date, DataManager.eOperador.Between);
                DataManager.AddBindParamCmd(ref cmd, ref query, "date_updated", updateBegin, updateEnd, OracleDbType.Date, DataManager.eOperador.Between);
                DataManager.AddBindParamCmd(ref cmd, ref query, "user_created", userCreated, OracleDbType.Varchar2);
                DataManager.AddBindParamCmd(ref cmd, ref query, "user_updated", userUpdated, OracleDbType.Varchar2);

                query.Append(" order by date_created desc) t) ");
                query.Append(" where rn >= " + startRowIndex + " and rn <=" + (startRowIndex + maxRows).ToString());

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new TransactionLog 
                        {
                            Id = row["transaction_id"] == null ? null : row["transaction_id"].ToString(),
                            LogId = row["log_id"] == null ? null : row["log_id"].ToString(),
                            DateBegin = row["date_begin"] == null ? null : Convert.ToDateTime(row["date_begin"]).ToString("MM/dd/yyyy"),
                            DateEnd = row["date_end"] == null ? null : row["date_end"].ToString(),
                            AccountId = row["account_id"] == null ? null : row["account_id"].ToString(),
                            AccountName = row["account_name"] == null ? null : row["account_name"].ToString(),
                            TransactionTypeId = row["transaction_type_id"] == null ? null : row["transaction_type_id"].ToString(),
                            TransactionTypeName = row["transaction_type_name"] == null ? null : row["transaction_type_name"].ToString(),
                            DeckId = row["deck_id"] == null ? null : row["deck_id"].ToString(),
                            DeckName = row["deck_name"] == null ? null : row["deck_name"].ToString(),
                            DeckProductId = row["deck_product_id"] == null ? null : row["deck_product_id"].ToString(),
                            DeckProductName = row["deck_product_name"] == null ? null : row["deck_product_name"].ToString(),
                            TransactionAmount = row["transaction_amount"] == null ? null : row["transaction_amount"].ToString(),
                            TransactionDiscount = row["transaction_discount"] == null ? null : row["transaction_discount"].ToString(),
                            TransactionDiscountAmount = row["transaction_discount_amount"] == null ? null : row["transaction_discount_amount"].ToString(),
                            Surcharge = row["surcharge"] == null ? null : row["surcharge"].ToString(),
                            SurchargeAmount = row["surcharge_amount"] == null ? null : row["surcharge_amount"].ToString(),
                            ProductId = row["product_id"] == null ? null : row["product_id"].ToString(),
                            ProductName = row["product_name"] == null ? null : row["product_name"].ToString(),
                            ProductAmount = row["product_amount"] == null ? null : row["product_amount"].ToString(),
                            VoidAllowed = row["void_allowed"] == null ? null : row["void_allowed"].ToString(),
                            Reference = row["reference"] == null ? null : row["reference"].ToString(),
                            CountryId = row["country_id"] == null ? null : row["country_id"].ToString(),
                            CountryName = row["country_name"] == null ? null : row["country_name"].ToString(),
                            CurrencyId = row["currency_id"] == null ? null : row["currency_id"].ToString(),
                            CurrencyName = row["currency_name"] == null ? null : row["currency_name"].ToString(),
                            CarrierId = row["carrier_id"] == null ? null : row["carrier_id"].ToString(),
                            CarrierName = row["carrier_name"] == null ? null : row["carrier_name"].ToString(),
                            CityId = row["city_id"] == null ? null : row["city_id"].ToString(),
                            CityName = row["city_name"] == null ? null : row["city_name"].ToString(),
                            Phone = row["phone"] == null ? null : row["phone"].ToString(),
                            OldBalance = row["old_balance"] == null ? null : row["old_balance"].ToString(),
                            NewBalance = row["new_balance"] == null ? null : row["new_balance"].ToString(),
                            ResponseCodeId = row["response_code_id"] == null ? null : row["response_code_id"].ToString(),
                            ResponseMessage = row["response_message"] == null ? null : row["response_message"].ToString(),
                            ProviderAccountId = row["provider_account_id"] == null ? null : row["provider_account_id"].ToString(),
                            ProviderAccountName = row["provider_account_name"] == null ? null : row["provider_account_name"].ToString(),
                            ProviderProfileId = row["provider_profile_id"] == null ? null : row["provider_profile_id"].ToString(),
                            ProviderId = row["provider_id"] == null ? null : row["provider_id"].ToString(),
                            ProviderName = row["provider_name"] == null ? null : row["provider_name"].ToString(),
                            ProviderTemplateId = row["provider_template_id"] == null ? null : row["provider_template_id"].ToString(),
                            ProviderDeckProductId = row["provider_deck_product_id"] == null ? null : row["provider_deck_product_id"].ToString(),
                            ProviderDeckProductName = row["provider_deck_product_name"] == null ? null : row["provider_deck_product_name"].ToString(),
                            ProviderProductCode = row["provider_product_code"] == null ? null : row["provider_product_code"].ToString(),
                            ProviderCountryCode = row["provider_country_code"] == null ? null : row["provider_country_code"].ToString(),
                            ProviderOperatorCode = row["provider_operator_code"] == null ? null : row["provider_operator_code"].ToString(),
                            ProviderReference = row["provider_reference"] == null ? null : row["provider_reference"].ToString(),
                            ProviderSerial = row["provider_serial"] == null ? null : row["provider_serial"].ToString(),
                            ProviderAmount = row["provider_amount"] == null ? null : row["provider_amount"].ToString(),
                            ProviderDiscount = row["provider_discount"] == null ? null : row["provider_discount"].ToString(),
                            ProviderDiscountAmount = row["provider_discount_amount"] == null ? null : row["provider_discount_amount"].ToString(),
                            ProviderOldBalance = row["provider_old_balance"] == null ? null : row["provider_old_balance"].ToString(),
                            ProviderNewBalance = row["provider_new_balance"] == null ? null : row["provider_new_balance"].ToString(),
                            ProviderResponseCodeId = row["provider_response_code_id"] == null ? null : row["provider_response_code_id"].ToString(),
                            ProviderResponseDescription = row["provider_response_description"] == null ? null : row["provider_response_description"].ToString(),
                            ProviderCurrencyId = row["provider_currency_id"] == null ? null : row["provider_currency_id"].ToString(),
                            ProviderCurrencyRate = row["provider_currency_rate"] == null ? null : row["provider_currency_rate"].ToString(),
                            ProviderCurrencyAmount = row["provider_currency_amount"] == null ? null : row["provider_currency_amount"].ToString(),
                            InvoiceId = row["invoice_id"] == null ? null : row["invoice_id"].ToString(),
                            ProviderInvoiceId = row["provider_invoice_id"] == null ? null : row["provider_invoice_id"].ToString(),
                            VoidedTransactionId = row["voided_transaction_id"] == null ? null : row["voided_transaction_id"].ToString(),
                            Ip = row["ip"] == null ? null : row["ip"].ToString(),
                            Profit = row["profit"] == null ? null : row["profit"].ToString(),
                            ProfitAmount = row["profit_amount"] == null ? null : row["profit_amount"].ToString(),
                            UserCreated = row["user_created"] == null ? null : row["user_created"].ToString(),
                            DateCreated = row["date_created"] == null ? null : row["date_created"].ToString(),
                            UserUpdated = row["user_updated"] == null ? null : row["user_updated"].ToString(),
                            DateUpdated = row["date_updated"] == null ? null : row["date_updated"].ToString(),
                            Merchant = row["merchant"] == null ? null : row["merchant"].ToString(),
                            ProductTypeId = row["product_type_id"] == null ? null : row["product_type_id"].ToString(),
                            ProductTypeName = row["product_type_name"] == null ? null : row["product_type_name"].ToString(),
                            ProviderNetAmount = row["provider_net_amount"] == null ? null : row["provider_net_amount"].ToString(),
                            TransactionNetAmount = row["transaction_net_amount"] == null ? null : row["transaction_net_amount"].ToString(),
                        });

                    }

                    result.TransactionLog = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }


        public CountTransactionLogResult SearchCount(String id, String logId, String accountId,
         String transactionTypeId, String productTypeId, String deckId, String productId,
         String providerId, DateTime? createdBegin, DateTime? createEnd,
         DateTime? updateBegin, DateTime? updateEnd, String userCreated, String userUpdated, String Reference, String Phone, String countryId, String carrierId)
        {
            CountTransactionLogResult result = new CountTransactionLogResult();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();

            try
            {

                query.Append("select count(1) from ip_vw_transaction_log ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "transaction_id", id, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "phone", Phone, OracleDbType.Int64);

                DataManager.AddBindParamCmd(ref cmd, ref query, "reference", Reference, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "log_id", logId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "account_id", accountId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "transaction_type_id", transactionTypeId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "product_type_id", productTypeId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "deck_id", deckId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "product_id", productId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "provider_id", providerId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "country_id", countryId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "carrier_id", carrierId, OracleDbType.Int64);


                DataManager.AddBindParamCmd(ref cmd, ref query, "date_begin", createdBegin, createEnd, OracleDbType.Date, DataManager.eOperador.Between);
                DataManager.AddBindParamCmd(ref cmd, ref query, "date_updated", updateBegin, updateEnd, OracleDbType.Date, DataManager.eOperador.Between);
                DataManager.AddBindParamCmd(ref cmd, ref query, "user_created", userCreated, OracleDbType.Varchar2);
                DataManager.AddBindParamCmd(ref cmd, ref query, "user_updated", userUpdated, OracleDbType.Varchar2);


                result.CountTransactionLog = Utils.ConvertToInt64(Utils.IsNull(DataManager.GetExecuteScalar(ref cmd, query.ToString(), "", ""), 0));
                result.Code = "0";
                result.Message = "TRUE";

            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }
    }
}
