﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class BlockExceptionResult : Result
    {
        public List<BlockException> BlockException { get; set; }
    }

    public class BlockExceptionDAO
    {
        public BlockExceptionResult Search(String id, String description)
        {
            BlockExceptionResult result = new BlockExceptionResult();
            List<BlockException> list = new List<BlockException>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select exception_type_id, description ");
                query.Append(" from ip_VW_BLOCK_EXCEPTION ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "exception_type_id", id, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "description", Common.Utils.IsNull(description,"").ToString().ToUpper(), OracleDbType.Varchar2, DataManager.eOperador.Like_Donde_Encuentre);
                
                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new BlockException(
                            row["exception_type_id"] == null ? null : row["exception_type_id"].ToString(),
                            row["description"] == null ? null : row["description"].ToString()));
                    }
                    result.BlockException = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }
    }
}
