﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using Common;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class ErrorLogResult : Result
    {
        public List<ErrorLog> ErrorLog { get; set; }
    }

    public class ErrorLogDAO
    {
        public ErrorLogResult Search(String id, DateTime? createdBegin, DateTime? createdEnd, String level,
            String code)
        {
            ErrorLogResult result = new ErrorLogResult();
            List<ErrorLog> list = new List<ErrorLog>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {
                query.Append(" select error_log_id, date_created, error_message, error, description,  error_level ");
                query.Append(" from ip_vw_error_log ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "error_log_id", (id == null) ? null : id, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "date_created", createdBegin, createdEnd, OracleDbType.Date, DataManager.eOperador.Between);
                DataManager.AddBindParamCmd(ref cmd, ref query, "error_level", (level == null) ? null : level.ToUpper(), OracleDbType.Varchar2);
                DataManager.AddBindParamCmd(ref cmd, ref query, "error", (code == null) ? null : code, OracleDbType.Int32);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new ErrorLog(
                            row["error_log_id"] == null ? null : row["error_log_id"].ToString(),
                            row["date_created"] == null ? null : row["date_created"].ToString(),
                            row["error_message"] == null ? null : row["error_message"].ToString(),
                            row["error"] == null ? null : row["error"].ToString(),
                            row["description"] == null ? null : row["description"].ToString(),
                            row["error_level"] == null ? null : row["error_level"].ToString()));
                    }

                    result.ErrorLog = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }
    }
}
