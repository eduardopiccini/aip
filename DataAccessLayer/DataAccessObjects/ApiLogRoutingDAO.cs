﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.EntityBase;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using Common;

namespace DataAccessLayer.DataAccessObjects
{
    [Serializable]
    public class ApiLogRoutingResult : Result
    {
        public List<ApiLogRouting> ApiRoutingParameter { get; set; }
    }

    public class ApiLogRoutingDAO
    {

        public ApiLogRoutingResult Search(String id, String logId, DateTime? dateBegin, DateTime? dateEnd)
        {
            ApiLogRoutingResult result = new ApiLogRoutingResult();
            List<ApiLogRouting> list = new List<ApiLogRouting>();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt;

            try
            {

                query.Append(" select routing_id, log_id, routing_mode, provider_id, provider_name, provider_account_id, provider_account_name, ");
                query.Append(" product_id, product_name, template_id, deck_product_id, weight, profile_id, product_code, country_code, ");
                query.Append(" operator_code, product_type_id, vendor_group_id, amount, currency_id, currency_rate, currency_amount, ");
                query.Append(" phone, ip,  port, url, date_created, user_created, app_user_created, app_user_updated ");
                query.Append(" from   ip_vw_api_log_routing ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "routing_id", id, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "log_id", logId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "date_created", dateBegin, dateEnd, OracleDbType.Date, DataManager.eOperador.Between);


                dt = DataManager.GetDataTable(ref cmd, query.ToString(), "", "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {

                        list.Add(new ApiLogRouting { LogId = Common.Utils.IsNull(row["log_id"], "").ToString(), Amount = Common.Utils.IsNull(row["amount"], "").ToString(), Url = Common.Utils.IsNull(row["url"], "").ToString(), Ip = Common.Utils.IsNull(row["ip"], "").ToString(), Port = Common.Utils.IsNull(row["port"], "").ToString(), Phone = Common.Utils.IsNull(row["phone"], "").ToString(), ProductId = Common.Utils.IsNull(row["product_id"], "").ToString(), ProductName = Common.Utils.IsNull(row["product_name"], "").ToString(), ProviderId = Common.Utils.IsNull(row["provider_id"], "").ToString(), ProviderName = Common.Utils.IsNull(row["provider_name"], "").ToString(), CurrencyId = Common.Utils.IsNull(row["currency_id"], "").ToString(), CurrencyAmount = Common.Utils.IsNull(row["currency_amount"], "").ToString(), CurrencyRate = Common.Utils.IsNull(row["currency_rate"], "").ToString(), RoutingId = Common.Utils.IsNull(row["routing_id"], "").ToString(), TemplateId = Common.Utils.IsNull(row["template_id"], "").ToString(), VendorGroupId = Common.Utils.IsNull(row["vendor_group_id"], "").ToString(), RoutingMode = Common.Utils.IsNull(row["routing_mode"], "").ToString(), ProviderAccountId = Common.Utils.IsNull(row["provider_account_id"], "").ToString(), ProviderAccountName = Common.Utils.IsNull(row["provider_account_name"], "").ToString(), DateCreated = Common.Utils.IsNull(row["date_created"], "").ToString(), AppUserCreated = Common.Utils.IsNull(row["app_user_created"], "").ToString(), AppUserUpdated = Common.Utils.IsNull(row["app_user_updated"], "").ToString() });
                    }

                    result.ApiRoutingParameter = list;
                    result.Code = "0";
                    result.Message = "TRUE";
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Details = ex.Message;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return result;
        }
    }
}
