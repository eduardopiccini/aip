﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;

namespace DataAccessLayer
{
    [Serializable]
    public class NewBalanceResult : Result
    {
        public NewBalanceResult()
        {
        }

        private Decimal balance;

        public Decimal Balance
        {
            get { return balance; }
            set { balance = value; }
        }
    }
}
