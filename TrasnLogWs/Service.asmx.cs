﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DataAccessLayer.DataAccessObjects;
using log4net;
using System.Text;

namespace TrasnLogWs
{
    [Serializable]
    public class LogRequest
    {
        public String RoutingId { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public String Code { get; set; }
        public String Message { get; set; }
        public String AuthorizationNumber { get; set; }
        public String SerialNumber { get; set; }
        public String PinNumber { get; set; }
        public String Locale { get; set; }
        public String DbUser { get; set; }
        public String TransactionDateTime { get; set; }
    }


    /// <summary>
    /// Summary description for Service
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Service : System.Web.Services.WebService
    {
        private static readonly ILog log = LogManager.GetLogger("Log");

        [WebMethod]
        public void Log(LogRequest Request)
        {
            RoutingTransactionLogResult result = null, resultWs = null;
            StringBuilder output = new StringBuilder();
            String exception = String.Empty;

            try
            {
                using (RoutingDAO dao = new RoutingDAO())
                {
                    result = dao.TransLog(Request.RoutingId, Request.BeginDate, Request.EndDate,
                        Request.Code,
                        Request.Message,
                        Request.AuthorizationNumber,
                        Request.SerialNumber,
                        Request.PinNumber,
                        Request.Locale,
                        Request.DbUser,
                        Request.TransactionDateTime);

                    if (!result.Code.Equals("0"))
                    {
                        resultWs = dao.TransLogWs(Request.RoutingId, "9", Request.BeginDate, Request.EndDate,
                            Request.Code, Request.Message,
                            Request.AuthorizationNumber, Request.SerialNumber, Request.PinNumber, Request.Locale,
                            Request.DbUser, Request.TransactionDateTime, result.Details);
                    }
                }
            }
            catch (Exception ex)
            {
                exception = ex.Message;
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "TRANS_LOG:", false, true);
                Common.Utils.Concat(ref output, "RoutingId", Request.RoutingId, false, false);
                Common.Utils.Concat(ref output, "BeginDate", Request.BeginDate, false, false);
                Common.Utils.Concat(ref output, "EndDate", Request.EndDate, false, false);
                Common.Utils.Concat(ref output, "Code", Request.Code, false, false);
                Common.Utils.Concat(ref output, "Message", Request.Message, false, false);
                Common.Utils.Concat(ref output, "AuthNumber", Request.AuthorizationNumber, false, false);
                Common.Utils.Concat(ref output, "SerialNumber", Request.SerialNumber, false, false);
                Common.Utils.Concat(ref output, "PinNumber", Request.PinNumber, false, false);
                Common.Utils.Concat(ref output, "Locale", Request.Locale, false, false);
                Common.Utils.Concat(ref output, "DbUser", Request.DbUser, false, false);
                Common.Utils.Concat(ref output, "TransDate", Request.TransactionDateTime, false, false);
                Common.Utils.Concat(ref output, "Exception", exception, false, false);

                if (result != null)
                {
                    Common.Utils.Concat(ref output, "ResultCode", result.Code, false, false);
                    Common.Utils.Concat(ref output, "ResultMessage", result.Message, false, false);
                }

                if (resultWs != null)
                {
                    Common.Utils.Concat(ref output, "ResultWsCode", resultWs.Code, false, false);
                    Common.Utils.Concat(ref output, "ResultWsMessage", resultWs.Message, false, false);
                }

                log.Debug(output.ToString());
            }
        }
    }
}
