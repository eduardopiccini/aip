﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TestWcf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            PrasanWcf.IappClient wcf = new PrasanWcf.IappClient();
            PrasanWcf.RechargeRequest req = new PrasanWcf.RechargeRequest();
            PrasanWcf.RechargeResponse resp = null;

            req.TransactionId = "101";
            req.Amount = "10";
            req.DbUser = "aiprs";
            req.Locale = "en-US";
            req.LoginId = "n3z62jgn";
            req.Phone = "8096291609";
            req.ProductId = "1";
            req.ProtectedKey = "23mvq7bj";
            req.Type = "Flexi";
            req.Url = "http://204.15.169.241/WebService/iTopUp/reseller_itopup.server.php";

            resp = wcf.Recharge(req);
        }
    }
}
