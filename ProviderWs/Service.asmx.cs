﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Common;
using DataAccessLayer.DataAccessObjects;
using System.Text;
using ProviderWs.Internacional;
using ProviderWs.Locales;
using log4net;
using ProviderWs.App_Code.Internacional;
using ProviderWs.App_Code.Locales;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Globalization;
using ProviderWs.PlWcf;

namespace ProviderWs
{
    /// <summary>
    /// Summary description for Service
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Service : System.Web.Services.WebService
    {
        public delegate void TransactionLog_Delegate(String routingId, DateTime beginDate, DateTime endDate, Transaction transaction, String locale, String dbuser);

        private readonly String UNLIMITED_PINS = "1";
        private readonly String TRANSFER_TO = "13";
        private readonly String ISEND = "2";
        private readonly String MORE_MAGIC = "3";
        private readonly String SBT = "4";
        private readonly String PRASAN = "5";
        private readonly String EASYCALL = "6";
        private readonly String NATIONAL = "7";
        private readonly String TRICOM = "8";
        private readonly String CLARO_LOCAL_UTD = "9";
        private readonly String ORANGE = "10";
        private readonly String VIVA = "11";
        private readonly String CLARO_INTERNACIONAL = "12";
        private readonly String EASYPHONE = "14";
        private readonly String DEV_PATEL = "15";
        private readonly String MOVIL_BANRESERVAS = "261";
        private readonly String CLARO_LOCAL_FREESTAR = "262";
        private readonly String TWI = "223";
        private readonly String OFFICIAL_PREPAID_NATION = "502";
        private readonly String AIPC_PREPAID_NATION = "982";
        private readonly String SOLOPIN = "21";
        private readonly String SOLOPIN_4G = "22";
        private readonly String PUBLICTECH = "163";
        private readonly String QUIERO_RECARGAS = "803";
        private readonly String MIDASCARD = "241";
        private readonly String IDEALTEL = "702";
        private readonly String MIPAIS = "781";
        private readonly String TPAGO = System.Configuration.ConfigurationManager.AppSettings["tpago_utp_id"].ToString();
        private readonly String DOMINICAN_MOVIL = System.Configuration.ConfigurationManager.AppSettings["dominican_movil_prov_id"].ToString();

        private readonly String PINLESS = "pinless";
        private readonly String PINLESS_4G = "pinless4g";

        private readonly String TRUMPIA_OFFICIAL = System.Configuration.ConfigurationManager.AppSettings["trumpia_official"].ToString();

        private readonly String LOCAL_PIN = System.Configuration.ConfigurationManager.AppSettings["local_pin"].ToString();
        private readonly String PIN = System.Configuration.ConfigurationManager.AppSettings["pin"].ToString();
        private readonly String RECHARGE = System.Configuration.ConfigurationManager.AppSettings["recharge"].ToString();

        private readonly String DM_RECHARGE_TYPE = System.Configuration.ConfigurationManager.AppSettings["dm_recharge_type"].ToString();
        private readonly String DM_ACTIVATION_TYPE = System.Configuration.ConfigurationManager.AppSettings["dm_activation_type"].ToString();

        private readonly String PINLESS_USER = System.Configuration.ConfigurationManager.AppSettings["pinless_user"].ToString();
        private readonly String PINLESS_PASS = System.Configuration.ConfigurationManager.AppSettings["pinless_password"].ToString();

        private readonly String MIPAIS_ACTIVATION = "3";
        private readonly String MIPAIS_RECHARGE = "4";

        private readonly String AYB_PUBLITECH = "publitech";
        private readonly String AYB_QUIERO_RECARGA = "quiero_recarga";

        private static readonly ILog log = LogManager.GetLogger("ProviderWs");
        private Int32 TimeoutNoTimeout;
        
        public Service()
        {
            ServicePointManager.ServerCertificateValidationCallback =
               delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
               {
                   return true;
               };

            

            this.TimeoutNoTimeout = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["timeout_no_timeout"].ToString());
        }

        public void TransactionLog(String routingId, DateTime beginDate, DateTime endDate, Transaction transaction, String locale, String dbuser)
        {
            RoutingTransactionLogResult tranResult = null, tranWsResult = null;
            StringBuilder output = new StringBuilder();

            using (RoutingDAO dao = new RoutingDAO())
            {
                output.AppendLine("Routing Id : ");
                output.Append(routingId);
                output.Append("|Begin Date : ");
                output.Append(beginDate.ToString());
                output.Append("|End Date : ");
                output.Append(endDate.ToString());
                output.Append("|Transaction Code : ");
                output.Append(transaction.Code);
                output.Append("|Transaction Message : ");
                output.Append(transaction.Message);
                output.Append("|Authorization Number : ");
                output.Append(transaction.AuthorizationNumber);
                output.Append("|Serial Number : ");
                output.Append(transaction.SerialNumber);
                output.Append("|Pin Number : ");
                output.Append(transaction.PinNumber);
                output.Append("|Transaction DateTime : ");
                output.Append(transaction.TransactionDateTime);
                

                tranResult = dao.TransLog(routingId, beginDate, endDate,
                    transaction.Code,
                    transaction.Message,
                    transaction.AuthorizationNumber,
                    transaction.SerialNumber,
                    transaction.PinNumber,
                    locale,
                    dbuser,
                    transaction.TransactionDateTime);

                output.Append("|Tran Result Code : ");
                output.AppendLine(tranResult.Code);
                output.Append("|Tran Result Message : ");
                
                if (!tranResult.Code.Equals("0"))
                {
                    output.Append(tranResult.Message);
                }
                else
                {
                    output.AppendLine(tranResult.Message);
                }
                
                

                if (!tranResult.Code.Equals("0"))
                {
                    tranWsResult = dao.TransLogWs(routingId, "9", beginDate, endDate,
                        transaction.Code, transaction.Message,
                        transaction.AuthorizationNumber, transaction.SerialNumber, transaction.PinNumber, locale,
                        dbuser, transaction.TransactionDateTime, tranResult.Details);

                    output.Append("|Tran Ws Result Code : ");
                    output.Append(tranWsResult.Code);
                    output.Append("|Tran Ws Result Message   : ");
                    output.AppendLine(tranWsResult.Message);
                }
            }

            log.Debug(output.ToString());
        }

        [WebMethod]
        public Transaction Buy(RoutingResult routingResult, String typeDb, String phone, String amount,
            String smsMessage, String accountId, String locale, String dbUser, String user, String ip, String plLocale,
            int plPhoneNotification)
        {
            Transaction transactionResult = new Transaction();
            Transaction ackTran = new Transaction();
            RoutingResult result = new RoutingResult();
            StringBuilder output = new StringBuilder();
            DateTime beginDate, endDate;
            RoutingTransactionLogResult tranResult = null, tranWsResult = null;
            StringBuilder resultProvider = new StringBuilder();
            DateTime? dateSend = null, dateRecv = null;
            String lastProviderId = null;
            ResponseCodeDAO responseCodeDao = new ResponseCodeDAO();
            ProviderDAO providerDao = new ProviderDAO();
            String transactionId = null;
            Common.Result returnResponse;
            TransactionLog_Delegate transactionLog = null;
            IAsyncResult asyncTransactionLog = null;
            Boolean stopRouting = false;
            Double totalSegundos = 0;
            String details = String.Empty;

            
            try
            {
                
                transactionLog = new TransactionLog_Delegate(TransactionLog);

                foreach (RoutingProvider item in routingResult.RoutingProvider)
                {
                    beginDate = DateTime.Now;
                    lastProviderId = item.ProviderId;

                    if (typeDb.Equals("N"))
                    {
                        if (item.ProviderId.Equals("1")) //Unlimited Pins
                        {
                            dateSend = DateTime.Now;

                            if (item.ProductTypeId.Equals(RECHARGE))
                            {
                                using (UnlimitedPins UnlimitedPins = new UnlimitedPins())
                                {
                                    transactionResult = UnlimitedPins.Recharge(item.ProductCode, phone,
                                    item.CountryCode, item.CurrencyAmount, item.RoutingId, item.TerminalId);
                                }
                            }
                            else if (item.ProductTypeId.Equals(PIN))
                            {
                                using (UnlimitedPins UnlimitedPins = new UnlimitedPins())
                                {
                                    transactionResult = UnlimitedPins.GetPin(item.ProductCode, item.CurrencyAmount, item.RoutingId);
                                }
                            }

                            dateRecv = DateTime.Now;
                        }
                        else if (item.ProviderId.Equals("13"))  //Transfer To
                        {
                            dateSend = DateTime.Now;

                            using (TransferTo TransferTo = new TransferTo())
                            {
                                transactionResult = TransferTo.Recharge(item.RoutingId, phone, item.ProductCode, item.ProviderOperatorId,
                                item.Url);
                            }

                            dateRecv = DateTime.Now;

                        }
                        else if (item.ProviderId.Equals("2")) //iSend
                        {
                            dateSend = DateTime.Now;

                            using (Isend Isend = new Isend())
                            {
                                transactionResult = Isend.Recharge(phone, item.LocalCurrencyAmount, item.ProductCode,
                                item.RoutingId, item.Url);
                            }

                            dateRecv = DateTime.Now;
                        }
                        
                        else if (item.ProviderId.Equals("3")) //More Magic
                        {
                            dateSend = DateTime.Now;

                            using (MoreMagic MoreMagic = new MoreMagic())
                            {
                                transactionResult = MoreMagic.Recharge(phone, item.CurrencyAmount, item.RoutingId);
                            }

                            dateRecv = DateTime.Now;
                        }
                        else if (item.ProviderId.Equals("4")) //Sbt
                        {

                            dateSend = DateTime.Now;

                            using (Sbt Sbt = new Sbt())
                            {
                                transactionResult = Sbt.Recharge(item.CurrencyAmount, item.CountryCode, phone, item.RoutingId, item.OperatorCode);
                            }

                            dateRecv = DateTime.Now;
                        }
                        else if (item.ProviderId.Equals("5")) //Prasan
                        {
                            dateSend = DateTime.Now;

                            if (item.TransactionMode.Equals("0")) //Fix
                            {
                                using (Prasan Prasan = new Prasan())
                                {
                                    transactionResult = Prasan.Recharge(phone, String.Empty, item.ProductCode, item.RoutingId,
                                        item.Url, Common.Utils.FIX_RECHARGE, Prasan.TYPE_PRASAN);
                                }
                            }
                            else
                            {
                                using (Prasan Prasan = new Prasan())
                                {
                                    transactionResult = Prasan.Recharge(phone, item.LocalCurrencyAmount, item.ProductCode, item.RoutingId,
                                        item.Url, Common.Utils.FLEXI_RECHARGE, Prasan.TYPE_PRASAN);
                                }
                            }

                            dateRecv = DateTime.Now;
                        }
                        else if (item.ProviderId.Equals("6")) //EasyCall
                        {
                            if (item.ProductTypeId.Equals(RECHARGE))
                            {
                                dateSend = DateTime.Now;

                                using (EasyCall EasyCall = new EasyCall())
                                {
                                    transactionResult = EasyCall.Recharge(item.Phone, item.CurrencyAmount,
                                    item.ProductCode, item.RoutingId, item.Url);
                                }

                                dateRecv = DateTime.Now;
                            }
                            else if (item.ProductTypeId.Equals(PIN))
                            {
                                dateSend = DateTime.Now;

                                using (EasyCall EasyCall = new EasyCall())
                                {
                                    transactionResult = EasyCall.GetPin(item.ProductCode, item.RoutingId, item.Url);
                                }

                                dateRecv = DateTime.Now;
                            }

                            if (transactionResult.Code.Equals(Common.Utils.APPROVED))
                            {
                                if (item.ProductTypeId.Equals(RECHARGE))
                                {
                                    using (EasyCall EasyCall = new EasyCall())
                                    {
                                        ackTran = EasyCall.ConfirmRecharge(transactionResult.AuthorizationNumber, item.Url);
                                    }
                                }
                                else if (item.ProductTypeId.Equals(PIN))
                                {
                                    using (EasyCall EasyCall = new EasyCall())
                                    {
                                        ackTran = EasyCall.ConfirmPin(transactionResult.AuthorizationNumber, item.Url);
                                    }
                                }

                                if (!ackTran.Code.Equals(Common.Utils.APPROVED))
                                {
                                    transactionResult.Code = ackTran.Code;
                                    transactionResult.Message = ackTran.Message;
                                }
                            }
                        }
                        else if (item.ProviderId.Equals("7")) //National
                        {
                            if (item.ProductTypeId.Equals(RECHARGE))
                            {
                                dateSend = DateTime.Now;

                                using (National National = new National())
                                {
                                    transactionResult = National.Recharge(item.ProductCode, item.CurrencyAmount, item.CountryCode, phone);
                                }

                                dateRecv = DateTime.Now;
                            }
                            else if (item.ProductTypeId.Equals(PIN))
                            {
                                dateSend = DateTime.Now;

                                using (National National = new National())
                                {
                                    transactionResult = National.GetPin(item.ProductCode, item.CurrencyAmount);
                                }

                                dateRecv = DateTime.Now;
                            }
                        }
                        else if (item.ProviderId.Equals("8")) //Tricom
                        {
                            dateSend = DateTime.Now;

                            using (Tricom Tricom = new Tricom())
                            {
                                transactionResult = Tricom.Recharge(phone, item.LocalCurrencyAmount, item.RoutingId, item.Ip, Convert.ToInt32(item.Port));
                            }

                            dateRecv = DateTime.Now;
                        }
                        else if (item.ProviderId.Equals("9")) //Claro Local UTD
                        {
                            dateSend = DateTime.Now;

                            if (item.ProductTypeId.Equals(RECHARGE))
                            {
                                using (ClaroLocal ClaroLocal = new ClaroLocal(Common.Utils.UTD))
                                {
                                    transactionResult = ClaroLocal.Recharge(phone, item.CurrencyAmount, item.RoutingId, item.Url);
                                }

                            }
                            else if (item.ProductTypeId.Equals(PIN))
                            {
                                using (ClaroLocal ClaroLocal = new ClaroLocal(Common.Utils.UTD))
                                {
                                    transactionResult = ClaroLocal.Pin(item.CurrencyAmount, item.LogId, item.Url);
                                }
                            }

                            dateRecv = DateTime.Now;
                        }
                        else if (item.ProviderId.Equals("262")) //Claro Local FREESTAR
                        {
                            dateSend = DateTime.Now;

                            if (item.ProductTypeId.Equals(RECHARGE))
                            {
                                using (ClaroLocal ClaroLocal = new ClaroLocal(Common.Utils.FREESTAR))
                                {
                                    transactionResult = ClaroLocal.Recharge(phone, item.CurrencyAmount, item.RoutingId, item.Url);
                                }

                            }
                            else if (item.ProductTypeId.Equals(PIN))
                            {
                                using (ClaroLocal ClaroLocal = new ClaroLocal(Common.Utils.FREESTAR))
                                {
                                    transactionResult = ClaroLocal.Pin(item.CurrencyAmount, item.LogId, item.Url);
                                }
                            }

                            dateRecv = DateTime.Now;
                        }
                        else if (item.ProviderId.Equals("10") || item.ProviderId.Equals("261")) //Orange y Movil BanReservas
                        {
                            dateSend = DateTime.Now;

                            using (Orange Orange = new Orange())
                            {
                                transactionResult = Orange.Recharge(item.RoutingId, phone, item.LocalCurrencyAmount, item.Ip,
                                    Convert.ToInt32(item.Port));
                            }

                            dateRecv = DateTime.Now;
                        }
                        else if (item.ProviderId.Equals(IDEALTEL))
                        {
                            dateSend = DateTime.Now;

                            using (IdealtelWcf.IdealtelClient wcf = new IdealtelWcf.IdealtelClient())
                            {
                                IdealtelWcf.IdealtelRequest request = new IdealtelWcf.IdealtelRequest();
                                IdealtelWcf.IdealtelResult response = null;

                                request.Amount = item.CurrencyAmount;
                                request.Phone = item.Phone;
                                request.TransactionId = item.RoutingId;
                                request.Url = item.Url;

                                response = wcf.Recharge(request);

                                transactionResult = new Transaction();

                                transactionResult.AuthorizationNumber = response.TransactionId;
                                transactionResult.Code = response.Code;
                                transactionResult.Message = response.Message;
                            }

                            dateRecv = DateTime.Now;
                        }
                        else if (item.ProviderId.Equals(VIVA)) //Viva
                        {
                            dateSend = DateTime.Now;

                            //if (!String.IsNullOrEmpty(item.Url))
                            //{
                            using (VivaWcf.VivaClient viva = new VivaWcf.VivaClient())
                            {
                                VivaWcf.VivaRechargeRequest request = new VivaWcf.VivaRechargeRequest();
                                VivaWcf.VivaRechargeResult response = null;

                                request.Amount = Convert.ToInt32(item.CurrencyAmount);
                                request.PhoneNumber = phone;
                                request.Subentity = Convert.ToInt32(accountId);
                                request.TransactionId = item.RoutingId;
                                request.Url = item.Url;

                                response = viva.Recharge(request);

                                transactionResult = new Transaction();

                                transactionResult.Code = response.Code;
                                transactionResult.Message = response.Message;

                                if (transactionResult.Code.Equals(Common.Utils.APPROVED))
                                {
                                    transactionResult.AuthorizationNumber = response.RechargeId.ToString();
                                }
                            }
                            //}
                            //else
                            //{
                            //using (Viva Viva = new Viva())
                            //{
                            //    transactionResult = Viva.Recharge(item.LocalCurrencyAmount, phone, item.RoutingId, item.Ip,
                            //        Convert.ToInt32(item.Port));
                            //}
                            //}



                            dateRecv = DateTime.Now;
                        }
                        else if (item.ProviderId.Equals("12")) //Claro Internacional
                        {
                            dateSend = DateTime.Now;

                            using (ClaroExt ClaroExt = new ClaroExt(item.Url))
                            {
                                transactionResult = ClaroExt.Recharge(phone, item.CurrencyAmount, item.RoutingId);
                            }

                            dateRecv = DateTime.Now;
                        }
                        else if (item.ProviderId.Equals("14"))  //Easy Phone
                        {
                            dateSend = DateTime.Now;

                            using (EasyPhone EasyPhone = new EasyPhone())
                            {
                                transactionResult = EasyPhone.SendSms(item.Phone, smsMessage, item.Url);
                            }

                            dateRecv = DateTime.Now;
                        }
                        else if (item.ProviderId.Equals("15"))  //DEV 
                        {
                            dateSend = DateTime.Now;

                            using (Iris Iris = new Iris())
                            {
                                transactionResult = Iris.SendSms(item.RoutingId, phone, smsMessage, item.Url);
                            }

                            dateRecv = DateTime.Now;
                        }
                        else if (item.ProviderId.Equals("223")) //TWI
                        {
                            dateSend = DateTime.Now;

                            if (item.TransactionMode.Equals("0")) //Fix
                            {
                                using (Prasan Prasan = new Prasan())
                                {
                                    transactionResult = Prasan.Recharge(phone, String.Empty, item.ProductCode, item.RoutingId,
                                        item.Url, Common.Utils.FIX_RECHARGE, Prasan.TYPE_TWI);
                                }
                            }
                            else
                            {
                                using (Prasan Prasan = new Prasan())
                                {
                                    transactionResult = Prasan.Recharge(phone, item.LocalCurrencyAmount, item.ProductCode, item.RoutingId,
                                        item.Url, Common.Utils.FLEXI_RECHARGE, Prasan.TYPE_TWI);
                                }
                            }


                            dateRecv = DateTime.Now;
                        }
                        else if (item.ProviderId.Equals(PUBLICTECH) || item.ProviderId.Equals(QUIERO_RECARGAS))
                        {
                            dateSend = DateTime.Now;

                            using (AyB ayb = new AyB(item.ProviderId.Equals(PUBLICTECH) ? this.AYB_PUBLITECH : this.AYB_QUIERO_RECARGA))
                            {
                                transactionResult = ayb.Recharge(item.ProductCode, phone, item.CurrencyAmount, item.RoutingId, accountId, item.Ip, item.Port);
                            }

                            dateRecv = DateTime.Now;
                        }
                        else if (item.ProviderId.Equals(TRUMPIA_OFFICIAL))
                        {
                            dateSend = DateTime.Now;

                            using (Trumpia trumpia = new Trumpia("official"))
                            {
                                transactionResult = trumpia.SendSms(item.Url, "OFFICIAL", "OFFICIAL", "1", phone, smsMessage);
                            }

                            dateRecv = DateTime.Now;
                        }
                        else if (item.ProviderId.Equals(MIDASCARD))
                        {
                            dateSend = DateTime.Now;

                            using (MidasCard midas = new MidasCard())
                            {
                                transactionResult = midas.Recharge(item.ProductCode, phone, item.CurrencyAmount, item.RoutingId, accountId, item.Url);
                            }

                            dateRecv = DateTime.Now;
                        }
                        else if (item.ProviderId.Equals(SOLOPIN) || item.ProviderId.Equals(SOLOPIN_4G))
                        {
                            dateSend = DateTime.Now;

                            using (PlWcf.IappClient wcf = new PlWcf.IappClient())
                            {
                                LoginRequest loginReq = new LoginRequest();
                                PlWcf.LoginResponse loginResponse = null;
                                BuyRequest req = new BuyRequest();
                                BuyResponse buyResponse = null;

                                loginReq.UserName = this.PINLESS_USER;
                                loginReq.Password = this.PINLESS_PASS;
                                
                                loginResponse = wcf.Login(loginReq);

                                if(loginResponse.Code.Equals("0"))
                                {
                                    req.AccountNumber = Convert.ToInt64(phone);
                                    req.LocaleId = plLocale;
                                    req.ProductId = Convert.ToInt32(item.ProductCode);
                                    req.Reference = Convert.ToInt64(item.RoutingId);
                                    req.StoreId = Convert.ToInt32(accountId);
                                    req.TransAmount = Convert.ToDouble(item.CurrencyAmount);
                                    req.SessionId = loginResponse.SessionId;
                                    req.PhoneNotification = plPhoneNotification;

                                    buyResponse = wcf.Buy(req);

                                    transactionResult = new Transaction();

                                    
                                    transactionResult.Code = buyResponse.Code;
                                    transactionResult.Message = buyResponse.Message;

                                    if (transactionResult.Code.Equals("0"))
                                    {
                                        tranResult.Code = Common.Utils.APPROVED;
                                        tranResult.Message = Common.Utils.APPROVED_MESSAGE;
                                        transactionResult.AuthorizationNumber = buyResponse.TransReference.ToString();
                                    }
                                }

                                
                            }

                            

                            //using (Pinless pinless = new Pinless(this.PINLESS))
                            //{
                            //    int dollarFree = 0;

                            //    transactionResult = pinless.Recharge(item.ProductCode, phone, item.CurrencyAmount, item.RoutingId, item.TerminalId, dollarFree, item.Url);
                            //}

                            dateRecv = DateTime.Now;
                        }
                        //else if (item.ProviderId.Equals(SOLOPIN_4G))
                        //{
                        //    dateSend = DateTime.Now;

                        //    using (Pinless pinless = new Pinless(this.PINLESS_4G))
                        //    {
                        //        int dollarFree = 0;

                        //        transactionResult = pinless.Recharge(item.ProductCode, phone, item.CurrencyAmount, item.RoutingId, item.TerminalId, dollarFree, item.Url);
                        //    }

                        //    dateRecv = DateTime.Now;
                        //}
                        else if (item.ProviderId.Equals(OFFICIAL_PREPAID_NATION))
                        {
                            dateSend = DateTime.Now;

                            if (item.ProductTypeId.Equals(RECHARGE))
                            {
                                using (PrepaidNation PrepaidNation = new PrepaidNation("official"))
                                {
                                    transactionResult = PrepaidNation.TopUp(Convert.ToInt32(item.ProductCode), item.RoutingId, item.TerminalId, phone, Convert.ToDecimal(item.CurrencyAmount), item.Url);
                                }

                            }
                            else if (item.ProductTypeId.Equals(PIN))
                            {
                                using (PrepaidNation PrepaidNation = new PrepaidNation("official"))
                                {
                                    transactionResult = PrepaidNation.PurchasePin(Convert.ToInt32(item.ProductCode), item.RoutingId, item.TerminalId, item.Url);
                                }
                            }


                            dateRecv = DateTime.Now;
                        }
                        else if (item.ProviderId.Equals(AIPC_PREPAID_NATION))
                        {
                            dateSend = DateTime.Now;

                            if (item.ProductTypeId.Equals(RECHARGE))
                            {
                                using (PrepaidNation PrepaidNation = new PrepaidNation("aipc"))
                                {
                                    transactionResult = PrepaidNation.TopUp(Convert.ToInt32(item.ProductCode), item.RoutingId, item.TerminalId, phone, Convert.ToDecimal(item.CurrencyAmount), item.Url);
                                }

                            }
                            else if (item.ProductTypeId.Equals(PIN))
                            {
                                using (PrepaidNation PrepaidNation = new PrepaidNation("aipc"))
                                {
                                    transactionResult = PrepaidNation.PurchasePin(Convert.ToInt32(item.ProductCode), item.RoutingId, item.TerminalId, item.Url);
                                }
                            }


                            dateRecv = DateTime.Now;
                        }
                        else
                        {
                            if (item.ProductTypeId.Equals(LOCAL_PIN))
                            {
                                using (RoutingDAO pinDao = new RoutingDAO())
                                {
                                    transactionResult = pinDao.BuyPin(accountId, item.ProductId, item.CurrencyAmount);
                                }
                            }
                            else
                            {
                                transactionResult.Code = Common.Utils.PROVIDER_NOT_FOUND;
                            }

                        }
                    }
                    else
                    {
                        String isgood = Common.Utils.GeneraNumeroRamdon(0, 3);
                        String esperar = Common.Utils.GeneraNumeroRamdon(0, 3);

                        if (esperar.Equals("0"))
                        {
                            System.Threading.Thread.Sleep(35000);
                        }

                        
                        transactionResult.TransactionDateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                        if (isgood.Equals("0"))
                        {
                            String error = Common.Utils.GeneraNumeroRamdon(0, 4);

                            switch (error)
                            {
                                case "0": transactionResult.Code = Common.Utils.PROVIDER_NOT_FOUND;
                                    break;
                                case "1": transactionResult.Code = Common.Utils.PHONE_DOES_NOT_EXISTS;
                                    break;
                                case "2": transactionResult.Code = Common.Utils.RECHARGE_NO_RESPONSE;
                                    break;
                                default: transactionResult.Code = Common.Utils.UNKNOWN_EXCEPTION;
                                    break;
                            }

                            transactionResult.Message = String.Empty;
                        }
                        else
                        {
                            transactionResult.Code = Common.Utils.APPROVED;
                            transactionResult.Message = "Success! - THIS IS UTP TEST WEB SERVICE!";

                            if (item.ProductTypeId.Equals(RECHARGE))
                            {
                                transactionResult.AuthorizationNumber = Common.Utils.GeneraNumeroRamdon(111111111, 999999999);
                            }
                            else if (item.ProductTypeId.Equals(PIN))
                            {
                                transactionResult.AuthorizationNumber = Common.Utils.GeneraNumeroRamdon(111111111, 999999999);
                                transactionResult.PinNumber = Common.Utils.GeneraNumeroRamdon(222222222, 888888888);
                                transactionResult.SerialNumber = Common.Utils.GeneraNumeroRamdon(333333, 999999);
                            }
                            else
                            {
                                transactionResult.Code = Common.Utils.PROVIDER_NOT_FOUND;
                                transactionResult.Message = String.Empty;

                            }
                        }
                    }



                    endDate = DateTime.Now;

                    transactionResult.TransactionId = item.LogId;
                    transactionResult.ProviderId = item.ProviderId;
                    transactionResult.ProviderDescription = item.ProviderName;
                    transactionResult.ProductId = item.ProductId;
                    transactionResult.ProductName = item.ProductName;
                    transactionResult.Cost = item.CurrencyAmount;


                    if (transactionResult.Code.Length > 5)
                    {
                        transactionResult.Details = transactionResult.Code;
                        transactionResult.Code = Common.Utils.UNCAUGHT_ERROR;
                        transactionResult.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                    }

                    using (RoutingDAO dao = new RoutingDAO())
                    {
                        tranResult = dao.TransLog(item.RoutingId, beginDate, endDate,
                            transactionResult.Code,
                            transactionResult.Message,
                            transactionResult.AuthorizationNumber,
                            transactionResult.SerialNumber,
                            transactionResult.PinNumber,
                            locale,
                            dbUser,
                            transactionResult.TransactionDateTime);

                        if (!tranResult.Code.Equals("0"))
                        {
                            tranWsResult = dao.TransLogWs(item.RoutingId, "9", beginDate, endDate,
                                transactionResult.Code, transactionResult.Message,
                                transactionResult.AuthorizationNumber, transactionResult.SerialNumber, transactionResult.PinNumber, locale,
                                dbUser, transactionResult.TransactionDateTime, tranResult.Details);
                        }
                    }

                    returnResponse = responseCodeDao.GetResponseCode(transactionResult.ProviderId,
                        transactionResult.Code, transactionResult.Message, locale, dbUser);

                    transactionResult.Code = returnResponse.Code;

                    if (typeDb.Equals("Y") && transactionResult.Code == Common.Utils.APPROVED)
                    {
                        transactionResult.Message = "Success! - THIS IS UTP TEST WEB SERVICE!";
                    }
                    else
                    {
                        transactionResult.Message = returnResponse.Message;
                    }

                    

                    resultProvider.AppendLine(item.ProviderName);
                    resultProvider.Append("Transaction Id: ");
                    resultProvider.Append(transactionResult.TransactionId);
                    resultProvider.Append("Authorization Number: ");
                    resultProvider.Append(transactionResult.AuthorizationNumber);
                    resultProvider.Append("|Code: ");
                    resultProvider.Append(transactionResult.Code);
                    resultProvider.Append("|Message: ");
                    resultProvider.Append(transactionResult.Message);
                    resultProvider.Append("|Details: ");
                    resultProvider.Append(transactionResult.Details);
                    resultProvider.Append("|Cost: ");
                    resultProvider.Append(transactionResult.Cost);
                    resultProvider.Append("|Pin Number: ");
                    resultProvider.Append(transactionResult.PinNumber);
                    resultProvider.Append("|Serial Number: ");
                    resultProvider.Append(transactionResult.SerialNumber);
                    resultProvider.Append("|Product Id: ");
                    resultProvider.Append(transactionResult.ProductId);
                    resultProvider.Append("|Product Name: ");
                    resultProvider.Append(transactionResult.ProductName);
                    resultProvider.Append("|Sent: ");
                    resultProvider.Append((dateSend == null) ? null : dateSend.ToString());
                    resultProvider.Append("|Received: ");
                    resultProvider.Append((dateRecv == null) ? null : dateRecv.ToString());
                    resultProvider.Append("|Provider Reference Number: ");
                    resultProvider.Append(transactionResult.ProviderReferenceNumber);
                    resultProvider.Append("|Reference Number: ");
                    resultProvider.Append(transactionResult.ReferenceNumber);
                    resultProvider.Append("|TopUp Tracking Number: ");
                    resultProvider.Append(transactionResult.TopUpTrackingNumber);
                    resultProvider.Append("|Provider Authorization Code: ");
                    resultProvider.Append(transactionResult.ProviderAuthorizationCode);
                    resultProvider.Append("|Provider Id: ");
                    resultProvider.Append(transactionResult.ProviderId);
                    resultProvider.Append("|Provider Description: ");
                    resultProvider.Append(transactionResult.ProviderDescription);
                    resultProvider.Append("|Max Intentos: ");
                    resultProvider.Append(transactionResult.MaxIntentos);
                    resultProvider.Append("|Intentos: ");
                    resultProvider.AppendLine(transactionResult.Intentos);
                    resultProvider.Append("Request:");
                    resultProvider.AppendLine(transactionResult.RequestMsg);
                    resultProvider.Append("Response:");
                    resultProvider.AppendLine(transactionResult.ResponseMsg);

                    if (tranResult != null)
                    {
                        resultProvider.Append("|Trans. Result Code: ");
                        resultProvider.Append(tranResult.Code);
                        resultProvider.Append("|Trans. Result Message: ");
                        resultProvider.AppendLine(tranResult.Message);
                    }

                    if (tranWsResult != null)
                    {
                        resultProvider.Append("|Trans. Ws Result Code: ");
                        resultProvider.Append(tranWsResult.Code);
                        resultProvider.Append("|Trans. Ws Result Message: ");
                        resultProvider.AppendLine(tranWsResult.Message);
                    }

                    if (ackTran != null)
                    {
                        if (!String.IsNullOrEmpty(ackTran.ResponseMsg))
                        {
                            resultProvider.Append("Ack Transaction:");
                            resultProvider.AppendLine(ackTran.ResponseMsg);
                        }
                    }

                    TimeSpan? diff = dateRecv - dateSend;

                    if (diff.HasValue)
                    {
                        totalSegundos += diff.Value.TotalSeconds;
                    }

                    

                    if (transactionResult.Code.Equals(Common.Utils.APPROVED))
                    {
                        break;
                    }
                    else
                    {
                        try 
	                    {	        
		                    if (!String.IsNullOrEmpty(item.SecondStopRouting))
                            {
                                if (totalSegundos >= Convert.ToDouble(item.SecondStopRouting))
                                {
                                    break;
                                }
                            }
	                    }
	                    catch (Exception)
	                    {
		
	                    }
                        
                    }
                } 
            }
            catch (Exception ex)
            {
                transactionResult.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                transactionResult.Code = Common.Utils.UNCAUGHT_ERROR;
                details = ex.Message;
                
            }
            finally
            {
                output.AppendLine("");
                output.AppendLine("***********************************************");
                output.AppendLine("Utp Web Service - Buy");
                output.Append("Transaction Id   : ").AppendLine(transactionId);
                output.Append("Account Id       : ").AppendLine(accountId);
                output.Append("Amount           : ").AppendLine(amount);
                output.Append("Amount           : ").AppendLine(amount);
                output.Append("Phone            : ").AppendLine(phone);
                output.Append("Locale           : ").AppendLine(locale);
                output.Append("User             : ").AppendLine(dbUser);
                output.Append("Ip               : ").AppendLine(ip);
                output.Append("Code             : ").AppendLine(transactionResult.Code);
                output.Append("Message          : ").AppendLine(transactionResult.Message);
                output.Append("Details          : ").AppendLine(transactionResult.Details);
                output.Append("Ex Details       : ").AppendLine(details);
                output.Append("Total Segundos   : ").AppendLine(totalSegundos.ToString());
                //output.Append("Routing Output   : ").AppendLine(routingDt.ToString());
                output.Append("Provider Output  : ").AppendLine(resultProvider.ToString());
                output.AppendLine("***********************************************");

                transactionResult.ResponseMsg = String.Empty;
                transactionResult.RequestMsg = String.Empty;

                log.Debug(output.ToString());
            }

            return transactionResult;
        }

        [WebMethod]
        public Common.Result Void(String transactionId, String apiUser,
            String apiPassword, String accountId, String reference,
            String amount, String authorizationNumber, String phone,
            String ipUser, String locale, String dbUser, String user, String ip,
            String typeDb)
        {
            Common.Result result = new Common.Result();
            Transaction transaction = new Transaction();
            RoutingDAO dao = new RoutingDAO();
            RoutingResult routingResult = null;
            StringBuilder output = new StringBuilder();
            DateTime beginDate, endDate;
            StringBuilder routingDt = new StringBuilder();
            StringBuilder resultProvider = new StringBuilder();
            ResponseCodeDAO responseCodeDao = new ResponseCodeDAO();
            Common.Result errorResult = null;
            ProviderDAO providerDao = new ProviderDAO();
            Common.Result returnResponse;
            ApiLogDAO apiLogDao = new ApiLogDAO();
            String routingId = null;
            Boolean esOrange = false;

            try
            {

                routingResult = dao.Void(transactionId, apiUser, apiPassword,
                    accountId, reference, ip, locale, dbUser);

                result.Code = routingResult.Code;
                result.Message = routingResult.Message;

                if (result.Code.Equals("0"))
                {
                    
                    foreach (RoutingProvider item in routingResult.RoutingProvider)
                    {

                        routingDt.Append("Log Id: ");
                        routingDt.Append(item.LogId);
                        routingDt.Append("|Routing Id: ");
                        routingDt.Append(item.RoutingId);
                        routingDt.Append("|Void Routing Id: ");
                        routingDt.Append(item.VoidRoutingId);
                        routingDt.Append("|Operator Code: ");
                        routingDt.AppendLine(item.OperatorCode);
                        routingDt.Append("|Product Code: ");
                        routingDt.Append(item.ProductCode);
                        routingDt.Append("|Product Type Id: ");
                        routingDt.Append(item.ProductTypeId);
                        routingDt.Append("|Profile Id: ");
                        routingDt.AppendLine(item.ProfileId);
                        routingDt.Append("|Provider Account Id: ");
                        routingDt.Append(item.ProviderAccountId);
                        routingDt.Append("|Provider Id: ");
                        routingDt.Append(item.ProviderId);
                        routingDt.Append("|Routing Id: ");
                        routingDt.AppendLine(item.RoutingId);
                        routingDt.Append("|Template Id: ");
                        routingDt.Append(item.TemplateId);
                        routingDt.Append("|Weight: ");
                        routingDt.Append(item.Weight);
                        routingDt.Append("|Country Code: ");
                        routingDt.AppendLine(item.CountryCode);

                        routingId = item.RoutingId;

                        if (item.ProviderId.Equals("10"))
                        {
                            esOrange = true;
                        }
                    }

                    if (!typeDb.Equals("N"))
                    {
                        if (esOrange && routingResult.RoutingProvider.Count==1)
                        {
                            typeDb = "N";
                        }
                    }

                    

                    if (typeDb.Equals("N"))
                    {
                        foreach (RoutingProvider item in routingResult.RoutingProvider)
                        {
                            beginDate = DateTime.Now;

                            if (item.ProviderId.Equals("1")) //Unlimited Pins
                            {
                                if (item.ProductTypeId.Equals(RECHARGE))
                                {
                                    using (UnlimitedPins UnlimitedPins = new UnlimitedPins())
                                    {
                                        transaction = UnlimitedPins.Void(item.VoidRoutingId);
                                    }
                                }
                                else if (item.ProductTypeId.Equals(LOCAL_PIN))
                                {
                                    /*using (RoutingDAO pinDao = new RoutingDAO())
                                    {
                                        transaction = pinDao.BuyPin(accountId, item.ProductId, amount);
                                    }*/
                                }
                            }
                            else if (item.ProviderId.Equals("2")) //iSend
                            {
                                if (item.ProductTypeId.Equals(LOCAL_PIN))
                                {
                                    /*using (RoutingDAO pinDao = new RoutingDAO())
                                    {
                                        transaction = pinDao.BuyPin(accountId, item.ProductId, amount);
                                    }*/
                                }
                            }
                            else if (item.ProviderId.Equals("3")) //More Magic
                            {
                                if (item.ProductTypeId.Equals(LOCAL_PIN))
                                {
                                    /*using (RoutingDAO pinDao = new RoutingDAO())
                                    {
                                        transaction = pinDao.VoidPin(accountId, item.ProductId, amount);
                                    }*/
                                }
                            }
                            else if (item.ProviderId.Equals("4")) //Sbt
                            {
                                if (item.ProductTypeId.Equals(LOCAL_PIN))
                                {
                                    /*using (RoutingDAO pinDao = new RoutingDAO())
                                    {
                                        transaction = pinDao.VoidPin(accountId, item.pr, amount);
                                    }*/
                                }
                            }
                            else if (item.ProviderId.Equals(SOLOPIN) || item.ProviderId.Equals(SOLOPIN_4G))
                            {
                                using (PlWcf.IappClient wcf = new IappClient())
                                {
                                    LoginRequest loginReq = new LoginRequest();
                                    PlWcf.LoginResponse loginResponse = null;
                                    VoidRequest req = new VoidRequest();
                                    VoidResponse voidResponse = null;

                                    loginReq.UserName = this.PINLESS_USER;
                                    loginReq.Password = this.PINLESS_PASS;

                                    loginResponse = wcf.Login(loginReq);

                                    if (loginResponse.Code.Equals("0"))
                                    {
                                        req.SessionId = loginResponse.SessionId;
                                        req.StoreId = Convert.ToInt32(accountId);
                                        req.Reference = Convert.ToInt64(item.VoidRoutingId);

                                        voidResponse = wcf.Void(req);

                                        transaction.Code = voidResponse.Code;
                                        transaction.Message = voidResponse.Message;

                                        if (transaction.Code.Equals("0"))
                                        {
                                            transaction.Code = Common.Utils.APPROVED;
                                        }
                                    }
                                    else
                                    {
                                        transaction.Code = loginResponse.Code;
                                        transaction.Message = loginResponse.Message;
                                    }
                                }
                            }
                            else if (item.ProviderId.Equals("5")) //Prasan
                            {
                                if (item.ProductTypeId.Equals(RECHARGE))
                                {
                                    if (item.TransactionMode.Equals("0")) //Fix
                                    {
                                        using (Prasan Prasan = new Prasan())
                                        {
                                            transaction = Prasan.Void(item.VoidRoutingId, item.Url, Common.Utils.FIX_RECHARGE, Prasan.TYPE_PRASAN);
                                        }
                                    }
                                    else
                                    {
                                        using (Prasan Prasan = new Prasan())
                                        {
                                            transaction = Prasan.Void(item.VoidRoutingId, item.Url, Common.Utils.FLEXI_RECHARGE, Prasan.TYPE_PRASAN);
                                        }
                                    }
                                }
                                else if (item.ProductTypeId.Equals(LOCAL_PIN))
                                {
                                    /*using (RoutingDAO pinDao = new RoutingDAO())
                                    {
                                        transaction = pinDao.BuyPin(accountId, item.ProductId, amount);
                                    }*/
                                }
                            }
                            else if (item.ProviderId.Equals(PUBLICTECH) || item.ProviderId.Equals(QUIERO_RECARGAS))
                            {
                                using (AyB ayb = new AyB(item.ProviderId.Equals(PUBLICTECH) ? this.AYB_PUBLITECH : this.AYB_QUIERO_RECARGA))
                                {
                                    transaction = ayb.Void(item.VoidRoutingId, accountId, item.Ip, item.Port);
                                }
                            }
                            else if (item.ProviderId.Equals(MIDASCARD))
                            {
                                using (MidasCard midas = new MidasCard())
                                {
                                    transaction = midas.Void(item.VoidRoutingId, accountId, item.Url);
                                }
                            }
                            else if (item.ProviderId.Equals("6")) //EasyCall
                            {
                                if (item.ProductTypeId.Equals(PIN)) //Pin
                                {
                                    using (EasyCall EasyCall = new EasyCall())
                                    {
                                        transaction = EasyCall.VoidPin(authorizationNumber, item.Url);
                                    }
                                }
                                else if (item.ProductTypeId.Equals(LOCAL_PIN))
                                {
                                    /*using (RoutingDAO pinDao = new RoutingDAO())
                                    {
                                        transaction = pinDao.BuyPin(accountId, item.ProductId, amount);
                                    }*/
                                }
                            }
                            else if (item.ProviderId.Equals("7")) //National
                            {
                                if (item.ProductTypeId.Equals(RECHARGE)) //Topup
                                {
                                    using (National National = new National())
                                    {
                                        transaction = National.VoidRecharge(item.ProductCode,
                                            amount, phone, authorizationNumber);
                                    }
                                }
                                else if (item.ProductTypeId.Equals(PIN))    //Pin
                                {
                                    using (National National = new National())
                                    {
                                        transaction = National.VoidPin(item.ProductCode,
                                            amount, authorizationNumber);
                                    }
                                }
                                else if (item.ProductTypeId.Equals(LOCAL_PIN))
                                {
                                    /*using (RoutingDAO pinDao = new RoutingDAO())
                                    {
                                        transaction = pinDao.BuyPin(accountId, item.ProductId, amount);
                                    }*/
                                }
                            }
                            else if (item.ProviderId.Equals("8")) //Tricom
                            {
                                if (item.ProductTypeId.Equals(RECHARGE)) //Topup
                                {
                                    using (Tricom Tricom = new Tricom())
                                    {
                                        transaction = Tricom.Void(phone, item.LocalCurrencyAmount, authorizationNumber,
                                            item.VoidRoutingId, item.Ip, Convert.ToInt32(item.Port));
                                    }
                                }
                                else if (item.ProductTypeId.Equals(LOCAL_PIN))
                                {
                                    /*using (RoutingDAO pinDao = new RoutingDAO())
                                    {
                                        transaction = pinDao.BuyPin(accountId, item.ProductId, amount);
                                    }*/
                                }
                            }
                            else if (item.ProviderId.Equals("9")) //Claro Local
                            {
                                using (ClaroLocal ClaroLocal = new ClaroLocal(Common.Utils.UTD))
                                {
                                    transaction = ClaroLocal.VoidRecharge(phone, item.CurrencyAmount,
                                        authorizationNumber, item.Url);
                                }
                            }
                            else if (item.ProviderId.Equals("262")) //Claro Local
                            {
                                using (ClaroLocal ClaroLocal = new ClaroLocal(Common.Utils.FREESTAR))
                                {
                                    transaction = ClaroLocal.VoidRecharge(phone, item.CurrencyAmount,
                                        authorizationNumber, item.Url);
                                }
                            }
                            else if (item.ProviderId.Equals("10") || item.ProviderId.Equals("261")) //Orange y Movil BanReservas
                            {
                                if (item.ProductTypeId.Equals(RECHARGE)) //Topup
                                {
                                    using (Orange Orange = new Orange())
                                    {
                                        transaction = Orange.Void(item.RechargeStamp, authorizationNumber,
                                         item.RoutingId, phone, item.Ip, Convert.ToInt32(item.Port));
                                    }
                                }
                                else if (item.ProductTypeId.Equals(LOCAL_PIN))
                                {
                                    /*using (RoutingDAO pinDao = new RoutingDAO())
                                    {
                                        transaction = pinDao.BuyPin(accountId, item.ProductId, amount);
                                    }*/
                                }
                            }
                            else if (item.ProviderId.Equals("11")) //Viva
                            {
                                if (item.ProductTypeId.Equals(LOCAL_PIN))
                                {
                                    /*using (RoutingDAO pinDao = new RoutingDAO())
                                    {
                                        transaction = pinDao.BuyPin(accountId, item.ProductId, amount);
                                    }*/
                                }
                            }
                            else if (item.ProviderId.Equals("12")) //Claro Internacional
                            {
                                if (item.ProductTypeId.Equals(RECHARGE)) //Topup
                                {
                                    using (ClaroExt ClaroExt = new ClaroExt(item.Url))
                                    {
                                        transaction = ClaroExt.Void(phone, amount, item.VoidRoutingId);
                                    }
                                }
                                else if (item.ProductTypeId.Equals(LOCAL_PIN))
                                {
                                    /*using (RoutingDAO pinDao = new RoutingDAO())
                                    {
                                        transaction = pinDao.BuyPin(accountId, item.ProductId, amount);
                                    }*/
                                }
                            }
                            else if (item.ProviderId.Equals("16")) //TWI
                            {
                                if (item.ProductTypeId.Equals(RECHARGE)) //Topup
                                {
                                    if (item.TransactionMode.Equals("0")) //Fix
                                    {
                                        using (Prasan Prasan = new Prasan())
                                        {
                                            transaction = Prasan.Void(item.VoidRoutingId, item.Url, Common.Utils.FIX_RECHARGE, Prasan.TYPE_TWI);
                                        }
                                    }
                                    else
                                    {
                                        using (Prasan Prasan = new Prasan())
                                        {
                                            transaction = Prasan.Void(item.VoidRoutingId, item.Url, Common.Utils.FLEXI_RECHARGE, Prasan.TYPE_TWI);
                                        }
                                    }
                                }
                                else if (item.ProductTypeId.Equals(LOCAL_PIN))
                                {
                                    using (RoutingDAO pinDao = new RoutingDAO())
                                    {
                                        transaction = pinDao.BuyPin(accountId, item.ProductId, amount);
                                    }
                                }
                            }
                            else if (item.ProviderId.Equals(OFFICIAL_PREPAID_NATION))
                            {
                                using (PrepaidNation PrepaidNation = new PrepaidNation("official"))
                                {
                                    transaction = PrepaidNation.Void(Convert.ToInt64(item.VoidRoutingId), item.RoutingId, item.TerminalId, item.Url);
                                }
                            }
                            else if (item.ProviderId.Equals(AIPC_PREPAID_NATION))
                            {
                                using (PrepaidNation PrepaidNation = new PrepaidNation("aipc"))
                                {
                                    transaction = PrepaidNation.Void(Convert.ToInt64(item.VoidRoutingId), item.RoutingId, item.TerminalId, item.Url);
                                }
                            }

                            result.Code = transaction.Code;
                            result.Details = transaction.Details;
                            result.Id = transaction.TransactionId;
                            result.Message = transaction.Message;
                            endDate = DateTime.Now;

                            if (result.Code.Length > 5)
                            {
                                result.Details = result.Code;
                                result.Code = Common.Utils.UNCAUGHT_ERROR;
                                result.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                            }
                            else
                            {
                                returnResponse = responseCodeDao.GetResponseCode(item.ProviderId,
                                result.Code, result.Message, locale, dbUser);

                                result.Code = returnResponse.Code;
                                result.Message = returnResponse.Message;
                            }

                            resultProvider.AppendLine(item.ProviderName);
                            resultProvider.Append("Transaction Id: ");
                            resultProvider.Append(item.LogId);
                            resultProvider.Append("|Void Routing Id: ");
                            resultProvider.Append(item.VoidRoutingId);
                            resultProvider.Append("|Amount: ");
                            resultProvider.Append(amount);
                            resultProvider.Append("|Phone: ");
                            resultProvider.Append(phone);
                            resultProvider.Append("|Begin Date: ");
                            resultProvider.Append(beginDate.ToString());
                            resultProvider.Append("|End Date: ");
                            resultProvider.Append(endDate.ToString());
                            resultProvider.Append("|Code: ");
                            resultProvider.Append(result.Code);
                            resultProvider.Append("|Message: ");
                            resultProvider.AppendLine(result.Message);
                            resultProvider.Append("Request:  ").AppendLine(transaction.RequestMsg);
                            resultProvider.Append("Response: ").AppendLine(transaction.ResponseMsg);

                            dao.TransLog(item.RoutingId, beginDate, endDate, result.Code,
                                result.Message, null, null,
                                null, locale, dbUser, null);

                            if (result.Code.Equals(Common.Utils.APPROVED))
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        result.Code = "00";
                        result.Message = "Success! - THIS IS UTP TEST WEB SERVICE!";

                        dao.TransLog(routingId, DateTime.Now, DateTime.Now, result.Code,
                                result.Message, authorizationNumber,
                                null, null, locale, dbUser, null);

                    }
                }
                else
                {
                    try
                    {
                        long temp;

                        if (!Int64.TryParse(routingResult.LogId, out temp))
                        {
                            routingResult.LogId = String.Empty;
                        }
                    }
                    catch (Exception)
                    {
                        routingResult.LogId = String.Empty;
                    }


                    if (!String.IsNullOrEmpty(routingResult.LogId))
                    {
                        returnResponse = apiLogDao.SearchApiLogResponseCode(routingResult.LogId);

                        if (returnResponse.Code.Equals(Common.Utils.NO_RECORDS_FOUND) ||
                            returnResponse.Code.Equals(Common.Utils.UNKNOWN_EXCEPTION))
                        {
                            result.Code = routingResult.Code;
                            result.Message = routingResult.Message;
                        }
                        else
                        {
                            result.Code = returnResponse.Code;
                            result.Message = returnResponse.Message;
                        }
                    }
                    else
                    {

                    }

                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                    if (res.Code.Equals("0"))
                    {
                        result.Code = res.ResponseCode[0].Code;
                        result.Message = res.ResponseCode[0].Message;
                    }
                    else
                    {
                        result.Code = Common.Utils.UNCAUGHT_ERROR;
                        result.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                    }
                }

                output.AppendLine("");
                output.AppendLine("***********************************************");
                output.AppendLine("Utp Web Service - Void");
                output.Append("Transaction Id       : ").AppendLine(transactionId);
                output.Append("Api User             : ").AppendLine(apiUser);
                output.Append("Account Id           : ").AppendLine(accountId);
                output.Append("Reference            : ").AppendLine(reference);
                output.Append("Amount               : ").AppendLine(amount);
                output.Append("Authorization Number : ").AppendLine(authorizationNumber);
                output.Append("Phone                : ").AppendLine(phone);
                output.Append("Locale               : ").AppendLine(locale);
                output.Append("User                 : ").AppendLine(user);
                output.Append("Ip User              : ").AppendLine(ipUser);
                output.Append("Ip                   : ").AppendLine(ip);
                output.Append("Code                 : ").AppendLine(result.Code);
                output.Append("Message              : ").AppendLine(result.Message);
                output.Append("Details              : ").AppendLine(result.Details);
                output.Append("Provider Output      : ").AppendLine(routingDt.ToString());
                output.Append("Result Output        : ").AppendLine(resultProvider.ToString());
                output.AppendLine("***********************************************");

                log.Debug(output.ToString());
            }

            return result;
        }

        [WebMethod]
        public Common.Result VoidByReference(String reference, String apiUser,
            String apiPassword, String accountId, String referenceVoid,
            String amount, String phone,
            String ipUser, String locale, String dbUser, String user, String ip,
            String typeDb)
        {
            Common.Result result = new Common.Result();
            Transaction transaction = new Transaction();
            RoutingDAO dao = new RoutingDAO();
            RoutingResult routingResult = null;
            StringBuilder output = new StringBuilder();
            DateTime beginDate, endDate;
            StringBuilder routingDt = new StringBuilder();
            StringBuilder resultProvider = new StringBuilder();
            ResponseCodeDAO responseCodeDao = new ResponseCodeDAO();
            Common.Result errorResult = null;
            ProviderDAO providerDao = new ProviderDAO();
            Common.Result returnResponse;
            ApiLogDAO apiLogDao = new ApiLogDAO();
            String routingId = null, providerReference = String.Empty;
            Boolean esOrange = false;

            try
            {

                routingResult = dao.VoidByReference(reference, apiUser, apiPassword,
                    accountId, referenceVoid, ip, locale, dbUser);

                result.Code = routingResult.Code;
                result.Message = routingResult.Message;

                if (result.Code.Equals("0"))
                {

                    foreach (RoutingProvider item in routingResult.RoutingProvider)
                    {

                        routingDt.Append("Log Id: ");
                        routingDt.Append(item.LogId);
                        routingDt.Append("|Routing Id: ");
                        routingDt.Append(item.RoutingId);
                        routingDt.Append("|Void Routing Id: ");
                        routingDt.Append(item.VoidRoutingId);
                        routingDt.Append("|Operator Code: ");
                        routingDt.AppendLine(item.OperatorCode);
                        routingDt.Append("|Product Code: ");
                        routingDt.Append(item.ProductCode);
                        routingDt.Append("|Product Type Id: ");
                        routingDt.Append(item.ProductTypeId);
                        routingDt.Append("|Profile Id: ");
                        routingDt.AppendLine(item.ProfileId);
                        routingDt.Append("|Provider Account Id: ");
                        routingDt.Append(item.ProviderAccountId);
                        routingDt.Append("|Provider Id: ");
                        routingDt.Append(item.ProviderId);
                        routingDt.Append("|Routing Id: ");
                        routingDt.AppendLine(item.RoutingId);
                        routingDt.Append("|Template Id: ");
                        routingDt.Append(item.TemplateId);
                        routingDt.Append("|Weight: ");
                        routingDt.Append(item.Weight);
                        routingDt.Append("|Country Code: ");
                        routingDt.AppendLine(item.CountryCode);

                        routingId = item.RoutingId;
                        providerReference = item.ProviderReference;

                        if (item.ProviderId.Equals("10"))
                        {
                            esOrange = true;
                        }
                    }

                    if (!typeDb.Equals("N"))
                    {
                        if (esOrange && routingResult.RoutingProvider.Count == 1)
                        {
                            typeDb = "N";
                        }
                    }



                    if (typeDb.Equals("N"))
                    {
                        foreach (RoutingProvider item in routingResult.RoutingProvider)
                        {
                            beginDate = DateTime.Now;

                            if (item.ProviderId.Equals("1")) //Unlimited Pins
                            {
                                if (item.ProductTypeId.Equals(RECHARGE))
                                {
                                    using (UnlimitedPins UnlimitedPins = new UnlimitedPins())
                                    {
                                        transaction = UnlimitedPins.Void(item.VoidRoutingId);
                                    }
                                }
                                /*else if (item.ProductTypeId.Equals(LOCAL_PIN))
                                {
                                    using (RoutingDAO pinDao = new RoutingDAO())
                                    {
                                        transaction = pinDao.BuyPin(accountId, item.ProductId, amount);
                                    }
                                }*/
                            }
                            /*else if (item.ProviderId.Equals("2")) //iSend
                            {
                                if (item.ProductTypeId.Equals(LOCAL_PIN))
                                {
                                    using (RoutingDAO pinDao = new RoutingDAO())
                                    {
                                        transaction = pinDao.BuyPin(accountId, item.ProductId, amount);
                                    }
                                }
                            }*/
                            /*else if (item.ProviderId.Equals("3")) //More Magic
                            {
                                if (item.ProductTypeId.Equals(LOCAL_PIN))
                                {
                                    using (RoutingDAO pinDao = new RoutingDAO())
                                    {
                                        transaction = pinDao.BuyPin(accountId, item.ProductId, amount);
                                    }
                                }
                            }*/
                            /*else if (item.ProviderId.Equals("4")) //Sbt
                            {
                                if (item.ProductTypeId.Equals(LOCAL_PIN))
                                {
                                    using (RoutingDAO pinDao = new RoutingDAO())
                                    {
                                        transaction = pinDao.BuyPin(accountId, item.ProductId, amount);
                                    }
                                }
                            }*/
                            else if (item.ProviderId.Equals("5")) //Prasan
                            {
                                if (item.ProductTypeId.Equals(RECHARGE))
                                {
                                    if (item.TransactionMode.Equals("0")) //Fix
                                    {
                                        using (Prasan Prasan = new Prasan())
                                        {
                                            transaction = Prasan.Void(item.VoidRoutingId, item.Url, Common.Utils.FIX_RECHARGE, Prasan.TYPE_PRASAN);
                                        }
                                    }
                                    else
                                    {
                                        using (Prasan Prasan = new Prasan())
                                        {
                                            transaction = Prasan.Void(item.VoidRoutingId, item.Url, Common.Utils.FLEXI_RECHARGE, Prasan.TYPE_PRASAN);
                                        }
                                    }
                                }
                                /*else if (item.ProductTypeId.Equals(LOCAL_PIN))
                                {
                                    using (RoutingDAO pinDao = new RoutingDAO())
                                    {
                                        transaction = pinDao.BuyPin(accountId, item.ProductId, amount);
                                    }
                                }*/
                            }
                            else if (item.ProviderId.Equals("6")) //EasyCall
                            {
                                if (item.ProductTypeId.Equals(PIN)) //Pin
                                {
                                    using (EasyCall EasyCall = new EasyCall())
                                    {
                                        transaction = EasyCall.VoidPin(item.ProviderReference, item.Url);
                                    }
                                }
                                /*else if (item.ProductTypeId.Equals(LOCAL_PIN))
                                {
                                    using (RoutingDAO pinDao = new RoutingDAO())
                                    {
                                        transaction = pinDao.BuyPin(accountId, item.ProductId, amount);
                                    }
                                }*/
                            }
                            else if (item.ProviderId.Equals("7")) //National
                            {
                                if (item.ProductTypeId.Equals(RECHARGE)) //Topup
                                {
                                    using (National National = new National())
                                    {
                                        transaction = National.VoidRecharge(item.ProductCode,
                                            amount, phone, item.ProviderReference);
                                    }
                                }
                                else if (item.ProductTypeId.Equals(PIN))    //Pin
                                {
                                    using (National National = new National())
                                    {
                                        transaction = National.VoidPin(item.ProductCode,
                                            amount, item.ProviderReference);
                                    }
                                }
                                /*else if (item.ProductTypeId.Equals(LOCAL_PIN))
                                {
                                    using (RoutingDAO pinDao = new RoutingDAO())
                                    {
                                        transaction = pinDao.BuyPin(accountId, item.ProductId, amount);
                                    }
                                }*/
                            }
                            else if (item.ProviderId.Equals("8")) //Tricom
                            {
                                if (item.ProductTypeId.Equals(RECHARGE)) //Topup
                                {
                                    using (Tricom Tricom = new Tricom())
                                    {
                                        transaction = Tricom.Void(phone, item.LocalCurrencyAmount, item.ProviderReference,
                                            item.VoidRoutingId, item.Ip, Convert.ToInt32(item.Port));
                                    }
                                }
                                /*else if (item.ProductTypeId.Equals(LOCAL_PIN))
                                {
                                    using (RoutingDAO pinDao = new RoutingDAO())
                                    {
                                        transaction = pinDao.BuyPin(accountId, item.ProductId, amount);
                                    }
                                }*/
                            }
                            else if (item.ProviderId.Equals("9")) //Claro Local
                            {
                                using (ClaroLocal ClaroLocal = new ClaroLocal(Common.Utils.UTD))
                                {
                                    transaction = ClaroLocal.VoidRecharge(phone, item.CurrencyAmount,
                                        item.ProviderReference, item.Url);
                                }
                            }
                            else if (item.ProviderId.Equals("262")) //Claro Local
                            {
                                using (ClaroLocal ClaroLocal = new ClaroLocal(Common.Utils.FREESTAR))
                                {
                                    transaction = ClaroLocal.VoidRecharge(phone, item.CurrencyAmount,
                                        item.ProviderReference, item.Url);
                                }
                            }
                            else if (item.ProviderId.Equals("10") || item.ProviderId.Equals("261")) //Orange y Movil BanReservas
                            {
                                if (item.ProductTypeId.Equals(RECHARGE)) //Topup
                                {
                                    using (Orange Orange = new Orange())
                                    {
                                        transaction = Orange.Void(item.RechargeStamp, item.ProviderReference,
                                         item.RoutingId, phone, item.Ip, Convert.ToInt32(item.Port));
                                    }
                                }
                                /*else if (item.ProductTypeId.Equals(LOCAL_PIN))
                                {
                                    using (RoutingDAO pinDao = new RoutingDAO())
                                    {
                                        transaction = pinDao.BuyPin(accountId, item.ProductId, amount);
                                    }
                                }*/
                            }
                            /*else if (item.ProviderId.Equals("11")) //Viva
                            {
                                if (item.ProductTypeId.Equals(LOCAL_PIN))
                                {
                                    using (RoutingDAO pinDao = new RoutingDAO())
                                    {
                                        transaction = pinDao.BuyPin(accountId, item.ProductId, amount);
                                    }
                                }
                            }*/
                            else if (item.ProviderId.Equals("12")) //Claro Internacional
                            {
                                if (item.ProductTypeId.Equals(RECHARGE)) //Topup
                                {
                                    using (ClaroExt ClaroExt = new ClaroExt(item.Url))
                                    {
                                        transaction = ClaroExt.Void(phone, amount, item.VoidRoutingId);
                                    }
                                }
                                /*else if (item.ProductTypeId.Equals(LOCAL_PIN))
                                {
                                    using (RoutingDAO pinDao = new RoutingDAO())
                                    {
                                        transaction = pinDao.BuyPin(accountId, item.ProductId, amount);
                                    }
                                }*/
                            }
                            else if (item.ProviderId.Equals("16")) //TWI
                            {
                                if (item.ProductTypeId.Equals(RECHARGE)) //Topup
                                {
                                    if (item.TransactionMode.Equals("0")) //Fix
                                    {
                                        using (Prasan Prasan = new Prasan())
                                        {
                                            transaction = Prasan.Void(item.VoidRoutingId, item.Url, Common.Utils.FIX_RECHARGE, Prasan.TYPE_TWI);
                                        }
                                    }
                                    else
                                    {
                                        using (Prasan Prasan = new Prasan())
                                        {
                                            transaction = Prasan.Void(item.VoidRoutingId, item.Url, Common.Utils.FLEXI_RECHARGE, Prasan.TYPE_TWI);
                                        }
                                    }
                                }
                                /*else if (item.ProductTypeId.Equals(LOCAL_PIN))
                                {
                                    using (RoutingDAO pinDao = new RoutingDAO())
                                    {
                                        transaction = pinDao.BuyPin(accountId, item.ProductId, amount);
                                    }
                                }*/
                            }
                            else if (item.ProviderId.Equals(OFFICIAL_PREPAID_NATION))
                            {
                                using (PrepaidNation PrepaidNation = new PrepaidNation("official"))
                                {
                                    transaction = PrepaidNation.Void(Convert.ToInt64(item.VoidRoutingId), item.RoutingId, item.TerminalId, item.Url);
                                }
                            }
                            else if (item.ProviderId.Equals(AIPC_PREPAID_NATION))
                            {
                                using (PrepaidNation PrepaidNation = new PrepaidNation("aipc"))
                                {
                                    transaction = PrepaidNation.Void(Convert.ToInt64(item.VoidRoutingId), item.RoutingId, item.TerminalId, item.Url);
                                }
                            }

                            result.Code = transaction.Code;
                            result.Details = transaction.Details;
                            result.Id = transaction.TransactionId;
                            result.Message = transaction.Message;
                            endDate = DateTime.Now;

                            if (result.Code.Length > 5)
                            {
                                result.Details = result.Code;
                                result.Code = Common.Utils.UNCAUGHT_ERROR;
                                result.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                            }
                            else
                            {
                                returnResponse = responseCodeDao.GetResponseCode(item.ProviderId,
                                result.Code, result.Message, locale, dbUser);

                                result.Code = returnResponse.Code;
                                result.Message = returnResponse.Message;
                            }

                            resultProvider.AppendLine(item.ProviderName);
                            resultProvider.Append("Transaction Id: ");
                            resultProvider.Append(item.LogId);
                            resultProvider.Append("|Void Routing Id: ");
                            resultProvider.Append(item.VoidRoutingId);
                            resultProvider.Append("|Amount: ");
                            resultProvider.Append(amount);
                            resultProvider.Append("|Phone: ");
                            resultProvider.Append(phone);
                            resultProvider.Append("|Begin Date: ");
                            resultProvider.Append(beginDate.ToString());
                            resultProvider.Append("|End Date: ");
                            resultProvider.Append(endDate.ToString());
                            resultProvider.Append("|Code: ");
                            resultProvider.Append(result.Code);
                            resultProvider.Append("|Message: ");
                            resultProvider.AppendLine(result.Message);
                            resultProvider.Append("Request:  ").AppendLine(transaction.RequestMsg);
                            resultProvider.Append("Response: ").AppendLine(transaction.ResponseMsg);

                            dao.TransLog(item.RoutingId, beginDate, endDate, result.Code,
                                result.Message, null, null,
                                null, locale, dbUser, null);

                            if (result.Code.Equals(Common.Utils.APPROVED))
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        result.Code = "00";
                        result.Message = "Success! - THIS IS UTP TEST WEB SERVICE!";

                        dao.TransLog(routingId, DateTime.Now, DateTime.Now, result.Code,
                                result.Message, providerReference,
                                null, null, locale, dbUser, null);

                    }
                }
                else
                {
                    try
                    {
                        long temp;

                        if (!Int64.TryParse(routingResult.LogId, out temp))
                        {
                            routingResult.LogId = String.Empty;
                        }
                    }
                    catch (Exception)
                    {
                        routingResult.LogId = String.Empty;
                    }


                    if (!String.IsNullOrEmpty(routingResult.LogId))
                    {
                        returnResponse = apiLogDao.SearchApiLogResponseCode(routingResult.LogId);

                        if (returnResponse.Code.Equals(Common.Utils.NO_RECORDS_FOUND) ||
                            returnResponse.Code.Equals(Common.Utils.UNKNOWN_EXCEPTION))
                        {
                            result.Code = routingResult.Code;
                            result.Message = routingResult.Message;
                        }
                        else
                        {
                            result.Code = returnResponse.Code;
                            result.Message = returnResponse.Message;
                        }
                    }
                    else
                    {

                    }

                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                    if (res.Code.Equals("0"))
                    {
                        result.Code = res.ResponseCode[0].Code;
                        result.Message = res.ResponseCode[0].Message;
                    }
                    else
                    {
                        result.Code = Common.Utils.UNCAUGHT_ERROR;
                        result.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                    }
                }

                output.AppendLine("");
                output.AppendLine("***********************************************");
                output.AppendLine("Utp Web Service - VoidByReference");
                output.Append("Reference            : ").AppendLine(reference);
                output.Append("Api User             : ").AppendLine(apiUser);
                output.Append("Account Id           : ").AppendLine(accountId);
                output.Append("Reference Void       : ").AppendLine(referenceVoid);
                output.Append("Amount               : ").AppendLine(amount);
                output.Append("Authorization Number : ").AppendLine(providerReference);
                output.Append("Phone                : ").AppendLine(phone);
                output.Append("Locale               : ").AppendLine(locale);
                output.Append("User                 : ").AppendLine(user);
                output.Append("Ip User              : ").AppendLine(ipUser);
                output.Append("Ip                   : ").AppendLine(ip);
                output.Append("Code                 : ").AppendLine(result.Code);
                output.Append("Message              : ").AppendLine(result.Message);
                output.Append("Details              : ").AppendLine(result.Details);
                output.Append("Provider Output      : ").AppendLine(routingDt.ToString());
                output.Append("Result Output        : ").AppendLine(resultProvider.ToString());
                output.AppendLine("***********************************************");

                log.Debug(output.ToString());
            }

            return result;
        }


        [WebMethod]
        public GeneralBillInformation QueryBill(RoutingResult routingResult, String typeDb, String contract, 
            String accountId, String locale, String dbUser, String user, String ip)
        {
            GeneralBillInformation billInformation = new GeneralBillInformation();
            RoutingResult result = new RoutingResult();

            StringBuilder output = new StringBuilder();
            DateTime beginDate, endDate;
            StringBuilder routingDt = new StringBuilder();
            StringBuilder resultProvider = new StringBuilder();
            Boolean errorFlag = false;
            DateTime? dateSend = null, dateRecv = null;
            String lastProviderId = null;
            ResponseCodeDAO responseCodeDao = new ResponseCodeDAO();
            Common.Result responseCodeResult = null;
            Common.Result errorResult = null;
            ProviderDAO providerDao = new ProviderDAO();
            String req = null, resp = null, transactionId = null;
            Common.Result returnResponse;
            RoutingDAO dao = new RoutingDAO();

            try
            {
                foreach (RoutingProvider item in routingResult.RoutingProvider)
                {

                    routingDt.Append("Transaction Id: ");
                    routingDt.Append(item.LogId);
                    routingDt.Append("|Routing Id: ");
                    routingDt.Append(item.RoutingId);
                    routingDt.Append("|Operator Code: ");
                    routingDt.AppendLine(item.OperatorCode);
                    routingDt.Append("|Product Code: ");
                    routingDt.Append(item.ProductCode);
                    routingDt.Append("|Product Type Id: ");
                    routingDt.Append(item.ProductTypeId);
                    routingDt.Append("|Profile Id: ");
                    routingDt.AppendLine(item.ProfileId);
                    routingDt.Append("|Provider Account Id: ");
                    routingDt.Append(item.ProviderAccountId);
                    routingDt.Append("|Provider Id: ");
                    routingDt.Append(item.ProviderId);
                    routingDt.Append("|Routing Id: ");
                    routingDt.AppendLine(item.RoutingId);
                    routingDt.Append("|Template Id: ");
                    routingDt.Append(item.TemplateId);
                    routingDt.Append("|Weight: ");
                    routingDt.Append(item.Weight);
                    routingDt.Append("|Country Code: ");
                    routingDt.Append(item.CountryCode);
                    routingDt.Append("|Terminal Id: ");
                    routingDt.AppendLine(item.TerminalId);
                    routingDt.Append("|Phone: ");
                    routingDt.AppendLine(item.Phone);
                }


                foreach (RoutingProvider item in routingResult.RoutingProvider)
                {
                    beginDate = DateTime.Now;
                    lastProviderId = item.ProviderId;

                    if (typeDb.Equals("N"))
                    {
                        if (item.ProviderId.Equals(TPAGO)) //T-PAGO
                        {
                            dateSend = DateTime.Now;

                            using (Tpago tpago = new Tpago())
                            {
                                billInformation = tpago.QueryInvoiceBalance(item.ProductCode, contract, item.Url); 
                            }

                            dateRecv = DateTime.Now;
                        }
                        else
                        {
                            billInformation.Code = Common.Utils.PROVIDER_NOT_FOUND;
                        }
                    }
                    else
                    {
                        String isgood = Common.Utils.GeneraNumeroRamdon(0, 3);
                        String esperar = Common.Utils.GeneraNumeroRamdon(0, 3);

                        if (esperar.Equals("0"))
                        {
                            System.Threading.Thread.Sleep(35000);
                        }


                        billInformation.TransactionDateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                        if (isgood.Equals("0"))
                        {
                            String error = Common.Utils.GeneraNumeroRamdon(0, 4);

                            switch (error)
                            {
                                case "0": billInformation.Code = Common.Utils.PROVIDER_NOT_FOUND;
                                    break;
                                case "1": billInformation.Code = Common.Utils.PHONE_DOES_NOT_EXISTS;
                                    break;
                                case "2": billInformation.Code = Common.Utils.RECHARGE_NO_RESPONSE;
                                    break;
                                default: billInformation.Code = Common.Utils.UNKNOWN_EXCEPTION;
                                    break;
                            }

                            billInformation.Message = String.Empty;
                        }
                        else
                        {
                            billInformation.Code = Common.Utils.APPROVED;
                            billInformation.Message = "Success! - THIS IS UTP TEST WEB SERVICE!";
                        }
                    }



                    endDate = DateTime.Now;

                    billInformation.TransactionId = item.LogId;
                    billInformation.ProviderId = item.ProviderId;
                    billInformation.ProviderDescription = item.ProviderName;
                    billInformation.ProductId = item.ProductId;
                    billInformation.ProductName = item.ProductName;
                    billInformation.Cost = item.CurrencyAmount;


                    if (billInformation.Code.Length > 5)
                    {
                        billInformation.Code = Common.Utils.UNCAUGHT_ERROR;
                        billInformation.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                    }

                    RoutingTransactionLogResult tranResult = dao.TransLog(item.RoutingId, beginDate, endDate,
                        billInformation.Code,
                        billInformation.Message,
                        null,
                        null,
                        null,
                        locale,
                        dbUser,
                        billInformation.TransactionDateTime);

                    returnResponse = responseCodeDao.GetResponseCode(billInformation.ProviderId,
                        billInformation.Code, billInformation.Message, locale, dbUser);

                    billInformation.Code = returnResponse.Code;

                    if (typeDb.Equals("Y") && billInformation.Code == Common.Utils.APPROVED)
                    {
                        billInformation.Message = "Success! - THIS IS UTP TEST WEB SERVICE!";
                    }
                    else
                    {
                        billInformation.Message = returnResponse.Message;
                    }



                    resultProvider.AppendLine(item.ProviderName);
                    resultProvider.Append("Transaction Id: ");
                    resultProvider.Append(billInformation.TransactionId);
                    resultProvider.Append("|Code: ");
                    resultProvider.Append(billInformation.Code);
                    resultProvider.Append("|Message: ");
                    resultProvider.Append(billInformation.Message);
                    resultProvider.Append("|Cost: ");
                    resultProvider.Append(billInformation.Cost);
                    resultProvider.Append("|Minimum Amount: ");
                    resultProvider.Append(billInformation.MinimumAmount);
                    resultProvider.Append("|Bill Amount: ");
                    resultProvider.Append(billInformation.BillAmount);
                    resultProvider.Append("|Bill Date: ");
                    resultProvider.Append(billInformation.BillDate);
                    resultProvider.Append("|Product Name: ");
                    resultProvider.Append(billInformation.ProductName);
                    resultProvider.Append("|Sent: ");
                    resultProvider.Append((dateSend == null) ? null : dateSend.ToString());
                    resultProvider.Append("|Received: ");
                    resultProvider.Append((dateRecv == null) ? null : dateRecv.ToString());
                    resultProvider.Append("|Tran. Result Code: ");
                    resultProvider.Append(tranResult.Code);
                    resultProvider.Append("|Tran. Result Desc.: ");
                    resultProvider.Append(tranResult.Message);
                    resultProvider.Append("|Tran. Result Detail: ");
                    resultProvider.Append(tranResult.Details);
                    resultProvider.Append("|Provider Id: ");
                    resultProvider.Append(billInformation.ProviderId);
                    resultProvider.Append("|Provider Description: ");
                    resultProvider.AppendLine(billInformation.ProviderDescription);
                    
                    if (!tranResult.Code.Equals("0"))
                    {
                        RoutingTransactionLogResult r = dao.TransLogWs(item.RoutingId, "9", beginDate, endDate,
                            billInformation.Code, billInformation.Message,
                            null, null, null, locale,
                            dbUser, billInformation.TransactionDateTime, tranResult.Details);

                        resultProvider.Append("|Code TransLogWs: ");
                        resultProvider.Append(r.Code);
                        resultProvider.Append("|Message TransLogWs: ");
                        resultProvider.AppendLine(r.Message);
                    }

                    resultProvider.Append("Request:");
                    resultProvider.AppendLine(billInformation.RequestMsg);
                    resultProvider.Append("Response:");
                    resultProvider.AppendLine(billInformation.ResponseMsg);


                    if (billInformation.Code.Equals(Common.Utils.APPROVED))
                    {
                        break;
                    }
                }


            }
            catch (Exception ex)
            {
                billInformation.Message = ex.Message;
                billInformation.Code = ex.Message;
                errorFlag = true;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                    if (res.Code.Equals("0"))
                    {
                        result.Code = res.ResponseCode[0].Code;
                        result.Message = res.ResponseCode[0].Message;
                    }
                    else
                    {
                        result.Code = Common.Utils.UNCAUGHT_ERROR;
                        result.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                    }
                }

                output.AppendLine("");
                output.AppendLine("***********************************************");
                output.AppendLine("Utp Web Service - QueryBill");
                output.Append("Transaction Id   : ").AppendLine(transactionId);
                output.Append("Account Id       : ").AppendLine(accountId);
                output.Append("Contract         : ").AppendLine(contract);
                output.Append("Locale           : ").AppendLine(locale);
                output.Append("User             : ").AppendLine(dbUser);
                output.Append("Ip               : ").AppendLine(ip);
                output.Append("Code             : ").AppendLine(billInformation.Code);
                output.Append("Message          : ").AppendLine(billInformation.Message);
                output.Append("Minimum Amount   : ").AppendLine(billInformation.MinimumAmount);
                output.Append("Bill Amount      : ").AppendLine(billInformation.BillAmount);
                output.Append("Bill Date        : ").AppendLine(billInformation.BillDate);
                output.Append("Routing Output   : ").AppendLine(routingDt.ToString());
                output.Append("Provider Output  : ").AppendLine(resultProvider.ToString());
                output.AppendLine("***********************************************");

                billInformation.ResponseMsg = String.Empty;
                billInformation.RequestMsg = String.Empty;

                log.Debug(output.ToString());
            }

            return billInformation;
        }


        [WebMethod]
        public GeneralPaymentInformation PayBill(RoutingResult routingResult, String typeDb, String contract, String amount,
             String accountId, String locale, String dbUser, String user, String ip)
        {
            GeneralPaymentInformation generalPay = new GeneralPaymentInformation();
            RoutingResult result = new RoutingResult();

            StringBuilder output = new StringBuilder();
            DateTime beginDate, endDate;
            StringBuilder routingDt = new StringBuilder();
            StringBuilder resultProvider = new StringBuilder();
            Boolean errorFlag = false;
            DateTime? dateSend = null, dateRecv = null;
            String lastProviderId = null;
            ResponseCodeDAO responseCodeDao = new ResponseCodeDAO();
            Common.Result responseCodeResult = null;
            Common.Result errorResult = null;
            ProviderDAO providerDao = new ProviderDAO();
            String req = null, resp = null, transactionId = null;
            Common.Result returnResponse;
            RoutingDAO dao = new RoutingDAO();

            try
            {
                foreach (RoutingProvider item in routingResult.RoutingProvider)
                {

                    routingDt.Append("Transaction Id: ");
                    routingDt.Append(item.LogId);
                    routingDt.Append("|Routing Id: ");
                    routingDt.Append(item.RoutingId);
                    routingDt.Append("|Operator Code: ");
                    routingDt.AppendLine(item.OperatorCode);
                    routingDt.Append("|Product Code: ");
                    routingDt.Append(item.ProductCode);
                    routingDt.Append("|Product Type Id: ");
                    routingDt.Append(item.ProductTypeId);
                    routingDt.Append("|Profile Id: ");
                    routingDt.AppendLine(item.ProfileId);
                    routingDt.Append("|Provider Account Id: ");
                    routingDt.Append(item.ProviderAccountId);
                    routingDt.Append("|Provider Id: ");
                    routingDt.Append(item.ProviderId);
                    routingDt.Append("|Routing Id: ");
                    routingDt.AppendLine(item.RoutingId);
                    routingDt.Append("|Template Id: ");
                    routingDt.Append(item.TemplateId);
                    routingDt.Append("|Weight: ");
                    routingDt.Append(item.Weight);
                    routingDt.Append("|Country Code: ");
                    routingDt.Append(item.CountryCode);
                    routingDt.Append("|Terminal Id: ");
                    routingDt.AppendLine(item.TerminalId);
                    routingDt.Append("|Phone: ");
                    routingDt.AppendLine(item.Phone);
                }


                foreach (RoutingProvider item in routingResult.RoutingProvider)
                {
                    beginDate = DateTime.Now;
                    lastProviderId = item.ProviderId;

                    if (typeDb.Equals("N"))
                    {
                        if (item.ProviderId.Equals(TPAGO)) //T-PAGO
                        {
                            dateSend = DateTime.Now;

                            using (Tpago tpago = new Tpago())
                            {
                                generalPay = tpago.Payment(item.Url, item.RoutingId, item.ProductCode, contract,
                                    amount);
                            }

                            dateRecv = DateTime.Now;
                        }
                        else
                        {
                                generalPay.Code = Common.Utils.PROVIDER_NOT_FOUND;
                        }
                    }
                    else
                    {
                        String isgood = Common.Utils.GeneraNumeroRamdon(0, 3);
                        String esperar = Common.Utils.GeneraNumeroRamdon(0, 3);

                        if (esperar.Equals("0"))
                        {
                            System.Threading.Thread.Sleep(35000);
                        }


                        generalPay.TransactionDateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                        if (isgood.Equals("0"))
                        {
                            String error = Common.Utils.GeneraNumeroRamdon(0, 4);

                            switch (error)
                            {
                                case "0": generalPay.Code = Common.Utils.PROVIDER_NOT_FOUND;
                                    break;
                                case "1": generalPay.Code = Common.Utils.PHONE_DOES_NOT_EXISTS;
                                    break;
                                case "2": generalPay.Code = Common.Utils.RECHARGE_NO_RESPONSE;
                                    break;
                                default: generalPay.Code = Common.Utils.UNKNOWN_EXCEPTION;
                                    break;
                            }

                            generalPay.Message = String.Empty;
                        }
                        else
                        {
                            generalPay.Code = Common.Utils.APPROVED;
                            generalPay.Message = "Success! - THIS IS UTP TEST WEB SERVICE!";

                            if (item.ProductTypeId.Equals(RECHARGE))
                            {
                                generalPay.AuthorizationNumber = Common.Utils.GeneraNumeroRamdon(111111111, 999999999);
                            }
                            else if (item.ProductTypeId.Equals(PIN))
                            {
                                generalPay.AuthorizationNumber = Common.Utils.GeneraNumeroRamdon(111111111, 999999999);
                            }
                            else
                            {
                                generalPay.Code = Common.Utils.PROVIDER_NOT_FOUND;
                                generalPay.Message = String.Empty;

                            }
                        }
                    }



                    endDate = DateTime.Now;

                    generalPay.TransactionId = item.LogId;
                    generalPay.ProviderId = item.ProviderId;
                    generalPay.ProviderDescription = item.ProviderName;
                    generalPay.ProductId = item.ProductId;
                    generalPay.ProductName = item.ProductName;
                    generalPay.Cost = item.CurrencyAmount;


                    if (generalPay.Code.Length > 5)
                    {
                        generalPay.Code = Common.Utils.UNCAUGHT_ERROR;
                        generalPay.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                    }

                    RoutingTransactionLogResult tranResult = dao.TransLog(item.RoutingId, beginDate, endDate,
                        generalPay.Code,
                        generalPay.Message,
                        generalPay.AuthorizationNumber,
                        null,
                        null,
                        locale,
                        dbUser,
                        generalPay.TransactionDateTime);

                    returnResponse = responseCodeDao.GetResponseCode(generalPay.ProviderId,
                        generalPay.Code, generalPay.Message, locale, dbUser);

                    generalPay.Code = returnResponse.Code;

                    if (typeDb.Equals("Y") && generalPay.Code == Common.Utils.APPROVED)
                    {
                        generalPay.Message = "Success! - THIS IS UTP TEST WEB SERVICE!";
                    }
                    else
                    {
                        generalPay.Message = returnResponse.Message;
                    }



                    resultProvider.AppendLine(item.ProviderName);
                    resultProvider.Append("Transaction Id: ");
                    resultProvider.Append(generalPay.TransactionId);
                    resultProvider.Append("Authorization Number: ");
                    resultProvider.Append(generalPay.AuthorizationNumber);
                    resultProvider.Append("|Code: ");
                    resultProvider.Append(generalPay.Code);
                    resultProvider.Append("|Message: ");
                    resultProvider.Append(generalPay.Message);
                    resultProvider.Append("|Cost: ");
                    resultProvider.Append(generalPay.Cost);
                    resultProvider.Append("|Product Id: ");
                    resultProvider.Append(generalPay.ProductId);
                    resultProvider.Append("|Product Name: ");
                    resultProvider.Append(generalPay.ProductName);
                    resultProvider.Append("|Sent: ");
                    resultProvider.Append((dateSend == null) ? null : dateSend.ToString());
                    resultProvider.Append("|Received: ");
                    resultProvider.Append((dateRecv == null) ? null : dateRecv.ToString());
                    resultProvider.Append("|Tran. Result Code: ");
                    resultProvider.Append(tranResult.Code);
                    resultProvider.Append("|Tran. Result Desc.: ");
                    resultProvider.Append(tranResult.Message);
                    resultProvider.Append("|Tran. Result Detail: ");
                    resultProvider.Append(tranResult.Details);
                    resultProvider.Append("|Provider Id: ");
                    resultProvider.Append(generalPay.ProviderId);
                    resultProvider.Append("|Provider Description: ");
                    resultProvider.AppendLine(generalPay.ProviderDescription);
                    
                    if (!tranResult.Code.Equals("0"))
                    {
                        RoutingTransactionLogResult r = dao.TransLogWs(item.RoutingId, "9", beginDate, endDate, generalPay.Code, generalPay.Message,
                            generalPay.AuthorizationNumber, null, null, locale,
                            dbUser, generalPay.TransactionDateTime, tranResult.Details);

                        resultProvider.Append("|Code TransLogWs: ");
                        resultProvider.Append(r.Code);
                        resultProvider.Append("|Message TransLogWs: ");
                        resultProvider.AppendLine(r.Message);
                    }

                    resultProvider.Append("Request:");
                    resultProvider.AppendLine(generalPay.RequestMsg);
                    resultProvider.Append("Response:");
                    resultProvider.AppendLine(generalPay.ResponseMsg);

                    if (generalPay.Code.Equals(Common.Utils.APPROVED))
                    {
                        break;
                    }
                }


            }
            catch (Exception ex)
            {
                generalPay.Message = ex.Message;
                generalPay.Code = ex.Message;
                errorFlag = true;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                    if (res.Code.Equals("0"))
                    {
                        result.Code = res.ResponseCode[0].Code;
                        result.Message = res.ResponseCode[0].Message;
                    }
                    else
                    {
                        result.Code = Common.Utils.UNCAUGHT_ERROR;
                        result.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                    }
                }

                output.AppendLine("");
                output.AppendLine("***********************************************");
                output.AppendLine("Utp Web Service - PayBill");
                output.Append("Transaction Id   : ").AppendLine(transactionId);
                output.Append("Account Id       : ").AppendLine(accountId);
                output.Append("Amount           : ").AppendLine(amount);
                output.Append("Contract         : ").AppendLine(contract);
                output.Append("Locale           : ").AppendLine(locale);
                output.Append("User             : ").AppendLine(dbUser);
                output.Append("Ip               : ").AppendLine(ip);
                output.Append("Code             : ").AppendLine(generalPay.Code);
                output.Append("Message          : ").AppendLine(generalPay.Message);
                output.Append("Routing Output   : ").AppendLine(routingDt.ToString());
                output.Append("Provider Output  : ").AppendLine(resultProvider.ToString());
                output.AppendLine("***********************************************");

                generalPay.ResponseMsg = String.Empty;
                generalPay.RequestMsg = String.Empty;

                log.Debug(output.ToString());
            }

            return generalPay;
        }

        //[WebMethod]
        //public BillersResult TpagoBillers(String url, String userId, String authKey)
        //{
        //    Tpago tPago = new Tpago();
        //    BillersResult result = null;

        //    result = tPago.QueryBillers(url, userId, authKey);

        //    return result;
        //}


        //[WebMethod]
        //public GeneralPaymentInformation TpagoPayment(String url, String userId, String authKey, String transactionId,
        //    String billerId, String contract, String amount, String currency)
        //{
        //    Tpago tPago = new Tpago();
        //    GeneralPaymentInformation result = null;

        //    result = tPago.Payment(url, transactionId, billerId, contract, amount);

        //    return result;
        //}


        //[WebMethod]
        //public GeneralBillInformation TpagoQueryInvoice(String url, 
        //    String billerId, String contract)
        //{
        //    Tpago tPago = new Tpago();
        //    GeneralBillInformation result = null;

        //    result = tPago.QueryInvoiceBalance(billerId, contract, url);

        //    return result;
        //}


        //[WebMethod]
        //public WsPrepaidNation.SkuListResponse PrepaidNationQueryProducts(String url)
        //{
        //    PrepaidNation prepaidNation = new PrepaidNation();
        //    WsPrepaidNation.SkuListResponse list = null;

        //    list = prepaidNation.QueryProducts(url);

        //    return list;
        //}

        //[WebMethod]
        //public Transaction PrepaidNationTopUp(int productId, String transactionId, String storeId, String phone, decimal amount, String url)
        //{
        //    PrepaidNation prepaidNation = new PrepaidNation();
        //    Transaction tran = null;

        //    tran = prepaidNation.TopUp(productId, transactionId, storeId, phone, amount, url);

        //    return tran;
        //}


        //[WebMethod]
        //public Transaction PrepaidNationPin(int productId, String transactionId, String storeId, String url)
        //{
        //    PrepaidNation prepaidNation = new PrepaidNation();
        //    Transaction tran = null;

        //    tran = prepaidNation.PurchasePin(productId, transactionId, storeId, url);

        //    return tran;
        //}

        //[WebMethod]
        //public Transaction PrepaidNationVoid(long oldTransactionId, String transactionId, String storeId, String url)
        //{
        //    PrepaidNation prepaidNation = new PrepaidNation();
        //    Transaction tran = null;

        //    tran = prepaidNation.Void(oldTransactionId, transactionId, storeId, url);

        //    return tran;
        //}

        //[WebMethod]
        //public Transaction PinlessRecharge(String productId, String phone,
        //    String amount, String transactionId, String accountId, int dollarFree, String url)
        //{
        //    Pinless pinless = new Pinless("pinless");
        //    Transaction result = new Transaction();

        //    result = pinless.Recharge(productId, phone, amount, transactionId, accountId, dollarFree, url);
        
        //    return result;
        //}


        //[WebMethod]
        //public Transaction AyBRecharge(String productCode, String phone,
        //    String amount, String transactionId, String idPunto, String ip, String puerto)
        //{
        //    AyB ayb = new AyB();
        //    Transaction result = new Transaction();

        //    result = ayb.Recharge(productCode, phone, amount, transactionId, idPunto, ip, puerto);

        //    return result;
        //}

        //[WebMethod]
        //public Transaction AyBVoid(String productCode, String transactionId, String idPunto, String ip, String puerto)
        //{
        //    AyB ayb = new AyB();
        //    Transaction result = new Transaction();

        //    result = ayb.Void(transactionId, idPunto, ip, puerto);


        //    return result;
        //}


        //[WebMethod(EnableSession = true)]
        //public Transaction SendSmsTrumpia(String url, String firstName, String lastName, String countryCode, String phone, String message)
        //{
        //    Trumpia trumpia = new Trumpia("udr");
        //    Transaction result = new Transaction();

        //    result = trumpia.SendSms(url, firstName, lastName, countryCode, phone, message);

        //    return result;
        //}


        //[WebMethod]
        //public Transaction MidasCardRecharge(String productCode, String phone, String amount, String transactionId, String accountId, String url)
        //{
        //    MidasCard midas = new MidasCard();
        //    Transaction result = new Transaction();

        //    result = midas.Recharge(productCode, phone, amount, transactionId, accountId, url);

        //    return result;
        //}


        //[WebMethod]
        //public Transaction MidasCardVoid(String transactionId, String accountId, String url)
        //{
        //    MidasCard midas = new MidasCard();
        //    Transaction result = new Transaction();

        //    result = midas.Void(transactionId, accountId, url);

        //    return result;
        //}

        //[WebMethod]
        //public Transaction OrangeRecharge(String transactionId, String phone, String amount, String ip, int port)
        //{
        //    Transaction transaction = null;
        //    Orange orange = new Orange();

        //        transaction = orange.Recharge(transactionId, phone, amount, ip, port);
         

        //    return transaction;
        //}


        //[WebMethod]
        //public Transaction OrangeVoid(String refillTransactionDate, String refillTransactionId, String transactionId, String phone, String amount, String ip, int port)
        //{
        //    Transaction transaction = null;

        //    using (Orange Orange = new Orange())
        //    {
        //        transaction = Orange.Void(refillTransactionDate, refillTransactionId,transactionId,  phone, ip, port);
        //    }

        //    return transaction;
        //}


        //[WebMethod]
        //public Transaction ClaroLocalConsultaUtd(String transactionId, String url)
        //{
        //    Transaction transaction = null;
        //    ClaroLocal claroLocal = new ClaroLocal(Common.Utils.UTD);

        //    transaction = claroLocal.Query(transactionId, url);

        //    return transaction;
        //}

        //[WebMethod]
        //public Transaction VivaRecarga(String phone, int entity, int subentity, float amount, String currencyCode, String transactionId)
        //{
        //    Transaction transaction = new Transaction();
        //    WsViva.VivaRechargeWsdl viva = new WsViva.VivaRechargeWsdl();
        //    WsViva.ResponseRechargeObject obj = null;

        //    viva.Credentials = new NetworkCredential("USRUTC", "UT*Abc123");

        //    obj  = viva.viva_topupRecharge(phone, entity, subentity, amount, "DOP", transactionId);

        //    transaction.Code = obj.code;
        //    transaction.Message = obj.message;
        //    transaction.AuthorizationNumber = obj.rechargeId.ToString();

        //    return transaction;
        //}



        //[WebMethod]
        //public String TestOrange(String ip, int port, int bytesReceived, String cmd)
        //{
        //    String response = null;
        //    Orange orange = new Orange();

        //    response = orange.NewProcess(ip, port, cmd, bytesReceived);

        //    return response;
        //}

        //[WebMethod]
        //public Transaction TestSoloPin(String productCode, String phone, String amount, String transactionId, String terminalId, String url)
        //{
        //    Transaction transactionResult = null;

        //    using (Pinless pinless = new Pinless(this.PINLESS))
        //    {
        //        int dollarFree = 0;

        //        transactionResult = pinless.Recharge(productCode, phone, amount, transactionId, terminalId, dollarFree, url);
        //    }

        //    return transactionResult;
        //}

        //[WebMethod]
        //public Transaction TestIdealTel(String transactionId, String phone, String amount, String url)
        //{
        //    Transaction transactionResult = null;

        //    using (IdealTel idealtel = new IdealTel())
        //    {
        //        transactionResult = idealtel.Recharge(transactionId, phone, amount, url);
        //    }

        //    return transactionResult;
        //}
    }
}
