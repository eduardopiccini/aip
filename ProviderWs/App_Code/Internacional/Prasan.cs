﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;
using System.Text;
using System.Net;
using System.Xml;
using System.IO;

namespace ProviderWs.Internacional
{
    public class Prasan : IDisposable
    {
        public const String TYPE_PRASAN = "prasan";
        public const String TYPE_TWI = "twi";

        public Prasan()
        {
        }

        public Transaction Recharge(String Phone, String Amount, String ProductId,
            String TransactionId, String url, String type, String providerType)
        {
            Transaction result = new Transaction();
            StringBuilder param = new StringBuilder();
            StringBuilder xmlReq = new StringBuilder();
            String toMd5 = null, xml = null, msg = null, loginId = null, protectedKey = null;
            Uri uri;
            HttpWebRequest req = null;
            HttpWebResponse resp = null;
            byte[] data = null;
            StreamReader reader = null;
            System.Xml.XmlDocument xdoc = new System.Xml.XmlDocument();
            System.Xml.XmlNode eCode, eMessage, eConfirmationCode;
            XmlNamespaceManager manager = null;
            StringBuilder output = new StringBuilder();

            try
            {
                loginId = System.Configuration.ConfigurationManager.AppSettings[providerType + "_login_id"].ToString();
                protectedKey = System.Configuration.ConfigurationManager.AppSettings[providerType + "_protected_key"].ToString();

                if (String.IsNullOrEmpty(Phone))
                    throw new Exception(Common.Utils.PHONE_NUMBER_REQUIRE);

                if (type.Equals(Common.Utils.FLEXI_RECHARGE))
                {
                    if (String.IsNullOrEmpty(Amount))
                        throw new Exception(Common.Utils.AMOUNT_REQUIRE);
                }

                if (String.IsNullOrEmpty(TransactionId))
                    throw new Exception(Common.Utils.TRANSACTION_ID_REQUIRE);

                param.Append(loginId);
                param.Append("|");
                param.Append(TransactionId);
                param.Append("|");
                param.Append(ProductId);
                param.Append("|2|");
                param.Append(Phone);
                param.Append("|");

                if (type.Equals(Common.Utils.FLEXI_RECHARGE))
                {
                    param.Append(Amount);
                    param.Append("|");
                }

                param.Append(protectedKey);

                toMd5 = Common.Utils.MD5_of_SHA1(param.ToString());

                xmlReq.Append("<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:m0=\"http://soapinterop.org/xsd\">");
                xmlReq.Append("<SOAP-ENV:Body>");
                xmlReq.Append("<m:");
                xmlReq.Append(type);
                xmlReq.Append("Recharge xmlns:m=\"");
                xmlReq.Append(url.Replace("/iTopUp/reseller_itopup.server.php", "/reseller_iTopUp/reseller_iTopUp.wsdl.php"));
                xmlReq.Append("\" SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">");
                xmlReq.Append("<");
                xmlReq.Append(type);
                xmlReq.Append("Recharge_Request xsi:type=\"m0:");
                xmlReq.Append(type);
                xmlReq.Append("Recharge_Request\"><LoginId xsi:type=\"xsd:string\">{0}</LoginId>");
                xmlReq.Append("<RequestId xsi:type=\"xsd:string\">{1}</RequestId>");
                xmlReq.Append("<BatchId xsi:type=\"xsd:string\">{2}</BatchId>");
                xmlReq.Append("<SystemServiceID xsi:type=\"xsd:string\">{3}</SystemServiceID>");
                xmlReq.Append("<MobileNumber xsi:type=\"xsd:string\">{4}</MobileNumber>");

                if (type.Equals(Common.Utils.FLEXI_RECHARGE))
                {
                    xmlReq.Append("<Amount xsi:type=\"xsd:string\">");
                    xmlReq.Append(Amount);
                    xmlReq.Append("</Amount>");
                }


                xmlReq.Append("<Checksum xsi:type=\"xsd:string\">{5}</Checksum>");
                xmlReq.Append("</");
                xmlReq.Append(type);
                xmlReq.Append("Recharge_Request>");
                xmlReq.Append("</m:");
                xmlReq.Append(type);
                xmlReq.Append("Recharge></SOAP-ENV:Body></SOAP-ENV:Envelope>");

                xml = String.Format(xmlReq.ToString(), loginId, TransactionId, ProductId, "2", Phone, toMd5);

                result.RequestMsg = xml;

                uri = new Uri(url);
                req = (HttpWebRequest)WebRequest.Create(uri);

                req.Headers.Add("SOAPAction: " + url.Replace("/iTopUp/reseller_itopup.server.php", "/reseller_iTopUp/reseller_iTopUp.wsdl.php"));

                req.ContentType = "text/xml";
                req.Method = "POST";

                data = System.Text.ASCIIEncoding.UTF8.GetBytes(xml);

                req.ContentLength = data.Length;
                System.IO.Stream os = req.GetRequestStream();
                os.Write(data, 0, data.Length);
                os.Close();

                resp = (HttpWebResponse)req.GetResponse();

                reader = new StreamReader(resp.GetResponseStream());

                msg = reader.ReadToEnd();

                result.ResponseMsg = msg;

                xdoc.LoadXml(msg);

                manager = new XmlNamespaceManager(xdoc.NameTable);
                manager.AddNamespace("ns4", url.Replace("/iTopUp/reseller_itopup.server.php", "/reseller_iTopUp/reseller_iTopUp.wsdl.php"));
                manager.AddNamespace("SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
                manager.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
                manager.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");


                eCode = xdoc.SelectSingleNode("/SOAP-ENV:Envelope/SOAP-ENV:Body/ns4:" + type + "RechargeResponse/" + type + "Recharge_Response/ResponseCode", manager);
                eMessage = xdoc.SelectSingleNode("/SOAP-ENV:Envelope/SOAP-ENV:Body/ns4:" + type + "RechargeResponse/" + type + "Recharge_Response/ResponseDescription", manager);
                eConfirmationCode = xdoc.SelectSingleNode("/SOAP-ENV:Envelope/SOAP-ENV:Body/ns4:" + type + "RechargeResponse/" + type + "Recharge_Response/ConfirmationCode", manager);

                result.Code = (eCode != null) ? eCode.InnerText : Common.Utils.RECHARGE_NO_RESPONSE;
                result.Message = (eMessage != null) ? eMessage.InnerText : "";
                result.AuthorizationNumber = (eConfirmationCode != null) ? eConfirmationCode.InnerText : "";
                result.TransactionDateTime = DateTime.Now.ToString();

                if (result.Code.Equals("000"))
                {
                    result.Code = Common.Utils.APPROVED;
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
            }
            finally
            {
                if (req != null)
                {
                    req = null;
                }

                if (data != null)
                {
                    data = null;
                }

                if (reader != null)
                {
                    reader = null;
                }
            }

            return result;
        }

        public Transaction Void(String TransactionId, String url, String type, String providerType)
        {
            Transaction result = new Transaction();
            StringBuilder param = new StringBuilder();
            StringBuilder xmlReq = new StringBuilder();
            String toMd5 = null, xml = null, msg = null, loginId = null, protectedKey = null;
            Uri uri;
            HttpWebRequest req = null;
            HttpWebResponse resp = null;
            byte[] data = null;
            StreamReader reader = null;
            System.Xml.XmlDocument xdoc = new System.Xml.XmlDocument();
            System.Xml.XmlNode eCode, eMessage;
            XmlNamespaceManager manager = null;
            StringBuilder output = new StringBuilder();

            try
            {
                loginId = System.Configuration.ConfigurationManager.AppSettings[providerType + "_login_id"].ToString();
                protectedKey = System.Configuration.ConfigurationManager.AppSettings[providerType + "_protected_key"].ToString();

                if (String.IsNullOrEmpty(TransactionId))
                    throw new Exception(Common.Utils.TRANSACTION_ID_REQUIRE);

                param.Append(loginId);
                param.Append("|");
                param.Append(TransactionId);
                param.Append("|");
                param.Append(protectedKey);

                toMd5 = Common.Utils.MD5_of_SHA1(param.ToString());

                xmlReq.Append("<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:m0=\"http://soapinterop.org/xsd\">");
                xmlReq.Append("<SOAP-ENV:Body>");
                xmlReq.Append("<m:");
                xmlReq.Append(type);
                xmlReq.Append("Void xmlns:m=\"");
                xmlReq.Append(url.Replace("/iTopUp/reseller_itopup.server.php", "/reseller_iTopUp/reseller_iTopUp.wsdl.php"));
                xmlReq.Append("\" SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">");
                xmlReq.Append("<");
                xmlReq.Append(type);
                xmlReq.Append("Void_Request xsi:type=\"m0:");
                xmlReq.Append(type);
                xmlReq.Append("Void_Request\"><LoginId xsi:type=\"xsd:string\">{0}</LoginId>");
                xmlReq.Append("<RequestId xsi:type=\"xsd:string\">{1}</RequestId>");
                xmlReq.Append("<Checksum xsi:type=\"xsd:string\">{2}</Checksum>");
                xmlReq.Append("</");
                xmlReq.Append(type);
                xmlReq.Append("Void_Request>");
                xmlReq.Append("</m:");
                xmlReq.Append(type);
                xmlReq.Append("Void></SOAP-ENV:Body></SOAP-ENV:Envelope>");

                xml = String.Format(xmlReq.ToString(), loginId, TransactionId, toMd5);

                result.RequestMsg = xml;

                uri = new Uri(url);
                req = (HttpWebRequest)WebRequest.Create(uri);

                req.Headers.Add("SOAPAction: " + url.Replace("/iTopUp/reseller_itopup.server.php", "/reseller_iTopUp/reseller_iTopUp.wsdl.php"));

                req.ContentType = "text/xml";
                req.Method = "POST";

                data = System.Text.ASCIIEncoding.UTF8.GetBytes(xml);

                req.ContentLength = data.Length;
                System.IO.Stream os = req.GetRequestStream();
                os.Write(data, 0, data.Length);
                os.Close();

                resp = (HttpWebResponse)req.GetResponse();

                reader = new StreamReader(resp.GetResponseStream());

                msg = reader.ReadToEnd();

                result.ResponseMsg = msg;

                xdoc.LoadXml(msg);

                manager = new XmlNamespaceManager(xdoc.NameTable);
                manager.AddNamespace("m", url.Replace("/iTopUp/reseller_itopup.server.php", "/reseller_iTopUp/reseller_iTopUp.wsdl.php"));
                manager.AddNamespace("SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
                manager.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
                manager.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");


                eCode = xdoc.SelectSingleNode("/SOAP-ENV:Envelope/SOAP-ENV:Body/m:" + type + "VoidResponse/" + type + "Void_Response/ResponseCode", manager);
                eMessage = xdoc.SelectSingleNode("/SOAP-ENV:Envelope/SOAP-ENV:Body/m:" + type + "VoidResponse/" + type + "Void_Response/ResponseDescription", manager);

                result.Code = (eCode != null) ? eCode.InnerText : Common.Utils.RECHARGE_NO_RESPONSE;
                result.Message = (eMessage != null) ? eMessage.InnerText : "";
                result.TransactionDateTime = DateTime.Now.ToString();

                if (result.Code.Equals("000"))
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
            }
            finally
            {
                if (req != null)
                {
                    req = null;
                }

                if (data != null)
                {
                    data = null;
                }

                if (reader != null)
                {
                    reader = null;
                }
            }

            return result;
        }

        #region IDisposable Members

        public void Dispose()
        {
            
        }

        #endregion
    }
}