﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using Common;
using ProviderWs.WsUnlimitedPins;

namespace ProviderWs.Internacional
{
    public class UnlimitedPins : IDisposable
    {
        private String MerchantId; 
        private String TerminalId;
        private String HardwareId; 
        private String Ip; 
        private String Password; 
        private String UserId;
        private ProviderWs.WsUnlimitedPins.Services WsUp;

        public UnlimitedPins()
        {
            WsUp = new ProviderWs.WsUnlimitedPins.Services();

            MerchantId = System.Configuration.ConfigurationManager.AppSettings["up_merchant_id"].ToString();
            TerminalId = System.Configuration.ConfigurationManager.AppSettings["up_terminal_id"].ToString();
            HardwareId = System.Configuration.ConfigurationManager.AppSettings["up_hardware_id"].ToString();
            Ip = System.Configuration.ConfigurationManager.AppSettings["up_ip"].ToString();
            Password = System.Configuration.ConfigurationManager.AppSettings["up_password"].ToString();
            UserId = System.Configuration.ConfigurationManager.AppSettings["up_user_id"].ToString();
        }


        public Transaction Void(String TransactionId)
        {
            Transaction result = new Transaction();
            String details = "";
            ClientData clientData = new ClientData();
            TransactionData transactionData = new TransactionData();
            TransactionResponse transactionResponse = null;
            StringBuilder output = new StringBuilder();

            try
            {
                if (String.IsNullOrEmpty(TransactionId))
                    throw new Exception(Common.Utils.TRANSACTION_ID_REQUIRE);

                clientData.HardwareId = HardwareId;
                clientData.IPAddress = Ip;
                clientData.MerchantId = Convert.ToInt64(MerchantId);
                clientData.Password = Password;
                clientData.TerminalId = Convert.ToInt64(TerminalId);
                clientData.UserId = UserId;

                transactionData.TransactionId = Convert.ToInt64(TransactionId);

                transactionResponse = WsUp.Void(clientData, transactionData);

                result.Code = transactionResponse.Result.ErrorCode.ToString();
                result.Message = transactionResponse.Result.ErrorDescription;

                if (transactionResponse.Result.ErrorCode == 0)
                {
                    result.Code = Common.Utils.APPROVED;
                    result.AuthorizationNumber = transactionResponse.ReferenceNumber.ToString();
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }

            return result;
        }

        public Transaction GetPin(String ProductId, String Amount, String TransactionId)
        {
            Transaction result = new Transaction();
            String details = "", batchNumber = "";
            int quantity = 1;
            ClientData clientData = new ClientData();
            ProductData productData = new ProductData();
            PinTransactionData pinTransactionData = new PinTransactionData();
            PinTransactionResponse pinTransactionResponse = null;
            StringBuilder output = new StringBuilder();

            try
            {
                if (String.IsNullOrEmpty(ProductId))
                    throw new Exception(Common.Utils.PRODUCT_ID_REQUIRE);

                if (String.IsNullOrEmpty(Amount))
                    throw new Exception(Common.Utils.AMOUNT_REQUIRE);

                if (String.IsNullOrEmpty(TransactionId))
                    throw new Exception(Common.Utils.TRANSACTION_ID_REQUIRE);

                clientData.HardwareId = HardwareId;
                clientData.IPAddress = Ip;
                clientData.MerchantId = Convert.ToInt64(MerchantId);
                clientData.Password = Password;
                clientData.TerminalId = Convert.ToInt64(TerminalId);
                clientData.UserId = UserId;

                productData.ProductId = ProductId;
                productData.Price = Convert.ToDouble(Amount);
                productData.Fee = 0;
                productData.Tax = 0;

                pinTransactionData.ProductData = productData;
                pinTransactionData.Language = PinLanguage.English;
                pinTransactionData.Quantity = quantity;
                pinTransactionData.TransactionId = Convert.ToInt64(TransactionId);

                pinTransactionResponse = WsUp.GetPins(clientData, pinTransactionData);

                result.Code = pinTransactionResponse.Result.ErrorCode.ToString();
                result.Message = pinTransactionResponse.Result.ErrorDescription;

                if (pinTransactionResponse.Result.ErrorCode == 0)
                {
                    result.Code = Common.Utils.APPROVED;
                    result.AuthorizationNumber = pinTransactionResponse.ReferenceNumber.ToString();
                    result.PinNumber = pinTransactionResponse.Pins[0].PinNumber;
                    batchNumber = pinTransactionResponse.Pins[0].BatchNumber;
                    result.SerialNumber = pinTransactionResponse.Pins[0].SerialNumber.ToString();
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }

            return result;
        }


        public Transaction Recharge(String ProductId, String Phone, String CountryCode,
            String Amount, String TransactionId, String ETerminalId)
        {
            ClientData clientData = new ClientData();
            TopUpTransactionData topUpTransactionData = new TopUpTransactionData();
            ProductData productData = new ProductData();
            TopUpTransactionResponse topUpTransactionResponse = null;
            Transaction result = new Transaction();
            String details = "", topUpTrackingNumber = "", receiptText = "";
            StringBuilder output = new StringBuilder();
            String CC = "1";

            try
            {

                clientData.HardwareId = HardwareId;
                clientData.IPAddress = Ip;
                clientData.MerchantId = Convert.ToInt64(MerchantId);
                clientData.Password = Password;
                clientData.TerminalId = Convert.ToInt64(TerminalId);
                clientData.UserId = UserId;

                productData.ProductId = ProductId;
                productData.Price = Convert.ToDouble(Amount);
                productData.Fee = 0;
                productData.Tax = 0;

                topUpTransactionData.ProductData = productData;
                topUpTransactionData.Language = TopUpLanguage.English;


                if (Phone.IndexOf(CC).Equals(0))
                {
                    topUpTransactionData.PhoneNumber = Phone.Remove(0, CC.Length);
                }
                else
                {
                    topUpTransactionData.PhoneNumber = Phone;
                }

                topUpTransactionData.CountryCode = CountryCode;
                topUpTransactionData.TransactionId = Convert.ToInt64(TransactionId);

                if (ProductId.Equals("103801") || ProductId.Equals("104102") || ProductId.Equals("104103"))
                {
                    topUpTransactionResponse = WsUp.DoEpayTopUp(clientData, topUpTransactionData, ETerminalId, null, null, null);
                }
                else
                {
                    topUpTransactionResponse = WsUp.DoTopUp(clientData, topUpTransactionData);
                }

                result.Code = topUpTransactionResponse.Result.ErrorCode.ToString();
                result.Message = topUpTransactionResponse.Result.ErrorDescription;

                if (topUpTransactionResponse.Result.ErrorCode == 0)
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;
                    result.AuthorizationNumber = topUpTransactionResponse.TopUpTrackingNumber;
                    result.ProviderAuthorizationCode = topUpTransactionResponse.ProviderAuthorizationCode;
                    result.ProviderReferenceNumber = topUpTransactionResponse.ProviderReferenceNumber;
                    result.ReferenceNumber = topUpTransactionResponse.ReferenceNumber.ToString();
                    result.TopUpTrackingNumber = topUpTransactionResponse.TopUpTrackingNumber;
                    topUpTrackingNumber = topUpTransactionResponse.TopUpTrackingNumber;
                    receiptText = topUpTransactionResponse.ReceiptText;
                    result.TransactionDateTime = DateTime.Now.ToString();

                    
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }

            return result;
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (WsUp != null)
            {
                WsUp.Dispose();
                WsUp = null;
            }
        }

        #endregion
    }
}