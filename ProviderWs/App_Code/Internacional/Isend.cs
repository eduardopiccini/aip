﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;
using System.Text;
using ProviderWs.WsIsend;

namespace ProviderWs.Internacional
{
    public class Isend : IDisposable
    {
        private String AgentId;
        private String AgentPassword;
        private iSendService isend;

        public Isend()
        {
            AgentId = System.Configuration.ConfigurationManager.AppSettings["isend_agent_id"].ToString();
            AgentPassword = System.Configuration.ConfigurationManager.AppSettings["isend_agent_password"].ToString();

            if (isend == null)
            {
                isend = new iSendService();
            }
        }

        public Transaction Recharge(String Phone, String Amount,
            String ProductId, String TransactionId, String url)
        {
            Transaction result = new Transaction();
            String details = null;
            PostSimplePaymentRequest postSimplePaymentRequest;
            ConfirmPaymentRequest confirmPaymentRequest;
            PostPaymentResponse postPaymentResponse = null;
            StringBuilder output = new StringBuilder();

            try
            {
                if (String.IsNullOrEmpty(Phone))
                    throw new Exception(Common.Utils.PHONE_NUMBER_REQUIRE);

                if (String.IsNullOrEmpty(Amount))
                    throw new Exception(Common.Utils.AMOUNT_REQUIRE);

                if (String.IsNullOrEmpty(ProductId))
                    throw new Exception(Common.Utils.PRODUCT_ID_REQUIRE);

                if (String.IsNullOrEmpty(TransactionId))
                    throw new Exception(Common.Utils.TRANSACTION_ID_REQUIRE);

                postSimplePaymentRequest = new PostSimplePaymentRequest();
                confirmPaymentRequest = new ConfirmPaymentRequest();

                isend.Url = url;

                postSimplePaymentRequest.AgentID = confirmPaymentRequest.AgentID = Convert.ToInt32(AgentId);
                postSimplePaymentRequest.AgentPassword = confirmPaymentRequest.AgentPassword = AgentPassword;
                postSimplePaymentRequest.ValidateOnly = false;

                postSimplePaymentRequest.ExternalTransactionID = TransactionId;
                postSimplePaymentRequest.Amount = Convert.ToInt32(Amount.Replace(".", ""));
                postSimplePaymentRequest.BillerID = Convert.ToInt32(ProductId);
                postSimplePaymentRequest.Account = Phone;
                postSimplePaymentRequest.PaymentServiceTypeID = 1;
                postSimplePaymentRequest.EntryTimeStamp = DateTime.Now;

                result.Code = Common.Utils.APPROVED;

                postPaymentResponse = isend.PostSimplePayment(postSimplePaymentRequest);

                if (postPaymentResponse.ConfirmationRequired)
                {
                    confirmPaymentRequest.ExternalTransactionID = postPaymentResponse.ExternalTransactionID;
                    confirmPaymentRequest.TransactionID = postPaymentResponse.TransactionID;

                    postPaymentResponse = isend.ConfirmPayment(confirmPaymentRequest);
                }

                foreach (int i in postPaymentResponse.ErrorCodes)
                {
                    result.Code = i.ToString("D2");
                    details = result.Code;
                }

                if (result.Code.Equals("00") && postPaymentResponse.Status == PaymentStatus.Processed)
                {
                    result.AuthorizationNumber = postPaymentResponse.TransactionID.ToString();
                    result.TransactionDateTime = DateTime.Now.ToString();
                    result.Message = Common.Utils.APPROVED_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = result.Code;
            }
            finally
            {

            }

            return result;
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (isend != null)
            {
                isend.Dispose();
                isend = null;
            }
        }

        #endregion
    }
}