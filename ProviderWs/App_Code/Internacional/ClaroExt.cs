﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;
using System.Data;
using System.Text;

namespace ProviderWs.Internacional
{
    public class ClaroExt : IDisposable
    {
        private String MerchantId;
        private String TerminalId;
        private String Currency;
        private ProviderWs.WsClaroExt.Service Ws;

        public ClaroExt(String url)
        {
            MerchantId = System.Configuration.ConfigurationManager.AppSettings["claro_inter_merchant_id"].ToString();
            TerminalId = System.Configuration.ConfigurationManager.AppSettings["claro_inter_terminal_id"].ToString();
            Currency = System.Configuration.ConfigurationManager.AppSettings["claro_inter_currency"].ToString();

            if (Ws == null)
            {
                Ws = new ProviderWs.WsClaroExt.Service();
                Ws.Url = url;
            }
        }

        public Transaction Recharge(String phone, String amount, String transactionId)
        {
            StringBuilder output = new StringBuilder();
            Transaction result = new Transaction();
            String details = "";
            DataSet dsConsulta, dsRecarga, dsLimite;

            try
            {
                if (String.IsNullOrEmpty(phone))
                {
                    throw new Exception(Common.Utils.PHONE_NUMBER_REQUIRE);
                }

                if (String.IsNullOrEmpty(amount))
                {
                    throw new Exception(Common.Utils.AMOUNT_REQUIRE);
                }

                if (String.IsNullOrEmpty(transactionId))
                {
                    throw new Exception(Common.Utils.TRANSACTION_ID_REQUIRE);
                }

                dsConsulta = Ws.REX_CONSULTA_CLIENTE_PREPAGO(phone, this.Currency);

                if (dsConsulta == null)
                {
                    throw new Exception(Common.Utils.PHONE_QUERY_NO_RESPONSE);
                }

                if (dsConsulta.Tables.Count == 0)
                {
                    throw new Exception(Common.Utils.PHONE_QUERY_NO_RESPONSE);
                }

                if (dsConsulta.Tables[0].Rows.Count == 0)
                {
                    throw new Exception(Common.Utils.PHONE_QUERY_NO_RESPONSE);
                }


                if (!dsConsulta.Tables[0].Rows[0]["Resultado"].ToString().Equals("0"))
                {
                    details = dsConsulta.Tables[0].Rows[0]["Resultado"].ToString();
                    throw new Exception(Common.Utils.PHONE_DOES_NOT_EXISTS);
                }

                dsRecarga = Ws.REX_PROCESA_RECARGA(this.MerchantId,
                    this.TerminalId, phone, dsConsulta.Tables[0].Rows[0]["MSD"].ToString(),
                    Common.Utils.LeftPadding(transactionId, "0", 6),
                    this.Currency, amount, "002",
                    dsConsulta.Tables[0].Rows[0]["Sigla_Estatus"].ToString());


                if (dsRecarga == null)
                {
                    throw new Exception(Common.Utils.RECHARGE_NO_RESPONSE);
                }

                if (dsRecarga.Tables.Count == 0)
                {
                    throw new Exception(Common.Utils.RECHARGE_NO_RESPONSE);
                }

                if (dsRecarga.Tables[0].Rows.Count == 0)
                {
                    throw new Exception(Common.Utils.RECHARGE_NO_RESPONSE);
                }

                result.Code = dsRecarga.Tables[0].Rows[0]["Codigo"].ToString();
                result.Message = dsRecarga.Tables[0].Rows[0]["Descripcion"].ToString();

                if (result.Code.Equals("0"))
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;
                    result.AuthorizationNumber = transactionId;
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                result.Details = ex.Message;
            }

            return result;
        }


        public Transaction Void(String phone, String amount, String transactionId)
        {
            StringBuilder output = new StringBuilder();
            Transaction result = new Transaction();
            String details = "";
            DataSet dsConsulta, dsVoid;

            try
            {
                if (String.IsNullOrEmpty(phone))
                {
                    throw new Exception(Common.Utils.PHONE_NUMBER_REQUIRE);
                }

                if (String.IsNullOrEmpty(amount))
                {
                    throw new Exception(Common.Utils.AMOUNT_REQUIRE);
                }

                if (String.IsNullOrEmpty(transactionId))
                {
                    throw new Exception(Common.Utils.TRANSACTION_ID_REQUIRE);
                }

                dsConsulta = Ws.REX_CONSULTA_CLIENTE_PREPAGO(phone, this.Currency);

                if (dsConsulta == null)
                {
                    throw new Exception(Common.Utils.PHONE_QUERY_NO_RESPONSE);
                }

                if (dsConsulta.Tables.Count == 0)
                {
                    throw new Exception(Common.Utils.PHONE_QUERY_NO_RESPONSE);
                }

                if (dsConsulta.Tables[0].Rows.Count == 0)
                {
                    throw new Exception(Common.Utils.PHONE_QUERY_NO_RESPONSE);
                }


                if (!dsConsulta.Tables[0].Rows[0]["Resultado"].ToString().Equals("0"))
                {
                    details = dsConsulta.Tables[0].Rows[0]["Resultado"].ToString();
                    throw new Exception(Common.Utils.PHONE_DOES_NOT_EXISTS);
                }

                dsVoid = Ws.REX_REVERSO_RECARGA(this.MerchantId,
                    this.TerminalId, phone, dsConsulta.Tables[0].Rows[0]["MSD"].ToString(),
                    Common.Utils.LeftPadding(transactionId, "0", 6),
                    this.Currency, amount, "002");


                if (dsVoid == null)
                {
                    throw new Exception(Common.Utils.VOID_NO_RESPONSE);
                }

                if (dsVoid.Tables.Count == 0)
                {
                    throw new Exception(Common.Utils.VOID_NO_RESPONSE);
                }

                if (dsVoid.Tables[0].Rows.Count == 0)
                {
                    throw new Exception(Common.Utils.VOID_NO_RESPONSE);
                }

                result.Code = dsVoid.Tables[0].Rows[0]["Codigo"].ToString();
                result.Message = dsVoid.Tables[0].Rows[0]["Descripcion"].ToString();

                if (result.Code.Equals("0"))
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                result.Details = ex.Message;
            }

            return result;
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (Ws != null)
            {
                Ws.Dispose();
                Ws = null;
            }
        }

        #endregion
    }
}