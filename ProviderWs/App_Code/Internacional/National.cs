﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;
using System.Text;

namespace ProviderWs.Internacional
{
    public class National : IDisposable
    {
        private String RetailerId;
        private String Username;
        private String Password; 
        private String Version;
        private String Mode; 
        private String TerminalId;
        private ProviderWs.WsNational.NationalUnlimitedWebserviceService wsNational;

        public National()
        {
            wsNational = new ProviderWs.WsNational.NationalUnlimitedWebserviceService();

            Username = System.Configuration.ConfigurationManager.AppSettings["national_username"].ToString();
            Password = System.Configuration.ConfigurationManager.AppSettings["national_password"].ToString();
            Version = System.Configuration.ConfigurationManager.AppSettings["national_version"].ToString();
            RetailerId = System.Configuration.ConfigurationManager.AppSettings["national_retailer_id"].ToString();
            Mode = System.Configuration.ConfigurationManager.AppSettings["national_mode"].ToString();
            TerminalId = System.Configuration.ConfigurationManager.AppSettings["national_terminal_id"].ToString();
        }

        public Transaction GetPin(String ProductId,
            String Denomination)
        {
            Transaction result = new Transaction();
            String details = "", xml = null;
            StringBuilder output = new StringBuilder();
            StringBuilder request = new StringBuilder();
            System.Xml.XmlDocument xdoc = new System.Xml.XmlDocument();
            System.Xml.XmlNode eCode, eMessage1, eMessage2, eTerminalId,
                eAuthNumber, ePinNumber, eSerialNumber;

            try
            {
                if (String.IsNullOrEmpty(ProductId))
                    throw new Exception(Common.Utils.PRODUCT_ID_REQUIRE);

                if (String.IsNullOrEmpty(Denomination))
                    throw new Exception(Common.Utils.DENOMINATION_REQUIRE);

                request.Append("<?xml version ='1.0'?><NATIONALUNLIMITED><UNAME>");
                request.Append(Username);
                request.Append("</UNAME><PASSWORD>");
                request.Append(Password);
                request.Append("</PASSWORD><VERSION>");
                request.Append(Version);
                request.Append("</VERSION><RETAILERID>");
                request.Append(RetailerId);
                request.Append("</RETAILERID><TRANCODE>001</TRANCODE><MODE>");
                request.Append(Mode);
                request.Append("</MODE><DATETIME>");
                request.Append(String.Format("{0:MMddyyHHmmss}", DateTime.Now));
                request.Append("</DATETIME><TERMINALID>");
                request.Append(TerminalId);
                request.Append("</TERMINALID><PRODUCTID>");
                request.Append(ProductId);
                request.Append("</PRODUCTID><DENOM>");
                request.Append(Denomination);
                request.Append("</DENOM><TOPUPAMT>0</TOPUPAMT>");
                request.Append("<CELL>0");
                request.Append("</CELL><APPCODE>0</APPCODE></NATIONALUNLIMITED>");

                result.RequestMsg = request.ToString();

                xml = wsNational.process(request.ToString());

                result.ResponseMsg = xml;

                xdoc.LoadXml(xml);

                eCode = xdoc.SelectSingleNode("/NATIONALUNLIMITED/RESPCODE");
                eMessage1 = xdoc.SelectSingleNode("/NATIONALUNLIMITED/MESSAGE1");
                eMessage2 = xdoc.SelectSingleNode("/NATIONALUNLIMITED/MESSAGE2");
                eTerminalId = xdoc.SelectSingleNode("/NATIONALUNLIMITED/TERMINALID");
                eAuthNumber = xdoc.SelectSingleNode("/NATIONALUNLIMITED/APPCODE");
                ePinNumber = xdoc.SelectSingleNode("/NATIONALUNLIMITED/PINNUMBER");
                eSerialNumber = xdoc.SelectSingleNode("/NATIONALUNLIMITED/SERIALNUMBER");

                result.Code = (eCode != null) ? eCode.InnerText : "PE13";
                result.Message = (eMessage2 != null) ? eMessage2.InnerText : "";
                result.TransactionDateTime = DateTime.Now.ToString();
                result.AuthorizationNumber = (eAuthNumber != null) ? eAuthNumber.InnerText : "";

                if (result.Code.Equals("0000"))
                {
                    result.Code = Common.Utils.APPROVED;
                }


            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = result.Code;
            }

            return result;
        }

        public Transaction VoidRecharge(String ProductId, String Amount,
            String Phone, String AuthorizationNumber)
        {
            Transaction result = new Transaction();
            String details = "", xml = null;
            StringBuilder output = new StringBuilder();
            StringBuilder request = new StringBuilder();
            System.Xml.XmlDocument xdoc = new System.Xml.XmlDocument();
            System.Xml.XmlNode eCode, eMessage1, eMessage2, eTerminalId,
                eAuthNumber;

            try
            {
                if (String.IsNullOrEmpty(Phone))
                    throw new Exception(Common.Utils.PHONE_NUMBER_REQUIRE);

                if (String.IsNullOrEmpty(Amount))
                    throw new Exception(Common.Utils.AMOUNT_REQUIRE);

                if (String.IsNullOrEmpty(ProductId))
                    throw new Exception(Common.Utils.PRODUCT_ID_REQUIRE);

                if (String.IsNullOrEmpty(AuthorizationNumber))
                    throw new Exception(Common.Utils.AUTHORIZATION_NUMBER_REQUIRE);

                request.Append("<?xml version ='1.0'?><NATIONALUNLIMITED><UNAME>");
                request.Append(Username);
                request.Append("</UNAME><PASSWORD>");
                request.Append(Password);
                request.Append("</PASSWORD><VERSION>");
                request.Append(Version);
                request.Append("</VERSION><RETAILERID>");
                request.Append(RetailerId);
                request.Append("</RETAILERID><TRANCODE>004</TRANCODE><MODE>");
                request.Append(Mode);
                request.Append("</MODE><DATETIME>");
                request.Append(String.Format("{0:MMddyyHHmmss}", DateTime.Now));
                request.Append("</DATETIME><TERMINALID>");
                request.Append(TerminalId);
                request.Append("</TERMINALID><PRODUCTID>");
                request.Append(ProductId);
                request.Append("</PRODUCTID><DENOM>000</DENOM><TOPUPAMT>");
                request.Append(Amount);
                request.Append("</TOPUPAMT>");
                request.Append("<CELL>");
                request.Append(Phone);
                request.Append("</CELL><APPCODE>");
                request.Append(AuthorizationNumber);
                request.Append("</APPCODE></NATIONALUNLIMITED>");

                result.RequestMsg = request.ToString();

                xml = wsNational.process(request.ToString());

                result.ResponseMsg = xml;

                xdoc.LoadXml(xml);

                eCode = xdoc.SelectSingleNode("/NATIONALUNLIMITED/RESPCODE");
                eMessage1 = xdoc.SelectSingleNode("/NATIONALUNLIMITED/MESSAGE1");
                eMessage2 = xdoc.SelectSingleNode("/NATIONALUNLIMITED/MESSAGE2");
                eTerminalId = xdoc.SelectSingleNode("/NATIONALUNLIMITED/TERMINALID");
                eAuthNumber = xdoc.SelectSingleNode("/NATIONALUNLIMITED/APPCODE");

                result.Code = (eCode != null) ? eCode.InnerText : Common.Utils.VOID_NO_RESPONSE;
                result.Message = (eMessage2 != null) ? eMessage2.InnerText : "";
                result.TransactionDateTime = DateTime.Now.ToString();
                result.AuthorizationNumber = (eAuthNumber != null) ? eAuthNumber.InnerText : "";

                if (result.Code.Equals("0000"))
                {
                    result.Code = Common.Utils.APPROVED;
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }

            return result;
        }


        public Transaction VoidPin(String ProductId, String Denomination,
            String AuthorizationNumber)
        {
            Transaction result = new Transaction();
            String details = "", xml = null;
            StringBuilder output = new StringBuilder();
            StringBuilder request = new StringBuilder();
            System.Xml.XmlDocument xdoc = new System.Xml.XmlDocument();
            System.Xml.XmlNode eCode, eMessage1, eMessage2, eTerminalId,
                eAuthNumber;

            try
            {
                if (String.IsNullOrEmpty(ProductId))
                    throw new Exception(Common.Utils.PRODUCT_ID_REQUIRE);

                if (String.IsNullOrEmpty(AuthorizationNumber))
                    throw new Exception(Common.Utils.AUTHORIZATION_NUMBER_REQUIRE);

                if (String.IsNullOrEmpty(Denomination))
                    throw new Exception(Common.Utils.DENOMINATION_REQUIRE);

                request.Append("<?xml version ='1.0'?><NATIONALUNLIMITED><UNAME>");
                request.Append(Username);
                request.Append("</UNAME><PASSWORD>");
                request.Append(Password);
                request.Append("</PASSWORD><VERSION>");
                request.Append(Version);
                request.Append("</VERSION><RETAILERID>");
                request.Append(RetailerId);
                request.Append("</RETAILERID><TRANCODE>004</TRANCODE><MODE>");
                request.Append(Mode);
                request.Append("</MODE><DATETIME>");
                request.Append(String.Format("{0:MMddyyHHmmss}", DateTime.Now));
                request.Append("</DATETIME><TERMINALID>");
                request.Append(TerminalId);
                request.Append("</TERMINALID><PRODUCTID>");
                request.Append(ProductId);
                request.Append("</PRODUCTID><DENOM>");
                request.Append(Denomination);
                request.Append("</DENOM><TOPUPAMT>0");
                request.Append("</TOPUPAMT>");
                request.Append("<CELL>0");
                request.Append("</CELL><APPCODE>");
                request.Append(AuthorizationNumber);
                request.Append("</APPCODE></NATIONALUNLIMITED>");

                result.RequestMsg = request.ToString();

                xml = wsNational.process(request.ToString());

                result.ResponseMsg = xml;

                xdoc.LoadXml(xml);

                eCode = xdoc.SelectSingleNode("/NATIONALUNLIMITED/RESPCODE");
                eMessage1 = xdoc.SelectSingleNode("/NATIONALUNLIMITED/MESSAGE1");
                eMessage2 = xdoc.SelectSingleNode("/NATIONALUNLIMITED/MESSAGE2");
                eTerminalId = xdoc.SelectSingleNode("/NATIONALUNLIMITED/TERMINALID");
                eAuthNumber = xdoc.SelectSingleNode("/NATIONALUNLIMITED/APPCODE");

                result.Code = (eCode != null) ? eCode.InnerText : Common.Utils.RECHARGE_NO_RESPONSE;
                result.Message = (eMessage2 != null) ? eMessage2.InnerText : "";
                result.TransactionDateTime = DateTime.Now.ToString();
                result.AuthorizationNumber = (eAuthNumber != null) ? eAuthNumber.InnerText : "";

                if (result.Code.Equals("0000"))
                {
                    result.Code = Common.Utils.APPROVED;
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }

            return result;
        }


        public Transaction Recharge(String ProductId, String Amount,
            String CountryCode, String Phone)
        {
            Transaction result = new Transaction();
            String details = "", xml = null;
            StringBuilder output = new StringBuilder();
            StringBuilder request = new StringBuilder();
            System.Xml.XmlDocument xdoc = new System.Xml.XmlDocument();
            System.Xml.XmlNode eCode, eMessage1, eMessage2, eTerminalId,
                eAuthNumber;

            try
            {
                if (String.IsNullOrEmpty(Phone))
                    throw new Exception(Common.Utils.PHONE_NUMBER_REQUIRE);

                if (String.IsNullOrEmpty(Amount))
                    throw new Exception(Common.Utils.AMOUNT_REQUIRE);

                if (String.IsNullOrEmpty(ProductId))
                    throw new Exception(Common.Utils.PRODUCT_ID_REQUIRE);

                if (String.IsNullOrEmpty(CountryCode))
                    throw new Exception(Common.Utils.COUNTRY_CODE_REQUIRE);

                request.Append("<?xml version ='1.0'?><NATIONALUNLIMITED><UNAME>");
                request.Append(Username);
                request.Append("</UNAME><PASSWORD>");
                request.Append(Password);
                request.Append("</PASSWORD><VERSION>");
                request.Append(Version);
                request.Append("</VERSION><RETAILERID>");
                request.Append(RetailerId);
                request.Append("</RETAILERID><TRANCODE>002</TRANCODE><MODE>");
                request.Append(Mode);
                request.Append("</MODE><DATETIME>");
                request.Append(String.Format("{0:MMddyyHHmmss}", DateTime.Now));
                request.Append("</DATETIME><TERMINALID>");
                request.Append(TerminalId);
                request.Append("</TERMINALID><PRODUCTID>");
                request.Append(ProductId);
                request.Append("</PRODUCTID><DENOM>000</DENOM><TOPUPAMT>");
                request.Append(Amount);
                request.Append("</TOPUPAMT><COUNTRYCODE>");
                request.Append(CountryCode);
                request.Append("</COUNTRYCODE><CELL>");
                request.Append(Phone);
                request.Append("</CELL><APPCODE>0</APPCODE></NATIONALUNLIMITED>");

                result.RequestMsg = request.ToString();

                xml = wsNational.process(request.ToString());

                result.ResponseMsg = xml;

                xdoc.LoadXml(xml);

                eCode = xdoc.SelectSingleNode("/NATIONALUNLIMITED/RESPCODE");
                eMessage1 = xdoc.SelectSingleNode("/NATIONALUNLIMITED/MESSAGE1");
                eMessage2 = xdoc.SelectSingleNode("/NATIONALUNLIMITED/MESSAGE2");
                eTerminalId = xdoc.SelectSingleNode("/NATIONALUNLIMITED/TERMINALID");
                eAuthNumber = xdoc.SelectSingleNode("/NATIONALUNLIMITED/APPCODE");

                result.Code = (eCode != null) ? eCode.InnerText : Common.Utils.RECHARGE_NO_RESPONSE;
                result.Message = (eMessage2 != null) ? eMessage2.InnerText : "";
                result.TransactionDateTime = DateTime.Now.ToString();
                result.AuthorizationNumber = (eAuthNumber != null) ? eAuthNumber.InnerText : "";

                if (result.Code.Equals("0000"))
                {
                    result.Code = Common.Utils.APPROVED;
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }

            return result;
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (wsNational != null)
            {
                wsNational.Dispose();
                wsNational = null;
            }
        }

        #endregion
    }
}