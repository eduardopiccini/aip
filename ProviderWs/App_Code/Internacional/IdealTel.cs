﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;

namespace ProviderWs.App_Code.Internacional
{
    public class IdealTel : IDisposable
    {
        private String UserName { get; set; }
        private String Password { get; set; }
        private String CurrencyCode { get; set; }

        public IdealTel()
        {
            this.UserName = System.Configuration.ConfigurationManager.AppSettings["idealtel_username"].ToString();
            this.Password = System.Configuration.ConfigurationManager.AppSettings["idealtel_password"].ToString();
            this.CurrencyCode = System.Configuration.ConfigurationManager.AppSettings["idealtel_currency_code"].ToString();
        }

        public Transaction Recharge(String transactionId, String phone, String amount, String url)
        {
            Transaction transaction = new Transaction();
            WsIdealTel.idealtelremote ws = new WsIdealTel.idealtelremote();
            String loginResponse = String.Empty, loginKey = null, topupResponse = String.Empty;
            String[] arrLogin = null, arrTopup = null;

            try
            {
                ws.Url = url;

                loginResponse = ws.ClientLogin(this.UserName, this.Password);

                if (!String.IsNullOrEmpty(loginResponse))
                {
                    arrLogin = loginResponse.Split(':');

                    if (arrLogin.Length > 0)
                    {
                        if (arrLogin[0].Equals("1"))
                        {
                            loginKey = arrLogin[1];

                            topupResponse = ws.AddTopUp(loginKey, transactionId, phone, CurrencyCode, amount);

                            arrTopup = topupResponse.Split(':');

                            if (arrTopup.Length > 0)
                            {
                                transaction.Code = arrTopup[0];
                                transaction.Message = arrTopup[1];

                                if (transaction.Code.Equals("301"))
                                {
                                    transaction.Code = Common.Utils.APPROVED;
                                    transaction.Message = Common.Utils.APPROVED_MESSAGE;
                                    transaction.AuthorizationNumber = transactionId;
                                }
                            }
                            else
                            {
                                transaction.Code = Common.Utils.RECHARGE_NO_RESPONSE;
                                transaction.Details = topupResponse;
                            }
                        }
                        else
                        {
                            transaction.Code = arrLogin[0];
                            transaction.Message = arrLogin[1];
                        }
                    }
                    else
                    {
                        transaction.Code = Common.Utils.RECHARGE_NO_RESPONSE;
                        transaction.Details = loginResponse;
                    }
                }
                else
                {
                    transaction.Code = Common.Utils.RECHARGE_NO_RESPONSE;
                    transaction.Details = loginResponse;
                }
            }
            catch (Exception ex)
            {
                transaction.Code = Common.Utils.UNCAUGHT_ERROR;
                transaction.Details = ex.Message;
            }
            finally
            {
                try
                {
                    if (!String.IsNullOrEmpty(loginKey))
                    {
                        ws.ClientLogout(loginKey);
                    }
                }
                catch (Exception)
                {
                }
            }

            return transaction;
        }

        #region IDisposable Members

        public void Dispose()
        {
            
        }

        #endregion
    }
}