﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using Common;
using ProviderWs.WsEasyCall;

namespace ProviderWs.Internacional
{
    public class EasyCall : IDisposable
    {

        private EasyCallSalesService Ws;
        private String Username;
        private String Password;

        public EasyCall()
        {
            Username = System.Configuration.ConfigurationManager.AppSettings["easycall_username"].ToString();
            Password = System.Configuration.ConfigurationManager.AppSettings["easycall_password"].ToString();

            if (Ws == null)
            {
                Ws = new EasyCallSalesService();
            }
        }

        public Transaction ConfirmRecharge(String AuthorizationNumber, String url)
        {
            Transaction result = new Transaction();
            String details = "", xml = "", retailerId = "", aditionalMessage = "";
            StringBuilder output = new StringBuilder();
            System.Xml.XmlDocument xdoc = new System.Xml.XmlDocument();
            System.Xml.XmlNode eCode, eMessage, eAditionalMessage, eRetailer, eTransactionDate,
                eOrderNumber;

            try
            {
                if (String.IsNullOrEmpty(AuthorizationNumber))
                    throw new Exception(Common.Utils.AUTHORIZATION_NUMBER_REQUIRE);

                Ws.Url = url;

                xml = Ws.ackTopUpOrder(this.Username, this.Password, AuthorizationNumber);

                result.ResponseMsg = xml;

                xdoc.LoadXml(xml);

                eCode = xdoc.SelectSingleNode("/EasyCallResponse/Code");
                eMessage = xdoc.SelectSingleNode("/EasyCallResponse/Message");
                eAditionalMessage = xdoc.SelectSingleNode("/EasyCallResponse/AditionalMessage");
                eRetailer = xdoc.SelectSingleNode("/EasyCallResponse/RetailerID");
                eTransactionDate = xdoc.SelectSingleNode("/EasyCallResponse/TransactionDate");
                eOrderNumber = xdoc.SelectSingleNode("/EasyCallResponse/OrderNumber");

                result.Code = (eCode != null) ? eCode.InnerText : Common.Utils.RECHARGE_NO_RESPONSE;
                result.Message = (eMessage != null) ? eMessage.InnerText : "";
                retailerId = (eRetailer != null) ? eRetailer.InnerText : "";
                aditionalMessage = (eAditionalMessage != null) ? eAditionalMessage.InnerText : "";
                result.TransactionDateTime = (eTransactionDate != null) ? eTransactionDate.InnerText : "";
                result.AuthorizationNumber = (eOrderNumber != null) ? eOrderNumber.InnerText : "";

                if (result.Code.Equals("0"))
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = result.Code;
            }

            return result;
        }


        public Transaction ConfirmPin(String AuthorizationNumber, String url)
        {
            Transaction result = new Transaction();
            String details = "", xml = "", retailerId = "", aditionalMessage = "";
            StringBuilder output = new StringBuilder();
            System.Xml.XmlDocument xdoc = new System.Xml.XmlDocument();
            System.Xml.XmlNode eCode, eMessage, eAditionalMessage, eRetailer, eTransactionDate,
                eOrderNumber;

            try
            {
                if (String.IsNullOrEmpty(AuthorizationNumber))
                    throw new Exception(Common.Utils.AUTHORIZATION_NUMBER_REQUIRE);

                Ws.Url = url;

                xml = Ws.ackPINorder(this.Username, this.Password, AuthorizationNumber);

                result.ResponseMsg = xml;

                xdoc.LoadXml(xml);

                eCode = xdoc.SelectSingleNode("/EasyCallResponse/Code");
                eMessage = xdoc.SelectSingleNode("/EasyCallResponse/Message");
                eAditionalMessage = xdoc.SelectSingleNode("/EasyCallResponse/AditionalMessage");
                eRetailer = xdoc.SelectSingleNode("/EasyCallResponse/RetailerID");
                eTransactionDate = xdoc.SelectSingleNode("/EasyCallResponse/TransactionDate");
                eOrderNumber = xdoc.SelectSingleNode("/EasyCallResponse/OrderNumber");

                result.Code = (eCode != null) ? eCode.InnerText : Common.Utils.PIN_NO_RESPONSE;
                result.Message = (eMessage != null) ? eMessage.InnerText : "";
                retailerId = (eRetailer != null) ? eRetailer.InnerText : "";
                aditionalMessage = (eAditionalMessage != null) ? eAditionalMessage.InnerText : "";
                result.TransactionDateTime = (eTransactionDate != null) ? eTransactionDate.InnerText : "";
                result.AuthorizationNumber = (eOrderNumber != null) ? eOrderNumber.InnerText : "";

                if (result.Code.Equals("0"))
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = result.Code;
            }

            return result;
        }


        public Transaction VoidPin(String AuthorizationNumber, String url)
        {
            Transaction result = new Transaction();
            String details = "", xml = "", retailerId = "", aditionalMessage = "";
            StringBuilder output = new StringBuilder();
            System.Xml.XmlDocument xdoc = new System.Xml.XmlDocument();
            System.Xml.XmlNode eCode, eMessage, eAditionalMessage, eRetailer, eTransactionDate,
                eOrderNumber;

            try
            {
                if (String.IsNullOrEmpty(AuthorizationNumber))
                    throw new Exception(Common.Utils.AUTHORIZATION_NUMBER_REQUIRE);

                Ws.Url = url;

                xml = Ws.voidPINorder(this.Username, this.Password, AuthorizationNumber);

                result.ResponseMsg = xml;

                xdoc.LoadXml(xml);

                eCode = xdoc.SelectSingleNode("/EasyCallResponse/Code");
                eMessage = xdoc.SelectSingleNode("/EasyCallResponse/Message");
                eAditionalMessage = xdoc.SelectSingleNode("/EasyCallResponse/AditionalMessage");
                eRetailer = xdoc.SelectSingleNode("/EasyCallResponse/RetailerID");
                eTransactionDate = xdoc.SelectSingleNode("/EasyCallResponse/TransactionDate");
                eOrderNumber = xdoc.SelectSingleNode("/EasyCallResponse/OrderNumber");

                result.Code = (eCode != null) ? eCode.InnerText : Common.Utils.VOID_NO_RESPONSE;
                result.Message = (eMessage != null) ? eMessage.InnerText : "";
                retailerId = (eRetailer != null) ? eRetailer.InnerText : "";
                aditionalMessage = (eAditionalMessage != null) ? eAditionalMessage.InnerText : "";
                result.TransactionDateTime = (eTransactionDate != null) ? eTransactionDate.InnerText : "";
                result.AuthorizationNumber = (eOrderNumber != null) ? eOrderNumber.InnerText : "";

                if (result.Code.Equals("0"))
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = result.Code;
            }

            return result;
        }


        public Transaction GetPin(String ProductId, String TransactionId, String url)
        {
            Transaction result = new Transaction();
            String xml = "", retailerId = "", aditionalMessage = "", details = "", controlNumber = "";
            System.Xml.XmlDocument xdoc = new System.Xml.XmlDocument();
            System.Xml.XmlNode eCode, eMessage, eAditionalMessage, eRetailer,
                eTransactionDate, ePin, eOrderNumber;
            StringBuilder output = new StringBuilder();

            try
            {

                if (String.IsNullOrEmpty(ProductId))
                    throw new Exception(Common.Utils.PRODUCT_ID_REQUIRE);

                if (String.IsNullOrEmpty(TransactionId))
                    throw new Exception(Common.Utils.TRANSACTION_ID_REQUIRE);

                Ws.Url = url;

                xml = Ws.placePINorder(this.Username,
                    this.Password, ProductId, this.Username,
                    this.Username, TransactionId);

                result.ResponseMsg = xml;

                xdoc.LoadXml(xml);

                eCode = xdoc.SelectSingleNode("/EasyCallResponse/Code");
                eMessage = xdoc.SelectSingleNode("/EasyCallResponse/Message");
                eAditionalMessage = xdoc.SelectSingleNode("/EasyCallResponse/AditionalMessage");
                eRetailer = xdoc.SelectSingleNode("/EasyCallResponse/RetailerID");
                eTransactionDate = xdoc.SelectSingleNode("/EasyCallResponse/TransactionDate");
                eOrderNumber = xdoc.SelectSingleNode("/EasyCallResponse/OrderNumber");

                result.Code = (eCode != null) ? eCode.InnerText : Common.Utils.PIN_NO_RESPONSE;
                result.Message = (eMessage != null) ? eMessage.InnerText : "";
                retailerId = (eRetailer != null) ? eRetailer.InnerText : "";
                aditionalMessage = (eAditionalMessage != null) ? eAditionalMessage.InnerText : "";
                result.TransactionDateTime = (eTransactionDate != null) ? eTransactionDate.InnerText : "";
                result.AuthorizationNumber = (eOrderNumber != null) ? eOrderNumber.InnerText : "";

                if (result.Code.Equals("0"))
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;

                    ePin = xdoc.SelectSingleNode("/EasyCallResponse/Pins");

                    if (ePin.ChildNodes.Count > 0)
                    {
                        result.SerialNumber = ePin.ChildNodes[0].ChildNodes[0].InnerText;
                        result.PinNumber = ePin.ChildNodes[0].ChildNodes[1].InnerText;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = result.Code;
            }

            return result;
        }


        public Transaction Recharge(String Phone, String Amount, String ProductId,
            String TransactionId, String url)
        {
            Transaction result = new Transaction();
            TransactionResult resultConfirm = new TransactionResult();
            String details = "", xml = "", retailerId = "", aditionalMessage = "";
            StringBuilder output = new StringBuilder();
            EasyCallSalesService ws = new EasyCallSalesService();
            System.Xml.XmlDocument xdoc = new System.Xml.XmlDocument();
            System.Xml.XmlNode eCode, eMessage, eAditionalMessage, eRetailer, eTransactionDate,
                eOrderNumber;

            try
            {
                if (String.IsNullOrEmpty(Phone))
                    throw new Exception(Common.Utils.PHONE_NUMBER_REQUIRE);

                if (String.IsNullOrEmpty(Amount))
                    throw new Exception(Common.Utils.AMOUNT_REQUIRE);

                if (String.IsNullOrEmpty(ProductId))
                    throw new Exception(Common.Utils.PRODUCT_ID_REQUIRE);

                if (String.IsNullOrEmpty(TransactionId))
                    throw new Exception(Common.Utils.TRANSACTION_ID_REQUIRE);


                ws.Url = url;

                xml = ws.doTopUp(this.Username, this.Password, ProductId, Phone, Amount, this.Username, this.Username, TransactionId);

                result.ResponseMsg = xml;

                xdoc.LoadXml(xml);

                eCode = xdoc.SelectSingleNode("/EasyCallResponse/Code");
                eMessage = xdoc.SelectSingleNode("/EasyCallResponse/Message");
                eAditionalMessage = xdoc.SelectSingleNode("/EasyCallResponse/AditionalMessage");
                eRetailer = xdoc.SelectSingleNode("/EasyCallResponse/RetailerID");
                eTransactionDate = xdoc.SelectSingleNode("/EasyCallResponse/TransactionDate");
                eOrderNumber = xdoc.SelectSingleNode("/EasyCallResponse/OrderNumber");

                result.Code = (eCode != null) ? eCode.InnerText : Common.Utils.RECHARGE_NO_RESPONSE;
                result.Message = (eMessage != null) ? eMessage.InnerText : "";
                retailerId = (eRetailer != null) ? eRetailer.InnerText : "";
                aditionalMessage = (eAditionalMessage != null) ? eAditionalMessage.InnerText : "";
                result.TransactionDateTime = (eTransactionDate != null) ? eTransactionDate.InnerText : "";
                result.AuthorizationNumber = (eOrderNumber != null) ? eOrderNumber.InnerText : "";

                if (result.Code.Equals("0"))
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = result.Code;
            }

            return result;
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (Ws != null)
            {
                Ws.Dispose();
                Ws = null;
            }
        }

        #endregion
    }
}