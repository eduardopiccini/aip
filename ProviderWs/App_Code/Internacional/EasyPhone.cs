﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;
using System.Text;
using System.Net;
using System.IO;

namespace ProviderWs.Internacional
{
    public class EasyPhone : IDisposable
    {
        private String User;
        private String Password; 
        private String Prefix;
        private String Way;

        public EasyPhone()
        {
            User = System.Configuration.ConfigurationManager.AppSettings["easyphone_user"].ToString();
            Password = System.Configuration.ConfigurationManager.AppSettings["easyphone_password"].ToString();
            Prefix = System.Configuration.ConfigurationManager.AppSettings["easyphone_prefix"].ToString();
            Way = System.Configuration.ConfigurationManager.AppSettings["easyphone_way"].ToString();
        }

        public Transaction SendSms(String phone, String message, String routingUrl)
        {
            StringBuilder urlTemp = new StringBuilder();
            Uri uri;
            HttpWebRequest req = null;
            HttpWebResponse resp = null;
            byte[] data = null;
            StreamReader reader = null;
            String msg, details = "", url = null;
            StringBuilder output = new StringBuilder();
            Transaction result = new Transaction();

            try
            {
                if (String.IsNullOrEmpty(phone))
                {
                    throw new Exception(Common.Utils.PHONE_NUMBER_REQUIRE);
                }

                if (String.IsNullOrEmpty(message))
                {
                    throw new Exception(Common.Utils.MESSAGE_REQUIRE);
                }

                urlTemp.Append(routingUrl);
                urlTemp.Append("?user={0}&password={1}&prefix={2}&number={3}&way={4}&message={5}");


                url = String.Format(urlTemp.ToString(), User, Password, Prefix, phone, Way,
                    message);

                result.RequestMsg = url;

                uri = new Uri(url);

                req = (HttpWebRequest)WebRequest.Create(uri);

                req.ContentType = "application/json";
                req.Method = "GET";

                resp = (HttpWebResponse)req.GetResponse();

                reader = new StreamReader(resp.GetResponseStream());

                msg = reader.ReadToEnd();

                result.ResponseMsg = msg;

                msg = msg.Replace('}', ' ').Replace('{', ' ').Trim();

                if (msg.Contains("\"sent:1\"")) //{"result":"sent:1","idsms":82447080}
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;

                    result.AuthorizationNumber = msg.Split(':')[3].ToString();
                }
                else if (msg.Contains(":") && msg.Contains("result")) //{"result":"ERROR Invalid length of phone number"}
                {
                    result.Code = Common.Utils.ERROR_SENDING_SMS;
                    result.Message = msg.Split(':')[1].Replace('"', ' ').Trim();
                }
                else
                {
                    result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                    result.Details = msg;
                }

            }
            catch (Exception ex)
            {
                details = ex.Message;
                result.Code = ex.Message;
            }
            finally
            {

                if (req != null)
                {
                    req = null;
                }

                if (data != null)
                {
                    data = null;
                }

                if (reader != null)
                {
                    reader = null;
                }
            }

            return result;
        }

        #region IDisposable Members

        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        #endregion
    }
}