﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using Common;
using System.Text;
using Newtonsoft.Json;
using System.Collections.Specialized;

namespace ProviderWs.App_Code.Internacional
{
    public class TrumpiaCheckResponse
    {
        public string statuscode { get; set; }
        public string errorcode { get; set; }
        public string errormessage { get; set; }
        public List<List<string>> duplicate { get; set; }
    }
    
    public class Trumpia : IDisposable
    {
        public String ListName { get; set; }
        public String ApiKey { get; set; }
        
        

        public Trumpia(String tipo)
        {
            ListName = System.Configuration.ConfigurationManager.AppSettings["trumpia_listname_" + tipo].ToString();
            ApiKey = System.Configuration.ConfigurationManager.AppSettings["trumpia_apikey_" + tipo].ToString();
            
        }

        private Transaction GetContactId(String url, String phone)
        {
            Transaction result = new Transaction();
            String response = null;
            StringBuilder data = new StringBuilder();
            

            try
            {
                url = url + "getcontactid";

                data.Append("apikey=");
                data.Append(this.ApiKey);
                data.Append("&list_name=");
                data.Append(this.ListName);
                data.Append("&tool_type=2&tool_data=");
                data.Append(phone);

                result.RequestMsg = data.ToString();

                response = HttpSender(url, "POST", data.ToString());

                result.ResponseMsg = response;

                response = @response.Replace("\\", "\"");

                

                Dictionary<string, string> values = JsonConvert.DeserializeObject<Dictionary<string, string>>(response);

                //{"statuscode":"1","message":"Add Contact Success","contactid":"73512402"}

                if (values.ContainsKey("errorcode") && values.ContainsKey("errormessage"))
                {
                    result.Code = values["errorcode"];
                    result.Message = values["errormessage"];
                }
                else if (values.ContainsKey("statuscode") && values.ContainsKey("message") && values.ContainsKey("contactid"))
                {
                    result.Code = values["statuscode"];
                    result.Message = values["message"];
                    result.ContactId = values["contactid"];

                    if (result.Code.Equals("1"))
                    {
                        result.Code = Common.Utils.APPROVED;
                    }
                }
                else
                {
                    result.Code = Common.Utils.RECHARGE_NO_RESPONSE;
                    result.Details = response;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNCAUGHT_ERROR;
                result.Details = response;
            }

            return result;
        }

        private Transaction CheckResponse(String url, String requestId)
        {
            Transaction result = new Transaction();
            String response = null;
            StringBuilder data = new StringBuilder();
            TrumpiaCheckResponse check = new TrumpiaCheckResponse();

            try
            {
                url = url + "checkresponse";

                data.Append("request_id=");
                data.Append(requestId);

                result.RequestMsg = data.ToString();

                response = HttpSender(url, "POST", data.ToString());

                result.ResponseMsg = response;

                response = @response.Replace("\\", "\"");

                check = JsonHelper.JsonDeserialize<TrumpiaCheckResponse>(response);

                result.Code = check.statuscode;
                result.Message = check.errormessage;


                if (result.Code.Equals("0"))
                {
                    result.Code = Common.Utils.APPROVED;

                    if (check.duplicate != null)
                    {
                        if (check.duplicate.Count > 0)
                        {
                            result.ContactId = check.duplicate[0][0].ToString();
                        }
                    }

                }

                //Dictionary<string, string> values = JsonConvert.DeserializeObject<Dictionary<string, string>>(response);

                ////{"statuscode":"1","message":"Add Contact Success","contactid":"73512402"}

                //if (values.ContainsKey("errorcode") && values.ContainsKey("errormessage"))
                //{
                //    result.Code = values["errorcode"];
                //    result.Message = values["errormessage"];
                //}
                //else if (values.ContainsKey("statuscode") && values.ContainsKey("message") && values.ContainsKey("contactid"))
                //{
                //    result.Code = values["statuscode"];
                //    result.Message = values["message"];
                //    result.ContactId = values["contactid"];

                //    if (result.Code.Equals("1"))
                //    {
                //        result.Code = Common.Utils.APPROVED;
                //    }
                //}
                //else
                //{
                //    result.Code = Common.Utils.RECHARGE_NO_RESPONSE;
                //    result.Details = response;
                //}
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNCAUGHT_ERROR;
                result.Details = response;
            }

            return result;
        }

        private Transaction AddContact(String url, String firstName, String lastName, String countryCode, String phone)
        {
            Transaction result = new Transaction();
            String response = null;
            StringBuilder data = new StringBuilder();

            try
            {
                url = url + "addcontact";

                data.Append("apikey=");
                data.Append(this.ApiKey);
                data.Append("&first_name=");
                data.Append(firstName);
                data.Append("&last_name=");
                data.Append(lastName);
                data.Append("&country_code=");
                data.Append(countryCode);
                data.Append("&mobile_number=");
                data.Append(phone);
                data.Append("&list_name=");
                data.Append(this.ListName);
                data.Append("&send_verification=FALSE&allow_access_info=TRUE");

                result.RequestMsg = data.ToString();
                 
                
                response = HttpSender(url, "POST", data.ToString());

                result.ResponseMsg = response;

                response = @response.Replace("\\", "\"");

                Dictionary<string, string> values = JsonConvert.DeserializeObject<Dictionary<string, string>>(response);

                if (values.ContainsKey("requestID") != null)
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;
                    result.AuthorizationNumber = values["requestID"];
                }
                else
                {
                    result.Code = Common.Utils.RECHARGE_NO_RESPONSE;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNCAUGHT_ERROR;
                result.Details = response;
            }

            return result;
        }

        public Transaction SendSms(String url, String firstName, String lastName, String countryCode, String phone,
            String message)
        {
            Transaction result = new Transaction();
            Transaction resultAddContact = new Transaction();
            Transaction resultCheck = new Transaction();
            Transaction resultContact = new Transaction();
            String response = null;
            StringBuilder data = new StringBuilder();

            try
            {
                if (phone.ToArray()[0].Equals('1'))
                {
                    phone = phone.Remove(0, 1);
                }

                resultAddContact = this.AddContact(url, firstName, lastName, countryCode, phone);

                result = resultAddContact;


                if (resultAddContact.Code.Equals(Common.Utils.APPROVED))
                {
                    System.Threading.Thread.Sleep(5000);

                    resultCheck = CheckResponse(url, resultAddContact.AuthorizationNumber);

                    result.Code = resultCheck.Code;
                    result.Message = resultCheck.Message;
                    result.AuthorizationNumber = resultCheck.AuthorizationNumber;
                    result.ContactId = resultCheck.ContactId;

                    result.RequestMsg += "\n";
                    result.RequestMsg += resultCheck.RequestMsg;

                    result.ResponseMsg += "\n";
                    result.ResponseMsg += resultCheck.ResponseMsg;

                    if (result.Code.Equals("10"))
                    {
                        resultContact = GetContactId(url, phone);

                        result.Code = resultContact.Code;
                        result.Message = resultContact.Message;
                        result.AuthorizationNumber = resultContact.AuthorizationNumber;
                        result.ContactId = resultContact.ContactId;

                        result.RequestMsg += "\n";
                        result.RequestMsg += resultContact.RequestMsg;

                        result.ResponseMsg += "\n";
                        result.ResponseMsg += resultContact.ResponseMsg;
                    }

                    if (result.Code.Equals(Common.Utils.APPROVED))
                    {
                        url = url + "sendtocontact";

                        data.Append("apikey=");
                        data.Append(this.ApiKey);
                        data.Append("&email_mode=FALSE&im_mode=FALSE&sms_mode=TRUE&sb_mode=FALSE&description=text+message&sms_message=");
                        data.Append(message);
                        data.Append("&contact_ids=");
                        data.Append(result.ContactId);


                        result.RequestMsg += "\n";
                        result.RequestMsg += data.ToString();

                        response = HttpSender(url, "POST", data.ToString());

                        result.ResponseMsg += "\n";
                        result.ResponseMsg += response;

                        response = @response.Replace("\\", "\"");

                        Dictionary<string, string> values = JsonConvert.DeserializeObject<Dictionary<string, string>>(response);

                        if (values.ContainsKey("requestID"))
                        {
                            result.Code = Common.Utils.APPROVED;
                            result.Message = Common.Utils.APPROVED_MESSAGE;
                            result.AuthorizationNumber = values["requestID"];
                        }
                        else
                        {
                            result.Code = Common.Utils.RECHARGE_NO_RESPONSE;
                            result.Details = response;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNCAUGHT_ERROR;
                result.Details = response + ex.Message;
            }

            return result;
        }

        private String HttpSender(String url, String method, String xml)
        {
            Uri uri;
            String msg = null;
            HttpWebRequest req = null;
            HttpWebResponse resp = null;
            byte[] data = null;
            StreamReader reader = null;


            try
            {
                uri = new Uri(url);

                req = (HttpWebRequest)WebRequest.Create(uri);
                req.ContentType = "application/x-www-form-urlencoded";
                req.Method = method;
                req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                //req.Headers.Add("Accept-Encoding","gzip,deflate,sdch");
                req.Headers.Add("Accept-Language", "es-ES,es;q=0.8");
                req.Headers.Add("Cache-Control", "max-age=0");
                req.Headers.Add("Accept-Encoding", "gzip,deflate,sdch");
                req.UserAgent = "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36";
                req.Headers.Add("Origin", "https://mail-attachment.googleusercontent.com");
                req.KeepAlive = true;
                req.ProtocolVersion = HttpVersion.Version11;
                System.Net.ServicePointManager.Expect100Continue = false;
                req.CookieContainer = new CookieContainer();
                req.AutomaticDecompression = DecompressionMethods.GZip;
                req.AllowAutoRedirect = true;
 
                if (!String.IsNullOrEmpty(xml))
                {
                    data = System.Text.ASCIIEncoding.UTF8.GetBytes(xml);

                    req.ContentLength = data.Length;
                    System.IO.Stream os = req.GetRequestStream();
                    os.Write(data, 0, data.Length);
                    os.Close();
                }

                resp = (HttpWebResponse)req.GetResponse();

                reader = new StreamReader(resp.GetResponseStream());

                msg = reader.ReadToEnd();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (resp != null)
                {
                    resp.Close();
                    resp = null;
                }

                if (reader != null)
                {
                    reader.Close();
                    reader = null;
                }

                //if (req != null)
                //{
                //    req = null;
                //}
            }

            return msg;
        }


        #region IDisposable Members

        public void Dispose()
        {

        }

        #endregion
    }
}