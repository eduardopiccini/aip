﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;
using System.Text;
using System.Net;
using System.IO;

namespace ProviderWs.Internacional
{
    public class Iris : IDisposable
    {
        private String SmsApiKey;
        private String OrigAddress; 
        private String Keyword;

        public Iris()
        {
            SmsApiKey = System.Configuration.ConfigurationManager.AppSettings["iris_sms_api_key"].ToString();
            OrigAddress = System.Configuration.ConfigurationManager.AppSettings["iris_sms_orig_address"].ToString();
            Keyword = System.Configuration.ConfigurationManager.AppSettings["iris_keyword"].ToString();
        }

        public Transaction SendSms(String transactionId, String phone, String message, String routingUrl)
        {
            StringBuilder urlTemp = new StringBuilder();
            Uri uri;
            HttpWebRequest req = null;
            HttpWebResponse resp = null;
            byte[] data = null;
            StreamReader reader = null;
            String msg, details = "", url = null;
            System.Xml.XmlDocument xdoc = new System.Xml.XmlDocument();
            StringBuilder output = new StringBuilder();
            System.Xml.XmlNode eCode, eMessage;
            Transaction result = new Transaction();

            try
            {
                if (String.IsNullOrEmpty(transactionId))
                {
                    throw new Exception(Common.Utils.TRANSACTION_ID_REQUIRE);
                }

                if (String.IsNullOrEmpty(phone))
                {
                    throw new Exception(Common.Utils.PHONE_NUMBER_REQUIRE);
                }

                if (String.IsNullOrEmpty(message))
                {
                    throw new Exception(Common.Utils.MESSAGE_REQUIRE);
                }

                urlTemp.Append(routingUrl);
                urlTemp.Append("?api_key={0}&message[originating_address]={1}");
                urlTemp.Append("&message[body]={2}&message[device_address]={3}");
                urlTemp.Append("&message[keyword]={4}&message[reporting_key]={5}");


                url = String.Format(urlTemp.ToString(), SmsApiKey, OrigAddress, message, phone, Keyword,
                    transactionId);

                result.RequestMsg = url;

                uri = new Uri(url);

                req = (HttpWebRequest)WebRequest.Create(uri);

                req.ContentType = "application/xml";
                req.Method = "POST";

                resp = (HttpWebResponse)req.GetResponse();

                reader = new StreamReader(resp.GetResponseStream());

                msg = reader.ReadToEnd();

                result.ResponseMsg = msg;

                xdoc.LoadXml(msg);

                eCode = xdoc.SelectSingleNode("/status/code");
                eMessage = xdoc.SelectSingleNode("/status/info");


                result.Code = (eCode != null) ? eCode.InnerText : Common.Utils.RECHARGE_NO_RESPONSE;
                result.Message = (eMessage != null) ? eMessage.InnerText : "";

                if (result.Code.Equals(Common.Utils.IRIS_SUCCESS))
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                details = ex.Message;
                result.Code = ex.Message;
            }
            finally
            {

                if (req != null)
                {
                    req = null;
                }

                if (data != null)
                {
                    data = null;
                }

                if (reader != null)
                {
                    reader = null;
                }
            }

            return result;
        }

        #region IDisposable Members

        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        #endregion
    }
}