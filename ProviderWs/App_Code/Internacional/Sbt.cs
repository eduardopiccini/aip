﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using Common;
using System.Text;
using System.Globalization;
using ProviderWs.WsSbt;

namespace ProviderWs.Internacional
{
    public class Sbt : IDisposable
    {
        public String PartnerId { get; set; }
        public String PartnerPassword { get; set; }
        public String TerminalId { get; set; }
        public RechargeServiceService Ws;

        public Sbt()
        {
            ServicePointManager.ServerCertificateValidationCallback =
               delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
               {
                   return true;
               };

            PartnerId = System.Configuration.ConfigurationManager.AppSettings["sbt_partner_id"].ToString();
            PartnerPassword = System.Configuration.ConfigurationManager.AppSettings["sbt_partner_password"].ToString();
            TerminalId = System.Configuration.ConfigurationManager.AppSettings["sbt_terminal_id"].ToString();

            if (Ws == null)
            {
                Ws = new RechargeServiceService();
            }
        }

        public Transaction Recharge(String Amount, String CountryCode,
            String Phone, String TransactionId,
            String OperatorId)
        {
            RechargeMobileResponseRechargeMobileResult sbtResult;

            String partnerId, partnerPassword, terminalId, details = "", responseId = "", balance = "";
            Transaction result = new Transaction();
            StringBuilder output = new StringBuilder();
            String dateFormat = "yyyy/MM/dd HH:mm:ss";
            DateTime Today = DateTime.Now;
            String actualTime = null;

            try
            {
                sbtResult = null;

                if (String.IsNullOrEmpty(Amount))
                    throw new Exception(Common.Utils.AMOUNT_REQUIRE);//"No Amount Specified";

                if (String.IsNullOrEmpty(CountryCode))
                    throw new Exception(Common.Utils.COUNTRY_CODE_REQUIRE);//"No Country Code Specified";

                if (String.IsNullOrEmpty(Phone))
                    throw new Exception(Common.Utils.PHONE_NUMBER_REQUIRE); //"No Phone Specified";

                if (String.IsNullOrEmpty(TransactionId))
                    throw new Exception(Common.Utils.TRANSACTION_ID_REQUIRE); //"No Transaction Id Specified";

                if (String.IsNullOrEmpty(OperatorId))
                    throw new Exception(Common.Utils.OPERATOR_ID_REQUIRE);//"No Operator Specified";

                partnerId = System.Configuration.ConfigurationManager.AppSettings["sbt_partner_id"].ToString();
                partnerPassword = System.Configuration.ConfigurationManager.AppSettings["sbt_partner_password"].ToString();
                terminalId = System.Configuration.ConfigurationManager.AppSettings["sbt_terminal_id"].ToString();

                Today = DateTime.Now.ToUniversalTime();

                actualTime = Today.ToString(dateFormat, CultureInfo.CreateSpecificCulture("ar-Dz"));


                sbtResult = Ws.RechargeMobile(TransactionId,
                    partnerId,
                    partnerPassword,
                    terminalId,
                    actualTime,
                    Convert.ToDouble(Amount),
                    CountryCode,
                    Phone,
                    OperatorId);

                result.Code = sbtResult.ResponseCode.ToString();
                result.Message = sbtResult.ResponseText;

                if (sbtResult.ResponseCode == 0)
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;
                    responseId = sbtResult.ResponseID;
                    balance = sbtResult.BalanceAmount.ToString();
                    result.TransactionDateTime = sbtResult.ResponseDateTime;
                    result.AuthorizationNumber = sbtResult.AuthorizationNumber;
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            

            return result;
        }

        #region IDisposable Members

        public void Dispose()
        {
            
        }

        #endregion
    }
}