﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;
using System.Text;
using System.Net;
using System.IO;

namespace ProviderWs.Internacional
{
    public class TransferTo : IDisposable
    {
        private String User;
        private String Pass;
        private String Msisdn;

        public TransferTo()
        {
            User = System.Configuration.ConfigurationManager.AppSettings["transferto_user"].ToString();
            Pass = System.Configuration.ConfigurationManager.AppSettings["transferto_pass"].ToString();
            Msisdn = System.Configuration.ConfigurationManager.AppSettings["transferto_msisdn"].ToString();
        }

        public Transaction Recharge(String transactionId, String phone, String productId, String operatorId,
            String url)
        {
            StringBuilder request = new StringBuilder();
            Uri uri;
            HttpWebRequest req = null;
            HttpWebResponse resp = null;
            byte[] data = null;
            StreamReader reader = null;
            String msg, details = "", md5 = null;
            System.Xml.XmlDocument xdoc = new System.Xml.XmlDocument();
            Transaction result = new Transaction();
            StringBuilder output = new StringBuilder();
            System.Xml.XmlNode eCode, eMessage, eTransactionId, ePinValue, ePinSerial;

            try
            {
                if (String.IsNullOrEmpty(phone))
                    throw new Exception(Common.Utils.PHONE_NUMBER_REQUIRE);

                if (String.IsNullOrEmpty(productId))
                    throw new Exception(Common.Utils.PRODUCT_ID_REQUIRE);

                if (String.IsNullOrEmpty(transactionId))
                    throw new Exception(Common.Utils.TRANSACTION_ID_REQUIRE);

                if (String.IsNullOrEmpty(operatorId))
                    throw new Exception(Common.Utils.OPERATOR_ID_REQUIRE);

                md5 = String.Format("{0:X}", Common.Utils.MD5Encrypt(User + Pass + transactionId));

                request.Append("<?xml version=\"1.0\"?>");
                request.Append("<xml>");
                request.Append("<login>");
                request.Append(User);
                request.Append("</login>");
                request.Append("<key>");
                request.Append(transactionId);
                request.Append("</key>");
                request.Append("<md5>");
                request.Append(md5);
                request.Append("</md5>");
                request.Append("<msisdn>");
                request.Append(Msisdn);
                request.Append("</msisdn>");
                request.Append("<destination_msisdn>");
                request.Append(phone);
                request.Append("</destination_msisdn>");
                request.Append("<product>");
                request.Append(productId);
                request.Append("</product>");
                request.Append("<operatorid>");
                request.Append(operatorId);
                request.Append("</operatorid>");
                request.Append("<action>topup</action>");
                request.Append("</xml>");

                result.RequestMsg = request.ToString();

                uri = new Uri(url);

                req = (HttpWebRequest)WebRequest.Create(uri);

                req.ContentType = "text/xml";
                req.Method = "POST";

                data = System.Text.ASCIIEncoding.UTF8.GetBytes(request.ToString());

                req.ContentLength = data.Length;
                System.IO.Stream os = req.GetRequestStream();
                os.Write(data, 0, data.Length);
                os.Close();

                resp = (HttpWebResponse)req.GetResponse();

                reader = new StreamReader(resp.GetResponseStream());

                msg = reader.ReadToEnd();

                result.ResponseMsg = msg;

                xdoc.LoadXml(msg);

                eCode = xdoc.SelectSingleNode("/TransferTo/error_code");
                eMessage = xdoc.SelectSingleNode("/TransferTo/error_txt");


                result.Code = (eCode != null) ? eCode.InnerText : Common.Utils.RECHARGE_NO_RESPONSE;
                result.Message = (eMessage != null) ? eMessage.InnerText : "";


                if (result.Code.Equals("0"))
                {
                    eTransactionId = xdoc.SelectSingleNode("/TransferTo/transactionid");
                    ePinSerial = xdoc.SelectSingleNode("/TransferTo/pin_serial");
                    ePinValue = xdoc.SelectSingleNode("/TransferTo/pin_value");

                    result.AuthorizationNumber = (eTransactionId != null) ? eTransactionId.InnerText : "";
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                details = ex.Message;
                result.Code = ex.Message;
            }
            finally
            {
                if (req != null)
                {
                    req = null;
                }

                if (data != null)
                {
                    data = null;
                }

                if (reader != null)
                {
                    reader = null;
                }
            }

            return result;
        }

        #region IDisposable Members

        public void Dispose()
        {
            
        }

        #endregion
    }
}