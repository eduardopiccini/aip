﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;
using System.Text;
using System.Net;
using System.IO;
using System.Xml.Linq;

namespace ProviderWs.Internacional
{
    public class MoreMagic : IDisposable
    {
        private String CurrencyCode;
        private String BuyerPhoneNumber;
        private String BuyerEmail;
        private String ServerName;
        private String ParentMerchantId;
        private String TerminalId;
        private String TerminalPin;

        public MoreMagic()
        {
            CurrencyCode = System.Configuration.ConfigurationManager.AppSettings["mm_currency_code"].ToString();
            BuyerPhoneNumber = System.Configuration.ConfigurationManager.AppSettings["mm_buyer_phone_number"].ToString();
            BuyerEmail = System.Configuration.ConfigurationManager.AppSettings["mm_buyer_email"].ToString();
            ServerName = System.Configuration.ConfigurationManager.AppSettings["mm_server_name"].ToString();
            ParentMerchantId = System.Configuration.ConfigurationManager.AppSettings["mm_parent_merchant_id"].ToString();
            TerminalId = System.Configuration.ConfigurationManager.AppSettings["mm_terminal_id"].ToString();
            TerminalPin = System.Configuration.ConfigurationManager.AppSettings["mm_terminal_pin"].ToString();
        }

        public Transaction Recharge(String Phone, String Amount, String TransactionId)
        {
            StringBuilder request = new StringBuilder();
            Uri uri;
            HttpWebRequest req = null;
            HttpWebResponse resp = null;
            byte[] data = null;
            StreamReader reader = null;
            String msg, response = null, customerId = "", newBalance = "",
                merchantBalance = "", mobileNetworkId = "", details = "";
            XDocument doc = null;
            int response_length = 0;
            Transaction result = new Transaction();
            StringBuilder output = new StringBuilder();

            try
            {
                if (String.IsNullOrEmpty(Phone))
                    throw new Exception(Common.Utils.PHONE_NUMBER_REQUIRE);

                if (String.IsNullOrEmpty(Amount))
                    throw new Exception(Common.Utils.AMOUNT_REQUIRE);

                if (String.IsNullOrEmpty(TransactionId))
                    throw new Exception(Common.Utils.TRANSACTION_ID_REQUIRE);


                request.AppendLine("<?xml version=\"1.0\"?>");
                request.AppendLine("<methodCall>");
                request.AppendLine(" <methodName>moremagic.eposmultiv25</methodName>");
                request.AppendLine(" <params> ");
                request.Append(" <param><value><string>");
                request.Append(ParentMerchantId);
                request.AppendLine("</string></value></param>");
                request.Append(" <param><value><string>");
                request.Append(TerminalId);
                request.AppendLine("</string></value></param>");
                request.Append(" <param><value><string>");
                request.Append(TerminalPin);
                request.AppendLine("</string></value></param>");
                request.Append(" <param><value><string>");
                request.Append(Phone);
                request.Append("</string></value>");
                request.AppendLine("</param>");
                request.Append(" <param><value><string>");
                request.Append(Amount);
                request.Append("</string></value>");
                request.AppendLine("</param>");
                request.Append(" <param><value><string>");
                request.Append(CurrencyCode);
                request.Append("</string></value>");
                request.AppendLine("</param>");
                request.Append(" <param><value><string>");
                request.Append(TransactionId);
                request.Append("</string></value>");
                request.AppendLine("</param>");
                request.Append(" <param><value><string>");
                request.Append(BuyerPhoneNumber);
                request.Append("</string></value>");
                request.AppendLine("</param>");
                request.Append(" <param><value><string>");
                request.Append(BuyerEmail);
                request.Append("</string></value>");
                request.AppendLine("</param>");
                request.AppendLine("</params>");
                request.AppendLine("</methodCall>");

                result.RequestMsg = request.ToString();

                uri = new Uri(ServerName);

                req = (HttpWebRequest)WebRequest.Create(uri);

                req.ContentType = "application/xml";
                req.Method = "POST";

                data = System.Text.ASCIIEncoding.UTF8.GetBytes(request.ToString());

                req.ContentLength = data.Length;
                System.IO.Stream os = req.GetRequestStream();
                os.Write(data, 0, data.Length);
                os.Close();

                resp = (HttpWebResponse)req.GetResponse();

                reader = new StreamReader(resp.GetResponseStream());

                msg = reader.ReadToEnd();

                result.ResponseMsg = msg;

                doc = XDocument.Parse(msg);

                response = doc.Root.Value;

                if (response != null)
                {
                    if (response.Split(':').Length >= 2)
                    {
                        response_length = Convert.ToInt32(response.Split(':')[0]);
                        result.Code = response.Split(':')[1];

                        if (result.Code.Equals("00") && response_length == 7)
                        {
                            customerId = response.Split(':')[2];
                            newBalance = response.Split(':')[3];
                            result.AuthorizationNumber = response.Split(':')[4];
                            result.TransactionDateTime = DateTime.Now.ToString();
                            result.Message = Common.Utils.APPROVED_MESSAGE;
                            merchantBalance = response.Split(':')[5];
                            mobileNetworkId = response.Split(':')[6];
                        }
                    }
                    else
                    {
                        details = response;
                        throw new Exception(Common.Utils.RECHARGE_NO_RESPONSE);
                    }
                }
                else
                {
                    throw new Exception(Common.Utils.RECHARGE_NO_RESPONSE);
                }
            }
            catch (Exception ex)
            {
                details = ex.Message;
                result.Code = ex.Message;
            }
            finally
            {
                if (req != null)
                {
                    req = null;
                }

                if (data != null)
                {
                    data = null;
                }

                if (reader != null)
                {
                    reader = null;
                }
            }

            return result;
        }

        #region IDisposable Members

        public void Dispose()
        {
            
        }

        #endregion
    }
}