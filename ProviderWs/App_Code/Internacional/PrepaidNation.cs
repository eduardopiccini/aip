﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;

namespace ProviderWs.App_Code.Internacional
{
    public class PrepaidNation : IDisposable
    {
        private String User { get; set; }
        private String Password { get; set; }
        private String Version { get; set; }


        public PrepaidNation(String type)
        {
            this.User = System.Configuration.ConfigurationManager.AppSettings[type + "_prepaid_nation_user"].ToString();
            this.Password = System.Configuration.ConfigurationManager.AppSettings[type + "_prepaid_nation_password"].ToString();
            this.Version = System.Configuration.ConfigurationManager.AppSettings[type + "_prepaid_nation_version"].ToString();
        }

        public Transaction PurchasePin(int productId, String transactionId, String storeId, String url)
        {
            Transaction transaction = new Transaction();
            WsPrepaidNation.ServiceManager ws = new WsPrepaidNation.ServiceManager();
            WsPrepaidNation.OrderResponse orderResponse = null;
            WsPrepaidNation.AuthenticationHeader header = new WsPrepaidNation.AuthenticationHeader();

            try
            {
                header.userId = this.User;
                header.password = this.Password;

                ws.AuthenticationHeaderValue = header;

                if (!String.IsNullOrEmpty(url))
                {
                    ws.Url = url;
                }

                orderResponse = ws.PurchasePin(this.Version, productId, 1, transactionId, storeId);

                if (orderResponse.responseCode.Equals("000"))
                {
                    transaction.Code = Common.Utils.APPROVED;
                    transaction.Message = Common.Utils.APPROVED_MESSAGE;

                    transaction.PinNumber = orderResponse.invoice.cards[0].pins[0].pinNumber;
                    transaction.SerialNumber = orderResponse.invoice.cards[0].pins[0].controlNumber;
                }
                else
                {
                    transaction.Code = orderResponse.responseCode;
                    transaction.Message = orderResponse.responseMessage;
                }
               
            }
            catch (Exception ex)
            {
                transaction.Code = Common.Utils.UNCAUGHT_ERROR;
                transaction.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                transaction.Details = ex.Message;
            }

            return transaction;
        }


        public Transaction TopUp(int productId, String transactionId, String storeId, String phone, decimal amount, String url)
        {
            Transaction transaction = new Transaction();
            WsPrepaidNation.ServiceManager ws = new WsPrepaidNation.ServiceManager();
            WsPrepaidNation.OrderResponse orderResponse = null;
            WsPrepaidNation.AuthenticationHeader header = new WsPrepaidNation.AuthenticationHeader();

            try
            {
                header.userId = this.User;
                header.password = this.Password;

                ws.AuthenticationHeaderValue = header;

                if (!String.IsNullOrEmpty(url))
                {
                    ws.Url = url;
                }

                orderResponse = ws.PurchaseRtr2(this.Version, productId, amount, phone, transactionId, null, storeId);

                if (orderResponse.responseCode.Equals("000"))
                {
                    transaction.Code = Common.Utils.APPROVED;
                    transaction.Message = Common.Utils.APPROVED_MESSAGE;

                    transaction.AuthorizationNumber = orderResponse.invoice.invoiceNumber.ToString();
                    transaction.TransactionDateTime = orderResponse.invoice.transactionDateTime.ToShortDateString() +  " " + orderResponse.invoice.transactionDateTime.ToShortTimeString();
                }
                else
                {
                    transaction.Code = orderResponse.responseCode;
                    transaction.Message = orderResponse.responseMessage;
                }

            }
            catch (Exception ex)
            {
                transaction.Code = Common.Utils.UNCAUGHT_ERROR;
                transaction.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                transaction.Details = ex.Message;
            }

            return transaction;
        }


        public Transaction Void(Int64 oldTransactionId, String transactionId, String storeId, String url)
        {
            Transaction transaction = new Transaction();
            WsPrepaidNation.ServiceManager ws = new WsPrepaidNation.ServiceManager();
            WsPrepaidNation.OrderResponse orderResponse = null;
            WsPrepaidNation.AuthenticationHeader header = new WsPrepaidNation.AuthenticationHeader();

            try
            {
                header.userId = this.User;
                header.password = this.Password;

                ws.AuthenticationHeaderValue = header;

                if (!String.IsNullOrEmpty(url))
                {
                    ws.Url = url;
                }

                orderResponse = ws.RtrReturn(this.Version, oldTransactionId, transactionId, storeId);

                if (orderResponse.responseCode.Equals("000"))
                {
                    transaction.Code = Common.Utils.APPROVED;
                    transaction.Message = Common.Utils.APPROVED_MESSAGE;
                }
                else
                {
                    transaction.Code = orderResponse.responseCode;
                    transaction.Message = orderResponse.responseMessage;
                }

            }
            catch (Exception ex)
            {
                transaction.Code = Common.Utils.UNCAUGHT_ERROR;
                transaction.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                transaction.Details = ex.Message;
            }

            return transaction;
        }


        public WsPrepaidNation.SkuListResponse QueryProducts(String url)
        {
            Transaction transaction = new Transaction();
            WsPrepaidNation.ServiceManager ws = new WsPrepaidNation.ServiceManager();
            WsPrepaidNation.SkuListResponse list = null;
            WsPrepaidNation.AuthenticationHeader header = new WsPrepaidNation.AuthenticationHeader();

            try
            {
                header.userId = this.User;
                header.password = this.Password;

                ws.AuthenticationHeaderValue = header;

                if (!String.IsNullOrEmpty(url))
                {
                    ws.Url = url;
                }

                list = ws.GetSkuList(this.Version);

            }
            catch (Exception ex)
            {
                transaction.Code = Common.Utils.UNCAUGHT_ERROR;
                transaction.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                transaction.Details = ex.Message;
            }

            return list;
        }

        #region IDisposable Members

        public void Dispose()
        {
        }

        #endregion
    }
}