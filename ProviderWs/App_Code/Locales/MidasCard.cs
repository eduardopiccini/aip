﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using Common;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace ProviderWs.App_Code.Locales
{
    public class MidasCard : IDisposable
    {
        public String Usuario { get; set; }
        public String Clave { get; set; }

        public MidasCard()
        {
            this.Usuario = System.Configuration.ConfigurationManager.AppSettings["midascard_usuario"].ToString();
            this.Clave = System.Configuration.ConfigurationManager.AppSettings["midascard_clave"].ToString();
        }

        private String HttpSender(String url, String method, String xml)
        {
            Uri uri;
            String msg = null;
            HttpWebRequest req = null;
            HttpWebResponse resp = null;
            byte[] data = null;
            StreamReader reader = null;


            try
            {
                uri = new Uri(url);

                req = (HttpWebRequest)WebRequest.Create(uri);
                req.ContentType = "application/json";
                req.Method = method;

                if (!String.IsNullOrEmpty(xml))
                {
                    data = System.Text.ASCIIEncoding.UTF8.GetBytes(xml);

                    req.ContentLength = data.Length;
                    System.IO.Stream os = req.GetRequestStream();
                    os.Write(data, 0, data.Length);
                    os.Close();
                }

                resp = (HttpWebResponse)req.GetResponse();

                reader = new StreamReader(resp.GetResponseStream());

                msg = reader.ReadToEnd();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (req != null)
                {
                    req = null;
                }

                if (data != null)
                {
                    data = null;
                }

                if (reader != null)
                {
                    reader = null;
                }
            }

            return msg;
        }


        public Transaction Recharge(String productId, String phone, String amount, String transactionId,
            String accountId, String tempUrl)
        {
            Transaction result = new Transaction();
            String response = null;
            StringBuilder jsonRequest = new StringBuilder();
            StringBuilder url = new StringBuilder(); 
            String[] arrResponse = null;

            try
            {
                url.Append(tempUrl);
                url.Append("/plataforma/ws2/");
                url.Append(productId);
                url.Append("?parametros=");
                url.Append(accountId);
                url.Append("-");
                url.Append(transactionId);
                url.Append("/");
                url.Append(this.Usuario);
                url.Append("/");
                url.Append(this.Clave);
                url.Append("/");
                url.Append(phone);
                url.Append("/");
                url.Append(amount);

                result.RequestMsg = url.ToString();

                response = HttpSender(url.ToString(), "GET", null);

                result.ResponseMsg = response;

                arrResponse = response.Split('/');

                if (arrResponse.Length > 1)
                {
                    result.Code = arrResponse[0].ToString();
                    result.Message = arrResponse[1].ToString();

                    if (result.Code.Equals(Common.Utils.APPROVED))
                    {
                        result.TransactionDateTime = response.Split('/')[3].ToString();

                        if (arrResponse.Length > 4)
                        {
                            result.AuthorizationNumber = response.Split('/')[5].ToString();
                        }
                        else
                        {
                            result.AuthorizationNumber = response.Split('/')[2].ToString();
                        }
                        
                    }
                }
                else
                {
                    result.Code = Common.Utils.RECHARGE_NO_RESPONSE;
                    result.Details = response;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNCAUGHT_ERROR;
                result.Details = response;
            }

            return result;
        }



        public Transaction Void(String transactionId, String accountId, String tempUrl)
        {
            Transaction result = new Transaction();
            String response = null;
            StringBuilder jsonRequest = new StringBuilder();
            StringBuilder url = new StringBuilder();

            try
            {
                url.Append(tempUrl);
                url.Append("/plataforma/ws2/Reverso.midas?parametros=");
                url.Append(this.Usuario);
                url.Append("/");
                url.Append(this.Clave);
                url.Append("/");
                url.Append(accountId);
                url.Append("-");
                url.Append(transactionId);

                result.RequestMsg = url.ToString();
                
                response = HttpSender(url.ToString(), "GET", null);

                result.ResponseMsg = response;

                if (response.Split('/').Length > 1)
                {
                    result.Code = response.Split('/')[0].ToString();
                    result.Message = response.Split('/')[1].ToString();

                    if (result.Code.Equals("noExiste"))
                    {
                        result.Message = result.Code;
                        result.Code = Common.Utils.MIDASCARD_NO_EXISTE;
                    }

                    if (result.Code.Equals("Error"))
                    {
                        result.Code = Common.Utils.MIDASCARD_ERROR_REVERSO;
                    }
                }
                else
                {
                    if (response.Contains("noExiste"))
                    {
                        result.Message = response;
                        result.Code = Common.Utils.MIDASCARD_NO_EXISTE;
                    }

                    else if (response.Contains("Error"))
                    {
                        result.Code = Common.Utils.MIDASCARD_ERROR_REVERSO;
                        result.Message = response;
                    }
                    else
                    {
                        result.Code = Common.Utils.RECHARGE_NO_RESPONSE;
                        result.Details = response;       
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNCAUGHT_ERROR;
                result.Details = response;
            }

            return result;
        }

        #region IDisposable Members

        public void Dispose()
        {

        }

        #endregion
    }
}