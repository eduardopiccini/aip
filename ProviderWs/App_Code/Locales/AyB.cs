﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Sockets;
using System.Text;
using Common;

namespace ProviderWs.App_Code.Locales
{
    public class AyB : IDisposable
    {
        public String Usuario { get; set; }
        public String Clave { get; set; }
        public String MaxIntentos { get; set; }
        public String IntentosRequest { get; set; }

        public AyB(String proveedor)
        {
            this.Usuario = System.Configuration.ConfigurationManager.AppSettings["ayb_" + proveedor + "_usuario"].ToString();
            this.Clave = System.Configuration.ConfigurationManager.AppSettings["ayb_" + proveedor +"_clave"].ToString();
            this.MaxIntentos = System.Configuration.ConfigurationManager.AppSettings["ayb_" + proveedor + "_max_intentos"].ToString();
        }


        private String BuildRecharge(String codigoSolicitud, String numTel, String monto, String usuario, String clave, 
            String idPunto, String transactionId)
        {
            StringBuilder msg = new StringBuilder();

            msg.Append("@");
            msg.Append(codigoSolicitud);
            msg.Append("#");
            msg.Append(numTel);
            msg.Append("#");
            msg.Append(monto);
            msg.Append("#");
            msg.Append(usuario);
            msg.Append("#");
            msg.Append(clave);
            msg.Append("#");
            msg.Append(idPunto);
            msg.Append("#");
            msg.Append(transactionId);
            msg.Append("%\n");

            return msg.ToString();
        }


        private String BuildVoid(String usuario, String clave,
            String idPunto, String transactionId)
        {
            StringBuilder msg = new StringBuilder();

            msg.Append("@99#");
            msg.Append(usuario);
            msg.Append("#");
            msg.Append(clave);
            msg.Append("#");
            msg.Append(idPunto);
            msg.Append("#");
            msg.Append(transactionId);
            msg.Append("%\n");

            return msg.ToString();
        }


        public Transaction Recharge(String productCode, String phone, String amount, String transactionId, String idPunto,
            String ip, String port)
        {
            String msg = null, response = null;
            String[] arrResponse = null;
            Transaction result = new Transaction();

            msg = this.BuildRecharge(productCode, phone, amount, this.Usuario, this.Clave, idPunto, transactionId);
            result.RequestMsg = msg;

            response = this.ProcessCmd(msg, ip, Convert.ToInt32(port));
            result.ResponseMsg = response;
            result.MaxIntentos = this.MaxIntentos;
            result.Intentos = this.IntentosRequest;

            if (!String.IsNullOrEmpty(response))
            {
                response = response.Trim();

                if (response.Contains("@"))
                {
                    response = response.Replace('@', ' ').Trim();
                }
                
                arrResponse = response.Split('#');

                if (arrResponse.Length > 0)
                {
                    result.Code = arrResponse[0];
                    result.Message = arrResponse[6];

                    if (result.Code.Equals("01") && arrResponse[1].Equals("ACP"))
                    {
                        result.Code = Common.Utils.APPROVED;
                        result.Message = Common.Utils.APPROVED_MESSAGE;

                        if (arrResponse[2].Length > 7)
                        {
                            result.AuthorizationNumber = arrResponse[2].Substring(0, 8);
                        }
                        else
                        {
                            result.AuthorizationNumber = arrResponse[2];
                        }

                        result.TransactionDateTime = arrResponse[5];
                    }
                }
                else
                {
                    result.Code = Common.Utils.RECHARGE_NO_RESPONSE;
                    result.Details = response;
                }
            }
            else
            {
                result.Code = Common.Utils.RECHARGE_NO_RESPONSE;
                result.Details = response;
            }
            
            return result;
        }


        public Transaction Void(String transactionId, String idPunto,
            String ip, String port)
        {
            String msg = null, response = null;
            String[] arrResponse = null;
            Transaction result = new Transaction();

            msg = this.BuildVoid(this.Usuario, this.Clave, idPunto, transactionId);
            result.RequestMsg = msg;

            response = this.ProcessCmd(msg, ip, Convert.ToInt32(port));
            result.ResponseMsg = response;
            result.MaxIntentos = this.MaxIntentos;
            result.Intentos = this.IntentosRequest;


            if (!String.IsNullOrEmpty(response))
            {
                response = response.Trim();

                if (response.Contains("@"))
                {
                    response = response.Replace('@', ' ').Trim();
                }

                arrResponse = response.Split('#');

                if (arrResponse.Length > 0)
                {
                    result.Code = arrResponse[0];
                    result.Message = arrResponse[6];

                    if (result.Code.Equals("01") && arrResponse[1].Equals("ACP"))
                    {
                        result.Code = Common.Utils.APPROVED;
                        result.Message = Common.Utils.APPROVED_MESSAGE;
                    }
                }
                else
                {
                    result.Code = Common.Utils.RECHARGE_NO_RESPONSE;
                    result.Details = response;
                }
            }
            else
            {
                result.Code = Common.Utils.RECHARGE_NO_RESPONSE;
                result.Details = response;
            }

            return result;
        }


        private String ProcessCmd(String cmd, String ip, int port)
        {
            TcpClient client = null;
            String response = null;
            Socket socket = null;
            byte[] inStream = new byte[1000025];
            byte[] outStream = System.Text.Encoding.ASCII.GetBytes(cmd);
            Int32 bytes = 0;
            int intentos = 0, maxIntentos = 0;

            try
            {
                if (!String.IsNullOrEmpty(this.MaxIntentos))
                {
                    maxIntentos = Convert.ToInt32(this.MaxIntentos);
                }

                client = new TcpClient(ip, port);

                socket = client.Client;
                //socket.ReceiveTimeout = 5000;
                maxIntentos = Convert.ToInt32(this.MaxIntentos);

                socket.Send(outStream, 0, outStream.Length, SocketFlags.None);

                while (bytes == 0)
                {
                   
                    bytes = socket.Receive(inStream);

                    bytes = (bytes > 0) ? bytes - 1 : bytes;

                    intentos++;
                    this.IntentosRequest = intentos.ToString();

                    if (intentos == maxIntentos)
                    {
                        bytes = -1;
                    }
                    else
                    {
                        if (bytes == 0)
                        {
                            System.Threading.Thread.Sleep(1000);
                        }
                    }
                    
                }

                if (bytes > 0)
                {
                    response = System.Text.Encoding.ASCII.GetString(inStream, 0, bytes);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                try
                {
                    if (socket != null)
                    {
                        socket.Close();
                    }
                }
                catch (Exception)
                {

                }

                try
                {
                    if (client != null)
                    {
                        client.Close();
                    }
                }
                catch (Exception)
                {

                }
            }

            return response;
        }

        #region IDisposable Members

        public void Dispose()
        {

        }

        #endregion
    }
}