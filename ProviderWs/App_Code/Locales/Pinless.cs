﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;
using System.Text;
using System.Web.Script.Serialization;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace ProviderWs.App_Code.Locales
{
    public class LoginResult : Result
    {
        public String SessionId { get; set; }
    }

    public class LoginResponse
    {
        //public String session_id { get; set; }
        public String code { get; set; }
        public String error { get; set; }
        public String logged { get; set; }
        public String message { get; set; }
    }


    public class Pinless : IDisposable
    {
        public String User { get; set; }
        public String Pass { get; set; }
        public String Guid { get; set; }

        public Pinless(String tipo)
        {
            this.User = System.Configuration.ConfigurationManager.AppSettings[tipo + "_user"].ToString();
            this.Pass = System.Configuration.ConfigurationManager.AppSettings[tipo + "_password"].ToString();
            this.Guid = System.Configuration.ConfigurationManager.AppSettings[tipo + "_guid"].ToString();
        }

        private String HttpSender(String url, String method, String xml)
        {
            Uri uri;
            String msg = null;
            HttpWebRequest req = null;
            HttpWebResponse resp = null;
            byte[] data = null;
            StreamReader reader = null;


            try
            {
                uri = new Uri(url);

                req = (HttpWebRequest)WebRequest.Create(uri);
                req.ContentType = "application/json";//"Application/x-www-form-urlencoded";
                req.Method = method;
               
                if (!String.IsNullOrEmpty(xml))
                {
                    data = System.Text.ASCIIEncoding.UTF8.GetBytes(xml);

                    req.ContentLength = data.Length;
                    System.IO.Stream os = req.GetRequestStream();
                    os.Write(data, 0, data.Length);
                    os.Close();
                }

                resp = (HttpWebResponse)req.GetResponse();

                reader = new StreamReader(resp.GetResponseStream());

                msg = reader.ReadToEnd();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (req != null)
                {
                    req = null;
                }

                if (data != null)
                {
                    data = null;
                }

                if (reader != null)
                {
                    reader = null;
                }
            }

            return msg;
        }

        public LoginResult Login(String url)
        {
            LoginResult result = new LoginResult();
            String response = null;
            StringBuilder jsonRequest = new StringBuilder();
            
            try
            {
                url = url + "login";

                jsonRequest.Append("{");
                jsonRequest.Append("\"acct_guid\":\"");
                jsonRequest.Append(this.Guid);
                jsonRequest.Append("\",\"acct_user\":\"");
                jsonRequest.Append(this.User);
                jsonRequest.Append("\",\"acct_pass\":\"");
                jsonRequest.Append(this.Pass);
                jsonRequest.Append("\"}");

                response = HttpSender(url, "POST", jsonRequest.ToString());

                response = @response.Replace("\\", "\"");

                Dictionary<string, string> values = JsonConvert.DeserializeObject<Dictionary<string, string>>(response);

                if (values.ContainsKey("code"))
                {
                    result.Code = values["code"].ToString();

                    if (values.ContainsKey("message"))
                    {
                        result.Code = values["message"];
                    }

                    if (result.Code.Equals("200"))
                    {
                        result.Code = Common.Utils.APPROVED;
                        result.Message = Common.Utils.APPROVED_MESSAGE;
                        result.SessionId = values["session_id"].ToString();
                    }
                }
                else
                {
                    result.Code = Common.Utils.RECHARGE_NO_RESPONSE;
                    result.Details = response;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNCAUGHT_ERROR;
                result.Details = response;
            }

            return result;
        }
    
        public Transaction Recharge(String productId, String phone, String amount, String transactionId,
            String accountId, int dollarFree, String url)
        {
            Transaction result = new Transaction();
            String response = null;
            StringBuilder jsonRequest = new StringBuilder();
            LoginResult loginResult = new LoginResult();

            try
            {
                loginResult = Login(url);

                if (loginResult.Code.Equals(Common.Utils.APPROVED))
                {
                    url = url + "recharge";

                    jsonRequest.Append("{");
                    jsonRequest.Append("\"session_id\":\"");
                    jsonRequest.Append(loginResult.SessionId);
                    jsonRequest.Append("\",\"acct_guid\":\"");
                    jsonRequest.Append(this.Guid);
                    jsonRequest.Append("\",\"product_id\":\"");
                    jsonRequest.Append(productId);
                    jsonRequest.Append("\",\"phone\":\"");
                    jsonRequest.Append(phone);
                    jsonRequest.Append("\",\"amount\":");
                    jsonRequest.Append(amount);
                    jsonRequest.Append(",\"create_if_not_exist\":1,");
                    jsonRequest.Append("\"trial_dollar\":");
                    jsonRequest.Append(dollarFree);
                    jsonRequest.Append(",\"reference\":\"");
                    jsonRequest.Append(transactionId);
                    jsonRequest.Append("\",\"customer_id\":\"");
                    jsonRequest.Append(accountId);
                    jsonRequest.Append("\"}");

                    result.RequestMsg = jsonRequest.ToString();

                    response = HttpSender(url, "POST", jsonRequest.ToString());

                    response = @response.Replace("\\", "\"");

                    result.ResponseMsg = response;

                    Dictionary<string, string> values = JsonConvert.DeserializeObject<Dictionary<string, string>>(response);

                    if (values.ContainsKey("code"))
                    {
                        result.Code = values["code"].ToString();

                        if (values.ContainsKey("message"))
                        {
                            result.Message = values["message"];
                        }

                        if (result.Code.Equals("200"))
                        {
                            result.Code = Common.Utils.APPROVED;
                            result.Message = Common.Utils.APPROVED_MESSAGE;
                            result.AuthorizationNumber = values["authCode"].ToString();

                            if (values.ContainsKey("newBalance"))
                            {
                                result.NewBalance = values["newBalance"].ToString();
                            }

                            if (values.ContainsKey("DID"))
                            {
                                result.Did = values["DID"].ToString();
                            }
                        }
                        
                    }
                    else
                    {
                        result.Code = Common.Utils.RECHARGE_NO_RESPONSE;
                        result.Details = response;
                    }
                }
                else
                {
                    result.Code = loginResult.Code;
                    result.Message = loginResult.Message;
                    result.Details = loginResult.Details;
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNCAUGHT_ERROR;
                result.Details = response;
            }

            return result;
        }


        #region IDisposable Members

        public void Dispose()
        {

        }

        #endregion

    
    }
}