﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using Common;
using System.Net.Sockets;
using ProviderWs.App_Code;
using System.Threading;
using System.Collections;
using System.Net;

namespace ProviderWs.Locales
{
    public class Orange : IDisposable
    {

        public SocketTask SocketTask;

        public Orange()
        {
            this.RequestMsg = "";
            this.ResponseMsg = "";

            TerminalId = System.Configuration.ConfigurationManager.AppSettings["orange_terminal_id"].ToString();
            WholeSeller = System.Configuration.ConfigurationManager.AppSettings["orange_wholeseller"].ToString();
            this.MaxIntentos = System.Configuration.ConfigurationManager.AppSettings["orange_max_intentos"].ToString();
            this.SocketTask = new SocketTask();
        }

        public String RequestMsg { get; set; }
        public String ResponseMsg { get; set; }
        private String TerminalId { get;set; }
        private String WholeSeller { get; set; }
        public String MaxIntentos { get; set; }
        public String IntentosRequest { get; set; }
        public String EsEscribible { get; set; }
        public String EsLeible { get; set; }
        public String EsErrado { get; set; }
        public String MensajeTimeout { get; set; }

        private String BuildRefill(String transactionId, String phone, String amount, String terminalId, String wholeSeller,
            String transactionDateTime)
        {
            StringBuilder msg = new StringBuilder();

            //01;01;0000001022;0003;002;20061011182922;8098450001;0010000;001

            msg.Append("01");   //Protocol Version
            msg.Append(";");
            msg.Append("01");   //Message Type
            msg.Append(";");
            msg.Append(Common.Utils.LeftPadding(transactionId, "0", 10));   //Transaction Id
            msg.Append(";");
            msg.Append(Common.Utils.LeftPadding(terminalId, "0", 4));   //Terminal Id
            msg.Append(";");
            msg.Append(Common.Utils.LeftPadding(wholeSeller, "0", 3));  //Whole Seller
            msg.Append(";");
            msg.Append(transactionDateTime);    //Transaction Date yyyyMMddHHmmss
            msg.Append(";");
            msg.Append(phone);              //Phone
            msg.Append(";");
            msg.Append(Common.Utils.LeftPadding(amount, "0", 7));   //Amount
            msg.Append(";");
            msg.Append("001");  //Currency
            msg.Append("\r");

            return msg.ToString();
        }

        private String BuildVoid(String refillTransactionDate, String refillTransactionId, String transactionId, 
            String phone, String terminalId, String wholeSeller, String transactionDateTime)
        {
            StringBuilder msg = new StringBuilder();

            msg.Append("01");   //Protocol Version
            msg.Append(";");
            msg.Append("03");   //Message Type
            msg.Append(";");
            msg.Append(Common.Utils.LeftPadding(transactionId, "0", 10));   //Transaction Id
            msg.Append(";");
            msg.Append(Common.Utils.LeftPadding(refillTransactionId, "0", 10));   //Transaction Id
            msg.Append(";");
            msg.Append(transactionDateTime);    //Transaction Date
            msg.Append(";");
            msg.Append(refillTransactionDate);    //Transaction Date
            msg.Append(";");
            msg.Append(phone);              //Phone
            msg.Append(";");
            msg.Append(Common.Utils.LeftPadding(terminalId, "0", 4));   //Terminal Id
            msg.Append(";");
            msg.Append(Common.Utils.LeftPadding(wholeSeller, "0", 3));  //Whole Seller
            msg.Append("\r");

            return msg.ToString();
        }

        public Transaction Recharge(String transactionId, String phone, 
            String amount, String ip, int port)
        {
            Transaction result = new Transaction();
            String[] arrResponse = null;

            try
            {
                result.TransactionDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");
                this.MensajeTimeout = String.Empty;

                this.RequestMsg = this.BuildRefill(transactionId, phone, amount, this.TerminalId, this.WholeSeller, result.TransactionDateTime);

                result.RequestMsg = this.RequestMsg;

                //this.ResponseMsg  = StartClient(ip, port, this.RequestMsg, 123);

                this.ResponseMsg = this.NewProcess(ip, port, 123); 

                result.ResponseMsg = this.ResponseMsg;

                if (!String.IsNullOrEmpty(this.ResponseMsg))
                {

                    arrResponse = this.ResponseMsg.Split(';');

                    if (arrResponse.Length == 14)
                    {
                        result.Code = arrResponse[6];
                        result.TransactionDateTime = arrResponse[5];
                        result.AuthorizationNumber = Convert.ToInt32(arrResponse[2]) == 0 ? "0" : Convert.ToInt32(arrResponse[2]).ToString();
                        //result.AmountRechargeWithTax = Convert.ToDecimal(arrResponse[8].Substring(0, arrResponse[8].Length - 2) + "." + arrResponse[8].Substring(arrResponse[8].Length-2, 2)).ToString();
                        //result.AmountRechargeWithoutTax = Convert.ToDecimal(arrResponse[9].Substring(0, arrResponse[9].Length - 2) + "." + arrResponse[9].Substring(arrResponse[9].Length - 2, 2)).ToString();
                        //result.BonusWithTax = Convert.ToDecimal(arrResponse[10].Substring(0, arrResponse[10].Length - 2) + "." + arrResponse[10].Substring(arrResponse[10].Length - 2, 2)).ToString();
                        //result.BonusWithoutTax = Convert.ToDecimal(arrResponse[11].Substring(0, arrResponse[11].Length - 2) + "." + arrResponse[11].Substring(arrResponse[11].Length - 2, 2)).ToString();

                        if (result.Code.Equals("001"))
                        {
                            result.Code = "00";
                        }
                    }
                    else
                    {
                        result.Code = Common.Utils.RECHARGE_NO_RESPONSE;
                    }
                }
                else
                {
                    result.Code = Common.Utils.RECHARGE_NO_RESPONSE;
                }

                
            }
            catch (Exception ex)
            {
                if (ex.Message.ToLower().Contains("timeout"))
                {
                    result.Code = Common.Utils.REQUEST_TIMEOUT;
                    result.Details = ex.Message;
                }
                //else if (ex.Message.ToLower().Contains("established connection was aborted by"))
                //{
                //    result.Code = Common.Utils.CONNECTION_WAS_ABORTED;
                //}
                //else if (ex.Message.ToLower().Contains("connection attempt failed because the connected party"))
                //{
                //    result.Code = Common.Utils.CONNECTION_ATTEMPT_FAILED;
                //}
                else
                {
                    result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                    result.Details = ex.Message;
                }
            }
            /*finally
            {
                result.ResponseMsg = result.ResponseMsg;// + ";Es Escribible=" + this.EsEscribible + ",Es Leible=" + this.EsLeible + ",Es Errado=" + this.EsErrado + ", Timeout Message: " + this.MensajeTimeout;
            }*/

            return result;
        }

        public Transaction Void(String refilTransactionDate, String refillTransactionId,
            String transactionId, String phone, String ip, int port)
        {
            Transaction result = new Transaction();
            String[] arrResponse = null;

            try
            {
                result.TransactionDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");
                

                this.RequestMsg = this.BuildVoid(refilTransactionDate, refillTransactionId, transactionId, phone, this.TerminalId, this.WholeSeller, result.TransactionDateTime);

                result.RequestMsg = this.RequestMsg;

                //this.ResponseMsg = StartClient(ip, port, this.RequestMsg, 72);

                this.ResponseMsg = this.NewProcess(ip, port, 72);

                result.ResponseMsg = this.ResponseMsg;

                if (!String.IsNullOrEmpty(this.ResponseMsg))
                {
                    arrResponse = this.ResponseMsg.Split(';');

                    if (arrResponse.Length >= 8)
                    {
                        if (arrResponse.Length > 8)
                        {
                            result.Code = arrResponse[6].Trim();
                        }
                        else if (arrResponse.Length == 8)
                        {
                            result.Code = arrResponse[7].Trim();
                        }
                        else
                        {
                            result.Code = Common.Utils.VOID_NO_RESPONSE;
                        }


                        if (result.Code.Equals("001"))
                        {
                            result.Code = Common.Utils.APPROVED;
                            result.Message = Common.Utils.APPROVED_MESSAGE;
                        }
                    }
                    else
                    {
                        result.Code = Common.Utils.VOID_NO_RESPONSE;
                    }
                }
                else
                {
                    result.Code = Common.Utils.VOID_NO_RESPONSE;
                }

            }
            catch (Exception ex)
            {
                if (ex.Message.ToLower().Contains("timeout"))
                {
                    result.Code = Common.Utils.REQUEST_TIMEOUT;
                }
                //else if (ex.Message.ToLower().Contains("established connection was aborted by"))
                //{
                //    result.Code = Common.Utils.CONNECTION_WAS_ABORTED;
                //}
                //else if (ex.Message.ToLower().Contains("connection attempt failed because the connected party"))
                //{
                //    result.Code = Common.Utils.CONNECTION_ATTEMPT_FAILED;
                //}
                else
                {
                    result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                    result.Details = ex.Message;
                }
            }
            /*finally
            {
                result.ResponseMsg = result.ResponseMsg;// +";Es Escribible=" + this.EsEscribible + ",Es Leible=" + this.EsLeible + ",Es Errado=" + this.EsErrado; 
            }*/

            return result;
        }

        public String NewProcess(String ip, int port, int bytesReceived)
        {
            TcpClient client = null;
            Socket socket = null;
            String response = null;
            byte[] buffer = new byte[bytesReceived];
            int timeout = Convert.ToInt32(this.MaxIntentos);

            try
            {
                timeout = timeout * 1000;
                client = new TcpClient(ip, port);

                socket = client.Client;

                try
                {
                    this.SocketWriteable(socket);
                }
                catch (SocketException se)
                {
                    this.EsEscribible = se.SocketErrorCode.ToString();
                }
                

                Send(socket, Encoding.UTF8.GetBytes(this.RequestMsg), 0, this.RequestMsg.Length, timeout);

                try
                {
                    this.SocketReadable(socket);
                }
                catch (SocketException se)
                {
                    this.EsLeible=se.SocketErrorCode.ToString();
                }


                buffer = Receive(socket, buffer, 0, buffer.Length, timeout);

                response = Encoding.UTF8.GetString(buffer, 0, buffer.Length);
            }
            catch (Exception)
            {
                throw;
            }
            finally 
            {
                try
                {
                    if (client != null)
                    {
                        client.GetStream().Close();
                        client.Close();
                        socket.Close();
                    }
                }
                catch (Exception)
                {
                }
                
            }

            return response;
        }

        private String ProcessCmd(String ip, int port)
        {
            TcpClient client = null;
            String response = null;
            Socket socket = null;
            byte[] inStream = new byte[10025];
            byte[] outStream = System.Text.Encoding.ASCII.GetBytes(this.RequestMsg);
            Int32 bytes = 0, intentos = 0, maxIntentos = 0;


            try
            {
                maxIntentos = Convert.ToInt32(this.MaxIntentos);

                client = new TcpClient(ip, port);

                socket = client.Client;

                socket.Send(outStream, 0, outStream.Length, SocketFlags.None);

                while (bytes == 0)
                {
                    bytes = socket.Receive(inStream);

                    bytes = (bytes > 0) ? bytes - 1 : bytes;

                    intentos++;
                    this.IntentosRequest = intentos.ToString();

                    if (intentos == maxIntentos)
                    {
                        bytes = -1;
                    }
                    else
                    {
                        if (bytes == 0)
                        {
                            System.Threading.Thread.Sleep(1000);
                        }
                    }

                }

                if (bytes > 0)
                {
                    response = System.Text.Encoding.ASCII.GetString(inStream, 0, bytes);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                try
                {
                    if (socket != null)
                    {
                        socket.Close();
                        socket.Dispose();
                    }
                }
                catch (Exception)
                {
                    
                }

                try
                {
                    if (client != null)
                    {
                        client.Close();
                    }
                }
                catch (Exception)
                {
                    
                }
            }

            return response;
        }

        public void Send(Socket socket, byte[] buffer, int offset, int size, int timeout)
        {
            int startTickCount = Environment.TickCount;
            int sent = 0;  // how many bytes is already sent

            try
            {
                do
                {
                    if (Environment.TickCount > startTickCount + timeout)
                        throw new Exception("Timeout Exception, message couldn't be sent in " + this.MaxIntentos + " seconds.");
                    try
                    {
                        sent += socket.Send(buffer, offset + sent, size - sent, SocketFlags.None);
                    }
                    catch (SocketException ex)
                    {
                        if (ex.SocketErrorCode == SocketError.WouldBlock ||
                            ex.SocketErrorCode == SocketError.IOPending ||
                            ex.SocketErrorCode == SocketError.NoBufferSpaceAvailable)
                        {
                            // socket buffer is probably full, wait and try again
                            Thread.Sleep(30);
                        }
                        else
                            throw ex;  // any serious error occurr
                    }
                } while (sent < size);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public byte[] Receive(Socket socket, byte[] buffer, int offset, int size, int timeout)
        {
            int startTickCount = Environment.TickCount;
            int received = 0, sizeTemp = 0;  // how many bytes is already received
            ArrayList list = new ArrayList();

            try
            {
                do
                {
                    if (Environment.TickCount > startTickCount + timeout)
                    {
                        this.MensajeTimeout = "Timeout Exception, no response in " + this.MaxIntentos + " seconds. Bytes received: " + ArraytoString(list);

                        this.SocketWithError(socket);
                        
                        throw new Exception("Timeout Exception, no response in " + this.MaxIntentos + " seconds. Bytes received: " + list.ToString());
                    }
                        
                        
                    try
                    {
                        sizeTemp = socket.Receive(buffer, offset + received, size - received, SocketFlags.None);
                        received += sizeTemp;

                        if (sizeTemp != 0)
                        {
                            list.Add(sizeTemp);
                        }
                    }
                    catch (SocketException ex)
                    {
                        if (ex.SocketErrorCode == SocketError.WouldBlock ||
                            ex.SocketErrorCode == SocketError.IOPending ||
                            ex.SocketErrorCode == SocketError.NoBufferSpaceAvailable)
                        {
                            // socket buffer is probably empty, wait and try again
                            Thread.Sleep(15);
                        }
                        else
                            throw ex;  // any serious error occurr
                    }
                } while (received < size);
            }
            catch (Exception)
            {
                throw;
            }

            

            return buffer;
        }

        private void SocketWriteable(Socket s)
        {
            bool part1 = s.Poll(1000, SelectMode.SelectWrite);

            this.EsEscribible = part1.ToString();
        }

        private String ArraytoString(ArrayList list)
        {
            StringBuilder ret = new StringBuilder();

            foreach (var item in list)
            {
                ret.Append(item);
                ret.Append(",");
            }

            return ret.ToString();
        }

        private void SocketWithError(Socket s)
        {
            try
            {
                try
                {
                    bool part1 = s.Poll(1000, SelectMode.SelectError);

                    this.EsErrado = part1.ToString();

                    if (String.IsNullOrEmpty(this.EsErrado))
                    {
                        this.EsErrado = "Error Vacio!";
                    }
                }
                catch (SocketException se)
                {
                    try
                    {

                        this.EsErrado = "ErrorCode : " + se.ErrorCode;
                        this.EsErrado += "Source : " + se.Source;
                        this.EsErrado += "Stack Trace : " + se.StackTrace;
                        this.EsErrado += "Native Error Code : " + se.NativeErrorCode;
                        this.EsErrado += "Exception Message : " + se.Message;
                        this.EsErrado += "SocketErrorCode : " + se.SocketErrorCode.ToString();
                    }
                    catch (Exception ex)
                    {
                        this.EsErrado = "Rayos! " + ex.Message;
                    }
                }
            }
            catch (Exception ex)
            {
                this.EsErrado = "Exception: " + ex.Message;
            }

            

            
        }

        private void SocketReadable(Socket s)
        {
            //bool part1 = s.Poll(0, SelectMode.SelectRead);
            bool part2 = (s.Available == 0);

            if (part2)
            {//connection is closed
                this.EsLeible = "false " + s.Available;
            }
            else
            {
                this.EsLeible = "true " + s.Available;
            }
        }

        public String StartClient(String ip, int port, String cmd, int total_bytes) 
        {
            // Data buffer for incoming data.
            byte[] bytes = new byte[total_bytes];
            int timeout = Convert.ToInt32(this.MaxIntentos);
            String response = null;

            // Connect to a remote device.
            try 
            {
                // Establish the remote endpoint for the socket.
                // This example uses port 11000 on the local computer.
                IPAddress ipAddress = IPAddress.Parse(ip);
                IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);

                // Create a TCP/IP  socket.
                Socket sender = new Socket(AddressFamily.InterNetwork, 
                    SocketType.Stream, ProtocolType.Tcp );
                timeout = timeout * 1000;

                // Connect the socket to the remote endpoint. Catch any errors.
                try {
                    sender.ReceiveTimeout = timeout;
                    sender.Connect(remoteEP);

                    // Encode the data string into a byte array.
                    byte[] msg = Encoding.ASCII.GetBytes(cmd);

                    // Send the data through the socket.
                    int bytesSent = sender.Send(msg);

                    // Receive the response from the remote device.
                    int bytesRec = sender.Receive(bytes);

                    response = Encoding.ASCII.GetString(bytes, 0, bytesRec);

                    // Release the socket.
                    sender.Shutdown(SocketShutdown.Both);
                    sender.Close();
                
                } catch (ArgumentNullException ane) {
                    throw new Exception("ArgumentNullException: " + ane.Message);
                } catch (SocketException se) {
                    throw new Exception("SocketException: " + se.Message);
                } catch (Exception e) {
                    throw new Exception("Exception: " + e.Message);
                }

            } catch (Exception e) {
                throw new Exception("General Exception: " + e.Message);
            }

            return response;
        }

        #region IDisposable Members

        public void Dispose()
        {
            
        }

        #endregion
    }
}