﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using Common;
using System.Text;
using System.Data;

namespace ProviderWs.Locales
{
    public class ClaroLocal : IDisposable
    {
        private String MerchantId;
        private String TerminalId;
        private String Currency;
        private String Usuario;
        private String Clave;
        private ProviderWs.WsClaroLocal.Service Ws;

        public ClaroLocal(String tipo)
        {
            MerchantId = System.Configuration.ConfigurationManager.AppSettings["claro_local_merchant_id"].ToString();
            TerminalId = System.Configuration.ConfigurationManager.AppSettings["claro_local_terminal_id"].ToString();
            Currency = System.Configuration.ConfigurationManager.AppSettings["claro_local_currency"].ToString();
            Usuario = System.Configuration.ConfigurationManager.AppSettings["claro_" + tipo + "_local_usuario"].ToString();
            Clave = System.Configuration.ConfigurationManager.AppSettings["claro_" + tipo + "_local_clave"].ToString();

            ServicePointManager.ServerCertificateValidationCallback =
              delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
              {
                  return true;
              };

            if (Ws == null)
            {
                Ws = new ProviderWs.WsClaroLocal.Service();

            }
        }

        public Transaction Recharge(String phone, String amount, String transactionId, String url)
        {
            StringBuilder output = new StringBuilder();
            Transaction result = new Transaction();
            DataSet dsConsulta = null, dsRecarga = null;

            try
            {
                

                Ws.Url = url;

                dsConsulta = Ws.CONSULTA_CLIENTE_PREPAGO(Ws.Encriptar(phone), Usuario, Clave);

                if (dsConsulta == null)
                {
                    throw new Exception(Common.Utils.PHONE_QUERY_NO_RESPONSE);
                }

                if (dsConsulta.Tables.Count == 0)
                {
                    throw new Exception(Common.Utils.PHONE_QUERY_NO_RESPONSE);
                }

                if (dsConsulta.Tables[0].Rows.Count == 0)
                {
                    throw new Exception(Common.Utils.PHONE_QUERY_NO_RESPONSE);
                }

                result.Details = dsConsulta.GetXml();

                if (!dsConsulta.Tables[0].Columns.Contains("Resultado"))
                {
                    if (dsConsulta.Tables[0].Rows[0]["Codigo_Respuesta"] != null)
                    {
                        if (!dsConsulta.Tables[0].Rows[0]["Codigo_Respuesta"].ToString().Equals("0"))
                        {
                            result.Code = dsConsulta.Tables[0].Rows[0]["Codigo_Respuesta"].ToString();
                            result.Message = dsConsulta.Tables[0].Rows[0]["Descripcion_Respuesta"].ToString();

                            throw new Exception(Common.Utils.PHONE_DOES_NOT_EXISTS);
                        }
                    }
                    else
                    {
                        throw new Exception(Common.Utils.RECHARGE_NO_RESPONSE);
                    }
                }

                dsRecarga = Ws.PROCESA_RECARGA(Ws.Encriptar(phone),
                    dsConsulta.Tables[0].Rows[0]["MSD"].ToString(),
                    Ws.Encriptar(amount),
                    transactionId,
                    "002",
                    dsConsulta.Tables[0].Rows[0]["Sigla_Estatus"].ToString(),
                    TerminalId,
                    Usuario,
                    Clave);

                if (dsRecarga == null)
                {
                    throw new Exception(Common.Utils.RECHARGE_NO_RESPONSE);
                }

                if (dsRecarga.Tables.Count == 0)
                {
                    throw new Exception(Common.Utils.RECHARGE_NO_RESPONSE);
                }

                if (dsRecarga.Tables[0].Rows.Count == 0)
                {
                    throw new Exception(Common.Utils.RECHARGE_NO_RESPONSE);
                }

                result.Code = dsRecarga.Tables[0].Rows[0]["Codigo_Respuesta"].ToString();
                result.Message = dsRecarga.Tables[0].Rows[0]["Descripcion_Respuesta"].ToString();

                if (result.Code.Equals("0"))
                {
                    result.Code = "00";
                    result.TransactionDateTime = DateTime.Now.ToString();
                    result.AuthorizationNumber = dsRecarga.Tables[0].Rows[0]["No_Autorizacion"].ToString();
                }
            }
            catch (Exception ex)
            {
                if (String.IsNullOrEmpty(result.Code) && String.IsNullOrEmpty(result.Message))
                {
                    result.Code = ex.Message;
                    //result.Details = ex.Message;
                }
            }
            finally
            {
            }

            return result;
        }


        public Transaction VoidRecharge(String phone, String amount,
            String authorizationNumber, String url)
        {
            StringBuilder output = new StringBuilder();
            Transaction result = new Transaction();
            DataSet dsConsulta = null, dsRecarga = null;

            try
            {

                Ws.Url = url;

                dsConsulta = Ws.CONSULTA_CLIENTE_PREPAGO(Ws.Encriptar(phone), Usuario, Clave);

                if (dsConsulta == null)
                {
                    throw new Exception(Common.Utils.PHONE_QUERY_NO_RESPONSE);
                }

                if (dsConsulta.Tables.Count == 0)
                {
                    throw new Exception(Common.Utils.PHONE_QUERY_NO_RESPONSE);
                }

                if (dsConsulta.Tables[0].Rows.Count == 0)
                {
                    throw new Exception(Common.Utils.PHONE_QUERY_NO_RESPONSE);
                }


                if (!dsConsulta.Tables[0].Columns.Contains("Resultado"))
                {
                    if (dsConsulta.Tables[0].Rows[0]["Codigo_Respuesta"] != null)
                    {
                        if (!dsConsulta.Tables[0].Rows[0]["Codigo_Respuesta"].ToString().Equals("0"))
                        {
                            result.Code = dsConsulta.Tables[0].Rows[0]["Codigo_Respuesta"].ToString();
                            result.Message = dsConsulta.Tables[0].Rows[0]["Descripcion_Respuesta"].ToString();

                            throw new Exception(Common.Utils.PHONE_DOES_NOT_EXISTS);
                        }
                    }
                    else
                    {
                        throw new Exception(Common.Utils.RECHARGE_NO_RESPONSE);
                    }
                }

                dsRecarga = Ws.REVERSO_RECARGA(Ws.Encriptar(phone),
                    dsConsulta.Tables[0].Rows[0]["MSD"].ToString(),
                    Ws.Encriptar(amount),
                    authorizationNumber,
                    "002",
                    DateTime.Now.ToString("yyyymmdd"),
                    TerminalId,
                    Usuario,
                    Clave);

                if (dsRecarga == null)
                {
                    throw new Exception(Common.Utils.VOID_NO_RESPONSE);
                }

                if (dsRecarga.Tables.Count == 0)
                {
                    throw new Exception(Common.Utils.VOID_NO_RESPONSE);
                }

                if (dsRecarga.Tables[0].Rows.Count == 0)
                {
                    throw new Exception(Common.Utils.VOID_NO_RESPONSE);
                }

                result.Code = dsRecarga.Tables[0].Rows[0]["Codigo_Respuesta"].ToString();
                result.Message = dsRecarga.Tables[0].Rows[0]["Descripcion_Respuesta"].ToString();

                if (result.Code.Equals("0"))
                {
                    result.Code = Common.Utils.APPROVED;
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                result.Details = ex.Message;
            }
            finally
            {

            }

            return result;
        }


        public Transaction Pin(String denomination, String transactionId, String url)
        {
            StringBuilder output = new StringBuilder();
            Transaction result = new Transaction();
            DataSet dsPin;

            try
            {

                Ws.Url = url;

                dsPin = Ws.PROCESA_COMPRA_PIN(Ws.Encriptar(denomination),
                    transactionId, "002", TerminalId,
                    Usuario, Clave);

                if (dsPin == null)
                {
                    throw new Exception(Common.Utils.PIN_NO_RESPONSE);
                }

                if (dsPin.Tables.Count == 0)
                {
                    throw new Exception(Common.Utils.PIN_NO_RESPONSE);
                }

                if (dsPin.Tables[0].Rows.Count == 0)
                {
                    throw new Exception(Common.Utils.PIN_NO_RESPONSE);
                }

                result.Code = dsPin.Tables[0].Rows[0]["Codigo_Respuesta"].ToString();
                result.Message = dsPin.Tables[0].Rows[0]["Descripcion_Respuesta"].ToString();

                if (result.Code.Equals("0"))
                {
                    result.Code = Common.Utils.APPROVED;
                    result.PinNumber = dsPin.Tables[0].Rows[0]["PIN"].ToString();
                    result.SerialNumber = dsPin.Tables[0].Rows[0]["Serial"].ToString();
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                result.Details = ex.Message;
            }
            finally
            {
            }

            return result;
        }

        public Transaction Query(String transactionId, String url)
        {
            StringBuilder output = new StringBuilder();
            Transaction result = new Transaction();
            DataSet dsQuery = null;

            try
            {


                Ws.Url = url;


                dsQuery = Ws.CONSULTA_PAGO_POR_DEPOSITAR("2", transactionId, Usuario, Clave);

                if (dsQuery == null)
                {
                    throw new Exception(Common.Utils.RECHARGE_NO_RESPONSE);
                }

                if (dsQuery.Tables[0].Rows[0]["No_Autorizacion"] != null)
                {
                    result.Code = "00";
                    result.TransactionDateTime = DateTime.Now.ToString();
                    result.AuthorizationNumber = dsQuery.Tables[0].Rows[0]["No_Autorizacion"].ToString();
                }
                else
                {
                    result.Code = Common.Utils.NO_RECORDS_FOUND;
                }
            }
            catch (Exception ex)
            {
                if (String.IsNullOrEmpty(result.Code) && String.IsNullOrEmpty(result.Message))
                {
                    result.Code = ex.Message;
                    //result.Details = ex.Message;
                }
            }
            finally
            {
            }

            return result;
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (Ws != null)
            {
                Ws.Dispose();
                Ws = null;
            }
        }

        #endregion
    }
}