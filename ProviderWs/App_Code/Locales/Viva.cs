﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using Common;
using System.Net.Sockets;

namespace ProviderWs.Locales
{
    public class Viva : IDisposable
    {

        public Viva()
        {
            

            TerminalId = System.Configuration.ConfigurationManager.AppSettings["viva_terminal_id"].ToString();
            MerchantId = System.Configuration.ConfigurationManager.AppSettings["viva_merchant_id"].ToString();
            MaxIntentos = System.Configuration.ConfigurationManager.AppSettings["viva_max_intentos"].ToString();
        }

        public String Request { get; set; }
        public String Response { get; set; }
        public String RechargeHeader { get; set; }
        private String TerminalId { get; set; }
        private String MerchantId { get; set; }
        private TcpClient client = null;
        private Socket socket = null;
        private String MaxIntentos;


        private String buildHeader()
        {
            StringBuilder header = new StringBuilder();
            StringBuilder myCounter = new StringBuilder();
            String counter = null, ticks = null;

            ticks = DateTime.Now.Ticks.ToString();

            counter = ticks.Substring(ticks.Length - 3, 3);

            header.Append("000500");    //VERSION
            header.Append("000");       //SIN USO
            header.Append(Common.Utils.LeftPadding(this.MerchantId, "0", 8));  //COMERCIO
            header.Append(Common.Utils.LeftPadding(this.TerminalId, "0", 8));  //TERMINAL
            header.Append(DateTime.Now.ToString("ddMMyyyyHHmmss")); //SECUENCIA NUMERIC
            header.Append(Common.Utils.LeftPadding(counter, "0", 3));
            header.Append("VIVA");  //OPERADOR
            header.Append("VIVA0001");  //PRODUCTO

            return header.ToString();
        }

        private String buildRecharge(String amount, String phone, String transactionId)
        {
            StringBuilder recharge = new StringBuilder();

            this.RechargeHeader = this.buildHeader();

            recharge.Append(this.RechargeHeader);

            recharge.Append("RCG"); //CODIGO OPERACION
            recharge.Append(" ");
            recharge.Append(Common.Utils.LeftPadding(amount, "0", 9));    //IMPORTE
            recharge.Append(" ");
            recharge.Append(Common.Utils.LeftPadding(phone, "0", 10));
            recharge.Append(" ");
            recharge.Append(Common.Utils.LeftPadding(transactionId, "0", 20));
            recharge.Append(" ");
            recharge.Append("1");   //TIPO IMPUESTO
            recharge.Append(" ");
            recharge.Append("2");    //MONEDA RD$


            return recharge.ToString();
        }

        private String buildConfirmation()
        {
            StringBuilder confirmation = new StringBuilder();

            confirmation.Append(this.RechargeHeader);

            confirmation.Append("CNF");
            confirmation.Append(" ");

            return confirmation.ToString();
        }

        public Transaction Recharge(String amount, String phone, String transactionId, 
            String ip, int port)
        {
            Transaction response = new Transaction();
            Transaction conResponse = new Transaction();
            String[] arrResponse = null;

            try
            {
                this.Request = String.Empty;
                this.Response = String.Empty;
                this.RechargeHeader = String.Empty;


                Request = this.buildRecharge(amount, phone, transactionId);

                response.RequestMsg = Request + "\n";

                this.client = new TcpClient(ip, port);

                this.socket = client.Client;

                Response = this.ProcessCmd();

                response.ResponseMsg = Response + "\n";

                if (Response != null)
                {
                    arrResponse = Response.Split(' ');

                    if (Response.Contains("NAK"))
                    {
                        response.Code = arrResponse[1].ToString();
                    }
                    else if (Response.Contains("REF"))
                    {
                        response.Code = "00";
                        response.TransactionDateTime = DateTime.Now.ToString();
                        response.AuthorizationNumber = arrResponse[1].ToString();

                        conResponse = ConfirmRecharge(ip, port);

                        response.Code = conResponse.Code;
                        response.ResponseMsg += conResponse.ResponseMsg;
                        response.RequestMsg += conResponse.RequestMsg;
                    }
                    else
                    {
                        response.Code = Common.Utils.RECHARGE_NO_RESPONSE;
                    }
                }
                else
                {
                    response.Code = Common.Utils.RECHARGE_NO_RESPONSE;
                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNKNOWN_EXCEPTION;
                response.Message = ex.Message;
            }
            


            return response;
        }

        public Transaction ConfirmRecharge(String ip, int port)
        {
            Transaction response = new Transaction();
            String[] arrResponse = null;

            try
            {
                this.Request= this.buildConfirmation();

                response.RequestMsg = this.Request + "\n";

                this.Response = this.ProcessCmd();

                response.ResponseMsg = this.Response + "\n";

                if (this.Response != null)
                {
                    arrResponse = this.Response.Split(' ');

                    if (this.Response.Contains("OK"))
                    {
                        response.Code = "00";
                    }
                    else
                    {
                        response.Code = Common.Utils.RECHARGE_NO_RESPONSE;
                    }
                }
                else
                {
                    response.Code = Common.Utils.RECHARGE_NO_RESPONSE;
                }
            }
            catch (Exception ex)
            {
                response.Code = "PE80";
                response.Message = ex.Message;
            }



            return response;
        }

        private String ProcessCmd()
        {
            
            String response = null;
            
            byte[] inStream = new byte[10025];
            byte[] outStream = System.Text.Encoding.ASCII.GetBytes(this.Request);
            Int32 bytes = 0, intentos = 0, maxIntentos = 0;

            try
            {
                maxIntentos = Convert.ToInt32(this.MaxIntentos);

                socket.Send(outStream, 0, outStream.Length, SocketFlags.None);

                while (bytes == 0)
                {

                    bytes = socket.Receive(inStream, SocketFlags.None);

                    bytes = (bytes > 0) ? bytes - 1 : bytes;

                    intentos++;

                    if (intentos == maxIntentos)
                    {
                        bytes = -1;
                    }
                    else
                    {
                        if (bytes == 0)
                        {
                            System.Threading.Thread.Sleep(1000);
                        }
                    }

                }

                if (bytes > 0)
                {
                    response = System.Text.Encoding.ASCII.GetString(inStream, 0, bytes);
                }
            }
            catch (Exception)
            {
                throw;
            }
            

            return response;
        }

        #region IDisposable Members

        public void Dispose()
        {
            try
            {
                if (socket != null)
                {
                    socket.Close();
                }
            }
            catch (Exception)
            {

            }

            try
            {
                if (client != null)
                {
                    client.Close();
                }
            }
            catch (Exception)
            {

            }
        }

        #endregion
    }
}