﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Sockets;
using System.Text;
using Common;

namespace ProviderWs.Locales
{
    public class Tricom : IDisposable
    {
        private String Version { get; set; }
        private String Provider { get; set; }
        private String Entity { get; set; }
        private String Terminal { get; set; }
        private String Merchant { get; set; }
        private String Company { get; set; }
        private String Currency { get; set; }

        public Tricom()
        {
            Version = System.Configuration.ConfigurationManager.AppSettings["tricom_version"].ToString();
            Provider = System.Configuration.ConfigurationManager.AppSettings["tricom_provider"].ToString();
            Entity = System.Configuration.ConfigurationManager.AppSettings["tricom_entity"].ToString();
            Terminal = System.Configuration.ConfigurationManager.AppSettings["tricom_terminal"].ToString();
            Merchant = System.Configuration.ConfigurationManager.AppSettings["tricom_merchant"].ToString();
            Company = System.Configuration.ConfigurationManager.AppSettings["tricom_company"].ToString();
            Currency = System.Configuration.ConfigurationManager.AppSettings["tricom_currency"].ToString();
        }


        private String BuildMessage(String phone, String amount, String type,
            String transactionIdTemp, String referenciaOperadora)
        {
            StringBuilder msg = new StringBuilder();
            String transactionId = null;

            if (transactionIdTemp.Length > 6)
            {
                transactionId = transactionIdTemp.Substring(transactionIdTemp.Length - 6);
            }

            msg.Append(this.Version);
            msg.Append((char)11);
            msg.Append(this.Provider);
            msg.Append(this.Entity);
            msg.Append(DateTime.Now.ToString("yyyyMMddHHmmss"));
            msg.Append(this.Terminal);
            msg.Append(Common.Utils.LeftPadding(transactionId, "0", 7));
            msg.Append((char)11);
            msg.Append(this.Provider);
            msg.Append(Common.Utils.LeftPadding(transactionIdTemp, "0", 16));
            msg.Append((char)11);
            msg.Append(Common.Utils.LeftPadding(referenciaOperadora, " ", 6));
            msg.Append((char)11);
            msg.Append(this.Provider);
            msg.Append((char)11);
            msg.Append(this.Entity);
            msg.Append((char)11);
            msg.Append(this.Merchant);
            msg.Append((char)11);
            msg.Append(this.Terminal);
            msg.Append((char)11);
            msg.Append(Common.Utils.LeftPadding("0", "0", 5));  //cp
            msg.Append((char)11);
            msg.Append(type);
            msg.Append((char)11);
            msg.Append(Common.Utils.LeftPadding(transactionId, "0", 6));
            msg.Append((char)11);
            msg.Append(this.Company);
            msg.Append((char)11);
            msg.Append(Common.Utils.LeftPadding(amount, "0", 6));
            msg.Append((char)11);
            msg.Append(this.Currency);
            msg.Append((char)11);
            msg.Append(Common.Utils.LeftPadding(phone, "0", 11));
            msg.Append((char)11);
            msg.Append(Common.Utils.LeftPadding("0", "0", 6));  //codigo producto
            msg.Append((char)11);
            msg.Append(Common.Utils.LeftPadding(" ", " ", 39));
            msg.Append((char)11);
            msg.Append(Common.Utils.LeftPadding("0", "0", 20));
            msg.Append((char)17);

            return msg.ToString();
        }


        public Transaction Void(String phone, String amount, String reference,
            String transactionId, String ip, int port)
        {
            String msg = null, response = null;
            String[] arrResponse = null;
            Transaction result = new Transaction();

            msg = this.BuildMessage(phone, amount, "AA", transactionId, reference);

            result.RequestMsg = msg;

            response = this.ProcessCmd(msg, ip, port);

            result.ResponseMsg = response;

            response = response.Replace((char)11, '|');
            response = response.Replace((char)17, ';');
            arrResponse = response.Split(';');

            if (arrResponse.Length > 0)
            {
                result.Code = arrResponse[3];
            }
            else
            {
                result.Code = "PE13";
                result.Details = response;
            }

            return result;
        }


        public Transaction Recharge(String phone, String amount, String transactionId,
            String ip, int port)
        {
            String msg = null, response = null;
            String[] arrResponse = null;
            Transaction result = new Transaction();

            msg = this.BuildMessage(phone, amount, "MR", transactionId, " ");
            result.RequestMsg = msg;

            response = this.ProcessCmd(msg, ip, port);
            result.ResponseMsg = response;


            response = response.Replace((char)11, '|');
            response = response.Replace((char)17, ';');
            arrResponse = response.Split(';');

            if (arrResponse.Length > 0)
            {
                result.Code = arrResponse[3];

                if (arrResponse[3].Equals("00"))
                {
                    result.AuthorizationNumber = arrResponse[9];
                }
            }
            else
            {
                result.Code = Common.Utils.RECHARGE_NO_RESPONSE;
                result.Details = response;
            }

            return result;
        }


        public Transaction Consulta(String phone, String amount, String transactionId,
            String ip, int port)
        {
            String msg = null, response = null;
            String[] arrResponse = null;
            Transaction result = new Transaction();

            msg = this.BuildMessage(phone, amount, "CR", transactionId, " ");
            result.RequestMsg = msg;

            response = this.ProcessCmd(msg, ip, port);
            result.ResponseMsg = response;


            response = response.Replace((char)11, '|');
            response = response.Replace((char)17, ';');
            arrResponse = response.Split(';');

            if (arrResponse.Length > 0)
            {
                result.Code = arrResponse[3];

                if (arrResponse[3].Equals("00"))
                {
                    result.AuthorizationNumber = arrResponse[9];
                }
            }
            else
            {
                result.Code = Common.Utils.RECHARGE_NO_RESPONSE;
                result.Details = response;
            }

            return result;
        }


        private String ProcessCmd(String cmd, String ip, int port)
        {
            TcpClient client = null;
            String response = null;
            Socket socket = null;
            byte[] inStream = new byte[10025];
            byte[] outStream = System.Text.Encoding.ASCII.GetBytes(cmd);
            Int32 bytes = 0;

            try
            {
                client = new TcpClient(ip, port);

                socket = client.Client;

                socket.Send(outStream, 0, outStream.Length, SocketFlags.None);

                bytes = socket.Receive(inStream, SocketFlags.None);

                bytes = (bytes > 0) ? bytes - 1 : bytes;

                response = System.Text.Encoding.ASCII.GetString(inStream, 0, bytes);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                try
                {
                    if (socket != null)
                    {
                        socket.Close();
                    }
                }
                catch (Exception)
                {

                }

                try
                {
                    if (client != null)
                    {
                        client.Close();
                    }
                }
                catch (Exception)
                {

                }
            }

            return response;
        }

        #region IDisposable Members

        public void Dispose()
        {
            
        }

        #endregion
    }
}