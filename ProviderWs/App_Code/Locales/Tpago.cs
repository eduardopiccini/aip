﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using Common;
using System.Web.Script.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace ProviderWs.Locales
{
    
    public class Tpago : IDisposable
    {
        private String UserId { get; set; }
        private String AuthKey { get; set; }
        private String Currency { get; set; }
        private String Location { get; set; }

        public Tpago()
        {
            this.UserId = System.Configuration.ConfigurationManager.AppSettings["tpago_user_id"].ToString();
            this.AuthKey = System.Configuration.ConfigurationManager.AppSettings["tpago_auth_key"].ToString();
            this.Currency = System.Configuration.ConfigurationManager.AppSettings["tpago_currency"].ToString();
        }

        private String HttpSender(String url, String method, String xml)
        {
            Uri uri;
            String msg = null;
            HttpWebRequest req = null;
            HttpWebResponse resp = null;
            byte[] data = null;
            StreamReader reader = null;


            try
            {
                uri = new Uri(url);
                this.Location = null;

                req = (HttpWebRequest)WebRequest.Create(uri);
                req.Accept = "application/vnd.tpago.billpayment+json";
                req.ContentType = "application/vnd.tpago.billpayment+json";
                req.Headers.Add("UserId", this.UserId);
                req.Headers.Add("Authentication", this.AuthKey);
                req.Method = method;
                req.AllowAutoRedirect = false;
                req.CookieContainer = new CookieContainer();


                if (!String.IsNullOrEmpty(xml))
                {
                    data = System.Text.ASCIIEncoding.UTF8.GetBytes(xml);

                    req.ContentLength = data.Length;
                    System.IO.Stream os = req.GetRequestStream();
                    os.Write(data, 0, data.Length);
                    os.Close();
                }

                resp = (HttpWebResponse)req.GetResponse();
                
                reader = new StreamReader(resp.GetResponseStream());

                msg = reader.ReadToEnd();

                if (resp.Headers["Location"] != null)
                {
                    this.Location = resp.Headers["Location"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (req != null)
                {
                    req = null;
                }

                if (data != null)
                {
                    data = null;
                }

                if (reader != null)
                {
                    reader = null;
                }
            }

            return msg;
        }


        public GeneralPaymentInformation Payment(String url, String transactionId, 
            String billerId, String contract, String amount)
        {
            GeneralPaymentInformation result = new GeneralPaymentInformation();
            String response = null;
            StringBuilder jsonRequest = new StringBuilder();

            try
            {
                this.Location = null;
                result.TransactionDateTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                url = url + "api/bill-payment/payments";

                jsonRequest.Append("{");
                jsonRequest.Append("\"local-tx-id\" : \"");
                jsonRequest.Append(transactionId);
                jsonRequest.Append("\",\"biller\" : \"");
                jsonRequest.Append(billerId);
                jsonRequest.Append("\",\"contract\" : \"");
                jsonRequest.Append(contract);
                jsonRequest.Append("\",\"amount\" : ");
                jsonRequest.Append(amount);
                jsonRequest.Append(",\"currency\" : \"");
                jsonRequest.Append(this.Currency);
                jsonRequest.Append("\"}");

                result.RequestMsg = url + " " + jsonRequest.ToString();

                response = HttpSender(url, "POST", jsonRequest.ToString());

                result.ResponseMsg = response;

                if (!String.IsNullOrEmpty(response))
                {
                    Dictionary<string, string> values = JsonConvert.DeserializeObject<Dictionary<string, string>>(response);

                    if (values.ContainsKey("error"))
                    {
                        if (values.ContainsKey("code"))
                        {
                            result.Code = values["code"];
                        }

                        if (values.ContainsKey("description"))
                        {
                            result.Message = values["description"];
                        }

                        if (String.IsNullOrEmpty(result.Code))
                        {
                            result.Code = Common.Utils.PAYMENT_INFORMATION_NO_RESPONSE;
                            result.Message = Common.Utils.PAYMENT_INFORMATION_NO_RESPONSE_MESSAGE;
                        }
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(this.Location))
                    {
                        response = HttpSender(this.Location, "GET", null);
                    }
                }

                
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNCAUGHT_ERROR;
            }

            return result;
        }


        //public BillersResult QueryBillers(String url, String userId, String authKey)
        //{
        //    BillersResult result = new BillersResult();
        //    String response = String.Empty;
        //    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
        //    Billers[] bill = null;

        //    try
        //    {
        //        url = url + "api/bill-payment/billers";

        //        response = HttpSender(url, "GET", null, userId, authKey);

        //        bill = json_serializer.Deserialize<Billers[]>(response);

        //        result.Billers = bill.ToList();

        //        if (bill.Length > 0)
        //        {
        //            result.Result.Code = Common.Utils.APPROVED;
        //            result.Result.Message = Common.Utils.APPROVED_MESSAGE;
        //        }
        //        else
        //        {
        //            result.Result.Code = Common.Utils.NO_RECORDS_FOUND;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        result.Result.Code = Common.Utils.UNCAUGHT_ERROR;
        //        result.Result.Details = response;
        //    }


        //    return result;
        //}


        public GeneralBillInformation QueryInvoiceBalance(String biller, String contract, String url)
        {
            String response = String.Empty;
            GeneralBillInformation bill = new GeneralBillInformation();

            try
            {
                bill.TransactionDateTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                url = url + "/api/bill-payment/invoice/" + biller + "/" + contract;
                bill.RequestMsg = url;

                response = HttpSender(url, "GET", null);

                bill.ResponseMsg = response;

                if (!String.IsNullOrEmpty(response))
                {
                    Dictionary<string, string> values = JsonConvert.DeserializeObject<Dictionary<string, string>>(response);

                    if (values.ContainsKey("error"))
                    {
                        if (values.ContainsKey("code"))
                        {
                            bill.Code = values["code"];
                        }

                        if (values.ContainsKey("description"))
                        {
                            bill.Message = values["description"];
                        }

                        if (String.IsNullOrEmpty(bill.Code))
                        {
                            bill.Code = Common.Utils.BILL_INFORMATION_NO_RESPONSE;
                            bill.Message = Common.Utils.BILL_INFORMATION_NO_RESPONSE_MESSAGE;
                        }
                    }
                    else
                    {
                        if (values.ContainsKey("minimum-amount"))
                        {
                            bill.MinimumAmount = values["minimum-amount"];
                        }

                        if (values.ContainsKey("invoice-amount"))
                        {
                            bill.BillAmount = values["invoice-amount"];
                        }

                        if (values.ContainsKey("invoice-date"))
                        {
                            bill.BillDate = values["invoice-date"];

                            DateTime billDate = Convert.ToDateTime(bill.BillDate);

                            bill.BillDate = billDate.ToString("yyyy/MM/dd");
                        }

                        if (!String.IsNullOrEmpty(bill.MinimumAmount) || !String.IsNullOrEmpty(bill.BillAmount))
                        {
                            bill.Code = Common.Utils.APPROVED;
                            bill.Message = Common.Utils.APPROVED_MESSAGE;


                        }
                        else
                        {
                            bill.Code = Common.Utils.BILL_INFORMATION_NO_RESPONSE;
                            bill.Message = Common.Utils.BILL_INFORMATION_NO_RESPONSE_MESSAGE;
                        }

                    }
                }
                else
                {
                    bill.Code = Common.Utils.BILL_INFORMATION_NO_RESPONSE;
                    bill.Message = Common.Utils.BILL_INFORMATION_NO_RESPONSE_MESSAGE;
                }

            }
            catch (Exception ex)
            {
                bill.Code = Common.Utils.UNCAUGHT_ERROR;
            }


            return bill ;
        }

        #region IDisposable Members

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}