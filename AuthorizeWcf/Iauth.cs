﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace AuthorizeWcf
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface Iauth
    {
        [OperationContract]
        SaleResponse Sale(SaleRequest Request);

        [OperationContract]
        SaleResponse SaleWithPaymentProfile(SaleWithPaymentProfileRequest Request);

        [OperationContract]
        CreateProfileResponse CreateProfile(CreateProfileRequest Request);

        [OperationContract]
        CreatePaymentProfileResponse CreatePaymentProfile(CreatePaymentProfileRequest Request);

        [OperationContract]
        Response DeleteProfile(DeleteProfileRequest Request);

        [OperationContract]
        Response DeletePaymentProfile(DeletePaymentProfileRequest Request);

        [OperationContract]
        Response UpdatePaymentProfile(UpdatePaymentProfileRequest Request);
    }

    [DataContract]
    public class DeletePaymentProfileRequest
    {
        [DataMember]
        public long ProfileId { get; set; }

        [DataMember]
        public long PaymentId { get; set; }
    }

    [DataContract]
    public class DeleteProfileRequest
    {
        [DataMember]
        public long ProfileId { get; set; }
    }

    [DataContract]
    public class CreatePaymentProfileRequest
    {
        [DataMember]
        public long ProfileId {get;set;}

        [DataMember]
        public String FullCardNumber { get; set; }

        [DataMember]
        public String ExpMonth { get; set; }

        [DataMember]
        public String ExpYear { get; set; }

        [DataMember]
        public Customer Customer { get; set; }
    }

    [DataContract]
    public class UpdatePaymentProfileRequest
    {
        [DataMember]
        public long ProfileId { get; set; }

        [DataMember]
        public long PaymentProfileId { get; set; }

        [DataMember]
        public String FullCardNumber { get; set; }

        [DataMember]
        public String ExpMonth { get; set; }

        [DataMember]
        public String ExpYear { get; set; }

        [DataMember]
        public Customer Customer { get; set; }
    }

    [DataContract]
    public class CreateProfileRequest
    {
        [DataMember]
        public String Email { get; set; }
    }

    [DataContract]
    public class SaleWithPaymentProfileRequest : Request
    {
        [DataMember]
        public long PaymentProfileId { get; set; }

        [DataMember]
        public long ProfileId { get; set; }

        [DataMember]
        public decimal Amount { get; set; }

        [DataMember]
        public String InvoiceNumber { get; set; }
    }

    [DataContract]
    public class Customer
    {
        [DataMember]
        public String FirstName { get; set; }

        [DataMember]
        public String LastName { get; set; }

        [DataMember]
        public String Address { get; set; }

        [DataMember]
        public String State { get; set; }

        [DataMember]
        public String ZipCode { get; set; }

        [DataMember]
        public String City { get; set; }

        [DataMember]
        public String Country { get; set; }

        [DataMember]
        public String Phone { get; set; }

        [DataMember]
        public String Email { get; set; }
    }

    [DataContract]
    public class CreditCardInfo
    {
        [DataMember]
        public String FullCardNumber { get; set; }

        [DataMember]
        public String ExpirationMonth { get; set; }

        [DataMember]
        public String ExpirationYear { get; set; }
    }

    [DataContract]
    public class SaleRequest : Request
    {
        [DataMember]
        public CreditCardInfo CreditCardInfo { get; set; }

        [DataMember]
        public Decimal Amount { get; set; }

        [DataMember]
        public String InvoiceNumber { get; set; }

        [DataMember]
        public Customer Customer { get; set; }

        [DataMember]
        public String SaleDescription { get; set; }
    }

    [DataContract]
    public class CreateProfileAndPaymentProfileResponse : Response
    {
        [DataMember]
        public long ProfileId { get; set; }

        [DataMember]
        public long PaymentProfileId { get; set; }
    }

    [DataContract]
    public class CreateProfileAndPaymentProfileRequest : Request
    {
        [DataMember]
        public CreditCardInfo CreditCardInfo { get; set; }

        [DataMember]
        public Customer Customer { get; set; }
    }

    [DataContract]
    public class ProfileResponse : Response
    {
        [DataMember]
        public String ProfileId { get; set; }
    }

    [DataContract]
    public class PaymentResponse : Response
    {
        [DataMember]
        public String PaymentId { get; set; }
    }

    [DataContract]
    public class SaleResponse : Response
    {
        [DataMember]
        public Boolean Approved { get; set; }

        [DataMember]
        public String AuthorizationCode { get; set; }

        [DataMember]
        public String InvoiceNumber { get; set; }

        [DataMember]
        public String TransactionId { get; set; }
    }

    [DataContract]
    public class Response
    {
        [DataMember]
        public String Code { get; set; }

        [DataMember]
        public String Description { get; set; }
    }

    [DataContract]
    public class Request
    {
        [DataMember]
        public String SessionId { get; set; }
    }

    public class CreateProfileResponse : Response
    {
        public long ProfileId { get; set; }
    }

    public class CreatePaymentProfileResponse : Response
    {
        public long PaymentProfileId { get; set; }


    }
}
