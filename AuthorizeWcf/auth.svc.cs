﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using log4net;
using AuthorizeNet;
using Common.Exceptions;

namespace AuthorizeWcf
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    public class auth : Iauth
    {
        private static readonly ILog log = LogManager.GetLogger("Log");
        public Boolean IsTestMode;
        private const String AUTHORIZE_APPROVED_CODE = "I00001";
        private String ApiLoginId;
        private String TransactionKey;

        public auth()
        {
            if (System.Configuration.ConfigurationManager.AppSettings["test_mode"].ToString().Equals("true"))
            {
                this.IsTestMode = true;
            }
            else
            {
                this.IsTestMode = false;
            }

            this.ApiLoginId = System.Configuration.ConfigurationManager.AppSettings["api_login_id"].ToString();
            this.TransactionKey = System.Configuration.ConfigurationManager.AppSettings["transaction_key"].ToString();
        }

        public SaleResponse Sale(SaleRequest Request)
        {
            AuthorizationRequest authRequest = null;
            String expMontYear = String.Empty;
            Gateway gateway = null;
            SaleResponse response = new SaleResponse();
            StringBuilder output = new StringBuilder();
            String exception = String.Empty;

            try
            {
                if (Request.Amount == 0)
                    throw new InvalidAmountException();

                if (String.IsNullOrEmpty(Request.CreditCardInfo.FullCardNumber))
                    throw new ValueRequireException("FullCardNumber");

                if (String.IsNullOrEmpty(Request.CreditCardInfo.ExpirationMonth))
                    throw new ValueRequireException("ExpirationMonth");

                if (String.IsNullOrEmpty(Request.CreditCardInfo.ExpirationYear))
                    throw new ValueRequireException("ExpirationYear");

                expMontYear = Request.CreditCardInfo.ExpirationMonth + Request.CreditCardInfo.ExpirationYear.Substring(2, 2);

                authRequest = new AuthorizationRequest(Request.CreditCardInfo.FullCardNumber, expMontYear, Request.Amount, Request.SaleDescription);

                authRequest.AddCustomer(null, Request.Customer.FirstName, Request.Customer.LastName,
                    Request.Customer.Address, Request.Customer.State, Request.Customer.ZipCode);

                authRequest.City = Request.Customer.City;
                authRequest.Country = Request.Customer.Country;
                authRequest.Phone = Request.Customer.Phone;
                authRequest.Email = Request.Customer.Email;
                authRequest.InvoiceNum = Request.InvoiceNumber;

                gateway = new Gateway(this.ApiLoginId, this.TransactionKey, this.IsTestMode);

                var resp = gateway.Send(authRequest);

                response.Approved = resp.Approved;
                response.AuthorizationCode = resp.AuthorizationCode;
                response.InvoiceNumber = resp.InvoiceNumber;
                response.Code = resp.ResponseCode;
                response.Description = resp.Message;
                response.TransactionId = resp.TransactionID;

                if (response.Code.Equals(Common.Utils.AUTHORIZE_APPROVED))
                {
                    response.Code = Common.Utils.APPROVED;
                }
            }
            catch (CustomException ce)
            {
                response.Code = ce.Code;
                response.Description = ce.Description;
                
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Description = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                exception = ex.Message;
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "SALE:", false, true);

                Common.Utils.Concat(ref output, "Approved", response.Approved, false, false);
                Common.Utils.Concat(ref output, "Auth. Code", response.AuthorizationCode, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Description", response.Description, false, false);
                Common.Utils.Concat(ref output, "Invoice Number", response.InvoiceNumber, false, false);
                Common.Utils.Concat(ref output, "Transaction Id", response.TransactionId, false, false);
                Common.Utils.Concat(ref output, "Amount", Request.Amount, false, false);
                Common.Utils.Concat(ref output, "Invoice Number", Request.InvoiceNumber, false, false);
                Common.Utils.Concat(ref output, "Sale Description", Request.SaleDescription, false, true);

                if (Request.CreditCardInfo != null)
                {
                    Common.Utils.Concat(ref output, "Exp. Month", Request.CreditCardInfo.ExpirationMonth, false, false);
                    Common.Utils.Concat(ref output, "Exp. Year", Request.CreditCardInfo.ExpirationYear, false, false);

                    if (!String.IsNullOrEmpty(Request.CreditCardInfo.FullCardNumber) && Request.CreditCardInfo.FullCardNumber.Length > 4)
                    {
                        Common.Utils.Concat(ref output, "Credit Card", Request.CreditCardInfo.FullCardNumber.Substring(Request.CreditCardInfo.FullCardNumber.Length-4, 4), false, true);
                    }
                }

                if (Request.Customer != null)
                {
                    Common.Utils.Concat(ref output, "Address", Request.Customer.Address, false, false);
                    Common.Utils.Concat(ref output, "City", Request.Customer.City, false, false);
                    Common.Utils.Concat(ref output, "Country", Request.Customer.Country, false, false);
                    Common.Utils.Concat(ref output, "Email", Request.Customer.Email, false, false);
                    Common.Utils.Concat(ref output, "First Name", Request.Customer.FirstName, false, false);
                    Common.Utils.Concat(ref output, "Last Name", Request.Customer.LastName, false, false);
                    Common.Utils.Concat(ref output, "Phone", Request.Customer.Phone, false, false);
                    Common.Utils.Concat(ref output, "State", Request.Customer.State, false, false);
                    Common.Utils.Concat(ref output, "Zip Code", Request.Customer.ZipCode, false, true);
                }

                Common.Utils.Concat(ref output, "Exception", exception, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        public SaleResponse SaleWithPaymentProfile(SaleWithPaymentProfileRequest Request)
        {
            AuthWs.MerchantAuthenticationType merchant = new AuthWs.MerchantAuthenticationType();
            AuthWs.ProfileTransAuthCaptureType auth_capture = new AuthWs.ProfileTransAuthCaptureType();
            AuthWs.ProfileTransactionType trans = new AuthWs.ProfileTransactionType();
            AuthWs.CreateCustomerProfileTransactionResponseType response = null;
            SaleResponse saleResponse = new SaleResponse();
            StringBuilder output = new StringBuilder();
            String exception = String.Empty;

            try
            {
                if (Request.Amount == 0)
                    throw new InvalidAmountException();

                if (Request.PaymentProfileId == 0)
                    throw new ValueRequireException("PaymentProfileId");

                if (Request.ProfileId == 0)
                    throw new ValueRequireException("ProfileId");

                merchant.name = this.ApiLoginId;
                merchant.transactionKey = this.TransactionKey;
                
                auth_capture.customerProfileId = Request.ProfileId;
                auth_capture.customerPaymentProfileId = Request.PaymentProfileId;
                auth_capture.amount = Request.Amount;
                auth_capture.order = new AuthWs.OrderExType();
                auth_capture.order.invoiceNumber = Request.InvoiceNumber;

                trans.Item = auth_capture;

                using (AuthWs.Service ws = new AuthWs.Service())
                {
                    response = ws.CreateCustomerProfileTransaction(merchant, trans, null);

                    saleResponse.Code = response.messages[0].code;
                    saleResponse.Description = response.messages[0].text;

                    if (saleResponse.Code.Equals(AUTHORIZE_APPROVED_CODE))
                    {
                        saleResponse.Approved = true;
                        saleResponse.Code = Common.Utils.APPROVED;
                        saleResponse.AuthorizationCode = response.directResponse.Split(',')[4];
                        saleResponse.TransactionId = response.directResponse.Split(',')[6];
                        saleResponse.InvoiceNumber = Request.InvoiceNumber;
                    }

                    //1,1,1,This transaction has been approved.,HD7D8V,Y,2204303158,,,2.00,CC,auth_capture,,,,,,,,,,,,eduardopiccini@me.com,,,,,,,,,,,,,,56AA75815B663E2E9C7C9C7019D9F6DB,,2,,,,,,,,,,,XXXX0027,Visa,,,,,,,,,,,,,,,,
                }
            }
            catch (CustomException ce)
            {
                saleResponse.Code = ce.Code;
                saleResponse.Description = ce.Description;
            }
            catch (Exception ex)
            {
                saleResponse.Code = Common.Utils.UNCAUGHT_ERROR;
                saleResponse.Description = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                exception = ex.Message;
            }
            finally
            { 
                Common.Utils.Concat(ref output, null, "SALE_WITH_PAYMENT_PROFILE:", false, true);

                Common.Utils.Concat(ref output, "Approved", saleResponse.Approved, false, false);
                Common.Utils.Concat(ref output, "Auth. Code", saleResponse.AuthorizationCode, false, false);
                Common.Utils.Concat(ref output, "Code", saleResponse.Code, false, false);
                Common.Utils.Concat(ref output, "Description", saleResponse.Description, false, false);
                Common.Utils.Concat(ref output, "Invoice Number", saleResponse.InvoiceNumber, false, false);
                Common.Utils.Concat(ref output, "Transaction Id", saleResponse.TransactionId, false, false);
                Common.Utils.Concat(ref output, "Amount", Request.Amount, false, false);
                Common.Utils.Concat(ref output, "Invoice Number", Request.InvoiceNumber, false, false);
                Common.Utils.Concat(ref output, "Profile Id", Request.ProfileId, false, false);
                Common.Utils.Concat(ref output, "Payment Profile Id", Request.PaymentProfileId, false, false);
                Common.Utils.Concat(ref output, "Exception", exception, false, true);

                log.Debug(output.ToString());
            }


            return saleResponse;
        }

        public CreatePaymentProfileResponse CreatePaymentProfile(CreatePaymentProfileRequest Request)
        {
            AuthWs.MerchantAuthenticationType merchant = new AuthWs.MerchantAuthenticationType();
            AuthWs.CustomerPaymentProfileType new_payment_profile = new AuthWs.CustomerPaymentProfileType();
            AuthWs.PaymentType new_payment = new AuthWs.PaymentType();
            AuthWs.CreditCardType new_card = new AuthWs.CreditCardType();
            AuthWs.CreateCustomerPaymentProfileResponseType paymentProfileResp = null;
            CreatePaymentProfileResponse response = new CreatePaymentProfileResponse();
            String exception = String.Empty;
            StringBuilder output = new StringBuilder();

            try
            {
                merchant.name = this.ApiLoginId;
                merchant.transactionKey = this.TransactionKey;

                using (AuthWs.Service ws = new AuthWs.Service())
                {
                    new_card.cardNumber = Request.FullCardNumber;
                    new_card.expirationDate = Request.ExpYear + "-" + Request.ExpMonth;
                    new_payment.Item = new_card;
                    new_payment_profile.payment = new_payment;

                    new_payment_profile.billTo = new AuthWs.CustomerAddressType();
                    new_payment_profile.billTo.address = Request.Customer.Address;
                    new_payment_profile.billTo.city = Request.Customer.City;
                    new_payment_profile.billTo.country = Request.Customer.Country;
                    new_payment_profile.billTo.firstName = Request.Customer.FirstName;
                    new_payment_profile.billTo.lastName = Request.Customer.LastName;
                    new_payment_profile.billTo.phoneNumber = Request.Customer.Phone;
                    new_payment_profile.billTo.state = Request.Customer.State;
                    new_payment_profile.billTo.zip = Request.Customer.ZipCode;

                    //if (IsTestMode)
                    //{

                        paymentProfileResp = ws.CreateCustomerPaymentProfile(merchant, Request.ProfileId, new_payment_profile, AuthWs.ValidationModeEnum.none);
                    //}
                    //else
                    //{
                    //    paymentProfileResp = ws.CreateCustomerPaymentProfile(merchant, Request.ProfileId, new_payment_profile, AuthWs.ValidationModeEnum.liveMode);
                    //}

                    response.Code = paymentProfileResp.messages[0].code;
                    response.Description = paymentProfileResp.messages[0].text;

                    if (paymentProfileResp.customerPaymentProfileId > 0)
                    {
                        response.PaymentProfileId = paymentProfileResp.customerPaymentProfileId;
                    }

                    if (response.Code.Equals(AUTHORIZE_APPROVED_CODE))
                    {
                        response.Code = Common.Utils.APPROVED;
                        response.Description = Common.Utils.APPROVED_MESSAGE;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Description = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                exception = ex.Message;
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "CREATE_PAYMENT_PROFILE:", false, true);

                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Description", response.Description, false, false);
                Common.Utils.Concat(ref output, "Payment Profile", response.PaymentProfileId, false, false);
                Common.Utils.Concat(ref output, "Exp. Month", Request.ExpMonth, false, false);
                Common.Utils.Concat(ref output, "Exp. Year", Request.ExpYear, false, false);
                Common.Utils.Concat(ref output, "Profile Id", Request.ProfileId, false, false);

                if (!String.IsNullOrEmpty(Request.FullCardNumber) && Request.FullCardNumber.Length > 4)
                {
                    Common.Utils.Concat(ref output, "Credit Card", Request.FullCardNumber.Substring(Request.FullCardNumber.Length - 4, 4), false, true);
                }

                if (Request.Customer != null)
                {
                    Common.Utils.Concat(ref output, "Address", Request.Customer.Address, false, false);
                    Common.Utils.Concat(ref output, "City", Request.Customer.City, false, false);
                    Common.Utils.Concat(ref output, "Country", Request.Customer.Country, false, false);
                    Common.Utils.Concat(ref output, "Email", Request.Customer.Email, false, false);
                    Common.Utils.Concat(ref output, "First Name", Request.Customer.FirstName, false, false);
                    Common.Utils.Concat(ref output, "Last Name", Request.Customer.LastName, false, false);
                    Common.Utils.Concat(ref output, "Phone", Request.Customer.Phone, false, false);
                    Common.Utils.Concat(ref output, "State", Request.Customer.State, false, false);
                    Common.Utils.Concat(ref output, "Zip Code", Request.Customer.ZipCode, false, true);
                }

                Common.Utils.Concat(ref output, "Exception", exception, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        public CreateProfileResponse CreateProfile(CreateProfileRequest Request)
        {
            AuthWs.MerchantAuthenticationType merchant = new AuthWs.MerchantAuthenticationType();
            AuthWs.CustomerProfileType profile = new AuthWs.CustomerProfileType();
            AuthWs.CreateCustomerProfileResponseType profileResponse = null;
            CreateProfileResponse response = new CreateProfileResponse();
            StringBuilder output = new StringBuilder();
            String exception = String.Empty;

            try
            {
                merchant.name = this.ApiLoginId;
                merchant.transactionKey = this.TransactionKey;
                
                profile.email = Request.Email;

                using (AuthWs.Service ws = new AuthWs.Service())
                {
                    //if (IsTestMode)
                    //{
                    profileResponse = ws.CreateCustomerProfile(merchant, profile, AuthWs.ValidationModeEnum.none);
                    //}
                    //else
                    //{

                    //    profileResponse = ws.CreateCustomerProfile(merchant, profile, AuthWs.ValidationModeEnum.liveMode);
                    //}

                    response.Code = profileResponse.messages[0].code;
                    response.Description = profileResponse.messages[0].text;

                    if (profileResponse.customerProfileId > 0)
                    {
                        response.ProfileId = profileResponse.customerProfileId;
                    }

                    if (response.Code.Equals(AUTHORIZE_APPROVED_CODE))
                    {
                        response.Code = Common.Utils.APPROVED;
                        response.Description = Common.Utils.APPROVED_MESSAGE;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Description = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                exception = ex.Message;
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "CREATE_PROFILE:", false, true);

                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Description", response.Description, false, false);
                Common.Utils.Concat(ref output, "Profile Id", response.ProfileId, false, false);
                Common.Utils.Concat(ref output, "Email", Request.Email, false, false);
                Common.Utils.Concat(ref output, "Exception", exception, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        public Response DeleteProfile(DeleteProfileRequest Request)
        {
            AuthWs.MerchantAuthenticationType merchant = new AuthWs.MerchantAuthenticationType();
            AuthWs.DeleteCustomerProfileResponseType profileResponse = null;
            Response response = new Response();
            String exception = String.Empty;
            StringBuilder output = new StringBuilder();

            try
            {
                merchant.name = this.ApiLoginId;
                merchant.transactionKey = this.TransactionKey;

                using (AuthWs.Service ws = new AuthWs.Service())
                {
                    profileResponse = ws.DeleteCustomerProfile(merchant, Request.ProfileId);

                    response.Code = profileResponse.messages[0].code;
                    response.Description = profileResponse.messages[0].text;

                    if (response.Code.Equals(AUTHORIZE_APPROVED_CODE))
                    {
                        response.Code = Common.Utils.APPROVED;
                        response.Description = Common.Utils.APPROVED_MESSAGE;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Description = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                exception = ex.Message;
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "DELETE_PROFILE:", false, true);

                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Description", response.Description, false, false);
                Common.Utils.Concat(ref output, "Profile Id", Request.ProfileId, false, false);
                Common.Utils.Concat(ref output, "Exception", exception, false, true);

                log.Debug(output.ToString());
            }
            return response;
        }

        public Response DeletePaymentProfile(DeletePaymentProfileRequest Request)
        {
            AuthWs.MerchantAuthenticationType merchant = new AuthWs.MerchantAuthenticationType();
            AuthWs.DeleteCustomerPaymentProfileResponseType profileResponse = null;
            Response response = new Response();
            String expcetion = String.Empty;
            StringBuilder output = new StringBuilder();
            String exception = String.Empty;

            try
            {
                merchant.name = this.ApiLoginId;
                merchant.transactionKey = this.TransactionKey;

                using (AuthWs.Service ws = new AuthWs.Service())
                {

                    profileResponse = ws.DeleteCustomerPaymentProfile(merchant, Request.ProfileId, Request.PaymentId);

                    response.Code = profileResponse.messages[0].code;
                    response.Description = profileResponse.messages[0].text;

                    if (response.Code.Equals(AUTHORIZE_APPROVED_CODE))
                    {
                        response.Code = Common.Utils.APPROVED;
                        response.Description = Common.Utils.APPROVED_MESSAGE;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Description = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                exception = ex.Message;
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "DELETE_PAYMENT_PROFILE:", false, true);

                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Description", response.Description, false, false);
                Common.Utils.Concat(ref output, "Payment Profile Id", Request.PaymentId, false, false);
                Common.Utils.Concat(ref output, "Profile Id", Request.ProfileId, false, false);
                Common.Utils.Concat(ref output, "Exception", exception, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        public Response UpdatePaymentProfile(UpdatePaymentProfileRequest Request)
        {
            AuthWs.MerchantAuthenticationType merchant = new AuthWs.MerchantAuthenticationType();
            AuthWs.CustomerPaymentProfileExType new_payment_profile = new AuthWs.CustomerPaymentProfileExType();
            AuthWs.PaymentType new_payment = new AuthWs.PaymentType();
            AuthWs.CreditCardType new_card = new AuthWs.CreditCardType();
            AuthWs.UpdateCustomerPaymentProfileResponseType paymentProfileResp = null;
            Response response = new Response();
            String exception = String.Empty;
            StringBuilder output = new StringBuilder();

            try
            {
                merchant.name = this.ApiLoginId;
                merchant.transactionKey = this.TransactionKey;

                using (AuthWs.Service ws = new AuthWs.Service())
                {
                    new_card.cardNumber = Request.FullCardNumber;
                    new_card.expirationDate = Request.ExpYear + "-" + Request.ExpMonth;
                    new_payment.Item = new_card;
                    new_payment_profile.customerPaymentProfileId = Request.PaymentProfileId;
                    new_payment_profile.payment = new_payment;

                    new_payment_profile.billTo = new AuthWs.CustomerAddressType();
                    new_payment_profile.billTo.address = Request.Customer.Address;
                    new_payment_profile.billTo.city = Request.Customer.City;
                    new_payment_profile.billTo.country = Request.Customer.Country;
                    new_payment_profile.billTo.firstName = Request.Customer.FirstName;
                    new_payment_profile.billTo.lastName = Request.Customer.LastName;
                    new_payment_profile.billTo.phoneNumber = Request.Customer.Phone;
                    new_payment_profile.billTo.state = Request.Customer.State;
                    new_payment_profile.billTo.zip = Request.Customer.ZipCode;

                    //if (IsTestMode)
                    //{

                        paymentProfileResp = ws.UpdateCustomerPaymentProfile(merchant, Request.ProfileId, new_payment_profile, AuthWs.ValidationModeEnum.none);
                    //}
                    //else
                    //{
                    //    paymentProfileResp = ws.UpdateCustomerPaymentProfile(merchant, Request.ProfileId, new_payment_profile, AuthWs.ValidationModeEnum.liveMode);
                    //}

                    response.Code = paymentProfileResp.messages[0].code;
                    response.Description = paymentProfileResp.messages[0].text;
                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Description = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                exception = ex.Message;
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "UPDATE_PAYMENT_PROFILE:", false, true);

                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Description", response.Description, false, false);
                Common.Utils.Concat(ref output, "Profile", Request.ProfileId, false, false);
                Common.Utils.Concat(ref output, "Payment Profile", Request.PaymentProfileId, false, false);
                Common.Utils.Concat(ref output, "Exp. Month", Request.ExpMonth, false, false);
                Common.Utils.Concat(ref output, "Exp. Year", Request.ExpYear, false, false);
                Common.Utils.Concat(ref output, "Profile Id", Request.ProfileId, false, false);

                if (!String.IsNullOrEmpty(Request.FullCardNumber) && Request.FullCardNumber.Length > 4)
                {
                    Common.Utils.Concat(ref output, "Credit Card", Request.FullCardNumber.Substring(Request.FullCardNumber.Length - 4, 4), false, true);
                }

                if (Request.Customer != null)
                {
                    Common.Utils.Concat(ref output, "Address", Request.Customer.Address, false, false);
                    Common.Utils.Concat(ref output, "City", Request.Customer.City, false, false);
                    Common.Utils.Concat(ref output, "Country", Request.Customer.Country, false, false);
                    Common.Utils.Concat(ref output, "Email", Request.Customer.Email, false, false);
                    Common.Utils.Concat(ref output, "First Name", Request.Customer.FirstName, false, false);
                    Common.Utils.Concat(ref output, "Last Name", Request.Customer.LastName, false, false);
                    Common.Utils.Concat(ref output, "Phone", Request.Customer.Phone, false, false);
                    Common.Utils.Concat(ref output, "State", Request.Customer.State, false, false);
                    Common.Utils.Concat(ref output, "Zip Code", Request.Customer.ZipCode, false, true);
                }

                Common.Utils.Concat(ref output, "Exception", exception, false, true);
            }

            return response;
        }
    }
}
