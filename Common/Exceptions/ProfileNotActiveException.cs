﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Exceptions
{
    public class ProfileNotActiveException : CustomException
    {
        public ProfileNotActiveException()
        {
            this.Code = "D5";
            this.Description = "Profile Not Active";
        }
    }
}
