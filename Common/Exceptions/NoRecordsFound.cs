﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Exceptions
{
    public class NoRecordsFoundException : CustomException
    {
        public NoRecordsFoundException()
        {
            this.Code = "D1";
            this.Description = "No records found";
        }
    }
}
