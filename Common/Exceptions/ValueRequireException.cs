﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Exceptions
{
    public class ValueRequireException : CustomException
    {
        public ValueRequireException(String fieldName)
        {
            this.Code = "D4";
            this.Description = fieldName + " require";
        }
    }
}
