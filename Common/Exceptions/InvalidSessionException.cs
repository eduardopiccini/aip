﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Exceptions
{
    public class InvalidSessionException : CustomException
    {
        public InvalidSessionException()
        {
            this.Code = "D2";
            this.Description = "Invalid session";
        }
    }
}
