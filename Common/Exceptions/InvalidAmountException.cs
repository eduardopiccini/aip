﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Exceptions
{
    public class InvalidAmountException : CustomException
    {
        public InvalidAmountException()
        {
            this.Code = "D3";
            this.Description = "Invalid amount";
        }
    }
}
