﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Exceptions
{
    public class CustomException : Exception
    {
        public String Code { get; set; }
        public String Description { get; set; }
        public String Entity { get; set; }
        public String Details { get; set; }

        public CustomException(String code, String description)
        {
            this.Code = code;
            this.Description = description;
        }

        public CustomException(String code, String description, String entity, String details)
        {
            this.Code = code;
            this.Description = description;
            this.Entity = entity;
            this.Details = details;
        }

        public CustomException()
        { 
        
        }

    }
}
