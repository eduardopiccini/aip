﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Exceptions
{
    public class PaymentNotActiveException : CustomException
    {
        public PaymentNotActiveException()
        {
            this.Code = "D6";
            this.Description = "Payment Not Active";
        }
    }
}
