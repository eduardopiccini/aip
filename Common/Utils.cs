﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Security.Cryptography;
using System.Net;
using System.IO;

namespace Common
{


    [Serializable]
    public class Result : IDisposable
    {
        public Result()
        {
            this.Code = "";
            this.Message = "";
            this.Details = "";
            this.Id = "";
        }

        public String Code { get; set; }
        public String Message { get; set; }
        public String Details { get; set; }
        public String Id { get; set; }

        #region IDisposable Members

        public void Dispose()
        {

        }

        #endregion
    }

    [Serializable]
    public class PaymentInformation
    {
        public String Code { get; set; }
        public String Message { get; set; }
        public String TransactionId { get; set; }
        public String AuthorizationNumber { get; set; }
        public String TransactionDateTime { get; set; }
    }

    [Serializable]
    public class GeneralPaymentInformation: PaymentInformation
    {
        public String RequestMsg { get; set; }
        public String ResponseMsg { get; set; }
        public String ProviderId { get; set; }
        public String ProviderDescription { get; set; }
        public String ProductId { get; set; }
        public String ProductName { get; set; }
        public String Cost { get; set; }
    }

    [Serializable]
    public class BillInformation
    {
        public String Code { get; set; }
        public String Message { get; set; }
        public String MinimumAmount { get; set; }
        public String BillAmount { get; set; }
        public String BillDate { get; set; }
        public String TransactionId { get; set; }
        public String TransactionDateTime { get; set; }
    }


    [Serializable]
    public class GeneralBillInformation : BillInformation
    {
        public String RequestMsg { get; set; }
        public String ResponseMsg { get; set; }
        public String ProviderId { get; set; }
        public String ProviderDescription { get; set; }
        public String ProductId { get; set; }
        public String ProductName { get; set; }
        public String Cost { get; set; }
    }


    [Serializable]
    public class Transaction
    {

        public String Code { get; set; }
        public String Message { get; set; }
        public String Details { get; set; }
        public String TransactionId { get; set; }
        public String TransactionDateTime { get; set; }
        public String AuthorizationNumber { get; set; }
        public String PinNumber { get; set; }
        public String SerialNumber { get; set; }
        public String ProviderId { get; set; }
        public String ProviderDescription { get; set; }
        public String Cost { get; set; }
        public String ProductId { get; set; }
        public String ProductName { get; set; }
        public String RequestMsg { get; set; }
        public String ResponseMsg { get; set; }
        public String ProviderAuthorizationCode {get; set;}
        public String ProviderReferenceNumber {get; set;}
        public String ReferenceNumber {get; set;}
        public String TopUpTrackingNumber { get; set; }
        public String ContactId { get; set; }
        public String MaxIntentos { get; set; }
        public String Intentos { get; set; }
        public String Did { get; set; }
        public String NewBalance { get; set; }
    }

    [Serializable]
    public class GeneralTransactionResult : TransactionResult
    {
        public String Cost { get; set; }
        public String ProviderId { get; set; }
        public String ProviderDescription { get; set; }
    }

    [Serializable]
    public class IrTransaction : GeneralTransactionResult
    {
        public String Cost { get; set; }
        public String ProductId { get; set; }
        public String ProductName { get; set; }
    }


    [Serializable]
    public class TransactionResult : Result
    {
        public TransactionResult()
        {
            this.TransactionDateTime = "";
            this.AuthorizationNumber = "";
            this.PinNumber = "";
            this.SerialNumber = "";
        }

        public String TransactionId { get; set; }
        public String TransactionDateTime { get; set; }
        public String AuthorizationNumber { get; set; }
        public String PinNumber { get; set; }
        public String SerialNumber { get; set; }
    }

    [Serializable]
    public class TransactionResultSp : TransactionResult
    {
        public String NewBalance { get; set; }
        public String Did { get; set; }
    }

    public class Utils
    {
        public const String AUTHORIZE_PAYMENT_PROFILE_INVALID = "ER00013";
        public const String AUTHORIZE_APPROVED = "1";
        public const String DB_APPROVED = "0";
        public const String APPROVED = "00";
        public const String TRANSACTION_IN_PROGRESS = "99";
        public const String APPROVED_MESSAGE = "Success";
        public const String DENOMINATION_REQUIRE = "PA6";
        public const String CURRENCY_REQUIRE = "PA7";
        public const String ENTITY_REQUIRE = "PA8";
        public const String SUBENTITY_REQUIRE = "PA9";
        public const String COUNTRY_CODE_REQUIRE = "PA10";
        public const String TRANSACTION_DATE_REQUIRE = "PA11";
        public const String RECHARGE_TRANSACTION_ID_REQUIRE = "PA12";
        public const String OPERATOR_ID_REQUIRE = "PA13";
        public const String REFERENCE_REQUIRE = "PA14";
        public const String ERROR_SENDING_SMS = "PA15";
        public const String TRANSACTION_ID_REQUIRE = "PA1";
        public const String PHONE_NUMBER_REQUIRE = "PA2";
        public const String AMOUNT_REQUIRE = "PA3";
        public const String PRODUCT_ID_REQUIRE = "PA4";
        public const String AUTHORIZATION_NUMBER_REQUIRE = "PA5";
        public const String NO_RECORDS_FOUND = "DL1";
        public const String NEW_NO_RECORDS_FOUND = "D1";
        public const String UNKNOWN_EXCEPTION = "DL2";
        public const String LIMIT_NOT_AVAILABLE = "PA16";
        public const String MERCHANT_NO_MONEY = "PA17";
        public const String PHONE_QUERY_NO_RESPONSE = "PA18";
        public const String PHONE_DOES_NOT_EXISTS = "PA19";
        public const String RECHARGE_NO_RESPONSE = "PA20";
        public const String VOID_NO_RESPONSE = "PA21";
        public const String PIN_NO_RESPONSE = "PA22";
        public const String ZIP_CODE_RESPONSE = "PA23";
        public const String STORE_ID_RESPONSE = "PA24";
        public const String BILL_INFORMATION_NO_RESPONSE = "PA25";
        public const String BILL_INFORMATION_NO_RESPONSE_MESSAGE = "NO BILL INFORMATION AVAILABLE";
        public const String PAYMENT_INFORMATION_NO_RESPONSE = "PA26";
        public const String PAYMENT_INFORMATION_NO_RESPONSE_MESSAGE = "NO PAYMENT INFORMATION AVAILABLE";
        public const String REQUEST_TIMEOUT = "PA27";
        public const String CONNECTION_WAS_ABORTED = "PA28";
        public const String CONNECTION_ATTEMPT_FAILED = "PA29";
        public const String SESSION_NOT_AVAILABLE = "UC1";
        public const String PROVIDER_NOT_FOUND = "UC3";
        public const String ERROR_NOT_DEFINED = "UC4";
        public const String FIX_RECHARGE = "Fix";
        public const String FLEXI_RECHARGE = "Flexi";
        public const String IRIS_SUCCESS = "10";
        public const String MESSAGE_REQUIRE = "UC5";
        public const String UNCAUGHT_ERROR = "AC10";
        public const String UNCAUGHT_ERROR_MESSAGE = "TECHNICAL ERROR";
        public const String UTD = "utd";
        public const String FREESTAR = "free";
        public const String MIDASCARD_NO_EXISTE = "56";
        public const String MIDASCARD_ERROR_REVERSO = "57";
        public const String CLIENT_SESSION_NOT_AVAILABLE_CODE = "UC77";
        public const String CLIENT_SESSION_NOT_AVAILABLE_MESSAGE = "CLIENT SESSION NOT AVAILABLE";
        public const String TECHNICAL_ERROR_CODE = "-1";
        public const String TECHNICAL_ERROR_MESSAGE = "UNKNOWN ERROR";
        public const String DB_SUCCESS = "0";
        public const String SUCCESS_MESSAGE = "SUCCESS";
        public const String NO_RECORDS_FOUND_CODE = "-2";
        public const String NO_RECORDS_FOUND_MESSAGE = "NO RECORDS FOUND";
        public const String SESSION_REQUIRE_CODE = "A01";
        public const String SESSION_REQUIRE_MESSAGE = "SESSION REQUIRE";
        public const String PRODUCT_CODE_REQUIRE_CODE = "UC20";
        public const String PRODUCT_CODE_REQUIRE_MESSAGE = "PRODUCT CODE REQUIRE";
        public const String SMS_RESPONSE_EMPTY_CODE = "T90";
        public const String SMS_RESPONSE_EMPTY_MESSAGE = "SMS RESPONSE CAME EMPTY";
        public const String SMS_NOT_APPROVED_CODE = "T91";
        public const String SMS_NOT_APPROVED_MESSAGE = "SMS NOT APPROVED";
        public const String NO_PROFILE_CONFIGURE_CODE = "T92";
        public const String NO_PROFILE_CONFIGURE_MESSAGE = "NO PROFILE CONFIGURE";

        /// <summary>
        /// Evalua si un valor es nulo o esta vacio y retorna un valor por defecto
        /// </summary>
        /// <param name="Valor"></param>
        /// <param name="ValorDefecto">Valor a retornar en caso de que el valor sea nulo</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static Object IsNull(Object Valor, Object ValorDefecto)
        {
            try
            {
                return (Convert.IsDBNull(Valor) || Valor == null || String.IsNullOrEmpty(Valor.ToString().Trim()) ? ValorDefecto : Valor);
            }
            catch { return ValorDefecto; }
        }

        


        public static Int64? ConvertToInt64(Object Valor,Object ValorDefecto)
        {

            try
            {
                Int64 result = 0;

                if (Int64.TryParse(Valor.ToString(), out result))
                    return result;
                else
                    return null;
            }
            catch
            {
                return null;
            }


        }

        public static Int64 ConvertToInt64(Object Valor)
        {
            return Convert.ToInt64(IsNull(Valor, 0).ToString());

        }

        public static Decimal ConvertToDecimal(Object Valor)
        {
            return Convert.ToDecimal(IsNull(Valor, 0).ToString());

        }
        public static Decimal? ConvertToDecimal(Object Valor,Object ValorDefecto)
        {

            try
            {
                Decimal result = 0;

                if (Decimal.TryParse(Valor.ToString(), out result))
                    return result;
                else
                    return null;
            }
            catch {
                return null;
            }

        }


        public static Object GetAppSettingValue(String key)
        {
            return System.Configuration.ConfigurationManager.AppSettings[key];
        }


        public static String FindResponseDescription(String code, String path)
        {
            XDocument doc = XDocument.Load(path);
            String description = null;

            XElement node = (from xml2 in doc.Descendants("Response")
                             where xml2.Element("Id").Value == code
                             select xml2).FirstOrDefault();

            if (node != null)
            {
                description = ((XElement)node.FirstNode.NextNode).Value;
            }

            return description;
        }

        public static string SHA1Encrypt(string text)
        {
            UnicodeEncoding UE = new UnicodeEncoding();
            byte[] hashValue;
            byte[] message = UE.GetBytes(text);

            SHA1Managed hashString = new SHA1Managed();
            string hex = "";

            hashValue = hashString.ComputeHash(message);
            foreach (byte x in hashValue)
            {
                hex += String.Format("{0:x2}", x);
            }
            return hex;
        }

        public static string MD5Encrypt(string value)
        {
            MD5CryptoServiceProvider provider = new MD5CryptoServiceProvider();

            byte[] data = System.Text.Encoding.ASCII.GetBytes(value);
            data = provider.ComputeHash(data);

            string md5 = string.Empty;

            for (int i = 0; i < data.Length; i++)
                md5 += data[i].ToString("x2").ToLower();

            return md5;
        }

        public static byte[] MD5_of_SHA1(byte[] stream)
        {
            SHA1 sha1 = new SHA1CryptoServiceProvider();
            MD5 md5 = new MD5CryptoServiceProvider();
            ASCIIEncoding Encode = new ASCIIEncoding();


            String sha1_string = BitConverter.ToString(sha1.ComputeHash(stream)).Replace("-", "").ToLower();
            byte[] md5_stream = md5.ComputeHash(Encode.GetBytes(sha1_string));

            return md5_stream;
        }

        public static String MD5_of_SHA1(string textString)
        {
            byte[] md5_stream = MD5_of_SHA1(Encoding.ASCII.GetBytes(textString));

            return BitConverter.ToString(md5_stream).Replace("-", "").ToLower();
        }

        public static String GetConfigValue(String key)
        {
            String value = null;

            if (System.Configuration.ConfigurationManager.AppSettings[key] != null)
            {
                value = System.Configuration.ConfigurationManager.AppSettings[key].ToString();
            }

            return value;
        }

        public static String GetGeneralErrorCode()
        {
            String value = null;

            value = GetConfigValue("general_error_code");

            return value;
        }

        public static String GetSessionNaCode()
        {
            String value = null;

            value = GetConfigValue("sesion_end_code");

            return value;
        }

        public static String LeftPadding(String text, String value, int count)
        {
            StringBuilder msg = new StringBuilder();

            for (int i = 0; i < (count - text.Length); i++)
            {
                msg.Append(value);
            }

            msg.Append(text);

            return msg.ToString();
        }

        public static String GeneraNumeroRamdon(int min, int max)
        {
            Random random = new Random();

            return random.Next(min, max).ToString(); 
        }

        public static void Concat(ref StringBuilder temp, String text, Object value, Boolean endoFLine, Boolean last)
        {
            if (!String.IsNullOrEmpty(text))
            {
                temp.Append(text);
                temp.Append("=");
            }
            
            if (value == null)
            {
                temp.Append(String.Empty);
            }
            else
            {
                if (String.IsNullOrEmpty(value.ToString()))
                {
                    temp.Append(String.Empty);
                }
                else
                {
                    if (endoFLine)
                    {
                        temp.AppendLine(value.ToString());
                    }
                    else
                    {
                        temp.Append(value);
                    }
                }

                if (!last)
                {
                    temp.Append(",");
                }
            }
        }

        public static DateTime? ObjectToDateTime(Object obj)
        {
            DateTime? value = null;

            if (obj != null)
            {
                if (!String.IsNullOrEmpty(obj.ToString()))
                {
                    value = Convert.ToDateTime(obj.ToString());
                }
                else
                {
                    value = null;
                }
            }

            return value;
        }

        public static long? ObjectToLong(Object obj)
        {
            long? value = null;

            if (obj != null)
            {
                if (!String.IsNullOrEmpty(obj.ToString()))
                {
                    value = Convert.ToInt64(obj.ToString());
                }
                else
                {
                    value = null;
                }
            }

            return value;
        }

        public static int? ObjectToInt(Object obj)
        {
            int? value = null;

            if (obj != null)
            {
                if (!String.IsNullOrEmpty(obj.ToString()))
                {
                    value = Convert.ToInt32(obj.ToString());
                }
                else
                {
                    value = null;
                }
            }

            return value;
        }

        public static double? ObjectToDouble(Object obj)
        {
            double? value = null;

            if (obj != null)
            {
                if (!String.IsNullOrEmpty(obj.ToString()))
                {
                    value = Convert.ToDouble(obj.ToString());
                }
                else
                {
                    value = null;
                }
            }

            return value;
        }

        public static String ObjectToString(Object obj)
        {
            String value = null;

            if (obj != null)
            {
                value = obj.ToString();
            }

            return value;
        }
        
    }
}
