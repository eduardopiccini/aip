﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using PL.App_Code;
using System.ServiceModel.Channels;
using log4net;

namespace PL
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    public class app : Iapp
    {
        private static readonly ILog log = LogManager.GetLogger("Log");
        private static readonly ILog logSummary = LogManager.GetLogger("LogSummary");

        public LoginResponse Login(LoginRequest Request)
        {
            LoginResponse response = new LoginResponse();
            String ip = String.Empty, locale = String.Empty, comments = String.Empty;
            StringBuilder output = new StringBuilder();
            
            try
            {
                ip = this.GetIp();

                using (SessionDao dao = new SessionDao())
                {
                    response = dao.Login(Request, ip, Guid.NewGuid().ToString(), locale);
                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                comments = ex.Message;
            }
            finally
            {
                if (!String.IsNullOrEmpty(response.Comments))
                {
                    comments = response.Comments;
                    response.Comments = String.Empty;
                }

                Common.Utils.Concat(ref output, null, "LOGIN:", false, true);
                Common.Utils.Concat(ref output, "User", Request.UserName, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Details", comments, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        public Response Logout(Request Request)
        {
            Response response = new Response();
            String ip = String.Empty, locale = String.Empty, comments = String.Empty;
            StringBuilder output = new StringBuilder();

            try
            {
                ip = this.GetIp();

                using (SessionDao dao = new SessionDao())
                {
                    response = dao.Logout(Request, locale);
                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                comments = ex.Message;
            }
            finally
            {
                if (!String.IsNullOrEmpty(response.Comments))
                {
                    comments = response.Comments;
                    response.Comments = String.Empty;
                }

                Common.Utils.Concat(ref output, null, "LOGIN:", false, true);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Details", comments, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        private String GetIp()
        {
            OperationContext context = null;
            MessageProperties messageProperties = null;
            RemoteEndpointMessageProperty endpointProperty = null;
            String ip = null;

            context = OperationContext.Current;

            messageProperties = context.IncomingMessageProperties;
            endpointProperty = messageProperties[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;

            ip = endpointProperty.Address;

            return ip;
        }

        public BuyResponse Buy(BuyRequest Request)
        {
            String details = String.Empty, ip = String.Empty, userId = String.Empty;
            BuyResponse buyResponse = new BuyResponse();
            ValidateResponse validateResponse = null;
            StringBuilder output = new StringBuilder();
            DateTime? begin = DateTime.Now, end;

            try
            {
                ip = this.GetIp();

                if (String.IsNullOrEmpty(Request.SessionId))
                {
                    buyResponse.Code = Common.Utils.SESSION_REQUIRE_CODE;
                    buyResponse.Message = Common.Utils.SESSION_REQUIRE_MESSAGE;

                    throw new Exception(buyResponse.Message);
                }

                if (String.IsNullOrEmpty(Request.ProductId))
                {
                    buyResponse.Code = Common.Utils.PRODUCT_CODE_REQUIRE_CODE;
                    buyResponse.Message = Common.Utils.PRODUCT_CODE_REQUIRE_MESSAGE;

                    throw new Exception(Common.Utils.PRODUCT_CODE_REQUIRE_MESSAGE);
                }

                using (SessionDao dao = new SessionDao())
                {
                    validateResponse = dao.Validate(Request.SessionId, ip, null);

                    if (!validateResponse.Code.Equals(Common.Utils.DB_SUCCESS))
                    {
                        buyResponse.Code = validateResponse.Code;
                        buyResponse.Message = validateResponse.Message;

                        throw new Exception(buyResponse.Message);
                    }

                    userId = validateResponse.UserId;
                }

                using (AccountDao dao = new AccountDao())
                {
                    buyResponse = dao.Buy(Request, userId);
                }
            }
            catch (Exception ex)
            {
                details = ex.Message;

                if (String.IsNullOrEmpty(buyResponse.Code))
                {
                    buyResponse.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                    buyResponse.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                }
            }
            finally
            {
                end = DateTime.Now;

                if (!String.IsNullOrEmpty(Request.RoutingId))
                {
                    using (TransLogWs.Service ws = new TransLogWs.Service())
                    {
                        TransLogWs.LogRequest req = new TransLogWs.LogRequest();

                        req.AuthorizationNumber = buyResponse.TransReference == null ? null : buyResponse.TransReference.ToString();
                        req.BeginDate = begin.Value;
                        req.EndDate = end.Value;
                        req.Code = buyResponse.Code;
                        req.Message = buyResponse.Message;
                        req.DbUser = Request.DbUser;
                        req.Locale = Request.Locale;
                        req.RoutingId = Request.RoutingId;

                        ws.Log(req);
                    }
                }

                Common.Utils.Concat(ref output, null, "BUY:", false, true);
                Common.Utils.Concat(ref output, "UserId", userId, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", buyResponse.Code, false, false);
                Common.Utils.Concat(ref output, "Message", buyResponse.Message, false, false);
                Common.Utils.Concat(ref output, "TransAmount", Request.TransAmount, false, false);
                Common.Utils.Concat(ref output, "AccountHolder", Request.AccountHolder, false, false);
                Common.Utils.Concat(ref output, "AccountNumber", Request.AccountNumber, false, false);
                Common.Utils.Concat(ref output, "CityId", Request.CityId, false, false);
                Common.Utils.Concat(ref output, "DollarFree", Request.DollarFree, false, false);
                Common.Utils.Concat(ref output, "LocaleId", Request.LocaleId, false, false);
                Common.Utils.Concat(ref output, "PhoneNotification", Request.PhoneNotification, false, false);
                Common.Utils.Concat(ref output, "ProductId", Request.ProductId, false, false);
                Common.Utils.Concat(ref output, "Reference", Request.Reference, false, false);
                Common.Utils.Concat(ref output, "StoreId", Request.StoreId, false, false);
                Common.Utils.Concat(ref output, "StoreName", Request.StoreName, false, false);

                
                if(begin!=null)
                {
                    Common.Utils.Concat(ref output, "BeginDate", begin.Value.ToString("hh:mm:ss tt"), false, false);
                }

                if (end != null)
                {
                    Common.Utils.Concat(ref output, "EndDate", end.Value.ToString("hh:mm:ss tt"), false, false);
                }

                Common.Utils.Concat(ref output, "Details", details, false, true);

                logSummary.Debug(output.ToString());
            }

            return buyResponse;
        }

        public VoidResponse Void(VoidRequest Request)
        {
            String details = String.Empty, ip = String.Empty, userId = String.Empty;
            VoidResponse voidResponse = new VoidResponse();
            ValidateResponse validateResponse = null;
            StringBuilder output = new StringBuilder();
            DateTime? begin = DateTime.Now, end = null;

            try
            {
                ip = this.GetIp();

                if (String.IsNullOrEmpty(Request.SessionId))
                {
                    voidResponse.Code = Common.Utils.SESSION_REQUIRE_CODE;
                    voidResponse.Message = Common.Utils.SESSION_REQUIRE_MESSAGE;

                    throw new Exception(voidResponse.Message);
                }

                using (SessionDao dao = new SessionDao())
                {
                    validateResponse = dao.Validate(Request.SessionId, ip, null);

                    if (!validateResponse.Code.Equals(Common.Utils.DB_SUCCESS))
                    {
                        voidResponse.Code = validateResponse.Code;
                        voidResponse.Message = validateResponse.Message;

                        throw new Exception(voidResponse.Message);
                    }

                    userId = validateResponse.UserId;
                }

                using (AccountDao dao = new AccountDao())
                {
                    voidResponse = dao.Void(Request, userId);
                }
            }
            catch (Exception ex)
            {
                details = ex.Message;

                if (String.IsNullOrEmpty(voidResponse.Code))
                {
                    voidResponse.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                    voidResponse.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                }
            }
            finally
            {
                end = DateTime.Now;

                if (!String.IsNullOrEmpty(Request.RoutingId))
                {
                    using (TransLogWs.Service ws = new TransLogWs.Service())
                    {
                        TransLogWs.LogRequest req = new TransLogWs.LogRequest();

                        req.AuthorizationNumber = Common.Utils.ObjectToString(voidResponse.TransReference);
                        req.BeginDate = begin.Value;
                        req.EndDate = end.Value;
                        req.Code = voidResponse.Code;
                        req.DbUser = Request.DbUser;
                        req.Locale = Request.Locale;
                        req.RoutingId = Request.RoutingId;

                        ws.LogAsync(req);
                    }
                }

                Common.Utils.Concat(ref output, null, "VOID:", false, true);
                Common.Utils.Concat(ref output, "User", userId, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", voidResponse.Code, false, false);
                Common.Utils.Concat(ref output, "Message", voidResponse.Message, false, false);
                Common.Utils.Concat(ref output, "TransReference", voidResponse.TransReference, false, false);
                Common.Utils.Concat(ref output, "Reference", Request.Reference, false, false);
                


                if (begin != null)
                {
                    Common.Utils.Concat(ref output, "BeginDate", begin.Value.ToString("hh:mm:ss tt"), false, false);
                }

                if (end != null)
                {
                    Common.Utils.Concat(ref output, "EndDate", end.Value.ToString("hh:mm:ss tt"), false, false);
                }
                
                Common.Utils.Concat(ref output, "Details", voidResponse.Comments, false, true);


                logSummary.Debug(output.ToString());
            }

            return voidResponse;
        }

        public Response SubscriberAdd(SubscriberInsertRequest Request)
        {
            String details = String.Empty, ip = String.Empty, userId = String.Empty;
            Response response = new Response();
            ValidateResponse validateResponse = null;
            StringBuilder output = new StringBuilder();

            try
            {
                ip = this.GetIp();

                if (String.IsNullOrEmpty(Request.SessionId))
                {
                    response.Code = Common.Utils.SESSION_REQUIRE_CODE;
                    response.Message = Common.Utils.SESSION_REQUIRE_MESSAGE;

                    throw new Exception(response.Message);
                }

                using (SessionDao dao = new SessionDao())
                {
                    validateResponse = dao.Validate(Request.SessionId, ip, null);

                    if (!validateResponse.Code.Equals(Common.Utils.DB_SUCCESS))
                    {
                        response.Code = validateResponse.Code;
                        response.Message = validateResponse.Message;

                        throw new Exception(response.Message);
                    }

                    userId = validateResponse.UserId;
                }

                using (AccountDao dao = new AccountDao())
                {
                    response = dao.SubscriberInsert(Request, userId, null);
                }
            }
            catch (Exception ex)
            {
                details = ex.Message;

                if (String.IsNullOrEmpty(response.Code))
                {
                    response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                    response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                }
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "SUBSCRIBER_ADD:", false, true);
                Common.Utils.Concat(ref output, "User", userId, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "AccountId", Request.AccountId, false, false);
                Common.Utils.Concat(ref output, "ProductId", Request.ProductId, false, false);
                Common.Utils.Concat(ref output, "Subscriber", Request.Subscriber, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        public Response SubscriberRemove(SubscriberDeleteRequest Request)
        {
            String details = String.Empty, ip = String.Empty, userId = String.Empty;
            Response response = new Response();
            ValidateResponse validateResponse = null;
            StringBuilder output = new StringBuilder();

            try
            {
                ip = this.GetIp();

                if (String.IsNullOrEmpty(Request.SessionId))
                {
                    response.Code = Common.Utils.SESSION_REQUIRE_CODE;
                    response.Message = Common.Utils.SESSION_REQUIRE_MESSAGE;

                    throw new Exception(response.Message);
                }

                using (SessionDao dao = new SessionDao())
                {
                    validateResponse = dao.Validate(Request.SessionId, ip, null);

                    if (!validateResponse.Code.Equals(Common.Utils.DB_SUCCESS))
                    {
                        response.Code = validateResponse.Code;
                        response.Message = validateResponse.Message;

                        throw new Exception(response.Message);
                    }

                    userId = validateResponse.UserId;
                }

                using (AccountDao dao = new AccountDao())
                {
                    response = dao.SubscriberDelete(Request, userId, null);
                }
            }
            catch (Exception ex)
            {
                details = ex.Message;
                
                if (String.IsNullOrEmpty(response.Code))
                {
                    response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                    response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                }
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "SUBSCRIBER_REMOVE:", false, true);
                Common.Utils.Concat(ref output, "User", userId, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "AccountId", Request.AccountId, false, false);
                Common.Utils.Concat(ref output, "Subscriber", Request.Subscriber, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        public TransferResponse TransferBalance(TransferRequest Request)
        {
            String details = String.Empty, ip = String.Empty, userId = String.Empty;
            TransferResponse response = new TransferResponse();
            ValidateResponse validateResponse = null;
            StringBuilder output = new StringBuilder();

            try
            {
                ip = this.GetIp();

                if (String.IsNullOrEmpty(Request.SessionId))
                {
                    response.Code = Common.Utils.SESSION_REQUIRE_CODE;
                    response.Message = Common.Utils.SESSION_REQUIRE_MESSAGE;

                    throw new Exception(response.Message);
                }

                using (SessionDao dao = new SessionDao())
                {
                    validateResponse = dao.Validate(Request.SessionId, ip, null);

                    if (!validateResponse.Code.Equals(Common.Utils.DB_SUCCESS))
                    {
                        response.Code = validateResponse.Code;
                        response.Message = validateResponse.Message;

                        throw new Exception(response.Message);
                    }

                    userId = validateResponse.UserId;
                }

                using (AccountDao dao = new AccountDao())
                {
                    response = dao.Transfer(Request, userId, null);
                }
            }
            catch (Exception ex)
            {
                details = ex.Message;

                if (String.IsNullOrEmpty(response.Code))
                {
                    response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                    response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                }
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "TRANSFER_BALANCE:", false, true);
                Common.Utils.Concat(ref output, "User", userId, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "AccountIdFrom", Request.AccountIdFrom, false, false);
                Common.Utils.Concat(ref output, "AccountIdTo", Request.AccountIdTo, false, false);
                Common.Utils.Concat(ref output, "TransAmount", Request.TransAmount, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        public AccountSearchResponse AccountSearch(AccountSearchRequest Request)
        {
            String details = String.Empty, ip = String.Empty, userId = String.Empty;
            AccountSearchResponse response = new AccountSearchResponse();
            ValidateResponse validateResponse = null;
            StringBuilder output = new StringBuilder();

            try
            {
                ip = this.GetIp();

                if (String.IsNullOrEmpty(Request.SessionId))
                {
                    response.Code = Common.Utils.SESSION_REQUIRE_CODE;
                    response.Message = Common.Utils.SESSION_REQUIRE_MESSAGE;

                    throw new Exception(response.Message);
                }

                using (SessionDao dao = new SessionDao())
                {
                    validateResponse = dao.Validate(Request.SessionId, ip, null);

                    if (!validateResponse.Code.Equals(Common.Utils.DB_SUCCESS))
                    {
                        response.Code = validateResponse.Code;
                        response.Message = validateResponse.Message;

                        throw new Exception(response.Message);
                    }

                    userId = validateResponse.UserId;
                }

                using (AccountDao dao = new AccountDao())
                {
                    response = dao.Search(Request);
                }
            }
            catch (Exception ex)
            {
                details = ex.Message;

                if (String.IsNullOrEmpty(response.Code))
                {
                    response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                    response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                }
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "ACCOUNT_SEARCH:", false, true);
                Common.Utils.Concat(ref output, "User", userId, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "AccountId", Request.AccountId, false, false);
                Common.Utils.Concat(ref output, "AccountNumber", Request.AccountNumber, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        public SpeedDialSearchResponse SpeedDialSearch(SpeedDialSearchRequest Request)
        {
            String details = String.Empty, ip = String.Empty, userId = String.Empty;
            SpeedDialSearchResponse response = new SpeedDialSearchResponse();
            ValidateResponse validateResponse = null;
            StringBuilder output = new StringBuilder();

            try
            {
                ip = this.GetIp();

                if (String.IsNullOrEmpty(Request.SessionId))
                {
                    response.Code = Common.Utils.SESSION_REQUIRE_CODE;
                    response.Message = Common.Utils.SESSION_REQUIRE_MESSAGE;

                    throw new Exception(response.Message);
                }

                using (SessionDao dao = new SessionDao())
                {
                    validateResponse = dao.Validate(Request.SessionId, ip, null);

                    if (!validateResponse.Code.Equals(Common.Utils.DB_SUCCESS))
                    {
                        response.Code = validateResponse.Code;
                        response.Message = validateResponse.Message;

                        throw new Exception(response.Message);
                    }

                    userId = validateResponse.UserId;
                }

                using (AccountDao dao = new AccountDao())
                {
                    response = dao.SpeedDialSearch(Request);
                }
            }
            catch (Exception ex)
            {
                details = ex.Message;

                if (String.IsNullOrEmpty(response.Code))
                {
                    response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                    response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                }
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "SPEED_DIAL_SEARCH:", false, true);
                Common.Utils.Concat(ref output, "User", userId, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "AccountId", Request.AccountId, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        public SubscriberSearchResponse SubscriberSearch(SubscriberSearchRequest Request)
        {
            String details = String.Empty, ip = String.Empty, userId = String.Empty;
            SubscriberSearchResponse response = new SubscriberSearchResponse();
            ValidateResponse validateResponse = null;
            StringBuilder output = new StringBuilder();

            try
            {
                ip = this.GetIp();

                if (String.IsNullOrEmpty(Request.SessionId))
                {
                    response.Code = Common.Utils.SESSION_REQUIRE_CODE;
                    response.Message = Common.Utils.SESSION_REQUIRE_MESSAGE;

                    throw new Exception(response.Message);
                }

                using (SessionDao dao = new SessionDao())
                {
                    validateResponse = dao.Validate(Request.SessionId, ip, null);

                    if (!validateResponse.Code.Equals(Common.Utils.DB_SUCCESS))
                    {
                        response.Code = validateResponse.Code;
                        response.Message = validateResponse.Message;

                        throw new Exception(response.Message);
                    }

                    userId = validateResponse.UserId;
                }

                using (AccountDao dao = new AccountDao())
                {
                    response = dao.SubscriberSearch(Request);
                }
            }
            catch (Exception ex)
            {
                details = ex.Message;

                if (String.IsNullOrEmpty(response.Code))
                {
                    response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                    response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                }
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "SUBSCRIBER_SEARCH:", false, true);
                Common.Utils.Concat(ref output, "User", userId, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "AccountId", Request.AccountId, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        public ProductRateSearchResponse PoductRateSearch(ProductRateSearchRequest Request)
        {
            String details = String.Empty, ip = String.Empty, userId = String.Empty;
            ProductRateSearchResponse response = new ProductRateSearchResponse();
            ValidateResponse validateResponse = null;
            StringBuilder output = new StringBuilder();

            try
            {
                ip = this.GetIp();

                if (String.IsNullOrEmpty(Request.SessionId))
                {
                    response.Code = Common.Utils.SESSION_REQUIRE_CODE;
                    response.Message = Common.Utils.SESSION_REQUIRE_MESSAGE;

                    throw new Exception(response.Message);
                }

                using (SessionDao dao = new SessionDao())
                {
                    validateResponse = dao.Validate(Request.SessionId, ip, null);

                    if (!validateResponse.Code.Equals(Common.Utils.DB_SUCCESS))
                    {
                        response.Code = validateResponse.Code;
                        response.Message = validateResponse.Message;

                        throw new Exception(response.Message);
                    }

                    userId = validateResponse.UserId;
                }

                using (ProductRateDao dao = new ProductRateDao())
                {
                    response = dao.Search(Request);
                }
            }
            catch (Exception ex)
            {
                details = ex.Message;

                if (String.IsNullOrEmpty(response.Code))
                {
                    response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                    response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                }
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "PRODUCT_RATE_SEARCH:", false, true);
                Common.Utils.Concat(ref output, "User", userId, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "CountrCode", Request.CountryCode, false, false);
                Common.Utils.Concat(ref output, "ProductId", Request.ProductId, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        public TransLogSearchResponse TransLogSearch(TransLogSearchRequest Request)
        {
            String details = String.Empty, ip = String.Empty, userId = String.Empty;
            TransLogSearchResponse response = new TransLogSearchResponse();
            ValidateResponse validateResponse = null;
            StringBuilder output = new StringBuilder();

            try
            {
                ip = this.GetIp();

                if (String.IsNullOrEmpty(Request.SessionId))
                {
                    response.Code = Common.Utils.SESSION_REQUIRE_CODE;
                    response.Message = Common.Utils.SESSION_REQUIRE_MESSAGE;

                    throw new Exception(response.Message);
                }

                using (SessionDao dao = new SessionDao())
                {
                    validateResponse = dao.Validate(Request.SessionId, ip, null);

                    if (!validateResponse.Code.Equals(Common.Utils.DB_SUCCESS))
                    {
                        response.Code = validateResponse.Code;
                        response.Message = validateResponse.Message;

                        throw new Exception(response.Message);
                    }

                    userId = validateResponse.UserId;
                }

                using (TransLogDao dao = new TransLogDao())
                {
                    response = dao.Search(Request);
                }
            }
            catch (Exception ex)
            {
                details = ex.Message;

                if (String.IsNullOrEmpty(response.Code))
                {
                    response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                    response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                }
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "TRANS_LOG_SEARCH:", false, true);
                Common.Utils.Concat(ref output, "User", userId, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "AccountId", Request.AccountId, false, false);
                Common.Utils.Concat(ref output, "TransDateBegin", Request.TransDateBegin, false, false);
                Common.Utils.Concat(ref output, "TransDateEnd", Request.TransDateEnd, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        public AccessNumberSearchResponse AccessNumberSearch(AccessNumberSearchRequest Request)
        {
            String details = String.Empty, ip = String.Empty, userId = String.Empty;
            AccessNumberSearchResponse response = new AccessNumberSearchResponse();
            ValidateResponse validateResponse = null;
            StringBuilder output = new StringBuilder();

            try
            {
                ip = this.GetIp();

                if (String.IsNullOrEmpty(Request.SessionId))
                {
                    response.Code = Common.Utils.SESSION_REQUIRE_CODE;
                    response.Message = Common.Utils.SESSION_REQUIRE_MESSAGE;

                    throw new Exception(response.Message);
                }

                using (SessionDao dao = new SessionDao())
                {
                    validateResponse = dao.Validate(Request.SessionId, ip, null);

                    if (!validateResponse.Code.Equals(Common.Utils.DB_SUCCESS))
                    {
                        response.Code = validateResponse.Code;
                        response.Message = validateResponse.Message;

                        throw new Exception(response.Message);
                    }

                    userId = validateResponse.UserId;
                }

                using (AccessNumberDao dao = new AccessNumberDao())
                {
                    response = dao.Search(Request);
                }
            }
            catch (Exception ex)
            {
                details = ex.Message;

                if (String.IsNullOrEmpty(response.Code))
                {
                    response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                    response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                }
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "ACCESS_NUMBER_SEARCH:", false, true);
                Common.Utils.Concat(ref output, "User", userId, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "StateId", Request.StateId, false, false);
                Common.Utils.Concat(ref output, "ProductId", Request.ProductId, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        public Response SpeedDialChange(SpeedDialChangeRequest Request)
        {
            String details = String.Empty, ip = String.Empty, userId = String.Empty;
            Response response = new Response();
            ValidateResponse validateResponse = null;
            StringBuilder output = new StringBuilder();

            try
            {
                ip = this.GetIp();

                if (String.IsNullOrEmpty(Request.SessionId))
                {
                    response.Code = Common.Utils.SESSION_REQUIRE_CODE;
                    response.Message = Common.Utils.SESSION_REQUIRE_MESSAGE;

                    throw new Exception(response.Message);
                }

                using (SessionDao dao = new SessionDao())
                {
                    validateResponse = dao.Validate(Request.SessionId, ip, null);

                    if (!validateResponse.Code.Equals(Common.Utils.DB_SUCCESS))
                    {
                        response.Code = validateResponse.Code;
                        response.Message = validateResponse.Message;

                        throw new Exception(response.Message);
                    }

                    userId = validateResponse.UserId;
                }

                using (AccountDao dao = new AccountDao())
                {
                    response = dao.SpeedDialChange(Request, userId, null);
                }
            }
            catch (Exception ex)
            {
                details = ex.Message;

                if (String.IsNullOrEmpty(response.Code))
                {
                    response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                    response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                }
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "SPEED_DIAL_CHANGE:", false, true);
                Common.Utils.Concat(ref output, "User", userId, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "AccountId", Request.AccountId, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        public Response AccountUpdate(AccountUpdateRequest Request)
        {
            String details = String.Empty, ip = String.Empty, userId = String.Empty;
            Response response = new Response();
            ValidateResponse validateResponse = null;
            StringBuilder output = new StringBuilder();

            try
            {
                ip = this.GetIp();

                if (String.IsNullOrEmpty(Request.SessionId))
                {
                    response.Code = Common.Utils.SESSION_REQUIRE_CODE;
                    response.Message = Common.Utils.SESSION_REQUIRE_MESSAGE;

                    throw new Exception(response.Message);
                }

                using (SessionDao dao = new SessionDao())
                {
                    validateResponse = dao.Validate(Request.SessionId, ip, null);

                    if (!validateResponse.Code.Equals(Common.Utils.DB_SUCCESS))
                    {
                        response.Code = validateResponse.Code;
                        response.Message = validateResponse.Message;

                        throw new Exception(response.Message);
                    }

                    userId = validateResponse.UserId;
                }

                using (AccountDao dao = new AccountDao())
                {
                    response = dao.UpdateAccount(Request, userId, null);
                }
            }
            catch (Exception ex)
            {
                details = ex.Message;

                if (String.IsNullOrEmpty(response.Code))
                {
                    response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                    response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                }
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "ACCOUNT_UPDATE:", false, true);
                Common.Utils.Concat(ref output, "User", userId, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "AccountId", Request.AccountId, false, false);
                Common.Utils.Concat(ref output, "CityId", Request.CityId, false, false);
                Common.Utils.Concat(ref output, "HolderName", Request.HolderName, false, false);
                Common.Utils.Concat(ref output, "Locale", Request.Locale, false, false);
                Common.Utils.Concat(ref output, "StatusId", Request.StatusId, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        public CallLogResponse CallLogSearch(CallLogRequest Request)
        {
            String details = String.Empty, ip = String.Empty, userId = String.Empty;
            CallLogResponse response = new CallLogResponse();
            ValidateResponse validateResponse = null;
            StringBuilder output = new StringBuilder();

            try
            {
                ip = this.GetIp();

                if (String.IsNullOrEmpty(Request.SessionId))
                {
                    response.Code = Common.Utils.SESSION_REQUIRE_CODE;
                    response.Message = Common.Utils.SESSION_REQUIRE_MESSAGE;

                    throw new Exception(response.Message);
                }

                using (SessionDao dao = new SessionDao())
                {
                    validateResponse = dao.Validate(Request.SessionId, ip, null);

                    if (!validateResponse.Code.Equals(Common.Utils.DB_SUCCESS))
                    {
                        response.Code = validateResponse.Code;
                        response.Message = validateResponse.Message;

                        throw new Exception(response.Message);
                    }

                    userId = validateResponse.UserId;
                }

                using (CallLogDao dao = new CallLogDao())
                {
                    response = dao.Search(Request);
                }
            }
            catch (Exception ex)
            {
                details = ex.Message;

                if (String.IsNullOrEmpty(response.Code))
                {
                    response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                    response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                }
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "CALL_LOG_SEARCH:", false, true);
                Common.Utils.Concat(ref output, "User", userId, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "AccountId", Request.AccountId, false, false);
                Common.Utils.Concat(ref output, "CreateDateBegin", Request.CreatedDateBegin, false, false);
                Common.Utils.Concat(ref output, "CreateDateEnd", Request.CreatedDateEnd, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }


        public CountryResponse CountrySearch(CountryRequest Request)
        {
            String details = String.Empty, ip = String.Empty, userId = String.Empty;
            CountryResponse response = new CountryResponse();
            ValidateResponse validateResponse = null;
            StringBuilder output = new StringBuilder();

            try
            {
                ip = this.GetIp();

                if (String.IsNullOrEmpty(Request.SessionId))
                {
                    response.Code = Common.Utils.SESSION_REQUIRE_CODE;
                    response.Message = Common.Utils.SESSION_REQUIRE_MESSAGE;

                    throw new Exception(response.Message);
                }

                using (SessionDao dao = new SessionDao())
                {
                    validateResponse = dao.Validate(Request.SessionId, ip, null);

                    if (!validateResponse.Code.Equals(Common.Utils.DB_SUCCESS))
                    {
                        response.Code = validateResponse.Code;
                        response.Message = validateResponse.Message;

                        throw new Exception(response.Message);
                    }

                    userId = validateResponse.UserId;
                }

                using (CountryDao dao = new CountryDao())
                {
                    response = dao.Search(Request);
                }
            }
            catch (Exception ex)
            {
                details = ex.Message;

                if (String.IsNullOrEmpty(response.Code))
                {
                    response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                    response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                }
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "COUNTRY_SEARCH:", false, true);
                Common.Utils.Concat(ref output, "User", userId, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }


        public StateResponse StateSearch(StateRequest Request)
        {
            String details = String.Empty, ip = String.Empty, userId = String.Empty;
            StateResponse response = new StateResponse();
            ValidateResponse validateResponse = null;
            StringBuilder output = new StringBuilder();

            try
            {
                ip = this.GetIp();

                if (String.IsNullOrEmpty(Request.SessionId))
                {
                    response.Code = Common.Utils.SESSION_REQUIRE_CODE;
                    response.Message = Common.Utils.SESSION_REQUIRE_MESSAGE;

                    throw new Exception(response.Message);
                }

                using (SessionDao dao = new SessionDao())
                {
                    validateResponse = dao.Validate(Request.SessionId, ip, null);

                    if (!validateResponse.Code.Equals(Common.Utils.DB_SUCCESS))
                    {
                        response.Code = validateResponse.Code;
                        response.Message = validateResponse.Message;

                        throw new Exception(response.Message);
                    }

                    userId = validateResponse.UserId;
                }

                using (StateDao dao = new StateDao())
                {
                    response = dao.Search(Request);
                }
            }
            catch (Exception ex)
            {
                details = ex.Message;

                if (String.IsNullOrEmpty(response.Code))
                {
                    response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                    response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                }
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "STATE_SEARCH:", false, true);
                Common.Utils.Concat(ref output, "User", userId, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        public CityResponse CitySearch(CityRequest Request)
        {
            String details = String.Empty, ip = String.Empty, userId = String.Empty;
            CityResponse response = new CityResponse();
            ValidateResponse validateResponse = null;
            StringBuilder output = new StringBuilder();

            try
            {
                ip = this.GetIp();

                if (String.IsNullOrEmpty(Request.SessionId))
                {
                    response.Code = Common.Utils.SESSION_REQUIRE_CODE;
                    response.Message = Common.Utils.SESSION_REQUIRE_MESSAGE;

                    throw new Exception(response.Message);
                }

                using (SessionDao dao = new SessionDao())
                {
                    validateResponse = dao.Validate(Request.SessionId, ip, null);

                    if (!validateResponse.Code.Equals(Common.Utils.DB_SUCCESS))
                    {
                        response.Code = validateResponse.Code;
                        response.Message = validateResponse.Message;

                        throw new Exception(response.Message);
                    }

                    userId = validateResponse.UserId;
                }

                using (CityDao dao = new CityDao())
                {
                    response = dao.Search(Request);
                }
            }
            catch (Exception ex)
            {
                details = ex.Message;

                if (String.IsNullOrEmpty(response.Code))
                {
                    response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                    response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                }
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "CITY_SEARCH:", false, true);
                Common.Utils.Concat(ref output, "User", userId, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return response;
        } 
    }
}
