﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using PL.App_Code;

namespace PL
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface Iapp
    {

        [OperationContract]
        LoginResponse Login(LoginRequest Request);

        [OperationContract]
        Response Logout(Request Request);

        [OperationContract]
        BuyResponse Buy(BuyRequest Request);

        [OperationContract]
        VoidResponse Void(VoidRequest Request);

        [OperationContract]
        Response SubscriberAdd(SubscriberInsertRequest Request);

        [OperationContract]
        Response SubscriberRemove(SubscriberDeleteRequest Request);

        [OperationContract]
        TransferResponse TransferBalance(TransferRequest Request);

        [OperationContract]
        AccountSearchResponse AccountSearch(AccountSearchRequest Request);

        [OperationContract]
        SpeedDialSearchResponse SpeedDialSearch(SpeedDialSearchRequest Request);

        [OperationContract]
        SubscriberSearchResponse SubscriberSearch(SubscriberSearchRequest Request);

        [OperationContract]
        ProductRateSearchResponse PoductRateSearch(ProductRateSearchRequest Request);

        [OperationContract]
        TransLogSearchResponse TransLogSearch(TransLogSearchRequest Request);

        [OperationContract]
        AccessNumberSearchResponse AccessNumberSearch(AccessNumberSearchRequest Request);

        [OperationContract]
        Response SpeedDialChange(SpeedDialChangeRequest Request);

        [OperationContract]
        Response AccountUpdate(AccountUpdateRequest Request);

        [OperationContract]
        CallLogResponse CallLogSearch(CallLogRequest Request);

        [OperationContract]
        CountryResponse CountrySearch(CountryRequest Request);

        [OperationContract]
        StateResponse StateSearch(StateRequest Request);

        [OperationContract]
        CityResponse CitySearch(CityRequest Request);

        // TODO: Add your service operations here
    }
}
