﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Text;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;

namespace PL.App_Code
{
    [DataContract]
    public class StateResponse : Response
    {
        [DataMember]
        public List<State> State { get; set; }
    }

    [DataContract]
    public class StateRequest : Request
    {
        [DataMember]
        public String Id { get; set; }
    }


    [DataContract]
    public class State
    {
        [DataMember]
        public String Id { get; set; }

        [DataMember]
        public String Description { get; set; }
    }

    public class StateDao : IDisposable
    {
        public StateResponse Search(StateRequest Request)
        {
            StateResponse response = new StateResponse();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt = null;

            try
            {
                query.Append(" select state_id, description ");
                query.Append(" from pl_vw_state ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "state_id", Request.Id, OracleDbType.Varchar2);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), String.Empty, String.Empty);

                if (dt != null && dt.Rows.Count > 0)
                {
                    response.State = new List<State>();

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        State co = new State();

                        co.Id = dt.Rows[i]["state_id"].ToString();
                        co.Description = Common.Utils.ObjectToString(dt.Rows[i]["description"]);

                        response.State.Add(co);
                    }

                    response.Code = Common.Utils.DB_SUCCESS;
                    response.Message = Common.Utils.SUCCESS_MESSAGE;
                }
                else
                {
                    response.Code = Common.Utils.NO_RECORDS_FOUND_CODE;
                    response.Message = Common.Utils.NO_RECORDS_FOUND_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                response.Comments = ex.Message;
            }

            return response;
        }

        public void Dispose()
        {

        }
    }
}