﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Text;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;

namespace PL.App_Code
{
    
    [DataContract]
    public class ProductRateSearchRequest : Request
    {
        [DataMember]
        public int? ProductId { get; set; }

        [DataMember]
        public String CountryCode { get; set; }

        [DataMember]
        public String Description { get; set; }
    }

    [DataContract]
    public class ProductRateSearchResponse : Response
    {
        [DataMember]
        public List<ProductRateInfo> ProductRateInfo { get; set; }
     }

    [DataContract]
    public class ProductRateInfo
    {
        [DataMember]
        public int? Digit { get; set; }

        [DataMember]
        public int? ProductId { get; set; }

        [DataMember]
        public String ProductName { get; set; }

        [DataMember]
        public double Rate { get; set; }

        [DataMember]
        public String Description { get; set; }

        [DataMember]
        public String CountryCode { get; set; }
    }

    public class ProductRateDao : IDisposable
    {
        public ProductRateSearchResponse Search(ProductRateSearchRequest request)
        {
            ProductRateSearchResponse response = new ProductRateSearchResponse();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt = null;

            try
            {
                query.Append(" select distinct rate, description ");
                query.Append(" from PL_VW_PRODUCT_RATE");

                if (!String.IsNullOrEmpty(request.CountryCode) && !String.IsNullOrEmpty(request.Description))
                {
                    query.Append(" where (country_code = :country_code or description like :description) ");
                    
                    cmd.Parameters.Add(":country_code", request.CountryCode);
                    cmd.Parameters.Add(":description", "%" + request.Description.ToUpper() + "%");
                }

                DataManager.AddBindParamCmd(ref cmd, ref query, "product_id", request.ProductId, OracleDbType.Int64);
                
                dt = DataManager.GetDataTable(ref cmd, query.ToString(), String.Empty, String.Empty);

                if (dt != null && dt.Rows.Count > 0)
                {
                    response.ProductRateInfo = new List<ProductRateInfo>();

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        ProductRateInfo co = new ProductRateInfo();

                        co.CountryCode = String.Empty;
                        co.Description = dt.Rows[i]["description"].ToString();
                        co.ProductId = null;
                        co.ProductName = String.Empty;
                        co.Digit = null;
                        co.Rate = Convert.ToDouble(dt.Rows[i]["rate"]);
                        
                        response.ProductRateInfo.Add(co);
                    }

                    response.Code = Common.Utils.DB_SUCCESS;
                    response.Message = Common.Utils.SUCCESS_MESSAGE;
                }
                else
                {
                    response.Code = Common.Utils.NO_RECORDS_FOUND_CODE;
                    response.Message = Common.Utils.NO_RECORDS_FOUND_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                response.Comments = ex.Message;
            }

            return response;
        }

        public void Dispose()
        {

        }
    }
}