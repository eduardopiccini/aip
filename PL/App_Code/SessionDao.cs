﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;

namespace PL.App_Code
{
    [DataContract]
    public class Request
    {
        [DataMember]
        public String SessionId { get; set; }
    }

    [DataContract]
    public class Response
    {
        [DataMember]
        public String Code { get; set; }

        [DataMember]
        public String Message { get; set; }

        [DataMember]
        public String Comments { get; set; }
    }

    [DataContract]
    public class LoginRequest
    {
        [DataMember]
        public String UserName { get; set; }

        [DataMember]
        public String Password { get; set; }
    }

    [DataContract]
    public class LoginResponse : Response
    {
        [DataMember]
        public String SessionId { get; set; }
    }

    public class ValidateResponse : Response
    {
        public String UserId { get; set; }
    }


    public class SessionDao : IDisposable
    {
        public LoginResponse Login(LoginRequest request, String ip, String sessionId, String locale)
        {
            LoginResponse response = new LoginResponse();
            OracleCommand cmd = new OracleCommand();

            try
            {
                cmd.Parameters.Add("iz_api_user_id", OracleDbType.Varchar2, 100).Value = request.UserName;
                cmd.Parameters.Add("iz_api_password", OracleDbType.Varchar2, 100).Value = request.Password;
                cmd.Parameters.Add("iz_ipaddress", OracleDbType.Varchar2, 50).Value = ip;
                cmd.Parameters.Add("iz_session_id", OracleDbType.Varchar2, 200).Value = sessionId;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int64).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 3000).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 200).Value = locale;

                DataManager.RunDataCommandSP(ref cmd, "pl_pg_session_ws.login", String.Empty, String.Empty);

                response.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                response.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (response.Code == Common.Utils.DB_SUCCESS)
                {
                    response.SessionId = sessionId;
                    response.Message = Common.Utils.SUCCESS_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                response.Comments = ex.Message;
            }

            return response;
        }

        public Response Logout(Request request, String locale)
        {
            Response response = new Response();
            OracleCommand cmd = new OracleCommand();

            try
            {
                cmd.Parameters.Add("iz_session_id", OracleDbType.Varchar2, 200).Value = request.SessionId;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int64).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 3000).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 200).Value = locale;

                DataManager.RunDataCommandSP(ref cmd, "pl_pg_session_ws.logout", String.Empty, String.Empty);

                response.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                response.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (response.Code == Common.Utils.DB_SUCCESS)
                {
                    response.Message = Common.Utils.SUCCESS_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                response.Comments = ex.Message;
            }

            return response;
        }

        public ValidateResponse Validate(String sessionId, String ip, String locale)
        {
            ValidateResponse response = new ValidateResponse();
            OracleCommand cmd = new OracleCommand();

            try
            {
                cmd.Parameters.Add("iz_session_id", OracleDbType.Varchar2, 200).Value = sessionId;
                cmd.Parameters.Add("iz_ipaddress", OracleDbType.Varchar2, 50).Value = ip;
                cmd.Parameters.Add("oz_user_id", OracleDbType.Varchar2, 3000).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int64).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 3000).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 200).Value = locale;

                DataManager.RunDataCommandSP(ref cmd, "pl_pg_session_ws.session_validate ", String.Empty, String.Empty);

                response.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                response.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (response.Code == Common.Utils.DB_SUCCESS)
                {
                    response.UserId = cmd.Parameters["oz_user_id"].Value.ToString();
                    response.Message = Common.Utils.SUCCESS_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                response.Comments = ex.Message;
            }

            return response;
        }

        public void Dispose()
        {

        }
    }
}