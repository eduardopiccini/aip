﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Text;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;

namespace PL.App_Code
{
    [DataContract]
    public class CountryResponse : Response
    {
        [DataMember]
        public List<Country> Country { get; set; }
    }

    [DataContract]
    public class CountryRequest : Request
    {
        [DataMember]
        public String Id { get; set; }

        [DataMember]
        public String IsoCode { get; set; }
    }


    [DataContract]
    public class Country
    {
        [DataMember]
        public String Id { get; set; }

        [DataMember]
        public String Description { get; set; }

        [DataMember]
        public String IsoCode { get; set; }
    }

    public class CountryDao : IDisposable
    {
        public CountryResponse Search(CountryRequest Request)
        {
            CountryResponse response = new CountryResponse();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt = null;

            try
            {
                query.Append(" select country_id, description, iso_code ");
                query.Append(" from pl_vw_country ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "country_id", Request.Id, OracleDbType.Varchar2);
                DataManager.AddBindParamCmd(ref cmd, ref query, "iso_code", Request.IsoCode, OracleDbType.Varchar2);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), String.Empty, String.Empty);

                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Country = new List<Country>();

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Country co = new Country();

                        co.Id = dt.Rows[i]["country_id"].ToString();
                        co.Description = Common.Utils.ObjectToString(dt.Rows[i]["description"]);
                        co.IsoCode = Common.Utils.ObjectToString(dt.Rows[i]["iso_code"]);

                        response.Country.Add(co);
                    }

                    response.Code = Common.Utils.DB_SUCCESS;
                    response.Message = Common.Utils.SUCCESS_MESSAGE;
                }
                else
                {
                    response.Code = Common.Utils.NO_RECORDS_FOUND_CODE;
                    response.Message = Common.Utils.NO_RECORDS_FOUND_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                response.Comments = ex.Message;
            }

            return response;
        }

        public void Dispose()
        {

        }
    }
}