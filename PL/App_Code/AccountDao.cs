﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using System.Text;

namespace PL.App_Code
{

    [DataContract]
    public class BuyRequest : Request
    {
        [DataMember]
        public String ProductId { get; set; }

        [DataMember]
        public long AccountNumber { get; set; }

        [DataMember]
        public long Reference { get; set; }

        [DataMember]
        public int StoreId { get; set; }

        [DataMember]
        public String StoreName { get; set; }

        [DataMember]
        public int CityId { get; set; }

        [DataMember]
        public double TransAmount { get; set; }

        [DataMember]
        public int DollarFree { get; set; }

        [DataMember]
        public String AccountHolder { get; set; }

        [DataMember]
        public String LocaleId { get; set; }

        [DataMember]
        public int PhoneNotification { get; set; }

        [DataMember]
        public String RoutingId { get; set; }

        [DataMember]
        public String Locale { get; set; }

        [DataMember]
        public String DbUser { get; set; }
    }

    [DataContract]
    public class BuyResponse : Response
    {
        [DataMember]
        public long? TransReference { get; set; }

        [DataMember]
        public long? AccountId { get; set; }

        [DataMember]
        public int? DollarFree { get; set; }
    }

    [DataContract]
    public class VoidRequest : Request
    {
        [DataMember]
        public long Reference { get; set; }

        [DataMember]
        public int StoreId { get; set; }

        [DataMember]
        public String RoutingId { get; set; }

        [DataMember]
        public String Locale { get; set; }

        [DataMember]
        public String DbUser { get; set; }
    }

    [DataContract]
    public class SubscriberInsertRequest : Request
    {
        [DataMember]
        public long AccountId { get; set; }

        [DataMember]
        public long Subscriber { get; set; }

        [DataMember]
        public int ProductId { get; set; }
    }

    [DataContract]
    public class SubscriberDeleteRequest : Request
    {
        [DataMember]
        public long AccountId { get; set; }

        [DataMember]
        public long Subscriber { get; set; }
    }

    [DataContract]
    public class TransferRequest : Request
    {
        [DataMember]
        public long AccountIdFrom { get; set; }

        [DataMember]
        public long AccountIdTo { get; set; }

        [DataMember]
        public double TransAmount { get; set; }
    }

    [DataContract]
    public class TransferResponse : Response
    {
        [DataMember]
        public long TransReference { get; set; }
    }

    [DataContract]
    public class VoidResponse : Response
    {
        [DataMember]
        public long TransReference { get; set; }
    }

    [DataContract]
    public class AccountInfo
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long Number { get; set; }

        [DataMember]
        public String HolderName { get; set; }

        [DataMember]
        public int ProductId { get; set; }

        [DataMember]
        public String ProductName { get; set; }

        [DataMember]
        public double Balance { get; set; }

        [DataMember]
        public String CurrencyId { get; set; }

        [DataMember]
        public String CurrencyName { get; set; }

        [DataMember]
        public String LocaleId { get; set; }

        [DataMember]
        public String LocaleName { get; set; }

        [DataMember]
        public int StatusId { get; set; }

        [DataMember]
        public String StatusName { get; set; }

        [DataMember]
        public String CityId { get; set; }

        [DataMember]
        public String CityName { get; set; }

        [DataMember]
        public String StateId { get; set; }
    }

    [DataContract]
    public class SpeedDialInfo
    {
        [DataMember]
        public long Did { get; set; }

        [DataMember]
        public long AccountId { get; set; }

        [DataMember]
        public String Destination { get; set; }

        [DataMember]
        public String Description { get; set; }
    }

    [DataContract]
    public class SubscriberInfo
    {
        [DataMember]
        public long Subscriber { get; set; }

        [DataMember]
        public int ProductId { get; set; }

        [DataMember]
        public String ProductName { get; set; }

        [DataMember]
        public long AccountId { get; set; }
    }

    [DataContract]
    public class AccountSearchRequest : Request
    {
        [DataMember]
        public long? AccountId { get; set; }

        [DataMember]
        public long? AccountNumber { get; set; }
    }

    [DataContract]
    public class AccountSearchResponse : Response
    {
        [DataMember]
        public List<AccountInfo> AccountInfo { get; set; }
    }

    [DataContract]
    public class SpeedDialSearchRequest : Request
    {
        [DataMember]
        public long AccountId { get; set; }
    }

    [DataContract]
    public class SpeedDialSearchResponse : Response
    {
        [DataMember]
        public List<SpeedDialInfo> SpeedDialInfo { get; set; }
    }

    [DataContract]
    public class SubscriberSearchRequest : Request
    {
        [DataMember]
        public long AccountId { get; set; }
    }

    [DataContract]
    public class SubscriberSearchResponse : Response
    {
        [DataMember]
        public List<SubscriberInfo> SubscriberInfo { get; set; }
    }

    [DataContract]
    public class SpeedDial
    {
        [DataMember]
        public Int64 Did { get; set; }

        [DataMember]
        public String Destination { get; set; }

        [DataMember]
        public String Description { get; set; }
    }

    [DataContract]
    public class SpeedDialChangeRequest : Request
    {
        [DataMember]
        public List<SpeedDial> SpeedDial { get; set; }

        [DataMember]
        public long AccountId { get; set; }
    }

    [DataContract]
    public class AccountUpdateRequest : Request
    {
        [DataMember]
        public long AccountId { get; set; }

        [DataMember]
        public String HolderName { get; set; }

        [DataMember]
        public String Locale { get; set; }

        [DataMember]
        public int StatusId { get; set; }

        [DataMember]
        public String StateId { get; set; }

        [DataMember]
        public int? CityId { get; set; }
    }


    public class AccountDao : IDisposable
    {
        public BuyResponse Buy(BuyRequest request, String userId)
        {
            BuyResponse response = new BuyResponse();
            OracleCommand cmd = new OracleCommand();

            try
            {
                cmd.Parameters.Add("in_product_id", OracleDbType.Int32).Value = request.ProductId;
                cmd.Parameters.Add("in_account_number", OracleDbType.Int64).Value = request.AccountNumber;
                cmd.Parameters.Add("in_reference", OracleDbType.Int64).Value = request.Reference;
                cmd.Parameters.Add("in_store_id", OracleDbType.Int32).Value = request.StoreId;
                cmd.Parameters.Add("iz_store_name", OracleDbType.Varchar2, 200).Value = request.StoreName;
                cmd.Parameters.Add("in_city_id", OracleDbType.Int32).Value = request.CityId;
                cmd.Parameters.Add("in_trans_amount", OracleDbType.Double).Value = request.TransAmount;
                cmd.Parameters.Add("in_dollar_free", OracleDbType.Int32).Value = request.DollarFree;
                cmd.Parameters.Add("iz_user_id", OracleDbType.Varchar2, 200).Value = userId;
                cmd.Parameters.Add("iz_account_holder", OracleDbType.Varchar2, 200).Value = request.AccountHolder;
                cmd.Parameters.Add("iz_locale_id", OracleDbType.Varchar2, 200).Value = request.LocaleId;
                cmd.Parameters.Add("in_phone_notification", OracleDbType.Int32).Value = request.PhoneNotification;
                cmd.Parameters.Add("on_trans_reference", OracleDbType.Int64).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_account_id", OracleDbType.Int64).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_dollar_free", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_response_code", OracleDbType.Varchar2, 20).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_response_message", OracleDbType.Varchar2, 3000).Direction = ParameterDirection.Output;

                DataManager.RunDataCommandSP(ref cmd, "PL_PG_ACCOUNT.buy", String.Empty, String.Empty);

                response.Code = cmd.Parameters["oz_response_code"].Value.ToString();
                response.Message = cmd.Parameters["oz_response_message"].Value.ToString();

                if (response.Code == Common.Utils.DB_SUCCESS || response.Code == "00")
                {
                    response.Code = "00";
                    response.Message = Common.Utils.SUCCESS_MESSAGE;
                    response.AccountId = Convert.ToInt64(cmd.Parameters["on_account_id"].Value.ToString());
                    response.TransReference = Convert.ToInt64(cmd.Parameters["on_trans_reference"].Value.ToString());
                    response.DollarFree = Convert.ToInt32(cmd.Parameters["on_dollar_free"].Value.ToString());
                    
                }
                else
                {
                    response.AccountId = null;
                    response.TransReference = null;
                    response.DollarFree = null;
                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                response.Comments = ex.Message;
            }

            return response;
        }

        public VoidResponse Void(VoidRequest request, String userId)
        {
            VoidResponse response = new VoidResponse();
            OracleCommand cmd = new OracleCommand();

            try
            {
                cmd.Parameters.Add("in_reference", OracleDbType.Int64).Value = request.Reference;
                cmd.Parameters.Add("in_store_id", OracleDbType.Int32).Value = request.StoreId;
                cmd.Parameters.Add("iz_user_id", OracleDbType.Varchar2, 200).Value = userId;
                cmd.Parameters.Add("on_trans_reference", OracleDbType.Int64).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_response_code", OracleDbType.Varchar2, 20).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_response_message", OracleDbType.Varchar2, 3000).Direction = ParameterDirection.Output;

                DataManager.RunDataCommandSP(ref cmd, "PL_PG_ACCOUNT.void", String.Empty, String.Empty);

                response.Code = cmd.Parameters["oz_response_code"].Value.ToString();
                response.Message = cmd.Parameters["oz_response_message"].Value.ToString();

                if (response.Code == Common.Utils.DB_SUCCESS)
                {
                    response.Message = "00";
                    response.Message = Common.Utils.SUCCESS_MESSAGE;
                    response.TransReference = Convert.ToInt64(cmd.Parameters["on_trans_reference"].Value.ToString());

                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                response.Comments = ex.Message;
            }

            return response;
        }

        public Response SubscriberInsert(SubscriberInsertRequest request, String userId, String locale)
        {
            Response response = new Response();
            OracleCommand cmd = new OracleCommand();

            try
            {
                cmd.Parameters.Add("in_account_id", OracleDbType.Int64).Value = request.AccountId;
                cmd.Parameters.Add("in_product_id", OracleDbType.Int32).Value = request.ProductId;
                cmd.Parameters.Add("in_subscriber", OracleDbType.Int64).Value = request.Subscriber;
                cmd.Parameters.Add("iz_user_id", OracleDbType.Varchar2, 200).Value = userId;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int64).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 3000).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 10).Value = locale;


                DataManager.RunDataCommandSP(ref cmd, "PL_PG_ACCOUNT.subscriber_insert", String.Empty, String.Empty);

                response.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                response.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (response.Code == Common.Utils.DB_SUCCESS)
                {
                    response.Message = Common.Utils.SUCCESS_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                response.Comments = ex.Message;
            }

            return response;
        }

        public Response SubscriberDelete(SubscriberDeleteRequest request, String userId, String locale)
        {
            Response response = new Response();
            OracleCommand cmd = new OracleCommand();

            try
            {
                cmd.Parameters.Add("in_account_id", OracleDbType.Int64).Value = request.AccountId;
                cmd.Parameters.Add("in_subscriber", OracleDbType.Int64).Value = request.Subscriber;
                cmd.Parameters.Add("iz_user_id", OracleDbType.Varchar2, 200).Value = userId;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int64).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 3000).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 10).Value = locale;

                DataManager.RunDataCommandSP(ref cmd, "PL_PG_ACCOUNT.subscriber_delete", String.Empty, String.Empty);

                response.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                response.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (response.Code == Common.Utils.DB_SUCCESS)
                {
                    response.Message = Common.Utils.SUCCESS_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                response.Comments = ex.Message;
            }

            return response;
        }

        public TransferResponse Transfer(TransferRequest request, String userId, String locale)
        {
            TransferResponse response = new TransferResponse();
            OracleCommand cmd = new OracleCommand();

            try
            {
                cmd.Parameters.Add("in_account_id_from", OracleDbType.Int64).Value = request.AccountIdFrom;
                cmd.Parameters.Add("in_account_id_to", OracleDbType.Int64).Value = request.AccountIdTo;
                cmd.Parameters.Add("in_trans_amount", OracleDbType.Decimal).Value = request.TransAmount;
                cmd.Parameters.Add("iz_user_id", OracleDbType.Varchar2, 200).Value = userId;
                cmd.Parameters.Add("iz_note", OracleDbType.Varchar2, 200).Value = String.Empty;
                cmd.Parameters.Add("on_trans_reference", OracleDbType.Int64).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int64).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 3000).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 10).Value = locale;

                DataManager.RunDataCommandSP(ref cmd, "PL_PG_ACCOUNT.transfer", String.Empty, String.Empty);

                response.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                response.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (response.Code == Common.Utils.DB_SUCCESS)
                {
                    response.Message = Common.Utils.SUCCESS_MESSAGE;
                    response.TransReference = Convert.ToInt64(cmd.Parameters["on_trans_reference"].Value.ToString());

                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                response.Comments = ex.Message;
            }

            return response;
        }

        public AccountSearchResponse Search(AccountSearchRequest request)
        {
            AccountSearchResponse response = new AccountSearchResponse();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt = null;

            try
            {
                query.Append(" select account_id, account_number, account_holder, product_id, product_name, ");
                query.Append(" balance, currency_id, currency_name, locale_id, locale_name, status_id, ");
                query.Append(" status_name, city_id, city_name, state_id ");
                query.Append(" from PL_VW_ACCOUNT ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "account_id", request.AccountId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "account_number", request.AccountNumber, OracleDbType.Int64);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), String.Empty, String.Empty);

                if (dt != null && dt.Rows.Count > 0)
                {
                    response.AccountInfo = new List<AccountInfo>();


                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        AccountInfo co = new AccountInfo();

                        co.Id = Convert.ToInt64(dt.Rows[i]["account_id"].ToString());
                        co.Number = Convert.ToInt64(dt.Rows[i]["account_number"].ToString());
                        co.ProductId = Convert.ToInt32(dt.Rows[i]["product_id"].ToString());
                        co.HolderName = dt.Rows[i]["account_holder"].ToString();
                        co.ProductName = dt.Rows[i]["product_name"].ToString();
                        co.Balance = Convert.ToDouble(dt.Rows[i]["balance"].ToString());
                        co.CurrencyId = dt.Rows[i]["currency_id"].ToString();
                        co.CurrencyName = dt.Rows[i]["currency_name"].ToString();
                        co.LocaleId = dt.Rows[i]["locale_id"].ToString();
                        co.LocaleName = dt.Rows[i]["locale_name"].ToString();
                        co.StatusId = Convert.ToInt32(dt.Rows[i]["status_id"].ToString());
                        co.StatusName = dt.Rows[i]["status_name"].ToString();
                        co.CityId = dt.Rows[i]["city_id"].ToString() == null ? null : dt.Rows[i]["city_id"].ToString();
                        co.CityName = dt.Rows[i]["city_name"].ToString();
                        co.StateId = dt.Rows[i]["state_id"].ToString();

                        response.AccountInfo.Add(co);
                    }

                    response.Code = Common.Utils.DB_SUCCESS;
                    response.Message = Common.Utils.SUCCESS_MESSAGE;
                }
                else
                {
                    response.Code = Common.Utils.NO_RECORDS_FOUND_CODE;
                    response.Message = Common.Utils.NO_RECORDS_FOUND_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                response.Comments = ex.Message;
            }

            return response;
        }

        public SpeedDialSearchResponse SpeedDialSearch(SpeedDialSearchRequest request)
        {
            SpeedDialSearchResponse response = new SpeedDialSearchResponse();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt = null;

            try
            {
                query.Append(" select did, account_id, destination, description ");
                query.Append(" from PL_VW_SPEED_DIAL ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "account_id", request.AccountId, OracleDbType.Int64);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), String.Empty, String.Empty);

                if (dt != null && dt.Rows.Count > 0)
                {
                    response.SpeedDialInfo = new List<SpeedDialInfo>();

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SpeedDialInfo co = new SpeedDialInfo();

                        co.Did = Convert.ToInt64(dt.Rows[i]["did"].ToString());
                        co.AccountId = Convert.ToInt64(dt.Rows[i]["account_id"].ToString());
                        co.Destination = dt.Rows[i]["destination"].ToString();
                        co.Description = dt.Rows[i]["description"].ToString();

                        response.SpeedDialInfo.Add(co);
                    }

                    response.Code = Common.Utils.DB_SUCCESS;
                    response.Message = Common.Utils.SUCCESS_MESSAGE;
                }
                else
                {
                    response.Code = Common.Utils.NO_RECORDS_FOUND_CODE;
                    response.Message = Common.Utils.NO_RECORDS_FOUND_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                response.Comments = ex.Message;
            }

            return response;
        }

        public SubscriberSearchResponse SubscriberSearch(SubscriberSearchRequest request)
        {
            SubscriberSearchResponse response = new SubscriberSearchResponse();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt = null;

            try
            {
                query.Append(" select subscriber, account_id, product_id, product_name ");
                query.Append(" from PL_VW_SUBSCRIBER ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "account_id", request.AccountId, OracleDbType.Int64);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), String.Empty, String.Empty);

                if (dt != null && dt.Rows.Count > 0)
                {
                    response.SubscriberInfo = new List<SubscriberInfo>();

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SubscriberInfo co = new SubscriberInfo();

                        co.Subscriber = Convert.ToInt64(dt.Rows[i]["subscriber"].ToString());
                        co.AccountId = Convert.ToInt64(dt.Rows[i]["account_id"].ToString());
                        co.ProductId = Convert.ToInt32(dt.Rows[i]["product_id"].ToString());
                        co.ProductName = dt.Rows[i]["product_name"].ToString();

                        response.SubscriberInfo.Add(co);
                    }

                    response.Code = Common.Utils.DB_SUCCESS;
                    response.Message = Common.Utils.SUCCESS_MESSAGE;
                }
                else
                {
                    response.Code = Common.Utils.NO_RECORDS_FOUND_CODE;
                    response.Message = Common.Utils.NO_RECORDS_FOUND_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                response.Comments = ex.Message;
            }

            return response;
        }

        public Response SpeedDialChange(SpeedDialChangeRequest request, String userId, String locale)
        {
            Response response = new Response();
            OracleCommand cmd = new OracleCommand();
            List<long> did = new List<long>();
            List<string> destination = new List<string>();
            List<string> description = new List<string>();

            try
            {
                if (request.SpeedDial != null)
                {
                    for (int i = 0; i < request.SpeedDial.Count; i++)
                    {
                        did.Add(request.SpeedDial[i].Did);
                        destination.Add(request.SpeedDial[i].Destination);
                        description.Add(request.SpeedDial[i].Description);
                    }
                }

                cmd.Parameters.Add("in_account_id", OracleDbType.Int64).Value = request.AccountId;
                cmd.Parameters.Add(DataManager.CrearOraParametroTable("in_did", OracleDbType.Int64, 1000, did.ToArray(), ParameterDirection.Input));
                cmd.Parameters.Add(DataManager.CrearOraParametroTable("iz_destination", OracleDbType.Varchar2, 1000, destination.ToArray(), ParameterDirection.Input, 200));
                cmd.Parameters.Add(DataManager.CrearOraParametroTable("iz_description", OracleDbType.Varchar2, 1000, description.ToArray(), ParameterDirection.Input, 200));
                cmd.Parameters.Add("iz_user_id", OracleDbType.Varchar2, 200).Value = userId;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int64).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 3000).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 10).Value = locale;

                DataManager.RunDataCommandSP(ref cmd, "PL_PG_ACCOUNT.speed_dial_change", String.Empty, String.Empty);

                response.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                response.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (response.Code == Common.Utils.DB_SUCCESS)
                {
                    response.Message = Common.Utils.SUCCESS_MESSAGE;

                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                response.Comments = ex.Message;
            }

            return response;
        }

        public Response UpdateAccount(AccountUpdateRequest Request, String userId, String locale)
        {
            Response response = new Response();
            OracleCommand cmd = new OracleCommand();

            try
            {
                cmd.Parameters.Add("in_account_id", OracleDbType.Int64).Value = Request.AccountId;
                cmd.Parameters.Add("iz_account_holder", OracleDbType.Varchar2, 100).Value = Request.HolderName;
                cmd.Parameters.Add("iz_locale_id", OracleDbType.Varchar2, 20).Value = Request.Locale;
                cmd.Parameters.Add("in_state_id", OracleDbType.Varchar2, 20).Value = Request.StateId;
                cmd.Parameters.Add("in_city_id", OracleDbType.Int64).Value = Request.CityId;
                cmd.Parameters.Add("iz_user_id", OracleDbType.Varchar2, 200).Value = userId;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int64).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 3000).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 10).Value = locale;


                DataManager.RunDataCommandSP(ref cmd, "PL_PG_ACCOUNT.account_update", String.Empty, String.Empty);

                response.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                response.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (response.Code == Common.Utils.DB_SUCCESS)
                {
                    response.Message = Common.Utils.SUCCESS_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                response.Comments = ex.Message;
            }

            return response;
        }

        public void Dispose()
        {

        }
    }
}