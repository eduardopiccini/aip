﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Text;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;

namespace PL.App_Code
{
    //access_number, product_id, product_name, state_id, state_name, city_i, city_name

    [DataContract]
    public class AccessNumberSearchRequest : Request
    {
        [DataMember]
        public int? ProductId { get; set; }

        [DataMember]
        public String StateId { get; set; }
    }

    [DataContract]
    public class AccessNumberSearchResponse : Response
    {
        [DataMember]
        public List<AccessNumberInfo> AccessNumberInfo { get; set; }
    }

    [DataContract]
    public class AccessNumberInfo
    {
        [DataMember]
        public long AccessNumber { get; set; }

        [DataMember]
        public int ProductId { get; set; }

        [DataMember]
        public String ProductName { get; set; }

        [DataMember]
        public String StateId { get; set; }

        [DataMember]
        public String StateName { get; set; }

        [DataMember]
        public int CityId { get; set; }

        [DataMember]
        public String CityName { get; set; }

    }

    public class AccessNumberDao : IDisposable
    {
        public AccessNumberSearchResponse Search(AccessNumberSearchRequest request)
        {
            AccessNumberSearchResponse response = new AccessNumberSearchResponse();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt = null;

            try
            {
                query.Append(" select access_number, product_id, product_name, state_id, state_name, city_id, city_name ");
                query.Append(" from PL_VW_ACCESS_NUMBER ");

                if (!String.IsNullOrEmpty(request.StateId))
                {
                    query.Append("where (state_id = :state_id or state_id is null)");

                    cmd.Parameters.Add(":state_id", request.StateId);
                }

                DataManager.AddBindParamCmd(ref cmd, ref query, "product_id", request.ProductId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "state_id", request.StateId, OracleDbType.Varchar2);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), String.Empty, String.Empty);

                if (dt != null && dt.Rows.Count > 0)
                {
                    response.AccessNumberInfo = new List<AccessNumberInfo>();

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        AccessNumberInfo co = new AccessNumberInfo();

                        co.AccessNumber = Convert.ToInt64(dt.Rows[i]["access_number"].ToString());
                        co.CityId = Convert.ToInt32(dt.Rows[i]["city_id"].ToString());
                        co.ProductId = Convert.ToInt32(dt.Rows[i]["product_id"].ToString());
                        co.ProductName = dt.Rows[i]["product_name"].ToString();
                        co.CityName = dt.Rows[i]["city_name"].ToString();
                        co.StateId = dt.Rows[i]["state_id"].ToString();
                        co.StateName = dt.Rows[i]["state_name"].ToString();

                        response.AccessNumberInfo.Add(co);
                    }

                    response.Code = Common.Utils.DB_SUCCESS;
                    response.Message = Common.Utils.SUCCESS_MESSAGE;
                }
                else
                {
                    response.Code = Common.Utils.NO_RECORDS_FOUND_CODE;
                    response.Message = Common.Utils.NO_RECORDS_FOUND_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                response.Comments = ex.Message;
            }

            return response;
        }

        public void Dispose()
        {

        }
    }
}