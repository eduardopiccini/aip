﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Text;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;

namespace PL.App_Code
{
    [DataContract]
    public class CityResponse : Response
    {
        [DataMember]
        public List<City> City { get; set; }
    }

    [DataContract]
    public class CityRequest : Request
    {
        [DataMember]
        public String Id { get; set; }

        [DataMember]
        public String CountryId { get; set; }

        [DataMember]
        public String StateId { get; set; }
    }


    [DataContract]
    public class City
    {
        [DataMember]
        public String Id { get; set; }

        [DataMember]
        public String Description { get; set; }
    }

    public class CityDao : IDisposable
    {
        public CityResponse Search(CityRequest Request)
        {
            CityResponse response = new CityResponse();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt = null;

            try
            {
                query.Append(" select city_id, description ");
                query.Append(" from pl_vw_city ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "state_id", Request.StateId, OracleDbType.Varchar2);
                DataManager.AddBindParamCmd(ref cmd, ref query, "country_id", Request.CountryId, OracleDbType.Varchar2);
                DataManager.AddBindParamCmd(ref cmd, ref query, "city_id", Request.Id, OracleDbType.Varchar2);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), String.Empty, String.Empty);

                if (dt != null && dt.Rows.Count > 0)
                {
                    response.City = new List<City>();

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        City co = new City();

                        co.Id = dt.Rows[i]["city_id"].ToString();
                        co.Description = Common.Utils.ObjectToString(dt.Rows[i]["description"]);

                        response.City.Add(co);
                    }

                    response.Code = Common.Utils.DB_SUCCESS;
                    response.Message = Common.Utils.SUCCESS_MESSAGE;
                }
                else
                {
                    response.Code = Common.Utils.NO_RECORDS_FOUND_CODE;
                    response.Message = Common.Utils.NO_RECORDS_FOUND_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                response.Comments = ex.Message;
            }

            return response;
        }

        public void Dispose()
        {

        }
    }
}