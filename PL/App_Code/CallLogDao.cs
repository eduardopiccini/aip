﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Text;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;

namespace PL.App_Code
{
    [DataContract]
    public class CallLog
    {
        [DataMember]
        public String Id { get; set; }

        [DataMember]
        public String AccountId { get; set; }

        [DataMember]
        public String OrganizationId { get; set; }

        [DataMember]
        public String CurrencyId { get; set; }

        [DataMember]
        public String ProductId { get; set; }

        [DataMember]
        public String ProductName { get; set; }

        [DataMember]
        public String Amount { get; set; }

        [DataMember]
        public String Balance { get; set; }

        [DataMember]
        public String OldBalance { get; set; }

        [DataMember]
        public String Subscriber { get; set; }

        [DataMember]
        public String Rate { get; set; }

        [DataMember]
        public String Did { get; set; }

        [DataMember]
        public String DialedNumber { get; set; }

        [DataMember]
        public String CallStart { get; set; }

        [DataMember]
        public String CallEnd { get; set; }

        [DataMember]
        public String CallSecond { get; set; }

        [DataMember]
        public String Locale { get; set; }

        [DataMember]
        public String CreatedDate { get; set; }

    }

    [DataContract]
    public class CallLogRequest : Request
    {
        [DataMember]
        public long AccountId { get; set; }

        [DataMember]
        public DateTime? CreatedDateBegin { get; set; }

        [DataMember]
        public DateTime? CreatedDateEnd { get; set; }
    }

    [DataContract]
    public class CallLogResponse : Response
    {
        [DataMember]
        public List<CallLog> CallLog { get; set; }
    }
    
    public class CallLogDao : IDisposable
    {

        public CallLogResponse Search(CallLogRequest Request)
        {
            CallLogResponse response = new CallLogResponse();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt = null;

            try
            {
                query.Append(" select trans_id, account_id, organization_id, currency_id, product_id, product_name, ");
                query.Append(" amount, balance, old_balance, subscriber, did, dialed_number, rate, ");
                query.Append(" call_start, call_end, call_Second, locale_id, created_date, ");
                query.Append(" created_by, inserted_by, inserted_name ");
                query.Append(" from PL_VW_call_log ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "account_id", Request.AccountId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "created_date", Request.CreatedDateBegin, Request.CreatedDateEnd, OracleDbType.Date, DataManager.eOperador.Between);

                query.Append("order by created_date desc ");

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), String.Empty, String.Empty);

                if (dt != null && dt.Rows.Count > 0)
                {
                    response.CallLog = new List<CallLog>();

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        CallLog co = new CallLog();

                        co.AccountId = dt.Rows[i]["account_id"].ToString();
                        co.Amount = Common.Utils.ObjectToString(dt.Rows[i]["amount"]);
                        co.Balance = Common.Utils.ObjectToString(dt.Rows[i]["balance"]);
                        co.CurrencyId = Common.Utils.ObjectToString(dt.Rows[i]["currency_id"]);
                        co.CallSecond = Common.Utils.ObjectToString(dt.Rows[i]["call_second"]);
                        co.CallEnd = Common.Utils.ObjectToString(dt.Rows[i]["call_end"]);
                        co.CallStart = Common.Utils.ObjectToString(dt.Rows[i]["call_start"]);
                        co.CreatedDate = Common.Utils.ObjectToString(dt.Rows[i]["created_date"]);
                        co.DialedNumber = Common.Utils.ObjectToString(dt.Rows[i]["dialed_number"]);
                        co.Did = Common.Utils.ObjectToString(dt.Rows[i]["did"]);
                        co.OldBalance = Common.Utils.ObjectToString(dt.Rows[i]["old_balance"]);
                        co.Id = Common.Utils.ObjectToString(dt.Rows[i]["trans_id"]);
                        co.Locale = Common.Utils.ObjectToString(dt.Rows[i]["locale_id"].ToString());
                        co.OrganizationId = Common.Utils.ObjectToString(dt.Rows[i]["organization_id"].ToString());
                        co.Rate = Common.Utils.ObjectToString(dt.Rows[i]["rate"].ToString());
                        co.ProductId = Common.Utils.ObjectToString(dt.Rows[i]["product_id"].ToString());
                        co.ProductName = Common.Utils.ObjectToString(dt.Rows[i]["product_name"].ToString());
                        co.Subscriber = Common.Utils.ObjectToString(dt.Rows[i]["subscriber"].ToString());

                        response.CallLog.Add(co);
                    }

                    response.Code = Common.Utils.DB_SUCCESS;
                    response.Message = Common.Utils.SUCCESS_MESSAGE;
                }
                else
                {
                    response.Code = Common.Utils.NO_RECORDS_FOUND_CODE;
                    response.Message = Common.Utils.NO_RECORDS_FOUND_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                response.Comments = ex.Message;
            }

            return response;
        }



        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}