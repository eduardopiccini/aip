﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Text;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;

namespace PL.App_Code
{
    [DataContract]
    public class TransLogInfo
    {
        [DataMember]
        public long TransId { get; set; }

        [DataMember]
        public long AccountId { get; set; }

        [DataMember]
        public int TransTypeId { get; set; }

        [DataMember]
        public String TransTypeName { get; set; }

        [DataMember]
        public long? Reference { get; set; }

        [DataMember]
        public int? OrganizarionId { get; set; }

        [DataMember]
        public String CurrencyId { get; set; }

        [DataMember]
        public int? ProductId { get; set; }

        [DataMember]
        public String ProductName { get; set; }

        [DataMember]
        public String TransDate { get; set; }

        [DataMember]
        public double? TransAmount { get; set; }

        [DataMember]
        public long? TransReference { get; set; }

        [DataMember]
        public double? Balance { get; set; }

        [DataMember]
        public double? OldBalance { get; set; }

        [DataMember]
        public int? Voided { get; set; }

        [DataMember]
        public int? StoreId { get; set; }

        [DataMember]
        public int? PhoneNotification { get; set; }

        [DataMember]
        public long? Subscriber { get; set; }

        [DataMember]
        public long? Did { get; set; }

        [DataMember]
        public long? DialedNumber { get; set; }

        [DataMember]
        public double? Rate { get; set; }

        [DataMember]
        public String CallStart { get; set; }

        [DataMember]
        public String CallEnd { get; set; }

        [DataMember]
        public long? CallSecond { get; set; }

        [DataMember]
        public String LocaleId { get; set; }

        [DataMember]
        public DateTime? CreateDate { get; set; }
    }

    [DataContract]
    public class TransLogSearchRequest : Request
    {
        [DataMember]
        public long? TransId { get; set; }

        [DataMember]
        public long? AccountId { get; set; }

        [DataMember]
        public int? TransTypeId { get; set; }

        [DataMember]
        public int? OrganizationId { get; set; }

        [DataMember]
        public String CurrencyId { get; set; }

        [DataMember]
        public int? ProductId { get; set; }

        [DataMember]
        public DateTime? TransDateBegin { get; set; }

        [DataMember]
        public DateTime? TransDateEnd { get; set; }

        [DataMember]
        public int? StoreId { get; set; }
    }


    [DataContract]
    public class TransLogSearchResponse : Response
    {
        [DataMember]
        public List<TransLogInfo> TransLogInfo { get; set; }
    }



    public class TransLogDao : IDisposable
    {
        public TransLogSearchResponse Search(TransLogSearchRequest request)
        {
            TransLogSearchResponse response = new TransLogSearchResponse();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt = null;

            try
            {
                query.Append(" select trans_id, account_id, trans_type_id, trans_type_name, reference, organization_id, ");
                query.Append(" currency_id, product_id, product_name, trans_date, trans_amount, trans_reference, ");
                query.Append(" balance, old_balance, voided, store_id,  phone_notification, subscriber, did, ");
                query.Append(" dialed_number, rate, call_start, call_end, call_second, locale_id, created_date ");
                query.Append(" from PL_VW_trans_log ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "trans_id", request.TransId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "account_id", request.AccountId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "currency_id", request.CurrencyId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "organization_id", request.OrganizationId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "product_id", request.ProductId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "store_id", request.StoreId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "trans_date", request.TransDateBegin, request.TransDateEnd, OracleDbType.Date, DataManager.eOperador.Between);
                DataManager.AddBindParamCmd(ref cmd, ref query, "trans_type_id", request.TransTypeId, OracleDbType.Int32);

                query.Append(" order by trans_date desc ");

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), String.Empty, String.Empty);

                if (dt != null && dt.Rows.Count > 0)
                {
                    response.TransLogInfo = new List<TransLogInfo>();

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        TransLogInfo co = new TransLogInfo();

                        co.AccountId = Convert.ToInt64(dt.Rows[i]["account_id"].ToString());
                        co.Balance = Common.Utils.ObjectToDouble(dt.Rows[i]["balance"]);
                        co.CallEnd = Common.Utils.ObjectToString(dt.Rows[i]["call_end"]);
                        co.CallSecond = Common.Utils.ObjectToLong(dt.Rows[i]["call_second"]);
                        co.CallStart = Common.Utils.ObjectToString(dt.Rows[i]["call_start"]);
                        co.CreateDate = Common.Utils.ObjectToDateTime(dt.Rows[i]["created_date"]);
                        co.CurrencyId = Common.Utils.ObjectToString(dt.Rows[i]["currency_id"]);
                        co.DialedNumber = Common.Utils.ObjectToLong(dt.Rows[i]["dialed_number"]);
                        co.Did = Common.Utils.ObjectToLong(dt.Rows[i]["did"]);
                        co.LocaleId = Common.Utils.ObjectToString(dt.Rows[i]["locale_id"]);
                        co.OldBalance = Common.Utils.ObjectToDouble(dt.Rows[i]["old_balance"].ToString());
                        co.OrganizarionId = Common.Utils.ObjectToInt(dt.Rows[i]["organization_id"].ToString());
                        co.PhoneNotification = Common.Utils.ObjectToInt(dt.Rows[i]["phone_notification"].ToString());
                        co.ProductId = Common.Utils.ObjectToInt(dt.Rows[i]["product_id"].ToString());
                        co.ProductName = Common.Utils.ObjectToString(dt.Rows[i]["product_name"].ToString());
                        co.Rate = Common.Utils.ObjectToDouble(dt.Rows[i]["rate"].ToString());
                        co.Reference = Common.Utils.ObjectToLong(dt.Rows[i]["reference"].ToString());
                        co.StoreId = Common.Utils.ObjectToInt(dt.Rows[i]["store_id"].ToString());
                        co.Subscriber = Common.Utils.ObjectToLong(dt.Rows[i]["subscriber"].ToString());
                        co.TransAmount = Common.Utils.ObjectToDouble(dt.Rows[i]["trans_amount"].ToString());
                        co.TransDate = Common.Utils.ObjectToString(dt.Rows[i]["trans_date"].ToString());
                        co.TransId = Convert.ToInt64(dt.Rows[i]["trans_id"].ToString());
                        co.TransReference = Common.Utils.ObjectToLong(dt.Rows[i]["trans_reference"].ToString());
                        co.TransTypeId = Convert.ToInt32(dt.Rows[i]["trans_type_id"].ToString());
                        co.TransTypeName = Common.Utils.ObjectToString(dt.Rows[i]["trans_type_name"].ToString());
                        co.Voided = Common.Utils.ObjectToInt(dt.Rows[i]["voided"].ToString());

                        response.TransLogInfo.Add(co);
                    }

                    response.Code = Common.Utils.DB_SUCCESS;
                    response.Message = Common.Utils.SUCCESS_MESSAGE;
                }
                else
                {
                    response.Code = Common.Utils.NO_RECORDS_FOUND_CODE;
                    response.Message = Common.Utils.NO_RECORDS_FOUND_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.TECHNICAL_ERROR_CODE;
                response.Message = Common.Utils.TECHNICAL_ERROR_MESSAGE;
                response.Comments = ex.Message;
            }

            return response;
        }

        public void Dispose()
        {

        }
    }
}