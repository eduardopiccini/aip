﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using CreditCardWfc.App_Code;
using System.ComponentModel;

namespace CreditCardWfc
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface Icc
    {
        [OperationContract]
        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "Login")]
        [Description("Creates a session for transactioning.")]
        LoginResponse Login(LoginRequest Request);

        [OperationContract]
        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "Logout")]
        [Description("Elimites the session.")]
        Response Logout(Request Request);

        [OperationContract]
        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "CountrySearch")]
        [Description("Get all the countries available.")]
        CountryResponse CountrySearch(CountryRequest Request);

        [OperationContract]
        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "CardTypeSearch")]
        [Description("Get all the card types available.")]
        CardTypeResponse CardTypeSearch(CardTypeRequest Request);

        [OperationContract]
        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "StateSearch")]
        [Description("Get all the states available.")]
        StateResponse StateSearch(StateRequest Request);

        [OperationContract]
        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "VoucherSearch")]
        VoucherSearchResponse VoucherSearch(VoucherSearchRequest Request);

        [OperationContract]
        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "ProfileSearch")]
        [Description("Get all the profile id available.")]
        ProfileSearchResponse ProfileSearch(ProfileSearchRequest Request);

        [OperationContract]
        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "PaymentSearch")]
        [Description("Get all the payments id available.")]
        PaymentSearchResponse PaymentSearch(PaymentSearchRequest Request);

        [OperationContract]
        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "TransLogSearch")]
        [Description("Get the approved transactions.")]
        TransLogSearchResponse TransLogSearch(TransLogSearchRequest Request);

        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "SaleWithProfileAndPaymentProfile")]
        SaleResponse SaleWithProfileAndPaymentProfile(SaleWithProfileAndPaymentProfileRequest Request);

        [OperationContract]
        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "Sale")]
        [Description("Creates a sale request with the specific credit card")]
        SaleResponse Sale(SaleRequest Request);


        [OperationContract]
        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "DeleteProfile")]
        [Description("Deletes profile")]
        Response DeleteProfile(DeleteProfileIdRequest Request);

        [OperationContract]
        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "DeletePaymentProfile")]
        [Description("Deletes profile")]
        Response DeletePaymentProfile(DeletePaymentIdRequest Request);

        [OperationContract]
        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "ChangePaymentProfileStatus")]
        [Description("Change payment status")]
        Response ChangePaymentProfileStatus(ChangePaymentRequest Request);


        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "StatusSearch")]
        StatusResponse StatusSearch(StatusRequest Request);

        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "ChangeAutoRecharge")]
        Response ChangeAutoRecharge(AutoRechargeRequest Request);
    }
}
