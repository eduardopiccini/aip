﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using Oracle.DataAccess.Client;
using System.Text;
using OracleAccessLayer;
using System.Data;
using Common.Exceptions;

namespace CreditCardWfc.App_Code
{
    [DataContract]
    public class Status
    {
        [DataMember]
        public String Id { get; set; }

        [DataMember]
        public String Description { get; set; }
    }

    [DataContract]
    public class StatusResponse : Response
    {
        [DataMember]
        public List<Status> Status { get; set; }
    }

    [DataContract]
    public class StatusRequest : Request
    {
        [DataMember]
        public String Id { get; set; }
    }

    public class StatusDao : IDisposable
    {
        public StatusResponse Search(StatusRequest Request)
        {
            StatusResponse response = new StatusResponse();
            OracleCommand cmd = new OracleCommand();
            StringBuilder query = new StringBuilder();
            DataTable dt = null;

            try
            {
                query.Append(" select status_id, description ");
                query.Append(" from cc_vw_status ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "status_id", Request.Id, OracleDbType.Int32);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), String.Empty, String.Empty);

                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Status = new List<Status>();

                    foreach (DataRow row in dt.Rows)
                    {
                        Status obj = new Status();

                        obj.Id = Common.Utils.ObjectToString(row["status_id"]);
                        obj.Description = Common.Utils.ObjectToString(row["description"]);

                        response.Status.Add(obj);
                    }

                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;
                }
                else
                {
                    throw new NoRecordsFoundException();
                }
            }
            catch (CustomException ex)
            {
                response.Code = ex.Code;
                response.Message = ex.Message;
                response.Status = null;
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                response.Status = null;
            }

            return response;
        }

        public void Dispose()
        {

        }
    }
}