﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using Oracle.DataAccess.Client;
using System.Text;
using OracleAccessLayer;
using System.Data;
using Common.Exceptions;

namespace CreditCardWfc.App_Code
{
    [DataContract]
    public class State
    {
        [DataMember]
        public String Id { get; set; }

        [DataMember]
        public String Description { get; set; }
    }

    [DataContract]
    public class StateResponse : Response
    {
        [DataMember]
        public List<State> State { get; set; }
    }

    [DataContract]
    public class StateRequest : Request
    {
        [DataMember]
        public String Id { get; set; }
    }

    public class StateDao : IDisposable
    {
        public StateResponse Search(StateRequest Request)
        {
            StateResponse response = new StateResponse();
            OracleCommand cmd = new OracleCommand();
            StringBuilder query = new StringBuilder();
            DataTable dt = null;

            try
            {
                query.Append(" select state_id, description ");
                query.Append(" from cc_vw_state ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "state_id", Request.Id, OracleDbType.Int32);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), String.Empty, String.Empty);

                if (dt != null && dt.Rows.Count > 0)
                {
                    response.State = new List<State>();

                    foreach (DataRow row in dt.Rows)
                    {
                        State obj = new State();

                        obj.Id = Common.Utils.ObjectToString(row["state_id"]);
                        obj.Description = Common.Utils.ObjectToString(row["description"]);

                        response.State.Add(obj);
                    }

                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;
                }
                else
                {
                    throw new NoRecordsFoundException();
                }
            }
            catch (CustomException ex)
            {
                response.Code = ex.Code;
                response.Message = ex.Message;
                response.State = null;
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                response.State = null;
            }

            return response;
        }

        public void Dispose()
        {

        }
    }
}