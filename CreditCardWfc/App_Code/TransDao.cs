﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using Oracle.DataAccess.Client;
using OracleAccessLayer;
using System.Data;
using System.Text;
using Common.Exceptions;

namespace CreditCardWfc.App_Code
{
    [DataContract]
    public class TransLog
    {
        [DataMember]
        public String LogId { get; set; }

        [DataMember]
        public String TransDate { get; set; }

        [DataMember]
        public String TransAmount { get; set; }

        [DataMember]
        public String TransTypeId { get; set; }

        [DataMember]
        public String TransTypeName { get; set; }

        [DataMember]
        public String UserId { get; set; }

        [DataMember]
        public String UserName { get; set; }

        [DataMember]
        public String SystemId { get; set; }

        [DataMember]
        public String SystemName { get; set; }

        [DataMember]
        public String ProfileId { get; set; }

        [DataMember]
        public String PaymentId { get; set; }

        [DataMember]
        public String CardNumber { get; set; }

        [DataMember]
        public String Reference { get; set; }

        [DataMember]
        public String AuthCode { get; set; }

        [DataMember]
        public String InvoiceNumber { get; set; }

        [DataMember]
        public String TransactionId { get; set; }

        [DataMember]
        public String ExpMonth { get; set; }

        [DataMember]
        public String ExpYear { get; set; }

        [DataMember]
        public String FirstName { get; set; }

        [DataMember]
        public String LastName { get; set; }

        [DataMember]
        public String Address { get; set; }

        [DataMember]
        public String ZipCode { get; set; }

        [DataMember]
        public String City { get; set; }

        [DataMember]
        public String State { get; set; }

        [DataMember]
        public String Country { get; set; }

        [DataMember]
        public String Email { get; set; }

        [DataMember]
        public String Phone { get; set; }

        [DataMember]
        public String Note { get; set; }
    }

    [DataContract]
    public class TransLogSearchRequest : Request
    {
        [DataMember]
        public String EntityId { get; set; }

        [DataMember]
        public String EntityTypeId { get; set; }

        [DataMember]
        public DateTime? DateBegin { get; set; }

        [DataMember]
        public DateTime? DateEnd { get; set; }
    }

    [DataContract]
    public class TransLogSearchResponse : Response
    {
        [DataMember]
        public List<TransLog> TransLog { get; set; }
    }

    [DataContract]
    public class SaleRequest : Request
    {
        [DataMember]
        public Decimal Amount { get; set; }

        [DataMember]
        public int EntityTypeId { get; set; }

        [DataMember]
        public int EntityId { get; set; }

        [DataMember]
        public String EntityName { get; set; }

        [DataMember]
        public int CardTypeId { get; set; }

        [DataMember]
        public String CardNumber { get; set; }

        [DataMember]
        public String Reference { get; set; }

        [DataMember]
        public String ExpMonth { get; set; }

        [DataMember]
        public int ExpYear { get; set; }

        [DataMember]
        public String FirstName { get; set; }

        [DataMember]
        public String LastName { get; set; }

        [DataMember]
        public String Address { get; set; }

        [DataMember]
        public String ZipCode { get; set; }

        [DataMember]
        public String City { get; set; }

        [DataMember]
        public String State { get; set; }

        [DataMember]
        public String Country { get; set; }

        [DataMember]
        public String Email { get; set; }

        [DataMember]
        public long Phone { get; set; }

        [DataMember]
        public String Note { get; set; }

        [DataMember]
        public String SaleDescription { get; set; }

        [DataMember]
        public bool ManageCustomer { get; set; }
    }

    [DataContract]
    public class ProfileAndPaymentProfileRequest : Request
    {
        [DataMember]
        public decimal Amount { get; set; }

        [DataMember]
        public int EntityTypeId { get; set; }

        [DataMember]
        public int EntityId { get; set; }

        [DataMember]
        public String EntityName { get; set; }

        [DataMember]
        public int CardTypeId { get; set; }

        [DataMember]
        public String CardNumber { get; set; }

        [DataMember]
        public String Reference { get; set; }

        [DataMember]
        public int ExpMonth { get; set; }

        [DataMember]
        public int ExpYear { get; set; }

        [DataMember]
        public String FirstName { get; set; }

        [DataMember]
        public String LastName { get; set; }

        [DataMember]
        public String Address { get; set; }

        [DataMember]
        public String ZipCode { get; set; }

        [DataMember]
        public String City { get; set; }

        [DataMember]
        public String State { get; set; }

        [DataMember]
        public String Country { get; set; }

        [DataMember]
        public String Email { get; set; }

        [DataMember]
        public long Phone { get; set; }

        [DataMember]
        public String Note { get; set; }

        [DataMember]
        public String SaleDescription { get; set; }
    }


    [DataContract]
    public class SaleWithProfileAndPaymentProfileRequest : Request
    {
        [DataMember]
        public Decimal Amount { get; set; }

        [DataMember]
        public long PaymentId { get; set; }

        [DataMember]
        public long ProfileId { get; set; }

        [DataMember]
        public String Reference { get; set; }

        [DataMember]
        public String Note { get; set; }
    }

    [DataContract]
    public class SaleResponse : Response
    {
        [DataMember]
        public String AuthorizationCode { get; set; }

        [DataMember]
        public String TransactionId { get; set; }

        [DataMember]
        public String InvoiceNumber { get; set; }

    }

    public class SaleLocalResponse : Response
    {
        public long LogId { get; set; }
        public long ProviderCode { get; set; }
    }

    public class TransResponseRequest
    {
        public long LogId { get; set; }
        public String ResponseCode { get; set; }
        public String ResponseMessage { get; set; }
        public String AuthCode { get; set; }
        public long InvoiceNumber { get; set; }
        public long TransactionId { get; set; }

    }
    
    public class TransDao : IDisposable
    {

        public SaleLocalResponse Sale(decimal amount, int entityId, int entityTypeId,
            String entityName, int cardTypeId, String cardNumber, String reference, String expMonth,
            int expYear, String firstName, String lastName, String address, 
            String city, String country, String state, String zipCode, String email, String note, 
            long phone, String userId, String locale)
        {
            SaleLocalResponse response = new SaleLocalResponse();
            OracleCommand cmd = new OracleCommand();
            String tempCardNumber = String.Empty;

            try
            {
                tempCardNumber = cardNumber;

                if (!String.IsNullOrEmpty(tempCardNumber) && tempCardNumber.Length > 4)
                {
                    tempCardNumber = tempCardNumber.ToString().Substring(tempCardNumber.ToString().Length - 4, 4);
                }

                cmd.Parameters.Add("on_log_id", OracleDbType.Int64).Direction = System.Data.ParameterDirection.Output;
                cmd.Parameters.Add("on_provider_code", OracleDbType.Int64).Direction = System.Data.ParameterDirection.Output;
                cmd.Parameters.Add("oz_response_code", OracleDbType.Varchar2, 1000).Direction = System.Data.ParameterDirection.Output;
                cmd.Parameters.Add("oz_response_message", OracleDbType.Varchar2, 1000).Direction = System.Data.ParameterDirection.Output;
                cmd.Parameters.Add("iz_user_id", OracleDbType.Varchar2, 100).Value = userId;
                cmd.Parameters.Add("in_trans_amount", OracleDbType.Decimal).Value = amount;
                cmd.Parameters.Add("in_entity_type_id", OracleDbType.Int32).Value = entityTypeId;
                cmd.Parameters.Add("in_entity_id", OracleDbType.Int32).Value = entityId;
                cmd.Parameters.Add("iz_entity_name", OracleDbType.Varchar2, 100).Value = entityName;
                cmd.Parameters.Add("in_card_type_id", OracleDbType.Int32).Value = cardTypeId;
                cmd.Parameters.Add("iz_card_number", OracleDbType.Varchar2, 200).Value = tempCardNumber;
                cmd.Parameters.Add("iz_reference", OracleDbType.Varchar2, 100).Value = reference;
                cmd.Parameters.Add("in_exp_month", OracleDbType.Int32).Value = expMonth;
                cmd.Parameters.Add("in_exp_year", OracleDbType.Int32).Value = expYear;
                cmd.Parameters.Add("iz_first_name", OracleDbType.Varchar2, 100).Value = firstName;
                cmd.Parameters.Add("iz_last_name", OracleDbType.Varchar2, 100).Value = lastName;
                cmd.Parameters.Add("iz_address", OracleDbType.Varchar2, 100).Value = address;
                cmd.Parameters.Add("iz_zip_code", OracleDbType.Varchar2, 100).Value = zipCode;
                cmd.Parameters.Add("iz_city", OracleDbType.Varchar2, 100).Value = city;
                cmd.Parameters.Add("iz_state", OracleDbType.Varchar2, 100).Value = state;
                cmd.Parameters.Add("iz_country", OracleDbType.Varchar2, 100).Value = country;
                cmd.Parameters.Add("iz_email", OracleDbType.Varchar2, 100).Value = email;
                cmd.Parameters.Add("in_phone", OracleDbType.Int64).Value = phone;
                cmd.Parameters.Add("iz_note", OracleDbType.Varchar2, 100).Value = note;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;

                DataManager.RunDataCommandSP(ref cmd, "cc_pg_trans.sale", "", "");

                response.Code = cmd.Parameters["oz_response_code"].Value.ToString();
                response.Message = cmd.Parameters["oz_response_message"].Value.ToString();

                if (response.Code.Equals(Common.Utils.TRANSACTION_IN_PROGRESS))
                {
                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;

                    response.LogId = Convert.ToInt64(cmd.Parameters["on_log_id"].Value.ToString());
                    response.ProviderCode = Convert.ToInt64(cmd.Parameters["on_provider_code"].Value.ToString());
                }
            }
            catch (Exception)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return response;
        }

        public SaleLocalResponse SalePayment(SaleWithProfileAndPaymentProfileRequest Request, String userId, String locale)
        {
            SaleLocalResponse response = new SaleLocalResponse();
            OracleCommand cmd = new OracleCommand();

            try
            {
                cmd.Parameters.Add("on_log_id", OracleDbType.Int64).Direction = System.Data.ParameterDirection.Output;
                cmd.Parameters.Add("on_provider_code", OracleDbType.Int64).Direction = System.Data.ParameterDirection.Output;
                cmd.Parameters.Add("oz_response_code", OracleDbType.Varchar2, 1000).Direction = System.Data.ParameterDirection.Output;
                cmd.Parameters.Add("oz_response_message", OracleDbType.Varchar2, 1000).Direction = System.Data.ParameterDirection.Output;
                cmd.Parameters.Add("iz_user_id", OracleDbType.Varchar2, 100).Value = userId;
                cmd.Parameters.Add("in_payment_id", OracleDbType.Int32).Value = Request.PaymentId;
                cmd.Parameters.Add("in_trans_amount", OracleDbType.Decimal).Value = Request.Amount;
                cmd.Parameters.Add("iz_reference", OracleDbType.Varchar2, 100).Value = Request.Reference;
                cmd.Parameters.Add("iz_note", OracleDbType.Varchar2, 100).Value = Request.Note;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;

                DataManager.RunDataCommandSP(ref cmd, "cc_pg_trans.sale_payment", String.Empty, String.Empty);

                response.Code = cmd.Parameters["oz_response_code"].Value.ToString();
                response.Message = cmd.Parameters["oz_response_message"].Value.ToString();

                if (response.Code.Equals(Common.Utils.TRANSACTION_IN_PROGRESS))
                {
                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;

                    response.LogId = Convert.ToInt64(cmd.Parameters["on_log_id"].Value.ToString());
                    response.ProviderCode = Convert.ToInt64(cmd.Parameters["on_provider_code"].Value.ToString());
                }
            }
            catch (Exception)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return response;
        }

        public Response Response(TransResponseRequest Request, String userId, String locale, String appUser)
        {
            Response response = new Response();
            OracleCommand cmd = new OracleCommand();

            try
            {
                cmd.Parameters.Add("on_trans_log_id", OracleDbType.Int64).Direction = System.Data.ParameterDirection.Output;
                cmd.Parameters.Add("in_log_id", OracleDbType.Int64).Value = Request.LogId;
                cmd.Parameters.Add("iz_response_code", OracleDbType.Varchar2, 200).Value = Request.ResponseCode;
                cmd.Parameters.Add("iz_response_message", OracleDbType.Varchar2, 200).Value = Request.ResponseMessage;
                cmd.Parameters.Add("iz_auth_code", OracleDbType.Varchar2, 200).Value = Request.AuthCode;
                cmd.Parameters.Add("in_invoice_number", OracleDbType.Int64).Value = Request.InvoiceNumber;
                cmd.Parameters.Add("in_transaction_id", OracleDbType.Int64).Value = Request.TransactionId;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 100).Value = appUser;
                
                DataManager.RunDataCommandSP(ref cmd, "cc_pg_trans.response", String.Empty, String.Empty);

                response.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                response.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (response.Code.Equals(Common.Utils.DB_APPROVED))
                {
                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;

                    //response.TransLogId = Convert.ToInt64(cmd.Parameters["on_trans_log_id"].Value.ToString());
                }
            }
            catch (Exception)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return response;
        }


        public TransLogSearchResponse TransLogSearch(TransLogSearchRequest Request)
        {
            TransLogSearchResponse response = new TransLogSearchResponse();
            OracleCommand cmd = new OracleCommand();
            StringBuilder query = new StringBuilder();
            DataTable dt = null;

            try
            {
                query.Append(" select log_id, trans_date, trans_amount, trans_type_id, trans_type_name, ");
                query.Append(" user_id, user_name, system_id, system_name, profile_id, ");
                query.Append(" payment_id,  card_number, reference, auth_code, invoice_number, ");
                query.Append(" transaction_id, exp_month, exp_year, first_name, last_name, ");
                query.Append(" address, zip_code, city, state, country, email, phone, note ");
                query.Append(" from cc_vw_trans_log ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "entity_id", Request.EntityId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "entity_type_id", Request.EntityTypeId, OracleDbType.Int32);

                if(Request.DateBegin!=null && Request.DateEnd!=null)
                    DataManager.AddBindParamCmd(ref cmd, ref query, "trans_date", Request.DateBegin.Value, Request.DateEnd.Value, OracleDbType.Date, DataManager.eOperador.Between);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), String.Empty, String.Empty);

                if (dt != null && dt.Rows.Count > 0)
                {
                    response.TransLog = new List<TransLog>();

                    foreach (DataRow row in dt.Rows)
                    {
                        TransLog obj = new TransLog();

                        obj.Address = Common.Utils.ObjectToString(row["address"]);
                        obj.AuthCode = Common.Utils.ObjectToString(row["auth_code"]);
                        obj.CardNumber = Common.Utils.ObjectToString(row["card_number"]);
                        obj.City = Common.Utils.ObjectToString(row["city"]);
                        obj.Country = Common.Utils.ObjectToString(row["country"]);
                        obj.Email = Common.Utils.ObjectToString(row["email"]);
                        obj.ExpMonth = Common.Utils.ObjectToString(row["exp_month"]);
                        obj.ExpYear = Common.Utils.ObjectToString(row["exp_year"]);
                        obj.FirstName = Common.Utils.ObjectToString(row["first_name"]);
                        obj.InvoiceNumber = Common.Utils.ObjectToString(row["invoice_number"]);
                        obj.LastName = Common.Utils.ObjectToString(row["last_name"]);
                        obj.LogId = Common.Utils.ObjectToString(row["log_id"]);
                        obj.Note = Common.Utils.ObjectToString(row["note"]);
                        obj.PaymentId = Common.Utils.ObjectToString(row["payment_id"]);
                        obj.Phone = Common.Utils.ObjectToString(row["phone"]);
                        obj.ProfileId = Common.Utils.ObjectToString(row["profile_id"]);
                        obj.Reference = Common.Utils.ObjectToString(row["reference"]);
                        obj.State = Common.Utils.ObjectToString(row["state"]);
                        obj.SystemId = Common.Utils.ObjectToString(row["system_id"]);
                        obj.SystemName = Common.Utils.ObjectToString(row["system_name"]);
                        obj.TransactionId = Common.Utils.ObjectToString(row["transaction_id"]);
                        obj.TransAmount = Common.Utils.ObjectToString(row["trans_amount"]);
                        obj.TransDate = Common.Utils.ObjectToString(row["trans_date"]);
                        obj.TransTypeId = Common.Utils.ObjectToString(row["trans_type_id"]);
                        obj.TransTypeName = Common.Utils.ObjectToString(row["trans_type_name"]);
                        obj.UserId = Common.Utils.ObjectToString(row["user_id"]);
                        obj.UserName = Common.Utils.ObjectToString(row["user_name"]);
                        obj.ZipCode = Common.Utils.ObjectToString(row["zip_code"]);

                        response.TransLog.Add(obj);
                    }

                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;
                }
                else
                {
                    throw new NoRecordsFoundException();
                }
            }
            catch (CustomException ex)
            {
                response.Code = ex.Code;
                response.Message = ex.Message;
                response.TransLog = null;
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                response.TransLog = null;
            }

            return response;
        }

        public void Dispose()
        {
        }
    }
}