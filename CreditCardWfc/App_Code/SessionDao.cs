﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Data;
using OracleAccessLayer;
using Oracle.DataAccess.Client;

namespace CreditCardWfc.App_Code
{
    [DataContract]
    public class LoginRequest
    {
        [DataMember]
        public String UserId { get; set; }

        [DataMember]
        public String Password { get; set; }
    }

    [DataContract]
    public class LoginResponse : Response
    {
        [DataMember]
        public String SessionId { get; set; }
    }

    [DataContract]
    public class ValidateResponse : Response
    {
        [DataMember]
        public String UserId { get; set; }

        [DataMember]
        public String SystemId { get; set; }
    }

    public class SessionDao : IDisposable
    {
        public LoginResponse Login(LoginRequest Request, String ip, String locale)
        {
            LoginResponse response = new LoginResponse();
            System.Guid guid = System.Guid.NewGuid();
            OracleCommand cmd = new OracleCommand();
            String sessionId = guid.ToString();

            try
            {
                cmd.Parameters.Add("iz_api_user_id", OracleDbType.Varchar2, 100).Value = Request.UserId;
                cmd.Parameters.Add("iz_api_password", OracleDbType.Varchar2, 100).Value = Request.Password;
                cmd.Parameters.Add("iz_ipaddress", OracleDbType.Varchar2, 100).Value = ip;
                cmd.Parameters.Add("iz_session_id", OracleDbType.Varchar2, 200).Value = sessionId;
                cmd.Parameters.Add("on_system_id", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;

                DataManager.RunDataCommandSP(ref cmd, "cc_pg_session_ws.login", "", "");

                response.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                response.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (response.Code.Equals(Common.Utils.DB_APPROVED))
                {
                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;
                    response.SessionId = sessionId;
                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return response;
        }


        public Response Logout(Request Request, String ip, String locale)
        {
            LoginResponse response = new LoginResponse();
            OracleCommand cmd = new OracleCommand();

            try
            {
                cmd.Parameters.Add("iz_session_id", OracleDbType.Varchar2, 200).Value = Request.SessionId;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;

                DataManager.RunDataCommandSP(ref cmd, "cc_pg_session_ws.logout", "", "");

                response.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                response.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (response.Code.Equals(Common.Utils.DB_APPROVED))
                {
                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;
                }
            }
            catch (Exception)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return response;
        }

        public ValidateResponse Validate(String sessionId, String ip, String locale)
        {
            ValidateResponse response = new ValidateResponse();
            OracleCommand cmd = new OracleCommand();

            try
            {
                cmd.Parameters.Add("iz_session_id", OracleDbType.Varchar2, 200).Value = sessionId;
                cmd.Parameters.Add("iz_ipaddress", OracleDbType.Varchar2, 100).Value = ip;
                cmd.Parameters.Add("oz_user_id", OracleDbType.Varchar2, 100).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_system_id", OracleDbType.Varchar2, 100).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;

                DataManager.RunDataCommandSP(ref cmd, "cc_pg_session_ws.session_validate", String.Empty, String.Empty);

                response.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                response.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (response.Code.Equals(Common.Utils.DB_APPROVED))
                {
                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;
                    response.UserId = cmd.Parameters["oz_user_id"].Value.ToString();
                    response.SystemId = cmd.Parameters["on_system_id"].Value.ToString();
                }
            }
            catch (Exception)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return response;
        }

        public void Dispose()
        {

        }
    }
}