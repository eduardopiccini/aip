﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;
using System.Runtime.Serialization;
using System.Text;
using Common.Exceptions;

namespace CreditCardWfc.App_Code
{
    [DataContract]
    public class Profile
    {
        [DataMember]
        public String ProfileId { get; set; }

        [DataMember]
        public String Email { get; set; }

        [DataMember]
        public String AutoRecharge { get; set; }

        [DataMember]
        public String AutoRechargeStatus { get; set; }

        [DataMember]
        public String AutoRechargeAmount { get; set; }

        [DataMember]
        public String AutoFloorAmount { get; set; }

        [DataMember]
        public String AutoPaymentId { get; set; }
    }

    [DataContract]
    public class Payment
    {
        [DataMember]
        public String PaymentId { get; set; }

        [DataMember]
        public String CardTypeId { get; set; }

        [DataMember]
        public String CardTypeName { get; set; }

        [DataMember]
        public String CardNumber { get; set; }

        [DataMember]
        public String ExpMonth { get; set; }

        [DataMember]
        public String ExpYear { get; set; }

        [DataMember]
        public String FirstName { get; set; }

        [DataMember]
        public String LastName { get; set; }

        [DataMember]
        public String StatusId { get; set; }

        [DataMember]
        public String StatusName { get; set; }

        [DataMember]
        public String EntityId { get; set; }

        [DataMember]
        public String EntityTypeId { get; set; }

        [DataMember]
        public String ProfileId { get; set; }
    }

    [DataContract]
    public class PaymentSearchResponse : Response
    {
        [DataMember]
        public List<Payment> Payment { get; set; }
    }

    [DataContract]
    public class ProfileSearchResponse : Response
    {
        [DataMember]
        public List<Profile> Profile { get; set; }
    }

    [DataContract]
    public class ProfileSearchRequest : Request
    {
        [DataMember]
        public int? EntityId { get; set; }

        [DataMember]
        public int? EntityTypeId { get; set; }

        [DataMember]
        public int? StatusId { get; set; }

        [DataMember]
        public String Email { get; set; }

        [DataMember]
        public long? ProfileId { get; set; }
    }

    [DataContract]
    public class PaymentSearchRequest : Request
    {
        [DataMember]
        public String ProfileId { get; set; }

        [DataMember]
        public String CardNumber { get; set; }

        [DataMember]
        public int? CardTypeId { get; set; }

        [DataMember]
        public int? StatusId { get; set; }

        [DataMember]
        public long? PaymentId { get; set; }

        [DataMember]
        public int? EntityId { get; set; }

        [DataMember]
        public int? EntityTypeId { get; set; }
    }

    [DataContract]
    public class DeleteProfileIdRequest : Request
    {
        [DataMember]
        public String ProfileId { get; set; }
    }

    [DataContract]
    public class DeletePaymentIdRequest : Request
    {
        [DataMember]
        public String PaymentId { get; set; }

        [DataMember]
        public String ProfileId { get; set; }
    }

    [DataContract]
    public class ChangePaymentRequest : Request
    {
        [DataMember]
        public String PaymentId { get; set; }

        [DataMember]
        public int StatusId { get; set; }
    }

    [DataContract]
    public class AutoRechargeRequest : Request
    {
        [DataMember]
        public long ProfileId { get; set; }

        [DataMember]
        public int AutoRecharge { get; set; }

        [DataMember]
        public long AutoPaymentId { get; set; }

        [DataMember]
        public decimal AutoRechargeAmount { get; set; }

        [DataMember]
        public decimal AutoFloorAmount { get; set; }
    }

    public class EntityDao : IDisposable
    {
        public Response SaveProfile(long profileId, String systemId, int entityTypeId, int entityId, 
            String email, String entityName, int statusId, String locale, String appUser)
        {
            Response response = new Response();
            OracleCommand cmd = new OracleCommand();

            try
            {

                cmd.Parameters.Add("in_profile_id", OracleDbType.Int64).Value = profileId;
                cmd.Parameters.Add("in_system_id", OracleDbType.Int32).Value = systemId;
                cmd.Parameters.Add("in_entity_type_id", OracleDbType.Int32).Value = entityTypeId;
                cmd.Parameters.Add("in_entity_id", OracleDbType.Int32).Value = entityId;
                cmd.Parameters.Add("iz_email", OracleDbType.Varchar2, 100).Value = email;
                cmd.Parameters.Add("iz_entity_name", OracleDbType.Varchar2, 100).Value = entityName;
                cmd.Parameters.Add("in_status_id", OracleDbType.Int32).Value = statusId;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 100).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "cc_pg_entity.save_profile", String.Empty, String.Empty);

                response.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                response.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (response.Code.Equals(Common.Utils.DB_APPROVED))
                {
                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;
                }
            }
            catch (Exception)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return response;
        }

        public Response RemoveProfile(long profileId, String locale, String appUser)
        {
            Response response = new Response();
            OracleCommand cmd = new OracleCommand();

            try
            {

                cmd.Parameters.Add("in_profile_id", OracleDbType.Int64).Value = profileId;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 100).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "cc_pg_entity.remove_profile", String.Empty, String.Empty);

                response.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                response.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (response.Code.Equals(Common.Utils.DB_APPROVED))
                {
                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;
                }
            }
            catch (Exception)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return response;
        }

        public Response RemovePayment(long paymentId, String locale, String appUser)
        {
            Response response = new Response();
            OracleCommand cmd = new OracleCommand();

            try
            {

                cmd.Parameters.Add("in_payment_id", OracleDbType.Int64).Value = paymentId;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 100).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "cc_pg_entity.remove_payment", String.Empty, String.Empty);

                response.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                response.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (response.Code.Equals(Common.Utils.DB_APPROVED))
                {
                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;
                }
            }
            catch (Exception)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return response;
        }

        public Response SavePaymentProfile(long paymentId, long profileId, int cardTypeId, String tempCardNumber,
            String expMonth, int expYear, String firstName, String lastName, int statusId,
            String locale, String appUser, String city, String state, String zipCode, String address,
            String phone)
        {
            Response response = new Response();
            OracleCommand cmd = new OracleCommand();
            String cardNumber = String.Empty;

            try
            {
                if (!String.IsNullOrEmpty(tempCardNumber) && tempCardNumber.Length > 4)
                {
                    cardNumber = tempCardNumber.ToString().Substring(tempCardNumber.ToString().Length - 4, 4);
                }

                cmd.Parameters.Add("in_payment_id", OracleDbType.Int64).Value = paymentId;
                cmd.Parameters.Add("in_profile_id", OracleDbType.Int64).Value = profileId;
                cmd.Parameters.Add("in_card_type_id", OracleDbType.Int32).Value = cardTypeId;
                cmd.Parameters.Add("in_card_number", OracleDbType.Varchar2, 4).Value = cardNumber;
                cmd.Parameters.Add("in_exp_month", OracleDbType.Int32).Value = expMonth;
                cmd.Parameters.Add("in_exp_year", OracleDbType.Int32).Value = expYear;
                cmd.Parameters.Add("iz_first_name", OracleDbType.Varchar2, 100).Value = firstName;
                cmd.Parameters.Add("iz_last_name", OracleDbType.Varchar2, 100).Value = lastName;
                cmd.Parameters.Add("in_status_id", OracleDbType.Int32).Value = statusId;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 100).Value = appUser;
                cmd.Parameters.Add("iz_city", OracleDbType.Varchar2, 100).Value = city;
                cmd.Parameters.Add("iz_state", OracleDbType.Varchar2, 100).Value = state;
                cmd.Parameters.Add("iz_zip_code", OracleDbType.Varchar2, 100).Value = zipCode;
                cmd.Parameters.Add("iz_address", OracleDbType.Varchar2, 1000).Value = address;
                cmd.Parameters.Add("iz_phone", OracleDbType.Varchar2, 100).Value = phone;

                DataManager.RunDataCommandSP(ref cmd, "cc_pg_entity.save_payment", String.Empty, String.Empty);

                response.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                response.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (response.Code.Equals(Common.Utils.DB_APPROVED))
                {
                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;
                }
            }
            catch (Exception)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return response;
        }

        public Response ChangePaymentStatus(long paymentId, int statusId, String locale, String appUser)
        {
            Response response = new Response();
            OracleCommand cmd = new OracleCommand();

            try
            {

                cmd.Parameters.Add("in_payment_id", OracleDbType.Int64).Value = paymentId;
                cmd.Parameters.Add("in_status_id", OracleDbType.Int64).Value = statusId;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 100).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "cc_pg_entity.change_payment_status", String.Empty, String.Empty);

                response.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                response.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (response.Code.Equals(Common.Utils.DB_APPROVED))
                {
                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;
                }
            }
            catch (Exception)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return response;
        }

       
        public Response AutoRecharge(long profileId, int autoRecharge, long autoPaymentId, decimal autoRechargeAmount,
            decimal autoFloorAmount, String locale, String appUser)
        {
            Response response = new Response();
            OracleCommand cmd = new OracleCommand();

            try
            {

                cmd.Parameters.Add("in_profile_id", OracleDbType.Int64).Value = profileId;
                cmd.Parameters.Add("in_auto_recharge", OracleDbType.Int32).Value = autoRecharge;
                cmd.Parameters.Add("in_auto_payment_id", OracleDbType.Int64).Value = autoPaymentId;
                cmd.Parameters.Add("in_auto_recharge_amount", OracleDbType.Decimal).Value = autoRechargeAmount;
                cmd.Parameters.Add("in_auto_floor_amount", OracleDbType.Decimal).Value = autoFloorAmount;
                cmd.Parameters.Add("on_errorcode", OracleDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_errormessage", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_message_locale", OracleDbType.Varchar2, 5).Value = locale;
                cmd.Parameters.Add("iz_app_user", OracleDbType.Varchar2, 100).Value = appUser;

                DataManager.RunDataCommandSP(ref cmd, "cc_pg_entity.auto_recharge", String.Empty, String.Empty);

                response.Code = cmd.Parameters["on_errorcode"].Value.ToString();
                response.Message = cmd.Parameters["oz_errormessage"].Value.ToString();

                if (response.Code.Equals(Common.Utils.DB_APPROVED))
                {
                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;
                }
            }
            catch (Exception)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return response;
        }

        public ProfileSearchResponse ProfileSearch(ProfileSearchRequest Request)
        {
            ProfileSearchResponse response = new ProfileSearchResponse();
            OracleCommand cmd = new OracleCommand();
            StringBuilder query = new StringBuilder();
            DataTable dt = null;

            try
            {
                query.Append(" select profile_id, email, auto_recharge, auto_recharge_status, auto_recharge_amount, ");
                query.Append(" auto_floor_amount, auto_payment_id ");
                query.Append(" from cc_vw_entity_profile ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "status_id", Request.StatusId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "entity_id", Request.EntityId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "entity_type_id", Request.EntityTypeId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "profile_id", Request.ProfileId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "email", Request.Email, OracleDbType.Varchar2);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), String.Empty, String.Empty);

                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Profile = new List<Profile>();

                    foreach (DataRow row in dt.Rows)
                    {
                        Profile obj = new Profile();

                        obj.Email = Common.Utils.ObjectToString(row["email"]);
                        obj.ProfileId = Common.Utils.ObjectToString(row["profile_id"]);
                        obj.AutoFloorAmount = Common.Utils.ObjectToString(row["auto_floor_amount"]);
                        obj.AutoPaymentId = Common.Utils.ObjectToString(row["auto_payment_id"]);
                        obj.AutoRecharge = Common.Utils.ObjectToString(row["auto_recharge"]);
                        obj.AutoRechargeAmount = Common.Utils.ObjectToString(row["auto_recharge_amount"]);
                        obj.AutoRechargeStatus = Common.Utils.ObjectToString(row["auto_recharge_status"]);
                        
                        response.Profile.Add(obj);
                    }

                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;
                }
                else
                {
                    throw new NoRecordsFoundException();
                }
            }
            catch (CustomException ex)
            {
                response.Code = ex.Code;
                response.Message = ex.Description;
                response.Profile = null;
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                response.Profile = null;
            }

            return response;
        }

        public PaymentSearchResponse PaymentSearch(PaymentSearchRequest Request)
        {
            PaymentSearchResponse response = new PaymentSearchResponse();
            OracleCommand cmd = new OracleCommand();
            StringBuilder query = new StringBuilder();
            DataTable dt = null;

            try
            {
                query.Append(" select payment_id, card_type_id, card_type_name, card_number, exp_month, exp_year, ");
                query.Append(" first_name, last_name, status_id, status_name, entity_id, entity_type_id, profile_id ");
                query.Append(" from cc_vw_entity_payment ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "status_id", Request.StatusId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "profile_id", Request.ProfileId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "card_type_id", Request.CardTypeId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "payment_id", Request.PaymentId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "card_number", Request.CardNumber, OracleDbType.Varchar2);
                DataManager.AddBindParamCmd(ref cmd, ref query, "entity_id", Request.EntityId, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "entity_type_id", Request.EntityTypeId, OracleDbType.Int32);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), String.Empty, String.Empty);

                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Payment = new List<Payment>();

                    foreach (DataRow row in dt.Rows)
                    {
                        Payment obj = new Payment();

                        obj.CardNumber = Common.Utils.ObjectToString(row["card_number"]);
                        obj.CardTypeId = Common.Utils.ObjectToString(row["card_type_id"]);
                        obj.CardTypeName = Common.Utils.ObjectToString(row["card_type_name"]);
                        obj.ExpMonth = Common.Utils.ObjectToString(row["exp_month"]);
                        obj.ExpYear = Common.Utils.ObjectToString(row["exp_year"]);
                        obj.FirstName = Common.Utils.ObjectToString(row["first_name"]);
                        obj.LastName = Common.Utils.ObjectToString(row["last_name"]);
                        obj.PaymentId = Common.Utils.ObjectToString(row["payment_id"]);
                        obj.StatusId = Common.Utils.ObjectToString(row["status_id"]);
                        obj.StatusName = Common.Utils.ObjectToString(row["status_name"]);
                        obj.EntityId = Common.Utils.ObjectToString(row["entity_id"]);
                        obj.EntityTypeId = Common.Utils.ObjectToString(row["entity_type_id"]);
                        obj.ProfileId = Common.Utils.ObjectToString(row["profile_id"]);

                        response.Payment.Add(obj);
                    }

                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;
                }
                else
                {
                    throw new NoRecordsFoundException();
                }
            }
            catch (CustomException ex)
            {
                response.Code = ex.Code;
                response.Message = ex.Message;
                response.Payment = null;
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                response.Payment = null;
            }

            return response;
        }

        public void Dispose()
        {

        }
    }
}