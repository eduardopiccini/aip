﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace CreditCardWfc.App_Code
{
    [DataContract]
    public class Response
    {
        [DataMember]
        public String Code { get; set; }

        [DataMember]
        public String Message { get; set; }
    }
}