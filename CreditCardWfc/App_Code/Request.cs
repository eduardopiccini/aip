﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace CreditCardWfc.App_Code
{
    [DataContract]
    public class Request
    {
        [DataMember]
        public String SessionId { get; set; }
    }
}