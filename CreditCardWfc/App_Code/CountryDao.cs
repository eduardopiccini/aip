﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using Oracle.DataAccess.Client;
using System.Text;
using OracleAccessLayer;
using System.Data;
using Common.Exceptions;

namespace CreditCardWfc.App_Code
{
    [DataContract]
    public class Country
    {
        [DataMember]
        public String Id { get; set; }

        [DataMember]
        public String Description { get; set; }

        [DataMember]
        public String IsoCode { get; set; }
    }

    [DataContract]
    public class CountryResponse : Response
    {
        [DataMember]
        public List<Country> Country { get; set; }
    }

    [DataContract]
    public class CountryRequest : Request
    {
        [DataMember]
        public String Id { get; set; }

        [DataMember]
        public String IsoCode { get; set; }
    }

    public class CountryDao : IDisposable
    {
        public CountryResponse Search(CountryRequest Request)
        {
            CountryResponse response = new CountryResponse();
            OracleCommand cmd = new OracleCommand();
            StringBuilder query = new StringBuilder();
            DataTable dt = null;

            try
            {
                query.Append(" select country_id, description, iso_code ");
                query.Append(" from cc_vw_country ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "country_id", Request.Id, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "iso_code", Request.IsoCode, OracleDbType.Varchar2);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), String.Empty, String.Empty);

                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Country = new List<Country>();

                    foreach (DataRow row in dt.Rows)
                    {
                        Country country = new Country();

                        country.Id = Common.Utils.ObjectToString(row["country_id"]);
                        country.Description = Common.Utils.ObjectToString(row["description"]);
                        country.IsoCode = Common.Utils.ObjectToString(row["iso_code"]);

                        response.Country.Add(country);
                    }

                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;
                }
                else
                {
                    throw new NoRecordsFoundException();
                }
            }
            catch (CustomException ex)
            {
                response.Code = ex.Code;
                response.Message = ex.Message;
                response.Country = null;
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                response.Country = null;
            }

            return response;
        }

        public void Dispose()
        {

        }
    }
}