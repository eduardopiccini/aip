﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using Oracle.DataAccess.Client;
using System.Text;
using OracleAccessLayer;
using System.Data;
using Common.Exceptions;

namespace CreditCardWfc.App_Code
{
    [DataContract]
    public class CardType
    {
        [DataMember]
        public String Id { get; set; }

        [DataMember]
        public String Description { get; set; }
    }

    [DataContract]
    public class CardTypeResponse : Response
    {
        [DataMember]
        public List<CardType> CardType { get; set; }
    }

    [DataContract]
    public class CardTypeRequest : Request
    {
        [DataMember]
        public String Id { get; set; }
    }

    public class CardTypeDao : IDisposable
    {
        public CardTypeResponse Search(CardTypeRequest Request)
        {
            CardTypeResponse response = new CardTypeResponse();
            OracleCommand cmd = new OracleCommand();
            StringBuilder query = new StringBuilder();
            DataTable dt = null;

            try
            {
                query.Append(" select card_type_id, description ");
                query.Append(" from cc_vw_card_type ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "card_type_id", Request.Id, OracleDbType.Int32);
                DataManager.AddBindParamCmd(ref cmd, ref query, "status_id", 1, OracleDbType.Int32);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), String.Empty, String.Empty);

                if (dt != null && dt.Rows.Count > 0)
                {
                    response.CardType = new List<CardType>();

                    foreach (DataRow row in dt.Rows)
                    {
                        CardType obj = new CardType();

                        obj.Id = Common.Utils.ObjectToString(row["card_type_id"]);
                        obj.Description = Common.Utils.ObjectToString(row["description"]);

                        response.CardType.Add(obj);
                    }

                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;
                }
                else
                {
                    throw new NoRecordsFoundException();
                }
            }
            catch (CustomException ex)
            {
                response.Code = ex.Code;
                response.Message = ex.Message;
                response.CardType = null;
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                response.CardType = null;
            }

            return response;
        }

        public void Dispose()
        {

        }
    }
}