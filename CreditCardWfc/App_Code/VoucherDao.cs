﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using Oracle.DataAccess.Client;
using System.Text;
using System.Data;
using OracleAccessLayer;
using Common.Exceptions;

namespace CreditCardWfc.App_Code
{
    [DataContract]
    public class VoucherSearchRequest : Request
    {
        [DataMember]
        public String LogId { get; set; }

        [DataMember]
        public String AuthCode { get; set; }

        [DataMember]
        public String InvoiceNumber { get; set; }

        [DataMember]
        public String TransactionId { get; set; }
    }

    [DataContract]
    public class VoucherSearchResponse : Response
    {
        [DataMember]
        public List<Voucher> Voucher { get; set; }
    }

    [DataContract]
    public class Voucher
    {
        [DataMember]
        public String LogId { get; set; }

        [DataMember]
        public String AuthCode { get; set; }

        [DataMember]
        public String CardNumber { get; set; }

        [DataMember]
        public String ResponseMessage { get; set; }

        [DataMember]
        public String TransDate { get; set; }

        [DataMember]
        public String InvoiceNumber { get; set; }

        [DataMember]
        public String TransactionId { get; set; }

        [DataMember]
        public String TransAmount { get; set; }

        [DataMember]
        public String HolderName { get; set; }

        [DataMember]
        public String City { get; set; }

        [DataMember]
        public String State { get; set; }

        [DataMember]
        public String ZipCode { get; set; }

        [DataMember]
        public String Address { get; set; }

        [DataMember]
        public String Phone { get; set; }

        [DataMember]
        public String CardTypeId { get; set; }

        [DataMember]
        public String CardTypeName { get; set; }

        [DataMember]
        public String EntityName { get; set; }
    }

    public class VoucherDao : IDisposable
    {
        public VoucherSearchResponse Search(VoucherSearchRequest Request)
        {
            VoucherSearchResponse response = new VoucherSearchResponse();
            OracleCommand cmd = new OracleCommand();
            StringBuilder query = new StringBuilder();
            DataTable dt = null;

            try
            {
                query.Append(" SELECT a.trans_log_id, a.auth_code, a.card_number, a.response_message, ");
                query.Append(" a.trans_date, a.invoice_number, a.transaction_id, a.trans_amount, ");
                query.Append(" a.holder_name, a.city, a.state, a.zip_code, a.address, a.phone, ");
                query.Append(" a.card_type_id, a.card_type_name, a.entity_name ");
                query.Append(" FROM cc_vw_voucher a ");

                DataManager.AddBindParamCmd(ref cmd, ref query, "auth_code", Request.AuthCode, OracleDbType.Varchar2);
                DataManager.AddBindParamCmd(ref cmd, ref query, "invoice_number", Request.InvoiceNumber, OracleDbType.Varchar2);
                DataManager.AddBindParamCmd(ref cmd, ref query, "trans_log_id", Request.LogId, OracleDbType.Int64);
                DataManager.AddBindParamCmd(ref cmd, ref query, "transaction_id", Request.TransactionId, OracleDbType.Int64);

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), String.Empty, String.Empty);

                if (dt != null && dt.Rows.Count > 0)
                {
                    response.Voucher = new List<Voucher>();

                    foreach (DataRow row in dt.Rows)
                    {
                        Voucher obj = new Voucher();

                        obj.Address = Common.Utils.ObjectToString(row["address"]);
                        obj.AuthCode = Common.Utils.ObjectToString(row["auth_code"]);
                        obj.CardNumber = Common.Utils.ObjectToString(row["card_number"]);
                        obj.CardTypeId = Common.Utils.ObjectToString(row["card_type_id"]);
                        obj.CardTypeName = Common.Utils.ObjectToString(row["card_type_name"]);
                        obj.City = Common.Utils.ObjectToString(row["city"]);
                        obj.HolderName = Common.Utils.ObjectToString(row["holder_name"]);
                        obj.InvoiceNumber = Common.Utils.ObjectToString(row["invoice_number"]);
                        obj.LogId = Common.Utils.ObjectToString(row["trans_log_id"]);
                        obj.Phone = Common.Utils.ObjectToString(row["phone"]);
                        obj.ResponseMessage = Common.Utils.ObjectToString(row["response_message"]);
                        obj.State = Common.Utils.ObjectToString(row["state"]);
                        obj.TransactionId = Common.Utils.ObjectToString(row["transaction_id"]);
                        obj.TransAmount = Common.Utils.ObjectToString(row["trans_amount"]);
                        obj.TransDate = Common.Utils.ObjectToString(row["trans_date"]);
                        obj.ZipCode = Common.Utils.ObjectToString(row["zip_code"]);
                        obj.EntityName = Common.Utils.ObjectToString(row["entity_name"]);


                        response.Voucher.Add(obj);
                    }

                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;
                }
                else
                {
                    throw new NoRecordsFoundException();
                }
            }
            catch (CustomException ex)
            {
                response.Code = ex.Code;
                response.Message = ex.Message;
                response.Voucher = null;
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                response.Voucher = null;
            }

            return response;
        }

        public void Dispose()
        {

        }
    }
}