﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using log4net;
using CreditCardWfc.App_Code;
using System.ServiceModel.Channels;
using System.Globalization;
using Common.Exceptions;
using System.ComponentModel;

namespace CreditCardWfc
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    public class cc : Icc
    {
        private const int AUTHORIZE = 1;

        private static readonly ILog log = LogManager.GetLogger("Log");
        private static readonly ILog logSummary = LogManager.GetLogger("LogSummary");

        private String Locale;
        private String AppUser;

        public cc()
        {
            Locale = CultureInfo.CurrentCulture.Name;
            AppUser = System.Configuration.ConfigurationManager.AppSettings["app_user"].ToString();
        }

        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "Login")]
        public LoginResponse Login(LoginRequest Request)
        {
            String exception = String.Empty, ip = String.Empty;
            LoginResponse response = new LoginResponse();
            StringBuilder output = new StringBuilder();

            try
            {
                ip = this.GetIp();

                if (Request == null)
                    throw new ValueRequireException("Request");

                if (String.IsNullOrEmpty(Request.UserId))
                    throw new ValueRequireException("UserId");

                if (String.IsNullOrEmpty(Request.Password))
                    throw new ValueRequireException("Password");

                using (SessionDao dao = new SessionDao())
                {
                    response = dao.Login(Request, ip, Locale);
                }
            }
            catch (CustomException ex)
            {
                response.Code = ex.Code;
                response.Message = ex.Description;
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                exception = ex.Message;
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "LOGIN:", false, true);
                Common.Utils.Concat(ref output, "User Id", Request.UserId, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Exception", exception, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "Logout")]
        public Response Logout(Request Request)
        {
            String exception = String.Empty, ip = String.Empty;
            Response response = new Response();
            StringBuilder output = new StringBuilder();

            try
            {
                ip = this.GetIp();

                if (Request == null)
                    throw new ValueRequireException("Request");

                if (String.IsNullOrEmpty(Request.SessionId))
                    throw new ValueRequireException("SessionId");

                using (SessionDao dao = new SessionDao())
                {
                    response = dao.Logout(Request, ip, Locale);
                }
            }
            catch (CustomException ex)
            {
                response.Code = ex.Code;
                response.Message = ex.Description;
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                exception = ex.Message;
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "LOGOUT:", false, true);
                Common.Utils.Concat(ref output, "Session Id", Request.SessionId, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Exception", exception, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "CountrySearch")]
        public CountryResponse CountrySearch(CountryRequest Request)
        {
            String exception = String.Empty, ip = String.Empty;
            CountryResponse response = new CountryResponse();
            StringBuilder output = new StringBuilder();
            ValidateResponse valid = new ValidateResponse();

            try
            {
                ip = this.GetIp();

                if (Request == null)
                    throw new ValueRequireException("Request");

                if (String.IsNullOrEmpty(Request.SessionId))
                    throw new ValueRequireException("SessionId");

                using (SessionDao dao = new SessionDao())
                {
                    valid = dao.Validate(Request.SessionId, ip, Locale);

                    if (!valid.Code.Equals(Common.Utils.APPROVED))
                    {
                        throw new CustomException(valid.Code, valid.Message);
                    }
                }

                using (CountryDao dao = new CountryDao())
                {
                    response = dao.Search(Request);
                }
            }
            catch (CustomException ex)
            {
                response.Code = ex.Code;
                response.Message = ex.Description;
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                exception = ex.Message;
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "COUNTRY_SEARCH:", false, true);
                Common.Utils.Concat(ref output, "Session Id", Request.SessionId, false, false);
                Common.Utils.Concat(ref output, "User Id", valid.UserId, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Exception", exception, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "StatusSearch")]
        public StatusResponse StatusSearch(StatusRequest Request)
        {
            String exception = String.Empty, ip = String.Empty;
            StatusResponse response = new StatusResponse();
            StringBuilder output = new StringBuilder();
            ValidateResponse valid = new ValidateResponse();

            try
            {
                ip = this.GetIp();

                if (Request == null)
                    throw new ValueRequireException("Request");

                if (String.IsNullOrEmpty(Request.SessionId))
                    throw new ValueRequireException("SessionId");

                using (SessionDao dao = new SessionDao())
                {
                    valid = dao.Validate(Request.SessionId, ip, Locale);

                    if (!valid.Code.Equals(Common.Utils.APPROVED))
                    {
                        throw new CustomException(valid.Code, valid.Message);
                    }
                }

                using (StatusDao dao = new StatusDao())
                {
                    response = dao.Search(Request);
                }
            }
            catch (CustomException ex)
            {
                response.Code = ex.Code;
                response.Message = ex.Description;
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                exception = ex.Message;
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "STATUS_SEARCH:", false, true);
                Common.Utils.Concat(ref output, "Session Id", Request.SessionId, false, false);
                Common.Utils.Concat(ref output, "User Id", valid.UserId, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Exception", exception, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "CardTypeSearch")]
        public CardTypeResponse CardTypeSearch(CardTypeRequest Request)
        {
            String exception = String.Empty, ip = String.Empty;
            CardTypeResponse response = new CardTypeResponse();
            StringBuilder output = new StringBuilder();
            ValidateResponse valid = new ValidateResponse();

            try
            {
                ip = this.GetIp();

                if (Request == null)
                    throw new ValueRequireException("Request");

                if (String.IsNullOrEmpty(Request.SessionId))
                    throw new ValueRequireException("SessionId");

                using (SessionDao dao = new SessionDao())
                {
                    valid = dao.Validate(Request.SessionId, ip, Locale);

                    if (!valid.Code.Equals(Common.Utils.APPROVED))
                    {
                        throw new CustomException(valid.Code, valid.Message);
                    }
                }

                using (CardTypeDao dao = new CardTypeDao())
                {
                    response = dao.Search(Request);
                }
            }
            catch (CustomException ex)
            {
                response.Code = ex.Code;
                response.Message = ex.Description;
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                exception = ex.Message;
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "CARD_TYPE_SEARCH:", false, true);
                Common.Utils.Concat(ref output, "Session Id", Request.SessionId, false, false);
                Common.Utils.Concat(ref output, "User Id", valid.UserId, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Exception", exception, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "StateSearch")]
        public StateResponse StateSearch(StateRequest Request)
        {
            String exception = String.Empty, ip = String.Empty;
            StateResponse response = new StateResponse();
            StringBuilder output = new StringBuilder();
            ValidateResponse valid = new ValidateResponse();

            try
            {
                ip = this.GetIp();

                if (Request == null)
                    throw new ValueRequireException("Request");

                if (String.IsNullOrEmpty(Request.SessionId))
                    throw new ValueRequireException("SessionId");

                using (SessionDao dao = new SessionDao())
                {
                    valid = dao.Validate(Request.SessionId, ip, Locale);

                    if (!valid.Code.Equals(Common.Utils.APPROVED))
                    {
                        throw new CustomException(valid.Code, valid.Message);
                    }
                }

                using (StateDao dao = new StateDao())
                {
                    response = dao.Search(Request);
                }
            }
            catch (CustomException ex)
            {
                response.Code = ex.Code;
                response.Message = ex.Description;
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                exception = ex.Message;
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "STATE_SEARCH:", false, true);
                Common.Utils.Concat(ref output, "Session Id", Request.SessionId, false, false);
                Common.Utils.Concat(ref output, "User Id", valid.UserId, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Exception", exception, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "VoucherSearch")]
        public VoucherSearchResponse VoucherSearch(VoucherSearchRequest Request)
        {
            String exception = String.Empty, ip = String.Empty;
            VoucherSearchResponse response = new VoucherSearchResponse();
            StringBuilder output = new StringBuilder();
            ValidateResponse valid = new ValidateResponse();

            try
            {
                ip = this.GetIp();

                if (Request == null)
                    throw new ValueRequireException("Request");

                if (String.IsNullOrEmpty(Request.SessionId))
                    throw new ValueRequireException("SessionId");

                using (SessionDao dao = new SessionDao())
                {
                    valid = dao.Validate(Request.SessionId, ip, Locale);

                    if (!valid.Code.Equals(Common.Utils.APPROVED))
                    {
                        throw new CustomException(valid.Code, valid.Message);
                    }
                }

                using (VoucherDao dao = new VoucherDao())
                {
                    response = dao.Search(Request);
                }
            }
            catch (CustomException ex)
            {
                response.Code = ex.Code;
                response.Message = ex.Description;
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                exception = ex.Message;
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "STATE_SEARCH:", false, true);
                Common.Utils.Concat(ref output, "Session Id", Request.SessionId, false, false);
                Common.Utils.Concat(ref output, "User Id", valid.UserId, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Exception", exception, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "ProfileSearch")]
        public ProfileSearchResponse ProfileSearch(ProfileSearchRequest Request)
        {
            String exception = String.Empty, ip = String.Empty;
            ProfileSearchResponse response = new ProfileSearchResponse();
            StringBuilder output = new StringBuilder();
            ValidateResponse valid = new ValidateResponse();

            try
            {
                ip = this.GetIp();

                if (Request == null)
                    throw new ValueRequireException("Request");

                if (String.IsNullOrEmpty(Request.SessionId))
                    throw new ValueRequireException("SessionId");

                using (SessionDao dao = new SessionDao())
                {
                    valid = dao.Validate(Request.SessionId, ip, Locale);

                    if (!valid.Code.Equals(Common.Utils.APPROVED))
                    {
                        throw new CustomException(valid.Code, valid.Message);
                    }
                }

                using (EntityDao dao = new EntityDao())
                {
                    response = dao.ProfileSearch(Request);
                }
            }
            catch (CustomException ex)
            {
                response.Code = ex.Code;
                response.Message = ex.Description;
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                exception = ex.Message;
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "PROFILE_SEARCH:", false, true);
                Common.Utils.Concat(ref output, "Session Id", Request.SessionId, false, false);
                Common.Utils.Concat(ref output, "User Id", valid.UserId, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Exception", exception, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "PaymentSearch")]
        public PaymentSearchResponse PaymentSearch(PaymentSearchRequest Request)
        {
            String exception = String.Empty, ip = String.Empty;
            PaymentSearchResponse response = new PaymentSearchResponse();
            StringBuilder output = new StringBuilder();
            ValidateResponse valid = new ValidateResponse();

            try
            {
                ip = this.GetIp();

                if (Request == null)
                    throw new ValueRequireException("Request");

                if (String.IsNullOrEmpty(Request.SessionId))
                    throw new ValueRequireException("SessionId");

                using (SessionDao dao = new SessionDao())
                {
                    valid = dao.Validate(Request.SessionId, ip, Locale);

                    if (!valid.Code.Equals(Common.Utils.APPROVED))
                    {
                        throw new CustomException(valid.Code, valid.Message);
                    }
                }

                using (EntityDao dao = new EntityDao())
                {
                    response = dao.PaymentSearch(Request);
                }
            }
            catch (CustomException ex)
            {
                response.Code = ex.Code;
                response.Message = ex.Description;
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                exception = ex.Message;
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "PAYMENT_SEARCH:", false, true);
                Common.Utils.Concat(ref output, "Session Id", Request.SessionId, false, false);
                Common.Utils.Concat(ref output, "User Id", valid.UserId, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Exception", exception, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "TransLogSearch")]
        public TransLogSearchResponse TransLogSearch(TransLogSearchRequest Request)
        {
            String exception = String.Empty, ip = String.Empty;
            TransLogSearchResponse response = new TransLogSearchResponse();
            StringBuilder output = new StringBuilder();
            ValidateResponse valid = new ValidateResponse();

            try
            {
                ip = this.GetIp();

                if (Request == null)
                    throw new ValueRequireException("Request");

                if (String.IsNullOrEmpty(Request.SessionId))
                    throw new ValueRequireException("SessionId");

                if (String.IsNullOrEmpty(Request.EntityId))
                    throw new ValueRequireException("EntityId");

                if (String.IsNullOrEmpty(Request.EntityTypeId))
                    throw new ValueRequireException("EntityTypeId");

                using (SessionDao dao = new SessionDao())
                {
                    valid = dao.Validate(Request.SessionId, ip, Locale);

                    if (!valid.Code.Equals(Common.Utils.APPROVED))
                    {
                        throw new CustomException(valid.Code, valid.Message);
                    }
                }

                using (TransDao dao = new TransDao())
                {
                    response = dao.TransLogSearch(Request);
                }
            }
            catch (CustomException ex)
            {
                response.Code = ex.Code;
                response.Message = ex.Description;
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                exception = ex.Message;
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "TRANS_LOG_SEARCH:", false, true);
                Common.Utils.Concat(ref output, "Session Id", Request.SessionId, false, false);
                Common.Utils.Concat(ref output, "User Id", valid.UserId, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Exception", exception, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "Sale")]
        public SaleResponse Sale(SaleRequest Request)
        {
            String exception = String.Empty, ip = String.Empty, tempProfileId = String.Empty, profileId = String.Empty;
            StringBuilder output = new StringBuilder();
            ValidateResponse valid = new ValidateResponse();
            SaleLocalResponse saleLocal = new SaleLocalResponse();
            SaleResponse response = new SaleResponse();
            Response profileResponse = new Response();
            Response paymentResponse = new Response();
            Response dbResponse = new Response();
            auth_wcf.CreateProfileRequest createProfileReq = new auth_wcf.CreateProfileRequest();
            auth_wcf.CreateProfileResponse createProfileResp = null;
            Response profileSaveResponse = null;
            Response paymentSaveResponse = null;
            auth_wcf.CreatePaymentProfileRequest createPaymentReq = new auth_wcf.CreatePaymentProfileRequest();
            auth_wcf.CreatePaymentProfileResponse createPaymentResp = null;

            try
            {
                ip = this.GetIp();

                if (Request == null)
                    throw new ValueRequireException("Request");

                if (String.IsNullOrEmpty(Request.SessionId))
                    throw new ValueRequireException("SessionId");

                if (String.IsNullOrEmpty(Request.Address))
                    throw new ValueRequireException("Address");

                if (String.IsNullOrEmpty(Request.CardNumber))
                    throw new ValueRequireException("CardNumber");

                if (Request.CardTypeId==0)
                    throw new ValueRequireException("CardTypeId");

                if (String.IsNullOrEmpty(Request.City))
                    throw new ValueRequireException("City");

                if (String.IsNullOrEmpty(Request.Country))
                    throw new ValueRequireException("Country");

                if (String.IsNullOrEmpty(Request.Email))
                    throw new ValueRequireException("Email");

                if (Request.EntityId==0)
                    throw new ValueRequireException("EntityId");

                if (Request.EntityTypeId==0)
                    throw new ValueRequireException("EntityTypeId");

                if (String.IsNullOrEmpty(Request.EntityName))
                    throw new ValueRequireException("EntityName");

                if (String.IsNullOrEmpty(Request.ExpMonth))
                    throw new ValueRequireException("ExpMonth");

                if (Request.ExpYear == 0)
                    throw new ValueRequireException("ExpYear");

                if (String.IsNullOrEmpty(Request.Reference))
                    throw new ValueRequireException("Reference");

                if (String.IsNullOrEmpty(Request.State))
                    throw new ValueRequireException("State");

                if (String.IsNullOrEmpty(Request.ZipCode))
                    throw new ValueRequireException("ZipCode");

                if (String.IsNullOrEmpty(Request.FirstName))
                    throw new ValueRequireException("FirstName");

                if (String.IsNullOrEmpty(Request.LastName))
                    throw new ValueRequireException("LastName");

                if (String.IsNullOrEmpty(Request.SaleDescription))
                    throw new ValueRequireException("SaleDescription");

                if (Request.Amount == 0)
                    throw new InvalidAmountException();

                using (SessionDao dao = new SessionDao())
                {
                    valid = dao.Validate(Request.SessionId, ip, Locale);

                    if (!valid.Code.Equals(Common.Utils.APPROVED))
                    {
                        throw new CustomException(valid.Code, valid.Message);
                    }
                }

                using (TransDao dao = new TransDao())
                {
                    saleLocal = dao.Sale(Request.Amount, Request.EntityId, Request.EntityTypeId,
                        Request.EntityName, Request.CardTypeId, Request.CardNumber, Request.Reference,
                        Request.ExpMonth, Request.ExpYear, Request.FirstName, Request.LastName,
                        Request.Address, Request.City, Request.Country, Request.State, Request.ZipCode,
                        Request.Email, Request.Note, Request.Phone, valid.UserId, Locale);
                }

                if (saleLocal.Code.Equals(Common.Utils.APPROVED))
                {
                    if (saleLocal.ProviderCode == AUTHORIZE)
                    {
                        using (auth_wcf.IauthClient wcf = new auth_wcf.IauthClient())
                        {
                            if (Request.ManageCustomer)
                            {
                                using (EntityDao dao = new EntityDao())
                                {
                                    ProfileSearchRequest proReq = new ProfileSearchRequest();
                                    ProfileSearchResponse proResp = null;

                                    proReq.EntityId = Request.EntityId;
                                    proReq.EntityTypeId = Request.EntityTypeId;
                                    proReq.Email = Request.Email;
                                    proReq.StatusId = 1;

                                    proResp = dao.ProfileSearch(proReq);

                                    if (proResp.Code.Equals(Common.Utils.APPROVED))
                                    {
                                        profileId = proResp.Profile[0].ProfileId;
                                    }
                                    else
                                    {
                                        createProfileReq.Email = Request.Email;
                                        
                                        createProfileResp = wcf.CreateProfile(createProfileReq);

                                        if (createProfileResp.Code.Equals(Common.Utils.APPROVED))
                                        {
                                            profileId = createProfileResp.ProfileId.ToString();

                                            profileSaveResponse = dao.SaveProfile(Convert.ToInt64(profileId), valid.SystemId,
                                                Request.EntityTypeId, Request.EntityId, Request.Email, Request.EntityName, 1,
                                                Locale, this.AppUser);
                                        }
                                        else
                                        {
                                            throw new CustomException(createProfileResp.Code, createProfileResp.Description);
                                        }
                                    }
                                }
                            }
                            
                            auth_wcf.SaleRequest request = new auth_wcf.SaleRequest();
                            auth_wcf.Customer customer = new auth_wcf.Customer();
                            auth_wcf.CreditCardInfo creditcard = new auth_wcf.CreditCardInfo();
                            auth_wcf.SaleResponse saleResponse = null;

                            request.Amount = Request.Amount;
                            request.InvoiceNumber = saleLocal.LogId.ToString();
                            request.SaleDescription = Request.SaleDescription;

                            customer.Address = Request.Address;
                            customer.City = Request.City;
                            customer.Country = Request.Country;
                            customer.Email = Request.Email;
                            customer.FirstName = Request.FirstName;
                            customer.LastName = Request.LastName;
                            customer.Phone = Request.Phone == 0 ? String.Empty : Request.Phone.ToString();
                            customer.State = Request.State;
                            customer.ZipCode = Request.ZipCode;

                            creditcard.ExpirationMonth = Request.ExpMonth.ToString();
                            creditcard.ExpirationYear = Request.ExpYear.ToString();
                            creditcard.FullCardNumber = Request.CardNumber;

                            request.Customer = customer;
                            request.CreditCardInfo = creditcard;

                            saleResponse = wcf.Sale(request);

                            using (TransDao dao = new TransDao())
                            {
                                TransResponseRequest req = new TransResponseRequest();

                                req.AuthCode = saleResponse.AuthorizationCode;

                                if (!String.IsNullOrEmpty(saleResponse.InvoiceNumber))
                                {
                                    req.InvoiceNumber = Convert.ToInt64(saleResponse.InvoiceNumber);
                                    response.InvoiceNumber = saleResponse.InvoiceNumber;
                                }

                                req.LogId = saleLocal.LogId;
                                req.ResponseCode = saleResponse.Code;
                                req.ResponseMessage = saleResponse.Description;

                                if (!String.IsNullOrEmpty(saleResponse.TransactionId))
                                {
                                    req.TransactionId = Convert.ToInt64(saleResponse.TransactionId);
                                    response.TransactionId = saleResponse.TransactionId;
                                }

                                dbResponse = dao.Response(req, valid.UserId, Locale, AppUser);

                                response.Code = saleResponse.Code;
                                response.Message = saleResponse.Description;
                                response.AuthorizationCode = req.AuthCode;
                            }

                            if (Request.ManageCustomer)
                            {
                                if (response.Code.Equals(Common.Utils.APPROVED))
                                {
                                    using (EntityDao dao = new EntityDao())
                                    {
                                        PaymentSearchRequest paySearchReq = new PaymentSearchRequest();
                                        PaymentSearchResponse paySearchResp = null;

                                        paySearchReq.CardNumber = Request.CardNumber.ToString().Substring(Request.CardNumber.ToString().Length - 4, 4);
                                        paySearchReq.CardTypeId = Request.CardTypeId;
                                        paySearchReq.ProfileId = profileId;

                                        paySearchResp = dao.PaymentSearch(paySearchReq);

                                        if (paySearchResp.Code.Equals(Common.Utils.NEW_NO_RECORDS_FOUND))
                                        {
                                            createPaymentReq.Customer = customer;
                                            createPaymentReq.ExpMonth = Request.ExpMonth.ToString();
                                            createPaymentReq.ExpYear = Request.ExpYear.ToString();
                                            createPaymentReq.FullCardNumber = Request.CardNumber;
                                            createPaymentReq.ProfileId = Convert.ToInt64(profileId);
                                            
                                            createPaymentResp = wcf.CreatePaymentProfile(createPaymentReq);

                                            if (createPaymentResp.Code.Equals(Common.Utils.APPROVED))
                                            {
                                                paymentSaveResponse = dao.SavePaymentProfile(createPaymentResp.PaymentProfileId,
                                                    Convert.ToInt64(profileId), Request.CardTypeId, Request.CardNumber,
                                                    Request.ExpMonth, Request.ExpYear, Request.FirstName,
                                                    Request.LastName, 0, Locale, this.AppUser, Request.City,
                                                    Request.State, Request.ZipCode, Request.Address,
                                                    Request.Phone.ToString());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {

                    }
                }
            }
            catch (CustomException ex)
            {
                response.Code = ex.Code;
                response.Message = ex.Description;
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                exception = ex.Message;
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "SALE:", false, true);
                Common.Utils.Concat(ref output, "Session Id", Request.SessionId, false, false);
                Common.Utils.Concat(ref output, "User Id", valid.UserId, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Exception", exception, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "SaleWithProfileAndPaymentProfile")]
        public SaleResponse SaleWithProfileAndPaymentProfile(SaleWithProfileAndPaymentProfileRequest Request)
        {
            String exception = String.Empty, ip = String.Empty, tempProfileId = String.Empty, profileId = String.Empty;
            StringBuilder output = new StringBuilder();
            ValidateResponse valid = new ValidateResponse();
            SaleLocalResponse saleLocal = new SaleLocalResponse();
            SaleResponse response = new SaleResponse();
            Response dbResponse = new Response();

            try
            {
                ip = this.GetIp();

                if (Request == null)
                    throw new ValueRequireException("Request");

                if (String.IsNullOrEmpty(Request.SessionId))
                    throw new ValueRequireException("SessionId");

                if (Request.PaymentId==0)
                    throw new ValueRequireException("PaymentId");

                if (Request.ProfileId==0)
                    throw new ValueRequireException("ProfileId");

                if (String.IsNullOrEmpty(Request.Reference))
                    throw new ValueRequireException("Reference");

                using (SessionDao dao = new SessionDao())
                {
                    valid = dao.Validate(Request.SessionId, ip, Locale);

                    if (!valid.Code.Equals(Common.Utils.APPROVED))
                    {
                        throw new CustomException(valid.Code, valid.Message);
                    }
                }

                using (TransDao dao = new TransDao())
                {
                    saleLocal = dao.SalePayment(Request, valid.UserId, Locale);
                }

                if (saleLocal.Code.Equals(Common.Utils.APPROVED))
                {
                    if (saleLocal.ProviderCode == AUTHORIZE)
                    {
                        using (auth_wcf.IauthClient wcf = new auth_wcf.IauthClient())
                        {
                            using (EntityDao dao = new EntityDao())
                            {
                                ProfileSearchRequest proReq = new ProfileSearchRequest();
                                ProfileSearchResponse proResp = null;
                                PaymentSearchRequest payReq = new PaymentSearchRequest();
                                PaymentSearchResponse payResp = null;

                                proReq.ProfileId = Request.ProfileId;
                                proReq.StatusId = 1;

                                proResp = dao.ProfileSearch(proReq);

                                if (!proResp.Code.Equals(Common.Utils.APPROVED))
                                {
                                    throw new ProfileNotActiveException();
                                }

                                payReq.ProfileId = Request.ProfileId.ToString();
                                payReq.PaymentId = Request.PaymentId;
                                payReq.StatusId = 1;

                                payResp = dao.PaymentSearch(payReq);

                                if (!payResp.Code.Equals(Common.Utils.APPROVED))
                                {
                                    throw new PaymentNotActiveException();
                                }
                            }

                            auth_wcf.SaleWithPaymentProfileRequest request = new auth_wcf.SaleWithPaymentProfileRequest();
                            auth_wcf.SaleResponse saleResponse = null;

                            request.Amount = Request.Amount;
                            request.InvoiceNumber = saleLocal.LogId.ToString();
                            request.PaymentProfileId = Request.PaymentId;
                            request.ProfileId = Request.ProfileId;
                            
                            saleResponse = wcf.SaleWithPaymentProfile(request);

                            using (TransDao dao = new TransDao())
                            {
                                TransResponseRequest req = new TransResponseRequest();

                                req.AuthCode = saleResponse.AuthorizationCode;

                                if (!String.IsNullOrEmpty(saleResponse.InvoiceNumber))
                                {
                                    req.InvoiceNumber = Convert.ToInt64(saleResponse.InvoiceNumber);
                                    response.InvoiceNumber = saleResponse.InvoiceNumber;
                                }

                                req.LogId = saleLocal.LogId;
                                req.ResponseCode = saleResponse.Code;
                                req.ResponseMessage = saleResponse.Description;

                                if (!String.IsNullOrEmpty(saleResponse.TransactionId))
                                {
                                    req.TransactionId = Convert.ToInt64(saleResponse.TransactionId);
                                    response.TransactionId = saleResponse.TransactionId;
                                }

                                dbResponse = dao.Response(req, valid.UserId, Locale, AppUser);

                                response.Code = saleResponse.Code;
                                response.Message = saleResponse.Description;
                                response.AuthorizationCode = req.AuthCode;
                            }
                        }
                    }
                    else
                    {

                    }
                }
            }
            catch (CustomException ex)
            {
                response.Code = ex.Code;
                response.Message = ex.Description;
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                exception = ex.Message;
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "SALE:", false, true);
                Common.Utils.Concat(ref output, "Session Id", Request.SessionId, false, false);
                Common.Utils.Concat(ref output, "User Id", valid.UserId, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Exception", exception, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }



        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "DeleteProfile")]
        public Response DeleteProfile(DeleteProfileIdRequest Request)
        {
            String exception = String.Empty, ip = String.Empty, tempProfileId = String.Empty, profileId = String.Empty;
            StringBuilder output = new StringBuilder();
            ValidateResponse valid = new ValidateResponse();
            Response dbResp = new Response();
            Response response = new Response();

            try
            {
                ip = this.GetIp();

                if (Request == null)
                    throw new ValueRequireException("Request");

                if (String.IsNullOrEmpty(Request.SessionId))
                    throw new ValueRequireException("SessionId");

                if (String.IsNullOrEmpty(Request.ProfileId))
                    throw new ValueRequireException("ProfileId");

                using (SessionDao dao = new SessionDao())
                {
                    valid = dao.Validate(Request.SessionId, ip, Locale);

                    if (!valid.Code.Equals(Common.Utils.APPROVED))
                    {
                        throw new CustomException(valid.Code, valid.Message);
                    }
                }
                using (auth_wcf.IauthClient wcf = new auth_wcf.IauthClient())
                {
                    auth_wcf.DeleteProfileRequest proReq = new auth_wcf.DeleteProfileRequest();
                    auth_wcf.Response proResp = null;

                    proReq.ProfileId = Convert.ToInt64(Request.ProfileId);


                    proResp = wcf.DeleteProfile(proReq);

                    response.Code = proResp.Code;
                    response.Message = proResp.Description;

                    if (response.Code.Equals(Common.Utils.APPROVED))
                    {
                        using (EntityDao dao = new EntityDao())
                        {
                            dbResp = dao.RemoveProfile(Convert.ToInt64(Request.ProfileId), Locale, this.AppUser);
                        }
                    }
                }           
                 
            }
            catch (CustomException ex)
            {
                response.Code = ex.Code;
                response.Message = ex.Description;
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                exception = ex.Message;
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "DELETE_PROFILE:", false, true);
                Common.Utils.Concat(ref output, "Session Id", Request.SessionId, false, false);
                Common.Utils.Concat(ref output, "User Id", valid.UserId, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Exception", exception, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "DeletePaymentProfile")]
        public Response DeletePaymentProfile(DeletePaymentIdRequest Request)
        {
            String exception = String.Empty, ip = String.Empty, tempProfileId = String.Empty, profileId = String.Empty;
            StringBuilder output = new StringBuilder();
            ValidateResponse valid = new ValidateResponse();
            Response dbResp = new Response();
            Response response = new Response();

            try
            {
                ip = this.GetIp();

                if (Request == null)
                    throw new ValueRequireException("Request");

                if (String.IsNullOrEmpty(Request.SessionId))
                    throw new ValueRequireException("SessionId");

                if (String.IsNullOrEmpty(Request.PaymentId))
                    throw new ValueRequireException("PaymentId");

                using (SessionDao dao = new SessionDao())
                {
                    valid = dao.Validate(Request.SessionId, ip, Locale);

                    if (!valid.Code.Equals(Common.Utils.APPROVED))
                    {
                        throw new CustomException(valid.Code, valid.Message);
                    }
                }
                using (auth_wcf.IauthClient wcf = new auth_wcf.IauthClient())
                {
                    auth_wcf.DeletePaymentProfileRequest proReq = new auth_wcf.DeletePaymentProfileRequest();
                    auth_wcf.Response proResp = null;

                    proReq.ProfileId = Convert.ToInt64(Request.ProfileId);
                    proReq.PaymentId = Convert.ToInt64(Request.PaymentId);


                    proResp = wcf.DeletePaymentProfile(proReq);

                    response.Code = proResp.Code;
                    response.Message = proResp.Description;

                    if (response.Code.Equals(Common.Utils.APPROVED) || response.Code.Equals(Common.Utils.AUTHORIZE_PAYMENT_PROFILE_INVALID))
                    {
                        using (EntityDao dao = new EntityDao())
                        {
                            dbResp = dao.RemovePayment(Convert.ToInt64(Request.PaymentId), Locale, this.AppUser);
                        }

                        response.Code = dbResp.Code;
                        response.Message = dbResp.Message;
                        
                    }
                }

            }
            catch (CustomException ex)
            {
                response.Code = ex.Code;
                response.Message = ex.Description;
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                exception = ex.Message;
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "DELETE_PAYMENT_PROFILE:", false, true);
                Common.Utils.Concat(ref output, "Session Id", Request.SessionId, false, false);
                Common.Utils.Concat(ref output, "User Id", valid.UserId, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Exception", exception, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }


        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "ChangePaymentProfileStatus")]
        public Response ChangePaymentProfileStatus(ChangePaymentRequest Request)
        {
            String exception = String.Empty, ip = String.Empty, tempProfileId = String.Empty, profileId = String.Empty;
            StringBuilder output = new StringBuilder();
            ValidateResponse valid = new ValidateResponse();
            Response dbResp = new Response();
            Response response = new Response();

            try
            {
                ip = this.GetIp();

                if (Request == null)
                    throw new ValueRequireException("Request");

                if (String.IsNullOrEmpty(Request.SessionId))
                    throw new ValueRequireException("SessionId");

                if (String.IsNullOrEmpty(Request.PaymentId))
                    throw new ValueRequireException("PaymentId");

                using (SessionDao dao = new SessionDao())
                {
                    valid = dao.Validate(Request.SessionId, ip, Locale);

                    if (!valid.Code.Equals(Common.Utils.APPROVED))
                    {
                        throw new CustomException(valid.Code, valid.Message);
                    }
                }

                using (EntityDao dao = new EntityDao())
                {
                    dbResp = dao.ChangePaymentStatus(Convert.ToInt64(Request.PaymentId), Request.StatusId, Locale, this.AppUser);
                }

                response.Code = dbResp.Code;
                response.Message = dbResp.Message;
            }
            catch (CustomException ex)
            {
                response.Code = ex.Code;
                response.Message = ex.Description;
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                exception = ex.Message;
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "DELETE_PAYMENT_PROFILE:", false, true);
                Common.Utils.Concat(ref output, "Session Id", Request.SessionId, false, false);
                Common.Utils.Concat(ref output, "User Id", valid.UserId, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Exception", exception, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "ChangeAutoRecharge")]
        public Response ChangeAutoRecharge(AutoRechargeRequest Request)
        {
            String exception = String.Empty, ip = String.Empty, tempProfileId = String.Empty, profileId = String.Empty;
            StringBuilder output = new StringBuilder();
            ValidateResponse valid = new ValidateResponse();
            Response dbResp = new Response();
            Response response = new Response();

            try
            {
                ip = this.GetIp();

                if (Request == null)
                    throw new ValueRequireException("Request");

                if (String.IsNullOrEmpty(Request.SessionId))
                    throw new ValueRequireException("SessionId");

                using (SessionDao dao = new SessionDao())
                {
                    valid = dao.Validate(Request.SessionId, ip, Locale);

                    if (!valid.Code.Equals(Common.Utils.APPROVED))
                    {
                        throw new CustomException(valid.Code, valid.Message);
                    }
                }

                using (EntityDao dao = new EntityDao())
                {
                    dbResp = dao.AutoRecharge(Request.ProfileId, Request.AutoRecharge,
                        Request.AutoPaymentId, Request.AutoRechargeAmount,
                        Request.AutoFloorAmount, Locale, this.AppUser);
                }

                response.Code = dbResp.Code;
                response.Message = dbResp.Message;
            }
            catch (CustomException ex)
            {
                response.Code = ex.Code;
                response.Message = ex.Description;
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                exception = ex.Message;
            }
            finally
            {
                Common.Utils.Concat(ref output, null, "CHANGE_AUTO_RECHARGE:", false, true);
                Common.Utils.Concat(ref output, "Session Id", Request.SessionId, false, false);
                Common.Utils.Concat(ref output, "User Id", valid.UserId, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Exception", exception, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }

        private String GetIp()
        {
            OperationContext context = null;
            MessageProperties messageProperties = null;
            RemoteEndpointMessageProperty endpointProperty = null;
            String ip = null;

            context = OperationContext.Current;

            messageProperties = context.IncomingMessageProperties;
            endpointProperty = messageProperties[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;

            ip = endpointProperty.Address;

            return ip;
        }
    }
}
