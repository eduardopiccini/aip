﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Security;
using System.Text;
using log4net;
using System.Globalization;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using Common;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using DataAccessLayer.DataAccessObjects;
using System.Net.Security;
using System.Data;
using DataAccessLayer.EntityBase;
using System.Threading;

namespace AccountsWs
{



    /// <summary>
    /// Summary description for Service
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Service2 : System.Web.Services.WebService
    {

        private const String USER_NAME_REQUIRE = "AC1";
        private const String PASSWORD_REQUIRE = "AC2";
        private const String ACCOUNT_ID_REQUIRE = "AC3";
        private const String REFERENCE_REQUIRE = "AC4";
        private const String PRODUCT_ID_REQUIRE = "AC5";
        private const String AMOUNT_REQUIRE = "AC7";
        private const String PHONE_REQUIRE = "AC8";
        private const String USER_NOT_AUTHORIZED = "AC9";
        private const String UNCAUGHT_ERROR = "AC10";
        private const String UNCAUGHT_ERROR_MESSAGE = "TECHNICAL ERROR";
        private const String TRANSACTION_ID_REQUIRE = "AC11";
        private const String AUTHORIZATION_NUMBER_REQUIRE = "AC12";
        private const String NO_RECORDS_FOUND = "AC13";
        private const String COUNTRY_ID_REQUIRE = "AC14";
        private const String DATE_REQUIRE = "AC15";
        private const String INVALID_DATE = "DL3";
        private const String AT_LEAST_TWO = "AC15";

        private readonly String LOCAL_PIN = System.Configuration.ConfigurationManager.AppSettings["local_pin"].ToString();
        private readonly String PIN = System.Configuration.ConfigurationManager.AppSettings["pin"].ToString();
        private readonly String RECHARGE = System.Configuration.ConfigurationManager.AppSettings["recharge"].ToString();

        

        private static readonly ILog log = LogManager.GetLogger("AccountWs");

        public static String Locale;
        public String UserDb;
        private String TestService;
        

        public Service2()
        {
            String conn = String.Empty;

            conn = System.Configuration.ConfigurationManager.AppSettings["ParamConn"].ToString();
            Locale = CultureInfo.CurrentCulture.Name;
            this.UserDb = System.Configuration.ConfigurationManager.AppSettings["user_db"].ToString();
            

            if (conn.Trim().Contains("SID = pre".Trim()))
            {
                TestService = "Y";
            }
            else
            {
                TestService = "N";
            }
        }


        [WebMethod]
        public Common.TransactionResult Buy(String reference, String merchant, String productId, String amount, String phone,
            String user, String password, String accountId)
        {
            String details = String.Empty, ip = String.Empty,
                encode = String.Empty, decode = String.Empty, routingId = String.Empty,
                transactionTimeout = String.Empty, transactionId = String.Empty, timeoutException = String.Empty;
            Common.TransactionResult result = new Common.TransactionResult();
            Transaction transaction = new Transaction();
            StringBuilder output = new StringBuilder();
            DataAccessLayer.DataAccessObjects.RoutingResult routingResult = new DataAccessLayer.DataAccessObjects.RoutingResult();
            Common.Result returnResponse;
            WsProvider.RoutingResult providerResult = new WsProvider.RoutingResult();
            RoutingTransactionLogTimeoutResult timeoutResult = new RoutingTransactionLogTimeoutResult();
            DateTime start = DateTime.Now, end;
            DateTime? providerStart = null, providerEnd = null;
            String estadoCliente = "CONNECTED";
            RoutingDAO routingDao = new RoutingDAO();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = null;

                    loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(loginResult.Code);
                    }
                }

                using (EncryptDAO dao = new EncryptDAO())
                {
                    password = dao.Encrypt(password);
                }

                using (RoutingDAO dao = new RoutingDAO())
                {
                    routingResult = dao.Buy(user, password, accountId, productId, amount, phone, reference, ip, merchant, null, "0", Locale, UserDb);
                }

                result.Message = routingResult.Message;
                result.Details = routingResult.Details;

                if (!String.IsNullOrEmpty(routingResult.LogId))
                {
                    if (!routingResult.LogId.ToLower().Equals("null"))
                    {
                        transactionId = routingResult.LogId;
                    }
                    else
                    {
                        routingResult.LogId = String.Empty;
                    }
                }

                if (routingResult.Code.Equals("0"))
                {
                    transactionTimeout = routingResult.RoutingProvider[0].TransactionTimeout;
                    providerResult.Code = routingResult.Code;
                    providerResult.Message = routingResult.Message;
                    providerResult.RoutingProvider = new WsProvider.RoutingProvider[routingResult.RoutingProvider.Count];

                    for(int i = 0; i < routingResult.RoutingProvider.Count; i++)
                    {
                        WsProvider.RoutingProvider pro = new WsProvider.RoutingProvider();

                        pro.CountryCode = routingResult.RoutingProvider[i].CountryCode;
                        pro.CurrencyAmount = routingResult.RoutingProvider[i].CurrencyAmount;
                        pro.DeckProductId = routingResult.RoutingProvider[i].DeckProductId;
                        pro.Ip = routingResult.RoutingProvider[i].Ip;
                        pro.LocalCurrencyAmount = routingResult.RoutingProvider[i].LocalCurrencyAmount;
                        pro.LogId = routingResult.RoutingProvider[i].LogId;
                        pro.OperatorCode = routingResult.RoutingProvider[i].OperatorCode;
                        pro.Phone = routingResult.RoutingProvider[i].Phone;
                        pro.Port = routingResult.RoutingProvider[i].Port;
                        pro.ProductCode = routingResult.RoutingProvider[i].ProductCode;
                        pro.ProductId = routingResult.RoutingProvider[i].ProductId;
                        pro.ProductName = routingResult.RoutingProvider[i].ProductName;
                        pro.ProductTypeId = routingResult.RoutingProvider[i].ProductTypeId;
                        pro.ProfileId = routingResult.RoutingProvider[i].ProfileId;
                        pro.ProviderAccountId = routingResult.RoutingProvider[i].ProviderAccountId;
                        pro.ProviderId = routingResult.RoutingProvider[i].ProviderId;
                        pro.ProviderName = routingResult.RoutingProvider[i].ProviderName;
                        pro.ProviderOperatorId = routingResult.RoutingProvider[i].ProviderOperatorId;
                        pro.RechargeStamp = routingResult.RoutingProvider[i].RechargeStamp;
                        pro.RoutingId = routingResult.RoutingProvider[i].RoutingId;
                        pro.TemplateId = routingResult.RoutingProvider[i].TemplateId;
                        pro.TerminalId = routingResult.RoutingProvider[i].TerminalId;
                        pro.TransactionMode = routingResult.RoutingProvider[i].TransactionMode;
                        pro.TransactionTimeout = routingResult.RoutingProvider[i].TransactionTimeout;
                        pro.Url = routingResult.RoutingProvider[i].Url;
                        pro.VoidRoutingId = routingResult.RoutingProvider[i].VoidRoutingId;
                        pro.Weight = routingResult.RoutingProvider[i].Weight;
                        pro.SecondStopRouting = routingResult.RoutingProvider[i].SecondStopRouting;
                        
                        routingId = pro.RoutingId;

                        providerResult.RoutingProvider[i] = pro;
                    }

                    if (String.IsNullOrEmpty(transactionTimeout))
                    {
                        transactionTimeout = "0";
                    }

                    providerStart = DateTime.Now;

                    try
                    {
                        if (!Context.Response.IsClientConnected)
                        {
                            estadoCliente = "DISCONNECTED";
                        }
                    }
                    catch (Exception)
                    {

                    }

                    #region Verificar estado del cliente

                    if (!estadoCliente.Equals("DISCONNECTED"))
                    {
                        #region Transaccion con timeout o no

                        if (transactionTimeout.Equals("0"))
                        {
                            using (WsProvider.Service ws = new WsProvider.Service())
                            {
                                transaction = ws.Buy(providerResult, TestService, phone, amount, null, accountId, Locale,
                                    UserDb, user, ip, null);
                            }

                            timeoutException = "No Timeout";
                        }
                        else
                        {
                            using (WsProvider.Service ws = new WsProvider.Service())
                            {
                                try
                                {
                                    ws.Timeout = Convert.ToInt32(transactionTimeout) * 1000;

                                    transaction = ws.Buy(providerResult, TestService, phone, amount, null, accountId, Locale,
                                        UserDb, user, ip, null);

                                    timeoutException = "No Timeout";
                                }
                                catch (Exception ex)
                                {
                                    timeoutException = ex.Message;
                                    transaction = null;
                                }
                            }


                            if (transaction == null)
                            {
                                using (RoutingDAO dao = new RoutingDAO())
                                {
                                    timeoutResult = dao.TransLogTimeout(providerResult.RoutingProvider[0].LogId, Locale, user);
                                }

                                transaction = new WsProvider.Transaction();
                                transaction.Code = timeoutResult.ResponseCodeId;
                                transaction.Message = timeoutResult.ResponseMessage;

                                if (transaction.Code.Equals("00") || transaction.Code.Equals("0"))
                                {
                                    transaction.Code = Common.Utils.APPROVED;
                                    transaction.Message = Common.Utils.APPROVED_MESSAGE;
                                }

                                transaction.TransactionId = timeoutResult.TransactionId;
                                transaction.TransactionDateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                transaction.AuthorizationNumber = timeoutResult.ProviderReference;
                            }
                        }

                        #endregion
                    }
                    else
                    {
                        transaction = new WsProvider.Transaction();
                        
                        transaction.Code = Common.Utils.CLIENT_SESSION_NOT_AVAILABLE_CODE;
                        transaction.Message = Common.Utils.CLIENT_SESSION_NOT_AVAILABLE_MESSAGE;
                        transaction.TransactionId = transactionId;

                        RoutingTransactionLogResult tranResult = routingDao.TransLog(routingId, DateTime.Now, DateTime.Now,
                        transaction.Code,
                        transaction.Message,
                        null,
                        null,
                        null,
                        Locale,
                        this.UserDb,
                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    }
                    #endregion

                    providerEnd = DateTime.Now;

                    result.Code = transaction.Code;
                    result.AuthorizationNumber = transaction.AuthorizationNumber;
                    result.Id = transaction.TransactionId;
                    result.Message = transaction.Message;
                    result.PinNumber = transaction.PinNumber;
                    result.SerialNumber = transaction.SerialNumber;
                    result.TransactionDateTime = transaction.TransactionDateTime;
                    result.TransactionId = transaction.TransactionId;
                    details = transaction.Details;
                }
                else
                {
                    try
                    {
                        long temp;

                        if (!Int64.TryParse(routingResult.LogId, out temp))
                        {
                            routingResult.LogId = String.Empty;
                        }
                    }
                    catch (Exception)
                    {
                        routingResult.LogId = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(routingResult.LogId))
                    {
                        using (ApiLogDAO apiLogDao = new ApiLogDAO())
                        {
                            returnResponse = apiLogDao.SearchApiLogResponseCode(routingResult.LogId);
                        }
                        

                        if (returnResponse.Code.Equals(Common.Utils.NO_RECORDS_FOUND) ||
                            returnResponse.Code.Equals(Common.Utils.UNKNOWN_EXCEPTION))
                        {
                            result.Code = routingResult.Code;
                            result.Message = routingResult.Message;
                        }
                        else
                        {
                            result.Code = returnResponse.Code;
                            result.Message = returnResponse.Message;
                        }
                    }
                    else
                    {
                        result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                try
                {
                    if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                    {
                        using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                        {
                            ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                            if (res.Code.Equals("0"))
                            {
                                result.Code = res.ResponseCode[0].Code;
                                result.Message = res.ResponseCode[0].Message;
                            }
                            else
                            {
                                result.Code = UNCAUGHT_ERROR;
                                result.Message = UNCAUGHT_ERROR_MESSAGE;
                            }
                        }
                    }

                    end = DateTime.Now;

                    output.AppendLine("");
                    output.AppendLine("***********************************************");
                    output.AppendLine("Accounts Web Service - Buy");
                    output.Append("Start Date Time              : ").AppendLine(start.ToShortDateString() + " " + start.ToLongTimeString());
                    output.Append("End Date Time                : ").AppendLine(end.ToShortDateString() + " " + end.ToLongTimeString());

                    if (providerStart != null)
                    {
                        output.Append("Provider Start Date Time     : ").AppendLine(providerStart.Value.ToShortDateString() + " " + providerStart.Value.ToLongTimeString());
                    }

                    if (providerEnd != null)
                    {
                        output.Append("Provider End Date Time       : ").AppendLine(providerEnd.Value.ToShortDateString() + " " + providerEnd.Value.ToLongTimeString());
                    }

                    output.Append("Transaction Timeout          : ").AppendLine(transactionTimeout);
                    output.Append("Transaction Exception        : ").AppendLine(timeoutException);
                    output.Append("Transaction Id               : ").AppendLine(result.TransactionId);
                    output.Append("Reference                    : ").AppendLine(reference);
                    output.Append("Account Id                   : ").AppendLine(accountId);
                    output.Append("Product Id                   : ").AppendLine(productId);
                    output.Append("Amount                       : ").AppendLine(amount);
                    output.Append("Phone                        : ").AppendLine(phone);
                    output.Append("User                         : ").AppendLine(user);
                    output.Append("Account                      : ").AppendLine(accountId);
                    output.Append("Ip                           : ").AppendLine(ip);
                    output.Append("Code                         : ").AppendLine(result.Code);
                    output.Append("Message                      : ").AppendLine(result.Message);
                    output.Append("Authorization Number         : ").AppendLine(result.AuthorizationNumber);
                    output.Append("Pin Number                   : ").AppendLine(result.PinNumber);
                    output.Append("Serial Number                : ").AppendLine(result.SerialNumber);
                    output.Append("Transaction Date             : ").AppendLine(result.TransactionDateTime);
                    output.Append("Details                      : ").AppendLine(details);
                    output.AppendLine("***********************************************");

                    log.Debug(output.ToString());
                }
                catch (Exception ex)
                {
                    log.Debug(transactionId + " Write Log Error! " + ex.Message);
                }
            }

            try
            {
                try
                {
                    if (!Context.Response.IsClientConnected)
                    {
                        estadoCliente = "DISCONNECTED";
                    }

                    log.Debug("ESTADO CLIENTE: " + estadoCliente);
                }
                catch (Exception)
                {

                }

                log.Debug("Before " + transactionId + " Answer");
                Common.Result resultAns = null;

                using (RoutingDAO dao = new RoutingDAO())
                {
                    resultAns = dao.Answered(transactionId, estadoCliente);
                }

                

                log.Debug("After " + transactionId + " Answered! Code: " + resultAns.Code + " " + resultAns.Message + " Details: " + resultAns.Details);
            
                
            }
            catch (Exception ex) 
            {
                if (String.IsNullOrEmpty(transactionId))
                {
                    transactionId = "N/A";
                }

                log.Debug(transactionId + " Answered " + ex.Message);
            }

            

            return result;
        }


       
        [WebMethod]
        public Common.TransactionResult BuySms(String reference, String merchant,
            String productId, String phone, String message, String user, String password, String accountId)
        {
            String details = String.Empty, ip = String.Empty,
                encode = String.Empty, timeoutException = String.Empty,
                transactionId = String.Empty, transactionTimeout = String.Empty;
            Common.TransactionResult result = new Common.TransactionResult();
            Transaction transaction = new Transaction();
            StringBuilder output = new StringBuilder();
            DataAccessLayer.DataAccessObjects.RoutingResult routingResult = new DataAccessLayer.DataAccessObjects.RoutingResult();
            Common.Result returnResponse;
            RoutingTransactionLogTimeoutResult timeoutResult = new RoutingTransactionLogTimeoutResult();
            RoutingResult providerResult = new RoutingResult();
            DateTime start = DateTime.Now, end;
            DateTime? providerStart = null, providerEnd = null;
            String estadoCliente = "CONNECTED";

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = null;

                    loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(USER_NOT_AUTHORIZED);
                    }
                }

                using (EncryptDAO dao = new EncryptDAO())
                {
                    password = dao.Encrypt(password);
                }

                using (RoutingDAO dao = new RoutingDAO())
                {
                    routingResult = dao.Buy(user, password, accountId,
                        productId, null, phone, reference, ip, merchant, message, "0", Locale, UserDb);
                }

                result.Message = routingResult.Message;
                result.Details = routingResult.Details;

                if (!String.IsNullOrEmpty(routingResult.LogId))
                {
                    if (!routingResult.LogId.ToLower().Equals("null"))
                    {
                        transactionId = routingResult.LogId;
                    }
                    else
                    {
                        routingResult.LogId = String.Empty;
                    }
                }

                if (routingResult.Code.Equals("0"))
                {
                    transactionTimeout = routingResult.RoutingProvider[0].TransactionTimeout;
                    providerResult.Code = routingResult.Code;
                    providerResult.Message = routingResult.Message;
                    providerResult.RoutingProvider = new WsProvider.RoutingProvider[routingResult.RoutingProvider.Count];

                    for (int i = 0; i < routingResult.RoutingProvider.Count; i++)
                    {
                        WsProvider.RoutingProvider pro = new WsProvider.RoutingProvider();

                        pro.CountryCode = routingResult.RoutingProvider[i].CountryCode;
                        pro.CurrencyAmount = routingResult.RoutingProvider[i].CurrencyAmount;
                        pro.DeckProductId = routingResult.RoutingProvider[i].DeckProductId;
                        pro.Ip = routingResult.RoutingProvider[i].Ip;
                        pro.LocalCurrencyAmount = routingResult.RoutingProvider[i].LocalCurrencyAmount;
                        pro.LogId = routingResult.RoutingProvider[i].LogId;
                        pro.OperatorCode = routingResult.RoutingProvider[i].OperatorCode;
                        pro.Phone = routingResult.RoutingProvider[i].Phone;
                        pro.Port = routingResult.RoutingProvider[i].Port;
                        pro.ProductCode = routingResult.RoutingProvider[i].ProductCode;
                        pro.ProductId = routingResult.RoutingProvider[i].ProductId;
                        pro.ProductName = routingResult.RoutingProvider[i].ProductName;
                        pro.ProductTypeId = routingResult.RoutingProvider[i].ProductTypeId;
                        pro.ProfileId = routingResult.RoutingProvider[i].ProfileId;
                        pro.ProviderAccountId = routingResult.RoutingProvider[i].ProviderAccountId;
                        pro.ProviderId = routingResult.RoutingProvider[i].ProviderId;
                        pro.ProviderName = routingResult.RoutingProvider[i].ProviderName;
                        pro.ProviderOperatorId = routingResult.RoutingProvider[i].ProviderOperatorId;
                        pro.RechargeStamp = routingResult.RoutingProvider[i].RechargeStamp;
                        pro.RoutingId = routingResult.RoutingProvider[i].RoutingId;
                        pro.TemplateId = routingResult.RoutingProvider[i].TemplateId;
                        pro.TerminalId = routingResult.RoutingProvider[i].TerminalId;
                        pro.TransactionMode = routingResult.RoutingProvider[i].TransactionMode;
                        pro.TransactionTimeout = routingResult.RoutingProvider[i].TransactionTimeout;
                        pro.Url = routingResult.RoutingProvider[i].Url;
                        pro.VoidRoutingId = routingResult.RoutingProvider[i].VoidRoutingId;
                        pro.Weight = routingResult.RoutingProvider[i].Weight;

                        providerResult.RoutingProvider[i] = pro;
                    }

                    if (String.IsNullOrEmpty(transactionTimeout))
                    {
                        transactionTimeout = "0";
                    }

                    providerStart = DateTime.Now;

                    if (transactionTimeout.Equals("0"))
                    {
                        using (WsProvider.Service ws = new WsProvider.Service())
                        {
                            transaction = ws.Buy(providerResult, TestService, phone, null, message, accountId, Locale,
                                UserDb, user, ip, null);
                        }

                        timeoutException = "No Timeout";
                    }
                    else
                    {
                        using (WsProvider.Service ws = new WsProvider.Service())
                        {
                            try
                            {
                                ws.Timeout = Convert.ToInt32(transactionTimeout) * 1000;

                                transaction = ws.Buy(providerResult, TestService, phone, null, message, accountId, Locale,
                                    UserDb, user, ip, null);

                                timeoutException = "No Timeout";
                            }
                            catch (Exception ex)
                            {
                                timeoutException = ex.Message;
                                transaction = null;
                            }
                        }


                        if (transaction == null)
                        {
                            using (RoutingDAO dao = new RoutingDAO())
                            {
                                timeoutResult = dao.TransLogTimeout(providerResult.RoutingProvider[0].LogId, Locale, user);
                            }

                            transaction = new WsProvider.Transaction();
                            transaction.Code = timeoutResult.ResponseCodeId;
                            transaction.Message = timeoutResult.ResponseMessage;

                            if (transaction.Code.Equals("00") || transaction.Code.Equals("0"))
                            {
                                transaction.Code = Common.Utils.APPROVED;
                                transaction.Message = Common.Utils.APPROVED_MESSAGE;
                            }

                            transaction.TransactionId = timeoutResult.TransactionId;
                            transaction.TransactionDateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                            transaction.AuthorizationNumber = timeoutResult.ProviderReference;
                        }
                    }

                    providerEnd = DateTime.Now;

                    result.Code = transaction.Code;
                    result.AuthorizationNumber = transaction.AuthorizationNumber;
                    result.Id = transaction.TransactionId;
                    result.Message = transaction.Message;
                    result.TransactionDateTime = transaction.TransactionDateTime;
                    result.TransactionId = transaction.TransactionId;
                    details = transaction.Details;

                    if (result.Code.Equals("0") || result.Code.Equals("00"))
                    {
                        result.Code = Common.Utils.APPROVED;
                        result.Message = Common.Utils.APPROVED_MESSAGE;
                    }
                }
                else
                {
                    try
                    {
                        long temp;

                        if (!Int64.TryParse(routingResult.LogId, out temp))
                        {
                            routingResult.LogId = String.Empty;
                        }
                    }
                    catch (Exception)
                    {
                        routingResult.LogId = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(routingResult.LogId))
                    {
                        using (ApiLogDAO apiLogDao = new ApiLogDAO())
                        {
                            returnResponse = apiLogDao.SearchApiLogResponseCode(routingResult.LogId);    
                        }
                        
                        if (returnResponse.Code.Equals(Common.Utils.NO_RECORDS_FOUND) ||
                            returnResponse.Code.Equals(Common.Utils.UNKNOWN_EXCEPTION))
                        {
                            result.Code = routingResult.Code;
                            result.Message = routingResult.Message;
                        }
                        else
                        {
                            result.Code = returnResponse.Code;
                            result.Message = returnResponse.Message;
                        }
                    }
                    else
                    {
                        result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                try
                {
                    if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                    {
                        using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                        {
                            ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                            if (res.Code.Equals("0"))
                            {
                                result.Code = res.ResponseCode[0].Code;
                                result.Message = res.ResponseCode[0].Message;
                            }
                            else
                            {
                                result.Code = UNCAUGHT_ERROR;
                                result.Message = UNCAUGHT_ERROR_MESSAGE;
                            }
                        }
                    }

                    end = DateTime.Now;

                    output.AppendLine("");
                    output.AppendLine("***********************************************");
                    output.AppendLine("Accounts Web Service - BuySms");
                    output.Append("Start Date Time              : ").AppendLine(start.ToShortDateString() + " " + start.ToLongTimeString());
                    output.Append("End Date Time                : ").AppendLine(end.ToShortDateString() + " " + end.ToLongTimeString());

                    if (providerStart != null)
                    {
                        output.Append("Provider Start Date Time     : ").AppendLine(providerStart.Value.ToShortDateString() + " " + providerStart.Value.ToLongTimeString());
                    }

                    if (providerEnd != null)
                    {
                        output.Append("Provider End Date Time       : ").AppendLine(providerEnd.Value.ToShortDateString() + " " + providerEnd.Value.ToLongTimeString());
                    }

                    output.Append("Transaction Timeout          : ").AppendLine(transactionTimeout);
                    output.Append("Transaction Exception        : ").AppendLine(timeoutException);
                    output.Append("Transaction Id               : ").AppendLine(result.TransactionId);
                    output.Append("Reference                    : ").AppendLine(reference);
                    output.Append("Account Id                   : ").AppendLine(accountId);
                    output.Append("Product Id                   : ").AppendLine(productId);
                    output.Append("Message                      : ").AppendLine(message);
                    output.Append("Phone                        : ").AppendLine(phone);
                    output.Append("User                         : ").AppendLine(user);
                    output.Append("Account                      : ").AppendLine(accountId);
                    output.Append("Ip                           : ").AppendLine(ip);
                    output.Append("Code                         : ").AppendLine(result.Code);
                    output.Append("Message                      : ").AppendLine(result.Message);
                    output.Append("Authorization Number         : ").AppendLine(result.AuthorizationNumber);
                    output.Append("Pin Number                   : ").AppendLine(result.PinNumber);
                    output.Append("Serial Number                : ").AppendLine(result.SerialNumber);
                    output.Append("Transaction Date             : ").AppendLine(result.TransactionDateTime);
                    output.Append("Details                      : ").AppendLine(details);
                    output.Append("Encode                       : ").AppendLine(encode);
                    output.AppendLine("***********************************************");

                    log.Debug(output.ToString());
                }
                catch (Exception ex)
                {
                    log.Debug(transactionId + " Write Log Error! " + ex.Message);
                }

            }

            try
            {
                try
                {
                    if (!Context.Response.IsClientConnected)
                    {
                        estadoCliente = "DISCONNECTED";
                    }

                    log.Debug("ESTADO CLIENTE: " + estadoCliente);
                }
                catch (Exception)
                {

                }

                log.Debug("Before " + transactionId + " Answer");
                Common.Result resultAns = null;

                RoutingDAO routingDao = new RoutingDAO();

                resultAns = routingDao.Answered(transactionId, estadoCliente);

                log.Debug("After " + transactionId + " Answered! Code: " + resultAns.Code + " " + resultAns.Message + " Details: " + resultAns.Details);
            }
            catch (Exception ex)
            {
                if (String.IsNullOrEmpty(transactionId))
                {
                    transactionId = "N/A";
                }

                log.Debug(transactionId + " Answered " + ex.Message);
            }

            return result;
        }


       

        [WebMethod]
        public Response Void(String transactionId,
            String reference, String amount, String authorizationNumber, String phone, String user,
            String password, String accountId)
        {
            String details = "", ip = null, decode = null, encode = null;
            String[] decodeArray = null;
            WsProvider.Result resultVoid = null;
            Response result = new Response();
            StringBuilder output = new StringBuilder();
            EncryptDAO encDao = new EncryptDAO();
            ResponseCodeDAO responseCodeDao = new ResponseCodeDAO();
            WsProvider.Service wsProvider = new WsProvider.Service();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = null;

                    loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(USER_NOT_AUTHORIZED);
                    }
                }

                encode = Context.User.Identity.Name;

                decode = encDao.Decrypt(encode);

                decodeArray = decode.Split('#');

                user = decodeArray[0];
                accountId = decodeArray[1];
                password = decodeArray[2];

                resultVoid = wsProvider.Void(transactionId, user, password, accountId, reference,
                    amount, authorizationNumber, phone, ip, Locale, this.UserDb, user, ip, TestService);

                result.Code = resultVoid.Code;
                result.Message = resultVoid.Message;
                details = resultVoid.Details;

                if (result.Code.Equals("0"))
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                try
                {
                    if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                    {
                        ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            result.Code = res.ResponseCode[0].Code;
                            result.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            result.Code = UNCAUGHT_ERROR;
                            result.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }

                    output.AppendLine("");
                    output.AppendLine("***********************************************");
                    output.AppendLine("Accounts Web Service - Void ");
                    output.Append("Transaction Id       : ").AppendLine(transactionId);
                    output.Append("Reference            : ").AppendLine(reference);
                    output.Append("Account Id           : ").AppendLine(accountId);
                    output.Append("Amount               : ").AppendLine(amount);
                    output.Append("Authorization Number : ").AppendLine(authorizationNumber);
                    output.Append("Phone                : ").AppendLine(phone);
                    output.Append("User                 : ").AppendLine(user);
                    output.Append("Ip                   : ").AppendLine(ip);
                    output.Append("Code                 : ").AppendLine(result.Code);
                    output.Append("Message              : ").AppendLine(result.Message);
                    output.Append("Details              : ").AppendLine(details);
                    output.AppendLine("***********************************************");

                    log.Debug(output.ToString());
                }
                catch (Exception ex)
                {
                    log.Debug(transactionId + " Write Log Error! " + ex.Message);
                }

            }

            return result;
        }



        [WebMethod]
        public Response VoidByReference(String oldReference, String newReference, String amount, String phone,
            String user, String password, String accountId)
        {
            String details = "", ip = null;
            WsProvider.Result resultVoid = null;
            Response result = new Response();
            StringBuilder output = new StringBuilder();
            ResponseCodeDAO responseCodeDao = new ResponseCodeDAO();
            WsProvider.Service wsProvider = new WsProvider.Service();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = null;

                    loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(USER_NOT_AUTHORIZED);
                    }
                }

                resultVoid = wsProvider.VoidByReference(oldReference, user, password, accountId, newReference,
                    amount, phone, ip, Locale, this.UserDb, user, ip, TestService);

                result.Code = resultVoid.Code;
                result.Message = resultVoid.Message;
                details = resultVoid.Details;

                if (result.Code.Equals("0"))
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                try
                {
                    if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                    {
                        ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            result.Code = res.ResponseCode[0].Code;
                            result.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            result.Code = UNCAUGHT_ERROR;
                            result.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }

                    output.AppendLine("");
                    output.AppendLine("***********************************************");
                    output.AppendLine("Accounts Web Service - Void By Reference ");
                    output.Append("Old Reference            : ").AppendLine(oldReference);
                    output.Append("New Reference       : ").AppendLine(newReference);
                    output.Append("Account Id           : ").AppendLine(accountId);
                    output.Append("Amount               : ").AppendLine(amount);
                    output.Append("Phone                : ").AppendLine(phone);
                    output.Append("User                 : ").AppendLine(user);
                    output.Append("Ip                   : ").AppendLine(ip);
                    output.Append("Code                 : ").AppendLine(result.Code);
                    output.Append("Message              : ").AppendLine(result.Message);
                    output.Append("Details              : ").AppendLine(details);
                    output.AppendLine("***********************************************");

                    log.Debug(output.ToString());
                }
                catch (Exception ex)
                {
                    log.Debug(newReference + " " + oldReference  + " Write Log Error! " + ex.Message);
                }

            }

            return result;
        }


        [WebMethod]
        public BalanceResponse GetBalance(String user, String password, String accountId)
        {
            String details = "", ip = null;
            BalanceResponse result = new BalanceResponse();
            StringBuilder output = new StringBuilder();
            AccountResult accountResult = new AccountResult();
            ResponseCodeDAO responseCodeDao = new ResponseCodeDAO();
            AccountDAO accountDao = new AccountDAO();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = null;

                    loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(USER_NOT_AUTHORIZED);
                    }
                }

                accountResult = accountDao.SearchAccount(accountId, null, null, null, null, null, null, null, null, null, null, null, false, null);

                result.Code = accountResult.Code;
                result.Message = accountResult.Message;
                details = accountResult.Details;

                if (result.Code.Equals("0"))
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;
                    result.Balance = accountResult.Account[0].Balance.ToString();
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                    if (res.Code.Equals("0"))
                    {
                        result.Code = res.ResponseCode[0].Code;
                        result.Message = res.ResponseCode[0].Message;
                    }
                    else
                    {
                        result.Code = UNCAUGHT_ERROR;
                        result.Message = UNCAUGHT_ERROR_MESSAGE;
                    }
                }

                output.AppendLine("");
                output.AppendLine("***********************************************");
                output.AppendLine("Accounts Web Service - Get Balance");
                output.Append("Account Id       : ").AppendLine(accountId);
                output.Append("User             : ").AppendLine(user);
                output.Append("Ip               : ").AppendLine(ip);
                output.Append("Code             : ").AppendLine(result.Code);
                output.Append("Message          : ").AppendLine(result.Message);
                output.Append("Balance          : ").AppendLine(result.Balance);
                output.Append("Details          : ").AppendLine(details);
                output.AppendLine("***********************************************");

                log.Debug(output.ToString());
            }

            return result;
        }


        [WebMethod]
        public AccountTransactionResponse QueryTransactionsByPhone(String phone, String user, String password, String accountId)
        {
            String ip = null;
            String details = null;
            StringBuilder output = new StringBuilder();
            AccountTransactionResponse response = new AccountTransactionResponse();
            TransactionLogResult transactionLogResult = new TransactionLogResult();
            List<AccountTransactions> list = new List<AccountTransactions>();
            EncryptDAO encDao = new EncryptDAO();
            ResponseCodeDAO responseCodeDao = new ResponseCodeDAO();
            TransactionLogDAO transactionDao = new TransactionLogDAO();

            try
            {
                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = null;

                    loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(USER_NOT_AUTHORIZED);
                    }
                }


                transactionLogResult = transactionDao.Search(null, null, accountId, null, null, null, null, null,
                    null, null, null, null, null, null, null, phone, null, null, 0, 50);


                response.Code = transactionLogResult.Code;
                response.Message = transactionLogResult.Message;
                details = transactionLogResult.Details;

                if (transactionLogResult.Code.Equals("0"))
                {
                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;

                    foreach (TransactionLog item in transactionLogResult.TransactionLog)
                    {
                        list.Add(new AccountTransactions(item.LogId, item.Reference, item.Merchant,
                            item.ProductTypeId, item.ProductId, item.TransactionAmount, item.Phone,
                            item.ResponseCodeId, item.DateCreated, item.ProviderReference,
                            item.TransactionTypeId, item.TransactionTypeName));
                    }

                    response.AccountTransactions = list.ToArray();
                }

            }
            catch (Exception ex)
            {
                response.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(response.Message) && !String.IsNullOrEmpty(response.Code))
                {
                    ResponseCodeResult res = responseCodeDao.Search(response.Code, null, null);

                    if (res.Code.Equals("0"))
                    {
                        response.Code = res.ResponseCode[0].Code;
                        response.Message = res.ResponseCode[0].Message;
                    }
                    else
                    {
                        response.Code = UNCAUGHT_ERROR;
                        response.Message = UNCAUGHT_ERROR_MESSAGE;
                    }
                }

                output.AppendLine("");
                output.AppendLine("***********************************************");
                output.AppendLine("Accounts Web Service - Query Transactions By Phone");
                output.Append("Phone                : ").AppendLine(phone);
                output.Append("User                 : ").AppendLine(user);
                output.Append("Code                 : ").AppendLine(response.Code);
                output.Append("Message              : ").AppendLine(response.Message);
                output.Append("Details              : ").AppendLine(details);
                output.AppendLine("***********************************************");

                log.Debug(output.ToString());
            }

            return response;
        }


        [WebMethod]
        public AccountTransactionResponse QueryTransactionsById(String transactionId, String user, String password, String accountId)
        {
            String ip = null;
            String details = null;
            StringBuilder output = new StringBuilder();
            AccountTransactionResponse response = new AccountTransactionResponse();
            TransactionLogResult transactionLogResult = new TransactionLogResult();
            List<AccountTransactions> list = new List<AccountTransactions>();
            EncryptDAO encDao = new EncryptDAO();
            ResponseCodeDAO responseCodeDao = new ResponseCodeDAO();
            TransactionLogDAO transactionDao = new TransactionLogDAO();

            try
            {
                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = null;

                    loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(USER_NOT_AUTHORIZED);
                    }
                }


                transactionLogResult = transactionDao.Search(null, transactionId, accountId, null, null, null, null, null,
                    null, null, null, null, null, null, null, null, null, null, 0, 50);


                response.Code = transactionLogResult.Code;
                response.Message = transactionLogResult.Message;
                details = transactionLogResult.Details;

                if (transactionLogResult.Code.Equals("0"))
                {
                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;

                    foreach (TransactionLog item in transactionLogResult.TransactionLog)
                    {
                        list.Add(new AccountTransactions(item.LogId, item.Reference, item.Merchant,
                            item.ProductTypeId, item.ProductId, item.TransactionAmount, item.Phone,
                            item.ResponseCodeId, item.DateCreated, item.ProviderReference,
                            item.TransactionTypeId, item.TransactionTypeName));
                    }

                    response.AccountTransactions = list.ToArray();
                }

            }
            catch (Exception ex)
            {
                response.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(response.Message) && !String.IsNullOrEmpty(response.Code))
                {
                    ResponseCodeResult res = responseCodeDao.Search(response.Code, null, null);

                    if (res.Code.Equals("0"))
                    {
                        response.Code = res.ResponseCode[0].Code;
                        response.Message = res.ResponseCode[0].Message;
                    }
                    else
                    {
                        response.Code = UNCAUGHT_ERROR;
                        response.Message = UNCAUGHT_ERROR_MESSAGE;
                    }
                }

                output.AppendLine("");
                output.AppendLine("***********************************************");
                output.AppendLine("Accounts Web Service - Query Transactions By Transaction Id");
                output.Append("Transaction Id       : ").AppendLine(transactionId);
                output.Append("User                 : ").AppendLine(user);
                output.Append("Code                 : ").AppendLine(response.Code);
                output.Append("Message              : ").AppendLine(response.Message);
                output.Append("Details              : ").AppendLine(details);
                output.AppendLine("***********************************************");

                log.Debug(output.ToString());
            }

            return response;
        }


        [WebMethod]
        public AccountTransactionResponse QueryTransactionsByReference(String reference, String user, String password, String accountId)
        {
            String ip = null;
            String details = null;
            StringBuilder output = new StringBuilder();
            AccountTransactionResponse response = new AccountTransactionResponse();
            TransactionLogWsResult transactionLogWsResult = new TransactionLogWsResult();
            List<AccountTransactions> list = new List<AccountTransactions>();
            EncryptDAO encDao = new EncryptDAO();
            ResponseCodeDAO responseCodeDao = new ResponseCodeDAO();
            TransactionLogDAO transactionDao = new TransactionLogDAO();

            try
            {
                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = null;

                    loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(USER_NOT_AUTHORIZED);
                    }
                }

                transactionLogWsResult = transactionDao.SearchByReference(reference, accountId);


                response.Code = transactionLogWsResult.Code;
                response.Message = transactionLogWsResult.Message;

                if (transactionLogWsResult.Code.Equals("0"))
                {
                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;

                    foreach (TransactionLogWs item in transactionLogWsResult.TransactionLogWs)
                    {
                        list.Add(new AccountTransactions(item.TransactionId, reference, item.Merchant,
                            item.ProductTypeId, item.ProductId, item.Amount, item.Phone,
                            item.ResponseCode, item.DateCreated, item.ProviderReference,
                            item.TransactionTypeId, item.TransactionTypeName));
                    }

                    response.AccountTransactions = list.ToArray();
                }

            }
            catch (Exception ex)
            {
                response.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(response.Message) && !String.IsNullOrEmpty(response.Code))
                {
                    ResponseCodeResult res = responseCodeDao.Search(response.Code, null, null);

                    if (res.Code.Equals("0"))
                    {
                        response.Code = res.ResponseCode[0].Code;
                        response.Message = res.ResponseCode[0].Message;
                    }
                    else
                    {
                        response.Code = UNCAUGHT_ERROR;
                        response.Message = UNCAUGHT_ERROR_MESSAGE;
                    }
                }

                output.AppendLine("");
                output.AppendLine("***********************************************");
                output.AppendLine("Accounts Web Service - Query Transactions By Reference");
                output.Append("Reference            : ").AppendLine(reference);
                output.Append("User                 : ").AppendLine(user);
                output.Append("Code                 : ").AppendLine(response.Code);
                output.Append("Message              : ").AppendLine(response.Message);
                output.Append("Details              : ").AppendLine(details);
                output.AppendLine("***********************************************");

                log.Debug(output.ToString());
            }

            return response;
        }


        [WebMethod]
        public AccountsProductResult GetProducts(String user, String password, String accountId)
        {
            String details = String.Empty, ip = String.Empty;
            AccountsProductResult accountProductResult = new AccountsProductResult();
            StringBuilder output = new StringBuilder();
            AccountDAO accountDao = new AccountDAO();
            EncryptDAO encDao = new EncryptDAO();
            ResponseCodeDAO responseCodeDao = new ResponseCodeDAO();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = null;

                    loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(USER_NOT_AUTHORIZED);
                    }
                }

                accountProductResult = accountDao.SearchProducts(accountId, null, null, null, null, null,
                    null, null, null, "1", "1");

                details = accountProductResult.Details;

                if (accountProductResult.Code.Equals("0"))
                {
                    accountProductResult.Code = Common.Utils.APPROVED;
                    accountProductResult.Message = Common.Utils.APPROVED_MESSAGE;
                } 
            }
            catch (Exception ex)
            {
                accountProductResult.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(accountProductResult.Message) && !String.IsNullOrEmpty(accountProductResult.Code))
                {
                    ResponseCodeResult res = responseCodeDao.Search(accountProductResult.Code, null, null);

                    if (res.Code.Equals("0"))
                    {
                        accountProductResult.Code = res.ResponseCode[0].Code;
                        accountProductResult.Message = res.ResponseCode[0].Message;
                    }
                    else
                    {
                        accountProductResult.Code = UNCAUGHT_ERROR;
                        accountProductResult.Message = UNCAUGHT_ERROR_MESSAGE;
                    }
                }

                output.AppendLine("");
                output.AppendLine("***********************************************");
                output.AppendLine("Accounts Web Service - Get Products");
                output.Append("User                 : ").AppendLine(user);
                output.Append("Code                 : ").AppendLine(accountProductResult.Code);
                output.Append("Message              : ").AppendLine(accountProductResult.Message);
                output.Append("Details              : ").AppendLine(details);
                output.AppendLine("***********************************************");

                log.Debug(output.ToString());
            }

            return accountProductResult;
        }


        [WebMethod]
        public AccCountryResult GetCountries(String user, String password, String accountId)
        {
            AccCountryResult result = new AccCountryResult();
            String details = String.Empty, ip = String.Empty;
            StringBuilder output = new StringBuilder();
            EncryptDAO encDao = new EncryptDAO();
            AccountDAO accountDao = new AccountDAO();
            ResponseCodeDAO responseCodeDao = new ResponseCodeDAO();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = null;

                    loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(USER_NOT_AUTHORIZED);
                    }
                }

                result = accountDao.SearchCountry(accountId);

                if (result.Code.Equals("0"))
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                    if (res.Code.Equals("0"))
                    {
                        result.Code = res.ResponseCode[0].Code;
                        result.Message = res.ResponseCode[0].Message;
                    }
                    else
                    {
                        result.Code = UNCAUGHT_ERROR;
                        result.Message = UNCAUGHT_ERROR_MESSAGE;
                    }
                }

                output.AppendLine("");
                output.AppendLine("***********************************************");
                output.AppendLine("Accounts Web Service Get Countries");
                output.Append("User                 : ").AppendLine(user);
                output.Append("Code                 : ").AppendLine(result.Code);
                output.Append("Message              : ").AppendLine(result.Message);
                output.Append("Details              : ").AppendLine(details);


                output.AppendLine("***********************************************");

                log.Debug(output.ToString());
            }

            return result;
        }


        [WebMethod]
        public ResponseCodeResult GetResponseCodes(String user, String password, String accountId)
        {
            ResponseCodeResult result = new ResponseCodeResult();
            String details = String.Empty, ip = String.Empty;
            StringBuilder output = new StringBuilder();
            EncryptDAO encDao = new EncryptDAO();
            ResponseCodeDAO responseCodeDao = new ResponseCodeDAO();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = null;

                    loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(USER_NOT_AUTHORIZED);
                    }
                }

                result = responseCodeDao.Search(null, null, null);

                if (result.Code.Equals("0"))
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                    if (res.Code.Equals("0"))
                    {
                        result.Code = res.ResponseCode[0].Code;
                        result.Message = res.ResponseCode[0].Message;
                    }
                    else
                    {
                        result.Code = UNCAUGHT_ERROR;
                        result.Message = UNCAUGHT_ERROR_MESSAGE;
                    }
                }

                output.AppendLine("");
                output.AppendLine("***********************************************");
                output.AppendLine("Accounts Web Service Get Countries");
                output.Append("User                 : ").AppendLine(user);
                output.Append("Code                 : ").AppendLine(result.Code);
                output.Append("Message              : ").AppendLine(result.Message);
                output.Append("Details              : ").AppendLine(details);


                output.AppendLine("***********************************************");

                log.Debug(output.ToString());
            }

            return result;
        }


        [WebMethod]
        public ProductTypeResponse GetProductTypes(String user, String password, String accountId)
        {
            ProductTypeResponse result = new ProductTypeResponse();
            ProductTypeResult typeResult = null;
            List<ProductTypes> list = new List<ProductTypes>();
            String details = "", ip = String.Empty;
            StringBuilder output = new StringBuilder();
            EncryptDAO encDao = new EncryptDAO();
            ProductTypeDAO productTypeDao = new ProductTypeDAO();
            ResponseCodeDAO responseCodeDao = new ResponseCodeDAO();

            try
            {
                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = null;

                    loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(USER_NOT_AUTHORIZED);
                    }
                }

                typeResult = productTypeDao.Search(null, "1", null);

                result.Code = typeResult.Code;
                result.Message = typeResult.Message;

                if (typeResult.Code.Equals("0") && typeResult.ProductType != null)
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;

                    foreach (ProductType item in typeResult.ProductType)
                    {
                        list.Add(new ProductTypes(item.Id.ToString(), item.Description));
                    }

                    result.ProductTypes = list.ToArray();
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                    if (res.Code.Equals("0"))
                    {
                        result.Code = res.ResponseCode[0].Code;
                        result.Message = res.ResponseCode[0].Message;
                    }
                    else
                    {
                        result.Code = UNCAUGHT_ERROR;
                        result.Message = UNCAUGHT_ERROR_MESSAGE;
                    }
                }

                output.AppendLine("");
                output.AppendLine("***********************************************");
                output.AppendLine("Accounts Web Service Get Product Types");
                output.Append("User                 : ").AppendLine(user);
                output.Append("Code                 : ").AppendLine(result.Code);
                output.Append("Message              : ").AppendLine(result.Message);
                output.Append("Details              : ").AppendLine(details);
                output.AppendLine("***********************************************");

                log.Debug(output.ToString());
            }

            return result;
        }


        [WebMethod]
        public CurrenciesResponse GetCurrencies(String user, String password, String accountId)
        {
            CurrenciesResponse result = new CurrenciesResponse();
            CurrencyResult currencyResult = null;
            List<Currencies> list = new List<Currencies>();
            String details = String.Empty, ip = String.Empty;
            StringBuilder output = new StringBuilder();
            CurrencyDAO currencyDao = new CurrencyDAO();
            ResponseCodeDAO responseCodeDao = new ResponseCodeDAO();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = null;

                    loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(USER_NOT_AUTHORIZED);
                    }
                }
                
                currencyResult = currencyDao.Search(null, "1", null);

                result.Code = currencyResult.Code;
                result.Message = currencyResult.Message;

                if (currencyResult.Code.Equals("0") && currencyResult.Currency != null)
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;

                    foreach (Currency item in currencyResult.Currency)
                    {
                        list.Add(new Currencies(item.Id.ToString(), item.Description, item.Abbreviation));
                    }

                    result.Currencies = list.ToArray();
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                    if (res.Code.Equals("0"))
                    {
                        result.Code = res.ResponseCode[0].Code;
                        result.Message = res.ResponseCode[0].Message;
                    }
                    else
                    {
                        result.Code = UNCAUGHT_ERROR;
                        result.Message = UNCAUGHT_ERROR_MESSAGE;
                    }
                }

                output.AppendLine("");
                output.AppendLine("***********************************************");
                output.AppendLine("Accounts Web Service Get Currencies");
                output.Append("User                 : ").AppendLine(user);
                output.Append("Code                 : ").AppendLine(result.Code);
                output.Append("Message              : ").AppendLine(result.Message);
                output.Append("Details              : ").AppendLine(details);
                output.AppendLine("***********************************************");

                log.Debug(output.ToString());
            }

            return result;
        }


        [WebMethod]
        public CarriersResponse GetCarriers(String user, String password, String accountId)
        {
            CarriersResponse result = new CarriersResponse();
            CarrierResult carrierResult = null;
            List<Carriers> list = new List<Carriers>();
            String details = String.Empty, ip = String.Empty;
            StringBuilder output = new StringBuilder();
            AccountDAO accountDao = new AccountDAO();
            ResponseCodeDAO responseCodeDao = new ResponseCodeDAO();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = null;

                    loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(USER_NOT_AUTHORIZED);
                    }
                }

                carrierResult = accountDao.SearchCarrier(accountId);

                result.Code = carrierResult.Code;
                result.Message = carrierResult.Message;

                if (carrierResult.Code.Equals("0") && carrierResult.Carrier != null)
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;

                    foreach (Carrier item in carrierResult.Carrier)
                    {
                        list.Add(new Carriers(item.Id, item.Description, item.Abbreviation));
                    }

                    result.Carriers = list.ToArray();
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                    if (res.Code.Equals("0"))
                    {
                        result.Code = res.ResponseCode[0].Code;
                        result.Message = res.ResponseCode[0].Message;
                    }
                    else
                    {
                        result.Code = UNCAUGHT_ERROR;
                        result.Message = UNCAUGHT_ERROR_MESSAGE;
                    }
                }

                output.AppendLine("");
                output.AppendLine("***********************************************");
                output.AppendLine("Accounts Web Service Get Carriers ");
                output.Append("User                 : ").AppendLine(user);
                output.Append("Code                 : ").AppendLine(result.Code);
                output.Append("Message              : ").AppendLine(result.Message);
                output.Append("Details              : ").AppendLine(details);
                output.AppendLine("***********************************************");

                log.Debug(output.ToString());
            }

            return result;
        }

        
        [WebMethod]
        public CitiesResponse GetCities(String countryId, String user, String password, String accountId)
        {
            CitiesResponse result = new CitiesResponse();
            CityResult cityResult = null;
            List<Cities> list = new List<Cities>();
            String details = String.Empty, ip = String.Empty;
            StringBuilder output = new StringBuilder();
            ResponseCodeDAO responseCodeDao = new ResponseCodeDAO();
            AccountDAO accountDao = new AccountDAO();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                if (String.IsNullOrEmpty(countryId))
                    throw new Exception(COUNTRY_ID_REQUIRE);

                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = null;

                    loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(USER_NOT_AUTHORIZED);
                    }
                }

                cityResult = accountDao.SearchCity(accountId, countryId);

                result.Code = cityResult.Code;
                result.Message = cityResult.Message;

                if (cityResult.Code.Equals("0") && cityResult.City != null)
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;

                    foreach (City item in cityResult.City)
                    {
                        list.Add(new Cities(item.Id.ToString(), item.Description, item.Abbreviation));
                    }

                    result.Cities = list.ToArray();
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                    if (res.Code.Equals("0"))
                    {
                        result.Code = res.ResponseCode[0].Code;
                        result.Message = res.ResponseCode[0].Message;
                    }
                    else
                    {
                        result.Code = UNCAUGHT_ERROR;
                        result.Message = UNCAUGHT_ERROR_MESSAGE;
                    }
                }

                output.AppendLine("");
                output.AppendLine("***********************************************");
                output.AppendLine("Accounts Web Service Get Cities ");
                output.Append("User                 : ").AppendLine(user);
                output.Append("Country Id           : ").AppendLine(countryId);
                output.Append("Code                 : ").AppendLine(result.Code);
                output.Append("Message              : ").AppendLine(result.Message);
                output.Append("Details              : ").AppendLine(details);
                output.AppendLine("***********************************************");

                log.Debug(output.ToString());
            }

            return result;
        }

    }
}
