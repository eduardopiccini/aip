﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Security;
using System.Text;
using log4net;
using System.Globalization;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using Common;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using DataAccessLayer.DataAccessObjects;
using System.Net.Security;
using System.Data;
using DataAccessLayer.EntityBase;
using AccountsWs.PLWcf;

namespace AccountsWs
{
    
    
    /// <summary>
    /// Summary description for Service
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Service2 : System.Web.Services.WebService
    {

        private const String USER_NAME_REQUIRE = "AC1";
        private const String PASSWORD_REQUIRE = "AC2";
        private const String ACCOUNT_ID_REQUIRE = "AC3";
        private const String REFERENCE_REQUIRE = "AC4";
        private const String PRODUCT_ID_REQUIRE = "AC5";
        private const String AMOUNT_REQUIRE = "AC7";
        private const String PHONE_REQUIRE = "AC8";
        private const String USER_NOT_AUTHORIZED = "AC9";
        private const String UNCAUGHT_ERROR = "AC10";
        private const String UNCAUGHT_ERROR_MESSAGE = "TECHNICAL ERROR";
        private const String TRANSACTION_ID_REQUIRE = "AC11";
        private const String AUTHORIZATION_NUMBER_REQUIRE = "AC12";
        private const String NO_RECORDS_FOUND = "AC13";
        private const String COUNTRY_ID_REQUIRE = "AC14";
        private const String DATE_REQUIRE = "AC15";
        private const String INVALID_DATE = "DL3";
        private const String AT_LEAST_TWO = "AC15";
        private const String PRASAN = "5";
        private readonly String OFFICIAL_PREPAID_NATION = "502";
        private readonly String AIPC_PREPAID_NATION = "982";
        private readonly String TRUMPIA = "583";
        private readonly String SOLOPIN = "21";
        private readonly String SOLOPIN_4G = "22";
        private readonly String ARIN_SMS = "1001";

        private readonly String LOCAL_PIN = System.Configuration.ConfigurationManager.AppSettings["local_pin"].ToString();
        private readonly String PIN = System.Configuration.ConfigurationManager.AppSettings["pin"].ToString();
        private readonly String RECHARGE = System.Configuration.ConfigurationManager.AppSettings["recharge"].ToString();

        

        private static readonly ILog log = LogManager.GetLogger("AccountWs");

        public static String Locale;
        public String UserDb;
        

        public Service2()
        {
            Locale = CultureInfo.CurrentCulture.Name;
            this.UserDb = System.Configuration.ConfigurationManager.AppSettings["user_db"].ToString();
        }


        [WebMethod]
        public TransactionResult Buy(String reference, String merchant, String productId, String amount, String phone,
            String user, String password, String accountId)
        {
            String ip = String.Empty, routingId = String.Empty, details = String.Empty,
                transactionTimeout = String.Empty, transactionId = String.Empty, timeoutException = String.Empty;
            TransactionResult result = new TransactionResult();
            StringBuilder output = new StringBuilder();
            DataAccessLayer.DataAccessObjects.RoutingResult routingResult = new DataAccessLayer.DataAccessObjects.RoutingResult();
            DateTime start = DateTime.Now, end;
            DateTime? providerStart = null, providerEnd = null;
            
            
            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(USER_NOT_AUTHORIZED);
                    }
                }

                using (EncryptDAO dao = new EncryptDAO())
                {
                    password = dao.Encrypt(password);
                }

                using (RoutingDAO dao = new RoutingDAO())
                {
                    routingResult = dao.Buy(user, password, accountId, productId, amount, 
                        phone, reference, ip, merchant, null, "0", Locale, UserDb);
                }

                result.Message = routingResult.Message;
                result.Details = routingResult.Details;

                if (!String.IsNullOrEmpty(routingResult.LogId))
                {
                    if (!routingResult.LogId.ToLower().Equals("null"))
                    {
                        transactionId = routingResult.LogId;
                    }
                    else
                    {
                        routingResult.LogId = String.Empty;
                    }
                }

                if (routingResult.Code.Equals("0"))
                {
                    foreach (RoutingProvider item in routingResult.RoutingProvider)
	                {
		                if (item.ProviderId.Equals(PRASAN))
                        {
                            providerStart = DateTime.Now;

                            PrasanWcf.RechargeRequest req = new PrasanWcf.RechargeRequest();
                            PrasanWcf.RechargeResponse resp = null;

                            if (item.TransactionMode.Equals("0")) //Fix
                            {
                                req.Amount = String.Empty;
                                req.Type = Common.Utils.FIX_RECHARGE;
                            }
                            else
                            {
                                req.Amount = item.LocalCurrencyAmount;
                                req.Type = Common.Utils.FLEXI_RECHARGE;
                            }

                            using(PrasanWcf.IappClient wcf = new PrasanWcf.IappClient())
	                        {
                                req.Phone = item.Phone;
                                req.ProductId = item.ProductCode;
                                req.TransactionId = item.RoutingId;
                                req.Url = item.Url;
                                req.LoginId = item.User;

                                using (EncryptDAO dao = new EncryptDAO())
                                {
                                    req.ProtectedKey = dao.Decrypt(item.Password);
                                }

                                req.DbUser = this.UserDb;
                                req.RoutingId = item.RoutingId;
                                req.Locale = Locale;

                                resp = wcf.Recharge(req);

                                result.AuthorizationNumber = resp.AuthorizationNumber;
                                result.Code = resp.Code;
                                result.Details = resp.Details;
                                result.TransactionId = item.LogId;
	                        }

                            providerEnd = DateTime.Now;  
                        }
                        else if (item.ProviderId.Equals(OFFICIAL_PREPAID_NATION) || item.ProviderId.Equals(AIPC_PREPAID_NATION))
                        {
                            providerStart = DateTime.Now;

                            if (item.ProductTypeId.Equals(RECHARGE))
                            {
                                using (PNWcf.IappClient wcf = new PNWcf.IappClient())
                                {
                                    PNWcf.RechargeRequest req = new PNWcf.RechargeRequest();
                                    PNWcf.RechargeResponse resp = null;

                                    req.Amount = Convert.ToDecimal(item.CurrencyAmount);
                                    
                                    using (EncryptDAO dao = new EncryptDAO())
                                    {
                                        req.Password = dao.Decrypt(item.Password);    
                                    }

                                    req.Phone = item.Phone;
                                    req.ProductId = item.ProductCode;
                                    req.StoreId = item.TerminalId;
                                    req.TransactionId = item.VoidRoutingId;
                                    req.Url = item.Url;
                                    req.User = item.User;
                                    req.DbUser = this.UserDb;
                                    req.RoutingId = item.RoutingId;
                                    req.Locale = Locale;

                                    resp = wcf.Recharge(req);

                                    result.TransactionId = item.LogId;
                                    result.AuthorizationNumber = resp.AuthorizationNumber;
                                    result.Code = resp.Code;
                                    result.Details = resp.Details;
                                    result.Message = resp.Message;
                                }
                            }
                            else if (item.ProductTypeId.Equals(PIN))
                            {
                                using (PNWcf.IappClient wcf = new PNWcf.IappClient())
                                {
                                    PNWcf.PurchasePinRequest req = new PNWcf.PurchasePinRequest();
                                    PNWcf.PurchasePinResponse resp = null;

                                    using (EncryptDAO dao = new EncryptDAO())
                                    {
                                        req.Password = dao.Decrypt(item.Password);
                                    }

                                    req.ProductId = item.ProductCode;
                                    req.StoreId = item.TerminalId;
                                    req.TransactionId = item.VoidRoutingId;
                                    req.Url = item.Url;
                                    req.User = item.User;
                                    req.DbUser = this.UserDb;
                                    req.RoutingId = item.RoutingId;
                                    req.Locale = Locale;
                                    
                                    resp = wcf.PurchasePin(req);

                                    result.TransactionId = item.LogId;
                                    result.Code = resp.Code;
                                    result.Details = resp.Details;
                                    result.Message = resp.Message;
                                    result.PinNumber = resp.PinNumber;
                                    result.SerialNumber = resp.SerialNumber;
                                }
                            }

                            providerEnd = DateTime.Now; 
                        }
                        else
                        {
                            result.Code = Common.Utils.PROVIDER_NOT_FOUND;
                        }

                        using (ResponseCodeDAO dao = new ResponseCodeDAO())
                        {
                            Result response = dao.GetResponseCode(item.ProviderId, result.Code, result.Message, Locale, this.UserDb);

                            result.Code = response.Code;
                            result.Message = response.Message;
                        }

                        result.TransactionId = item.LogId;

                        if (result.Code.Equals(Common.Utils.APPROVED))
                        {
                            break;
                        }
                    }         
                }
                else
                {
                    if (!String.IsNullOrEmpty(routingResult.LogId))
                    {
                        using (ApiLogDAO apiLogDao = new ApiLogDAO())
                        {
                            Result apiLogResult = apiLogDao.SearchApiLogResponseCode(routingResult.LogId);

                            if (apiLogResult.Code.Equals(Common.Utils.NO_RECORDS_FOUND) || apiLogResult.Code.Equals(Common.Utils.UNKNOWN_EXCEPTION))
                            {
                                result.Code = routingResult.Code;
                                result.Message = routingResult.Message;
                            }
                            else
                            {
                                result.Code = apiLogResult.Code;
                                result.Message = apiLogResult.Message;
                            }
                        }
                    }
                    else
                    {
                        result.Code = Common.Utils.UNCAUGHT_ERROR;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            result.Code = res.ResponseCode[0].Code;
                            result.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            result.Code = UNCAUGHT_ERROR;
                            result.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                details = result.Details;
                result.Details = String.Empty;

                end = DateTime.Now;

                Common.Utils.Concat(ref output, null, "BUY:", false, true);
                Common.Utils.Concat(ref output, "StartTime", start.ToString("hh:mm:ss tt"), false, false);
                Common.Utils.Concat(ref output, "EndDate", end.ToString("hh:mm:ss tt"), false, false);
                    
                if (providerStart != null)
                {
                    Common.Utils.Concat(ref output, "ProviderStartTime", providerStart.Value.ToString("hh:mm:ss tt"), false, false);
                }

                if (providerEnd != null)
                {
                    Common.Utils.Concat(ref output, "ProviderEndTime", providerEnd.Value.ToString("hh:mm:ss tt"), false, false);
                }

                Common.Utils.Concat(ref output, "TransactionId", result.TransactionId, false, false);
                Common.Utils.Concat(ref output, "Reference", reference, false, false);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "ProductId", productId, false, false);
                Common.Utils.Concat(ref output, "Amount", amount, false, false);
                Common.Utils.Concat(ref output, "Phone", phone, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", result.Code, false, false);
                Common.Utils.Concat(ref output, "Message", result.Message, false, false);
                Common.Utils.Concat(ref output, "AuthNumber", result.AuthorizationNumber, false, false);
                Common.Utils.Concat(ref output, "PinNumber", result.PinNumber, false, false);
                Common.Utils.Concat(ref output, "SerialNumber", result.SerialNumber, false, false);
                Common.Utils.Concat(ref output, "TranDate", result.TransactionDateTime, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);


                log.Debug(output.ToString());
                
            }

            return result;
        }


        [WebMethod]
        public TransactionResult BuySms(String reference, String merchant,
            String productId, String phone, String message, String user, String password, String accountId)
        {
            String details = String.Empty, ip = String.Empty,
               timeoutException = String.Empty,
                transactionId = String.Empty, transactionTimeout = String.Empty;
            TransactionResult result = new TransactionResult();
            StringBuilder output = new StringBuilder();
            DataAccessLayer.DataAccessObjects.RoutingResult routingResult = new DataAccessLayer.DataAccessObjects.RoutingResult();
            DateTime start = DateTime.Now, end;
            DateTime? providerStart = null, providerEnd = null;

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(USER_NOT_AUTHORIZED);
                    }
                }

                using (EncryptDAO dao = new EncryptDAO())
                {
                    password = dao.Encrypt(password);
                }

                using (RoutingDAO dao = new RoutingDAO())
                {
                    routingResult = dao.Buy(user, password, accountId,
                        productId, null, phone, reference, ip, merchant, message, "0", Locale, UserDb);
                }

                result.Message = routingResult.Message;
                result.Details = routingResult.Details;

                if (!String.IsNullOrEmpty(routingResult.LogId))
                {
                    if (!routingResult.LogId.ToLower().Equals("null"))
                    {
                        transactionId = routingResult.LogId;
                    }
                    else
                    {
                        routingResult.LogId = String.Empty;
                    }
                }

                if (routingResult.Code.Equals("0"))
                {
                    foreach (RoutingProvider item in routingResult.RoutingProvider)
                    {
                        if (item.ProviderId.Equals(TRUMPIA))
                        {
                            using (TrumpiaWcf.IappClient wcf = new TrumpiaWcf.IappClient())
                            {
                                TrumpiaWcf.SendSmsRequest req = new TrumpiaWcf.SendSmsRequest();
                                TrumpiaWcf.SendSmsResponse resp = null;

                                using (EncryptDAO dao = new EncryptDAO())
                                {
                                    req.ApiKey = dao.Decrypt(item.Password);
                                }

                                req.CountryCode = "1";
                                req.DbUser = this.UserDb;
                                req.FirstName = "OFFICIAL";
                                req.LastName = "OFFICIAL";
                                req.ListName = item.User;
                                req.Locale = Locale;
                                req.Message = message;
                                req.RoutingId = item.RoutingId;
                                req.Url = item.Url;
                                req.Phone = item.Phone;

                                resp = wcf.SendSms(req);

                                result.Code = resp.Code;
                                result.AuthorizationNumber = resp.AuthorizationNumber;
                                result.Details = resp.Details;
                                
                            }
                        }
                        else if (item.ProviderId.Equals(ARIN_SMS))
                        {
                            providerStart = DateTime.Now;

                            using (ArinWcf.IappClient wcf = new ArinWcf.IappClient())
                            {
                                ArinWcf.SmsRequest req = new ArinWcf.SmsRequest();
                                ArinWcf.Response resp = null;

                                req.DbUser = this.UserDb;
                                req.Locale = Locale;
                                req.Message = message;
                                req.Phone = phone;
                                req.RoutingId = item.RoutingId;

                                using (EncryptDAO dao = new EncryptDAO())
                                {
                                    req.ServerPassword = dao.Decrypt(item.Password);
                                }

                                req.Url = item.Url;

                                resp = wcf.SendSms(req);

                                result.Code = resp.Code;
                                result.Message = resp.Message;
                                result.AuthorizationNumber = resp.AuthorizationNumber;
                            }
                        }
                        else
                        {
                            result.Code = Common.Utils.PROVIDER_NOT_FOUND;
                        }

                        using (ResponseCodeDAO dao = new ResponseCodeDAO())
                        {
                            Result response = dao.GetResponseCode(item.ProviderId, result.Code, result.Message, Locale, this.UserDb);

                            result.Code = response.Code;
                            result.Message = response.Message;
                        }

                        result.TransactionId = item.LogId;

                        if (result.Code.Equals(Common.Utils.APPROVED))
                        {
                            break;
                        }
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(routingResult.LogId))
                    {
                        using (ApiLogDAO apiLogDao = new ApiLogDAO())
                        {
                            Result apiLogResult = apiLogDao.SearchApiLogResponseCode(routingResult.LogId);

                            if (apiLogResult.Code.Equals(Common.Utils.NO_RECORDS_FOUND) || apiLogResult.Code.Equals(Common.Utils.UNKNOWN_EXCEPTION))
                            {
                                result.Code = routingResult.Code;
                                result.Message = routingResult.Message;
                            }
                            else
                            {
                                result.Code = apiLogResult.Code;
                                result.Message = apiLogResult.Message;
                            }
                        }
                        
                        
                    }
                    else
                    {
                        result.Code = Common.Utils.UNCAUGHT_ERROR;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            result.Code = res.ResponseCode[0].Code;
                            result.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            result.Code = UNCAUGHT_ERROR;
                            result.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                end = DateTime.Now;

                Common.Utils.Concat(ref output, null, "BUY_SMS:", false, true);
                Common.Utils.Concat(ref output, "StartTime", start.ToString("hh:mm:ss tt"), false, false);
                Common.Utils.Concat(ref output, "EndDate", end.ToString("hh:mm:ss tt"), false, false);
                    
                if (providerStart != null)
                {
                    Common.Utils.Concat(ref output, "ProviderStartTime", providerStart.Value.ToString("hh:mm:ss tt"), false, false);
                }

                if (providerEnd != null)
                {
                    Common.Utils.Concat(ref output, "ProviderEndTime", providerEnd.Value.ToString("hh:mm:ss tt"), false, false);
                }

                Common.Utils.Concat(ref output, "TransactionId", result.TransactionId, false, false);
                Common.Utils.Concat(ref output, "Reference", reference, false, false);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "ProductId", productId, false, false);
                Common.Utils.Concat(ref output, "Phone", phone, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", result.Code, false, false);
                Common.Utils.Concat(ref output, "Message", result.Message, false, false);
                Common.Utils.Concat(ref output, "AuthNumber", result.AuthorizationNumber, false, false);
                Common.Utils.Concat(ref output, "TranDate", result.TransactionDateTime, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);


            }

            return result;
        }


        [WebMethod]
        public Response Void(String transactionId,
            String reference, String amount, String authorizationNumber, String phone, String user, 
            String password, String accountId)
        {
            String details = String.Empty, ip = null;
            Response result = new Response();
            StringBuilder output = new StringBuilder();
            RoutingResult routingResult = null;

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(USER_NOT_AUTHORIZED);
                    }
                }

                using (EncryptDAO dao = new EncryptDAO())
                {
                    password = dao.Encrypt(password);
                }

                using (RoutingDAO dao = new RoutingDAO())
                {
                    routingResult = dao.Void(transactionId, user, password, accountId, reference, ip, Locale, this.UserDb);
                }

                result.Message = routingResult.Message;

                if (!String.IsNullOrEmpty(routingResult.LogId))
                {
                    if (!routingResult.LogId.ToLower().Equals("null"))
                    {
                        transactionId = routingResult.LogId;
                    }
                    else
                    {
                        routingResult.LogId = String.Empty;
                    }
                }

                if (routingResult.Code.Equals("0"))
                {
                    foreach (RoutingProvider item in routingResult.RoutingProvider)
                    {
                        if (item.ProviderId.Equals(PRASAN))
                        {
                            PrasanWcf.VoidRequest req = new PrasanWcf.VoidRequest();
                            PrasanWcf.Response resp = null;

                            using (PrasanWcf.IappClient wcf = new PrasanWcf.IappClient())
                            {
                                req.TransactionId = item.RoutingId;
                                req.Url = item.Url;
                                req.LoginId = item.User;
                                req.TransactionId = item.VoidRoutingId;

                                using (EncryptDAO dao = new EncryptDAO())
                                {
                                    req.ProtectedKey = dao.Decrypt(item.Password);
                                }

                                req.DbUser = this.UserDb;
                                req.RoutingId = item.RoutingId;
                                req.Locale = Locale;

                                resp = wcf.Void(req);

                                result.Code = resp.Code;
                                result.Message = resp.Message;
                            }
                        }
                        else if (item.ProviderId.Equals(OFFICIAL_PREPAID_NATION) || item.ProviderId.Equals(AIPC_PREPAID_NATION))
                        {
                            if (item.ProductTypeId.Equals(RECHARGE))
                            {
                                using (PNWcf.IappClient wcf = new PNWcf.IappClient())
                                {
                                    PNWcf.VoidRequest req = new PNWcf.VoidRequest();
                                    PNWcf.Response resp = null;

                                    using (EncryptDAO dao = new EncryptDAO())
                                    {
                                        req.Password = dao.Decrypt(item.Password);
                                    }

                                    req.OldTransactionId = Convert.ToInt64(item.VoidRoutingId);
                                    req.StoreId = item.TerminalId;
                                    req.TransactionId = item.RoutingId;
                                    req.Url = item.Url;
                                    req.User = item.User;
                                    req.DbUser = this.UserDb;
                                    req.RoutingId = item.RoutingId;
                                    req.Locale = Locale;

                                    resp = wcf.Void(req);

                                    result.Code = resp.Code;
                                    result.Message = resp.Message;
                                }
                            }
                            
                        }
                        else if (item.ProviderId.Equals(SOLOPIN) || item.ProviderId.Equals(SOLOPIN_4G))
                        {
                            using (PLWcf.IappClient wcf = new PLWcf.IappClient())
                            {
                                LoginRequest loginReq = new LoginRequest();
                                PLWcf.LoginResponse loginResponse = null;
                                VoidRequest req = new VoidRequest();
                                PLWcf.VoidResponse voidResponse = null;
                                PLWcf.Request lreq = new Request();

                                loginReq.UserName = item.User;

                                using (EncryptDAO dao = new EncryptDAO())
                                {
                                    loginReq.Password = dao.Decrypt(item.Password);
                                }

                                loginResponse = wcf.Login(loginReq);

                                if (loginResponse.Code.Equals("0"))
                                {
                                    req.DbUser = UserDb;
                                    req.Locale = Locale;
                                    req.Reference = Convert.ToInt64(item.VoidRoutingId);
                                    req.StoreId = Convert.ToInt32(accountId);
                                    req.SessionId = loginResponse.SessionId;
                                    req.RoutingId = item.RoutingId;
                                    
                                    voidResponse = wcf.Void(req);

                                    result.Code = voidResponse.Code;
                                    result.Message = voidResponse.Message;

                                    lreq.SessionId = loginResponse.SessionId;

                                    wcf.Logout(lreq);
                                }

                            }
                        }
                        else
                        {
                            result.Code = Common.Utils.PROVIDER_NOT_FOUND;
                        }

                        using (ResponseCodeDAO dao = new ResponseCodeDAO())
                        {
                            Result response = dao.GetResponseCode(item.ProviderId, result.Code, result.Message, Locale, this.UserDb);

                            result.Code = response.Code;
                            result.Message = response.Message;
                        }

                        if (result.Code.Equals(Common.Utils.APPROVED))
                        {
                            break;
                        }
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(routingResult.LogId))
                    {
                        using (ApiLogDAO apiLogDao = new ApiLogDAO())
                        {
                            Result apiLogResult = apiLogDao.SearchApiLogResponseCode(routingResult.LogId);

                            if (apiLogResult.Code.Equals(Common.Utils.NO_RECORDS_FOUND) || apiLogResult.Code.Equals(Common.Utils.UNKNOWN_EXCEPTION))
                            {
                                result.Code = routingResult.Code;
                                result.Message = routingResult.Message;
                            }
                            else
                            {
                                result.Code = apiLogResult.Code;
                                result.Message = apiLogResult.Message;
                            }
                        }


                    }
                    else
                    {
                        result.Code = Common.Utils.UNCAUGHT_ERROR;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            result.Code = res.ResponseCode[0].Code;
                            result.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            result.Code = UNCAUGHT_ERROR;
                            result.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                Common.Utils.Concat(ref output, null, "VOID:", false, true);
                Common.Utils.Concat(ref output, "TransactionId", transactionId, false, false);
                Common.Utils.Concat(ref output, "Reference", reference, false, false);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "Amount", amount, false, false);
                Common.Utils.Concat(ref output, "Phone", phone, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", result.Code, false, false);
                Common.Utils.Concat(ref output, "Message", result.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return result;
        }



        [WebMethod]
        public Response VoidByReference(String oldReference, String newReference, String amount, String phone,
            String user, String password, String accountId)
        {
            String details = String.Empty, ip = null;
            Response result = new Response();
            StringBuilder output = new StringBuilder();
            RoutingResult routingResult = null;

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(USER_NOT_AUTHORIZED);
                    }
                }

                using (EncryptDAO dao = new EncryptDAO())
                {
                    password = dao.Encrypt(password);
                }

                using (RoutingDAO dao = new RoutingDAO())
                {
                    routingResult = dao.VoidByReference(oldReference, user, password, accountId, newReference, ip, Locale, this.UserDb);
                }

                result.Message = routingResult.Message;

                if (routingResult.Code.Equals("0"))
                {
                    foreach (RoutingProvider item in routingResult.RoutingProvider)
                    {
                        if (item.ProviderId.Equals(PRASAN))
                        {
                            PrasanWcf.VoidRequest req = new PrasanWcf.VoidRequest();
                            PrasanWcf.Response resp = null;

                            using (PrasanWcf.IappClient wcf = new PrasanWcf.IappClient())
                            {
                                req.TransactionId = item.RoutingId;
                                req.Url = item.Url;
                                req.LoginId = item.User;
                                req.TransactionId = item.VoidRoutingId;

                                using (EncryptDAO dao = new EncryptDAO())
                                {
                                    req.ProtectedKey = dao.Decrypt(item.Password);
                                }

                                req.DbUser = this.UserDb;
                                req.RoutingId = item.RoutingId;
                                req.Locale = Locale;

                                resp = wcf.Void(req);

                                result.Code = resp.Code;
                                result.Message = resp.Message;
                            }
                        }
                        else if (item.ProviderId.Equals(OFFICIAL_PREPAID_NATION) || item.ProviderId.Equals(AIPC_PREPAID_NATION))
                        {
                            if (item.ProductTypeId.Equals(RECHARGE))
                            {
                                using (PNWcf.IappClient wcf = new PNWcf.IappClient())
                                {
                                    PNWcf.VoidRequest req = new PNWcf.VoidRequest();
                                    PNWcf.Response resp = null;

                                    using (EncryptDAO dao = new EncryptDAO())
                                    {
                                        req.Password = dao.Decrypt(item.Password);
                                    }

                                    req.OldTransactionId = Convert.ToInt64(item.VoidRoutingId);
                                    req.StoreId = item.TerminalId;
                                    req.TransactionId = item.RoutingId;
                                    req.Url = item.Url;
                                    req.User = item.User;
                                    req.DbUser = this.UserDb;
                                    req.RoutingId = item.RoutingId;
                                    req.Locale = Locale;

                                    resp = wcf.Void(req);

                                    result.Code = resp.Code;
                                    result.Message = resp.Message;
                                }
                            }

                        }
                        else if (item.ProviderId.Equals(SOLOPIN) || item.ProviderId.Equals(SOLOPIN_4G))
                        {
                            using (PLWcf.IappClient wcf = new PLWcf.IappClient())
                            {
                                LoginRequest loginReq = new LoginRequest();
                                PLWcf.LoginResponse loginResponse = null;
                                VoidRequest req = new VoidRequest();
                                PLWcf.VoidResponse voidResponse = null;
                                PLWcf.Request lreq = new Request();

                                loginReq.UserName = item.User;

                                using (EncryptDAO dao = new EncryptDAO())
                                {
                                    loginReq.Password = dao.Decrypt(item.Password);
                                }

                                loginResponse = wcf.Login(loginReq);

                                if (loginResponse.Code.Equals("0"))
                                {
                                    req.DbUser = UserDb;
                                    req.Locale = Locale;
                                    req.Reference = Convert.ToInt64(item.VoidRoutingId);
                                    req.StoreId = Convert.ToInt32(accountId);
                                    req.SessionId = loginResponse.SessionId;
                                    req.RoutingId = item.RoutingId;

                                    voidResponse = wcf.Void(req);

                                    result.Code = voidResponse.Code;
                                    result.Message = voidResponse.Message;

                                    lreq.SessionId = loginResponse.SessionId;

                                    wcf.Logout(lreq);
                                }

                            }
                        }
                        else
                        {
                            result.Code = Common.Utils.PROVIDER_NOT_FOUND;
                        }

                        using (ResponseCodeDAO dao = new ResponseCodeDAO())
                        {
                            Result response = dao.GetResponseCode(item.ProviderId, result.Code, result.Message, Locale, this.UserDb);

                            result.Code = response.Code;
                            result.Message = response.Message;
                        }

                        if (result.Code.Equals(Common.Utils.APPROVED))
                        {
                            break;
                        }
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(routingResult.LogId))
                    {
                        using (ApiLogDAO apiLogDao = new ApiLogDAO())
                        {
                            Result apiLogResult = apiLogDao.SearchApiLogResponseCode(routingResult.LogId);

                            if (apiLogResult.Code.Equals(Common.Utils.NO_RECORDS_FOUND) || apiLogResult.Code.Equals(Common.Utils.UNKNOWN_EXCEPTION))
                            {
                                result.Code = routingResult.Code;
                                result.Message = routingResult.Message;
                            }
                            else
                            {
                                result.Code = apiLogResult.Code;
                                result.Message = apiLogResult.Message;
                            }
                        }


                    }
                    else
                    {
                        result.Code = Common.Utils.UNCAUGHT_ERROR;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            result.Code = res.ResponseCode[0].Code;
                            result.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            result.Code = UNCAUGHT_ERROR;
                            result.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                Common.Utils.Concat(ref output, null, "VOID_BY_REFERENCE:", false, true);
                Common.Utils.Concat(ref output, "OldReference", oldReference, false, false);
                Common.Utils.Concat(ref output, "NewReference", newReference, false, false);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "Amount", amount, false, false);
                Common.Utils.Concat(ref output, "Phone", phone, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", result.Code, false, false);
                Common.Utils.Concat(ref output, "Message", result.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());

            }

            return result;
        }

        [WebMethod]
        public BalanceResponse GetBalance(String user, String password, String accountId)
        {
            String details = String.Empty, ip = null; 
            BalanceResponse result = new BalanceResponse();
            StringBuilder output = new StringBuilder();
            AccountResult accountResult = new AccountResult();
            AccountDAO accountDao = new AccountDAO();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(USER_NOT_AUTHORIZED);
                    }
                }

                accountResult = accountDao.SearchAccount(accountId, null, null, null, null, null, null, null, null, null, null, null, false, null);

                result.Code = accountResult.Code;
                result.Message = accountResult.Message;
                details = accountResult.Details;

                if (result.Code.Equals("0"))
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;
                    result.Balance = accountResult.Account[0].Balance.ToString();
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            result.Code = res.ResponseCode[0].Code;
                            result.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            result.Code = UNCAUGHT_ERROR;
                            result.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                Common.Utils.Concat(ref output, null, "GET_BALANCE:", false, true);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "Balance", result.Balance, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", result.Code, false, false);
                Common.Utils.Concat(ref output, "Message", result.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return result;
        }


        [WebMethod]
        public AccountTransactionResponse QueryTransactionsByPhone(String phone, String user, String password, String accountId)
        {
            String ip = null, details = null;
            StringBuilder output = new StringBuilder();
            AccountTransactionResponse response = new AccountTransactionResponse();
            TransactionLogResult transactionLogResult = new TransactionLogResult();
            List<AccountTransactions> list = new List<AccountTransactions>();
            TransactionLogDAO transactionDao = new TransactionLogDAO();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(USER_NOT_AUTHORIZED);
                    }
                }

                if (String.IsNullOrEmpty(phone))
                {
                    throw new Exception(PHONE_REQUIRE);
                }


                transactionLogResult = transactionDao.Search(null, null, accountId, null, null, null, null, null,
                    null, null, null, null, null, null, null, phone, null, null, 0, 50);


                response.Code = transactionLogResult.Code;
                response.Message = transactionLogResult.Message;
                details = transactionLogResult.Details;

                if (transactionLogResult.Code.Equals("0"))
                {
                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;

                    foreach (TransactionLog item in transactionLogResult.TransactionLog)
                    {
                        list.Add(new AccountTransactions(item.LogId, item.Reference, item.Merchant,
                            item.ProductTypeId, item.ProductId, item.TransactionAmount, item.Phone,
                            item.ResponseCodeId, item.DateCreated, item.ProviderReference,
                            item.TransactionTypeId, item.TransactionTypeName));
                    }

                    response.AccountTransactions = list.ToArray();
                }

            }
            catch (Exception ex)
            {
                response.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(response.Message) && !String.IsNullOrEmpty(response.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(response.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            response.Code = res.ResponseCode[0].Code;
                            response.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            response.Code = UNCAUGHT_ERROR;
                            response.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                Common.Utils.Concat(ref output, null, "QUERY_TRANS_BY_PHONE:", false, true);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "Phone", phone, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }


        [WebMethod]
        public AccountTransactionResponse QueryTransactionsById(String transactionId, String user, String password, String accountId)
        {
            String ip = null;
            String details = null;
            StringBuilder output = new StringBuilder();
            AccountTransactionResponse response = new AccountTransactionResponse();
            TransactionLogResult transactionLogResult = new TransactionLogResult();
            List<AccountTransactions> list = new List<AccountTransactions>();
            TransactionLogDAO transactionDao = new TransactionLogDAO();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(USER_NOT_AUTHORIZED);
                    }
                }

                if (String.IsNullOrEmpty(transactionId))
                {
                    throw new Exception(TRANSACTION_ID_REQUIRE);
                }

                

                transactionLogResult = transactionDao.Search(null, transactionId, accountId, null, null, null, null, null,
                    null, null, null, null, null, null, null, null, null, null, 0, 50);


                response.Code = transactionLogResult.Code;
                response.Message = transactionLogResult.Message;
                details = transactionLogResult.Details;

                if (transactionLogResult.Code.Equals("0"))
                {
                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;

                    foreach (TransactionLog item in transactionLogResult.TransactionLog)
                    {
                        list.Add(new AccountTransactions(item.LogId, item.Reference, item.Merchant,
                            item.ProductTypeId, item.ProductId, item.TransactionAmount, item.Phone,
                            item.ResponseCodeId, item.DateCreated, item.ProviderReference,
                            item.TransactionTypeId, item.TransactionTypeName));
                    }

                    response.AccountTransactions = list.ToArray();
                }

            }
            catch (Exception ex)
            {
                response.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(response.Message) && !String.IsNullOrEmpty(response.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(response.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            response.Code = res.ResponseCode[0].Code;
                            response.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            response.Code = UNCAUGHT_ERROR;
                            response.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                Common.Utils.Concat(ref output, null, "QUERY_TRANS_BY_ID:", false, true);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "TransactionId", transactionId, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }


        [WebMethod]
        public AccountTransactionResponse QueryTransactionsByReference(String reference, String user, String password, String accountId)
        {
            String ip = null;
            String details = null;
            StringBuilder output = new StringBuilder();
            AccountTransactionResponse response = new AccountTransactionResponse();
            TransactionLogWsResult transactionLogWsResult = new TransactionLogWsResult();
            List<AccountTransactions> list = new List<AccountTransactions>();
            TransactionLogDAO transactionDao = new TransactionLogDAO();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(USER_NOT_AUTHORIZED);
                    }
                }

                if (String.IsNullOrEmpty(reference))
                {
                    throw new Exception(REFERENCE_REQUIRE);
                }

                transactionLogWsResult = transactionDao.SearchByReference(reference, accountId);


                response.Code = transactionLogWsResult.Code;
                response.Message = transactionLogWsResult.Message;

                if (transactionLogWsResult.Code.Equals("0"))
                {
                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;

                    foreach (TransactionLogWs item in transactionLogWsResult.TransactionLogWs)
                    {
                        list.Add(new AccountTransactions(item.TransactionId, reference, item.Merchant,
                            item.ProductTypeId, item.ProductId, item.Amount, item.Phone,
                            item.ResponseCode, item.DateCreated, item.ProviderReference,
                            item.TransactionTypeId, item.TransactionTypeName));
                    }

                    response.AccountTransactions = list.ToArray();
                }

            }
            catch (Exception ex)
            {
                response.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(response.Message) && !String.IsNullOrEmpty(response.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(response.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            response.Code = res.ResponseCode[0].Code;
                            response.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            response.Code = UNCAUGHT_ERROR;
                            response.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                Common.Utils.Concat(ref output, null, "QUERY_TRANS_BY_REFENCE:", false, true);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "Reference", reference, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }


        [WebMethod]
        public AccountsProductResult GetProducts(String user, String password, String accountId)
        {
            String details = String.Empty,
                ip = String.Empty;
            AccountsProductResult accountProductResult = new AccountsProductResult();
            StringBuilder output = new StringBuilder();
            AccountDAO accountDao = new AccountDAO();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(USER_NOT_AUTHORIZED);
                    }
                }

                accountProductResult = accountDao.SearchProducts(accountId, null, null, null, null, null,
                    null, null, null, "1", "1");

                details = accountProductResult.Details;

                if (accountProductResult.Code.Equals("0"))
                {
                    accountProductResult.Code = Common.Utils.APPROVED;
                    accountProductResult.Message = Common.Utils.APPROVED_MESSAGE;
                } 
            }
            catch (Exception ex)
            {
                accountProductResult.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(accountProductResult.Message) && !String.IsNullOrEmpty(accountProductResult.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(accountProductResult.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            accountProductResult.Code = res.ResponseCode[0].Code;
                            accountProductResult.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            accountProductResult.Code = UNCAUGHT_ERROR;
                            accountProductResult.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                Common.Utils.Concat(ref output, null, "GET_PRODUCTS:", false, true);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", accountProductResult.Code, false, false);
                Common.Utils.Concat(ref output, "Message", accountProductResult.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return accountProductResult;
        }


        [WebMethod]
        public AccCountryResult GetCountries(String user, String password, String accountId)
        {
            AccCountryResult result = new AccCountryResult();
            String details = String.Empty, ip = String.Empty;
            StringBuilder output = new StringBuilder();
            AccountDAO accountDao = new AccountDAO();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(USER_NOT_AUTHORIZED);
                    }
                }

                result = accountDao.SearchCountry(accountId);

                if (result.Code.Equals("0"))
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            result.Code = res.ResponseCode[0].Code;
                            result.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            result.Code = UNCAUGHT_ERROR;
                            result.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                Common.Utils.Concat(ref output, null, "GET_COUNTRIES:", false, true);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", result.Code, false, false);
                Common.Utils.Concat(ref output, "Message", result.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return result;
        }


        [WebMethod]
        public ResponseCodeResult GetResponseCodes(String user, String password, String accountId)
        {
            ResponseCodeResult result = new ResponseCodeResult();
            String details = String.Empty, ip = String.Empty;
            StringBuilder output = new StringBuilder();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(USER_NOT_AUTHORIZED);
                    }
                }

                using (ResponseCodeDAO dao = new ResponseCodeDAO())
                {
                    result = dao.Search(null, null, null);    
                }
                

                if (result.Code.Equals("0"))
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            result.Code = res.ResponseCode[0].Code;
                            result.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            result.Code = UNCAUGHT_ERROR;
                            result.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                Common.Utils.Concat(ref output, null, "GET_RESPONSE_CODE:", false, true);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", result.Code, false, false);
                Common.Utils.Concat(ref output, "Message", result.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return result;
        }


        [WebMethod]
        public ProductTypeResponse GetProductTypes(String user, String password, String accountId)
        {
            ProductTypeResponse result = new ProductTypeResponse();
            ProductTypeResult typeResult = null;
            List<ProductTypes> list = new List<ProductTypes>();
            String details = String.Empty, ip = String.Empty;
            StringBuilder output = new StringBuilder();
            ProductTypeDAO productTypeDao = new ProductTypeDAO();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(USER_NOT_AUTHORIZED);
                    }
                }

                typeResult = productTypeDao.Search(null, "1", null);

                result.Code = typeResult.Code;
                result.Message = typeResult.Message;

                if (typeResult.Code.Equals("0") && typeResult.ProductType != null)
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;

                    foreach (ProductType item in typeResult.ProductType)
                    {
                        list.Add(new ProductTypes(item.Id.ToString(), item.Description));
                    }

                    result.ProductTypes = list.ToArray();
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            result.Code = res.ResponseCode[0].Code;
                            result.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            result.Code = UNCAUGHT_ERROR;
                            result.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                Common.Utils.Concat(ref output, null, "GET_PRODUCT_TYPES:", false, true);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", result.Code, false, false);
                Common.Utils.Concat(ref output, "Message", result.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return result;
        }


        [WebMethod]
        public CurrenciesResponse GetCurrencies(String user, String password, String accountId)
        {
            CurrenciesResponse result = new CurrenciesResponse();
            CurrencyResult currencyResult = null;
            List<Currencies> list = new List<Currencies>();
            String details = String.Empty, ip = String.Empty;
            StringBuilder output = new StringBuilder();
            CurrencyDAO currencyDao = new CurrencyDAO();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(USER_NOT_AUTHORIZED);
                    }
                }
                
                currencyResult = currencyDao.Search(null, "1", null);

                result.Code = currencyResult.Code;
                result.Message = currencyResult.Message;

                if (currencyResult.Code.Equals("0") && currencyResult.Currency != null)
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;

                    foreach (Currency item in currencyResult.Currency)
                    {
                        list.Add(new Currencies(item.Id.ToString(), item.Description, item.Abbreviation));
                    }

                    result.Currencies = list.ToArray();
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            result.Code = res.ResponseCode[0].Code;
                            result.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            result.Code = UNCAUGHT_ERROR;
                            result.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                Common.Utils.Concat(ref output, null, "GET_CURRENCIES:", false, true);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", result.Code, false, false);
                Common.Utils.Concat(ref output, "Message", result.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return result;
        }


        [WebMethod]
        public CarriersResponse GetCarriers(String user, String password, String accountId)
        {
            CarriersResponse result = new CarriersResponse();
            CarrierResult carrierResult = null;
            List<Carriers> list = new List<Carriers>();
            String details = String.Empty, 
                ip = String.Empty;
            StringBuilder output = new StringBuilder();
            AccountDAO accountDao = new AccountDAO();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(USER_NOT_AUTHORIZED);
                    }
                }

                carrierResult = accountDao.SearchCarrier(accountId);

                result.Code = carrierResult.Code;
                result.Message = carrierResult.Message;

                if (carrierResult.Code.Equals("0") && carrierResult.Carrier != null)
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;

                    foreach (Carrier item in carrierResult.Carrier)
                    {
                        list.Add(new Carriers(item.Id, item.Description, item.Abbreviation));
                    }

                    result.Carriers = list.ToArray();
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            result.Code = res.ResponseCode[0].Code;
                            result.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            result.Code = UNCAUGHT_ERROR;
                            result.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                Common.Utils.Concat(ref output, null, "GET_CARRIERS:", false, true);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", result.Code, false, false);
                Common.Utils.Concat(ref output, "Message", result.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return result;
        }

        
        [WebMethod]
        public CitiesResponse GetCities(String countryId, String user, String password, String accountId)
        {
            CitiesResponse result = new CitiesResponse();
            CityResult cityResult = null;
            List<Cities> list = new List<Cities>();
            String details = String.Empty, ip = null;
            StringBuilder output = new StringBuilder();
            AccountDAO accountDao = new AccountDAO();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    ProfileLoginResult loginResult = dao.LoginProfile(user, password, accountId, ip, Locale);

                    if (!loginResult.Code.Equals("0"))
                    {
                        throw new Exception(USER_NOT_AUTHORIZED);
                    }
                }

                if (String.IsNullOrEmpty(countryId))
                    throw new Exception(COUNTRY_ID_REQUIRE);

                cityResult = accountDao.SearchCity(accountId, countryId);

                result.Code = cityResult.Code;
                result.Message = cityResult.Message;

                if (cityResult.Code.Equals("0") && cityResult.City != null)
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;

                    foreach (DataAccessLayer.EntityBase.City item in cityResult.City)
                    {
                        list.Add(new Cities(item.Id.ToString(), item.Description, item.Abbreviation));
                    }

                    result.Cities = list.ToArray();
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            result.Code = res.ResponseCode[0].Code;
                            result.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            result.Code = UNCAUGHT_ERROR;
                            result.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                Common.Utils.Concat(ref output, null, "GET_CITIES:", false, true);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "CountryId", countryId, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", result.Code, false, false);
                Common.Utils.Concat(ref output, "Message", result.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return result;
        }


        [WebMethod]
        public Common.Result Test(int seconds)
        {
            StringBuilder output = new StringBuilder();
            Common.Result result = new Common.Result();
            DateTime? inicio = null, fin = null;
            String estadoCliente = "CONNECTED";

            try
            {
                inicio = DateTime.Now;
                System.Threading.Thread.Sleep(seconds * 1000);
                fin = DateTime.Now;
                result.Code = Common.Utils.APPROVED;
                result.Message = Common.Utils.APPROVED_MESSAGE;

                if (!Context.Response.IsClientConnected)
                {
                    estadoCliente = "DISCONNECTED";
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Message = ex.Message;
            }
            finally
            {
                output.AppendLine(String.Empty);
                output.AppendLine("***********************************************");
                output.AppendLine("Utp Web Service - Test");
                output.Append("Segundos             : ").AppendLine(seconds.ToString());

                if (inicio != null & inicio.HasValue)
                {
                    output.Append("Inicio               : ").AppendLine(inicio.Value.ToString("T"));
                }

                if (fin != null & fin.HasValue)
                {
                    output.Append("Fin                  : ").AppendLine(fin.Value.ToString("T"));
                }

                output.Append("Estado Cliente       : ").AppendLine(estadoCliente);
                output.Append("Code                 : ").AppendLine(result.Code);
                output.Append("Message              : ").AppendLine(result.Message);
                output.Append("Details              : ").AppendLine(result.Details);
                output.AppendLine("***********************************************");

                log.Debug(output.ToString());
            }

            return result;
        }
    }
}
