﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Security;
using System.Text;
using log4net;
using System.Globalization;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using Common;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using DataAccessLayer.DataAccessObjects;
using System.Net.Security;
using System.Data;
using DataAccessLayer.EntityBase;
using AccountsWs.PLWcf;

namespace AccountsWs
{

    [Serializable]
    public class BuyAllRequest
    {
        public String Reference { get; set; }
        public String Merchant { get; set; }
        public String ProductId { get; set; }
        public String Amount { get; set; }
        public String Phone { get; set; }
        public String Distributor { get; set; }
        public String SmsMessage { get; set; }
        public String PlLocale { get; set; }
        public String PlPhoneNotification { get; set; }
        public String StoreId { get; set; }
        public String StoreName { get; set; }
    }

    
    [Serializable]
    public class Countries
    {
        public Countries()
        {
        }

        public Countries(String id, String description)
        {
            this.Id = id;
            this.Description = description;
        }

        public String Id {get; set;}
        public String Description { get; set; }
    }


    [Serializable]
    public class ProductTypes
    {
        public ProductTypes()
        {
        }

        public ProductTypes(String id, String description)
        {
            this.Id = id;
            this.Description = description;
        }

        public String Id { get; set; }
        public String Description { get; set; }
    }


    [Serializable]
    public class Carriers
    {
        public Carriers()
        {
        }

        public Carriers(String id, String description, String abbreviation)
        {
            this.Id = id;
            this.Description = description;
            this.Abbreviation = abbreviation;
        }

        public String Id { get; set; }
        public String Description { get; set; }
        public String Abbreviation { get; set; }
    }


    [Serializable]
    public class Currencies
    {
        public Currencies()
        {
        }

        public Currencies(String id, String description, String abbreviation)
        {
            this.Id = id;
            this.Description = description;
            this.Abbreviation = abbreviation;
        }

        public String Id { get; set; }
        public String Description { get; set; }
        public String Abbreviation { get; set; }
    }


    [Serializable]
    public class Cities
    {
        public Cities()
        {
        }

        public Cities(String id, String description, String abbreviation)
        {
            this.Id = id;
            this.Description = description;
            this.Abbreviation = abbreviation;
        }

        public String Id { get; set; }
        public String Description { get; set; }
        public String Abbreviation { get; set; }
    }


    [Serializable]
    public class Response
    {
        public Response()
        {
            this.Code = String.Empty;
            this.Message = String.Empty;
        }

        public String Code { get; set; }
        public String Message { get; set; }
    }


    [Serializable]
    public class AccountTransactions
    {
        public AccountTransactions()
        {
        }

        public AccountTransactions(String id, String reference, String merchant, String productTypeId,
            String productId, String amount, String phone, String responseCode, String transactionDate,
            String authorizationNumber, String transactionTypeId, String transactionTypeName)
        {
            this.Id = id;
            this.Reference = reference;
            this.Merchant = merchant;
            this.ProductTypeId = productTypeId;
            this.ProductId = productId;
            this.Amount = amount;
            this.Phone = phone;
            this.ResponseCode = responseCode;
            this.TransactionDate = transactionDate;
            this.AuthorizationNumber = authorizationNumber;
            this.TransationTypeId = transactionTypeId;
            this.TransactionTypeName = transactionTypeName;
        }

        public String Id {get; set;}
        public String Reference { get; set; }
        public String Merchant { get; set; }
        public String ProductTypeId { get; set; }
        public String ProductId { get; set; }
        public String Amount { get; set; }
        public String Phone { get; set; }
        public String ResponseCode { get; set; }
        public String TransactionDate { get; set; }
        public String AuthorizationNumber { get; set; }
        public String TransationTypeId { get; set; }
        public String TransactionTypeName { get; set; }
    }


    [Serializable]
    public class ProductTypeResponse : Response
    {
        public ProductTypes[] ProductTypes { get; set; }
    }
    

    [Serializable]
    public class AccountTransactionResponse : Response
    {
        public AccountTransactions[] AccountTransactions { get; set; }
    }


    [Serializable]
    public class BalanceResponse : Response
    {
        public String Balance { get; set; }
    }


    [Serializable]
    public class CarriersResponse : Response
    {
        public Carriers[] Carriers { get; set; }
    }


    [Serializable]
    public class CurrenciesResponse : Response
    {
        public Currencies[] Currencies { get; set; }
    }


    [Serializable]
    public class CitiesResponse : Response
    {
        public Cities[] Cities { get; set; }
    }



    /// <summary>
    /// Summary description for Service
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Service : System.Web.Services.WebService
    {

        private const String USER_NAME_REQUIRE = "AC1";
        private const String PASSWORD_REQUIRE = "AC2";
        private const String ACCOUNT_ID_REQUIRE = "AC3";
        private const String REFERENCE_REQUIRE = "AC4";
        private const String PRODUCT_ID_REQUIRE = "AC5";
        private const String AMOUNT_REQUIRE = "AC7";
        private const String PHONE_REQUIRE = "AC8";
        private const String USER_NOT_AUTHORIZED = "AC9";
        private const String UNCAUGHT_ERROR = "AC10";
        private const String UNCAUGHT_ERROR_MESSAGE = "TECHNICAL ERROR";
        private const String TRANSACTION_ID_REQUIRE = "AC11";
        private const String AUTHORIZATION_NUMBER_REQUIRE = "AC12";
        private const String NO_RECORDS_FOUND = "AC13";
        private const String COUNTRY_ID_REQUIRE = "AC14";
        private const String DATE_REQUIRE = "AC15";
        private const String INVALID_DATE = "DL3";
        private const String AT_LEAST_TWO = "AC15";
        private const String PRASAN = "5";
        private readonly String OFFICIAL_PREPAID_NATION = "502";
        private readonly String AIPC_PREPAID_NATION = "982";
        private readonly String TRUMPIA = "583";
        private readonly String SOLOPIN = "21";
        private readonly String SOLOPIN_4G = "22";
        private readonly String ARIN_SMS = "1001";

        private readonly String LOCAL_PIN = System.Configuration.ConfigurationManager.AppSettings["local_pin"].ToString();
        private readonly String PIN = System.Configuration.ConfigurationManager.AppSettings["pin"].ToString();
        private readonly String RECHARGE = System.Configuration.ConfigurationManager.AppSettings["recharge"].ToString();

        

        private static readonly ILog log = LogManager.GetLogger("AccountWs");

        public static String Locale;
        public String UserDb;
        

        public Service()
        {
            Locale = CultureInfo.CurrentCulture.Name;
            this.UserDb = System.Configuration.ConfigurationManager.AppSettings["user_db"].ToString();
        }


        [WebMethod]
        public Response Login(String username, String password, String accountId)
        {
            String strEncryptedTicket = null, details = String.Empty, ip = null, encode = null;
            HttpCookie cookie = null;
            Response result = new Response();
            StringBuilder output = new StringBuilder();
            ProfileLoginResult loginResult = null;
            DateTime? begin = DateTime.Now, end = null;
            double totalSeg = 0;
            
            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                if (String.IsNullOrEmpty(username))
                {
                    throw new Exception(USER_NAME_REQUIRE);
                }

                if (String.IsNullOrEmpty(password))
                {
                    throw new Exception(PASSWORD_REQUIRE);
                }

                if (String.IsNullOrEmpty(accountId))
                {
                    throw new Exception(ACCOUNT_ID_REQUIRE);
                }

                using (AccountProfileDAO dao = new AccountProfileDAO())
                {
                    loginResult = dao.LoginProfile(username, password, accountId, ip, Locale);    
                }

                result.Code = loginResult.Code;
                result.Message = loginResult.Message;

                if (result.Code.Equals("0"))
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;

                    using (EncryptDAO dao = new EncryptDAO())
                    {
                        encode = dao.Encrypt(username + "#" + accountId + "#" + dao.Encrypt(password));
                    }
                    
                    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
                    encode,
                    DateTime.Now,
                    DateTime.Now.AddMinutes(60),
                    false,
                    String.Empty);

                    strEncryptedTicket = FormsAuthentication.Encrypt(ticket);

                    cookie = new HttpCookie(FormsAuthentication.FormsCookieName, strEncryptedTicket);

                    Context.Response.Cookies.Add(cookie);
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    using (ResponseCodeDAO dao = new ResponseCodeDAO())
                    {
                        DataAccessLayer.DataAccessObjects.ResponseCodeResult res = dao.Search(result.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            result.Code = res.ResponseCode[0].Code;
                            result.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            result.Code = UNCAUGHT_ERROR;
                            result.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                end = DateTime.Now;

                TimeSpan? diff = end.Value - begin.Value;
                totalSeg = diff.Value.TotalSeconds;

                Common.Utils.Concat(ref output, null, "LOGIN:", false, true);
                Common.Utils.Concat(ref output, "User", username, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Seg", totalSeg, false, false);
                Common.Utils.Concat(ref output, "Code", result.Code, false, false);
                Common.Utils.Concat(ref output, "Message", result.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return result;
        }


        [WebMethod]
        public Response Logout()
        {
            Response result = new Response();
            String user = String.Empty, details = String.Empty, ip = String.Empty;
            StringBuilder output = new StringBuilder();
            DateTime? begin = DateTime.Now, end = null;
            double totalSeg = 0;

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                FormsAuthentication.SignOut();

                user = Context.User.Identity.Name;

                result.Code = Common.Utils.APPROVED;
                result.Message = Common.Utils.APPROVED_MESSAGE;
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    using (ResponseCodeDAO dao = new ResponseCodeDAO())
                    {
                        DataAccessLayer.DataAccessObjects.ResponseCodeResult res = dao.Search(result.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            result.Code = res.ResponseCode[0].Code;
                            result.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            result.Code = UNCAUGHT_ERROR;
                            result.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                end = DateTime.Now;

                TimeSpan? diff = end.Value - begin.Value;
                totalSeg = diff.Value.TotalSeconds;

                Common.Utils.Concat(ref output, null, "LOGOUT:", false, true);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Seg", totalSeg, false, false);
                Common.Utils.Concat(ref output, "Code", result.Code, false, false);
                Common.Utils.Concat(ref output, "Message", result.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return result;
        }

        private TransactionResult SendProviders(RoutingResult routingResult, ref StringBuilder providers,
            ref DateTime? providerStart, ref DateTime? providerEnd, String phone,
            String smsMessage, String plLocale, String plPhoneNotification,
            String storeId, String storeName)
        {
            TransactionResult result = new TransactionResult();

            try
            {
                foreach (RoutingProvider item in routingResult.RoutingProvider)
                {
                    Common.Utils.Concat(ref providers, "RoutingId", item.RoutingId, false, false);
                    Common.Utils.Concat(ref providers, "ProviderId", item.ProviderId, false, false);
                    Common.Utils.Concat(ref providers, "ProviderName", item.ProviderName, false, false);
                    Common.Utils.Concat(ref providers, "ProductCode", item.ProductCode, false, false);
                    Common.Utils.Concat(ref providers, "User", item.User, false, false);
                    Common.Utils.Concat(ref providers, "LocalCurrencyAmount", item.LocalCurrencyAmount, false, false);


                    if (item.ProviderId.Equals(PRASAN))
                    {
                        providerStart = DateTime.Now;

                        PrasanWcf.RechargeRequest req = new PrasanWcf.RechargeRequest();
                        PrasanWcf.RechargeResponse resp = null;

                        if (item.TransactionMode.Equals("0")) //Fix
                        {
                            req.Amount = String.Empty;
                            req.Type = Common.Utils.FIX_RECHARGE;
                        }
                        else
                        {
                            req.Amount = item.LocalCurrencyAmount;
                            req.Type = Common.Utils.FLEXI_RECHARGE;
                        }

                        using (PrasanWcf.IappClient wcf = new PrasanWcf.IappClient())
                        {
                            req.Phone = item.Phone;
                            req.ProductId = item.ProductCode;
                            req.TransactionId = item.RoutingId;
                            req.Url = item.Url;
                            req.LoginId = item.User;

                            using (EncryptDAO dao = new EncryptDAO())
                            {
                                req.ProtectedKey = dao.Decrypt(item.Password);
                            }

                            req.DbUser = this.UserDb;
                            req.RoutingId = item.RoutingId;
                            req.Locale = Locale;

                            resp = wcf.Recharge(req);

                            result.AuthorizationNumber = resp.AuthorizationNumber;
                            result.Code = resp.Code;
                            result.Details = resp.Details;
                            result.TransactionId = item.LogId;
                        }
                        
                        providerEnd = DateTime.Now;
                    }
                    else if (item.ProviderId.Equals(TRUMPIA))
                    {
                        using (TrumpiaWcf.IappClient wcf = new TrumpiaWcf.IappClient())
                        {
                            TrumpiaWcf.SendSmsRequest req = new TrumpiaWcf.SendSmsRequest();
                            TrumpiaWcf.SendSmsResponse resp = null;

                            using (EncryptDAO dao = new EncryptDAO())
                            {
                                req.ApiKey = dao.Decrypt(item.Password);
                            }

                            req.CountryCode = "1";
                            req.DbUser = this.UserDb;
                            req.FirstName = "OFFICIAL";
                            req.LastName = "OFFICIAL";
                            req.ListName = item.User;
                            req.Locale = Locale;
                            req.Message = smsMessage;
                            req.RoutingId = item.RoutingId;
                            req.Url = item.Url;
                            req.Phone = item.Phone;

                            resp = wcf.SendSms(req);

                            result.Code = resp.Code;
                            result.AuthorizationNumber = resp.AuthorizationNumber;
                            result.Details = resp.Details;

                        }
                    }
                    else if (item.ProviderId.Equals(ARIN_SMS))
                    {
                        providerStart = DateTime.Now;

                        using (ArinWcf.IappClient wcf = new ArinWcf.IappClient())
                        {
                            ArinWcf.SmsRequest req = new ArinWcf.SmsRequest();
                            ArinWcf.Response resp = null;

                            req.DbUser = this.UserDb;
                            req.Locale = Locale;
                            req.Message = smsMessage;
                            req.Phone = phone;
                            req.RoutingId = item.RoutingId;

                            using (EncryptDAO dao = new EncryptDAO())
                            {
                                req.ServerPassword = dao.Decrypt(item.Password);    
                            }

                            req.Url = item.Url;
                            
                            resp = wcf.SendSms(req);

                            result.Code = resp.Code;
                            result.Message = resp.Message;
                            result.AuthorizationNumber = resp.AuthorizationNumber;
                        }
                    }
                    else if (item.ProviderId.Equals(SOLOPIN) || item.ProviderId.Equals(SOLOPIN_4G))
                    {
                        providerStart = DateTime.Now;

                        using (PLWcf.IappClient wcf = new PLWcf.IappClient())
                        {
                            LoginRequest loginReq = new LoginRequest();
                            PLWcf.LoginResponse loginResponse = null;
                            BuyRequest req = new BuyRequest();
                            BuyResponse buyResponse = null;
                            PLWcf.Request lreq = new Request();

                            loginReq.UserName = item.User;

                            using (EncryptDAO dao = new EncryptDAO())
                            {
                                loginReq.Password = dao.Decrypt(item.Password);
                            }

                            loginResponse = wcf.Login(loginReq);

                            if (loginResponse.Code.Equals("0"))
                            {
                                req.AccountNumber = Convert.ToInt64(item.Phone);
                                req.LocaleId = plLocale;
                                req.ProductId = item.ProductCode;
                                req.Reference = Convert.ToInt64(item.RoutingId);
                                req.StoreId = Convert.ToInt32(storeId);
                                req.StoreName = storeName;
                                req.TransAmount = Convert.ToDouble(item.CurrencyAmount);
                                req.SessionId = loginResponse.SessionId;
                                req.RoutingId = item.RoutingId;
                                req.DbUser = this.UserDb;
                                req.Locale = Locale;

                                if (!String.IsNullOrEmpty(plPhoneNotification))
                                {
                                    req.PhoneNotification = Convert.ToInt32(plPhoneNotification);
                                }

                                buyResponse = wcf.Buy(req);

                                result.Code = buyResponse.Code;
                                result.Message = buyResponse.Message;

                                if (result.Code.Equals(Common.Utils.APPROVED))
                                {
                                    result.Message = Common.Utils.APPROVED_MESSAGE;
                                    result.AuthorizationNumber = buyResponse.TransReference.ToString();
                                }

                                lreq.SessionId = loginResponse.SessionId;

                                wcf.Logout(lreq);
                            }
                        }

                        providerEnd = DateTime.Now;
                    }
                    else if (item.ProviderId.Equals(OFFICIAL_PREPAID_NATION) || item.ProviderId.Equals(AIPC_PREPAID_NATION))
                    {
                        providerStart = DateTime.Now;

                        if (item.ProductTypeId.Equals(RECHARGE))
                        {
                            using (PNWcf.IappClient wcf = new PNWcf.IappClient())
                            {
                                PNWcf.RechargeRequest req = new PNWcf.RechargeRequest();
                                PNWcf.RechargeResponse resp = null;

                                req.Amount = Convert.ToDecimal(item.CurrencyAmount);

                                using (EncryptDAO dao = new EncryptDAO())
                                {
                                    req.Password = dao.Decrypt(item.Password);
                                }

                                req.Phone = item.Phone;
                                req.ProductId = item.ProductCode;
                                req.StoreId = item.TerminalId;
                                req.TransactionId = item.RoutingId;
                                req.Url = item.Url;
                                req.User = item.User;
                                req.DbUser = this.UserDb;
                                req.RoutingId = item.RoutingId;
                                req.Locale = Locale;

                                resp = wcf.Recharge(req);

                                result.TransactionId = item.LogId;
                                result.AuthorizationNumber = resp.AuthorizationNumber;
                                result.Code = resp.Code;
                                result.Details = resp.Details;
                                result.Message = resp.Message;
                            }
                        }
                        else if (item.ProductTypeId.Equals(PIN))
                        {
                            using (PNWcf.IappClient wcf = new PNWcf.IappClient())
                            {
                                PNWcf.PurchasePinRequest req = new PNWcf.PurchasePinRequest();
                                PNWcf.PurchasePinResponse resp = null;

                                using (EncryptDAO dao = new EncryptDAO())
                                {
                                    req.Password = dao.Decrypt(item.Password);
                                }

                                req.ProductId = item.ProductCode;
                                req.StoreId = item.TerminalId;
                                req.TransactionId = item.RoutingId;
                                req.Url = item.Url;
                                req.User = item.User;
                                req.DbUser = this.UserDb;
                                req.RoutingId = item.RoutingId;
                                req.Locale = Locale;

                                resp = wcf.PurchasePin(req);

                                result.TransactionId = item.LogId;
                                result.Code = resp.Code;
                                result.Details = resp.Details;
                                result.Message = resp.Message;
                                result.PinNumber = resp.PinNumber;
                                result.SerialNumber = resp.SerialNumber;
                            }
                        }

                        providerEnd = DateTime.Now;
                    }

                    else
                    {
                        result.Code = Common.Utils.PROVIDER_NOT_FOUND;

                    }

                    using (ResponseCodeDAO dao = new ResponseCodeDAO())
                    {
                        Result response = dao.GetResponseCode(item.ProviderId, result.Code, result.Message, Locale, this.UserDb);

                        result.Code = response.Code;
                        result.Message = response.Message;
                    }

                    result.TransactionId = item.LogId;

                    Common.Utils.Concat(ref providers, "Code", result.Code, false, false);
                    Common.Utils.Concat(ref providers, "Message", result.Message, false, false);
                    Common.Utils.Concat(ref providers, "AuthNumber", result.AuthorizationNumber, false, false);
                    Common.Utils.Concat(ref providers, "PinNumber", result.PinNumber, false, false);
                    Common.Utils.Concat(ref providers, "SerialNumber", result.SerialNumber, false, false);
                    Common.Utils.Concat(ref providers, "TransactionDateTime", result.TransactionDateTime, true, true);

                    if (result.Code.Equals(Common.Utils.APPROVED))
                    {
                        break;
                    }
                } 
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNCAUGHT_ERROR;
                result.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
            }

            return result;
        }

        [WebMethod]
        public TransactionResult Buy(String reference, String merchant, String productId, String amount, String phone)
        {
            String user = String.Empty, details = String.Empty, accountId = String.Empty, ip = String.Empty,
                encode = String.Empty, password = String.Empty, decode = String.Empty, routingId = String.Empty,
                transactionTimeout = String.Empty, transactionId = String.Empty, timeoutException = String.Empty;
            String[] decodeArray = null;
            TransactionResult result = new TransactionResult();
            StringBuilder output = new StringBuilder();
            RoutingResult routingResult = new DataAccessLayer.DataAccessObjects.RoutingResult();
            DateTime start = DateTime.Now, end;
            DateTime? providerStart = null, providerEnd = null;
            StringBuilder providers = new StringBuilder();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                if (!Context.User.Identity.IsAuthenticated)
                {
                    throw new Exception(USER_NOT_AUTHORIZED);
                }

                encode = Context.User.Identity.Name;

                using (EncryptDAO encDao = new EncryptDAO())
                {
                    decode = encDao.Decrypt(encode);
                }

                decodeArray = decode.Split('#');
                
                user = decodeArray[0];
                accountId = decodeArray[1];
                password = decodeArray[2];

                using (RoutingDAO dao = new RoutingDAO())
                {
                    routingResult = dao.Buy(user, password, accountId, productId, amount, 
                        phone, reference, ip, merchant, null, "0", Locale, UserDb);
                }

                if (!String.IsNullOrEmpty(routingResult.LogId))
                {
                    if (!routingResult.LogId.ToLower().Equals("null"))
                    {
                        transactionId = routingResult.LogId;
                    }
                    else
                    {
                        routingResult.LogId = String.Empty;
                    }
                }

                if (routingResult.Code.Equals("0"))
                {
                    result = this.SendProviders(routingResult, ref providers, ref providerStart, ref providerEnd, phone,
                        null, null, null, null, null);
                }
                else
                {
                    if (!String.IsNullOrEmpty(routingResult.LogId))
                    {
                        using (ApiLogDAO apiLogDao = new ApiLogDAO())
                        {
                            Result apiLogResult = apiLogDao.SearchApiLogResponseCode(routingResult.LogId);

                            if (apiLogResult.Code.Equals(Common.Utils.NO_RECORDS_FOUND) || apiLogResult.Code.Equals(Common.Utils.UNKNOWN_EXCEPTION))
                            {
                                result.Code = routingResult.Code;
                                result.Message = routingResult.Message;
                            }
                            else
                            {
                                result.Code = apiLogResult.Code;
                                result.Message = apiLogResult.Message;
                            }
                        }
                    }
                    else
                    {
                        result.Code = Common.Utils.UNCAUGHT_ERROR;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            result.Code = res.ResponseCode[0].Code;
                            result.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            result.Code = UNCAUGHT_ERROR;
                            result.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                details = result.Details;
                result.Details = String.Empty;

                end = DateTime.Now;

                Common.Utils.Concat(ref output, null, "BUY:", false, true);
                Common.Utils.Concat(ref output, "StartTime", start.ToString("hh:mm:ss tt"), false, false);
                Common.Utils.Concat(ref output, "EndDate", end.ToString("hh:mm:ss tt"), false, false);
                    
                if (providerStart != null)
                {
                    Common.Utils.Concat(ref output, "ProviderStartTime", providerStart.Value.ToString("hh:mm:ss tt"), false, false);
                }

                if (providerEnd != null)
                {
                    Common.Utils.Concat(ref output, "ProviderEndTime", providerEnd.Value.ToString("hh:mm:ss tt"), false, false);
                }

                Common.Utils.Concat(ref output, "TransactionId", transactionId, false, false);
                Common.Utils.Concat(ref output, "Reference", reference, false, false);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "ProductId", productId, false, false);
                Common.Utils.Concat(ref output, "Amount", amount, false, false);
                Common.Utils.Concat(ref output, "Phone", phone, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", result.Code, false, false);
                Common.Utils.Concat(ref output, "Message", result.Message, false, false);
                Common.Utils.Concat(ref output, "AuthNumber", result.AuthorizationNumber, false, false);
                Common.Utils.Concat(ref output, "PinNumber", result.PinNumber, false, false);
                Common.Utils.Concat(ref output, "SerialNumber", result.SerialNumber, false, false);
                Common.Utils.Concat(ref output, "TranDate", result.TransactionDateTime, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);
                //Common.Utils.Concat(ref output, "ProvidersOutput", providers.ToString(), true, true);


                log.Debug(output.ToString());
                
            }

            return result;
        }


        [WebMethod]
        public TransactionResultSp Buy_SP_Pinless(String reference, String merchant, String productId, String amount,
            String phone, String distributor, String plLocale, String plPhoneNotification)
        {
            String user = String.Empty, details = String.Empty, accountId = String.Empty, ip = String.Empty,
                encode = String.Empty, password = String.Empty, decode = String.Empty, routingId = String.Empty,
                transactionTimeout = String.Empty, transactionId = String.Empty, timeoutException = String.Empty;
            String[] decodeArray = null;
            Common.TransactionResultSp result = new Common.TransactionResultSp();
            StringBuilder output = new StringBuilder();
            DataAccessLayer.DataAccessObjects.RoutingResult routingResult = new DataAccessLayer.DataAccessObjects.RoutingResult();
            DateTime start = DateTime.Now, end;
            DateTime? providerStart = null, providerEnd = null;


            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                if (!Context.User.Identity.IsAuthenticated)
                {
                    throw new Exception(USER_NOT_AUTHORIZED);
                }

                encode = Context.User.Identity.Name;

                using (EncryptDAO encDao = new EncryptDAO())
                {
                    decode = encDao.Decrypt(encode);
                }

                decodeArray = decode.Split('#');
                
                user = decodeArray[0];
                accountId = decodeArray[1];
                password = decodeArray[2];

                using (RoutingDAO dao = new RoutingDAO())
                {
                    routingResult = dao.Buy(user, password, accountId, productId, amount, phone, reference, ip, merchant, null, "0", Locale, UserDb);
                }

                if (!String.IsNullOrEmpty(routingResult.LogId))
                {
                    if (!routingResult.LogId.ToLower().Equals("null"))
                    {
                        transactionId = routingResult.LogId;
                    }
                    else
                    {
                        routingResult.LogId = String.Empty;
                    }
                }

                if (routingResult.Code.Equals("0"))
                {
                    foreach (RoutingProvider item in routingResult.RoutingProvider)
                    {
                        if (item.ProviderId.Equals(PRASAN))
                        {
                            providerStart = DateTime.Now;

                            PrasanWcf.RechargeRequest req = new PrasanWcf.RechargeRequest();
                            PrasanWcf.RechargeResponse resp = null;

                            if (item.TransactionMode.Equals("0")) //Fix
                            {
                                req.Amount = String.Empty;
                                req.Type = Common.Utils.FIX_RECHARGE;
                            }
                            else
                            {
                                req.Amount = item.LocalCurrencyAmount;
                                req.Type = Common.Utils.FLEXI_RECHARGE;
                            }

                            using (PrasanWcf.IappClient wcf = new PrasanWcf.IappClient())
                            {
                                req.Phone = item.Phone;
                                req.ProductId = item.ProductCode;
                                req.TransactionId = item.RoutingId;
                                req.Url = item.Url;
                                req.LoginId = item.User;

                                using (EncryptDAO dao = new EncryptDAO())
                                {
                                    req.ProtectedKey = dao.Decrypt(item.Password);
                                }

                                req.DbUser = this.UserDb;
                                req.RoutingId = item.RoutingId;
                                req.Locale = Locale;

                                resp = wcf.Recharge(req);

                                result.AuthorizationNumber = resp.AuthorizationNumber;
                                result.Code = resp.Code;
                                result.Details = resp.Details;
                                result.TransactionId = item.LogId;
                            }
                            
                            providerEnd = DateTime.Now;
                        }
                        else if (item.ProviderId.Equals(OFFICIAL_PREPAID_NATION) || item.ProviderId.Equals(AIPC_PREPAID_NATION))
                        {
                            providerStart = DateTime.Now;

                            if (item.ProductTypeId.Equals(RECHARGE))
                            {
                                using (PNWcf.IappClient wcf = new PNWcf.IappClient())
                                {
                                    PNWcf.RechargeRequest req = new PNWcf.RechargeRequest();
                                    PNWcf.RechargeResponse resp = null;

                                    req.Amount = Convert.ToDecimal(item.CurrencyAmount);

                                    using (EncryptDAO dao = new EncryptDAO())
                                    {
                                        req.Password = dao.Decrypt(item.Password);
                                    }

                                    req.Phone = item.Phone;
                                    req.ProductId = item.ProductCode;
                                    req.StoreId = item.TerminalId;
                                    req.TransactionId = item.RoutingId;
                                    req.Url = item.Url;
                                    req.User = item.User;
                                    req.DbUser = this.UserDb;
                                    req.RoutingId = item.RoutingId;
                                    req.Locale = Locale;

                                    resp = wcf.Recharge(req);

                                    result.TransactionId = item.LogId;
                                    result.AuthorizationNumber = resp.AuthorizationNumber;
                                    result.Code = resp.Code;
                                    result.Details = resp.Details;
                                    result.Message = resp.Message;
                                }
                            }
                            else if (item.ProductTypeId.Equals(PIN))
                            {
                                using (PNWcf.IappClient wcf = new PNWcf.IappClient())
                                {
                                    PNWcf.PurchasePinRequest req = new PNWcf.PurchasePinRequest();
                                    PNWcf.PurchasePinResponse resp = null;

                                    using (EncryptDAO dao = new EncryptDAO())
                                    {
                                        req.Password = dao.Decrypt(item.Password);
                                    }

                                    req.ProductId = item.ProductCode;
                                    req.StoreId = item.TerminalId;
                                    req.TransactionId = item.RoutingId;
                                    req.Url = item.Url;
                                    req.User = item.User;
                                    req.DbUser = this.UserDb;
                                    req.RoutingId = item.RoutingId;
                                    req.Locale = Locale;

                                    resp = wcf.PurchasePin(req);

                                    result.TransactionId = item.LogId;
                                    result.Code = resp.Code;
                                    result.Details = resp.Details;
                                    result.Message = resp.Message;
                                    result.PinNumber = resp.PinNumber;
                                    result.SerialNumber = resp.SerialNumber;
                                }
                            }
                            
                            providerEnd = DateTime.Now;
                        }
                        else if(item.ProviderId.Equals(SOLOPIN) || item.ProviderId.Equals(SOLOPIN_4G))
                        {
                            providerStart = DateTime.Now;

                            using (PLWcf.IappClient wcf = new PLWcf.IappClient())
                            {
                                LoginRequest loginReq = new LoginRequest();
                                PLWcf.LoginResponse loginResponse = null;
                                BuyRequest req = new BuyRequest();
                                BuyResponse buyResponse = null;
                                PLWcf.Request lreq = new Request();

                                loginReq.UserName = item.User;

                                using (EncryptDAO dao = new EncryptDAO())
                                {
                                    loginReq.Password = dao.Decrypt(item.Password);
                                }

                                loginResponse = wcf.Login(loginReq);

                                if (loginResponse.Code.Equals("0"))
                                {
                                    req.AccountNumber = Convert.ToInt64(item.Phone);
                                    req.LocaleId = plLocale;
                                    req.ProductId = item.ProductCode;
                                    req.Reference = Convert.ToInt64(item.RoutingId);
                                    req.StoreId = Convert.ToInt32(accountId);
                                    req.TransAmount = Convert.ToDouble(item.CurrencyAmount);
                                    req.SessionId = loginResponse.SessionId;
                                    req.RoutingId = item.RoutingId;
                                    req.DbUser = this.UserDb;
                                    req.Locale = Locale;

                                    if (!String.IsNullOrEmpty(plPhoneNotification))
                                    {
                                        req.PhoneNotification = Convert.ToInt32(plPhoneNotification);
                                    }

                                    buyResponse = wcf.Buy(req);

                                    result.Code = buyResponse.Code;
                                    result.Message = buyResponse.Message;

                                    if (result.Code.Equals(Common.Utils.APPROVED))
                                    {
                                        result.Message = Common.Utils.APPROVED_MESSAGE;
                                        result.AuthorizationNumber = buyResponse.TransReference.ToString();
                                    }

                                    lreq.SessionId = loginResponse.SessionId;

                                    wcf.Logout(lreq);
                                }

                            }

                            providerEnd = DateTime.Now;
                            
                        }
                        else
                        {
                            result.Code = Common.Utils.PROVIDER_NOT_FOUND;
                        }

                        using (ResponseCodeDAO dao = new ResponseCodeDAO())
                        {
                            Result response = dao.GetResponseCode(item.ProviderId, result.Code, result.Message, Locale, this.UserDb);

                            result.Code = response.Code;
                            result.Message = response.Message;
                        }

                        result.TransactionId = item.LogId;

                        if (result.Code.Equals(Common.Utils.APPROVED))
                        {
                            break;
                        }
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(routingResult.LogId))
                    {
                        using (ApiLogDAO apiLogDao = new ApiLogDAO())
                        {
                            Result apiLogResult = apiLogDao.SearchApiLogResponseCode(routingResult.LogId);

                            if (apiLogResult.Code.Equals(Common.Utils.NO_RECORDS_FOUND) || apiLogResult.Code.Equals(Common.Utils.UNKNOWN_EXCEPTION))
                            {
                                result.Code = routingResult.Code;
                                result.Message = routingResult.Message;
                            }
                            else
                            {
                                result.Code = apiLogResult.Code;
                                result.Message = apiLogResult.Message;
                            }
                        }
                    }
                    else
                    {
                        result.Code = Common.Utils.UNCAUGHT_ERROR;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            result.Code = res.ResponseCode[0].Code;
                            result.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            result.Code = UNCAUGHT_ERROR;
                            result.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                end = DateTime.Now;

                Common.Utils.Concat(ref output, null, "BUY_SP_PINLESS:", false, true);
                Common.Utils.Concat(ref output, "StartTime", start.ToString("hh:mm:ss tt"), false, false);
                Common.Utils.Concat(ref output, "EndDate", end.ToString("hh:mm:ss tt"), false, false);

                if (providerStart != null)
                {
                    Common.Utils.Concat(ref output, "ProviderStartTime", providerStart.Value.ToString("hh:mm:ss tt"), false, false);
                }

                if (providerEnd != null)
                {
                    Common.Utils.Concat(ref output, "ProviderEndTime", providerEnd.Value.ToString("hh:mm:ss tt"), false, false);
                }

                Common.Utils.Concat(ref output, "TransactionId", result.TransactionId, false, false);
                Common.Utils.Concat(ref output, "Reference", reference, false, false);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "ProductId", productId, false, false);
                Common.Utils.Concat(ref output, "Amount", amount, false, false);
                Common.Utils.Concat(ref output, "Phone", phone, false, false);
                Common.Utils.Concat(ref output, "Distributor", distributor, false, false);
                Common.Utils.Concat(ref output, "PlLocale", plLocale, false, false);
                Common.Utils.Concat(ref output, "PlPhonenotification", plPhoneNotification, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", result.Code, false, false);
                Common.Utils.Concat(ref output, "Message", result.Message, false, false);
                Common.Utils.Concat(ref output, "AuthNumber", result.AuthorizationNumber, false, false);
                Common.Utils.Concat(ref output, "PinNumber", result.PinNumber, false, false);
                Common.Utils.Concat(ref output, "SerialNumber", result.SerialNumber, false, false);
                Common.Utils.Concat(ref output, "TranDate", result.TransactionDateTime, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return result;
        }

        [WebMethod]
        public TransactionResult BuyAll(BuyAllRequest Request)
        {
            String user = String.Empty, details = String.Empty, accountId = String.Empty, ip = String.Empty,
                encode = String.Empty, password = String.Empty, decode = String.Empty, routingId = String.Empty,
                transactionTimeout = String.Empty, transactionId = String.Empty, timeoutException = String.Empty;
            String[] decodeArray = null;
            TransactionResult result = new Common.TransactionResult();
            StringBuilder output = new StringBuilder();
            DataAccessLayer.DataAccessObjects.RoutingResult routingResult = new DataAccessLayer.DataAccessObjects.RoutingResult();
            DateTime start = DateTime.Now, end;
            DateTime? providerStart = null, providerEnd = null;
            StringBuilder providers = new StringBuilder();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                if (!Context.User.Identity.IsAuthenticated)
                {
                    throw new Exception(USER_NOT_AUTHORIZED);
                }

                encode = Context.User.Identity.Name;

                using (EncryptDAO encDao = new EncryptDAO())
                {
                    decode = encDao.Decrypt(encode);
                }

                decodeArray = decode.Split('#');

                user = decodeArray[0];
                accountId = decodeArray[1];
                password = decodeArray[2];

                using (RoutingDAO dao = new RoutingDAO())
                {
                    routingResult = dao.Buy(user, password, accountId, Request.ProductId, Request.Amount,
                        Request.Phone, Request.Reference, ip, Request.Merchant, null, "0", Locale, UserDb);
                }

                if (!String.IsNullOrEmpty(routingResult.LogId))
                {
                    if (!routingResult.LogId.ToLower().Equals("null"))
                    {
                        transactionId = routingResult.LogId;
                    }
                    else
                    {
                        routingResult.LogId = String.Empty;
                    }
                }

                if (routingResult.Code.Equals("0"))
                {
                    result = this.SendProviders(routingResult, ref providers, ref providerStart, ref providerEnd, Request.Phone,
                        Request.SmsMessage, Request.PlLocale, Request.PlPhoneNotification, Request.StoreId, Request.StoreName);
                }
                else
                {
                    if (!String.IsNullOrEmpty(routingResult.LogId))
                    {
                        using (ApiLogDAO apiLogDao = new ApiLogDAO())
                        {
                            Result apiLogResult = apiLogDao.SearchApiLogResponseCode(routingResult.LogId);

                            if (apiLogResult.Code.Equals(Common.Utils.NO_RECORDS_FOUND) || apiLogResult.Code.Equals(Common.Utils.UNKNOWN_EXCEPTION))
                            {
                                result.Code = routingResult.Code;
                                result.Message = routingResult.Message;
                            }
                            else
                            {
                                result.Code = apiLogResult.Code;
                                result.Message = apiLogResult.Message;
                            }
                        }
                    }
                    else
                    {
                        result.Code = Common.Utils.UNCAUGHT_ERROR;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            result.Code = res.ResponseCode[0].Code;
                            result.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            result.Code = UNCAUGHT_ERROR;
                            result.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                end = DateTime.Now;

                Common.Utils.Concat(ref output, null, "BUY_ALL:", false, true);
                Common.Utils.Concat(ref output, "StartTime", start.ToString("hh:mm:ss tt"), false, false);
                Common.Utils.Concat(ref output, "EndDate", end.ToString("hh:mm:ss tt"), false, false);

                if (providerStart != null)
                {
                    Common.Utils.Concat(ref output, "ProviderStartTime", providerStart.Value.ToString("hh:mm:ss tt"), false, false);
                }

                if (providerEnd != null)
                {
                    Common.Utils.Concat(ref output, "ProviderEndTime", providerEnd.Value.ToString("hh:mm:ss tt"), false, false);
                }

                Common.Utils.Concat(ref output, "TransactionId", transactionId, false, false);
                Common.Utils.Concat(ref output, "Reference", Request.Reference, false, false);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "ProductId", Request.ProductId, false, false);
                Common.Utils.Concat(ref output, "Amount", Request.Amount, false, false);
                Common.Utils.Concat(ref output, "Phone", Request.Phone, false, false);
                Common.Utils.Concat(ref output, "Distributor", Request.Distributor, false, false);
                Common.Utils.Concat(ref output, "PlLocale", Request.PlLocale, false, false);
                Common.Utils.Concat(ref output, "PlPhonenotification", Request.PlPhoneNotification, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", result.Code, false, false);
                Common.Utils.Concat(ref output, "Message", result.Message, false, false);
                Common.Utils.Concat(ref output, "AuthNumber", result.AuthorizationNumber, false, false);
                Common.Utils.Concat(ref output, "PinNumber", result.PinNumber, false, false);
                Common.Utils.Concat(ref output, "SerialNumber", result.SerialNumber, false, false);
                Common.Utils.Concat(ref output, "TranDate", result.TransactionDateTime, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return result;
        }

        [WebMethod]
        public TransactionResult BuySms(String reference, String merchant,
            String productId, String phone, String message)
        {
            String user = String.Empty, details = String.Empty, accountId = String.Empty, ip = String.Empty,
                decode = String.Empty, password = String.Empty, encode = String.Empty, timeoutException = String.Empty,
                transactionId = String.Empty, transactionTimeout = String.Empty;
            String[] decodeArray = null;
            TransactionResult result = new TransactionResult();
            StringBuilder output = new StringBuilder();
            DataAccessLayer.DataAccessObjects.RoutingResult routingResult = new DataAccessLayer.DataAccessObjects.RoutingResult();
            DateTime start = DateTime.Now, end;
            DateTime? providerStart = null, providerEnd = null;
            StringBuilder providers = new StringBuilder();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                if (!Context.User.Identity.IsAuthenticated)
                {
                    throw new Exception(USER_NOT_AUTHORIZED);
                }

                encode = Context.User.Identity.Name;

                using (EncryptDAO encDao = new EncryptDAO())
                {
                    decode = encDao.Decrypt(encode);    
                }
                
                decodeArray = decode.Split('#');

                user = decodeArray[0];
                accountId = decodeArray[1];
                password = decodeArray[2];

                using (RoutingDAO dao = new RoutingDAO())
                {
                    routingResult = dao.Buy(user, password, accountId,
                        productId, null, phone, reference, ip, merchant, message, "0", Locale, UserDb);
                }

                result.Message = routingResult.Message;
                result.Details = routingResult.Details;

                if (!String.IsNullOrEmpty(routingResult.LogId))
                {
                    if (!routingResult.LogId.ToLower().Equals("null"))
                    {
                        transactionId = routingResult.LogId;
                    }
                    else
                    {
                        routingResult.LogId = String.Empty;
                    }
                }

                if (routingResult.Code.Equals("0"))
                {
                    result = this.SendProviders(routingResult, ref providers, ref providerStart, ref providerEnd, phone,
                        message, null, null, null, null);
                }
                else
                {
                    if (!String.IsNullOrEmpty(routingResult.LogId))
                    {
                        using (ApiLogDAO apiLogDao = new ApiLogDAO())
                        {
                            Result apiLogResult = apiLogDao.SearchApiLogResponseCode(routingResult.LogId);

                            if (apiLogResult.Code.Equals(Common.Utils.NO_RECORDS_FOUND) || apiLogResult.Code.Equals(Common.Utils.UNKNOWN_EXCEPTION))
                            {
                                result.Code = routingResult.Code;
                                result.Message = routingResult.Message;
                            }
                            else
                            {
                                result.Code = apiLogResult.Code;
                                result.Message = apiLogResult.Message;
                            }
                        }
                        
                        
                    }
                    else
                    {
                        result.Code = Common.Utils.UNCAUGHT_ERROR;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            result.Code = res.ResponseCode[0].Code;
                            result.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            result.Code = UNCAUGHT_ERROR;
                            result.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                end = DateTime.Now;

                Common.Utils.Concat(ref output, null, "BUY_SMS:", false, true);
                Common.Utils.Concat(ref output, "StartTime", start.ToString("hh:mm:ss tt"), false, false);
                Common.Utils.Concat(ref output, "EndDate", end.ToString("hh:mm:ss tt"), false, false);
                    
                if (providerStart != null)
                {
                    Common.Utils.Concat(ref output, "ProviderStartTime", providerStart.Value.ToString("hh:mm:ss tt"), false, false);
                }

                if (providerEnd != null)
                {
                    Common.Utils.Concat(ref output, "ProviderEndTime", providerEnd.Value.ToString("hh:mm:ss tt"), false, false);
                }

                Common.Utils.Concat(ref output, "TransactionId", result.TransactionId, false, false);
                Common.Utils.Concat(ref output, "Reference", reference, false, false);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "ProductId", productId, false, false);
                Common.Utils.Concat(ref output, "Phone", phone, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", result.Code, false, false);
                Common.Utils.Concat(ref output, "Message", result.Message, false, false);
                Common.Utils.Concat(ref output, "AuthNumber", result.AuthorizationNumber, false, false);
                Common.Utils.Concat(ref output, "TranDate", result.TransactionDateTime, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);


            }

            return result;
        }


        [WebMethod]
        public Response Void(String transactionId,
            String reference, String amount, String authorizationNumber, String phone)
        {
            String user = String.Empty, details = String.Empty, accountId = String.Empty, ip = null, decode = null, password = null, encode = null;
            String[] decodeArray = null;
            Response result = new Response();
            StringBuilder output = new StringBuilder();
            RoutingResult routingResult = null;

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                if (!Context.User.Identity.IsAuthenticated)
                {
                    throw new Exception(USER_NOT_AUTHORIZED);
                }

                encode = Context.User.Identity.Name;

                using (EncryptDAO dao = new EncryptDAO())
                {
                    decode = dao.Decrypt(encode);    
                }
                
                decodeArray = decode.Split('#');

                user = decodeArray[0];
                accountId = decodeArray[1];
                password = decodeArray[2];

                using (RoutingDAO dao = new RoutingDAO())
                {
                    routingResult = dao.Void(transactionId, user, password, accountId, reference, ip, Locale, this.UserDb);
                }

                result.Message = routingResult.Message;

                if (!String.IsNullOrEmpty(routingResult.LogId))
                {
                    if (!routingResult.LogId.ToLower().Equals("null"))
                    {
                        transactionId = routingResult.LogId;
                    }
                    else
                    {
                        routingResult.LogId = String.Empty;
                    }
                }

                if (routingResult.Code.Equals("0"))
                {
                    foreach (RoutingProvider item in routingResult.RoutingProvider)
                    {
                        if (item.ProviderId.Equals(PRASAN))
                        {
                            PrasanWcf.VoidRequest req = new PrasanWcf.VoidRequest();
                            PrasanWcf.Response resp = null;

                            using (PrasanWcf.IappClient wcf = new PrasanWcf.IappClient())
                            {
                                req.TransactionId = item.RoutingId;
                                req.Url = item.Url;
                                req.LoginId = item.User;
                                req.TransactionId = item.VoidRoutingId;

                                using (EncryptDAO dao = new EncryptDAO())
                                {
                                    req.ProtectedKey = dao.Decrypt(item.Password);
                                }

                                req.DbUser = this.UserDb;
                                req.RoutingId = item.RoutingId;
                                req.Locale = Locale;

                                resp = wcf.Void(req);

                                result.Code = resp.Code;
                                result.Message = resp.Message;
                            }
                        }
                        else if (item.ProviderId.Equals(OFFICIAL_PREPAID_NATION) || item.ProviderId.Equals(AIPC_PREPAID_NATION))
                        {
                            if (item.ProductTypeId.Equals(RECHARGE))
                            {
                                using (PNWcf.IappClient wcf = new PNWcf.IappClient())
                                {
                                    PNWcf.VoidRequest req = new PNWcf.VoidRequest();
                                    PNWcf.Response resp = null;

                                    using (EncryptDAO dao = new EncryptDAO())
                                    {
                                        req.Password = dao.Decrypt(item.Password);
                                    }

                                    req.OldTransactionId = Convert.ToInt64(item.VoidRoutingId);
                                    req.StoreId = item.TerminalId;
                                    req.TransactionId = item.RoutingId;
                                    req.Url = item.Url;
                                    req.User = item.User;
                                    req.DbUser = this.UserDb;
                                    req.RoutingId = item.RoutingId;
                                    req.Locale = Locale;

                                    resp = wcf.Void(req);

                                    result.Code = resp.Code;
                                    result.Message = resp.Message;
                                }
                            }
                            
                        }
                        else if (item.ProviderId.Equals(SOLOPIN) || item.ProviderId.Equals(SOLOPIN_4G))
                        {
                            using (PLWcf.IappClient wcf = new PLWcf.IappClient())
                            {
                                LoginRequest loginReq = new LoginRequest();
                                PLWcf.LoginResponse loginResponse = null;
                                VoidRequest req = new VoidRequest();
                                PLWcf.VoidResponse voidResponse = null;
                                PLWcf.Request lreq = new Request();

                                loginReq.UserName = item.User;

                                using (EncryptDAO dao = new EncryptDAO())
                                {
                                    loginReq.Password = dao.Decrypt(item.Password);
                                }

                                loginResponse = wcf.Login(loginReq);

                                if (loginResponse.Code.Equals("0"))
                                {
                                    req.DbUser = UserDb;
                                    req.Locale = Locale;
                                    req.Reference = Convert.ToInt64(item.VoidRoutingId);
                                    req.StoreId = Convert.ToInt32(accountId);
                                    req.SessionId = loginResponse.SessionId;
                                    req.RoutingId = item.RoutingId;
                                    
                                    voidResponse = wcf.Void(req);

                                    result.Code = voidResponse.Code;
                                    result.Message = voidResponse.Message;

                                    lreq.SessionId = loginResponse.SessionId;

                                    wcf.Logout(lreq);
                                }

                            }
                        }
                        else
                        {
                            result.Code = Common.Utils.PROVIDER_NOT_FOUND;
                        }

                        using (ResponseCodeDAO dao = new ResponseCodeDAO())
                        {
                            Result response = dao.GetResponseCode(item.ProviderId, result.Code, result.Message, Locale, this.UserDb);

                            result.Code = response.Code;
                            result.Message = response.Message;
                        }

                        if (result.Code.Equals(Common.Utils.APPROVED))
                        {
                            break;
                        }
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(routingResult.LogId))
                    {
                        using (ApiLogDAO apiLogDao = new ApiLogDAO())
                        {
                            Result apiLogResult = apiLogDao.SearchApiLogResponseCode(routingResult.LogId);

                            if (apiLogResult.Code.Equals(Common.Utils.NO_RECORDS_FOUND) || apiLogResult.Code.Equals(Common.Utils.UNKNOWN_EXCEPTION))
                            {
                                result.Code = routingResult.Code;
                                result.Message = routingResult.Message;
                            }
                            else
                            {
                                result.Code = apiLogResult.Code;
                                result.Message = apiLogResult.Message;
                            }
                        }


                    }
                    else
                    {
                        result.Code = Common.Utils.UNCAUGHT_ERROR;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            result.Code = res.ResponseCode[0].Code;
                            result.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            result.Code = UNCAUGHT_ERROR;
                            result.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                Common.Utils.Concat(ref output, null, "VOID:", false, true);
                Common.Utils.Concat(ref output, "TransactionId", transactionId, false, false);
                Common.Utils.Concat(ref output, "Reference", reference, false, false);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "Amount", amount, false, false);
                Common.Utils.Concat(ref output, "Phone", phone, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", result.Code, false, false);
                Common.Utils.Concat(ref output, "Message", result.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return result;
        }


        [WebMethod]
        public Response VoidByReference(String oldReference, String newReference, String amount, String phone)
        {
            String user = String.Empty, details = String.Empty, accountId = String.Empty, ip = null, decode = null, password = null, encode = null;
            String[] decodeArray = null;
            Response result = new Response();
            StringBuilder output = new StringBuilder();
            RoutingResult routingResult = null;

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                if (!Context.User.Identity.IsAuthenticated)
                {
                    throw new Exception(USER_NOT_AUTHORIZED);
                }

                encode = Context.User.Identity.Name;

                using (EncryptDAO dao = new EncryptDAO())
                {
                    decode = dao.Decrypt(encode);    
                }

                decodeArray = decode.Split('#');

                user = decodeArray[0];
                accountId = decodeArray[1];
                password = decodeArray[2];

                using (RoutingDAO dao = new RoutingDAO())
                {
                    routingResult = dao.VoidByReference(oldReference, user, password, accountId, newReference, ip, Locale, this.UserDb);
                }

                result.Message = routingResult.Message;

                if (routingResult.Code.Equals("0"))
                {
                    foreach (RoutingProvider item in routingResult.RoutingProvider)
                    {
                        if (item.ProviderId.Equals(PRASAN))
                        {
                            PrasanWcf.VoidRequest req = new PrasanWcf.VoidRequest();
                            PrasanWcf.Response resp = null;

                            using (PrasanWcf.IappClient wcf = new PrasanWcf.IappClient())
                            {
                                req.TransactionId = item.RoutingId;
                                req.Url = item.Url;
                                req.LoginId = item.User;
                                req.TransactionId = item.VoidRoutingId;

                                using (EncryptDAO dao = new EncryptDAO())
                                {
                                    req.ProtectedKey = dao.Decrypt(item.Password);
                                }

                                req.DbUser = this.UserDb;
                                req.RoutingId = item.RoutingId;
                                req.Locale = Locale;

                                resp = wcf.Void(req);

                                result.Code = resp.Code;
                                result.Message = resp.Message;
                            }
                        }
                        else if (item.ProviderId.Equals(OFFICIAL_PREPAID_NATION) || item.ProviderId.Equals(AIPC_PREPAID_NATION))
                        {
                            if (item.ProductTypeId.Equals(RECHARGE))
                            {
                                using (PNWcf.IappClient wcf = new PNWcf.IappClient())
                                {
                                    PNWcf.VoidRequest req = new PNWcf.VoidRequest();
                                    PNWcf.Response resp = null;

                                    using (EncryptDAO dao = new EncryptDAO())
                                    {
                                        req.Password = dao.Decrypt(item.Password);
                                    }

                                    req.OldTransactionId = Convert.ToInt64(item.VoidRoutingId);
                                    req.StoreId = item.TerminalId;
                                    req.TransactionId = item.RoutingId;
                                    req.Url = item.Url;
                                    req.User = item.User;
                                    req.DbUser = this.UserDb;
                                    req.RoutingId = item.RoutingId;
                                    req.Locale = Locale;

                                    resp = wcf.Void(req);

                                    result.Code = resp.Code;
                                    result.Message = resp.Message;
                                }
                            }

                        }
                        else if (item.ProviderId.Equals(SOLOPIN) || item.ProviderId.Equals(SOLOPIN_4G))
                        {
                            using (PLWcf.IappClient wcf = new PLWcf.IappClient())
                            {
                                LoginRequest loginReq = new LoginRequest();
                                PLWcf.LoginResponse loginResponse = null;
                                VoidRequest req = new VoidRequest();
                                PLWcf.VoidResponse voidResponse = null;
                                PLWcf.Request lreq = new Request();

                                loginReq.UserName = item.User;

                                using (EncryptDAO dao = new EncryptDAO())
                                {
                                    loginReq.Password = dao.Decrypt(item.Password);
                                }

                                loginResponse = wcf.Login(loginReq);

                                if (loginResponse.Code.Equals("0"))
                                {
                                    req.DbUser = UserDb;
                                    req.Locale = Locale;
                                    req.Reference = Convert.ToInt64(item.VoidRoutingId);
                                    req.StoreId = Convert.ToInt32(accountId);
                                    req.SessionId = loginResponse.SessionId;
                                    req.RoutingId = item.RoutingId;

                                    voidResponse = wcf.Void(req);

                                    result.Code = voidResponse.Code;
                                    result.Message = voidResponse.Message;

                                    lreq.SessionId = loginResponse.SessionId;

                                    wcf.Logout(lreq);
                                }

                            }
                        }
                        else
                        {
                            result.Code = Common.Utils.PROVIDER_NOT_FOUND;
                        }

                        using (ResponseCodeDAO dao = new ResponseCodeDAO())
                        {
                            Result response = dao.GetResponseCode(item.ProviderId, result.Code, result.Message, Locale, this.UserDb);

                            result.Code = response.Code;
                            result.Message = response.Message;
                        }

                        if (result.Code.Equals(Common.Utils.APPROVED))
                        {
                            break;
                        }
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(routingResult.LogId))
                    {
                        using (ApiLogDAO apiLogDao = new ApiLogDAO())
                        {
                            Result apiLogResult = apiLogDao.SearchApiLogResponseCode(routingResult.LogId);

                            if (apiLogResult.Code.Equals(Common.Utils.NO_RECORDS_FOUND) || apiLogResult.Code.Equals(Common.Utils.UNKNOWN_EXCEPTION))
                            {
                                result.Code = routingResult.Code;
                                result.Message = routingResult.Message;
                            }
                            else
                            {
                                result.Code = apiLogResult.Code;
                                result.Message = apiLogResult.Message;
                            }
                        }


                    }
                    else
                    {
                        result.Code = Common.Utils.UNCAUGHT_ERROR;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            result.Code = res.ResponseCode[0].Code;
                            result.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            result.Code = UNCAUGHT_ERROR;
                            result.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                Common.Utils.Concat(ref output, null, "VOID_BY_REFERENCE:", false, true);
                Common.Utils.Concat(ref output, "OldReference", oldReference, false, false);
                Common.Utils.Concat(ref output, "NewReference", newReference, false, false);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "Amount", amount, false, false);
                Common.Utils.Concat(ref output, "Phone", phone, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", result.Code, false, false);
                Common.Utils.Concat(ref output, "Message", result.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());

            }

            return result;
        }

        [WebMethod]
        public BalanceResponse GetBalance()
        {
            String user = String.Empty, details = String.Empty, accountId = String.Empty, ip = null, decode = null, encode = null;
            String[] decodeArray = null; 
            BalanceResponse result = new BalanceResponse();
            StringBuilder output = new StringBuilder();
            AccountResult accountResult = new AccountResult();
            AccountDAO accountDao = new AccountDAO();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                if (!Context.User.Identity.IsAuthenticated)
                {
                    throw new Exception(USER_NOT_AUTHORIZED);
                }

                encode = Context.User.Identity.Name;

                using (EncryptDAO dao = new EncryptDAO())
                {
                    decode = dao.Decrypt(encode);
                }

                decodeArray = decode.Split('#');

                user = decodeArray[0];
                accountId = decodeArray[1];

                accountResult = accountDao.SearchAccount(accountId, null, null, null, null, null, null, null, null, null, null, null, false, null);

                result.Code = accountResult.Code;
                result.Message = accountResult.Message;
                details = accountResult.Details;

                if (result.Code.Equals("0"))
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;
                    result.Balance = accountResult.Account[0].Balance.ToString();
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            result.Code = res.ResponseCode[0].Code;
                            result.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            result.Code = UNCAUGHT_ERROR;
                            result.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                Common.Utils.Concat(ref output, null, "GET_BALANCE:", false, true);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "Balance", result.Balance, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", result.Code, false, false);
                Common.Utils.Concat(ref output, "Message", result.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return result;
        }


        [WebMethod]
        public AccountTransactionResponse QueryTransactionsByPhone(String phone)
        {
            String user = String.Empty, ip = null, accountId = null;
            String details = null, decode = null, encode = null;
            String[] decodeArray = null;
            StringBuilder output = new StringBuilder();
            AccountTransactionResponse response = new AccountTransactionResponse();
            TransactionLogResult transactionLogResult = new TransactionLogResult();
            List<AccountTransactions> list = new List<AccountTransactions>();
            TransactionLogDAO transactionDao = new TransactionLogDAO();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                if (!Context.User.Identity.IsAuthenticated)
                {
                    throw new Exception(USER_NOT_AUTHORIZED);
                }

                if (String.IsNullOrEmpty(phone))
                {
                    throw new Exception(PHONE_REQUIRE);
                }

                encode = Context.User.Identity.Name;

                using (EncryptDAO dao = new EncryptDAO())
                {
                    decode = dao.Decrypt(encode);
                }

                decodeArray = decode.Split('#');

                user = decodeArray[0];
                accountId = decodeArray[1];


                transactionLogResult = transactionDao.Search(null, null, accountId, null, null, null, null, null,
                    null, null, null, null, null, null, null, phone, null, null, 0, 50);


                response.Code = transactionLogResult.Code;
                response.Message = transactionLogResult.Message;
                details = transactionLogResult.Details;

                if (transactionLogResult.Code.Equals("0"))
                {
                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;

                    foreach (TransactionLog item in transactionLogResult.TransactionLog)
                    {
                        list.Add(new AccountTransactions(item.LogId, item.Reference, item.Merchant,
                            item.ProductTypeId, item.ProductId, item.TransactionAmount, item.Phone,
                            item.ResponseCodeId, item.DateCreated, item.ProviderReference,
                            item.TransactionTypeId, item.TransactionTypeName));
                    }

                    response.AccountTransactions = list.ToArray();
                }

            }
            catch (Exception ex)
            {
                response.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(response.Message) && !String.IsNullOrEmpty(response.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(response.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            response.Code = res.ResponseCode[0].Code;
                            response.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            response.Code = UNCAUGHT_ERROR;
                            response.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                Common.Utils.Concat(ref output, null, "QUERY_TRANS_BY_PHONE:", false, true);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "Phone", phone, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }


        [WebMethod]
        public AccountTransactionResponse QueryTransactionsById(String transactionId)
        {
            String user = String.Empty, ip = null, accountId = null;
            String details = null, decode = null, encode = null;
            String[] decodeArray = null;
            StringBuilder output = new StringBuilder();
            AccountTransactionResponse response = new AccountTransactionResponse();
            TransactionLogResult transactionLogResult = new TransactionLogResult();
            List<AccountTransactions> list = new List<AccountTransactions>();
            TransactionLogDAO transactionDao = new TransactionLogDAO();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                if (!Context.User.Identity.IsAuthenticated)
                {
                    throw new Exception(USER_NOT_AUTHORIZED);
                }

                if (String.IsNullOrEmpty(transactionId))
                {
                    throw new Exception(TRANSACTION_ID_REQUIRE);
                }

                encode = Context.User.Identity.Name;

                using (EncryptDAO dao = new EncryptDAO())
                {
                    decode = dao.Decrypt(encode);    
                }


                decodeArray = decode.Split('#');

                user = decodeArray[0];
                accountId = decodeArray[1];


                transactionLogResult = transactionDao.Search(null, transactionId, accountId, null, null, null, null, null,
                    null, null, null, null, null, null, null, null, null, null, 0, 50);


                response.Code = transactionLogResult.Code;
                response.Message = transactionLogResult.Message;
                details = transactionLogResult.Details;

                if (transactionLogResult.Code.Equals("0"))
                {
                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;

                    foreach (TransactionLog item in transactionLogResult.TransactionLog)
                    {
                        list.Add(new AccountTransactions(item.LogId, item.Reference, item.Merchant,
                            item.ProductTypeId, item.ProductId, item.TransactionAmount, item.Phone,
                            item.ResponseCodeId, item.DateCreated, item.ProviderReference,
                            item.TransactionTypeId, item.TransactionTypeName));
                    }

                    response.AccountTransactions = list.ToArray();
                }

            }
            catch (Exception ex)
            {
                response.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(response.Message) && !String.IsNullOrEmpty(response.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(response.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            response.Code = res.ResponseCode[0].Code;
                            response.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            response.Code = UNCAUGHT_ERROR;
                            response.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                Common.Utils.Concat(ref output, null, "QUERY_TRANS_BY_ID:", false, true);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "TransactionId", transactionId, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }


        [WebMethod]
        public AccountTransactionResponse QueryTransactionsByReference(String reference)
        {
            String user = String.Empty, ip = null, accountId = null;
            String details = null, decode = null, encode = null;
            String[] decodeArray = null;
            StringBuilder output = new StringBuilder();
            AccountTransactionResponse response = new AccountTransactionResponse();
            TransactionLogWsResult transactionLogWsResult = new TransactionLogWsResult();
            List<AccountTransactions> list = new List<AccountTransactions>();
            TransactionLogDAO transactionDao = new TransactionLogDAO();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                if (!Context.User.Identity.IsAuthenticated)
                {
                    throw new Exception(USER_NOT_AUTHORIZED);
                }

                if (String.IsNullOrEmpty(reference))
                {
                    throw new Exception(REFERENCE_REQUIRE);
                }

                encode = Context.User.Identity.Name;

                using (EncryptDAO dao = new EncryptDAO())
                {
                    decode = dao.Decrypt(encode);    
                }

                decodeArray = decode.Split('#');

                user = decodeArray[0];
                accountId = decodeArray[1];


                transactionLogWsResult = transactionDao.SearchByReference(reference, accountId);


                response.Code = transactionLogWsResult.Code;
                response.Message = transactionLogWsResult.Message;

                if (transactionLogWsResult.Code.Equals("0"))
                {
                    response.Code = Common.Utils.APPROVED;
                    response.Message = Common.Utils.APPROVED_MESSAGE;

                    foreach (TransactionLogWs item in transactionLogWsResult.TransactionLogWs)
                    {
                        list.Add(new AccountTransactions(item.TransactionId, reference, item.Merchant,
                            item.ProductTypeId, item.ProductId, item.Amount, item.Phone,
                            item.ResponseCode, item.DateCreated, item.ProviderReference,
                            item.TransactionTypeId, item.TransactionTypeName));
                    }

                    response.AccountTransactions = list.ToArray();
                }

            }
            catch (Exception ex)
            {
                response.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(response.Message) && !String.IsNullOrEmpty(response.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(response.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            response.Code = res.ResponseCode[0].Code;
                            response.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            response.Code = UNCAUGHT_ERROR;
                            response.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                Common.Utils.Concat(ref output, null, "QUERY_TRANS_BY_REFENCE:", false, true);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "Reference", reference, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }


        [WebMethod]
        public AccountsProductResult GetProducts()
        {
            String user = String.Empty, details = String.Empty, accountId = String.Empty, decode = null, encode = null,
                ip = String.Empty;
            String[] decodeArray = null;
            AccountsProductResult accountProductResult = new AccountsProductResult();
            StringBuilder output = new StringBuilder();
            AccountDAO accountDao = new AccountDAO();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                if (!Context.User.Identity.IsAuthenticated)
                {
                    throw new Exception(USER_NOT_AUTHORIZED);
                }

                encode = Context.User.Identity.Name;

                using (EncryptDAO dao = new EncryptDAO())
                {
                    decode = dao.Decrypt(encode);    
                }
                
                decodeArray = decode.Split('#');

                user = decodeArray[0];
                accountId = decodeArray[1];

                accountProductResult = accountDao.SearchProducts(accountId, null, null, null, null, null,
                    null, null, null, "1", "1");

                details = accountProductResult.Details;

                if (accountProductResult.Code.Equals("0"))
                {
                    accountProductResult.Code = Common.Utils.APPROVED;
                    accountProductResult.Message = Common.Utils.APPROVED_MESSAGE;
                } 
            }
            catch (Exception ex)
            {
                accountProductResult.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(accountProductResult.Message) && !String.IsNullOrEmpty(accountProductResult.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(accountProductResult.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            accountProductResult.Code = res.ResponseCode[0].Code;
                            accountProductResult.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            accountProductResult.Code = UNCAUGHT_ERROR;
                            accountProductResult.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                Common.Utils.Concat(ref output, null, "GET_PRODUCTS:", false, true);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", accountProductResult.Code, false, false);
                Common.Utils.Concat(ref output, "Message", accountProductResult.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return accountProductResult;
        }


        [WebMethod]
        public AccCountryResult GetCountries()
        {
            AccCountryResult result = new AccCountryResult();
            String details = String.Empty, ip = String.Empty;
            StringBuilder output = new StringBuilder();
            String user = String.Empty, accountId = null, decode = null, encode = null;
            String[] decodeArray = null;
            AccountDAO accountDao = new AccountDAO();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                if (!Context.User.Identity.IsAuthenticated)
                {
                    throw new Exception(USER_NOT_AUTHORIZED);
                }


                encode = Context.User.Identity.Name;

                using (EncryptDAO dao = new EncryptDAO())
                {
                    decode = dao.Decrypt(encode);    
                }

                decodeArray = decode.Split('#');

                user = decodeArray[0];
                accountId = decodeArray[1];

                result = accountDao.SearchCountry(accountId);

                if (result.Code.Equals("0"))
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            result.Code = res.ResponseCode[0].Code;
                            result.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            result.Code = UNCAUGHT_ERROR;
                            result.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                Common.Utils.Concat(ref output, null, "GET_COUNTRIES:", false, true);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", result.Code, false, false);
                Common.Utils.Concat(ref output, "Message", result.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return result;
        }


        [WebMethod]
        public ResponseCodeResult GetResponseCodes()
        {
            ResponseCodeResult result = new ResponseCodeResult();
            String details = String.Empty, ip = String.Empty;
            StringBuilder output = new StringBuilder();
            String user = String.Empty, accountId = null, decode = null, encode = null;
            String[] decodeArray = null;

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                if (!Context.User.Identity.IsAuthenticated)
                {
                    throw new Exception(USER_NOT_AUTHORIZED);
                }

                encode = Context.User.Identity.Name;

                using (EncryptDAO dao = new EncryptDAO())
                {
                    decode = dao.Decrypt(encode);
                }

                decodeArray = decode.Split('#');

                user = decodeArray[0];
                accountId = decodeArray[1];

                using (ResponseCodeDAO dao = new ResponseCodeDAO())
                {
                    result = dao.Search(null, null, null);    
                }
                

                if (result.Code.Equals("0"))
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            result.Code = res.ResponseCode[0].Code;
                            result.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            result.Code = UNCAUGHT_ERROR;
                            result.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                Common.Utils.Concat(ref output, null, "GET_RESPONSE_CODE:", false, true);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", result.Code, false, false);
                Common.Utils.Concat(ref output, "Message", result.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return result;
        }


        [WebMethod]
        public ProductTypeResponse GetProductTypes()
        {
            ProductTypeResponse result = new ProductTypeResponse();
            ProductTypeResult typeResult = null;
            List<ProductTypes> list = new List<ProductTypes>();
            String details = String.Empty, user = String.Empty, decode = null, encode = null, accountId = null, ip = String.Empty;
            StringBuilder output = new StringBuilder();
            String[] decodeArray = null;
            ProductTypeDAO productTypeDao = new ProductTypeDAO();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                if (!Context.User.Identity.IsAuthenticated)
                {
                    throw new Exception(USER_NOT_AUTHORIZED);
                }

                encode = Context.User.Identity.Name;

                using (EncryptDAO dao = new EncryptDAO())
                {
                    decode = dao.Decrypt(encode);    
                }

                decodeArray = decode.Split('#');

                user = decodeArray[0];
                accountId = decodeArray[1];

                typeResult = productTypeDao.Search(null, "1", null);

                result.Code = typeResult.Code;
                result.Message = typeResult.Message;

                if (typeResult.Code.Equals("0") && typeResult.ProductType != null)
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;

                    foreach (ProductType item in typeResult.ProductType)
                    {
                        list.Add(new ProductTypes(item.Id.ToString(), item.Description));
                    }

                    result.ProductTypes = list.ToArray();
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            result.Code = res.ResponseCode[0].Code;
                            result.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            result.Code = UNCAUGHT_ERROR;
                            result.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                Common.Utils.Concat(ref output, null, "GET_PRODUCT_TYPES:", false, true);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", result.Code, false, false);
                Common.Utils.Concat(ref output, "Message", result.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return result;
        }


        [WebMethod]
        public CurrenciesResponse GetCurrencies()
        {
            CurrenciesResponse result = new CurrenciesResponse();
            CurrencyResult currencyResult = null;
            List<Currencies> list = new List<Currencies>();
            String details = String.Empty, user = String.Empty, decode = null, encode = null, ip = String.Empty,
                accountId = String.Empty;
            String[] decodeArray = null;
            StringBuilder output = new StringBuilder();
            CurrencyDAO currencyDao = new CurrencyDAO();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                if (!Context.User.Identity.IsAuthenticated)
                {
                    throw new Exception(USER_NOT_AUTHORIZED);
                }

                encode = Context.User.Identity.Name;

                using (EncryptDAO dao = new EncryptDAO())
                {
                    decode = dao.Decrypt(encode);
                }

                decodeArray = decode.Split('#');

                user = decodeArray[0];
                
                currencyResult = currencyDao.Search(null, "1", null);

                result.Code = currencyResult.Code;
                result.Message = currencyResult.Message;

                if (currencyResult.Code.Equals("0") && currencyResult.Currency != null)
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;

                    foreach (Currency item in currencyResult.Currency)
                    {
                        list.Add(new Currencies(item.Id.ToString(), item.Description, item.Abbreviation));
                    }

                    result.Currencies = list.ToArray();
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            result.Code = res.ResponseCode[0].Code;
                            result.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            result.Code = UNCAUGHT_ERROR;
                            result.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                Common.Utils.Concat(ref output, null, "GET_CURRENCIES:", false, true);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", result.Code, false, false);
                Common.Utils.Concat(ref output, "Message", result.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return result;
        }


        [WebMethod]
        public CarriersResponse GetCarriers()
        {
            CarriersResponse result = new CarriersResponse();
            CarrierResult carrierResult = null;
            List<Carriers> list = new List<Carriers>();
            String details = String.Empty, user = String.Empty, accountId = null, encode = null, decode = null, 
                ip = String.Empty;
            StringBuilder output = new StringBuilder();
            String[] decodeArray = null;
            AccountDAO accountDao = new AccountDAO();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                if (!Context.User.Identity.IsAuthenticated)
                {
                    throw new Exception(USER_NOT_AUTHORIZED);
                }

                encode = Context.User.Identity.Name;

                using (EncryptDAO dao = new EncryptDAO())
                {
                    decode = dao.Decrypt(encode);
                }

                decodeArray = decode.Split('#');

                user = decodeArray[0];
                accountId = decodeArray[1];

                carrierResult = accountDao.SearchCarrier(accountId);

                result.Code = carrierResult.Code;
                result.Message = carrierResult.Message;

                if (carrierResult.Code.Equals("0") && carrierResult.Carrier != null)
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;

                    foreach (Carrier item in carrierResult.Carrier)
                    {
                        list.Add(new Carriers(item.Id, item.Description, item.Abbreviation));
                    }

                    result.Carriers = list.ToArray();
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            result.Code = res.ResponseCode[0].Code;
                            result.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            result.Code = UNCAUGHT_ERROR;
                            result.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                Common.Utils.Concat(ref output, null, "GET_CARRIERS:", false, true);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", result.Code, false, false);
                Common.Utils.Concat(ref output, "Message", result.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return result;
        }

        
        [WebMethod]
        public CitiesResponse GetCities(String countryId)
        {
            CitiesResponse result = new CitiesResponse();
            CityResult cityResult = null;
            List<Cities> list = new List<Cities>();
            String details = String.Empty, user = String.Empty, accountId = null, encode = null, decode = null, ip = null;
            StringBuilder output = new StringBuilder();
            String[] decodeArray = null;
            AccountDAO accountDao = new AccountDAO();

            try
            {
                ip = HttpContext.Current.Request.UserHostAddress;

                if (!Context.User.Identity.IsAuthenticated)
                {
                    throw new Exception(USER_NOT_AUTHORIZED);
                }

                if (String.IsNullOrEmpty(countryId))
                    throw new Exception(COUNTRY_ID_REQUIRE);

                encode = Context.User.Identity.Name;

                using (EncryptDAO dao = new EncryptDAO())
                {
                    decode = dao.Decrypt(encode);    
                }
                
                decodeArray = decode.Split('#');

                user = decodeArray[0];
                accountId = decodeArray[1];

                cityResult = accountDao.SearchCity(accountId, countryId);

                result.Code = cityResult.Code;
                result.Message = cityResult.Message;

                if (cityResult.Code.Equals("0") && cityResult.City != null)
                {
                    result.Code = Common.Utils.APPROVED;
                    result.Message = Common.Utils.APPROVED_MESSAGE;

                    foreach (DataAccessLayer.EntityBase.City item in cityResult.City)
                    {
                        list.Add(new Cities(item.Id.ToString(), item.Description, item.Abbreviation));
                    }

                    result.Cities = list.ToArray();
                }
            }
            catch (Exception ex)
            {
                result.Code = ex.Message;
                details = ex.Message;
            }
            finally
            {
                if (String.IsNullOrEmpty(result.Message) && !String.IsNullOrEmpty(result.Code))
                {
                    using (ResponseCodeDAO responseCodeDao = new ResponseCodeDAO())
                    {
                        ResponseCodeResult res = responseCodeDao.Search(result.Code, null, null);

                        if (res.Code.Equals("0"))
                        {
                            result.Code = res.ResponseCode[0].Code;
                            result.Message = res.ResponseCode[0].Message;
                        }
                        else
                        {
                            result.Code = UNCAUGHT_ERROR;
                            result.Message = UNCAUGHT_ERROR_MESSAGE;
                        }
                    }
                }

                Common.Utils.Concat(ref output, null, "GET_CITIES:", false, true);
                Common.Utils.Concat(ref output, "AccountId", accountId, false, false);
                Common.Utils.Concat(ref output, "CountryId", countryId, false, false);
                Common.Utils.Concat(ref output, "User", user, false, false);
                Common.Utils.Concat(ref output, "Ip", ip, false, false);
                Common.Utils.Concat(ref output, "Code", result.Code, false, false);
                Common.Utils.Concat(ref output, "Message", result.Message, false, false);
                Common.Utils.Concat(ref output, "Details", details, false, true);

                log.Debug(output.ToString());
            }

            return result;
        }


        [WebMethod]
        public Common.Result Test(int seconds)
        {
            StringBuilder output = new StringBuilder();
            Common.Result result = new Common.Result();
            DateTime? inicio = null, fin = null;
            String estadoCliente = "CONNECTED";

            try
            {
                inicio = DateTime.Now;
                System.Threading.Thread.Sleep(seconds * 1000);
                fin = DateTime.Now;
                result.Code = Common.Utils.APPROVED;
                result.Message = Common.Utils.APPROVED_MESSAGE;

                if (!Context.Response.IsClientConnected)
                {
                    estadoCliente = "DISCONNECTED";
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNKNOWN_EXCEPTION;
                result.Message = ex.Message;
            }
            finally
            {
                output.AppendLine(String.Empty);
                output.AppendLine("***********************************************");
                output.AppendLine("Utp Web Service - Test");
                output.Append("Segundos             : ").AppendLine(seconds.ToString());

                if (inicio != null & inicio.HasValue)
                {
                    output.Append("Inicio               : ").AppendLine(inicio.Value.ToString("T"));
                }

                if (fin != null & fin.HasValue)
                {
                    output.Append("Fin                  : ").AppendLine(fin.Value.ToString("T"));
                }

                output.Append("Estado Cliente       : ").AppendLine(estadoCliente);
                output.Append("Code                 : ").AppendLine(result.Code);
                output.Append("Message              : ").AppendLine(result.Message);
                output.Append("Details              : ").AppendLine(result.Details);
                output.AppendLine("***********************************************");

                log.Debug(output.ToString());
            }

            return result;
        }
    }
}
