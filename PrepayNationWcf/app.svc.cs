﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using PrepayNationWcf.PPNWs;
using log4net;

namespace PrepayNationWcf
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    public class app : Iapp
    {
        public const String VERSION = "1.0";
        private static readonly ILog log = LogManager.GetLogger("Log");
        private static readonly ILog logSummary = LogManager.GetLogger("LogSummary");

        public RechargeResponse Recharge(RechargeRequest Request)
        {
            RechargeResponse rechargeResponse = new RechargeResponse();
            DateTime? begin = DateTime.Now, end = null;
            StringBuilder output = new StringBuilder();

            try
            {
                using (PPNWs.ServiceManager ws = new PPNWs.ServiceManager())
                {
                    OrderResponse orderResponse = null;
                    AuthenticationHeader header = new AuthenticationHeader();

                    header.userId = Request.User;
                    header.password = Request.Password;

                    ws.AuthenticationHeaderValue = header;

                    if (!String.IsNullOrEmpty(Request.Url))
                    {
                        ws.Url = Request.Url;
                    }

                    if (String.IsNullOrEmpty(Request.ProductId))
                    {
                        rechargeResponse.Code = Common.Utils.PRODUCT_CODE_REQUIRE_CODE;
                        rechargeResponse.Message = Common.Utils.PRODUCT_CODE_REQUIRE_MESSAGE;

                        throw new Exception(Common.Utils.PRODUCT_CODE_REQUIRE_MESSAGE);
                    }

                    orderResponse = ws.PurchaseRtr2(VERSION, 
                        Convert.ToInt32(Request.ProductId), 
                        Request.Amount, Request.Phone, 
                        Request.TransactionId, null, Request.StoreId);

                    if (orderResponse.responseCode.Equals("000"))
                    {
                        rechargeResponse.Code = Common.Utils.APPROVED;
                        rechargeResponse.Message = Common.Utils.APPROVED_MESSAGE;

                        rechargeResponse.AuthorizationNumber = orderResponse.invoice.invoiceNumber.ToString();
                        rechargeResponse.TransactionDateTime = orderResponse.invoice.transactionDateTime.ToShortDateString() + " " + orderResponse.invoice.transactionDateTime.ToShortTimeString();
                    }
                    else
                    {
                        rechargeResponse.Code = orderResponse.responseCode;
                        rechargeResponse.Message = orderResponse.responseMessage;
                    }
                }
            }
            catch (Exception ex)
            {
                if (String.IsNullOrEmpty(rechargeResponse.Code) || String.IsNullOrEmpty(rechargeResponse.Message))
                {
                    rechargeResponse.Code = Common.Utils.UNCAUGHT_ERROR;
                    rechargeResponse.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                    rechargeResponse.Details = ex.Message;
                }
            }
            finally
            {
                end = DateTime.Now;

                if (!String.IsNullOrEmpty(Request.RoutingId))
                {
                    using (TransLogWs.Service ws = new TransLogWs.Service())
                    {
                        TransLogWs.LogRequest requ = new TransLogWs.LogRequest();

                        requ.AuthorizationNumber = rechargeResponse.AuthorizationNumber;
                        requ.BeginDate = begin.Value;
                        requ.EndDate = end.Value;
                        requ.Code = rechargeResponse.Code;
                        requ.Message = rechargeResponse.Message;
                        requ.DbUser = Request.DbUser;
                        requ.Locale = Request.Locale;
                        requ.RoutingId = Request.RoutingId;

                        ws.LogAsync(requ);
                    }
                }

                Common.Utils.Concat(ref output, null, "RECHARGE:", false, true);
                Common.Utils.Concat(ref output, "Code", rechargeResponse.Code, false, false);
                Common.Utils.Concat(ref output, "Message", rechargeResponse.Message, false, false);
                Common.Utils.Concat(ref output, "AuthNumber", rechargeResponse.AuthorizationNumber, false, false);
                Common.Utils.Concat(ref output, "Phone", Request.Phone, false, false);
                Common.Utils.Concat(ref output, "Amount", Request.Amount, false, false);
                Common.Utils.Concat(ref output, "ProductId", Request.ProductId, false, false);
                Common.Utils.Concat(ref output, "TransactionId", Request.TransactionId, false, false);
                Common.Utils.Concat(ref output, "RoutingId", Request.RoutingId, false, true);

                logSummary.Debug(output.ToString());

                Common.Utils.Concat(ref output, ",StoreId", Request.StoreId, false, false);
                Common.Utils.Concat(ref output, "Url", Request.Url, false, true);

                log.Debug(output.ToString());
            }

            return rechargeResponse;
        }

        public PurchasePinResponse PurchasePin(PurchasePinRequest Request)
        {
            PurchasePinResponse pinResponse = new PurchasePinResponse();
            DateTime? begin = DateTime.Now, end = null;
            StringBuilder output = new StringBuilder();

            try
            {
                using (ServiceManager ws = new ServiceManager())
                {
                    OrderResponse orderResponse = null;
                    AuthenticationHeader header = new AuthenticationHeader();

                    header.userId = Request.User;
                    header.password = Request.Password;

                    ws.AuthenticationHeaderValue = header;

                    if (!String.IsNullOrEmpty(Request.Url))
                    {
                        ws.Url = Request.Url;
                    }

                    if (String.IsNullOrEmpty(Request.ProductId))
                    {
                        pinResponse.Code = Common.Utils.PRODUCT_CODE_REQUIRE_CODE;
                        pinResponse.Message = Common.Utils.PRODUCT_CODE_REQUIRE_MESSAGE;

                        throw new Exception(Common.Utils.PRODUCT_CODE_REQUIRE_MESSAGE);
                    }

                    orderResponse = ws.PurchasePin(VERSION, Convert.ToInt32(Request.ProductId), 1, Request.TransactionId, Request.StoreId);

                    if (orderResponse.responseCode.Equals("000"))
                    {
                        pinResponse.Code = Common.Utils.APPROVED;
                        pinResponse.Message = Common.Utils.APPROVED_MESSAGE;

                        pinResponse.PinNumber = orderResponse.invoice.cards[0].pins[0].pinNumber;
                        pinResponse.SerialNumber = orderResponse.invoice.cards[0].pins[0].controlNumber;
                    }
                    else
                    {
                        pinResponse.Code = orderResponse.responseCode;
                        pinResponse.Message = orderResponse.responseMessage;
                    }
                }
            }
            catch (Exception ex)
            {
                if (String.IsNullOrEmpty(pinResponse.Code) || String.IsNullOrEmpty(pinResponse.Message))
                {
                    pinResponse.Code = Common.Utils.UNCAUGHT_ERROR;
                    pinResponse.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                    pinResponse.Details = ex.Message;
                }
            }
            finally
            {
                end = DateTime.Now;

                if (!String.IsNullOrEmpty(Request.RoutingId))
                {
                    using (TransLogWs.Service ws = new TransLogWs.Service())
                    {
                        TransLogWs.LogRequest requ = new TransLogWs.LogRequest();

                        requ.BeginDate = begin.Value;
                        requ.EndDate = end.Value;
                        requ.Code = pinResponse.Code;
                        requ.Message = pinResponse.Message;
                        requ.PinNumber = pinResponse.PinNumber;
                        requ.SerialNumber = pinResponse.SerialNumber;
                        requ.DbUser = Request.DbUser;
                        requ.Locale = Request.Locale;
                        requ.RoutingId = Request.RoutingId;

                        ws.LogAsync(requ);
                    }
                }

                Common.Utils.Concat(ref output, null, "PURCHASE_PIN:", false, true);
                Common.Utils.Concat(ref output, "Code", pinResponse.Code, false, false);
                Common.Utils.Concat(ref output, "Message", pinResponse.Message, false, false);
                Common.Utils.Concat(ref output, "PinNumber", pinResponse.PinNumber, false, false);
                Common.Utils.Concat(ref output, "SerialNumber", pinResponse.SerialNumber, false, false);
                Common.Utils.Concat(ref output, "ProductId", Request.ProductId, false, false);
                Common.Utils.Concat(ref output, "TransactionId", Request.TransactionId, false, false);
                Common.Utils.Concat(ref output, "RoutingId", Request.RoutingId, false, true);

                logSummary.Debug(output.ToString());

                Common.Utils.Concat(ref output, ",StoreId", Request.StoreId, false, false);
                Common.Utils.Concat(ref output, "Url", Request.Url, false, true);

                log.Debug(output.ToString());
            }

            return pinResponse;
        }

        public Response Void(VoidRequest Request)
        {
            Response response = new Response();
            DateTime? begin = DateTime.Now, end = null;
            StringBuilder output = new StringBuilder();

            try
            {
                using (PPNWs.ServiceManager ws = new PPNWs.ServiceManager())
                {
                    OrderResponse orderResponse = null;
                    AuthenticationHeader header = new AuthenticationHeader();

                    header.userId = Request.User;
                    header.password = Request.Password;

                    ws.AuthenticationHeaderValue = header;

                    if (!String.IsNullOrEmpty(Request.Url))
                    {
                        ws.Url = Request.Url;
                    }

                    orderResponse = ws.RtrReturn(VERSION, Request.OldTransactionId, Request.TransactionId, Request.StoreId);

                    if (orderResponse.responseCode.Equals("000"))
                    {
                        response.Code = Common.Utils.APPROVED;
                        response.Message = Common.Utils.APPROVED_MESSAGE;
                    }
                    else
                    {
                        response.Code = orderResponse.responseCode;
                        response.Message = orderResponse.responseMessage;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                response.Details = ex.Message;
            }
            finally
            {
                end = DateTime.Now;

                if (!String.IsNullOrEmpty(Request.RoutingId))
                {
                    using (TransLogWs.Service ws = new TransLogWs.Service())
                    {
                        TransLogWs.LogRequest requ = new TransLogWs.LogRequest();

                        requ.BeginDate = begin.Value;
                        requ.EndDate = end.Value;
                        requ.Code = response.Code;
                        requ.Message = response.Message;
                        requ.DbUser = Request.DbUser;
                        requ.Locale = Request.Locale;
                        requ.RoutingId = Request.RoutingId;

                        ws.LogAsync(requ);
                    }
                }

                Common.Utils.Concat(ref output, null, "VOID:", false, true);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "OldTransactionId", Request.OldTransactionId, false, false);
                Common.Utils.Concat(ref output, "TransactionId", Request.TransactionId, false, false);
                Common.Utils.Concat(ref output, "RoutingId", Request.RoutingId, false, true);

                logSummary.Debug(output.ToString());

                Common.Utils.Concat(ref output, ",StoreId", Request.StoreId, false, false);
                Common.Utils.Concat(ref output, "Url", Request.Url, false, true);

                log.Debug(output.ToString());

            }

            return response;
        }
    }
}
