﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace PrepayNationWcf
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface Iapp
    {

        [OperationContract]
        RechargeResponse Recharge(RechargeRequest Request);

        [OperationContract]
        PurchasePinResponse PurchasePin(PurchasePinRequest Request);

        [OperationContract]
        Response Void(VoidRequest Request);
    }

    [DataContract]
    public class VoidRequest
    {
        [DataMember]
        public long OldTransactionId { get; set; }

        [DataMember]
        public String TransactionId { get; set; }

        [DataMember]
        public String StoreId { get; set; }

        [DataMember]
        public String User { get; set; }

        [DataMember]
        public String Password { get; set; }

        [DataMember]
        public String Url { get; set; }

        [DataMember]
        public String RoutingId { get; set; }

        [DataMember]
        public String Locale { get; set; }

        [DataMember]
        public String DbUser { get; set; }
    }

    [DataContract]
    public class Response
    {
        [DataMember]
        public String Code { get; set; }

        [DataMember]
        public String Message { get; set; }

        [DataMember]
        public String Details { get; set; }

        [DataMember]
        public String TransactionDateTime { get; set; }
    }

    [DataContract]
    public class PurchasePinResponse : Response
    {
        [DataMember]
        public String PinNumber { get; set; }

        [DataMember]
        public String SerialNumber { get; set; }
    }

    [DataContract]
    public class PurchasePinRequest
    { 
        [DataMember]
        public String ProductId;

        [DataMember]
        public String TransactionId;

        [DataMember]
        public String StoreId;

        [DataMember]
        public String Url;

        [DataMember]
        public String User;

        [DataMember]
        public String Password;

        [DataMember]
        public String RoutingId { get; set; }

        [DataMember]
        public String Locale { get; set; }

        [DataMember]
        public String DbUser { get; set; }
    }

    [DataContract]
    public class RechargeResponse : Response
    {
        [DataMember]
        public String AuthorizationNumber { get; set; }
    }

    [DataContract]
    public class RechargeRequest
    {
        [DataMember]
        public String ProductId { get; set; }

        [DataMember]
        public String TransactionId { get; set; }

        [DataMember]
        public String StoreId { get; set; }

        [DataMember]
        public String Phone { get; set; }

        [DataMember]
        public decimal Amount { get; set; }

        [DataMember]
        public String Url { get; set; }

        [DataMember]
        public String User { get; set; }

        [DataMember]
        public String Password { get; set; }

        [DataMember]
        public String RoutingId { get; set; }

        [DataMember]
        public String Locale { get; set; }

        [DataMember]
        public String DbUser { get; set; }
    }
}
