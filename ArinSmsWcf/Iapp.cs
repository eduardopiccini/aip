﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace ArinSmsWcf
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface Iapp
    {

        [OperationContract]
        Response SendSms(SmsRequest Request);

        // TODO: Add your service operations here
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class SmsRequest
    {
        [DataMember]
        public String Phone { get; set; }

        [DataMember]
        public String Message { get; set; }

        [DataMember]
        public String Url { get; set; }

        [DataMember]
        public String ServerPassword { get; set; }

        [DataMember]
        public String RoutingId { get; set; }

        [DataMember]
        public String Locale { get; set; }

        [DataMember]
        public String DbUser { get; set; }
    }

    [DataContract]
    public class Response
    {
        [DataMember]
        public String Code { get; set; }

        [DataMember]
        public String Message { get; set; }

        [DataMember]
        public String AuthorizationNumber { get; set; }

        [DataMember]
        public String TransactionDateTime { get; set; }
    }
}
