﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using log4net;
using System.Net;
using System.IO;

namespace ArinSmsWcf
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    public class app : Iapp
    {
        private static readonly ILog log = LogManager.GetLogger("Log");

        public Response SendSms(SmsRequest Request)
        {
            Response response = new Response();
            DateTime? begin = DateTime.Now, end = null;
            StringBuilder output = new StringBuilder();
            StringBuilder url = new StringBuilder();
            Uri uri;
            HttpWebRequest req = null;
            HttpWebResponse resp = null;
            StreamReader reader = null;
            String msg = String.Empty;

            try
            {
                url.Append(Request.Url);
                url.Append("?server_password=");
                url.Append(Request.ServerPassword);
                url.Append("&msg=");
                url.Append(Request.Message);
                url.Append("&number=");
                url.Append(Request.Phone);

                uri = new Uri(url.ToString());
                req = (HttpWebRequest)WebRequest.Create(uri);

                req.ContentType = "text/xml";
                req.Method = "GET";

                resp = (HttpWebResponse)req.GetResponse();

                reader = new StreamReader(resp.GetResponseStream());

                msg = reader.ReadToEnd();

                if (!String.IsNullOrEmpty(msg))
                {
                    if (msg.Contains("PROCEEDING"))
                    {
                        response.Code = Common.Utils.APPROVED;
                        response.Message = Common.Utils.APPROVED_MESSAGE;
                        response.AuthorizationNumber = Request.RoutingId;
                    }
                    else
                    {
                        response.Code = Common.Utils.SMS_NOT_APPROVED_CODE;
                        response.Message = Common.Utils.SMS_NOT_APPROVED_MESSAGE + " (" + msg + ")";
                    }
                }
                else
                {
                    response.Code = Common.Utils.SMS_RESPONSE_EMPTY_CODE;
                    response.Message = Common.Utils.SMS_RESPONSE_EMPTY_MESSAGE;
                }
                
            }
            catch (Exception)
            {
                response.Code = Common.Utils.UNCAUGHT_ERROR;
                response.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
            }
            finally
            {
                end = DateTime.Now;

                if (!String.IsNullOrEmpty(Request.RoutingId))
                {
                    using (TransLogWs.Service ws = new TransLogWs.Service())
                    {
                        TransLogWs.LogRequest requ = new TransLogWs.LogRequest();

                        requ.AuthorizationNumber = response.AuthorizationNumber;
                        requ.BeginDate = begin.Value;
                        requ.EndDate = end.Value;
                        requ.Code = response.Code;
                        requ.Message = response.Message;
                        requ.DbUser = Request.DbUser;
                        requ.Locale = Request.Locale;
                        requ.RoutingId = Request.RoutingId;

                        ws.LogAsync(requ);
                    }
                }

                Common.Utils.Concat(ref output, null, "SEND_SMS:", false, true);
                Common.Utils.Concat(ref output, "Code", response.Code, false, false);
                Common.Utils.Concat(ref output, "Message", response.Message, false, false);
                Common.Utils.Concat(ref output, "AuthNumber", response.AuthorizationNumber, false, false);
                Common.Utils.Concat(ref output, "TranDateTime", response.TransactionDateTime, false, false);
                Common.Utils.Concat(ref output, "Phone", Request.Phone, false, false);
                Common.Utils.Concat(ref output, "RoutingId", Request.RoutingId, false, false);
                Common.Utils.Concat(ref output, "Url", Request.Url, false, true);

                log.Debug(output.ToString());
            }

            return response;
        }
    }
}
