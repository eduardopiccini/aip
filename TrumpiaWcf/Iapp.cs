﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace TrumpiaWcf
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface Iapp
    {
        [OperationContract]
        SendSmsResponse SendSms(SendSmsRequest Request);

        // TODO: Add your service operations here
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class Request
    {
        [DataMember]
        public String ListName { get; set; }

        [DataMember]
        public String ApiKey { get; set; }

        [DataMember]
        public String Url { get; set; }
    }

    [DataContract]
    public class SendSmsRequest : Request
    {
        [DataMember]
        public String FirstName { get; set; }

        [DataMember]
        public String LastName { get; set; }

        [DataMember]
        public String CountryCode { get; set; }

        [DataMember]
        public String Phone { get; set; }

        [DataMember]
        public String Message { get; set; }

        [DataMember]
        public String RoutingId { get; set; }

        [DataMember]
        public String Locale { get; set; }

        [DataMember]
        public String DbUser { get; set; }
    }

    [DataContract]
    public class SendSmsResponse
    {
        [DataMember]
        public String Code { get; set; }

        [DataMember]
        public String Message { get; set; }

        [DataMember]
        public String Details { get; set; }

        [DataMember]
        public String AuthorizationNumber { get; set; }

        [DataMember]
        public String ContactId { get; set; }

        [DataMember]
        public String RequestMsg { get; set; }

        [DataMember]
        public String ResponseMsg { get; set; }
    }

    public class TrumpiaResponse
    {
        public string requestID { get; set; }
    }

    public class TrumpiaAlreadyCheckResponse
    {
        public string statuscode { get; set; }
        public string errorcode { get; set; }
        public string errormessage { get; set; }
        public List<List<string>> duplicate { get; set; }
    }

    public class TrumpiaNewCheckResponse
    {
        public string statuscode { get; set; }
        public string message { get; set; }
        public string contactid { get; set; }
    }
}
