﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Net;
using System.IO;
using log4net;

namespace TrumpiaWcf
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    public class app : Iapp
    {
        private static readonly ILog log = LogManager.GetLogger("Log");
        private static readonly ILog logSummary = LogManager.GetLogger("LogSummary");

        public SendSmsResponse SendSms(SendSmsRequest Request)
        {
            SendSmsResponse result = null;
            StringBuilder data = new StringBuilder();
            String response = String.Empty;
            TrumpiaResponse trumpiaResponse = null;
            DateTime? begin = DateTime.Now, end = null;
            StringBuilder output = new StringBuilder();

            try
            {
                if (Request.Phone.ToArray()[0].Equals('1'))
                {
                    Request.Phone = Request.Phone.Remove(0, 1);
                }

                result = this.AddContact(Request.ApiKey, Request.ListName, Request.Url, 
                    Request.FirstName, Request.LastName, Request.CountryCode, Request.Phone);

                if (result.Code.Equals(Common.Utils.APPROVED))
                {
                    System.Threading.Thread.Sleep(5000);

                    result = this.CheckResponse(Request.Url, result.AuthorizationNumber);

                    if (result.Code.Equals(Common.Utils.APPROVED))
                    {
                        data.Append("apikey=");
                        data.Append(Request.ApiKey);
                        data.Append("&email_mode=FALSE&im_mode=FALSE&sms_mode=TRUE&sb_mode=FALSE&description=text+message&sms_message=");
                        data.Append(Request.Message);
                        data.Append("&contact_ids=");
                        data.Append(result.ContactId);


                        result.RequestMsg += "\n";
                        result.RequestMsg += data.ToString();

                        response = HttpSender(Request.Url + "sendtocontact", "POST", data.ToString());

                        trumpiaResponse = Common.JsonHelper.JsonDeserialize<TrumpiaResponse>(response);

                        result.Code = Common.Utils.APPROVED;
                        result.Message = Common.Utils.APPROVED_MESSAGE;
                        result.AuthorizationNumber = trumpiaResponse.requestID;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNCAUGHT_ERROR;
                result.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                result.Details = ex.Message;
            }
            finally
            {
                end = DateTime.Now;

                if (!String.IsNullOrEmpty(Request.RoutingId))
                {
                    using (TransLogWs.Service ws = new TransLogWs.Service())
                    {
                        TransLogWs.LogRequest requ = new TransLogWs.LogRequest();

                        requ.AuthorizationNumber = result.AuthorizationNumber;
                        requ.BeginDate = begin.Value;
                        requ.EndDate = end.Value;
                        requ.Code = result.Code;
                        requ.Message = result.Message;
                        requ.DbUser = Request.DbUser;
                        requ.Locale = Request.Locale;
                        requ.RoutingId = Request.RoutingId;

                        ws.LogAsync(requ);
                    }
                }
                
                Common.Utils.Concat(ref output, null, "SEND_SMS:", false, true);
                Common.Utils.Concat(ref output, "Code", result.Code, false, false);
                Common.Utils.Concat(ref output, "Message", result.Message, false, false);
                Common.Utils.Concat(ref output, "AuthNumber", result.AuthorizationNumber, false, false);
                Common.Utils.Concat(ref output, "Phone", Request.Phone, false, false);
                Common.Utils.Concat(ref output, "Message", Request.Message, false, false);
                Common.Utils.Concat(ref output, "ListName", Request.ListName, false, false);
                Common.Utils.Concat(ref output, "RoutingId", Request.RoutingId, false, true);

                logSummary.Debug(output.ToString());

                Common.Utils.Concat(ref output, ",FirstName", Request.FirstName, false, false);
                Common.Utils.Concat(ref output, "LastName", Request.LastName, false, false);
                Common.Utils.Concat(ref output, "Url", Request.Url, false, false);
                Common.Utils.Concat(ref output, "RequestMsg", result.RequestMsg, true, false);
                Common.Utils.Concat(ref output, "ResponseMsg", result.ResponseMsg, true, false);
                
                log.Debug(output.ToString());
            }

            return result;
        }

        private SendSmsResponse AddContact(String apikey, String listName, String url, String firstName, String lastName, String countryCode, String phone)
        {
            SendSmsResponse result = new SendSmsResponse();
            String response = null;
            StringBuilder data = new StringBuilder();
            TrumpiaResponse tresp = null;

            try
            {
                url = url + "addcontact";

                data.Append("apikey=");
                data.Append(apikey);
                data.Append("&first_name=");
                data.Append(firstName);
                data.Append("&last_name=");
                data.Append(lastName);
                data.Append("&country_code=");
                data.Append(countryCode);
                data.Append("&mobile_number=");
                data.Append(phone);
                data.Append("&list_name=");
                data.Append(listName);
                data.Append("&send_verification=FALSE&allow_access_info=TRUE");

                result.RequestMsg = data.ToString();


                response = HttpSender(url, "POST", data.ToString());

                result.ResponseMsg = response;

                response = @response.Replace("\\", "\"");

                tresp = Common.JsonHelper.JsonDeserialize<TrumpiaResponse>(response);

                result.Code = Common.Utils.APPROVED;
                result.Message = Common.Utils.APPROVED_MESSAGE;
                result.AuthorizationNumber = tresp.requestID;
            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNCAUGHT_ERROR;
                result.Message = Common.Utils.UNCAUGHT_ERROR_MESSAGE;
                result.Details = ex.Message;
            }

            return result;
        }

        private SendSmsResponse CheckResponse(String url, String requestId)
        {
            SendSmsResponse result = new SendSmsResponse();
            String response = null;
            StringBuilder data = new StringBuilder();
            TrumpiaAlreadyCheckResponse checkAlready = new TrumpiaAlreadyCheckResponse();
            TrumpiaNewCheckResponse checkNew = new TrumpiaNewCheckResponse();

            try
            {
                url = url + "checkresponse";

                data.Append("request_id=");
                data.Append(requestId);

                result.RequestMsg = data.ToString();

                response = HttpSender(url, "POST", data.ToString());

                result.ResponseMsg = response;

                response = @response.Replace("\\", "\"");

                if (response.Contains("Add Contact Success"))
                {
                    checkNew = Common.JsonHelper.JsonDeserialize<TrumpiaNewCheckResponse>(response);

                    result.Code = checkNew.statuscode;
                    result.Message = checkNew.message;


                    if (result.Code.Equals("1"))
                    {
                        result.Code = Common.Utils.APPROVED;
                        result.ContactId = checkNew.contactid;
                    }
                }
                else
                {
                    checkAlready = Common.JsonHelper.JsonDeserialize<TrumpiaAlreadyCheckResponse>(response);

                    result.Code = checkAlready.statuscode;
                    result.Message = checkAlready.errormessage;


                    if (result.Code.Equals("0"))
                    {
                        result.Code = Common.Utils.APPROVED;
                        result.ContactId = checkAlready.duplicate[0][0].ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                result.Code = Common.Utils.UNCAUGHT_ERROR;
                result.Details = response;
            }

            return result;
        }

        private String HttpSender(String url, String method, String xml)
        {
            Uri uri;
            String msg = null;
            HttpWebRequest req = null;
            HttpWebResponse resp = null;
            byte[] data = null;
            StreamReader reader = null;


            try
            {
                uri = new Uri(url);

                req = (HttpWebRequest)WebRequest.Create(uri);
                req.ContentType = "application/x-www-form-urlencoded";
                req.Method = method;
                req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                //req.Headers.Add("Accept-Encoding","gzip,deflate,sdch");
                req.Headers.Add("Accept-Language", "es-ES,es;q=0.8");
                req.Headers.Add("Cache-Control", "max-age=0");
                req.Headers.Add("Accept-Encoding", "gzip,deflate,sdch");
                req.UserAgent = "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36";
                req.Headers.Add("Origin", "https://mail-attachment.googleusercontent.com");
                req.KeepAlive = true;
                req.ProtocolVersion = HttpVersion.Version11;
                System.Net.ServicePointManager.Expect100Continue = false;
                req.CookieContainer = new CookieContainer();
                req.AutomaticDecompression = DecompressionMethods.GZip;
                req.AllowAutoRedirect = true;

                if (!String.IsNullOrEmpty(xml))
                {
                    data = System.Text.ASCIIEncoding.UTF8.GetBytes(xml);

                    req.ContentLength = data.Length;
                    System.IO.Stream os = req.GetRequestStream();
                    os.Write(data, 0, data.Length);
                    os.Close();
                }

                resp = (HttpWebResponse)req.GetResponse();

                reader = new StreamReader(resp.GetResponseStream());

                msg = reader.ReadToEnd();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (resp != null)
                {
                    resp.Close();
                    resp = null;
                }

                if (reader != null)
                {
                    reader.Close();
                    reader = null;
                }
            }

            return msg;
        }
    }
}
